# Contributing to _PanTools_

## Get in touch

There are many ways to get in touch with the _PanTools_ team!

- GitLab [issues][pantools-issues] and [merge requests][pantools-mergerequests]
  - Join a discussion, collaborate on an ongoing task and exchange your thoughts with others. You will have to request a guest role to start contributing.
  - Can't find your idea being discussed anywhere?
    [Open a new issue](https://git.wur.nl/bioinformatics/pantools/-/issues/new)! (See our [Where to start: issues](#where-to-start-issues) section below.)
- Contact the Project Lead of the _PanTools_ project - Sandra Smit - by email at [pantools@bioinformatics.nl](mailto:pantools@bioinformatics.nl).

## Contributing through GitLab

[Git][git] is a really useful tool for version control. 
[GitLab][gitlab] sits on top of Git and supports collaborative and distributed working.

We know that it can be daunting to start using Git and GitLab if you haven't worked with them in the past, but the _PanTools_ maintainers are here to help you figure out any of the jargon or confusing instructions you encounter.

GitLab has a helpful page on [getting started with GitLab][gitlab-gettingstarted].

In order to contribute via GitLab, you'll need to [set up an account][gitlab-signup] and [sign in][gitlab-signin].
Remember that you can ask us any questions you need to along the way.

## Where to start: issues

Before you open a new issue, please check if any of our [open issues](https://git.wur.nl/bioinformatics/pantools/-/issues?scope=all&state=opened) cover your idea already.

## Making a change with a merge request

We appreciate all contributions to _PanTools_.
**THANK YOU** for helping us build this useful resource.

All project management, conversations and questions related to the _PanTools_ project happens here in the _PanTools_ repository.

The following steps are a guide to help you contribute in a way that will be easy for everyone to review and accept with ease.

### 1. Comment on an [existing issue][pantools-issues] or open a new issue referencing your addition

This allows other members of the _PanTools_ team to confirm that you aren't overlapping with work that's currently underway and that everyone is on the same page with the goal of the work you're going to carry out.

[This blog](https://www.igvita.com/2011/12/19/dont-push-your-pull-requests/) is a nice explanation of why putting this work in upfront is so useful to everyone involved.

You will need a guest role to create new issues using the _PanTools_ repository website. You can request a guest role by [getting in touch](#get-in-touch).

Alternatively, you can send an [email][pantools-email] to create a new issue. The title of the email will be used as the issue title and the email body will be put in the issue description.
Note that we are in the process of setting up this email functionality; when this is done, we will update the email link here.

### 2. [Fork][gitlab-fork] the [_PanTools_ repository][pantools-repo]

This is now your own unique copy of _PanTools_.
Changes here won't affect anyone else's work, so it's a safe space to explore edits to the code!

Make sure to [keep your fork up to date][gitlab-syncfork] with the main repository, otherwise, you can end up with lots of dreaded [merge conflicts][gitlab-mergeconflicts].

The repository website only provides functionality for forking inside the same server (git.wur.nl).
You can fork the _PanTools_ project onto another GitLab server.
Forking onto a GitHub server is currently not possible, but this is being worked on.
First create a blank repository on GitLab, let's assume it's called https://gitlab.com/johndoe/my-pantools.
Use the following commands in a directory where you wish to fork _PanTools_.

~~~bash
mkdir my-pantools
cd my-pantools
git init
git remote add origin https://gitlab.com/johndoe/my-pantools
git remote add upstream https://git.wur.nl/bioinformatics/pantools
#There are now two remotes: origin (your remote fork) and upstream (the pantools repository).

git pull upstream develop				#Get the pantools content.
git push --set-upstream origin develop	#Push this content to the origin (fork).
~~~

### 3. Make the changes you've discussed

Try to keep the changes focused.
If you submit a large amount of work all in one go it will be much more work for whoever is reviewing your merge request.

While making your changes, commit often and write good, detailed commit messages.
[This blog](https://chris.beams.io/posts/git-commit/) explains how to write a good Git commit message and why it matters.
It is also perfectly fine to have a lot of commits - including ones that break code.
A good rule of thumb is to push up to GitLab when you _do_ have passing tests then the continuous integration (CI) has a good chance of passing everything.

If you feel tempted to "branch out" then please make a [new branch][gitlab-branches] and a [new issue](https://git.wur.nl/bioinformatics/pantools/-/issues/new) to go with it.
[This blog](https://nvie.com/posts/a-successful-git-branching-model/) details the different Git branching models.

Please do not re-write history!
That is, please do not use the [rebase](https://docs.gitlab.com/ee/topics/git/git_rebase.html) command to edit previous commit messages, combine multiple commits into one, or delete or revert commits that are no longer necessary.

Are you new to Git and GitLab or just want a detailed guide on getting started with version control? Check out the [Version Control chapter](https://the-turing-way.netlify.com/version_control/version_control.html) in _The Turing Way_ Book!

### 4. Submit a [merge request][gitlab-mergerequest]

We encourage you to open a merge request as early in your contributing process as possible.
This allows everyone to see what is currently being worked on.
It also provides you, the contributor, feedback in real-time from both the community and the continuous integration as you make commits (which will help prevent stuff from breaking).

When you are ready to submit a merge request, please describe in the merge request body:

- The problem you're trying to fix in the merge request, reference any related issue and use fixes/close to automatically close them, if pertinent.
- A list of changes proposed in the merge request.
- What the reviewer should concentrate their feedback on.

By providing as much detail as possible, you will make it really easy for someone to review your contribution!

If you have opened the merge request early and know that its contents are not ready for review or to be merged, add "Draft: " at the start of the merge request title.
When you are happy with it and are happy for it to be merged into the main repository, remove the "Draft: " in the title of the merge request.

Please make sure to select the correct target branch for your merge request.
For most cases, this will be the `develop` branch.
Also see the section about [Git organization](#git-organization) for more information on the branching model used in _PanTools_.

A member of the _PanTools_ team will then review your changes to confirm that they can be merged into the main repository.
A [review][gitlab-review] will probably consist of a few questions to help clarify the work you've done.
Keep an eye on your GitLab notifications and be prepared to join in that conversation.

You can update your [fork][gitlab-fork] of the _PanTools_ [repository][pantools-repo] and the merge request will automatically update with those changes.
You don't need to submit a new merge request when you make a change in response to a review.

You can also submit merge requests to other contributors' branches!
Do you see an [open merge request](https://git.wur.nl/bioinformatics/pantools/-/merge_requests?scope=all&state=opened ) that you find interesting and want to contribute to?
Simply make your edits on their files and open a merge request to their branch!

GitLab has a [nice introduction][gitlab-flow] to the merge request workflow, but please [get in touch](#get-in-touch) if you have any questions.

## Local development

You can build and run _PanTools_ locally.
Please refer to the [manual][pantools-manual] for instructions on how to setup, build, and run _PanTools_.
This is a short-list of important parts of our setup:
- We use [pre-commit](https://pre-commit.com/) to run checks before committing.
- We use [IntelliJ IDEA](https://www.jetbrains.com/idea/) as our IDE.
- We use [Maven](https://maven.apache.org/) as our build tool.
- We use [log4j2](https://logging.apache.org/log4j/2.x/) for logging.
- We use [ReadTheDocs](https://readthedocs.org/) for our documentation.
- We use [Snakemake](https://snakemake.readthedocs.io/) for our end-to-end tests in the CI/CD pipeline.

## Git organization

The _PanTools_ project uses the following branching model:

```mermaid
%%{init: { 'gitGraph': {'mainBranchName': "pantools_v4", 'showCommitLabel': false }} }%%
gitGraph:
  options
    {
      "node-revert-next-line": false,
      "node-revert-commit": false
    }
  end
  commit
  branch develop order: 2
  commit
  checkout develop
  branch add_small_feature1 order: 3
  commit
  commit
  commit
  checkout develop
  branch add_big_feature order: 4
  commit
  commit
  checkout develop
  merge add_small_feature1
  checkout develop
  branch add_small_feature2 order: 3
  commit
  commit
  checkout develop
  merge add_small_feature2
  checkout add_big_feature
  commit
  commit
  merge develop
  commit
  checkout develop
  branch release_v4.3.1 order: 1
  commit
  checkout pantools_v4
  merge release_v4.3.1 tag: "v4.3.1"
  commit
  checkout develop
  merge release_v4.3.1
  merge add_big_feature
  commit
```

- `pantools_v4` is the main branch of the _PanTools_ project.
  This branch should always be stable and contain the latest release of _PanTools_.
- `develop` is the branch where all new features are developed.
  This branch should always be stable and contain the latest development version of _PanTools_.
- `release_v4.3.1` is a release branch.
  When a new release is ready, a release branch is created from `develop`.
  In this release branch, only updates necessary for the release are made (e.g. version number updates, critical minor bugs, etc.).
  The release branch is merged into `pantools_v4` and tagged with the release version.
  The release branch is also merged back into `develop` to make sure that the release is also part of the development version.
- Feature branches are created from `develop` and merged back into `develop` when the feature is ready.
  Make sure to keep your feature branches up-to-date with `develop` by regularly merging `develop` into your feature branch.

## Recognizing Contributions

We welcome and recognise all kinds of contributions, from fixing small errors, to developing documentation, maintaining the project infrastructure, writing code or reviewing existing resources.

### Current Contributors

The _PanTools_ team wants to graciously thank the following people for their contributions to the _PanTools_ project.

- Sandra Smit
- Dick de Ridder
- Siavash Sheikhizadeh
- Eef Jonkheer
- Dirk-Jan van Workum
- Robin van Esch
- Matthijs Moed
- Nauman Ahmed
- Thijs van Lankveld
- Astrid van den Brandt
- Meixin Yang
- Lakhansing Pardeshi

---

_These Contributing Guidelines have been adapted from the [Contributing Guidelines](https://github.com/bids-standard/bids-starter-kit/blob/master/CONTRIBUTING.md) of the [BIDS Starter Kit](https://github.com/bids-standard/bids-starter-kit)! (License: CC-BY)_

[pantools-repo]: https://git.wur.nl/bioinformatics/pantools
[pantools-issues]: https://git.wur.nl/bioinformatics/pantools/-/issues
[pantools-mergerequests]: https://git.wur.nl/bioinformatics/pantools/-/merge_request
[pantools-manual]: https://pantools.readthedocs.io/
[pantools-email]: mailto:pantools@bioinformatics.nl
[git]: https://git-scm.com
[gitlab]: https://gitlab.com
[gitlab-signup]: https://gitlab.com/users/sign_up
[gitlab-signin]:https://gitlab.com/users/sign_in
[gitlab-gettingstarted]:https://about.gitlab.com/get-started/
[gitlab-fork]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[gitlab-syncfork]: https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/
[gitlab-mergeconflicts ]:https://docs.gitlab.com/ee/topics/git/merge_conflicts.html
[gitlab-branches]: https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch
[gitlab-rebase]:https://docs.gitlab.com/ee/topics/git/git_rebase.html
[gitlab-mergerequest]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[gitlab-review]: https://docs.gitlab.com/ee/development/code_review.html
[gitlab-flow]: https://docs.gitlab.com/ee/topics/gitlab_flow.html
[markdown]: https://daringfireball.net/projects/markdown
[rick-roll]: https://www.youtube.com/watch?v=dQw4w9WgXcQ