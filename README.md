[![install with bioconda](https://img.shields.io/badge/install%20with-bioconda-brightgreen.svg?style=flat)](http://bioconda.github.io/recipes/pantools/README.html)
[![bioconda downloads](https://img.shields.io/conda/dn/bioconda/pantools.svg?label=bioconda%20downloads)](https://anaconda.org/bioconda/pantools)
[![documentation status](https://readthedocs.org/projects/pantools/badge/?version=stable)](https://pantools.readthedocs.io/)
[![pipeline](https://git.wur.nl/bioinformatics/pantools/badges/pantools_v4/pipeline.svg)](https://git.wur.nl/bioinformatics/pantools/-/pipelines?ref=pantools_v4)
[![latest release](https://git.wur.nl/bioinformatics/pantools/-/badges/release.svg)](https://git.wur.nl/bioinformatics/pantools/-/releases)


# PanTools version 4
PanTools is a pangenomic toolkit for comparative analysis of large number of genomes. It is developed in the Bioinformatics Group of Wageningen University, the Netherlands. Please cite the relevant publication(s) from the list of publications if you use PanTools in your research.


## Publications
- [2022] [PanTools v3: functional annotation, classification, and phylogenomics](https://doi.org/10.1093/bioinformatics/btac506)
- [2021] [The *Pectobacterium* pangenome, with a focus on *Pectobacterium brasiliense*, shows a robust core and extensive exchange of genes from a shared gene pool.](https://doi.org/10.1186/s12864-021-07583-5)
- [2019] [Pan-genomic read mapping](https://www.biorxiv.org/content/10.1101/813634v1.full)
- [2018] [Efficient inference of homologs in large eukaryotic pan-proteomes.](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2362-4)
- [2016] [PanTools: representation, storage and exploration of pan-genomic data.](https://academic.oup.com/bioinformatics/article/32/17/i487/2450785)

## Functionalities
PanTools currently provides these functionalities:

- Construction of a pangenome
- Construction of a panproteome
- Adding new genomes to the pangenome
- Adding structural annotations to the genomes
- Detecting homology groups based on similarity of proteins
- Retrieving features/regions/genomes
- Read mapping
- Gene classification
- Functional annotation incorporation
- Phylogenetic methods
- Optimal homology grouping using BUSCO

## Documentation
An extensive manual is available at [https://pantools.readthedocs.io/](https://pantools.readthedocs.io/).

## Cloning this git
For cloning this git, please run:
```bash
git clone https://git.wur.nl/bioinformatics/pantools.git
cd pantools
```

And check out a desired version (e.g. `v4.0.0`):
```bash
git checkout v4.0.0
```

We provide conda environments for installing all dependencies of PanTools, which can be installed using conda/mamba (we recommend using mamba):
```bash
mamba env create -n pantools -f conda.yaml
```

## Building a runnable jar
To build a standalone jar that can be run on any machine with a compatible JVM without any dependencies, install [Maven](https://maven.apache.org) and JDK version 8.
Then run `mvn package` in the PanTools root directory:

```bash
mvn package
```

<!--
Note: tests are broken at the moment, which is why we're skipping the Maven test phase with `-DskipTests=true`.
-->

`mvn package` will generate two jar files in the `target/` directory.
The standalone jar file is named `pantools-<VERSION>.jar`, with `<VERSION>` being the PanTools version you have checked out.
To run it, use (we'll take version `v4.0.0` as an example):

```bash
java -jar target/pantools-4.0.0.jar
```

## Contact
Should you have any questions or issues concerning PanTools, please contact us at pantools@bioinformatics.nl.


## Licence
PanTools has been licensed under [GNU GENERAL PUBLIC LICENSE version 3](https://www.gnu.org/licenses/gpl-3.0.en.html):

    Copyright (C) 2016  the PanTools team, Bioinformatics Group, Wageningen University & Research

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
