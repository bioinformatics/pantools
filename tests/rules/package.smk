"""
Package the local and remote (reference) PanTools jar files.
"""

rule package_local_pantools:
    """Package the local (git) version of PanTools into a .jar."""
    output:
        "jars/pantools/target/pantools-local.jar"
    shell:
        """
        {maven} -f ../pom.xml clean package
        mv ../target/pantools-*.jar {output}
        """

rule package_remote_pantools_version:
    """
    Package any PanTools version into a .jar by cloning it from git.
    NOTE: to reduce clone time and disk space the repo is cloned only to a limited
    depth. The reference version should be updated periodically.
    """
    output:
        "jars/pantools/target/pantools-{build_version}.jar"
    params:
        repository_url=config["tools"]["pantools"]["repository_url"],
        clone_depth=150
    shell:
        """
        # TODO: clean up
        TEMPORARY_DIRECTORY=$(mktemp -d)
        git clone --depth {params.clone_depth} --branch develop {params.repository_url} ${{TEMPORARY_DIRECTORY}}
        cd ${{TEMPORARY_DIRECTORY}}
        git checkout {wildcards.build_version}
        cd -
        {maven} -f ${{TEMPORARY_DIRECTORY}}/pom.xml clean package
        mv ${{TEMPORARY_DIRECTORY}}/target/pantools-*.jar {output}
        rm -rf ${{TEMPORARY_DIRECTORY}}
        """