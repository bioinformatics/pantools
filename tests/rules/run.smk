"""
Run pantools PanTools functions.
"""

rule build_pangenome:
    """Run pantools build_pangenome with a versioned .jar on a genome_locations.txt."""
    input:
        jar="jars/pantools/target/pantools-{build_version}.jar",
        genomes=config["dataset"]["genomes"]
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.build.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            build_pangenome \
            {params.database} \
            {input.genomes} \
            --threads=30 \
            --kmer-size=13
        """

checkpoint build_panproteome:
    """Run pantools build_pangenome with a versioned .jar on a genome_locations.txt."""
    input:
        jar="jars/pantools/target/pantools-{build_version}.jar",
        proteomes=config["dataset"]["proteomes"]
    output:
        done_marker1=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.build.done"),
        done_marker2=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            build_panproteome \
            {params.database} \
            {input.proteomes}
        """

rule export_pangenome:
    """Export pangenome to multiple csv files, each containing different properties."""
    input:
        "output/done_markers/{dataset_name}_{dataset_type}/{build_version}.build.done",
        jar="jars/pantools/target/pantools-local.jar"
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.export.done"),
        node_properties="output/exports/{dataset_name}_{dataset_type}/{build_version}/node-properties.csv",
        rel_properties="output/exports/{dataset_name}_{dataset_type}/{build_version}/relationship-properties.csv",
        sequence_node_anchors="output/exports/{dataset_name}_{dataset_type}/{build_version}/sequence-node-anchors.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            export_pangenome \
            {params.database} \
           --node-properties-file={output.node_properties} \
           --relationship-properties-file={output.rel_properties} \
           --sequence-node-anchors-file={output.sequence_node_anchors}
        """

checkpoint add_annotations:
    """Run pantools add_annotations with a versioned .jar on a annotation_locations.txt."""
    input:
        rules.export_pangenome.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar",
        annotations=config["dataset"]["annotations"]
    output:
        proteins=directory("output/databases/{dataset_name}_{dataset_type}/{build_version}/proteins/"),
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            add_annotations \
            {params.database} \
            {input.annotations}
        """

rule group:
    """Run pantools group with a versioned .jar"""
    input:
        rules.export_pangenome.output.done_marker,
        "output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done",
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.group.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            group \
            {params.database} \
            --relaxation=3
        """

rule grouping_overview:
    """Run pantools grouping_overview."""
    input:
        rules.group.output.done_marker,
        jar="jars/pantools/target/pantools-local.jar"
    output:
        overview1="output/databases/{dataset_name}_{dataset_type}/{build_version}/pantools_homology_groups.txt",
        overview2="output/databases/{dataset_name}_{dataset_type}/{build_version}/group/grouping_overview.txt"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            grouping_overview \
            {params.database}
        """

rule add_phenotypes:
    """Run pantools add_phenotypes with a versioned .jar on a phenotypes file."""
    input:
        rules.export_pangenome.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar",
        phenotypes=config["dataset"]["phenotypes"]
    output:
        phenotype_overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/phenotype_overview.txt",
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.phenotypes.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            add_phenotypes \
            {params.database} \
            {input.phenotypes}
        """

rule add_pav:
    """Run pantools add_pav with a versioned .jar on a pav file."""
    input:
        rules.export_pangenome.output.done_marker,
        "output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done",
        jar="jars/pantools/target/pantools-{build_version}.jar",
        pav=config["dataset"]["pav"]
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.pav.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            add_pav \
            {params.database} \
            {input.pav}
        """

rule add_variants:
    """Run pantools add_variants with a versioned .jar on a vcf file."""
    input:
        rules.export_pangenome.output.done_marker,
        "output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done",
        jar="jars/pantools/target/pantools-{build_version}.jar",
        vcf=config["dataset"]["vcf"]
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.vcf.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            add_variants \
            {params.database} \
            {input.vcf}
        """

rule variation_overview:
    """Run pantools variation_overview."""
    input:
        rules.add_pav.output.done_marker,
        rules.add_variants.output.done_marker if dataset_type == "pangenome" else [],
        jar="jars/pantools/target/pantools-local.jar"
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/variation/variation_overview.txt"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            variation_overview \
            {params.database}
        """

rule gene_classification:
    """Run pantools gene_classification"""
    input:
        rules.group.output.done_marker,
        rules.add_phenotypes.output.done_marker,
        jar="jars/pantools/target/pantools-local.jar"
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.homology_id.done"),
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/"
                 "gene_classification_overview.txt",
        accessory_groups="output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/"
                         "group_identifiers/accessory_groups.csv",
        single_copy_orthologs="output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/"
                              "group_identifiers/single_copy_orthologs.csv",
        classified_groups="output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/"
                          "classified_groups.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            gene_classification \
            {params.database} \
            --phenotype=Reference

        #TODO: REMOVE BELOW
        if [ -f {params.database}/gene_classification/accessory_groups.csv ]; then mkdir -p $(dirname {output.accessory_groups}); cp {params.database}/gene_classification/accessory_groups.csv {output.accessory_groups}; fi
        if [ -f {params.database}/gene_classification/single_copy_orthologs.csv ]; then mkdir -p $(dirname {output.single_copy_orthologs}); cp {params.database}/gene_classification/single_copy_orthologs.csv {output.single_copy_orthologs}; fi
        if [ -f {output.accessory_groups} ]; then cp {output.accessory_groups} $(dirname {output.classified_groups})/; fi
        if [ -f {output.single_copy_orthologs} ]; then cp {output.single_copy_orthologs} $(dirname {output.classified_groups})/; fi
        """

rule kmer_classification:
    """Run pantools kmer_classification"""
    input:
        rules.export_pangenome.output.done_marker,
        jar="jars/pantools/target/pantools-local.jar"
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
                 "kmer_classification/kmer_classification_overview.txt"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            kmer_classification \
            {params.database}
        """

rule pangenome_structure_gene:
    """Run pantools pangenome_structure"""
    input:
        rules.group.output.done_marker,
        rules.add_pav.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
                 "pangenome_size/gene/pangenome_size.txt",
        csv1="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
             "pangenome_size/gene/core_accessory_unique_size.csv",
        csv2="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
             "pangenome_size/gene/core_dispensable_size.csv",
        csv3="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
             "pangenome_size/gene/core_dispensable_total_size.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            pangenome_structure \
            {params.database} \
            --pavs \
            --seed=12345
        """

rule pangenome_structure_kmer:
    """Run pantools pangenome_structure --kmer"""
    input:
        rules.group.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
                 "pangenome_size/kmer/pangenome_size_kmer.txt",
        csv1="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
             "pangenome_size/kmer/core_accessory_size.csv",
        csv2="output/databases/{dataset_name}_{dataset_type}/{build_version}/" \
             "pangenome_size/kmer/core_accessory_unique_size.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            pangenome_structure \
            {params.database} \
            --kmer \
            --seed=12345 \
            --threads=1
        """

rule add_functions:
    """Run pantools add_functions with a versioned .jar on a function_locations.txt."""
    input:
        rules.export_pangenome.output.done_marker,
        "output/done_markers/{dataset_name}_{dataset_type}/{build_version}.proteins.done",
        jar="jars/pantools/target/pantools-{build_version}.jar",
        functions=config["dataset"]["functions"]
    output:
        done_marker=touch("output/done_markers/{dataset_name}_{dataset_type}/{build_version}.functions.done")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            add_functions \
            {params.database} \
            {input.functions} \
            --functional-databases-directory=functional_databases
        """

rule functional_classification:
    """Run pantools functional_classification."""
    input:
        rules.add_functions.output.done_marker,
        rules.add_phenotypes.output.done_marker,
        jar="jars/pantools/target/pantools-local.jar"
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/" \
                 "functional_classification/functional_classification_overview.txt",
        core="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/" \
             "functional_classification/core_functions.txt",
        accessory="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/" \
                  "functional_classification/accessory_functions.txt",
        unique="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/" \
               "functional_classification/unique_functions.txt",
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            functional_classification \
            {params.database} \
            --phenotype=Reference
        """

rule function_overview:
    """Run pantools function_overview."""
    input:
        rules.add_functions.output.done_marker,
        rules.group.output.done_marker,
        jar="jars/pantools/target/pantools-local.jar"
    output:
        go="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_overview.csv",
        interpro="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/interpro_overview.csv",
        pfam="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/pfam_overview.csv",
        tigrfam="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/tigrfam_overview.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            function_overview \
            {params.database}
        """

rule go_enrichment:
    """Run pantools go_enrichment."""
    input:
        rules.add_functions.output.done_marker,
        rules.group.output.done_marker,
        groups=rules.gene_classification.output.accessory_groups,
        jar="jars/pantools/target/pantools-{build_version}.jar",
    output:
        overview="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/" \
                 "go_enrichment_overview_per_go.txt",
        csv="output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/go_enrichment.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            go_enrichment \
            {params.database} \
            --homology-file={input.groups}
        """

checkpoint map_reads:
    """Map reads against a database, outputs a SAM file per genome."""
    input:
        rules.export_pangenome.output.done_marker,
        fastq1=config["dataset"]["reads"]["fastq1"],
        fastq2=config["dataset"]["reads"]["fastq2"],
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        sam_files=directory("output/databases/{dataset_name}_{dataset_type}/{build_version}/read_mapping/")
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        mkdir -p {output.sam_files}; # TODO: add to FileUtils.java in PanTools Java code
        {java} \
            -jar {input.jar} \
            map \
            {params.database} \
            {input.fastq1} \
            {input.fastq2} \
            --threads=1 \
            --gap-open=-20 \
            --gap-extension=-3 \
            --out-format=SAM
        """

checkpoint msa_var:
    """"Run multiple sequence alignments on a pangenome."""
    input:
        "output/done_markers/{dataset_name}_pangenome/{build_version}.group.done",
        "output/done_markers/{dataset_name}_pangenome/{build_version}.pav.done",
        "output/done_markers/{dataset_name}_pangenome/{build_version}.vcf.done",
        "output/done_markers/{dataset_name}_pangenome/{build_version}.phenotypes.done",
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        done_marker=touch("output/done_markers/{dataset_name}_pangenome/{build_version}.msa.done"),
        alignments=directory("output/databases/{dataset_name}_pangenome/{build_version}/alignments/"
                             "msa_per_group_var/grouping_v1/")
    params:
        database="output/databases/{dataset_name}_pangenome/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            msa \
            {params.database} \
            --threads=30 \
            --align-variants \
            --pavs \
            --phenotype
        """

checkpoint msa_prot:
    """"Run multiple sequence alignments on a panproteome."""
    input:
        "output/done_markers/{dataset_name}_panproteome/{build_version}.group.done",
        "output/done_markers/{dataset_name}_panproteome/{build_version}.phenotypes.done",
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        done_marker=touch("output/done_markers/{dataset_name}_panproteome/{build_version}.msa.done"),
        alignments=directory("output/databases/{dataset_name}_panproteome/{build_version}/alignments/"
                             "msa_per_group/grouping_v1/")
    params:
        database="output/databases/{dataset_name}_panproteome/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            msa \
            {params.database} \
            --threads=30 \
            --align-protein \
            --phenotype
        """

rule ani:
    """Run pantools ani."""
    input:
        rules.add_phenotypes.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar",
    output:
        csv="output/databases/{dataset_name}_{dataset_type}/{build_version}/ANI/MASH/ANI_distance_matrix.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/"
    shell:
        """
        {java} \
            -jar {input.jar} \
            ani \
            {params.database} \
            --phenotype=Reference
        """

rule core_phylogeny:
    """Run pantools core_phylogeny."""
    input:
        rules.gene_classification.output.done_marker,
        rules.msa_var.output.done_marker if dataset_type == "pangenome" else rules.msa_prot.output.done_marker,
        jar="jars/pantools/target/pantools-{build_version}.jar"
    output:
        csv="output/databases/{dataset_name}_{dataset_type}/{build_version}/core_phylogeny/sites_per_group.csv"
    params:
        database="output/databases/{dataset_name}_{dataset_type}/{build_version}/",
        variants="--variants --pavs" if config["dataset"]["vcf"] else ""
    shell:
        """
        {java} \
            -jar {input.jar} \
            core_phylogeny \
            {params.database} \
            {params.variants}
        """
