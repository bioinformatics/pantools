"""
Clean PanTools output files that would otherwise fail comparison.
Remove non-deterministic data like node identifiers and dates, sort and aggregate files.
"""

rule sort_exported_csvs:
    """Sort CSVs exported by export_pangenome."""
    input:
        "output/exports/{dataset_name}_{dataset_type}/{build_version}/{filename}.csv"
    output:
        "output/exports/{dataset_name}_{dataset_type}/{build_version}/{filename}.sorted.csv"
    priority: 50
    shell:
        "sort {input} > {output}"

rule strip_sam_pg_header_line:
    """Strip the @PG header line of a SAM file."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/read_mapping/{filename}.sam"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/read_mapping/{filename}.no-pg-header-line.sam"
    priority: 50
    shell:
        "grep -v '^@PG' {input} > {output}"

rule strip_variation_overview_dates:
    """Strip the date properties from the variation_overview.txt."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/variation/variation_overview.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/variation/variation_overview.no-dates.txt"
    priority: 50
    shell:
        "grep -v '^date' {input} > {output}"

rule strip_classified_groups_identifiers:
    """Strip the homology group identifiers from classified_groups.csv and sort it."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/classified_groups.csv"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/gene_classification/" \
            "classified_groups.no-identifiers.csv"
    priority: 50
    shell:
        "sed 's/^[0-9]*,//' {input} | sort > {output}"

rule clean_phenotype_overview:
    """
    Remove the first two parts of the overview and sort the paragraphs of the remaining statistics.
    Remove phenotype node identifiers.
    """
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/phenotype_overview.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/phenotype_overview.cleaned.txt"
    priority: 50
    shell:
        """
        grep -A30 -m1 -e '##Part 3' {input} | \
            perl -n00 -e 'push @a, $_; END {{ print sort @a }}' | \
            sed 's/, phenotype node: [0-9]*$//' > {output}
        """

rule clean_grouping_overview:
    """Remove group identifiers from grouping overview and sort the file."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/group/grouping_overview.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/group/grouping_overview.cleaned.txt"
    priority: 50
    shell:
        "sed -E 's/^[0-9]*, //g' {input} | sed 'N;s/\\n/ /' | sort > {output}"

rule clean_pantools_homology_groups:
    """Remove group identifiers from pantools_homology_groups.txt, sort the file and every line separately."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/pantools_homology_groups.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/pantools_homology_groups.cleaned.txt"
    priority: 50
    shell:
        """
        gawk 'FNR==1{{print; next;}} \
            {{n=split(substr($0, length($1)+2), a, ","); \
            asort(a,b); \
            printf "%s", b[1]; \
            for (i=2; i<=n; i++){{printf ",%s", b[i]}}; printf "\\n";}}' \
            {input} | sort > {output}
        """

rule clean_function_overview:
    """Remove node identifiers from go, interpro, pfam and tigrfam overviews. Only keep connected functions."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/{filename}.csv"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/{filename}.no-identifiers.csv"
    priority: 50
    shell:
        """
        awk 'BEGIN{{display=0; FS=OFS=";"}} \
            /^#Not/{{exit;}} \
            /^$/{{next;}} \
            display{{print;}} \
            /^#Connected/{{display=1;}}' {input} | \
            cut -d';' -f -2,4-8 | tail -n+2 > {output}
        """

rule aggregate_msa_info:
    """Aggregate and sort .info files created by msa."""
    input:
        collect_aggregate_msa_input
    output:
        "output/aggregated/{dataset_name}_{dataset_type}/{build_version}/msa/{subdir}/{filename}.aggregated.{ext,(?!fasta).*}"
    priority: 50
    shell:
        "cat {input} | sort > {output}"

rule aggregate_msa_var_structure:
    """Aggregate and sort var.structure.tsv files created by msa_var."""
    input:
        lambda wildcards: collect_aggregate_msa_input(wildcards, "tsv")
    output:
        "output/aggregated/{dataset_name}_{dataset_type}/{build_version}/msa/{subdir}/{filename}.aggregated.tsv"
    priority: 50
    shell:
        "cat {input} | sort > {output}"

rule aggregate_msa_sequences:
    """Aggregate and sort var.fasta files created by msa."""
    input:
        collect_aggregate_msa_input
    output:
        "output/aggregated/{dataset_name}_{dataset_type}/{build_version}/msa/{subdir}/{filename}.aggregated.{ext,fasta}"
    priority: 50
    shell:
        """
        cat {input} | \
            sed 's/>/\\n&/g' | \
            sed '$a\\' | \
            perl -n00 -e 'push @a, $_; END {{ print sort @a }}' | \
            sed '/^$/d' > {output}
        """

rule clean_core_phylogeny_sites_per_group:
    """Cut non-deterministic columns from siters_per_group.csv."""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/core_phylogeny/sites_per_group.csv",
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/core_phylogeny/sites_per_group.cleaned.csv",
    priority: 50
    shell:
        #"cut -d';' -f 2- {input} | sort > {output};" #TODO: put this back in pipeline (temporarily removed because of added column and changed header to sites_per_group.csv
        """cut -d';' -f 2- {input} | awk 'BEGIN{{FS=OFS=";";}} FNR==1{{next;}} NF==7{{print $1,$2,$3,$4,$6,$7; next;}} {{print;}}' | sort > {output};""" #TODO: remove this from pipeline

rule clean_go_enrichment_overview_per_go:
    """Remove node identifiers from go_enrichment_overview_per_go.txt"""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/go_enrichment_overview_per_go.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/go_enrichment_overview_per_go.cleaned.txt"
    priority: 50
    shell:
        "sed 's/ [[:digit:]]*$//g' {input} | sort > {output}"

rule sort_go_enrichment_csv:
    """Sort the go_enrichment csv"""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/go_enrichment.csv"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/function/go_enrichment/go_enrichment.sorted.csv"
    priority: 50
    shell:
        "sort {input} > {output}"

rule clean_functional_classification:
    """Remove node identifiers"""
    input:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/functional_classification/{filename}.txt"
    output:
        "output/databases/{dataset_name}_{dataset_type}/{build_version}/functional_classification/{filename}.cleaned.txt"
    priority: 50
    shell:
        "sed 's/,[[:digit:]]*$//g' {input} > {output}"