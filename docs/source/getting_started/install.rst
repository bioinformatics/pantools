Installing PanTools
===================

There are multiple options for installing PanTools:

1. :ref:`getting_started/install:install from bioconda` (recommended)
2. :ref:`getting_started/install:install from download`
3. :doc:`/developer_guide/install` (for developers)

--------------

Install from bioconda
---------------------

The easiest way to install PanTools is through `bioconda
<https://bioconda.github.io/>`_. Bioconda is a channel for the conda package
manager specializing in bioinformatics software.

For users that don't have conda installed, we recommend to install conda first:

.. substitution-code-block:: bash

   $ curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
   $ bash Miniforge3-$(uname)-$(uname -m).sh

   # Follow the instructions on the screen
   # Restart your terminal

   $ conda config --set auto_activate_base false
   $ conda config --add channels bioconda
   $ conda config --add channels conda-forge
   $ conda config --remove channels defaults
   $ conda config --set channel_priority strict

   # Restart your terminal

Now, we can install PanTools. We recommend to install PanTools into a
separate environment. Please make sure you install the most recent versions of
the tools.

.. substitution-code-block:: bash

   $ mamba create -n pantools pantools

.. note::

  If this doesn't work, you can try to install PanTools using the
  following way:

  .. substitution-code-block:: bash

     $ mamba create -n pantools --strict-channel-priority -c conda-forge -c bioconda pantools=|ProjectVersion|

Please make sure to activate the environment before using PanTools:

.. substitution-code-block:: bash

   $ conda activate pantools

Test if PanTools is executable:

.. substitution-code-block:: bash

   $ pantools --help

--------------

Install from download
---------------------

The second option is to download PanTools from `our git page
<https://git.wur.nl/bioinformatics/pantools/-/releases>`_. For this, you will
need to install all dependencies separately. We provide YAML files for a conda
environment that has all dependencies.

Install dependencies using conda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All dependencies are listed in conda.yaml which can be found on the
`release page <https://git.wur.nl/bioinformatics/pantools/-/releases>`_.

For users that don't have conda installed, we recommend to install conda first:

.. substitution-code-block:: bash

   $ curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
   $ bash Miniforge3-$(uname)-$(uname -m).sh

   # Follow the instructions on the screen
   # Restart your terminal

   $ conda config --set auto_activate_base false
   $ conda config --add channels bioconda
   $ conda config --add channels conda-forge
   $ conda config --remove channels defaults
   $ conda config --set channel_priority strict

   # Restart your terminal

Now, we can install all dependencies into a separate environment:

.. substitution-code-block:: bash

   $ mamba env create -n pantools -f conda.yaml

Please make sure to activate the environment before using PanTools:

.. substitution-code-block:: bash

   $ conda activate pantools

Download PanTools
~~~~~~~~~~~~~~~~~

Download the latest release from
https://git.wur.nl/bioinformatics/pantools/-/releases. The file to download is
named pantools-|ProjectVersion|.jar.

Set PanTools alias
~~~~~~~~~~~~~~~~~~

To avoid typing long command line arguments every time, we suggest setting
an alias to your profile. Set an alias in your ~/.bashrc using the following
command. Always include the **full** path to PanTools .jar file.

Run the following to add the alias to your ~/.bashrc:

.. substitution-code-block:: bash

   $ echo "alias pantools='java -Xms20g -Xmx50g -jar /path/to/pantools-|ProjectVersion|.jar'" >> ~/.bashrc

Source your ~/.bashrc and test if the alias works:

.. code:: bash

   $ source ~/.bashrc
   pantools --help

Download MCScanX
~~~~~~~~~~~~~~~~

MCScanX is the tool PanTools uses for calculating synteny between genomes or
sequences. Since the tools is unavailable in conda, it needs to be downloaded
and manually installed from https://github.com/wyp1125/MCScanX. After
installation, you can add MCScanX to your path:

.. substitution-code-block:: bash

   $ echo "export PATH=/path/to/MCScanX:$PATH" >> ~/.bashrc
   $ source ~/.bashrc
