Technical setup validation
==========================

Performance is important when running PanTools on larger datasets. Therefore,
it is recommended to test your PanTools installation on one or more of the
test data sets below, before applying it to your own data.

PanTools pangenome construction commands make a large amount of
transactions to the database and should be built on a location that can
handle this for larger pangenomes. We recommend using an SSD or a RAM-disk
(/dev/shm/, available on Linux machines). If pangenome construction takes
too much time, the location of the database might be the issue. For reference,
the building time of a small yeast data set of 10 genomes on our RAM-disk takes
a little over two minutes, while it takes nearly 30 minutes on a network disk!
Larger datasets exacerbate this gap in runtime. Disk size needed for
construction depends on input data (genome size and number of genomes),
for 8 arabidopsis genomes ±20GB should suffice, for 3 lettuce genomes ±250GB.
Java heap space needed depends on the same parameters: in general it is
recommendable to set this to larger than expected because java running out
of heap space makes the tool crash (e.g. for lettuce we used
-Xmx200g -Xms200g).

.. warning::
 If you want to do everything in a single directory, make sure to download the
 data on the disk where you want to write the output. If the input data is on
 a network disk, adjust the commands to write the pangenome database on a
 local disk (RAM or SSD).

Ten yeast genomes
-----------------

Download the data from:
https://www.bioinformatics.nl/pangenomics/data/yeast_10strains.tgz

.. code:: bash

   $ wget https://www.bioinformatics.nl/pangenomics/data/yeast_10strains.tgz
   $ tar zxvf yeast_10strains.tgz

Build the pangenome:

.. code:: bash

   $ pantools -Xms20g -Xmx50g build_pangenome -t16 yeast_db yeast_10strains/metadata/genome_locations.txt

Add annotations:

.. code:: bash

   $ pantools -Xms20g -Xmx50g add_annotations yeast_db yeast_10strains/metadata/annotation_locations.txt

Group proteins:

.. code:: bash

   $ pantools -Xms20g -Xmx50g group yeast_db -t16 --relaxation=3

Run time statistics (16 threads on RAM-disk):

.. csv-table::
   :file: /tables/runtime_statistics_yeast.csv
   :header-rows: 1
   :delim: ,

Two *Arabidopsis* genomes
-------------------------

Start this use case with a bash script to download the data. Download the
script from here:
https://www.bioinformatics.nl/pangenomics/data/download_2_ara.sh
To download the *Arabidopsis* data, run the script:

.. code:: bash

   $ bash download_2_ara.sh <threads>

Build the pangenome:

.. code:: bash

   $ pantools -Xms40g -Xmx80g build_pangenome -t50 --kmer-size=17 ara_db a_thaliana_2genomes/metadata/genome_locations.txt

Add annotations:

.. code:: bash

   $ pantools -Xms40g -Xmx80g add_annotations ara_db a_thaliana_2genomes/metadata/annotation_locations.txt

Group proteins:

.. code:: bash

   $ pantools -Xms40g -Xmx80g group ara_db -t50 --relaxation=3

Run time statistics (50 threads on RAM-disk)

.. csv-table::
   :file: /tables/runtime_statistics_arabidopsis.csv
   :header-rows: 1
   :delim: ,

Three tomato genomes
--------------------

Download the script from here:
https://www.bioinformatics.nl/pangenomics/data/tomato_3genomes.tar.gz

Extract the tomato data:

.. code:: bash

   $ tar xvzf tomato_3genomes.tar.gz

Build the pangenome:

.. code:: bash

   $ pantools -Xms40g -Xmx80g build_pangenome -t50 --kmer-size=13 tomato_db tomato_3genomes/metadata/genome_locations.txt

Add annotations:

.. code:: bash

   $ pantools -Xms40g -Xmx80g add_annotations tomato_db tomato_3genomes/metadata/annotation_locations.txt

Group proteins:

.. code:: bash

   $ pantools -Xms40g -Xmx80g group tomato_db -t50 --relaxation=3

Run time statistics (50 threads on RAM-disk)

.. csv-table::
   :file: /tables/runtime_statistics_tomato.csv
   :header-rows: 1
   :delim: ,
