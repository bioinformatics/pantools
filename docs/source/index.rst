PanTools version |ProjectVersion|
=================================

PanTools is a toolkit for comparative analysis of large number of
genomes. It is developed in the Bioinformatics Group of Wageningen
University, the Netherlands. Please cite the relevant publication(s)
from the list of publications if you use PanTools in your research.

Licence
-------

PanTools has been licensed under `GNU GENERAL PUBLIC LICENSE version 3.
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_

Publications
------------

-  `Pantools v3: functional annotation, classification, and phylogenomics
   <https://doi.org/10.1093/bioinformatics/btac506>`_
-  `The Pectobacterium pangenome, with a focus on Pectobacterium
   brasiliense, shows a robust core and extensive exchange of genes
   from a shared gene pool
   <https://doi.org/10.1186/s12864-021-07583-5>`_
-  `Pan-genomic read mapping
   <https://www.biorxiv.org/content/10.1101/813634v1>`_
-  `Efficient inference of homologs in large eukaryotic pan-proteomes
   <https://bmcbioinformatics.biomedcentral.com/articles/10.1186/
   s12859-018-2362-4>`_
-  `PanTools: representation, storage and exploration of pan-genomic
   data.
   <https://academic.oup.com/bioinformatics/article/32/17/i487/2450785>`_

Functionalities
---------------

PanTools currently provides these functionalities:

-  Construction of a panproteome
-  Adding new genomes to the pangenome
-  Adding structural/functional annotations to the genomes
-  Detecting homology groups based on similarity of proteins
-  Optimization of homology grouping using BUSCO
-  Read mapping
-  Gene classification
-  Phylogenetic methods

Running the program
-------------------

Install PanTools as described in the :doc:`/getting_started/install` page.
You can run PanTools from the command line using:

.. substitution-code-block:: bash

   $ pantools <JVM options> <subcommand> <arguments>

Useful JVM options:

-  **-Xms<heap size>[unit]** : Initial heap size in MB ('m') or GB ('g')
-  **-Xmx<heap size>[unit]** : Maximum heap size in MB ('m') or GB ('g')
-  **-XX:StartFlightRecording=filename=<name>.jfr,disk=true** : Enable
   the Flight Recorder (for developers)

Options
-------

All options except for ``--version`` also apply to all subcommands.

.. list-table::
   :widths: 30 70

   * - ``--version``/``-V``
     - Display version info.
   * - ``--help``/``-h``
     - Display a help message for pantools or any subcommand.
   * - ``--manual``/``-M``
     - Open the manual for pantools or any subcommand in a local browser.
   * - ``--force``/``-f``
     - Force. For instance, force overwrite a database with ``build_pangenome``.
   * - ``--no-input``
     - Ignore prompts or any other interactive user input.
   * - ``--debug``/``-d``
     - Show debug messages in the console.
   * - ``--quiet``/``-q``
     - Only show warnings or higher level logging messages in the console.

Contents
--------

.. toctree::
   :caption: Getting Started
   :maxdepth: 1

   getting_started/diy_pangenomics
   Install <getting_started/install>
   getting_started/technical_setup
   getting_started/differences
   getting_started/workflows
   getting_started/selection
   getting_started/query

.. toctree::
   :caption: Tutorial PanTools
   :maxdepth: 1

   Tutorial 1 - Pangenome construction <tutorial/tutorial_part1>
   Tutorial 2 - Pangenome analysis <tutorial/tutorial_part2>
   Tutorial 3 - Phylogeny <tutorial/tutorial_part3>
   Tutorial 4 - Visualization (PanVA) <tutorial/tutorial_part4>
   Tutorial 5 - Neo4j browser <tutorial/tutorial_part5>

.. toctree::
   :caption: Pangenome Construction
   :maxdepth: 1

   construction/build
   construction/annotate
   construction/group
   construction/synteny

.. toctree::
   :caption: Pangenome Analysis
   :maxdepth: 1

   analysis/blast
   analysis/classification
   analysis/explore
   analysis/gene_retention
   analysis/go_enrichment
   analysis/metrics
   analysis/msa
   analysis/phylogeny
   analysis/mapping
   analysis/sequence_visualization
   analysis/support
   analysis/dn_ds

.. toctree::
   :caption: Developer guide
   :maxdepth: 1

   Installation <developer_guide/install>
   Release steps <developer_guide/release>
   Database structure <developer_guide/database>
