package nl.wur.bif.pantools.cli.validation;

import jakarta.validation.Validator;
import nl.wur.bif.pantools.utils.cli.validation.Constraints.InputDirectory;
import nl.wur.bif.pantools.utils.BeanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.FileUtils.addShutDownHook;
import static org.apache.commons.io.FileUtils.cleanDirectory;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for all constraints within the Constraints class.
 *
 * @author Robin van Esch, Bioinformatics group, Wageningen University, the Netherlands
 */
class ConstraintsTest {

    @BeforeAll
    static void disableLogging() {
        System.setProperty("log4j2.rootLevel", "off");
    }

    @Nested
    @DisplayName("Test @MinOrZero constraint")
    class MinOrZeroTests implements Bean {
        @MinOrZero(value = 2)
        private int value;

        @Test
        void aboveMinTest() {
            value = 3;
            assertTrue(this.isValid());
        }

        @Test
        void belowMinTest() {
            value = 1;
            assertFalse(this.isValid());
        }

        @Test
        void isZeroTest() {
            value = 0;
            assertTrue(this.isValid());
        }
    }

    @Nested
    @DisplayName("Test @Patterns constraint")
    class PatternsTests implements Bean {
        @Patterns(regexp = "foo|bar")
        private List<String> strings;

        @Patterns(regexp = "foo|bar")
        private HashMap<String, String> map;

        @Test
        void matchAllTest() {
            strings = Arrays.asList("foo", "bar");
            map = new HashMap<>();
            map.put("foo", "abc");
            map.put("bar", "def");
            assertTrue(this.isValid());
        }

        @Test
        void matchOneTest() {
            strings = Arrays.asList("foo", "baz");
            map = new HashMap<>();
            map.put("foo", "abc");
            map.put("baz", "def");
            assertFalse(this.isValid());
        }

        @Test
        void matchNoneTest() {
            strings = Arrays.asList("baz", "baz");
            map = new HashMap<>();
            map.put("baz", "abc");
            map.put("bax", "def");
            assertFalse(this.isValid());
        }
    }

    @Nested
    @DisplayName("Test @MatchInteger constraint")
    class MatchIntegerTests implements Bean {
        @MatchInteger(value = 1)
        private int integer;

        @Test
        void integerMatchTest() {
            integer = 1;
            assertTrue(this.isValid());
        }

        @Test
        void noIntegerMatchTest() {
            integer = 2;
            assertFalse(this.isValid());
        }
    }

    @Nested
    @DisplayName("Tests for files and directory constraints")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class FileConstraintTests {
        private Path tempDirectory;

        @BeforeAll
        void createTempDirectory() throws IOException {
            tempDirectory = Files.createTempDirectory(RandomStringUtils.randomAlphanumeric(10));
            addShutDownHook(tempDirectory);
        }

        @Nested
        @DisplayName("Test @InputDirectory constraint")
        class InputDirectoryTests implements Bean {
            @InputDirectory
            private Path inputDirectory;

            @Test
            void noDirectoryTest() {
                inputDirectory = tempDirectory.resolve("directory");
                assertFalse(this.isValid());
            }

            @Test
            void inputDirectoryTest() throws IOException {
                inputDirectory = Files.createDirectory(tempDirectory.resolve("directory"));
                assertTrue(this.isValid());
            }

            @Test
            void inputFileTest() throws IOException {
                inputDirectory = Files.createFile(tempDirectory.resolve("file"));
                assertFalse(this.isValid());
            }
        }

        @Nested
        @DisplayName("Test @InputFile constraint")
        class InputFileTests implements Bean {
            @InputFile
            private Path inputFile;

            @Test
            void noInputFileTest() {
                inputFile = tempDirectory.resolve("file");
                assertFalse(this.isValid());
            }

            @Test
            void inputFileTest() throws IOException {
                inputFile = Files.createFile(tempDirectory.resolve("file"));
                assertTrue(this.isValid());
            }

            @Test
            void inputDirectoryTest() throws IOException {
                inputFile = Files.createDirectory(tempDirectory.resolve("directory"));
                assertFalse(this.isValid());
            }
        }

        @Nested
        @DisplayName("Test @InputFiles constraint")
        class InputFilesTests implements Bean {
            @InputFiles
            private Path[] inputFiles;

            private Path file1;
            private Path file2;

            @Test
            void noInputFileTest() {
                file1 = tempDirectory.resolve("file1");
                file2 = tempDirectory.resolve("file1");
                inputFiles = new Path[] {file1, file2};
                assertFalse(this.isValid());
            }

            @Test
            void inputFileTest() throws IOException {
                file1 = Files.createFile(tempDirectory.resolve("file1"));
                file2 = Files.createFile(tempDirectory.resolve("file2"));
                inputFiles = new Path[] {file1, file2};
                assertTrue(this.isValid());
            }

            @Test
            void inputDirectoryTest() throws IOException {
                file1 = Files.createDirectory(tempDirectory.resolve("directory1"));
                file2 = Files.createDirectory(tempDirectory.resolve("directory2"));
                inputFiles = new Path[] {file1, file2};
                assertFalse(this.isValid());
            }
        }

        @AfterEach
        void cleanTempDirectory() throws IOException {
            cleanDirectory(tempDirectory.toFile());
        }
    }

    @Nested
    @DisplayName("Test @ExcludePatterns constraint")
    @ExcludePatterns(regexp = "foo|bar", field = "value")
    public class ExcludePatternsTests implements Bean {
        private String value;

        @Test
        void matchPattern() {
            value = "bar";
            assertFalse(this.isValid());
        }

        @Test
        void noMatchPattern() {
            value = "baz";
            assertTrue(this.isValid());
        }

        public String getValue() {return value;}
    }

    /**
     * Generic interface for Bean classes. Contains isValid() function to validate itself using bean validation.
     */
    private interface Bean {
        default boolean isValid() {
            final Validator validator = BeanUtils.createValidator();
            return validator.validate(this).isEmpty();
        }
    }


}