directory.busco     = BUSCO directory not found

exclude.core = Nodes of type 'nucleotide', 'sequence', 'pangenome', 'genome', and 'degenerate' should not be removed \
  from the graph database.
exclude.ann = Nodes of type 'feature', 'CDS', 'tRNA', 'gene', 'mRNA', 'coding_gene', 'tRNA', 'annotation', \
  'exon', and 'intron' should only be removed using the 'remove_annotations' function.
exclude.homology = Nodes of type 'homology_group' should only be removed using the 'remove_grouping' function.
exclude.functions = Nodes of type 'GO', 'pfam', 'tigrfam' and 'interpro' should only be removed using the \
  'remove_functions' function.
exclude.function-properties = Labels of type 'COG', 'phobius' and 'signalp' are part of mRNA nodes, strip these \
  properties using the 'remove_functions' function.

file.annotations    = Annotations file not found (${validatedValue})
file.selection-file = Genome/sequence selection file not found (${validatedValue})
file.antismash      = AntiSMASH file not found (${validatedValue})
file.busco          = BUSCO file not found (${validatedValue})
file.fasta          = FASTA file not found (${validatedValue})
file.functions      = Function file not found (${validatedValue})
file.genomes        = Genomes file not found (${validatedValue})
file.genome-numbers = Genome numbers file not found (${validatedValue})
file.homology       = Homology group file not found (${validatedValue})
file.matrix         = Matrix group file not found (${validatedValue})
file.pavs           = PAVs file not found (${validatedValue})
file.phenotype      = Phenotype file not found (${validatedValue})
file.proteomes      = Proteomes file not found (${validatedValue})
file.previous-run   = Raw abundance file not found (${validatedValue})
file.regions        = Regions file not found (${validatedValue})
file.short-read     = Short-read file(s) not found
file.vcfs           = VCFs file not found (${validatedValue})
file.tree           = Phylogenetic tree file not found (${validatedValue})

match.alignment-mode    = --alignment-mode should be 5, 10 or 20
match.blosum            = --blosum should be 45, 62 or 80
match.busco-version     = --busco-version should be 4 or 5


max.alignment-band          = --alignment-band must be less than or equal to {value}
max.contrast                = --contrast must be less than or equal to {value}
max.ct                      = --core-threshold must be less than or equal to {value}
max.fdr                     = --fdr must be less than or equal to {value}
max.gap-open                = --gap-open must be less than or equal to {value}
max.gap-ext                 = --gap-ext must be less than or equal to {value}
max.ksize                   = K must be less than or equal to {value}
min.num-buckets             = number of buckets for sorting must be at least {value}
min.transaction-size        = transaction size must be at least {value}
min.num-db-writer-threads   = number of database writer threads must be at least {value}
min.cache-size              = cache size must be at least {value}
max.loops                   = --loops must be less than or equal to {value}
max.max-alignment-length    = --max-alignment-length must be less than or equal to {value}
max.max-fragment-length     = --max-fragment-length must be less than or equal to {value}
max.max-num-locations       = --max-num-locations must be less than or equal to {value}
max.mcl-i                   = --mcl-inflation must be less than or equal to {value}
max.min-hit-length          = --min-hit-length must be less than or equal to {value}
max.min-identity            = --min-identity must be less than or equal to {value}
max.pt                      = --phenotype-threshold must be less than or equal to {value}
max.st                      = --similarity-threshold must be less than or equal to {value}
max.ut                      = --unique-threshold must be less than or equal to {value}

min.alignment-band          = --alignment-band must be greater than or equal to {value}
min.contrast                = --contrast must be greater than or equal to {value}
min.ct                      = --core-threshold must be greater than or equal to {value}
min.fdr                     = --false-disc-rate must be greater than or equal to {value}
min.gap-open                = --gap-open must be greater than or equal to {value}
min.gap-ext                 = --gap-ext must be greater than or equal to {value}
min.kmers                   = --num-kmer-samples must be greater than or equal to {value}
min.ksize                   = K must be greater than or equal to {value}
min.loops                   = --loops must be greater than 0
min.max-alignment-length    = --max-alignment-length must be greater than or equal to {value}
min.max-fragment-length     = --max-fragment-length must be greater than or equal to {value}
min.max-num-locations       = --max-num-locations must be greater than or equal to {value}
min.mcl-i                   = --mcl-inflation must be greater than or equal to {value}
min.min-hit-length          = --min-hit-length must be greater than or equal to {value}
min.min-identity            = --min-identity must be greater than or equal to {value}
min.nucleotides             = --nucleotides must be greater than or equal to {value}
min.pt                      = --phenotype-threshold greater than or equal to {value}
min.seed                    = --seed must be greater than or equal to {value}
min.st                      = --similarity-threshold greater than or equal to {value}
min.ut                      = --unique-threshold must be greater than or equal to {value}

output-database = Pangenome database already exists and contains files

pattern.best-hits           = --best-hits should be 'none', 'random', 'all' or 'threshold'
pattern.busco-set           = --odb10 should be an odb10 database name
pattern.coloring            = --coloring should be either 'distinct' or 'phased'
pattern.clustering-mode     = --clustering-mode should be either 'ML' or 'NJ'
pattern.feature-type        = --feature-type should be one of 'gene', 'coding_gene', 'mRNA', 'tRNA', 'rRNA', 'CDS', 'exon', 'intron', 'feature', 'broken_protein'
pattern.method-msa          = --method should be 'per-group', 'multiple-groups', 'regions' or 'functions'
pattern.mode-ani            = --mode should be either 'MASH' or 'FastANI'
pattern.mode-msa            = --mode should be 'nucleotide' or 'protein'
pattern.mode-structure      = --mode should be 'gene' or 'kmer'
pattern.mode-blast          = --mode should be 'BLASTN', 'BLASTP', 'BLASTX', TBLASTX or 'TBLASTN'
pattern.mode-rmf            = --mode should be 'nodes', 'properties', 'bgc', 'all', 'COG', 'phobius' or 'signalp'
pattern.out-format          = --format should be 'SAM', 'BAM' or 'none'
pattern.read-group          = --read-group string should contain TAG:VALUE entries separated with TAB (\\t) and enclosed in single or double quotes. An example is 'ID:foo\\tSM:bar'. The available tags are ID, BC, CN, DS, DT, FO, KS, LB, PG, PI, PL, PM, PU and SM. Must contain ID.
pattern.grouping-version    = --version must be a number, 'all' or 'all-inactive'

patterns.functions  = --functions should consist of GO, InterPro, PFAM or TIGRFAM identifiers
patterns.go         = --go should consist of GO identifiers separated by comma

positive.bins           = --bins must be greater than 0
positive.colour         = --colour must be greater than 0
positive.threads        = --threads must be greater than 0

range.clipping-stringency   = '--clipping-stringency' must be in range [0..3]
range.ir                    = --intersection-rate must be in range [0.001,0.1]

scoring_matrix  = Invalid scoring matrix, choose between BLOSUM45, BLOSUM50, BLOSUM62, BLOSUM80, BLOSUM90, \
  PAM30, PAM70 and PAM250.

size.empty.exclude         = --exclude should consist of comma separated integers or ranges for genomes \
  (example: --exclude 1,2,5-10)
size.empty.include         = --include should consist of comma separated integers or ranges for genomes \
  (example: --include 1,2,5-10)
size.empty.node            = --node should consist of comma separated integers (example: --node 1,2,3)
size.empty.homology-groups = --homology-groups should consist of comma separated integers \
  (example: --homology-groups 1,2,3)
size.empty.relaxation      = --relaxation should consist of comma separated integers (example: --relaxation 1,2,3)
size.go                    = --go requires exactly two GO identifiers
size.node                  = --node requires exactly two node identifiers

values.msa  = Invalid option for --method 'regions': --mode 'protein'
