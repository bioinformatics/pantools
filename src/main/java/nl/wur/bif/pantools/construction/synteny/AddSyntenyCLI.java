package nl.wur.bif.pantools.construction.synteny;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools add_synteny
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "add_synteny", sortOptions = false, abbreviateSynopsis = true)
public class AddSyntenyCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "collinearity", index = "0+")
    @InputFile(message = "{file.collinearity}")
    private Path collinearityFile;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();
        new Synteny().addSynteny(collinearityFile);
        return 0;
    }
}
