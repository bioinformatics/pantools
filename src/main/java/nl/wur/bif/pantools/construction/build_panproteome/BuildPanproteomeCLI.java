package nl.wur.bif.pantools.construction.build_panproteome;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Build a panproteome out of a set of proteins.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "build_panproteome", sortOptions = false)
public class BuildPanproteomeCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    private Pantools pantools;

    @Parameters(descriptionKey = "proteomes-file", index = "0+")
    @InputFile(message = "{file.proteomes}")
    Path proteomesFile;

    @Override
    public Integer call() throws IOException {
        pantools.createDatabaseDirectory();
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        setGlobalParameters(); //TODO: use local parameters instead

        new BuildPanproteome(pantools.getDatabaseDirectory());
        return 0;
    }

    private void setGlobalParameters() {
        PROTEOME = true;
        PATH_TO_THE_PROTEOMES_FILE = proteomesFile.toString();
    }

    public Pantools getPantools() {return pantools;}

}
