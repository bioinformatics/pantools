package nl.wur.bif.pantools.construction.synteny;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.cli.validation.Constraints;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Command line interface class for pantools synteny_overview
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "synteny_overview", aliases = "synteny_statistics", sortOptions = false, abbreviateSynopsis = true)
public class SyntenyStatisticsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "collinearity", index = "0+")
    @InputFile(message = "{file.collinearity}")
    private Path collinearityFile;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);
        pantools.setPangenomeGraph();
        INPUT_FILE = collinearityFile.toString();
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        new Synteny().analyze_blocks();
        return 0;
    }



}
