package nl.wur.bif.pantools.construction.export_pangenome.records;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Record class for implementing a sequence anchor. Sequence anchors are stored
 * in sequence nodes, as three arrays: base pair positions, nucleotide node IDs,
 * and nucleotide node sides. This class packs each element at position i of
 * these three arrays into one.
 */
public class SequenceAnchor implements Record {
    private final String identifier;
    private final int index, position;
    private final String nodeSequence;
    private final String side;

    public SequenceAnchor(String identifier, int index, int position, String nodeSequence, String side) {
        this.identifier = identifier;
        this.index = index;
        this.position = position;
        this.nodeSequence = nodeSequence;
        this.side = side;
    }

    @Override
    public List<String> asList() {
        return ImmutableList.of(
            identifier,
            Integer.toString(index),
            Integer.toString(position),
            nodeSequence,
            side
        );
    }
}
