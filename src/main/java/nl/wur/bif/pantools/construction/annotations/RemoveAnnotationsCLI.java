package nl.wur.bif.pantools.construction.annotations;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Remove all the genomic features that belong to annotations.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_annotations", sortOptions = false)
public class RemoveAnnotationsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup(multiplicity = "1")
    ExclusiveOptions exclusiveOptions;
    private static class ExclusiveOptions {
        @Option(names = {"-A", "--annotations"})
        @InputFile(message = "{file.annotations}")
        Path annotationsFile;

        @ArgGroup SelectGenomes selectGenomes;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, exclusiveOptions, exclusiveOptions.selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        annLayer.remove_annotations();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(exclusiveOptions.selectGenomes);
        if (exclusiveOptions.annotationsFile != null)
            PATH_TO_THE_ANNOTATIONS_FILE = exclusiveOptions.annotationsFile.toString();
    }

}
