package nl.wur.bif.pantools.construction.build_pangenome;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.PATH_TO_THE_REGIONS_FILE;
import static nl.wur.bif.pantools.utils.Globals.seqLayer;
import static picocli.CommandLine.*;

/**
 * Retrieve the sequence of genomic regions from the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "retrieve_regions", sortOptions = false)
public class RetrieveRegionsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "regions-file", index = "0+")
    @InputFile(message = "{file.regions}")
    Path regionsFile;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        seqLayer.retrieve_regions();
        return 0;
    }

    private void setGlobalParameters() {
        PATH_TO_THE_REGIONS_FILE = regionsFile.toString();
    }

}
