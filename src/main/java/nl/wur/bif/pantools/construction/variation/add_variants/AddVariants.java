package nl.wur.bif.pantools.construction.variation.add_variants;

import nl.wur.bif.pantools.construction.index.IndexScanner;
import nl.wur.bif.pantools.construction.variation.add_variants.parallel.CreateVariantFasta;
import nl.wur.bif.pantools.construction.variation.add_variants.parallel.VariantFastaHeaderParser;
import nl.wur.bif.pantools.utils.ExecCommand;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.FileUtils;
import nl.wur.bif.pantools.construction.variation.AddVariation;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.io.DNASequenceCreator;
import org.biojava.nbio.core.sequence.io.FastaReader;
import org.neo4j.graphdb.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.check_if_program_exists_stderr;
import static nl.wur.bif.pantools.utils.Utils.delete_file_full_path;

/**
 * This class is used to add VCF variation to the pangenome graph.
 * Here, we handle accession nodes and variant nodes.
 * Accession nodes contain metadata about the added variation and are connected to the genome node in case of a
 * pangenome.
 * Variant nodes contain the actual variation and are connected to the mRNA nodes.
 * In case of a pangenome, the variant nodes also contain their consensus (nucleotide) sequence.
 *
 * @author Dirk-Jan van Workum
 */
public class AddVariants extends AddVariation {

    private final Path vcfLocationsFile;
    private final Path scratchDirectory;
    private final int threads;
    private final boolean keepTemporarilyFiles;
    private final Map<Path, Set<String>> variantSamples;

    public AddVariants(Path vcfLocationsFile, int threads, Path scratchDirectory,
                       boolean keepTemporarilyFiles) throws IOException {
        this.variantSamples = new HashMap<>();
        this.vcfLocationsFile = vcfLocationsFile;
        this.scratchDirectory = scratchDirectory;
        this.threads = threads;
        this.keepTemporarilyFiles = keepTemporarilyFiles;

        // check if bcftools and tabix are installed
        check_if_program_exists_stderr("bcftools", 1000, "bcftools", true);
        check_if_program_exists_stderr("tabix", 1000, "tabix", true);

        // add variants main code
        addVariants();
    }

    /**
     * Main method of `pantools add_variants`.
     * Reads vcf files from input VCF file list and adds them to a pangenome database.
     * Based on the work of Edwin van der Werf
     */
    private void addVariants() throws IOException {
        Pantools.logger.info("Adding variants from VCF file to the pangenome.");

        // read the vcf file list and add the variants to the pangenome
        final Map<Path, Integer> vcfFileMap = readVariationLocationsFile(vcfLocationsFile, "VCF");

        // validate the provided vcf files
        validateVariationFiles(vcfFileMap, "VCF");

        // add variant information to the pangenome
        addVariantsToPangenome(vcfFileMap, threads, keepTemporarilyFiles, scratchDirectory);

        Pantools.logger.info("Finished adding variants to the pangenome.");
    }

    /**
     * Read the provided vcf files and add their information to the pangenome.
     *
     * @param vcfFileMap map of variation file paths and their reference genome
     * @param threads number of threads to use
     * @param keepTemporarilyFiles whether to keep the temporary files created during the process
     * @param scratchDirectory path to the scratch directory
     * @throws IOException if a file could not be read
     */
    private void addVariantsToPangenome(Map<Path, Integer> vcfFileMap, int threads, boolean keepTemporarilyFiles,
                                        Path scratchDirectory) throws IOException {

        for (Map.Entry<Path, Integer> entry : vcfFileMap.entrySet()) {
            final Path vcfFile = entry.getKey();
            final int genomeNr = entry.getValue();
            final Node genomeNode;

            // find and check the genome node for annotations
            try (Transaction tx = GRAPH_DB.beginTx()) {
                genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
                Pantools.logger.info("Adding variants from {} to genome {}", vcfFile, genomeNr);

                // skip if the genome is not annotated
                if (!genomeNode.hasRelationship(RelTypes.annotates, Direction.INCOMING)) {
                    Pantools.logger.warn("Genome {} is not annotated; Skipping.", genomeNr);
                    tx.close();
                    continue;
                }
            } catch (NotFoundException nfe) {
                Pantools.logger.error("Unable to start up the graph database.");
                throw new NotFoundException(nfe.getMessage());
            }

            // index vcf file with tabix if not present
            final Path vcfIndexFile = Paths.get(vcfFile + ".tbi");
            if (!vcfIndexFile.toFile().exists()) {
                indexVcfFile(vcfFile);
            }

            // get all sequence titles
            final List<Map<Integer, String>> sequenceTitles = getSequenceTitleMap();

            // add the accession to the pangenome by creating an accession node and variant nodes for each mRNA node
            final Set<String> samples = getSamples(vcfFile);
            Pantools.logger.info("Adding {} samples to genome {}", samples.size(), genomeNr);

            try (Transaction tx = GRAPH_DB.beginTx()) {
                final Set<Node> accessionNodes = createAccessionNodes(genomeNr, samples, "VCF");
                connectAccessionNodes(genomeNode, accessionNodes);
                addVariantNodes(genomeNr, samples, vcfFile, sequenceTitles, scratchDirectory, keepTemporarilyFiles,
                        threads);
                tx.success();
            }
        }
    }

    /**
     * Index vcf file with tabix
     *
     * @param vcfFile vcf file
     */
    private void indexVcfFile(Path vcfFile) {
        Pantools.logger.info("Indexing {} with tabix.", vcfFile);
        final String tabixCommand = String.format("tabix %s", vcfFile);
        Pantools.logger.debug("Executing {}", tabixCommand);
        try {
            final Process p = Runtime.getRuntime().exec(tabixCommand);
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all sequence titles for all genomes in the database.
     *
     * @return a list of maps with key: sequence number, value: sequence title
     */
    private List<Map<Integer, String>> getSequenceTitleMap() {
        final List<Map<Integer, String>> sequenceTitles = new ArrayList<>();
        sequenceTitles.add(null); // add null to index 0
        for (int i = 1; i <= GENOME_DB.num_genomes; i++) {
            Pantools.logger.debug("Getting sequence titles for genome {}", i);
            sequenceTitles.add(i, getSequenceTitles(i));
        }
        return sequenceTitles;
    }

    /**
     * Get the sequence titles for a given genome based on its genome number.
     *
     * @param genomeNr the genome number to get the sequence numbers for
     * @return a map with key: sequence number, value: sequence title
     */
    private Map<Integer, String> getSequenceTitles(int genomeNr) {
        final Map<Integer, String> sequenceNumbers = new HashMap<>();
        final int numSequences = GENOME_DB.num_sequences[genomeNr];
        for (int i = 1; i <= numSequences; i++) {
            try {
                final String sequence = GENOME_DB.sequence_titles[genomeNr][i];
                final String[] fields = sequence.split("\\s+");
                final String id = fields[0];
                sequenceNumbers.put(i, id);
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                Pantools.logger.error("Could not find sequence for genome {} and sequence number {}", genomeNr, i);
                throw new RuntimeException("Invalid sequences in genome DB");
            }
        }
        return sequenceNumbers;
    }

    /**
     * Get the sequence title of the given sequence.
     *
     * @param genomeNumber the genome number of the sequence
     * @param sequenceNumber the sequence number of the sequence
     * @param sequenceTitles the sequence titles of all genomes
     * @return the sequence title of the given sequence
     * @throws NotFoundException if the database gives an error
     */
    private String getSequenceTitle(int genomeNumber, int sequenceNumber, List<Map<Integer, String>> sequenceTitles)
            throws NotFoundException {
        Pantools.logger.trace("Getting sequence title for genome {}, sequence {}", genomeNumber, sequenceNumber);

        String sequenceTitle;
        try {
            sequenceTitle = sequenceTitles.get(genomeNumber).get(sequenceNumber);
        } catch (NullPointerException e) {
            Pantools.logger.error("Could not find sequence title for genome {} and sequence number {}",
                    genomeNumber, sequenceNumber);
            throw new NotFoundException("Could not find sequence title for genome " + genomeNumber +
                    " and sequence number " + sequenceNumber);
        }

        Pantools.logger.trace("Sequence title is {}", sequenceTitle);
        return sequenceTitle;
    }

    /**
     * Get the strand of the mRNA node.
     *
     * @param mrnaNode the mRNA node
     * @return the strand of the mRNA node (true for positive, false for negative)
     */
    private boolean getStrand(Node mrnaNode) {
        Pantools.logger.trace("Getting strand for mrna {}", mrnaNode.getProperty("id"));

        String strandStr = (String) mrnaNode.getProperty("strand");
        if (strandStr.equals("+")) {
            Pantools.logger.trace("Strand is positive.");
            return true;
        } else if (strandStr.equals("-")) {
            Pantools.logger.trace("Strand is negative.");
            return false;
        } else {
            Pantools.logger.error("Invalid strand ({}) for mRNA node {}", strandStr, mrnaNode);
            System.exit(1);
            return true; // to make the compiler happy
        }
    }

    /**
     * Adds variants of all samples to the pangenome by creating variant nodes for each mRNA
     * node with the help of bcftools consensus.
     *
     * @param genomeNumber number of the genome
     * @param samples the samples in the vcf file
     * @param vcfFile a (multi-sample) vcf file belonging to given genomeNode
     * @param sequenceTitles the sequence titles of all genomes
     * @param scratchDirectory the path to the scratch directory
     * @param keepTemporaryFiles whether to keep the temporary files created during the process
     * @param threads the number of threads to use
     * @throws NotFoundException if the database gives an error
     * @throws IOException if a file could not be read
     */
    private void addVariantNodes(int genomeNumber, Set<String> samples, Path vcfFile,
                                 List<Map<Integer, String>> sequenceTitles, Path scratchDirectory,
                                 boolean keepTemporaryFiles, int threads)
            throws NotFoundException, IOException {

        // create a directory for storing all temporary files
        scratchDirectory = FileUtils.createScratchDirectory(scratchDirectory);
        Pantools.logger.debug("Created temporary directory {}", scratchDirectory);

        final Path genomeMrnaFastaFile = scratchDirectory.resolve(String.format("genome_%s.mrna.fa", genomeNumber));

        // create a fasta file with all mRNA sequences of the reference genome
        createGenomeMrnaFastaFile(genomeNumber, sequenceTitles, genomeMrnaFastaFile);

        // create variant fasta files
        final Map<String, Path> variantFastaFiles = createVariantFastaFiles(genomeNumber, samples, threads,
                keepTemporaryFiles, genomeMrnaFastaFile, vcfFile, scratchDirectory);

        // create variant nodes for all samples
        createVariantNodes(samples, variantFastaFiles, genomeNumber, keepTemporaryFiles);

        // remove the temporary directory
        if (!keepTemporaryFiles) {
            FileUtils.deleteDirectoryRecursively(scratchDirectory);
            Pantools.logger.debug("Removing temporary directory {}", scratchDirectory);
        }
    }

    /**
     * Create temporary variant fasta files for each vcf sample of a genome.
     *
     * @param genomeNumber number of the genome
     * @param samples the samples in the vcf file
     * @param threads the number of threads to use
     * @param keepTemporaryFiles whether to keep the temporary files created during the process
     * @param genomeMrnaFastaFile fasta file with all mRNA sequences of a genome that are not variant nodes
     * @param vcfFile a (multi-sample) vcf file belonging to given genomeNode
     * @param scratchDirectory the path to the scratch directory
     * @return a map of variant fasta files for each sample
     */
    private Map<String, Path> createVariantFastaFiles(int genomeNumber, Set<String> samples, int threads,
                                                      boolean keepTemporaryFiles, Path genomeMrnaFastaFile,
                                                      Path vcfFile, Path scratchDirectory) {

        final Map<String, Path> variantFastaFiles = new HashMap<>();

        // loop over all samples to create variant fasta files
        Pantools.logger.debug("Using {} threads to create variant fasta files for {} samples ({}).",
                threads, samples.size(), String.join(", ", samples));
        final ExecutorService es = Executors.newFixedThreadPool(threads);
        final List<Future<String>> futures = new ArrayList<>();
        for (String sample : samples) {
            // replace sample with a unique string to prevent problems with special characters in the sample name
            final String uniqueSample = UUID.randomUUID().toString();
            final String fileName = String.format("genome_%s_%s.mrna.fa", genomeNumber, uniqueSample);
            final Path variantMrnaFastaFile = scratchDirectory.resolve(fileName);
            variantFastaFiles.put(sample, variantMrnaFastaFile);

            futures.add(es.submit(new CreateVariantFasta(
                    genomeMrnaFastaFile,
                    vcfFile,
                    sample,
                    variantMrnaFastaFile,
                    keepTemporaryFiles)));

            Pantools.logger.debug("Sample {} will be replaced with {} in the temporary variant fasta filename.",
                    sample, uniqueSample);
        }

        // wait for all threads to finish
        int counter = 1;
        final int total = futures.size();
        Pantools.logger.info("Creating variant fasta files for all samples of genome {}", genomeNumber);
        for (Future<String> future : futures) {
            try {
                final String result = future.get();
                Pantools.logger.info("\t{}/{}: {}", counter, total, result); // TODO: add progress bar
                counter++;
            } catch (InterruptedException | ExecutionException e) {
                Pantools.logger.error("Encountered an error while creating variant fasta files for future {}", future);
                e.printStackTrace();
            }
        }
        es.shutdown();

        // remove the fasta files with all mRNA sequences
        if (!keepTemporaryFiles) {
            Pantools.logger.debug("Removing fasta file with all mRNA sequences of genome {} at {}",
                    genomeNumber, genomeMrnaFastaFile);
            delete_file_full_path(genomeMrnaFastaFile);
        }

        return variantFastaFiles;
    }

    /**
     * Create a fasta file with all mRNA sequences of a genome that are not variant nodes.
     *
     * @param genomeNumber number of the genome
     * @param sequenceTitles the sequence titles of all genomes
     * @throws NotFoundException if the database gives an error
     */
    private void createGenomeMrnaFastaFile(int genomeNumber, List<Map<Integer, String>> sequenceTitles,
                                           Path genomeMrnaFastaFile) throws NotFoundException, IOException {

        Pantools.logger.debug("Creating fasta file with all mRNA sequences of genome {} at {}",
                genomeNumber, genomeMrnaFastaFile);

        // get all mRNAs of the genome
        final HashMap<String, String> mrnaSequencesMap = new HashMap<>();
        try (ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(
                MRNA_LABEL, "genome", genomeNumber)) {

            // get the mRNA sequences of the mRNAs and put them in a map
            while (mrnaNodes.hasNext()) {
                final Node mrnaNode = mrnaNodes.next();
                final String mrnaId = (String) mrnaNode.getProperty("id");
                Pantools.logger.trace("Getting mRNA sequence for mrna {}", mrnaId);

                int[] address = (int[]) mrnaNode.getProperty("address");
                int number = address[0];
                int sequenceNumber = address[1];
                int mrnaStart = address[2];
                int mrnaEnd = address[3];

                // create fasta header for the mRNA node
                final String mrnaHeader = String.format("%s:%s-%s %s",
                        getSequenceTitle(number, sequenceNumber, sequenceTitles),
                        mrnaStart,
                        mrnaEnd,
                        mrnaId);

                // get the mRNA sequence of the mRNA node
                final String mrnaSequence = getSequence(mrnaNode);

                // add the mRNA sequence to the map
                mrnaSequencesMap.put(mrnaHeader, mrnaSequence);
            }
        }

        // write the mRNA sequences of the genome to the fasta file
        try {
            final BufferedWriter genomeFastaWriter = new BufferedWriter(new FileWriter(genomeMrnaFastaFile.toFile()));
            for (Map.Entry<String, String> entry : mrnaSequencesMap.entrySet()) {
                final String mrnaHeader = entry.getKey();
                final String mrnaSequence = entry.getValue();
                Pantools.logger.trace("Writing mRNA sequence for mRNA {} to fasta file.", mrnaHeader);
                genomeFastaWriter.write(String.format(">%s\n", mrnaHeader));
                genomeFastaWriter.write(String.format("%s\n", mrnaSequence.replace("\n", "")));
            }
            genomeFastaWriter.close();
        } catch (IOException e) {
            Pantools.logger.error("Error while writing to fasta file {}", genomeMrnaFastaFile);
            throw new IOException("Unable to write to file in scratch directory");
        }
    }

    /**
     * Get the sequence of a feature node on the positive strand.
     *
     * @param featureNode the feature node
     * @return the sequence of the feature node
     */
    private String getSequence(Node featureNode) {
        Pantools.logger.trace("Getting sequence for feature node {}", featureNode.getProperty("id"));

        if (!featureNode.hasLabel(FEATURE_LABEL)) {
            Pantools.logger.error("mRNA node {} is not a feature node.", featureNode.getProperty("id"));
            throw new RuntimeException("Invalid mRNA nodes in database");
        }

        final int[] address = (int[]) featureNode.getProperty("address");
        final int length = (int) featureNode.getProperty("length");

        final int genome = address[0];
        final int sequenceNr = address[1];
        final int position = address[2] - 1;

        Pantools.logger.trace("Sequence is on genome {} , sequence {}, position {}, length {}",
                genome, sequenceNr, position, length);

        final StringBuilder sequenceSB = new StringBuilder();
        GENOME_SC.get_sub_sequence(sequenceSB, genome, sequenceNr, position, length, true);

        return sequenceSB.toString();
    }

    /**
     * Read sequences from provided variant fasta files and add them to variant nodes in the graph database.
     *
     * @param samples the samples in the vcf file
     * @param variantFastaFiles paths to variant fasta files by vcf sample
     * @param genomeNumber number of the genome
     * @param keepTemporaryFiles whether to keep the temporary files created during the process
     * @throws IOException if the variant fasta file cant be read.
     */
    private void createVariantNodes(Set<String> samples, Map<String, Path> variantFastaFiles, int genomeNumber,
                                    boolean keepTemporaryFiles) throws IOException {

        for (String sample : samples) {
            Pantools.logger.info("Creating variant nodes for sample {}", sample);

            // parse variant mRNA fasta file
            final Path variantMrnaFastaFile = variantFastaFiles.get(sample);
            final FastaReader<DNASequence, NucleotideCompound> mrnaFastaReader =
                    new FastaReader<>(variantMrnaFastaFile.toFile(),
                            new VariantFastaHeaderParser<>(),
                            new DNASequenceCreator(AmbiguityDNACompoundSet.getDNACompoundSet()));
            final Map<String, DNASequence> variantMrnaSequences = mrnaFastaReader.process();

            // create variant nodes for each mRNA node
            try (ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genomeNumber)) {
                while (mrnaNodes.hasNext()) {
                    final Node mrnaNode = mrnaNodes.next();

                    // get mRNA node properties (id + strand)
                    final String mrnaId = (String) mrnaNode.getProperty("id");
                    Pantools.logger.trace("\tmrnaID : {}", mrnaId);
                    boolean strand = getStrand(mrnaNode);
                    Pantools.logger.trace("\tstrand : {}", strand);

                    // get the variant sequence for the given mRNA node from the fasta files
                    final DNASequence variantMrnaSequence = variantMrnaSequences.get(mrnaId);
                    if (variantMrnaSequence == null) {
                        Pantools.logger.debug("No variant sequence found for mRNA {} of sample {}", mrnaId, sample);
                        continue;
                    }

                    Node variantNode = null;

                    // check if variant node already exists
                    for (Relationship rel : mrnaNode.getRelationships(RelTypes.has_variant, Direction.OUTGOING)) {
                        final Node node = rel.getEndNode();
                        final String variantId = (String) node.getProperty("sample");
                        if (variantId.equals(sample)) {
                            Pantools.logger.debug("Variant node already exists for sample {} and mRNA node {}",
                                    sample, mrnaId);
                            variantNode = node;
                            break; // only one variant node with the sample can exist per mRNA
                        }
                    }

                    // if variant node does not exist and the mRNA node has the "protein_ID" property, create it
                    if (variantNode == null && mrnaNode.hasProperty("protein_ID")) {
                        variantNode = createVariantNode(mrnaNode, sample);
                    }

                    // if the variant still does not exist, skip it
                    if (variantNode == null) {
                        continue;
                    }

                    // add sequence to variant node
                    if (strand) {
                        variantNode.setProperty("mRNA_sequence", variantMrnaSequence
                                .getSequenceAsString());
                    } else {
                        variantNode.setProperty("mRNA_sequence", variantMrnaSequence
                                .getReverseComplement()
                                .getSequenceAsString());
                    }
                }
            } catch (NotFoundException nfe) {
                Pantools.logger.error("Could not find mRNA nodes for genome {}", genomeNumber);
                throw new NotFoundException(nfe.getMessage());
            }

            // remove the fasta files with all mRNA sequences
            if (!keepTemporaryFiles) {
                Pantools.logger.debug("Removing fasta file with all mRNA sequences of genome {} at {}",
                        genomeNumber, variantMrnaFastaFile);
                delete_file_full_path(variantMrnaFastaFile);
            }
        }
    }
    
    /**
     * Adds information from a vcf file to an accession node
     *
     * @param accessionNode accession node
     */
    @Override
    protected void addAccessionNodeInformation(Node accessionNode) {
        accessionNode.setProperty("VCF", true);
        if (!accessionNode.hasProperty("PAV")) {
            accessionNode.setProperty("PAV", false);
        }
    }
    
    /**
     * Uses bcftools to get a list of samples from a vcf file
     *
     * @param vcfFile path to vcf file
     * @return set of vcf samples
     */
    @Override
    protected Set<String> getSamples(Path vcfFile) {
        if (this.variantSamples.containsKey(vcfFile)) {
            return this.variantSamples.get(vcfFile);
        }
        final String[] bcftoolsRetrieveSamples = {"bcftools", "query", "-l", vcfFile.toString()};
        Pantools.logger.debug("Running {} to get sample names from vcf file.",
                Arrays.toString(bcftoolsRetrieveSamples));
        final String[] result = ExecCommand.ExecCommand(bcftoolsRetrieveSamples).split("\n");
        final Set<String> samples = new HashSet<>(Arrays.asList(result));
        this.variantSamples.put(vcfFile, samples);
        return samples;
    }
}
