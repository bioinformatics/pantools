package nl.wur.bif.pantools.construction.grouping;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Find the most suitable settings for <Group>.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "optimal_grouping", sortOptions = false)
public class OptimalGroupingCLI implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "busco-dir", index = "0+")
    void setBuscoDirectory(String value) {
        if (Files.isDirectory(Paths.get(value))) buscoDirectory = Paths.get(value);
        else buscoDirectory = Paths.get(pantools.getDatabaseDirectory() + "/busco/" + value);
    }
    @InputDirectory(message = "{directory.busco}")
    Path buscoDirectory;

    @Option(names = {"-A", "--annotations-file"}, paramLabel = "<annotationsFile>")
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = "--fast")
    boolean fast;

    @Option(names = "--longest")
    boolean longestTranscript;

    @Option(names = "--phasing")
    boolean phasing;

    @Option(names = "--relaxation")
    void setRelaxation(String value) {
        relaxation = stringToIntegerList(value);
    }
    @Size(min = 1, message = "{size.empty.relaxation}")
    List<Integer> relaxation;

    @Option(names = "--scoring-matrix")
    @ScoringMatrix(message = "{scoring_matrix}")
    String scoringMatrix;

    @Override
    public Integer call() throws IOException, InterruptedException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        proLayer.optimal_grouping(scoringMatrix);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        INPUT_FILE = buscoDirectory.toString();
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = annotationsFile.toString();
        THREADS = threadNumber.getnThreads();
        if (relaxation != null) {
            NODE_VALUE = relaxation.toString().replaceAll("[\\[\\]\\s]", "");
            System.out.println(NODE_VALUE);
        }
        FAST = fast;
        longest_transcripts = longestTranscript;
        if (phasing) {
            PHASED_ANALYSIS = true;
        }
    }
}
