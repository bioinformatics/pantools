package nl.wur.bif.pantools.construction.repeats;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools add_repeats
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "add_repeats", sortOptions = false, abbreviateSynopsis = true)
public class AddRepeatsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "annotations-file", index = "0+")
    @InputFile(message = "{file.annotations}")
    private Path annotationsFile;

    @Option(names = {"--strict"})
    private boolean strict;

    @Option(names = "--connect")
    private boolean connectAnnotations;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();
        new Repeats().addRepeats(annotationsFile, strict, connectAnnotations);
        return 0;
    }
}
