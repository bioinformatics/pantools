package nl.wur.bif.pantools.construction.variation.variation_overview;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.nio.file.Path;
import java.util.*;

import static nl.wur.bif.pantools.utils.Globals.GRAPH_DB;
import static nl.wur.bif.pantools.utils.Globals.total_genomes;
import static nl.wur.bif.pantools.utils.Globals.ACCESSION_LABEL;
import static nl.wur.bif.pantools.utils.Globals.VARIANT_LABEL;
import static nl.wur.bif.pantools.utils.Utils.write_SB_to_file_full_path;

/**
 * This class is creates an overview of PAV anc VCF information created by add_variants and add_pav.
 * The information is obtained from accession and variant nodes.
 *
 * @author Dirk-Jan van Workum
 */
public class VariationOverview {

    private final Path databaseDirectory;

    public VariationOverview(Path databaseDirectory) {
        this.databaseDirectory = databaseDirectory;
        accessionOverview();
    }

    /**
     * Writes overview of all properties of the accession nodes to WORKING_DIRECTORY + variation/variation_overview.txt
     *
     * @throws NotFoundException if the database gave an error
     */
    private void accessionOverview() throws NotFoundException {
        final StringBuilder outputBuilder = new StringBuilder();
        final Path accessionOverviewPath = databaseDirectory.resolve("variation").resolve("variation_overview.txt");
        Pantools.logger.info("Writing overview of accession nodes to {}", accessionOverviewPath);

        // loop over all genomes
        for (int i = 1; i <= total_genomes; i++) {
            outputBuilder.append("#Genome ").append(i).append("\n\n");

            // start a database transaction
            try (Transaction tx = GRAPH_DB.beginTx()) {

                // create a map of all accession nodes for this genome in alphabetical order
                final Map<String, Node> allAccessions = new TreeMap<>();
                try (ResourceIterator<Node> allAccessionsIterator = GRAPH_DB.findNodes(ACCESSION_LABEL, "genome", i)) {
                    while (allAccessionsIterator.hasNext()) {
                        final Node accessionNode = allAccessionsIterator.next();
                        allAccessions.put((String) accessionNode.getProperty("sample"), accessionNode);
                    }
                } catch (NotFoundException nfe) {
                    Pantools.logger.error("Error while retrieving accession nodes for genome {}", i);
                    throw nfe;
                }

                // loop over all accessions in alphabetical order
                for (String accession : allAccessions.keySet()) {
                    final Node accessionNode = allAccessions.get(accession);

                    // loop over all accession properties in sorted order and append them to the output
                    final Set<String> sortedProperties = new TreeSet<>();
                    for (String property : accessionNode.getPropertyKeys()) {
                        sortedProperties.add(property);
                    }
                    for (String property : sortedProperties) {
                        outputBuilder.append(property).append(": ")
                                .append(accessionNode.getProperty(property)).append("\n");
                    }

                    // count the number of variant nodes for this accession for PAV and VCF information separately
                    int variantCount = 0;
                    int presentCount = 0;
                    int absentCount = 0;
                    int vcfCount = 0;
                    final String variantId = String.format("%s|%s",
                            accessionNode.getProperty("genome"),
                            accessionNode.getProperty("sample"));
                    try (ResourceIterator<Node> variantNodes = GRAPH_DB.findNodes(VARIANT_LABEL, "id", variantId)) {
                        while (variantNodes.hasNext()) {
                            final Node variantNode = variantNodes.next();
                            variantCount++;

                            // check if the variant is present in the sample
                            if (variantNode.hasProperty("present")) {
                                if ((boolean) variantNode.getProperty("present")) {
                                    presentCount++;
                                } else {
                                    absentCount++;
                                }
                            }

                            // check if the variant contains VCF information
                            if (variantNode.hasProperty("mRNA_sequence")) {
                                vcfCount++;
                            }
                        }
                    } catch (NotFoundException nfe) {
                        Pantools.logger.error("Error while counting variant nodes for accession {}",
                                accessionNode.getProperty("sample"));
                        throw nfe;
                    }

                    // append the number of variant nodes and PAV and VCF count to the output
                    outputBuilder.append("Number of variant nodes: ").append(variantCount).append("\n");
                    outputBuilder.append("\tNumber of absent variants: ").append(absentCount).append("\n");
                    outputBuilder.append("\tNumber of present variants: ").append(presentCount).append("\n");
                    outputBuilder.append("\tNumber of variants with variation sequence: ")
                            .append(vcfCount).append("\n");
                    outputBuilder.append("\n");
                }

                // commit the database transaction
                tx.success();
            } catch (NotFoundException nfe) {
                Pantools.logger.error("Error while getting accessions for genome {}", i);
                throw nfe;
            }

            outputBuilder.append("\n");
        }

        Pantools.logger.debug("Writing file.");
        write_SB_to_file_full_path(outputBuilder, accessionOverviewPath);
    }
}
