package nl.wur.bif.pantools.construction.build_pangenome.parallel;

import java.util.ArrayList;
import java.util.List;

/**
 * Stores anchor nodes, positions and sides during localization. This is a convenience class for aggregating these three
 * elements into a single class, rather than keeping an array for each as in the previous implementation.
 */
public class Anchors {
    private final List<Anchor> anchors;

    private static class Anchor {
        public final long nodeId;
        public final int position;
        public final char side;

        public Anchor(long nodeId, int position, char side) {
            this.nodeId = nodeId;
            this.position = position;
            this.side = side;
        }
    }

    public Anchors() {
        // TODO: pre-allocate
        anchors = new ArrayList<>();
    }

    public void add(long nodeId, int position, char side) {
        anchors.add(new Anchor(nodeId, position, side));
    }

    public long[] getNodeIds() {
        return anchors.stream().mapToLong(anchor -> anchor.nodeId).toArray();
    }

    public int[] getPositions() {
        return anchors.stream().mapToInt(anchor -> anchor.position).toArray();
    }

    public int size() {
        return anchors.size();
    }

    public String getSides() {
        final StringBuilder builder = new StringBuilder(anchors.size());
        anchors.forEach(anchor -> builder.append(anchor.side));
        return builder.toString();
    }
}
