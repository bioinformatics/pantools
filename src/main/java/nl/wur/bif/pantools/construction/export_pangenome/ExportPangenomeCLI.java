package nl.wur.bif.pantools.construction.export_pangenome;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static picocli.CommandLine.*;

/**
 * Classify functional annotations as core, accessory or unique.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@CommandLine.Command(name = "export_pangenome", sortOptions = false, abbreviateSynopsis = true)
public class ExportPangenomeCLI implements Callable<Integer> {
    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--node-properties-file"}, required = true)
    Path nodePropertiesFile;

    @Option(names = {"--relationship-properties-file"}, required = true)
    Path relationshipPropertiesFile;

    @Option(names = {"--sequence-node-anchors-file"}, required = true)
    Path sequenceNodeAnchorsFile;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();
        new PangenomeExporter(nodePropertiesFile, relationshipPropertiesFile, sequenceNodeAnchorsFile);
        return 0;
    }
}
