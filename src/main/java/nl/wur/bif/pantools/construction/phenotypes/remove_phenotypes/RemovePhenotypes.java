package nl.wur.bif.pantools.construction.phenotypes.remove_phenotypes;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.phenotypes.Phenotypes;
import org.neo4j.graphdb.*;

import java.io.File;
import java.util.Scanner;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class RemovePhenotypes extends Phenotypes {

    public RemovePhenotypes() {
        removePhenotype();
    }

    /**
     * remove_phenotype callable by user
     *
     * Removal of phenotype nodes or properties on phenotype nodes.
     * phenotype properties:
     * The user included a phenotype property via --phenotype. In this case PHENOTYPE != null.
     *
     * phenotype nodes:
     * With no --phenotype included. PHENOTYPE == null and all nodes phenotype nodes will be removed.
     *
     */
    public void removePhenotype() {
        Pantools.logger.info("Removing phenotype information/nodes from the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException("Database error");
        }
        if (PHENOTYPE != null) { // verify the phenotype, not allowed to be "genome"
            if (PHENOTYPE.equals("genome")) {
                Pantools.logger.error("The --phenotype property is not allowed to be called \"genome\"");
                System.exit(1);
            }
        }

        int nodeCounter = 0, propertyCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodes.hasNext()) { // go over phenotype nodes. Delete phenotype node or certain property
                Node phenotypeNode = phenotypeNodes.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                if (PHENOTYPE == null) { // no phenotype given, remove nodes
                    Iterable<Relationship> relations = phenotypeNode.getRelationships(); // has_phenotype relationship
                    for (Relationship relation : relations){
                        relation.delete();
                    }
                    phenotypeNode.delete();
                    nodeCounter++;
                } else {
                    if (phenotypeNode.hasProperty(PHENOTYPE)) {
                        phenotypeNode.removeProperty(PHENOTYPE);
                        propertyCounter ++;
                    }
                }
            }

            // ask user for verification before removal of nodes and properties
            boolean remove = false;
            if (PHENOTYPE == null && nodeCounter > 1) {
                System.out.print("\r" + nodeCounter + " phenotype nodes will be removed. Do you want to continue [y/n]?\n-> ");
                Scanner s = new Scanner(System.in);
                String str = s.nextLine().toLowerCase();
                if (str.equals("y") || str.equals("yes")) {
                    remove = true;
                } else {
                    System.out.println("\rNothing was removed\n");
                    return;
                }
            } else if (propertyCounter > 1) {
                System.out.print("\rThe property '" + PHENOTYPE + "' will be removed from " + propertyCounter + " phenotype nodes. Do you want to continue [y/n]?\n-> ");
                Scanner s = new Scanner(System.in);
                String str = s.nextLine().toLowerCase();
                if (str.equals("y") || str.equals("yes")) {
                    remove = true;
                } else {
                    System.out.println("\rNothing was removed\n");
                    return;
                }
            } else if (PHENOTYPE != null && propertyCounter == 0) {
                System.out.println("\rThe property '" + PHENOTYPE + "' did not match with any phenotype nodes\n");
            }

            if (remove) {
                tx.success();
                System.out.println("\rRemoval was successful");
                long phenotypeNodeCount = count_nodes(PHENOTYPE_LABEL);
                new File(WORKING_DIRECTORY + "phenotype_overview.txt").delete(); // delete older summmary file
                if (phenotypeNodeCount > 0) { // create a new phenotype_overview.txt if there still are phenotype nodes
                    phenotypeOverview();
                }
            } else {
                tx.failure();
                System.out.println("\rNothing was removed\n");
            }
        }
    }
}
