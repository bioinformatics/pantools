package nl.wur.bif.pantools.construction.remove;

import nl.wur.bif.pantools.Pantools;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.graphdb.*;
import java.util.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;


public class Remove {


    public void remove_phasing() {
        int seq_counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequence_nodes.hasNext()) {
                Node seq_node = sequence_nodes.next();
                seq_counter ++;
                if (seq_counter == 1 || seq_counter % 10000 == 0){
                    System.out.print("\rRemoving phasing properties from sequence nodes: " + seq_counter);
                }
                if (seq_node.hasProperty("phasing_chromosome")) {
                    seq_node.removeProperty("phasing_chromosome");
                    seq_node.removeProperty("phasing_ID");
                    seq_node.removeProperty("phasing_assigned");
                }
            }
            tx.success();
        }
    }

    /**
     * Not allowed to remove anything from 'build_pangenome' and 'annotation' nodes
     */
    public void remove_nodes() {
        Pantools.logger.info("Removing nodes from the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            skip.create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nee ) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        if (NODE_ID == null && SELECTED_LABEL == null) {
            Pantools.logger.error("Provide either --node or --label");
            System.exit(1);
        } else if (NODE_ID != null && SELECTED_LABEL == null) { // --node argument was used
            remove_selected_nodes();
        } else if (SELECTED_LABEL != null && NODE_ID == null) { // --label argument was used
            remove_all_nodes_with_label(Label.label(SELECTED_LABEL));
        }
    }

    /**
     * Collects nodes and relations that match a specific or multiple node labels. These will be removed at the end of the function
     * @param selected_label
     */
    public void remove_all_nodes_with_label(Label selected_label) {
        final String label_str = selected_label.toString();
        final HashSet<Node> all_nodes = new HashSet<>();
        final HashSet<Relationship> all_relationships = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            check_if_label_exists();
            collect_nodes_relations_matching_label(selected_label, all_nodes, all_relationships);
            tx.success(); // transaction successful, commit changes
        }
        remove_selected_nodes_from_database("remove_nodes_relations", all_nodes, all_relationships);
        if (label_str.equals("repeat")) {
            remove_added_repeats_properties();
        }
    }

    /**
     * Check all the labels of the pangenome to see if the given label is valid
     * @return
     */
    public Label check_if_label_exists() {
        if (SELECTED_LABEL == null) {
            Pantools.logger.error("No --label was provided.");
            System.exit(1);
        }
        Label selected_label = Label.label(SELECTED_LABEL);
        ResourceIterable<Label> all_labels = GRAPH_DB.getAllLabels();
        boolean found_label = false;
        String present_labels = "";
        for (Label label1 : all_labels) {
            if (label1.equals(selected_label)) {
                found_label = true;
                break;
            }
            present_labels += label1 + ",";
        }
        if (!found_label) {
            System.out.println("\rProvided label '" + selected_label + "' does not match with anything in the pangenome\n"
                    + "Present labels: " + present_labels.replaceFirst(".$", "") + "\n");
            System.exit(1);
        }
        return selected_label;
    }

    /**
     * When repeats are removed, remove the property that states they were added
     */
    public static void remove_added_repeats_properties() {
        boolean first = true;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequence_nodes.hasNext()) {
                Node sequence_node = sequence_nodes.next();
                int genomeNr = (int) sequence_node.getProperty("genome");
                if (!skip_array[genomeNr-1]) { // genome was not skipped
                    if (first) {
                        sequence_node.removeProperty("repeats_added");
                        first = false;
                    }
                    sequence_node.removeProperty("repeats_added");
                }
            }
            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     *
     * @param what_to_do
     * @param all_nodes
     * @param all_relations
     */
    public void remove_selected_nodes_from_database(String what_to_do, HashSet<Node> all_nodes, HashSet<Relationship> all_relations) {
        switch (what_to_do) {
            case "remove_nodes_relations":
                delete_nodes_and_relationships(all_nodes, all_relations);
                break;
            case "remove_phobius_functions":
                remove_phobius_properties();
                break;
            case "remove_cog_functions":
                remove_COG_properties();
                break;
            case "remove_signalp":
                remove_signalp_properties();
                break;
            case "remove_all_function_properties":
                remove_phobius_properties();
                remove_COG_properties();
                remove_signalp_properties();
            case "remove_all_functional_annotations":
                remove_COG_properties();
                remove_phobius_properties();
                remove_signalp_properties();
                delete_nodes_and_relationships(all_nodes, all_relations);
                break;
            default:
                Pantools.logger.error("Unknown function to remove: {}", what_to_do);
                throw new RuntimeException("Unknown function");
        }
    }

    /**
     * Removes nodes and relationships. Commits a transaction every 10k changes.
     * @param all_nodes
     * @param all_relations
     */
    public static void delete_nodes_and_relationships(HashSet<Node> all_nodes, HashSet<Relationship> all_relations) {
        MAX_TRANSACTION_SIZE = 10000;
        long one_procent_rel = (all_relations.size()+100)/100;
        long one_procent_node = (all_nodes.size()+100)/100;
        int trsc = 0;
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            double rel_counter = 0, node_counter = 0;
            for (Relationship relation : all_relations) {
                rel_counter ++;
                if (rel_counter % 10000 == 0) {
                    String percentage = String.format("%.2f", rel_counter/one_procent_rel);
                    System.out.print("\rRemoving relationships " + (int) rel_counter +"/" + all_relations.size() + " (" + percentage + "%)       "); // spaces are intentional
                }
                relation.delete();
                trsc ++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
            }
            System.out.println("\rRemoving relationships " + (int) rel_counter + "/" + all_relations.size() + " (100%)        "); // spaces are intentional

            for (Node node : all_nodes) {
                node_counter ++;
                if (node_counter % 10000 == 0) {
                    String percentage = String.format("%.2f", node_counter/one_procent_node);
                    System.out.print("\rRemoving nodes " + (int) node_counter + "/" + all_nodes.size() + " (" + percentage + "%)      "); // spaces are intentional
                }
                node.delete();
                trsc ++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
            }
            System.out.println("\rRemoving nodes " + (int) node_counter + "/" + all_nodes.size() + " (100%)       "); // spaces are intentional
            tx.success();
        } finally {
            tx.close();
        }
    }

    /**
     * Remove the properties related to COG
     */
    public void remove_COG_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(COG_LABEL);
            int counter = 0;
            while (nodes.hasNext()) {
                counter ++;
                if (counter % 10000 == 0) {
                    System.out.print("\rRemoving COG annotations: " + counter + " nodes ");
                }
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(COG_LABEL);
                mrna_node.removeProperty("COG_id"); // old, can be removed later
                mrna_node.removeProperty("COG_category");
                mrna_node.removeProperty("COG_name"); // old, can be removed later
                mrna_node.removeProperty("COG_description");
            }
            System.out.println("\rRemoved the COG annotations from " + counter + " nodes ");
            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     * Remove some properties related to Phobius
     */
    public void remove_phobius_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(PHOBIUS_LABEL);
            int counter = 0;
            while (nodes.hasNext()) {
                counter ++;
                if (counter % 10000 == 0) {
                    System.out.print("\rRemoving Phobius properties: " + counter + " nodes ");
                }
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(PHOBIUS_LABEL);
                mrna_node.removeProperty("phobius_effector"); // old can be removed later
                mrna_node.removeProperty("phobius_secreted_protein"); // old can be removed later
                mrna_node.removeProperty("phobius_receptor"); // old can be removed later
                mrna_node.removeProperty("phobius_transmembrane");
                mrna_node.removeProperty("phobius_signal_peptide");
            }
            System.out.println("\rRemoved the Phobius annotations from " + counter + " nodes ");
            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     * Remove some properties related to signalp
     */
    public void remove_signalp_properties() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> nodes = GRAPH_DB.findNodes(SIGNALP_LABEL);
            int counter = 0;
            while (nodes.hasNext()) {
                counter ++;
                Node mrna_node = nodes.next();
                mrna_node.removeLabel(SIGNALP_LABEL);
                mrna_node.removeProperty("signal_signal_peptide");
            }
            System.out.println("\rRemoved the SignalP annotations from " + counter + " nodes ");
            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     *
     * @param selected_label
     * @param all_nodes
     * @param all_rels
     */
    public void collect_nodes_relations_matching_label(Label selected_label, HashSet<Node> all_nodes, HashSet<Relationship> all_rels) {
        ResourceIterator<Node> nodes = GRAPH_DB.findNodes(selected_label);
        while (nodes.hasNext()) {
            Node node = nodes.next();
            Iterable<Relationship> rels = node.getRelationships();
            for (Relationship rel : rels) {
                all_rels.add(rel);
            }
            all_nodes.add(node);
        }
    }

    /**
     *
     * @param label_array2
     * @param all_nodes
     * @param all_rels
     * @param selected_label
     * @return
     */
    public String collect_functional_annotation_nodes_to_remove(String[] label_array2, HashSet<Node> all_nodes, HashSet<Relationship> all_rels, Label selected_label) {
        String what_to_do = "do_nothing";
        System.out.print("\rYou want to remove nodes which are part of the functional annotation."
                + "\nDo you want to remove all nodes with these labels: " + Arrays.toString(label_array2).replace("[","").replace("]","").replace(", ",",") + " [y/n]?\n-> ");
        Scanner s = new Scanner(System.in);
        String str = s.nextLine().toLowerCase();
        if (str.equals("y") || str.equals("yes")) {
            collect_nodes_relations_matching_label(GO_LABEL, all_nodes, all_rels);
            collect_nodes_relations_matching_label(PFAM_LABEL, all_nodes, all_rels);
            collect_nodes_relations_matching_label(TIGRFAM_LABEL, all_nodes, all_rels);
            collect_nodes_relations_matching_label(INTERPRO_LABEL, all_nodes, all_rels);
            what_to_do = "remove_all_functional_annotations";
        } else { // no
            System.out.print("\nDo you ONLY want to remove the '" + SELECTED_LABEL + "' nodes [y/n]?\n-> ");
            s = new Scanner(System.in);
            str = s.nextLine().toLowerCase();
            if (str.equals("y") || str.equals("yes")) {
                switch (SELECTED_LABEL) {
                    case "COG":
                        what_to_do = "remove_cog_functions";
                        break;
                    case "phobius":
                        what_to_do = "remove_phobius_functions";
                        break;
                    case "signalp":
                        what_to_do = "remove_signalp";
                        break;
                    default:
                        what_to_do = "remove_nodes_relations";
                        collect_nodes_relations_matching_label(selected_label, all_nodes, all_rels);
                        break;
                }
            } else {
                // do nothing
            }
        }
        return what_to_do;
    }

    /**
     * Count how many of the mRNAs are protein coding
     * @param mrna_nodes
     * @return
     */
    public int check_protein_ids(HashSet<Node> mrna_nodes) {
        int counter = 0;
        for (Node mrna_node : mrna_nodes) {
            if (mrna_node.hasProperty("protein_ID")) {
                counter ++;
            }
        }
        return counter;
    }

    /**
     * Remove a set of nodes from the pangenome that were given by the user via the command line (--node)
     * Not allowed to remove anything from 'build_pangenome' and 'add_annotations'
     */
    public void remove_selected_nodes() {
        HashSet<Node> all_nodes = new HashSet<>(); // holds all nodes that will be removed
        HashSet<Relationship> all_relations = new HashSet<>(); // the relationships belonging to the nodes that will be removed. are removed aswell
        HashSet<Node> mrna_nodes = new HashSet<>(); // check if mRNA nodes have 'protein_ID' to adjust the 'num_proteins' property
        boolean remove = false;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            retrieve_selected_nodes_for_removal(all_nodes, all_relations, mrna_nodes);
            int total_proteins = check_protein_ids(mrna_nodes);
            Scanner s = new Scanner(System.in);

            // TODO: replace with proper prompt. there are more cases like this and there is actually a prompt function in utils
            Pantools.logger.info("Do you really want to remove these from the database [y/n]?");
            System.out.println();

            String str = s.nextLine().toLowerCase();
            if (str.equals("y") || str.equals("yes")) {
                remove = true;
                if (total_proteins > 0) {
                    Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
                    int num_proteins = (int) pangenome_node.getProperty("num_proteins");
                    pangenome_node.removeProperty("num_proteins");
                    pangenome_node.setProperty("num_proteins", (num_proteins-total_proteins));
                }
            } else { // no successfull transaction
                Pantools.logger.info("Did not remove them.");
            }
            tx.success(); // transaction successful, commit changes
        }
        if (remove) {
            delete_nodes_and_relationships(all_nodes, all_relations);
        }
    }

    /**
     * Properties for phobius and COG are removed without asking permission
     * @param all_nodes
     * @param all_relations
     * @param mrna_nodes
     */
    public void retrieve_selected_nodes_for_removal(HashSet<Node> all_nodes, HashSet<Relationship> all_relations, HashSet<Node> mrna_nodes) {
        String[] label_array_no = new String[]{"nucleotide","sequence","pangenome","genome","degenerate","annotation"}; // these cannot be removed
        String[] remove_properties = new String[]{"phobius","COG"}; // only the functions properties are allowed to be be removed
        String[] node_str_array = NODE_ID.replace(",,","").replace(" ","").split(",");
        for (String node_str : node_str_array) {
            long node_id = Long.parseLong(node_str);
            Node target_node = GRAPH_DB.getNodeById(node_id);
            Iterable<Label> labels = target_node.getLabels();
            for (Label label1 : labels) {
                String label_str = label1.toString();
                if (ArrayUtils.contains(label_array_no, label_str)) {
                    Pantools.logger.info("Unable to delete Node {} because it has one of these labels {}", node_id, Arrays.toString(label_array_no));
                } else if (label_str.equals("mRNA")) {
                    mrna_nodes.add(target_node);
                } else if (ArrayUtils.contains(remove_properties, label_str)) {
                    remove_cog_or_phobius_properties(target_node, label_str);
                }
            }
            all_nodes.add(target_node);
            Iterable<Relationship> rels = target_node.getRelationships();
            for (Relationship rel : rels) {
                all_relations.add(rel);
            }
        }
        Pantools.logger.info("Found {} nodes with {} relationships.", all_nodes.size(), all_relations.size());
    }

    /**
     * Remove some properties related to COG and phobius
     * @param node
     * @param label_str
     */
    public void remove_cog_or_phobius_properties(Node node, String label_str) {
        if (label_str.equals("COG")) {
            node.removeProperty("COG_category");
            node.removeProperty("COG_description");
        } else { // phobius
            node.removeProperty("phobius_secreted_protein");
            node.removeProperty("phobius_receptor");
            node.removeProperty("phobius_transmembrane");
        }
    }

    public void removeFunctions(RemoveFunctionsCLI cli) {
        final String mode = cli.getMode().toUpperCase();
        final HashSet<Node> allNodes = new HashSet<>();
        final HashSet<Relationship> allRelationships = new HashSet<>();
        String whatToDo;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            switch (mode) {
                case "NODES":
                    Pantools.logger.info("Removing all function nodes.");
                    collectFunctionNodes(allNodes, allRelationships);
                    whatToDo = "remove_nodes_relations";
                    break;
                case "BGC":
                    Pantools.logger.info("Removing BGC annotations.");
                    collect_nodes_relations_matching_label(BGC_LABEL, allNodes, allRelationships);
                    whatToDo = "remove_nodes_relations";
                    break;
                case "PROPERTIES":
                    Pantools.logger.info("Removing all functional properties.");
                    whatToDo = "remove_all_function_properties";
                    break;
                case "COG":
                    Pantools.logger.info("Removing COG functional annotations.");
                    whatToDo = "remove_cog_functions";
                    break;
                case "PHOBIUS":
                    Pantools.logger.info("Removing Phobius functional annotations.");
                    whatToDo = "remove_phobius_functions";
                    break;
                case "SIGNALP":
                    Pantools.logger.info("Removing SignalP functional annotations.");
                    whatToDo = "remove_signalp";
                    break;
                case "ALL":
                    Pantools.logger.info("Removing all functional annotations.");
                    collectFunctionNodes(allNodes, allRelationships);
                    whatToDo = "remove_all_functional_annotations";
                    break;
                default:
                    Pantools.logger.error("Unknown mode: {}", mode);
                    throw new RuntimeException("Unknown mode");
            }
        }
        remove_selected_nodes_from_database(whatToDo, allNodes, allRelationships);
    }

    private void collectFunctionNodes(HashSet<Node> allNodes, HashSet<Relationship> allRelationships) {
        collect_nodes_relations_matching_label(GO_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(PFAM_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(TIGRFAM_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(INTERPRO_LABEL, allNodes, allRelationships);
        collect_nodes_relations_matching_label(BGC_LABEL, allNodes, allRelationships);
    }


}
