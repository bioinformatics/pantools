package nl.wur.bif.pantools.construction.build_pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Input;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.utils.Globals.GRAPH_DB;
import static nl.wur.bif.pantools.utils.Globals.GENOME_DB;

/**
 * Task for writing out genome frequency information to nucleotide nodes. This task reads a list of {@link Localization}
 * items from a file, groups them by node ID and genome ID, and calculates the number of times this node occurs in each
 * genome, before writing out three properties to the node:
 * <br/>
 * 1. `frequencies`: an array counting each time this node occurs in each genome (position 0 left 0);
 * 2. `frequency`: the sum of the `frequencies` array;
 * 3. `high_frequency`: flag (set to true only, not false) indicating whether the node occurs with high frequency.
 * offsets before writing them out to a relationship.
 */
public class NodeFrequenciesWriterTask implements Callable<Long> {
    private final Path path;
    private final Logger log;
    private final long maximumBatchSize;

    // TODO: refactor to base task for adding entries to commit
    public NodeFrequenciesWriterTask(Path path, Logger log, long maximumBatchSize) {
        this.path = path;
        this.log = log;
        this.maximumBatchSize = maximumBatchSize;
    }

    @Override
    public Long call() throws Exception {
        final Transaction[] tx = {GRAPH_DB.beginTx()};

        AtomicLong highestFrequency = new AtomicLong();
        AtomicLong counter = new AtomicLong(0);
        try {
            final List<Localization> localizations = loadLocalizations(path, KryoUtils.getKryo());
            log.debug(String.format("Loaded %,d localizations from file %s", localizations.size(), path));
//            localizations.sort(new LocalizationOffsetComparator());

            if (localizations.isEmpty())
                return 0L;

            // TODO: optimize memory by looping
            final Map<Long, List<Localization>> grouped = localizations
                .stream()
                .collect(Collectors.groupingBy(Localization::getEndNodeId));

            grouped.forEach((nodeId, ls) -> {
                final long[] frequencies = new long[GENOME_DB.num_genomes + 1];
                Arrays.fill(frequencies, 0);

                for (Localization l : ls)
                    frequencies[l.getGenomeIndex()]++;

                final long frequency = Arrays.stream(frequencies).sum();

                if (frequency > highestFrequency.get())
                    highestFrequency.set(frequency);

                final Node node = GRAPH_DB.getNodeById(nodeId);
                node.setProperty("frequencies", frequencies);
                node.setProperty("frequency", frequency);
                if (frequency >= GENOME_DB.num_genomes * 100L) {
                    // TODO: do we clear previously set high_frequency property if this is not a high freqyency node?
                    node.setProperty("high_frequency", true);
                }

                //

                counter.getAndIncrement();
                if (counter.get() % maximumBatchSize == 0) {
                    log.trace(String.format("Committing batch of size %,d", maximumBatchSize));
                    tx[0].success();
                    tx[0].close();
                    tx[0] = GRAPH_DB.beginTx();
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("error creating Kryo input for path " + path + ": " + e);
        } finally {
            // TODO: debug message for last batch
            tx[0].success();
            tx[0].close();
        }
        log.debug(String.format("Written %,d properties for bucket %s", counter.get(), path));

        // TODO: return more involved metrics
        return highestFrequency.get();
    }

    // TODO: remove duplicate code
    public List<Localization> loadLocalizations(Path path, Kryo kryo) throws IOException {
        final List<Localization> updates = new ArrayList<>();

        try (final Input input = KryoUtils.createInput(path)) {
            while (!input.end())
                updates.add(kryo.readObject(input, Localization.class));
        }

        return updates;
    }
}
