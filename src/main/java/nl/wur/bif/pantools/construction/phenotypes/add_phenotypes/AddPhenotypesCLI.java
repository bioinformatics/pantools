package nl.wur.bif.pantools.construction.phenotypes.add_phenotypes;

import jakarta.validation.constraints.Positive;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Include phenotype data to the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "add_phenotypes", sortOptions = false)
public class AddPhenotypesCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "phenotype", index = "0+")
    @InputFile(message = "{file.phenotype}")
    Path phenotypesFile;

    @Option(names = "--bins")
    @Positive(message = "{positive.bins}")
    int bins;

    @Option(names = "--append")
    boolean append;

    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();
        new AddPhenotypes(phenotypesFile, bins, append);
        return 0;
    }

}
