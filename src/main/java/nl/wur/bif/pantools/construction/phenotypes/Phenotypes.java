package nl.wur.bif.pantools.construction.phenotypes;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.util.*;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.write_string_to_file_in_DB;

public abstract class Phenotypes {

    /**
     * Reports all phenotype nodes and their properties
     */
    protected void phenotypeOverview() {
        HashMap<String, Integer> count_phenotype = new HashMap<>();
        HashMap<String, ArrayList<Integer>> genomes_per_phenotype = new HashMap<>();
        Set<String> phenotype_set = new HashSet<>(); // phenotype properties
        String phenos_per_genomes = retrievePhenotypesForOverview(count_phenotype, genomes_per_phenotype, phenotype_set);
        String phenotype_occurrence = countPhenotypeOccurrence(phenotype_set, count_phenotype);
        String genomes_per_pheno = printGenomesPerPhenotype(phenotype_set, genomes_per_phenotype);
        write_string_to_file_in_DB(genomes_per_pheno + phenotype_occurrence + phenos_per_genomes, "phenotype_overview.txt");
        Pantools.logger.info("Overview written to: {}phenotype_overview.txt", WORKING_DIRECTORY);
    }

    /**
     *
     * @param count_phenotype
     * @param genomes_per_phenotype
     * @param phenotype_set
     * @return
     */
    private String retrievePhenotypesForOverview(HashMap<String, Integer> count_phenotype, HashMap<String, ArrayList<Integer>> genomes_per_phenotype, Set<String> phenotype_set) {
        StringBuilder per_genome_builder = new StringBuilder("##Part 3. Phenotypes per genome\n");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (pheno_nodes.hasNext()) {
                Node pheno_node = pheno_nodes.next();
                long pheno_id = pheno_node.getId();
                Iterable<String> properties = pheno_node.getPropertyKeys();
                int genome_nr = (int) pheno_node.getProperty("genome");
                per_genome_builder.append("\nGenome ").append(genome_nr).append(", phenotype node: ").append(pheno_id).append("\n");

                // loop over the sorted properties using a stream
                List<String> sortedProperties = new ArrayList<>();
                properties.forEach(sortedProperties::add);
                Collections.sort(sortedProperties);
                for (String property : sortedProperties) {
                    if (property.equals("genome")) {
                        continue;
                    }
                    Object prop_value = pheno_node.getProperty(property); // print a value from node without converting?!
                    per_genome_builder.append(" ").append(property).append(": '").append(prop_value).append("'\n");
                    phenotype_set.add(property);
                    count_phenotype.merge(property + "#" + prop_value, 1, Integer::sum);
                    genomes_per_phenotype.computeIfAbsent(property + ":" + prop_value, k -> new ArrayList<>())
                            .add(genome_nr);
                }
            }
            per_genome_builder.append("\n");
            tx.success(); // transaction successful, commit changes
        }
        return per_genome_builder.toString();
    }

    /**
     *
     * @param phenotype_set
     * @param count_phenotype
     * @return
     */
    private String countPhenotypeOccurrence(Set<String> phenotype_set, HashMap<String, Integer> count_phenotype) {
        StringBuilder pheno_occurrence = new StringBuilder("##Part 2. Occurrence of each value within a phenotype\n");
        for (String phenotype : phenotype_set) {
            StringBuilder part_builder = new StringBuilder();
            int counter = 0;
            for (String phenotype_value : count_phenotype.keySet()) {
                if (phenotype_value.startsWith(phenotype + "#")) {
                    int value = count_phenotype.get(phenotype_value);
                    phenotype_value = phenotype_value.replace(phenotype + "#", "");
                    part_builder.append(" ").append(phenotype_value).append(", ").append(value).append("\n");
                    counter += value;
                }
            }
            pheno_occurrence.append(phenotype).append(" (").append(counter).append(" genomes)\n").append(part_builder).append("\n");
        }
        return pheno_occurrence.toString();
    }

    /**
     *
     * @param phenotype_set
     * @param genomes_per_phenotype
     * @return
     */
    private String printGenomesPerPhenotype(Set<String> phenotype_set, HashMap<String, ArrayList<Integer>> genomes_per_phenotype) {
        StringBuilder output_builder = new StringBuilder("##This file consists of three parts.\n##Part 1. Overview of values and genomes per phenotype.\n");
        StringBuilder header = new StringBuilder("#Present phenotypes: ");
        for (String pheno : phenotype_set) {
            header.append(pheno).append(", ");
            output_builder.append("#").append(pheno).append("\n");
            for (Map.Entry<String, ArrayList<Integer>> entry : genomes_per_phenotype.entrySet()) {
                String key = entry.getKey();
                ArrayList<Integer> value_list = entry.getValue();
                if (key.startsWith(pheno + ":")) {
                    StringBuilder allValues = new StringBuilder();
                    for (int value : value_list) {
                        allValues.append(value).append(",");
                    }
                    output_builder.append(" ").append(key.replace(pheno + ":", "")).append(": ").append(allValues).append("\n");
                }
            }
            output_builder.append("\n");
        }
        return header.toString().replaceFirst(".$", "").replaceFirst(".$", "") + "\n\n" + output_builder.toString();
    }
}
