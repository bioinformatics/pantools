package nl.wur.bif.pantools.construction.remove;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static picocli.CommandLine.*;

/**
 * Remove an homology grouping from the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "remove_functions", sortOptions = false)
public class RemoveFunctionsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-m", "--mode"})
    @Pattern(regexp = "nodes|properties|bgc|all|COG|phobius|signalp", flags = CASE_INSENSITIVE,
            message = "{pattern.mode-rmf}")
    String mode;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();

        Remove remove = new Remove();
        remove.removeFunctions(this);
        return 0;
    }

    public String getMode() {
        return mode;
    }
}
