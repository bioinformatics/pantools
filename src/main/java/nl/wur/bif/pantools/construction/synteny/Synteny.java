package nl.wur.bif.pantools.construction.synteny;

import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.Utils.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.*;

public class Synteny {

    // holds 3 callable functions

    // calculate_synteny
    // add_synteny
    // synteny_overview

    private static AtomicInteger mrnasInAnalysis;
    private static AtomicInteger homologousMrnasInAnalysis;

    private BlockingQueue<String> sequenceCombinationQueue;
    private BlockingQueue<Node> nodeQueue;
    private static AtomicLong comparisonCounter;
    private PhasedFunctionalities pf;
    public String[] two_letter_combis = new String[]{"aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap", "aq", "ar", "as", "at", "au",
            "av", "aw", "ax", "ay", "az", "ba", "bb", "bc", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bk", "bl", "bm", "bn", "bo", "bp", "bq", "br", "bs", "bt", "bu", "bv", "bw",
            "bx", "by", "bz", "ca", "cb", "cc", "cd", "ce", "cf", "cg", "ch", "ci", "cj", "ck", "cl", "cm", "cn", "co", "cp", "cq", "cr", "cs", "ct", "cu", "cv", "cw", "cx", "cy",
            "cz", "da", "db", "dc", "dd", "de", "df", "dg", "dh", "di", "dj", "dk", "dl", "dm", "dn", "do", "dp", "dq", "dr", "ds", "dt", "du", "dv", "dw", "dx", "dy", "dz", "ea",
            "eb", "ec", "ed", "ee", "ef", "eg", "eh", "ei", "ej", "ek", "el", "em", "en", "eo", "ep", "eq", "er", "es", "et", "eu", "ev", "ew", "ex", "ey", "ez", "fa", "fb", "fc", "fd",
            "fe", "ff", "fg", "fh", "fi", "fj", "fk", "fl", "fm", "fn", "fo", "fp", "fq", "fr", "fs", "ft", "fu", "fv", "fw", "fx", "fy", "fz", "ga", "gb", "gc", "gd", "ge", "gf", "gg",
            "gh", "gi", "gj", "gk", "gl", "gm", "gn", "go", "gp", "gq", "gr", "gs", "gt", "gu", "gv", "gw", "gx", "gy", "gz", "ha", "hb", "hc", "hd", "he", "hf", "hg", "hh", "hi", "hj",
            "hk", "hl", "hm", "hn", "ho", "hp", "hq", "hr", "hs", "ht", "hu", "hv", "hw", "hx", "hy", "hz", "ia", "ib", "ic", "id", "ie", "if", "ig", "ih", "ii", "ij", "ik", "il", "im",
            "in", "io", "ip", "iq", "ir", "is", "it", "iu", "iv", "iw", "ix", "iy", "iz", "ja", "jb", "jc", "jd", "je", "jf", "jg", "jh", "ji", "jj", "jk", "jl", "jm", "jn", "jo", "jp",
            "jq", "jr", "js", "jt", "ju", "jv", "jw", "jx", "jy", "jz", "ka", "kb", "kc", "kd", "ke", "kf", "kg", "kh", "ki", "kj", "kk", "kl", "km", "kn", "ko", "kp", "kq", "kr", "ks",
            "kt", "ku", "kv", "kw", "kx", "ky", "kz", "la", "lb", "lc", "ld", "le", "lf", "lg", "lh", "li", "lj", "lk", "ll", "lm", "ln", "lo", "lp", "lq", "lr", "ls", "lt", "lu", "lv",
            "lw", "lx", "ly", "lz", "ma", "mb", "mc", "md", "me", "mf", "mg", "mh", "mi", "mj", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my",
            "mz", "na", "nb", "nc", "nd", "ne", "nf", "ng", "nh", "ni", "nj", "nk", "nl", "nm", "nn", "no", "np", "nq", "nr", "ns", "nt", "nu", "nv", "nw", "nx", "ny", "nz", "oa", "ob",
            "oc", "od", "oe", "of", "og", "oh", "oi", "oj", "ok", "ol", "om", "on", "oo", "op", "oq", "or", "os", "ot", "ou", "ov", "ow", "ox", "oy", "oz", "pa", "pb", "pc", "pd", "pe", "pf",
            "pg", "ph", "pi", "pj", "pk", "pl", "pm", "pn", "po", "pp", "pq", "pr", "ps", "pt", "pu", "pv", "pw", "px", "py", "pz", "qa", "qb", "qc", "qd", "qe", "qf", "qg", "qh", "qi", "qj",
            "qk", "ql", "qm", "qn", "qo", "qp", "qq", "qr", "qs", "qt", "qu", "qv", "qw", "qx", "qy", "qz", "ra", "rb", "rc", "rd", "re", "rf", "rg", "rh", "ri", "rj", "rk", "rl", "rm", "rn",
            "ro", "rp", "rq", "rr", "rs", "rt", "ru", "rv", "rw", "rx", "ry", "rz", "sa", "sb", "sc", "sd", "se", "sf", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sp", "sq", "sr",
            "ss", "st", "su", "sv", "sw", "sx", "sy", "sz", "ta", "tb", "tc", "td", "te", "tf", "tg", "th", "ti", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tq", "tr", "ts", "tt",
            "tu", "tv", "tw", "tx", "ty", "tz", "ua", "ub", "uc", "ud", "ue", "uf", "ug", "uh", "ui", "uj", "uk", "ul", "um", "un", "uo", "up", "uq", "ur", "us", "ut", "uu", "uv", "uw",
            "ux", "uy", "uz", "va", "vb", "vc", "vd", "ve", "vf", "vg", "vh", "vi", "vj", "vk", "vl", "vm", "vn", "vo", "vp", "vq", "vr", "vs", "vt", "vu", "vv", "vw", "vx", "vy", "vz",
            "wa", "wb", "wc", "wd", "we", "wf", "wg", "wh", "wi", "wj", "wk", "wl", "wm", "wn", "wo", "wp", "wq", "wr", "ws", "wt", "wu", "wv", "ww", "wx", "wy", "wz", "xa", "xb", "xc", "xd",
            "xe", "xf", "xg", "xh", "xi", "xj", "xk", "xl", "xm", "xn", "xo", "xp", "xq", "xr", "xs", "xt", "xu", "xv", "xw", "xx", "xy", "xz", "ya", "yb", "yc", "yd", "ye", "yf", "yg",
            "yh", "yi", "yj", "yk", "yl", "ym", "yn", "yo", "yp", "yq", "yr", "ys", "yt", "yu", "yv", "yw", "yx", "yy", "yz", "za", "zb", "zc", "zd", "ze", "zf", "zg", "zh", "zi",
            "zj", "zk", "zl", "zm", "zn", "zo", "zp", "zq", "zr", "zs", "zt", "zu", "zv", "zw", "zx", "zy", "zz"};

    /**
     **
     * @param runMcscanx MCScanX will be executed by the 'calculate_synteny' function. Requires preparation of pairwise files per sequence.
     * @param homologyGroups
     */
    public void calculateSynteny(boolean runMcscanx, List<Long> homologyGroups) {
        Pantools.logger.info("Inferring synteny between genomes and sequences using MCScanX.");
        pf = new PhasedFunctionalities();
        if (!runMcscanx) {
            Pantools.logger.warn("--run was not included. PanTools will only create the required input files for MCScanX.");
        } else {
            checkIfProgramExistsUsingStdout("MCScanX_h", 500, "MCScanX_h", true, true);
        }

        createDirectory("synteny", true);
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome("calculate_synteny"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // sets grouping_version and longest_transcripts
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(100000, false);
        pf.preparePhasedGenomeInformation(true, selectedSequences);

        if (compare_sequences && selectedSequences.size() > two_letter_combis.length) {
            throw new RuntimeException("Only " + two_letter_combis.length + " sequences can be included in the synteny analysis. "
                    + "Currently " + selectedSequences.size() + " are selected.");

        } else if (selectedGenomes.size() > two_letter_combis.length) {
            throw new RuntimeException("Only " + two_letter_combis.length + " genomes can be included in the synteny analysis. "
                    + "Currently " + selectedGenomes + " are selected.");
        }

        proLayer.find_longest_transcript_per_gene(false, 0);
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            long totalHomologyNodes = count_nodes(HOMOLOGY_GROUP_LABEL);
            if (totalHomologyNodes == 0) {
                throw new RuntimeException("No homology grouping is active. Please run `group` first or activate the grouping via `activate_grouping`.");
            }
            tx.success();
        }

        if (runMcscanx) {
            createDirectory("synteny/sequence_combinations", true);
        }

        HashSet<String> sequenceCombinationsSet = retrieveSyntenySequenceCombinations("homologs", compare_sequences, selectedSequences, selectedGenomes);
        ArrayList<String> sequenceCombinations = new ArrayList<>(sequenceCombinationsSet);

        if (compare_sequences) {
            Pantools.logger.info("--sequence is included. Identifiers are created so sequences from the same genome are compared to another.");
            createMcscanxGffForSequences(sequenceCombinations, runMcscanx);
        } else { // only compare_genomes
            Pantools.logger.info("No --sequence was included. Identifiers are created in such a way that only different genomes are compared to another.");
            createMcscanxGffForGenomes(sequenceCombinations, runMcscanx);
        }
        Pantools.logger.info("Sequence identifiers were changed in order to make them compatible with MCScanX! Please check out: synteny_identifiers.csv");

        createMcscanxHomologyFiles(runMcscanx, sequenceCombinations, homologyGroups);
        createSkippedSequencesFile();

        if (runMcscanx) {
            prepareMcscanxParallelRun(sequenceCombinations);
            combineMcscanxParallelFiles(sequenceCombinations);
        }

        Pantools.logger.info("In- and output of {} comparisons written to:", sequenceCombinations.size());
        Pantools.logger.info(" {}synteny/synteny_identifiers.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/mcscanx.gff", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/mcscanx.homology", WORKING_DIRECTORY);

        if (runMcscanx) {
            Pantools.logger.info("MCScanX was run. Output of all comparisons is combined in: {}synteny/mcscanx.collinearity", WORKING_DIRECTORY);
        } else {
            Pantools.logger.info("MCScanX was not run yet. How to infer synteny?");
            Pantools.logger.info(" MCScanX_h {}synteny/mcscanx", WD_full_path);
        }
        Pantools.logger.info("Visualize the results on https://accusyn.usask.ca or https://synvisio.github.io.");
        Pantools.logger.info("Synteny information can be integrated into the pangenome using:");
        Pantools.logger.info(" pantools add_synteny {} {}synteny/mcscanx.collinearity.", WORKING_DIRECTORY, WORKING_DIRECTORY);
    }


    /**
     * Creates the required MCScanX homology input file for each (pairwise) comparison.
     * Two column format. One protein_ID per column. tab separated.
     *
     * Three ways to consider homologs. Method 2 is currently implemented
     *  1. everything is homologous in homology group
     *  2. only proteins with 'is_similar_to' relation in homology groups are homologs
     *  3. All proteins with 'is_similar_to' relations are homologs (also proteins not part of same homology group)
     * @param runMcscanx MCScanX will be executed by the 'calculate_synteny' function. Requires preparation of pairwise files per sequence.
     * @param sequenceCombinations
     * @param homologyGroups
     */
    private void createMcscanxHomologyFiles(boolean runMcscanx, ArrayList<String> sequenceCombinations, List<Long> homologyGroups) {

        try {
            HashMap<String, BufferedWriter> homologyFileWriters = startHomologyWriters(runMcscanx, sequenceCombinations);
            startParralelFindHomologsBetweenSequences(homologyFileWriters, homologyGroups);
            closeHomologyFileWriters(runMcscanx, sequenceCombinations, homologyFileWriters);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Creating homology input failed") ;
        }
    }

    /**
     *
     * @param runMcscanx
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     * @return
     */
    private HashMap<String, BufferedWriter> startHomologyWriters(boolean runMcscanx, ArrayList<String> sequenceCombinations) throws IOException {
        HashMap<String, BufferedWriter> homologyFileWriters = new HashMap<>();
        BufferedWriter homologyFile = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "synteny/mcscanx.homology"));
        homologyFileWriters.put("main", homologyFile);
        if (runMcscanx) {
            for (String seqCombi : sequenceCombinations) {
                String renamedSeqCombi = seqCombi.replace("_", "S").replace("#", "_");
                homologyFile = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "synteny/sequence_combinations/" + renamedSeqCombi + "/mcscanx.homology"));
                homologyFileWriters.put(seqCombi, homologyFile);
            }
        }
        return homologyFileWriters;
    }

    /**
     * @param runMcscanx
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     * @param homologyFileWriters mcscanx.homology writers per sequence combination
     */
    private void closeHomologyFileWriters(boolean runMcscanx,  ArrayList<String> sequenceCombinations,
                                          HashMap<String, BufferedWriter> homologyFileWriters) throws IOException {

        BufferedWriter homologyFile = homologyFileWriters.get("main");
        homologyFile.close();
        if (runMcscanx) {
            for (String seqCombi : sequenceCombinations) {
                BufferedWriter file = homologyFileWriters.get(seqCombi);
                file.close();
            }
        }
    }

    /**
     * Create a file to see which sequences were included in the last 'calculate_synteny' run.
     */
    private void createSkippedSequencesFile() {
        delete_file_full_path(WORKING_DIRECTORY + "synteny/skipped_sequences.info");
        StringBuilder skipped_builder = new StringBuilder();
        int[] sequencesPerGenome = GENOME_DB.num_sequences; // first position is always empty
        for (int i = 1; i <= GENOME_DB.num_genomes; i++) {
            for (int j = i; j <= sequencesPerGenome[i]; j++) {
                if (!selectedSequences.contains(i + "_" + j)) {
                    skipped_builder.append(i).append("_").append(j).append(",");
                }
            }
        }
        skipped_builder.append("\n");
        writeStringToFile(skipped_builder.toString(), WORKING_DIRECTORY + "synteny/skipped_sequences.info", false, false);
    }

    /**
     *
     * @param homologsOrSyntelogs String that is either "syntelogs" or "homologs"
     * @param allowWithinGenome allow sequence combinations to be made within a genome
     * @param selectedSequences sequence identifiers in the current user selection
     * @param selectedGenomes genome numbers for the current user selection
     * @return a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     */
    public static HashSet<String> retrieveSyntenySequenceCombinations(String homologsOrSyntelogs, boolean allowWithinGenome,
                                                                      LinkedHashSet<String> selectedSequences,
                                                                      LinkedHashSet<Integer> selectedGenomes) {
        HashSet<String> sequence_combinations = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (homologsOrSyntelogs.equals("syntelogs")) {
                ResourceIterator<Node> synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
                while (synteny_nodes.hasNext()) {
                    Node synteny_node = synteny_nodes.next();
                    String sequence_identifiers = (String) synteny_node.getProperty("sequence_identifiers");
                    String[] seq_combi_array = sequence_identifiers.split(",");
                    String[] seq_1_array = seq_combi_array[0].split("_");
                    String[] seq_2_array = seq_combi_array[1].split("_");
                    int genome1 = Integer.parseInt(seq_1_array[0]);
                    int seq1 = Integer.parseInt(seq_1_array[1]);
                    int genome2 = Integer.parseInt(seq_2_array[0]);
                    int seq2 = Integer.parseInt(seq_2_array[1]);
                    if (!allowWithinGenome && selectedGenomes.size() > 1 && genome1 == genome2) {
                        // don't calculate between same genome when multiple genomes were selected
                        continue;
                    }
                    if (!selectedSequences.contains(genome1 + "_" + seq1) || !selectedSequences.contains(genome2 + "_" + seq2)) {
                        continue;
                    }
                    String seqIdcombination = sequence_identifiers.replace(",", "#");
                    sequence_combinations.add(seqIdcombination);
                }
            } else { // homologs
                ResourceIterator<Node> homology_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
                HashSet<String> unordered_combinations = new HashSet<>();
                while (homology_nodes.hasNext()) {
                    Node homology_node = homology_nodes.next();
                    Iterable<Relationship> relations = homology_node.getRelationships(RelTypes.has_homolog);
                    HashSet<String> sequence_identifiers = new HashSet<>();
                    for (Relationship rel : relations) {
                        Node mrna_node = rel.getEndNode();
                        int[] address = (int[]) mrna_node.getProperty("address");
                        int genome_nr = address[0];
                        int sequence_nr = address[1];
                        if (!selectedSequences.contains(genome_nr + "_" +sequence_nr)) {
                            continue;
                        }
                        sequence_identifiers.add(address[0] + "_" + address[1]);
                    }
                    //System.out.println(sequence_identifiers.size());
                    for (String seq1_id : sequence_identifiers) {
                        boolean skip_lines = true;
                        for (String seq2_id : sequence_identifiers) {
                            if (seq2_id.equals(seq1_id)) {
                                skip_lines = false;
                            } else if (!skip_lines) {
                                unordered_combinations.add(seq1_id + " " + seq2_id);
                            }
                        }
                    }
                }
                //System.out.println(unordered_combinations);
                for (String seq_combi : unordered_combinations) {
                    String[] seq_array = seq_combi.split(" ");
                    String[] ordered_seq_array = orderTwoSequenceIdentifiers(seq_array[0], seq_array[1]);
                    sequence_combinations.add(ordered_seq_array[0] + "#" + ordered_seq_array[1]);
                }
            }
            tx.success();
        }
        return sequence_combinations;
    }

    /**
     * Combines all pairwise .collinearity files from the parallel run into a single file
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     */
    private void combineMcscanxParallelFiles(ArrayList<String> sequenceCombinations) {
        HashSet<String> syntenic_genes_set = new HashSet<>();
        int counter = 0, seqCombiCounter = 0;
        StringBuilder header_builder = new StringBuilder();
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "synteny/temp.collinearity"))) {
            int lineCounter = 0;
            for (String seq_combi : sequenceCombinations) {
                seqCombiCounter++;
                if (seqCombiCounter == 1 || seqCombiCounter % 100 == 0 || seqCombiCounter == sequenceCombinations.size()) {
                    System.out.print("\r Combining files into a single .collinearity file: " + seqCombiCounter + "/" + sequenceCombinations.size() + "   ");
                }
                String renamed_seq_combi = seq_combi.replace("_","S").replace("#","_");
                String line;
                try (BufferedReader in = new BufferedReader(new FileReader(WD_full_path + "synteny/sequence_combinations/" + renamed_seq_combi + "/mcscanx.collinearity"))) {
                    boolean print = false;
                    while ((line = in.readLine()) != null) {
                        line = line.trim();
                        if (lineCounter < 7) { // take the mcscanx parameters and put them in the header
                            header_builder.append(line).append("\n");
                            lineCounter++;
                        }

                        if (line.startsWith("## Alignment")) {
                            print = true;
                            String[] line_array = line.split(":");
                            out.write("## Alignment " + counter + ":" + line_array[1] + "\n");
                            counter++;
                            continue;
                        } else if (!line.startsWith("#")) {
                            String[] line_array = line.split("\t");
                            syntenic_genes_set.add(line_array[1]);
                            syntenic_genes_set.add(line_array[2]);
                        }
                        if (print) {
                            out.write("  " + line + "\n");
                        }
                    }
                } catch (IOException ioe) {
                    // file does not exists but that's OK, do nothing
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write temporary files: " + WORKING_DIRECTORY + "synteny/temp.collinearity");
        }
        System.out.println("");

        String percentageTotal = percentageAsString(syntenic_genes_set.size(), mrnasInAnalysis.get(), 2);
        String percentageHomologs = percentageAsString(syntenic_genes_set.size(), homologousMrnasInAnalysis .get(), 2);
        header_builder.append("############### Statistics ###############\n")
                .append("# Number of collinear genes: ") .append(syntenic_genes_set.size()) .append(", Percentage: ") .append(percentageTotal).append("\n")
                .append("# Number of all genes: ").append(mrnasInAnalysis.get()).append("\n")
                .append("##########################################\n");

        Pantools.logger.info("Collinear genes based on all genes       : " + percentageTotal + "%");
        Pantools.logger.info("Collinear genes based on homologous genes: " + percentageHomologs + "%");
        writeStringToFile(header_builder.toString(), WORKING_DIRECTORY + "synteny/mcscanx.collinearity", false, false);
        append_file_to_other_file(WORKING_DIRECTORY + "synteny/temp.collinearity", WORKING_DIRECTORY + "synteny/mcscanx.collinearity");
        delete_file_full_path(WORKING_DIRECTORY + "synteny/temp.collinearity");
    }

    /**
     *
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     */
    private void prepareMcscanxParallelRun(ArrayList<String> sequenceCombinations) {
        sequenceCombinationQueue = new LinkedBlockingQueue<>();
        comparisonCounter = new AtomicLong(0);
        for (String sequenceCombi : sequenceCombinations) {
            sequenceCombi = sequenceCombi.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
            sequenceCombinationQueue.add(sequenceCombi);
        }
        int threads_to_use = THREADS;
        if (sequenceCombinations.size() < threads_to_use) {
            threads_to_use = sequenceCombinations.size();
        }

        ArrayList<String> list = new ArrayList<>();
        List<String> synlist = Collections.synchronizedList(list); // holds sequence combinations that are currently being analyzed by MCScanX
        try {
            ExecutorService es = Executors.newFixedThreadPool(threads_to_use);
            for (int i = 1; i <= threads_to_use; i++) {
                sequenceCombinationQueue.add("stop"); // add a stop string for every thread
                es.execute(new run_mcscanx_parallel(synlist, sequenceCombinations.size()));
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Something went wrong while running MCScanX");
        }
    }

    /**
     *
     */
    private class run_mcscanx_parallel implements Runnable {
        List<String> synlist; // holds sequence combinations that are currently being analyzed by MCScanX
        int total_comparisons; // total number of sequence combinations

        private run_mcscanx_parallel(List<String> synlist, int total_comparisons) {
            this.synlist = synlist;
            this.total_comparisons = total_comparisons;
        }

        public void run() {
            String sequence_combi = "";
            while (!sequence_combi.equals("stop")) {
                try {
                    sequence_combi = sequenceCombinationQueue.take();
                } catch(InterruptedException e) {
                    break;
                }

                if (sequence_combi.equals("stop")) {
                    continue;
                }
                String renamed_id = sequence_combi.replace("_","-").replace("G","").replace("S","_");
                synlist.add(renamed_id);
                String command ="MCScanX_h " + WD_full_path + "synteny/sequence_combinations/" + sequence_combi + "/mcscanx";
                System.out.print("\r Running MCScanX: " + comparisonCounter.incrementAndGet() + "/" + total_comparisons + " ");
                Pantools.logger.debug("MCSCANX command: " + command);
                runCommand(command);
                synlist.remove(renamed_id);
            }
            System.out.print("\r Running final MCScanX comparisons: " + synlist.size() + "                           "); // spaces are intentional
            if (synlist.isEmpty()) {
                System.out.println("");
                Pantools.logger.info("Finished running MCScanX!");
            }
        }

        /**
         * Execute the MCScanX command
         */
        private void runCommand(String command) {
            List<String> cmd = new ArrayList<>();
            cmd.add("sh");
            cmd.add("-c");
            cmd.add(command);
            ProcessBuilder pb = new ProcessBuilder(cmd);
            try {
                Process p = pb.start();
                p.waitFor(3600, TimeUnit.SECONDS);
                if (p.exitValue() > 0) {
                    final BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line;
                    while ((line = input.readLine()) != null){
                        Pantools.logger.warn(line);
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException("crashed: " + command);
            }
        }
    }

    /**
     * Check which genes are homologous. Genes must be in same homology group and have 'is_similar_to' relationship
     *
     */
    private class findHomologsBetweenSequences implements Runnable {
        int thread_nr;
        HashMap<String, HashSet<Node>> homolog_counts_per_seq_and_combi;
        HashMap<String, BufferedWriter> homologyFileWriters;

        private findHomologsBetweenSequences(int thread_nr, HashMap<String, HashSet<Node>> homolog_counts_per_seq_and_combi,
                                             HashMap<String, BufferedWriter> homologyFileWriters) {
            this.thread_nr = thread_nr;
            this.homolog_counts_per_seq_and_combi = homolog_counts_per_seq_and_combi;
            this.homologyFileWriters = homologyFileWriters;
        }

        public void run() {
            HashMap<String, HashMap<String, HashSet<Node>>> homologsPerSequenceCombi = new HashMap<>();
            // map holds another map to avoid exponential growth of sequence combination.
            // key of first map is a genome combination:  1#1, 1#2
            // key of inner map is a sequence combination: 1_1#1_2, 1_2#2_1

            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                Node homologyNode = GRAPH_DB.getNodeById(0);
                while (homologyNode != null) {
                    try {
                        homologyNode = nodeQueue.take();
                    } catch (InterruptedException e) {
                        break;
                    }

                    if (homologyNode.getId() == 0) {
                        break;
                    }
                    if (nodeQueue.size() % 10 == 0) {
                        System.out.print("\r Gathering homology groups: " + nodeQueue.size() + "  ");
                    }

                    Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
                    for (Relationship rel : relationships) {
                        Node mrnaNodeA = rel.getEndNode();
                        int[] address = (int[]) mrnaNodeA.getProperty("address");
                        int genomeNr1 = address[0];
                        int sequenceNr1 = address[1];
                        String sequenceIdA = genomeNr1 + "_" + sequenceNr1;
                        if (skip_array[genomeNr1 - 1] || skip_seq_array[genomeNr1 - 1][sequenceNr1 - 1]) {
                            continue;
                        }
                        String proteinId = (String) mrnaNodeA.getProperty("protein_ID");
                        proteinId = removeMcscanDisassolowedCharacters(proteinId);
                        mrnasInAnalysis.incrementAndGet();
                        //boolean gene_has_homolog = false;
                        Iterable<Relationship> is_similar_relations = mrnaNodeA.getRelationships(RelTypes.is_similar_to);
                        for (Relationship rel2 : is_similar_relations) {
                            Node mrnaNodeB = rel2.getEndNode();
                            if (mrnaNodeB.getId() == mrnaNodeA.getId()) {
                                mrnaNodeB = rel2.getStartNode();
                            }
                            int[] address2 = (int[]) mrnaNodeB.getProperty("address");
                            int genomeNr2 = address2[0];
                            int sequenceNr2 = address2[1];
                            String sequenceIdB = genomeNr2 + "_" + sequenceNr2;

                            if (genomeNr1 == genomeNr2 && sequenceNr1 == sequenceNr2) { // same sequence, no synteny is inferred between itself
                                continue;
                            }
                            if (!compare_sequences && genomeNr1 == genomeNr2) { // when comparing between genomes, no sequence combinations within genome
                                continue;
                            }

                            if (skip_seq_array[genomeNr2 - 1][sequenceNr2 - 1] || skip_array[genomeNr2 - 1]) {
                                continue;
                            }

                            Relationship hm_rel = mrnaNodeB.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                            Node homologyNode2 = hm_rel.getStartNode();
                            if (homologyNode2.getId() != homologyNode.getId()) { // proteins are not part of the same homology group
                                continue;
                            }

                            if (genomeNr2 < genomeNr1 || (genomeNr2 == genomeNr1 && sequenceNr2 < sequenceNr1)) {
                                //if (genomeNr2 > genomeNr1) {
                                    //System.out.println("continue " + genomeNr1 + "_" + sequenceNr1 + " " + genomeNr2+ "_" + sequenceNr2);
                                //}
                                continue; // the mrna node was already added
                            }

                            String proteinIdB = (String) mrnaNodeB.getProperty("protein_ID");
                            proteinIdB = removeMcscanDisassolowedCharacters(proteinIdB);


                            //gene_has_homolog = true;
                            HashMap<String, HashSet<Node>> innerMap = homologsPerSequenceCombi.get(genomeNr1 + "#" + genomeNr2);
                            if (innerMap == null) {
                                innerMap = new HashMap<>();
                            }

                            // seqA has a homologous geneA
                            innerMap.computeIfAbsent(sequenceIdA, k -> new HashSet<>()).add(mrnaNodeA);
                            innerMap.computeIfAbsent(sequenceIdB, k -> new HashSet<>()).add(mrnaNodeB);


                            // seqA homologous geneA is homologous to seqB
                            innerMap.computeIfAbsent(sequenceIdA + "#" + sequenceIdB + "#" + sequenceIdA, k -> new HashSet<>()).add(mrnaNodeA);
                            innerMap.computeIfAbsent(sequenceIdA + "#" + sequenceIdB + "#" + sequenceIdB, k -> new HashSet<>()).add(mrnaNodeB);

                            homologsPerSequenceCombi.put(genomeNr1 + "#" + genomeNr2, innerMap);
                            String line = genomeNr1 + "_" + sequenceNr1 + "#" + proteinId + "\t"
                                    + genomeNr2 + "_" + sequenceNr2 + "#" + proteinIdB + "\n";

                            if (homologyFileWriters != null) {
                                writeMcscanHomology2(homologyFileWriters, "main", line);
                            }

                            if (homologyFileWriters != null && homologyFileWriters.size() > 1) {
                                if (!homologyFileWriters.containsKey(genomeNr1 + "_" + sequenceNr1 + "#" + genomeNr2 + "_" + sequenceNr2)){
                                    Pantools.logger.warn("Something might be off here, please contact developers: {}_{}#{}_{}", genomeNr1, sequenceNr1, genomeNr2, sequenceNr2);
                                }
                                writeMcscanHomology2(homologyFileWriters,
                                        genomeNr1 + "_" + sequenceNr1 + "#" + genomeNr2 + "_" + sequenceNr2, line);
                            }
                        }

                        //if (gene_has_homolog) {
                            //homologs_per_seq_and_combi.computeIfAbsent(genomeNr1 + "_" + sequenceNr1, k -> new HashSet<>()).add(mrnaNode);
                        //}

                    }
                }
                tx.success();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // combine homologsPerSequenceCombi of all parralel threads into a single map
            combineHashMaps(homologsPerSequenceCombi, homolog_counts_per_seq_and_combi, thread_nr);
        }
    }

    private synchronized void writeMcscanHomology2(HashMap<String, BufferedWriter> homologyFileWriters, String key, String line) throws IOException {
        BufferedWriter file = homologyFileWriters.get(key);
        file.write(line);
    }



    private synchronized void combineHashMaps(HashMap<String, HashMap<String, HashSet<Node>>> homologsPerSequenceCombi,
                                              HashMap<String, HashSet<Node>> homolog_counts_per_seq_and_combi, int thread_nr) {

        //.out.print("\rCombining homologs:                  ");
        for (String genomeCombination : homologsPerSequenceCombi.keySet()) {
            //System.out.println("genome " +  genomeCombination);
            HashMap<String, HashSet<Node>> innerMap = homologsPerSequenceCombi.get(genomeCombination);
            for (String sequenceKey : innerMap.keySet()) {
                //System.out.println(" " + sequenceKey);
                if (sequenceKey.contains("#")) { // its a sequence combination
                    String[] seq_combi_array = sequenceKey.split("#"); // example is 1_1#2_1#1_1 becomes [ 1_1, 2_1, 1_1]
                    String sequenceCombination = seq_combi_array[0] + "#" + seq_combi_array[1];
                    String[] seq1_array = seq_combi_array[0].split("_"); // example is 1_1, becomes [1,1]
                    String[] seq2_array = seq_combi_array[1].split("_"); // example is 2_1, becomes [2,1]
                    String key1 = sequenceCombination + "#" + seq_combi_array[0];
                    String key2 = sequenceCombination + "#" + seq_combi_array[1];
                    //System.out.println(key1 + " " + key2);

                    String genome_key1 = seq1_array[0] + "#" + seq2_array[0] + "#" + seq1_array[0];
                    String genome_key2 = seq1_array[0] + "#" + seq2_array[0] + "#" + seq2_array[0];
                    //System.out.println(seq_combination + " " + homologs_per_seq_and_combi.get(key1).size() + " " + homologs_per_seq_and_combi.get(key2).size());
                    HashSet<Node> nodes1 = innerMap.get(key1);
                    for (Node node : nodes1) {
                        homolog_counts_per_seq_and_combi.computeIfAbsent(sequenceCombination + "#" + seq_combi_array[0], k -> new HashSet<>()).add(node);
                        homolog_counts_per_seq_and_combi.computeIfAbsent(genome_key1, k -> new HashSet<>()).add(node);
                    }
                    HashSet<Node> nodes2 = innerMap.get(key2);
                    for (Node node : nodes2) {
                        homolog_counts_per_seq_and_combi.computeIfAbsent(sequenceCombination + "#" + seq_combi_array[1], k -> new HashSet<>()).add(node);
                        homolog_counts_per_seq_and_combi.computeIfAbsent(genome_key2, k -> new HashSet<>()).add(node);
                    }

                } else { // its a single sequence
                    HashSet<Node> nodes = innerMap.get(sequenceKey);
                    //System.out.println(seq_id + " " + nodes_of_seq.size());
                    String[] seq_array = sequenceKey.split("_");
                    for (Node node : nodes) {
                        homolog_counts_per_seq_and_combi.computeIfAbsent(seq_array[0], k -> new HashSet<>()).add(node); // total for genome
                        homolog_counts_per_seq_and_combi.computeIfAbsent(sequenceKey, k -> new HashSet<>()).add(node);
                    }
                }
            }
        }
        //System.out.println("out " + thread_nr);
    }

    /**
     * for mcscanx, Sequences from the same genome must get a different letter combination or else they are not compared.
     * @param runMcscanx MCScanX will be executed by the 'calculate_synteny' function. Requires preparation of pairwise files per sequence.
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     */
    private void createMcscanxGffForSequences(ArrayList<String> sequenceCombinations, boolean runMcscanx) {
        HashMap<String, StringBuilder> addresses_per_seq = new HashMap<>();
        StringBuilder info_builder = new StringBuilder();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            boolean uniqueLetterPerGenome = checkIfFirstLetterCanBeUniquePerGenome();
            int[] seqCounterPerGenome = new int[GENOME_DB.num_genomes]; //counter for when unique first letter per genome is possible
            AtomicInteger sequenceCounter = new AtomicInteger(0); //  counter if it is NOT possible
            try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "synteny/mcscanx.gff"))) {
                for (int genomeNr : selectedGenomes) {
                    System.out.print("\r Preparing .gff input: Genome " + genomeNr  + "   ");
                    ArrayList<Node> sequenceNodes = seqNodesInChromosomePhasingOrder(genomeNr);
                    for (Node sequenceNode : sequenceNodes) {
                        String sequenceId = (String) sequenceNode.getProperty("identifier");
                        String[] seqIdArray = sequenceId.split("_"); // 1_1 becomes [1,1]
                        int sequenceNr = Integer.parseInt(seqIdArray[1]);
                        String mcscanxId = determineIdForMcscanx(uniqueLetterPerGenome, seqCounterPerGenome, sequenceCounter, genomeNr);
                        String phasingInformation = obtainChromosomeInformation(genomeNr, sequenceNr);
                        info_builder.append(genomeNr).append("_").append(sequenceNr).append(",").append(mcscanxId).append("1").append(phasingInformation).append("\n");
                        ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genomeNr, "sequence", sequenceNr);
                        while (mrna_nodes.hasNext()) {
                            Node mrna_node = mrna_nodes.next();
                            if (!mrna_node.hasProperty("protein_ID")) {
                                continue;
                            }
                            if (longest_transcripts && !mrna_node.hasProperty("longest_transcript")) {
                                continue;
                            }
                            String protein_id = (String) mrna_node.getProperty("protein_ID");
                            protein_id = removeMcscanDisassolowedCharacters(protein_id);
                            int[] address = (int[]) mrna_node.getProperty("address");
                            out.write(mcscanxId + "1\t" + genomeNr + "_" + sequenceNr +"#" + protein_id + "\t" + address[2] + "\t" + address[3] + "\n");
                            addresses_per_seq.computeIfAbsent(genomeNr + "_" + sequenceNr, k -> new StringBuilder()).append(mcscanxId + "1\t" + genomeNr + "_" + sequenceNr + "#" + protein_id + "\t" + address[2] + "\t" + address[3] + "\n");
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to write to: " + WORKING_DIRECTORY + "synteny/mcscanx.gff");
            }

            tx.success();
        }
        if (runMcscanx) {
            createMcscanxGffSequence(sequenceCombinations, addresses_per_seq);
        }
        System.out.println("");
        String header = "#PanTool sequence identifiers,MCScanX identifier\n";
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
            header = "#PanTool sequence identifiers,MCScanX identifier,chromosome,subgenome\n";
        }
        writeStringToFile(header + info_builder.toString(), WORKING_DIRECTORY + "synteny/synteny_identifiers.csv", false, false);

    }

    private String obtainChromosomeInformation(int genomeNr, int sequenceNr) {
        String phasingInformation = "";
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) { // get chromosome number and phasing letter
            String[] phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr); // example [1, B, 1B, 1_B]
            if (phasingInfo == null) {
                phasingInformation = ",unphased,unphased";
            } else {
                phasingInformation = "," + phasingInfo[0] + "," + phasingInfo[1];
            }
        }
        return phasingInformation;
    }

    private ArrayList<Node> seqNodesInChromosomePhasingOrder(int genomeNr) {
        HashMap<String, Node> sequenceNodeMap = new HashMap<>(); // key is sequence identifier
        ArrayList<String> sequenceIdentifiers = new ArrayList<>();
        ArrayList<Node> sequenceNodeList = new ArrayList<>();
        ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genomeNr);
        while (sequenceNodes.hasNext()) {
            Node sequenceNode = sequenceNodes.next();
            String sequenceId = (String) sequenceNode.getProperty("identifier");
            String[] idArray = sequenceId.split("_"); // 1_1 becomes [1,1]
            int sequenceNr = Integer.parseInt(idArray[1]);
            if (!selectedSequences.contains(genomeNr  + "_" + sequenceNr)) {
                continue;
            }
            sequenceNodeMap.put(sequenceId, sequenceNode);
            sequenceIdentifiers.add(sequenceId);
            sequenceNodeList.add(sequenceNode);
        }

        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return sequenceNodeList;
        }

        ArrayList<Node> orderedNodes = new ArrayList<>();
        sequenceIdentifiers = pf.sequencesInChromosomePhasingOrder(sequenceIdentifiers);
        for (String sequenceId : sequenceIdentifiers) {
            Node sequenceNode = sequenceNodeMap.get(sequenceId);
            orderedNodes.add(sequenceNode);
        }
        return orderedNodes;
    }

    /**
     * Generate the two letters for the new sequence ID. Depends on whether the first letter can be unique for each genome
     * @param uniqueLetterPerGenome
     * @param seqCounterPerGenome
     * @param seq_counter
     * @param genomeNr
     * @return
     */
    private String determineIdForMcscanx(boolean uniqueLetterPerGenome, int[] seqCounterPerGenome, AtomicInteger seq_counter, int genomeNr) {
        String mcscanxId = "";
        if (uniqueLetterPerGenome) {
            String letter1 = letters[genomeNr-1];
            String letter2 = letters[seqCounterPerGenome[genomeNr-1]];
            mcscanxId = letter1 + letter2;
            seqCounterPerGenome[genomeNr-1]++;
        } else {
           mcscanxId = two_letter_combis[seq_counter.get()];
           seq_counter.incrementAndGet();
        }
        return mcscanxId;
    }

    /**
     * first 3 options are with mcscan identifiers already present
     *  // make functions that counts how many sequences are included in the analysis
     // optie 0, kan niet!
     // optie 1, kan mooi, eerste letter voor genoom, 2e voor sequentie, elke is dus max 26
     // optie 2, kan niet mooi, wel minder dan 676 sequenties
     @return int
     */
    private boolean checkIfFirstLetterCanBeUniquePerGenome() {
        boolean below_27_sequences = true;
        boolean below_27_genomes = true;
        if (selectedGenomes.size() > 26) {
            below_27_genomes = false;
        }

        int[] sequencesPerGenome = GENOME_DB.num_sequences;
        for (int genomeNr : selectedGenomes) {
            int seqCounter = 0;
            for (int j = 1; j <= sequencesPerGenome[genomeNr]; j++) {
                if (selectedSequences.contains(genomeNr + "_" + j)) {
                    seqCounter++;
                }
            }

            if (seqCounter > 26) {
                below_27_sequences = false;
            }
        }

        if (below_27_sequences && below_27_genomes) {
            return true;
        }
        return false;
    }

    /**
     * @param sequenceCombinations a list with sequence combinations. A combination consists of SeqIdA + "#" SeqIdB. Example 1_1#1_2
     * @param runMcscanx MCScanX will be executed by the 'calculate_synteny' function. Requires preparation of pairwise files per sequence.
     * Currently takes EVERY annotation
     * All sequences of a genome will have the same two letters, therefore mcscanx will not compare them
     */
    private void createMcscanxGffForGenomes(ArrayList<String> sequenceCombinations, boolean runMcscanx) {
        StringBuilder info_builder = new StringBuilder();
        ArrayList<Integer> more_than_thousand = new ArrayList<>();
        ArrayList<Integer> more_than_hundred = new ArrayList<>();
        ArrayList<Integer> selectedGenomesList = new ArrayList<>(selectedGenomes);
        HashMap<String, StringBuilder> addresses_per_seq = new HashMap<>();
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "synteny/mcscanx.gff"))) {
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                int[] sequencesPerGenome = GENOME_DB.num_sequences; // IMPORTANT array size is total genomes + 1 . first position is always empty!
                for (int i = 0; i < selectedGenomesList.size(); i++) {
                    int genomeNr = selectedGenomesList.get(i);
                    String genome_id = two_letter_combis[i];
                    int sequence_counter = 0;
                    for (int j = 1; j <= sequencesPerGenome[genomeNr]; j++) { // go over the total number of sequences of a genome
                        if (!selectedSequences.contains(genomeNr + "_" + j)) { // sequence is not in user selection
                            continue;
                        }
                        if (j % 10 == 0) {
                            System.out.print("\r Preparing gff input: Genome " + genomeNr + ", sequence " + j + " ");
                        }
                        sequence_counter++;
                        String phasingInformation = obtainChromosomeInformation(genomeNr, j);

                        info_builder.append(genomeNr).append("_").append(j).append(",").append(genome_id).append(j).append(phasingInformation).append("\n");
                        ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genomeNr, "sequence", j);
                        while (mrna_nodes.hasNext()) {
                            Node mrna_node = mrna_nodes.next();
                            if (!mrna_node.hasProperty("protein_ID")) {
                                continue;
                            }
                            if (longest_transcripts && !mrna_node.hasProperty("longest_transcript")) {
                                continue;
                            }
                            String protein_id = (String) mrna_node.getProperty("protein_ID");
                            protein_id = removeMcscanDisassolowedCharacters(protein_id);
                            int[] address = (int[]) mrna_node.getProperty("address");
                            out.write(genome_id + j + "\t" + genomeNr + "_" + j + "#" + protein_id + "\t" + address[2] + "\t" + address[3] + "\n");
                            addresses_per_seq.computeIfAbsent(genomeNr + "_" + j, k -> new StringBuilder()).append(genome_id + j + "\t" + genomeNr + "_" + j + "#" + protein_id + "\t" + address[2] + "\t" + address[3] + "\n");
                        }
                    }

                    if (sequence_counter > 999) {
                        more_than_thousand.add(i); // "More than 999 sequences, accusyn cannot visualize it
                    } else if (sequence_counter > 99) {
                        more_than_hundred.add(i); // More than 99 sequences, synvisio cannot visualize it
                    }

                }
                tx.success();
            }
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong while writing to: " + WORKING_DIRECTORY + "synteny/mcscanx.gff");
        }

        if (runMcscanx) {
            createMcscanxGffSequence(sequenceCombinations, addresses_per_seq);
        }
        System.out.println("");

        // use the gff lines gathered from the previous loop to create a gff per genome combination

        String header = createHeaderForSyntenyIdentifiers(more_than_thousand, more_than_hundred);
        writeStringToFile(header + info_builder, WORKING_DIRECTORY + "synteny/synteny_identifiers.csv", false, false);
    }


    private String removeMcscanDisassolowedCharacters(String proteinId) {
        return proteinId.replace(":","").replace("-","_").replace("|","_");
    }

    private String createHeaderForSyntenyIdentifiers(ArrayList<Integer> more_than_thousand, ArrayList<Integer> more_than_hundred) {
        String header = "#PanTools sequence identifiers must be changed for MCScanX. Requirements are two letters with a number. " +
                "All sequences of a genome get the same letter combination, therefore we (currently) cannot handle more than " + two_letter_combis.length+ " genomes.\n";
        if (more_than_hundred.size() > 0) {
            header += "#These genomes have more than 100 sequences and are unusable by Synvisio: " + more_than_hundred + "\n";
        }
        if (more_than_thousand.size() > 0) {
            header += "#In addition, genomes having over 1000 sequences that are also unusable by Accusyn: " + more_than_thousand + "\n";
        }
        if (more_than_hundred.size() > 0 || more_than_thousand.size() > 0) {
            header += "\n";
        }
        if (PHASED_ANALYSIS) {
            header += "#PanTool sequence identifiers,MCScanX identifier,chromosome,subgenome\n";
        }else {
            header += "#PanTool sequence identifiers,MCScanX identifier\n";
        }
        return header;
    }

    /**
     * use the gff lines gathered from the previous loop to create a gff per sequence combination
     *
     * @param sequenceCombinations
     * @param addresses_per_seq
     */
    private void createMcscanxGffSequence(ArrayList<String> sequenceCombinations,
                                                HashMap<String, StringBuilder> addresses_per_seq) {
        int seqCounter = 0;
        int totalSeqCombis = sequenceCombinations.size();
        for (String seqCombi : sequenceCombinations) {
            seqCounter++;
            if (seqCounter % 100 == 0 || seqCounter == totalSeqCombis) {
                System.out.print("\r Writing GFF input files: " + seqCounter + "/" + totalSeqCombis + "          ");
            }
            String[] seqArray = seqCombi.split("#");
            String renamedSequenceCombi = seqCombi.replace("_", "S").replace("#", "_");
            createDirectory(WORKING_DIRECTORY + "synteny/sequence_combinations/" + renamedSequenceCombi, false);
            writeStringToFile(addresses_per_seq.get(seqArray[0]) + "" + addresses_per_seq.get(seqArray[1]),
                    WORKING_DIRECTORY + "synteny/sequence_combinations/" + renamedSequenceCombi + "/mcscanx.gff", false, false);
        }
    }

    /**
     * Reads a .collinearity file (MCSCanX) to add synteny block in the pangenome
     *
     * Requires
     * --database-path
     * --input-file a .collinearity file
     */
    public void addSynteny(Path collinearityFile) {
        Pantools.logger.info("Adding synteny information to the pangenome using a .collinearity (or .synteny) file.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            stop_if_panproteome("add_synteny"); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // sets grouping_version and longest_transcripts
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            System.exit(1);
        }

        // use the positions to also add information to genes that are part of a block
        //HashMap<String, Node[]> mrnaNodesPerSequence = get_ordered_mrna_per_seq(false, true, null); // key is sequence identifier, value is array with 'gene' nodes
        //HashMap<Node, Integer> mrna_position_map = create_mrna_position_map(mrnaNodesPerSequence);

        MAX_TRANSACTION_SIZE = 100000; // 100k actions before commit
        removeSyntenyRelationsbetweenMrnas();
        removeSyntenyNodes();
        add_synteny_information(collinearityFile);

        Pantools.logger.info("Overview written to:");
        Pantools.logger.info(" {}synteny/synteny_nodes.csv", WORKING_DIRECTORY);
    }

    /**
     * 1. Removes all synteny nodes and every relationship to these nodes
     * 2. Removes the 'is_syntenic_to' relationships between mRNA nodes
     */
    private void removeSyntenyNodes() {
        int removed_block_counter = 0;
        int trsc = 0;
        // 1/2 Remove all synteny nodes and every relationship to these nodes
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            ResourceIterator<Node> synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
            while (synteny_nodes.hasNext()) {
                Node synteny_node = synteny_nodes.next();
                Iterable<Relationship> relations = synteny_node.getRelationships();
                for (Relationship rel : relations) {
                    rel.delete();
                    trsc++;
                }
                synteny_node.delete();
                removed_block_counter++;

                System.out.print("\rRemoving previous synteny blocks: " + removed_block_counter);
                trsc++;
                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
                }
            }
            tx.success();
        } finally {
            tx.close();
        }

        if (removed_block_counter > 0) {
            System.out.println("\rRemoved the nodes and edges of " + removed_block_counter + " synteny blocks from a previous run");
        }
    }

    /**
     *  Removes the 'is_syntenic_to' relationships between mRNA nodes
     */
    private void removeSyntenyRelationsbetweenMrnas() {
        long removed_rel_counter = 0;
        int trsc = 0;
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "is_syntenic", true);
            while (mrna_nodes.hasNext()) {
                Node mrna_node = mrna_nodes.next();
                mrna_node.removeProperty("is_syntenic");
                Iterable<Relationship> relations = mrna_node.getRelationships(RelTypes.is_syntenic_to);
                for (Relationship rel : relations) {
                    rel.delete();
                    trsc++;
                    removed_rel_counter++;
                    if (removed_rel_counter % 1000 == 0 || removed_rel_counter == 1) {
                        System.out.print("\rRemoving 'is_syntenic' relationships: " + removed_rel_counter + "  ");
                    }
                }

                if (trsc >= MAX_TRANSACTION_SIZE) {
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "is_syntenic", true);
                }
            }
            tx.success();
        } finally {
            tx.close();
        }
    }

    /**
     * Expects MSCScanX collinearity file
     *
     * Example file
     * ############### Parameters ###############
     * # MATCH_SCORE: 50
     * # MATCH_SIZE: 5
     * # GAP_PENALTY: -1
     * # OVERLAP_WINDOW: 5
     * # E_VALUE: 1e-05
     * # MAX GAPS: 25
     * ############### Statistics ###############
     * # Number of collinear genes: 43, Percentage: 97.73
     * # Number of all genes: 44
     * ##########################################
     * ## Alignment 0: score=550.0 e_value=3.5e-32 N=11 ab1&ad1 plus
     *   0-  0:    2_1#gene_GU280_gp01 4_1#gene_GU280_gp01       0
     *   0-  1:    2_1#gene_GU280_gp02 4_1#gene_GU280_gp02       0
     *   0-  2:    2_1#gene_GU280_gp03 4_1#gene_GU280_gp03       0
     *   0-  3:    2_1#gene_GU280_gp04 4_1#gene_GU280_gp04       0
     *   0-  4:    2_1#gene_GU280_gp05 4_1#gene_GU280_gp05       0
     *   0-  5:    2_1#gene_GU280_gp06 4_1#gene_GU280_gp06       0
     *   0-  6:    2_1#gene_GU280_gp07 4_1#gene_GU280_gp07       0
     *   0-  7:    2_1#gene_GU280_gp08 4_1#gene_GU280_gp08       0
     *   0-  8:    2_1#gene_GU280_gp09 4_1#gene_GU280_gp09       0
     *   0-  9:    2_1#gene_GU280_gp10 4_1#gene_GU280_gp10       0
     *   0- 10:    2_1#gene_GU280_gp11 4_1#gene_GU280_gp11       0
     * ## Alignment 1: score=550.0 e_value=3.5e-32 N=11 ab1&ac1 plus
     *   0-  0:    2_1#gene_GU280_gp01 3_1#gene_GU280_gp01       0
     *   0-  1:    2_1#gene_GU280_gp02 3_1#gene_GU280_gp02       0
     *   0-  2:    2_1#gene_GU280_gp03 3_1#gene_GU280_gp03       0
     *   0-  3:    2_1#gene_GU280_gp04 3_1#gene_GU280_gp04       0
     *   0-  4:    2_1#gene_GU280_gp05 3_1#gene_GU280_gp05       0
     *   0-  5:    2_1#gene_GU280_gp06 3_1#gene_GU280_gp06       0
     *   0-  6:    2_1#gene_GU280_gp07 3_1#gene_GU280_gp07       0
     *   0-  7:    2_1#gene_GU280_gp08 3_1#gene_GU280_gp08       0
     *   0-  8:    2_1#gene_GU280_gp09 3_1#gene_GU280_gp09       0
     *   0-  9:    2_1#gene_GU280_gp10 3_1#gene_GU280_gp10       0
     *   0- 10:    2_1#gene_GU280_gp11 3_1#gene_GU280_gp11       0
     */
    private void add_synteny_information(Path collinearityFile) {
        StringBuilder synteny_node_csv = new StringBuilder("Block number,Synteny node identifier\n");
        int block_nr = 0;
        HashMap<String, Node> mrna_node_map = PhasedFunctionalities.retrieve_mrna_nodes(true);
        int trsc = 0;
        boolean first_gene_row = true; // use to first row of the block to extract the sequence identifiers
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            Node synteny_node = GRAPH_DB.getNodeById(0);
            try {
                BufferedReader br = Files.newBufferedReader(collinearityFile);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    line = line.trim();
                    String[] line_array, line_array2;

                    // Start of a syntenic block
                    if (line.startsWith("## Alignment")) { // example line '## Alignment 0: score=5767.0 e_value=0 N=116 ad2&ae15 minus'
                        line_array = line.split(": ");
                        line_array2 = line_array[0].split(" ");
                        String[] line_array3 = line_array[1].split(" ");
                        String mcscan_identifiers = line_array3[line_array3.length-2].replace("&",",");
                        String block_size_str = line_array3[line_array3.length-3].replace("N=","");

                        int block_size = Integer.parseInt(block_size_str);
                        if (trsc >= MAX_TRANSACTION_SIZE) {
                            tx.success();
                            tx.close();
                            trsc = 0;
                            tx = GRAPH_DB.beginTx(); // start a new database transaction
                        }
                        synteny_node = GRAPH_DB.createNode(SYNTENY_LABEL);
                        block_nr = Integer.parseInt(line_array2[2]);
                        synteny_node_csv.append(block_nr).append(",").append(synteny_node.getId()).append("\n");
                        synteny_node.setProperty("block_number", block_nr);
                        synteny_node.setProperty("block_size", block_size);
                        synteny_node.setProperty("sequence_identifiers_mcscanx", mcscan_identifiers);
                        boolean inverted = false;
                        if (line_array3[line_array3.length-1].equals("minus")) {
                            inverted = true;
                        }
                        synteny_node.setProperty("block_inverted", inverted);
                        if (block_nr < 10 || block_nr % 1000 == 0) {
                            System.out.print("\rAdding synteny blocks: " + block_nr);
                        }
                        trsc += 2; // 2 transactions are made in this IF statement
                        first_gene_row = true;
                    } else if (line.startsWith("#")) {
                        // comments/information on start of the file
                        // do nothing
                    } else { // synteny block information.
                        // Example line:  0-  0:    2_1#gene_GU280_gp01 3_1#gene_GU280_gp01       0
                        line_array = line.split("\t");
                        line_array2 = line_array[0].split("-");
                        String[] gene_id1 = line_array[1].split("#");
                        String[] gene_id2 = line_array[2].split("#");
                        Node mrna_node1 = mrna_node_map.get(line_array[1]);
                        Node mrna_node2 = mrna_node_map.get(line_array[2]);
                        if (mrna_node1 == null) {
                            throw new RuntimeException("Unable to find mRNA" + line_array[1] + ". Incompatible .collinearity file");
                        } else if (mrna_node2 == null) {
                            throw new RuntimeException("Unable to find mRNA" + line_array[2] + ". Incompatible .collinearity file");
                        }
                        if (first_gene_row) {
                            //System.out.println(gene_id1[0] + " " + gene_id2[0]);
                            synteny_node.setProperty("sequence_identifiers", gene_id1[0] + "," + gene_id2[0]);
                            first_gene_row = false;
                        }

                        boolean forward = orientation_of_relation(gene_id1[0], gene_id2[0], mrna_node1.getId(), mrna_node2.getId());
                        int position = Integer.parseInt(line_array2[1].replace(":","").replace(" ",""));
                        Relationship part_of_rel1 = mrna_node1.createRelationshipTo(synteny_node, RelTypes.part_of);
                        Relationship part_of_rel2 = mrna_node2.createRelationshipTo(synteny_node, RelTypes.part_of);
                        //System.out.println(mrna_node1 + " " + mrna_node2 + " " + line_array[1] + " " + line_array[2] + " rels " +  part_of_rel1 + " " + part_of_rel2);
                        part_of_rel1.setProperty("block_number", block_nr);
                        part_of_rel2.setProperty("block_number", block_nr);
                        part_of_rel1.setProperty("position", position);
                        part_of_rel2.setProperty("position", position);

                        if (!mrna_node1.hasProperty("is_syntenic")) {
                            mrna_node1.setProperty("is_syntenic", true);
                        }
                        if (!mrna_node2.hasProperty("is_syntenic")) {
                            mrna_node2.setProperty("is_syntenic", true);
                        }

                        Relationship is_syntenic_rel;
                        if (forward) {
                            is_syntenic_rel = mrna_node1.createRelationshipTo(mrna_node2, RelTypes.is_syntenic_to);
                        } else {
                            is_syntenic_rel = mrna_node2.createRelationshipTo(mrna_node1, RelTypes.is_syntenic_to);
                        }
                        is_syntenic_rel.setProperty("block_number", block_nr);
                        is_syntenic_rel.setProperty("position", position);
                        trsc += 12; // 12 transactions are made in this ELSE statement
                    }
                }
            }  catch (IOException ioe) {
                Pantools.logger.error("Failed to read: {}", INPUT_FILE);
                System.exit(1);
            }
            tx.success();
        } finally {
            tx.close();
        }
        block_nr++; // +1 because counting started at 0
        writeStringToFile(synteny_node_csv.toString(), WORKING_DIRECTORY + "synteny/synteny_nodes.csv", false, false);
        Pantools.logger.info("Added " + block_nr + " synteny blocks to the pangenome.");
    }

    /**
     * Creates new .gff andf .collinearity files for https://accusyn.usask.ca
     */
    public void analyze_blocks() {
        Pantools.logger.info("Analyzing synteny blocks in the pangenome.");
        pf = new PhasedFunctionalities();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // sets grouping_version and longest_transcripts
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            return;
        }

        if (INPUT_FILE == null) {
            Pantools.logger.info("No --input-file was given. Requires a .collinearity");
            //System.exit(1);
        } else if (!INPUT_FILE.endsWith(".collinearity")) {
            Pantools.logger.error("The --input-file must have a .collinearity file extension.");
            System.exit(1);
        }

        if (!longest_transcripts) {
            Pantools.logger.info("--longest transcripts is automatically included.");
        }

        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(100000, false);
        pf.preparePhasedGenomeInformation(true, selectedSequences);
        // homologs is lower than gene_classificatioon because genes require 'is_similar_to' relation
        HashSet<String> sequenceCombinations = check_which_sequences_have_synteny();



        HashMap<String, Integer> homologs_counts_per_seq_and_combi2 = startParralelFindHomologsBetweenSequences(null, null);


        HashMap<String, String> renamedIdentifiersMap = retrieveRenamedSyntentyIdentifiers(true);

        //HashMap<String, Integer> homologs_per_seq_and_combi = phased_functionalities.gather_homologs_between_sequences(); //DON'T USE THIS
        // new method without the files
        HashMap<String, Node[]> mrnaNodesPerSequence = pf.getOrderedMrnaNodesPerSequence(false, true, null); // key is sequence identifier, value is array with 'gene' nodes
        HashMap<Node, Integer> mrna_position_map = create_mrna_position_map(mrnaNodesPerSequence);
        HashMap<String, Integer> gene_position = transformMrnaNodetoStringMap(mrna_position_map);

        /*
        HashMap<String, Integer> countsPergenome = new HashMap<>(); // is a test
        for (String key : mrnaNodesPerSequence.keySet()) {
            String[] keyArray = key.split("_");
            countsPergenome.merge(keyArray[0], mrnaNodesPerSequence.get(key).length, Integer::sum);
        }
        for (String key :countsPergenome.keySet()) {
            System.out.println(key + " " + countsPergenome.get(key));
        }*/

        createDirectory("synteny/statistics", true);
        read_synteny_blocks(mrnaNodesPerSequence, mrna_position_map, homologs_counts_per_seq_and_combi2);

        if (INPUT_FILE == null) {
            Pantools.logger.error("no -if");
            System.exit(1);
        }

        //String gff_file = INPUT_FILE.replace("collinearity", "gff");
        String homology_file = INPUT_FILE.replace("collinearity", "homology");

        update_skip_array_based_on_collinear_file();
        //get_conserved_missing_genes_from_blocks(genes_per_sequences, gene_position);
        HashMap<String, ArrayList<String>> mrnas_per_sequences = new HashMap<>();
        HashMap<String, String> sequence_info_map = create_sequence_info_map(mrnas_per_sequences, renamedIdentifiersMap);
        // distinct genes of seq1, distinct genes of seq1, total homology relationships, total genes, average length , median length, shortest, longest
        HashMap<String, Integer> homolog_genes_statistics = count_homologous_genes_from_homology_file(homology_file);
        HashMap<String, int[]> mrna_address_map = PhasedFunctionalities.retrieve_gene_addresses(true);

        HashMap<String, String> gene_on_position = transformMrnaNodetoStringMap2(mrna_position_map, renamedIdentifiersMap);  // key is seq ID + "#" + gene position
        read_mcscan_collinear_file(mrna_address_map, renamedIdentifiersMap, sequence_info_map, gene_position, homolog_genes_statistics, gene_on_position);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}synteny_blocks_statistics.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny_blocks_overview.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/statistics/gene_frequency.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/statistics/genome_overlap.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/sequence_overlap_per_sequence.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/sequence_overlap_per_block.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}synteny/statistics/blocks_between_genomes.csv", WORKING_DIRECTORY);
    }

    public HashMap<String, String> retrieveRenamedSyntentyIdentifiers(boolean print) {
        HashMap<String, String> renamedIdentifiersMap = new HashMap<>();
        boolean synteny = checkIfFileExists(WORKING_DIRECTORY + "synteny/synteny_identifiers.csv");
        if (!synteny && print) {
            System.out.println("\rNo synteny blocks in the pangenome");
        } else { // only use renamed ids when synteny blocks are present
            renamedIdentifiersMap = read_renamed_identifiers_for_mcscanx_pt(WORKING_DIRECTORY + "synteny/synteny_identifiers.csv");
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                long synteny_nodes = count_nodes(SYNTENY_LABEL);
                if (synteny_nodes == 0) {
                    synteny = false;
                }
                tx.success();
            }
            if (synteny && print) {
                System.out.println("\rSynteny information found in the pangenome. "
                        + "Including renamed sequence identifiers: " + WORKING_DIRECTORY + "synteny/synteny_identifiers.csv");
            }
        }
        return renamedIdentifiersMap;
    }

    public static void increase_shared_genes_between_seqs(HashMap<String, HashSet<String>> shared_genes_between_seqs, String gene_id1, String gene_id2) {
        String[] gene_array = gene_id1.split("#");
        String[] gene_array2 = gene_id2.split("#");
        HashSet<String> shared_genes;
        if (shared_genes_between_seqs.containsKey(gene_array[0] + "#" + gene_array2[0])) {
            shared_genes = shared_genes_between_seqs.get(gene_array[0] + "#" + gene_array2[0]);
        } else {
            shared_genes = new HashSet<>();
        }
        //System.out.println(Arrays.toString(gene_array));
        shared_genes.add(gene_array[1]);
        shared_genes_between_seqs.put(gene_array[0] + "#" + gene_array2[0], shared_genes);
    }

    /**
     *
     * @param shared_genes_between_seqs
     * @param gene_id1
     * @param gene_id2
     */
    public static void increase_total_shared_genes_between_seqs(HashMap<String, ArrayList<String>> shared_genes_between_seqs, String gene_id1, String gene_id2) {
        String[] gene_array = gene_id1.split("#");
        String[] gene_array2 = gene_id2.split("#");
        ArrayList<String> shared_genes;
        if (shared_genes_between_seqs.containsKey(gene_array[0] + "#" + gene_array2[0])) {
            shared_genes = shared_genes_between_seqs.get(gene_array[0] + "#" + gene_array2[0]);
        } else {
            shared_genes = new ArrayList<>();
        }
        //System.out.println(Arrays.toString(gene_array));
        shared_genes.add(gene_array[1]);
        shared_genes_between_seqs.put(gene_array[0] + "#" + gene_array2[0], shared_genes);
    }

    public static void increase_frequencies_for_gene(HashMap<String, HashMap<String, ArrayList<String>>> gene_freqs_per_seq,
                                                     String gene1, String gene2) {

        String[] gene_array = gene1.split("#");
        String[] gene2_array = gene2.split("#");
        HashMap<String, ArrayList<String>> gene_frequencies;
        if (gene_freqs_per_seq.containsKey(gene_array[0])) {
            gene_frequencies = gene_freqs_per_seq.get(gene_array[0]);
        } else {
            gene_frequencies = new HashMap<>();
        }
        ArrayList<String> freq;
        if (gene_frequencies.containsKey(gene_array[1])) {
            freq = gene_frequencies.get(gene_array[1]);
        } else {
            freq = new ArrayList<>();
        }
        freq.add(gene2_array[0]);
        gene_frequencies.put(gene_array[1], freq);
        gene_freqs_per_seq.put(gene_array[0], gene_frequencies);
    }

    public static long[] count_genes_bases_in_blocks(ArrayList<Integer> combined_start_stop_positions, Node[] mrnas_of_seq) {
        long total_bases_in_block = 0, total_mrna_in_block = 0;
        for (int i = 0; i < combined_start_stop_positions.size()-1; i += 2) {
            int start_position = combined_start_stop_positions.get(i);
            int end_position = combined_start_stop_positions.get(i+1);
            Node start_mrna = mrnas_of_seq[start_position-1];
            Node end_mrna = mrnas_of_seq[end_position-1];
            int[] start_address = (int[]) start_mrna.getProperty("address");
            int[] end_address = (int[]) end_mrna.getProperty("address");
            total_bases_in_block += (end_address[3] - start_address[2]) + 1;
            total_mrna_in_block += (end_position- start_position) +1;
        }
        return new long[] {total_mrna_in_block,total_bases_in_block};
    }

    public static String getConcatenatedIdsOfMrnaNodes(Node[] mrnaNodes) {
        StringBuilder output = new StringBuilder();
        for (Node mrnaNode : mrnaNodes) {
            String id = (String) mrnaNode.getProperty("protein_ID");
            output.append(id + " ");
        }
        return output.toString().replaceFirst(".$","");
    }

    /**
     *
     * @param renamed_ids_file
     * @return
     */
    private HashMap<String, String> read_renamed_identifiers_for_mcscanx_pt(String renamed_ids_file) {
        HashMap<String, String> renamedIdentifiersMap = new HashMap<>(); //key is aa1, value is 1_1
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(renamed_ids_file))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                line = line.trim();
                if (line.startsWith("#") || line.equals("")) {
                    continue;
                }
                String[] line_array = line.split(",");
                renamedIdentifiersMap.put(line_array[1], line_array[0]);
                renamedIdentifiersMap.put(line_array[0], line_array[1]);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", renamed_ids_file);
            System.exit(1);
        }
        return renamedIdentifiersMap;
    }

    public static HashMap<String, Integer> count_homologous_genes_from_homology_file(String homology_file) {
        HashMap<String, Integer> homolog_genes_statistics = new HashMap<>();
        HashMap<String, HashSet<String>> shared_genes_between_seqs = new HashMap<>(); // key is combination of two sequence identifiers (1_1#1_2)
        HashMap<String, ArrayList<String>> total_shared_genes_between_seqs = new HashMap<>(); // key is combination of two sequence identifiers (1_1#1_2)
        //total shared should be the distinct genes of 1_1#1_2 and 1_2#1_1
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(homology_file))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                String[] line_array = line.trim().split("\t");
                increase_shared_genes_between_seqs(shared_genes_between_seqs, line_array[0], line_array[1]);
                increase_shared_genes_between_seqs(shared_genes_between_seqs, line_array[1], line_array[0]);
                increase_total_shared_genes_between_seqs(total_shared_genes_between_seqs, line_array[0], line_array[1]);
                increase_total_shared_genes_between_seqs(total_shared_genes_between_seqs, line_array[1], line_array[0]);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", homology_file);
            System.exit(1);
        }

        Pantools.logger.info("distinct homologs from .homology file.");
        for (String seq_combi : shared_genes_between_seqs.keySet()) {
            HashSet<String> homologous_genes_seq1 = shared_genes_between_seqs.get(seq_combi);
            String[] seq_combi_array = seq_combi.split("#");
            HashSet<String> homologous_genes_seq2 = shared_genes_between_seqs.get(seq_combi_array[1] + "#" + seq_combi_array[0]);
            ArrayList<String> total_relationships = total_shared_genes_between_seqs.get(seq_combi);
            // number of times the two sequences are found together in the .homology file
            //+System.out.println(seq_combi + " " + shared_genes.size() + " " + total_relationships.size());
            homolog_genes_statistics.put(seq_combi_array[0], homologous_genes_seq1.size()); // distinct genes of the first sequence
            homolog_genes_statistics.put(seq_combi_array[1], homologous_genes_seq2.size() ); // distinct genes of second sequence
            homolog_genes_statistics.put(seq_combi, total_relationships.size());
            homolog_genes_statistics.put(seq_combi_array[1] + "#" + seq_combi_array[0], total_relationships.size() );
        }
        return  homolog_genes_statistics;

    }

    /**
     * Update the skip arrays and lists when certain sequencies were skipped earlier during the synteny contstruction
     * Only read the '#Skipped sequences:' line of the .collinearity file
     */
    public static void update_skip_array_based_on_collinear_file() {
        if (INPUT_FILE == null) {
            return;
        }
        String skip_file = INPUT_FILE.replace("mcscanx.collinearity", "skipped_sequences.info");
        if (!checkIfFileExists(skip_file)) {
            return; // no sequences were skipped
        }

        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(skip_file))) {
            while ((line = in.readLine()) != null) { // go over the lines of a file
                line = line.trim();
                if (line.startsWith("#Skipped sequences:")) {
                    String[] line_array = line.split(": ");
                    String[] line_array2 = line_array[1].split(",");
                    for (String seq_id : line_array2) {
                        if (seq_id.equals("")) {
                            continue;
                        }
                        if (!skip_seq_list.contains(seq_id)) {
                            skip_seq_list.add(seq_id);
                            String[] seq_id_array = seq_id.split("_");
                            int genome_nr = Integer.parseInt(seq_id_array[0]);
                            int seq_nr = Integer.parseInt(seq_id_array[1]);
                            skip_seq_array[genome_nr-1][seq_nr-1] = true;
                        }
                    }
                }

                if (line.startsWith("## Alignment")) {
                    break;
                }
            }
        }  catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", skip_file);
            System.exit(1);
        }
    }

    /**
     *
     * @param genes_per_sequences
     * @param gene_position
     */
    public static void get_conserved_missing_genes_from_blocks(HashMap<String, ArrayList<String>> genes_per_sequences,
                                                               HashMap<String, Integer> gene_position) {
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                line = line.trim();
                if (line.startsWith("#")) {
                    continue;
                }
                String[] line_array = line.split("\t");
                int gene_pos1 = gene_position.get(line_array[1]);
                int gene_pos2 = gene_position.get(line_array[2]);
                Pantools.logger.info("{} {} {} {}", line_array[1], line_array[2], gene_pos1, gene_pos2);

            }
        }  catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
    }

    private HashMap<String, String> create_sequence_info_map(HashMap<String, ArrayList<String>> genes_per_sequences,
                                                                   HashMap<String, String> renamedIdentifiersMap) {
        HashMap<String, String> sequence_info_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequence_nodes.hasNext()) {
                Node seq_node = sequence_nodes.next();
                String identifier = (String) seq_node.getProperty("identifier");
                String renamed_id = renamedIdentifiersMap.get(identifier);
                long seq_length = (long) seq_node.getProperty("length");
                //System.out.println(identifier + " " + seq_length);
                sequence_info_map.put(renamed_id + "#length", seq_length + "");
            }
            tx.success();
        }
        for (String renamed_id : genes_per_sequences.keySet()) {
            ArrayList<String> genes_of_seq = genes_per_sequences.get(renamed_id);
            sequence_info_map.put(renamed_id + "#genes", genes_of_seq.size() + "");
        }
        return sequence_info_map;
    }

    /**
     * with a high number of genomes and mrnas, might be better to
     * Positions start at 1 (not 0)
     * @param mrnaNodesPerSequence
     * @return
     */
    public HashMap<Node, Integer> create_mrna_position_map(HashMap<String, Node[]> mrnaNodesPerSequence) {
        HashMap<Node, Integer> mrna_position_map = new HashMap<>();
        for (String key : mrnaNodesPerSequence.keySet()) {
            int counter = 0;
            Node[] mrna_nodes = mrnaNodesPerSequence.get(key);
            //System.out.println("new " + key + " " +  mrna_nodes.length);
            for (Node mrna_node : mrna_nodes) {
                counter++;
                mrna_position_map.put(mrna_node, counter);
            }
        }
        return mrna_position_map;
    }

    private HashMap<String, Integer> transformMrnaNodetoStringMap(HashMap<Node, Integer> mrna_position_map) {
        HashMap<String, Integer> mrna_position_map2 = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node node : mrna_position_map.keySet()) {
                int position = mrna_position_map.get(node);
                int[] address = (int[]) node.getProperty("address");
                String id = (String) node.getProperty("protein_ID");
                id = removeMcscanDisassolowedCharacters(id);
                mrna_position_map2.put(address[0] + "_" + address[1] + "#" + id, position);
            }
        }
        return mrna_position_map2;
    }

    private HashMap<String, String> transformMrnaNodetoStringMap2(HashMap<Node, Integer> mrna_position_map,
                                                                  HashMap<String, String> renamedIdentifiersMap) {
        HashMap<String, String> mrna_position_map2 = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node node : mrna_position_map.keySet()) {
                int position = mrna_position_map.get(node);
                int[] address = (int[]) node.getProperty("address");
                String id = (String) node.getProperty("protein_ID");
                id = removeMcscanDisassolowedCharacters(id);
                String mcscanid = renamedIdentifiersMap.get(address[0] + "_" + address[1]);
                mrna_position_map2.put(mcscanid + "#" + position, id);
            }
        }
        return mrna_position_map2;
    }


    public static void create_mcscan_csv(HashMap<String, String> block_size_map, HashMap<Integer, String> genes_per_block,
                                         HashMap<Integer, ArrayList<String>> coordinate_info_map, HashMap<Integer, ArrayList<String>> overlapping_blocks_map, HashMap<Integer, String[]> block_identifiers_map,
                                         HashMap<String, String> sequence_info_map, ArrayList<Boolean> inverted_blocks) {

        StringBuilder csv_builder = new StringBuilder("Synteny block number;Sequence identifier;Original sequence identifier;Number of genes on sequence;"
                + "Sequence size;Block size (genes);Block is inverted;Syntenic genes in block;Percentage of syntenic genes in block;Start position;Stop position;"
                + "Block length;Block overlaps % of sequence;Block overlaps % of genes;Syntenic genes in block overlap % of genes;"
                + "Numbers of overlapping blocks;This block overlaps blocks;Locations of overlapped blocks;Original sequence identifiers;"
                + "Partially overlapping blocks;Locations of partially overlapping blocks;Original sequence identifiers;"
                + "This block is completely overlapped by blocks;Locations of overlapped by blocks;Original sequence identifiers;Gene identifiers;\n");

        String previous_key = "";
        for (int block_nr : genes_per_block.keySet()) {
            String[] block_identifiers = block_identifiers_map.get(block_nr);
            String seq_combi = block_identifiers[0] + " " + block_identifiers[1];
            //System.out.println(block_nr + " " + seq_combi);
            String[] seq1_array = block_identifiers[0].split(";");
            String[] seq2_array = block_identifiers[1].split(";");
            String syntenic_genes_str = block_size_map.get(block_nr + "_syntenic_" + seq1_array[0]); // is the same number for both sequences
            int syntenic_genes = Integer.parseInt(syntenic_genes_str);
            //System.out.println(key + " " + seq1_array[0] + " " + seq2_array[0] );
            String block_size_genes_str1 = block_size_map.get(block_nr + "_total_" + seq1_array[0]);
            int block_size_genes1 = Integer.parseInt(block_size_genes_str1);

            String block_size_genes_str2 = block_size_map.get(block_nr + "_total_" + seq2_array[0]);
            int block_size_genes2 = Integer.parseInt(block_size_genes_str2);

            ArrayList<String> coordinate_info = coordinate_info_map.get(block_nr);
            String seq1_length = sequence_info_map.get(seq1_array[0] + "#length");
            String seq2_length = sequence_info_map.get(seq2_array[0] + "#length");
            String seq1_genes = sequence_info_map.get(seq1_array[0] + "#genes");
            String seq2_genes = sequence_info_map.get(seq2_array[0] + "#genes");
            if (seq1_genes == null || seq2_genes == null) {
                continue;
            }
            int seq1_gene_int = Integer.parseInt(seq1_genes);
            int seq2_gene_int = Integer.parseInt(seq2_genes);
            long seq1_length_l = Long.parseLong(seq1_length);
            long seq2_length_l = Long.parseLong(seq2_length);
            String seq1_length_str = b_Kb_Mb_or_Gb(seq1_length_l);
            String seq2_length_str = b_Kb_Mb_or_Gb(seq2_length_l);

            if (!seq_combi.equals(previous_key)) {
                csv_builder.append("\n#blocks for ").append(seq1_array[1]).append(" ").append(seq2_array[1]).append("\n");
            }
            if (block_nr % 100 == 0 || block_nr < 10) {
                System.out.print("\rblock " + block_nr + "  ");
            }
            previous_key = block_identifiers[0] + " " + block_identifiers[1];

            int block_size1 = Integer.parseInt(coordinate_info.get(4));
            int block_size2 = Integer.parseInt(coordinate_info.get(5));
            String pc_seq1_covered = get_percentage_str(block_size1, seq1_length_l, 2);
            String pc_seq2_covered = get_percentage_str(block_size2, seq2_length_l, 2);
            String pc_seq1_covered_genes = get_percentage_str(block_size_genes1, seq1_gene_int, 2);
            String pc_seq2_covered_genes = get_percentage_str(block_size_genes2, seq2_gene_int, 2);
            String pc_seq1_covered_S_genes = get_percentage_str(syntenic_genes, seq1_gene_int, 2);
            String pc_seq2_covered_S_genes = get_percentage_str(syntenic_genes, seq2_gene_int, 2);

            String pc_seq1_block_covered_S_genes = get_percentage_str(syntenic_genes, block_size_genes1, 2);
            String pc_seq2_block_covered_S_genes = get_percentage_str(syntenic_genes, block_size_genes2, 2);
            //System.out.println(syntenic_genes + " " + block_size1 + " " + block_size2 + " " + pc_seq1_block_covered_S_genes + " " + pc_seq2_block_covered_S_genes);
            String block_size1_str = b_Kb_Mb_or_Gb(block_size1);
            String block_size2_str = b_Kb_Mb_or_Gb(block_size2);
            String info_overlaps1 = "";
            String info_overlaps2 = "";
            //String gene_names = genes_per_block.get(key);
            //System.out.println("GENEN " + gene_names);
            String[] gene_names_array = genes_per_block.get(block_nr).split("##");
            if (overlapping_blocks_map.containsKey(block_nr)) {
                ArrayList<String> overlapping_blocks = overlapping_blocks_map.get(block_nr);
                //System.out.println("\n " + seq1_array[1] + " is referentie ");

                for (String info : overlapping_blocks) {
                    String[] info_array = info.split("\t");
                    String complete_ali_nr = info_array[2].replace("[", "").replace("]", "").replace(" ", "");
                    String complete_seqs = info_array[3].replace("[", "").replace("]", "").replace(" ", "");
                    String part_ali_nrs = info_array[4].replace("[", "").replace("]", "").replace(" ", "");
                    String part_seqs = info_array[5].replace("[", "").replace("]", "").replace(" ", "");
                    String overlapped_ali_nrs = info_array[6].replace("[", "").replace("]", "").replace(" ", "");
                    String overlapped_seqs = info_array[7].replace("[", "").replace("]", "").replace(" ", "");
                    HashSet<String> distinct_ovl_blocks = new HashSet<>();
                    //System.out.println(ovl_info);
                    String [] complete_array = complete_ali_nr.split(",");
                    String [] part_array =  part_ali_nrs.split(",");
                    String [] overlapped_array = overlapped_ali_nrs.split(",");
                    for (String nr : complete_array) {
                        if (nr.equals("")) {
                            continue;
                        }
                        distinct_ovl_blocks.add(nr);
                    }
                    for (String nr : part_array) {
                        if (nr.equals("")) {
                            continue;
                        }
                        distinct_ovl_blocks.add(nr);
                    }
                    for (String nr : overlapped_array) {
                        if (nr.equals("")) {
                            continue;
                        }
                        distinct_ovl_blocks.add(nr);
                    }
                    //System.out.println(" " + distinct_ovl_blocks.size()  + " " + distinct_ovl_blocks);
                    String ovl_info = distinct_ovl_blocks.size() + ";" + complete_ali_nr + ";" + complete_seqs + ";" + part_ali_nrs + ";" + part_seqs + ";"
                            + overlapped_ali_nrs + ";" + overlapped_seqs +";";
                    if (info_array[1].equals(seq1_array[1])) {
                        info_overlaps1 = ovl_info;
                    } else {
                        info_overlaps2 = ovl_info;
                    }
                }
            }

            if (info_overlaps1.equals("")) {
                info_overlaps1 = ";;;;;;;;;";
            }

            if (info_overlaps2.equals("")) {
                info_overlaps2 = ";;;;;;;;;";
            }

            csv_builder.append(block_nr + ";" + block_identifiers[0] + ";" + seq1_genes + ";" + seq1_length_str + ";" + block_size_genes_str1 + ";" + inverted_blocks.get(block_nr-1) +
                    ";" + syntenic_genes_str + ";" + pc_seq1_block_covered_S_genes + ";" + coordinate_info.get(0) + ";" + coordinate_info.get(1) + ";" + block_size1_str + ";" + pc_seq1_covered + ";" + pc_seq1_covered_genes + ";" +
                    pc_seq1_covered_S_genes + ";" + info_overlaps1 + gene_names_array[0].replaceFirst(".$","") + "\n");

            csv_builder.append(block_nr + ";" + block_identifiers[1] + ";" + seq2_genes + ";" + seq2_length_str + ";" + block_size_genes_str2 + ";" + inverted_blocks.get(block_nr-1) +
                    ";" + syntenic_genes_str  + ";" + pc_seq2_block_covered_S_genes + ";" + coordinate_info.get(2) + ";" + coordinate_info.get(3) + ";" + block_size2_str + ";" + pc_seq2_covered + ";" + pc_seq2_covered_genes + ";" +
                    pc_seq2_covered_S_genes + ";" + info_overlaps2 + gene_names_array[1].replaceFirst(".$","") + "\n");
        }
        writeStringToFile(csv_builder.toString(), WORKING_DIRECTORY + "synteny_blocks_overview.csv", false, false);
        System.out.println("\rDone!              ");
    }

    /**
     *
     * @return
     */
    public HashSet<String> check_which_sequences_have_synteny() {
        HashSet<String> present_sequences = new HashSet<>();
        HashSet<String> sequence_combinations = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
            while (synteny_nodes.hasNext()) {
                Node synteny_node = synteny_nodes.next();
                String sequence_identifiers = (String) synteny_node.getProperty("sequence_identifiers"); // example: 1_1,2_1
                String[] seq_id_array = sequence_identifiers.split(",");
                present_sequences.add(seq_id_array[0]);
                present_sequences.add(seq_id_array[1]);
                sequence_combinations.add(sequence_identifiers);
            }
            tx.success();
        }

        int extra_skip = 0;
        for (int i = 1; i <= total_genomes; i++) { // i is a genome number
            if (skip_array[i-1]) {
                continue;
            }
            for (int j = 1; j <= GENOME_DB.num_sequences[i]; ++j) {
                if (skip_seq_array[i-1][j-1]) {
                    continue;
                }
                if (!present_sequences.contains(i + "_" + j)) {
                    extra_skip++;
                    skip_seq_array[i-1][j-1] = true;
                    selectedSequences.remove(i + "_" + j);
                } else {
                    present_sequences.remove(i + "_" + j); // remove to speed up search
                }
            }
        }
        if (extra_skip > 0) {
            Pantools.logger.info("{} sequences were skipped because they have no synteny.", extra_skip);
        }
        Pantools.logger.info("{} genomes with {} sequences in analysis", adj_total_genomes, selectedSequences.size());
        return sequence_combinations;
    }

    /**
     *
     * @param homologyFileWriters  mcscanx.homology writers per sequence combination
     * @param homologyGroups
     * @return 'homolog_counts_per_seq_and_combi' has different types of keys
     *  1. sequence id
     *  2. genome number
     *  // to do , remove third part of following two keys
     *  3. sequence id 1 +  sequence id2 +  sequence id 1
     *  4. genome nr 1 + genome nr 2 + genome nr 1
     */
    private HashMap<String, Integer> startParralelFindHomologsBetweenSequences(HashMap<String, BufferedWriter> homologyFileWriters,
            List<Long> homologyGroups) {

        mrnasInAnalysis = new AtomicInteger();
        homologousMrnasInAnalysis = new AtomicInteger();
        fillHomologyNodeQueue(homologyGroups);

        HashMap<String, HashSet<Node>> homolog_counts_per_seq_and_combi2 = new HashMap<>();
        HashMap<String, Integer> homolog_counts_per_seq_and_combi = new HashMap<>();

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try {
                ExecutorService es = Executors.newFixedThreadPool(THREADS);
                for (int i = 1; i <= THREADS; i++) {
                    Node zero = GRAPH_DB.getNodeById(0);
                    nodeQueue.add(zero); // add a stop string for every thread
                    es.execute(new findHomologsBetweenSequences(i, homolog_counts_per_seq_and_combi2, homologyFileWriters));
                }
                es.shutdown();
                es.awaitTermination(10, TimeUnit.DAYS);
            } catch (OutOfMemoryError oom) {
                Pantools.logger.error("The process ran out of memory, increase your java heap space.");
                throw new OutOfMemoryError(oom.getMessage());
            } catch (InterruptedException e) {
                Pantools.logger.error("Something went wrong while running MCScanX.");
                throw new RuntimeException(e.getMessage());
            }
            tx.success();
        }

        // Use the size of the hashset per sequence (and combination) and store as integer
        for (String key : homolog_counts_per_seq_and_combi2.keySet()) {
            HashSet<Node> nodes = homolog_counts_per_seq_and_combi2.get(key);
            if (!key.contains("_") && !key.contains("#")) { // the number of homologous genes for a single genome
                homologousMrnasInAnalysis.addAndGet(nodes.size());
            }
            homolog_counts_per_seq_and_combi.put(key, nodes.size());
        }
        return homolog_counts_per_seq_and_combi;
    }

    private void fillHomologyNodeQueue(List<Long> homologyGroups){
        nodeQueue = new LinkedBlockingQueue<>();
        ArrayList<Node> homologyNodes = retrieveHomologyNodes();
        for (Node homologyNode : homologyNodes){
            if (homologyGroups != null && !homologyGroups.contains(homologyNode.getId())) { // if homology groups were included by user -H
                continue;
            }
            nodeQueue.add(homologyNode);
        }
    }

    /**
     * example file
     *
     * ############### Parameters ###############
     * # MATCH_SCORE: 50
     * # MATCH_SIZE: 5
     * # GAP_PENALTY: -1
     * # OVERLAP_WINDOW: 5
     * # E_VALUE: 1e-05
     * # MAX GAPS: 25
     * ############### Statistics ###############
     * # Number of collinear genes: 479623, Percentage: 73.66
     * # Number of all genes: 651102
     * ##########################################
     * ## Alignment 0: score=543.0 e_value=2.7e-33 N=13 ct1&hu1 minus
     *   0-  0:    3_43#Soltu.Cru.03_4G021810.1    5_23#C88_C12H3G067260.1       0
     *   0-  1:    3_43#Soltu.Cru.03_4G021980.1    5_23#C88_C12H3G067100.1       0
     *   0-  2:    3_43#Soltu.Cru.03_4G022050.1    5_23#C88_C12H3G066940.3       0
     *   0-  3:    3_43#Soltu.Cru.03_4G022260.1    5_23#C88_C12H3G066890.1       0
     *   0-  4:    3_43#Soltu.Cru.03_4G022270.1    5_23#C88_C12H3G066870.1       0
     *   0-  5:    3_43#Soltu.Cru.03_4G022410.1    5_23#C88_C12H3G066680.1       0
     *   0-  6:    3_43#Soltu.Cru.03_4G022450.1    5_23#C88_C12H3G066670.1       0
     *   0-  7:    3_43#Soltu.Cru.03_4G022490.3    5_23#C88_C12H3G066600.1       0
     *   0-  8:    3_43#Soltu.Cru.03_4G022550.2    5_23#C88_C12H3G066570.2       0
     *   0-  9:    3_43#Soltu.Cru.03_4G022650.1    5_23#C88_C12H3G066540.2       0
     *   0- 10:    3_43#Soltu.Cru.03_4G022670.1    5_23#C88_C12H3G066530.1       0
     *   0- 11:    3_43#Soltu.Cru.03_4G022690.1    5_23#C88_C12H3G066520.1       0
     *   0- 12:    3_43#Soltu.Cru.03_4G022870.1    5_23#C88_C12H3G066510.1       0
     *
     *
     * @param mrna_address_map
     * @param renamedIdentifiersMap
     * @param sequence_info_map
     * @param gene_position
     * @param homolog_genes_statistics
     * @param gene_on_position
     */
    public void read_mcscan_collinear_file(HashMap<String, int[]> mrna_address_map, HashMap<String, String> renamedIdentifiersMap,
                                           HashMap<String, String> sequence_info_map, HashMap<String, Integer> gene_position,
                                           HashMap<String, Integer> homolog_genes_statistics, HashMap<String, String> gene_on_position) {

        HashMap<String, ArrayList<Integer>> cluster_coords_per_seq = new HashMap<>(); // key is two sequence identifiers seperated by a '&'
        HashMap<String, int[]> shared_blocks_map = new HashMap<>(); // key is two sequence identifiers seperated by a '&'
        HashMap<String, HashSet<String>> collinear_genes_map = new HashMap<>();
        // value is [shared blocks, shared gene pairs, inverted blocks]
        HashSet<String> gene_names = new HashSet<>();
        int block_counter = 0 ;
        int colinear_genes_counter = 0 ;
        boolean start_counting = false;
        String sequence1_id = "";
        String sequence2_id = "";
        String sequence_combi = "", prev_sequence_combi = "";
        String[] first_last_gene_seq1 = new String[2];
        String[] first_last_gene_seq2 = new String[2];
        HashMap<String, String> block_size_map = new HashMap<>(); // key is a block_nr + "_syntenic" or "_total"
        //ArrayList<String> block_info = new ArrayList<>();
        String genes_of_seq1 = "";
        String genes_of_seq2 = "";
        HashMap<Integer, ArrayList<String>> coordinate_info_map = new HashMap<>();
        HashMap<Integer, ArrayList<String>> overlapping_blocks_map = new HashMap<>();
        HashMap<Integer, String[]> block_identifiers_map = new HashMap<>();
        HashMap<Integer, String> genes_per_block = new HashMap<>(); // key is two sequence identifiers seperated by a '&'
        HashMap<String, ArrayList<Integer>> block_positions = new HashMap<>(); // key is two sequence identifiers seperated by a '&'
        ArrayList<Boolean> inverted_blocks = new ArrayList<>();
        HashMap<String, Integer> nr_blocks_between_seqs = new HashMap<>(); // key is combination of two seq IDs: 1_1#1_2
        HashMap<String, ArrayList<Integer>> block_sizes_between_seqs = new HashMap<>(); // key is combination of two seq IDs: 1_1#1_2

        //HashMap<String, Integer> blocks_between_seqs = new HashMap<>(); //key is combination of two seq IDs: 1_1#1_2
        DecimalFormat formatter = new DecimalFormat("0.00");
        HashMap<String, HashSet<String>> shared_genes_between_seqs = new HashMap<>(); // key is combination of two sequence identifiers (1_1#1_2)
        HashMap<String, ArrayList<String>> total_shared_genes_between_seqs = new HashMap<>(); // key is combination of two sequence identifiers (1_1#1_2)
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                line = line.trim();
                if (line.startsWith("## Between")) {
                    continue;
                }
                if (line.startsWith("## Alignment")) {
                    Pantools.logger.info(line);
                    String[] line_array = line.split(" ");
                    //System.out.println(line_array.length + " " + Arrays.toString(line_array));
                    // String[] id_array = line_array[6].split("&"); // Example string is '1_1&2_1'
                    String number_of_genes_str = line_array[5].replace("N=","");
                    int number_of_genes = Integer.parseInt(number_of_genes_str);
                    sequence_combi = line_array[6].replace("&","#");
                    //System.out.println("COMBI " + sequence_combi);

                    String[] id_array = sequence_combi.split("#");
                    sequence1_id = id_array[0];
                    sequence2_id = id_array[1];
                    String original_seq1_id = renamedIdentifiersMap.get(sequence1_id);
                    String original_seq2_id = renamedIdentifiersMap.get(sequence2_id);
                    String[] seq1Array = original_seq1_id.split("_");
                    String[] seq2Array = original_seq2_id.split("_");
                    int genome1 = Integer. parseInt(seq1Array[0]);
                    int genome2 = Integer. parseInt(seq2Array[0]);

                    int sequence1 = Integer. parseInt(seq1Array[1]);
                    int sequence2 = Integer. parseInt(seq2Array[1]);
                    //System.out.println(" " + original_seq1_id + " " + original_seq2_id);
                    if (skip_seq_array[genome1-1][sequence1-1] || skip_seq_array[genome2-1][sequence2-1]) {
                        start_counting = false;
                        Pantools.logger.info("SKIPPED");
                        continue;
                    }

                    int[] values;
                    if (!shared_blocks_map.containsKey(sequence_combi)) {
                        values = new int[3];
                    } else {
                        values = shared_blocks_map.get(sequence_combi);
                    }
                    values[0]++;
                    values[1] += number_of_genes;
                    if (line_array[7].equals("minus")) {
                        values[2]++;
                    }
                    shared_blocks_map.put(sequence_combi, values);

                    block_counter++;
                    start_counting = true;
                    block_identifiers_map.put(block_counter, new String[]{sequence1_id + ";" + original_seq1_id, sequence2_id + ";" + original_seq2_id });
                    if (!genes_of_seq1.equals("")) {
                        genes_per_block.put(block_counter-1, genes_of_seq1 + "##" + genes_of_seq2);
                        get_gene_positions_from_synteny_block(first_last_gene_seq1, first_last_gene_seq2, gene_position, block_size_map, block_counter-1, block_positions,
                                prev_sequence_combi, inverted_blocks, block_sizes_between_seqs);
                        get_coordinates_from_synteny_block(first_last_gene_seq1, first_last_gene_seq2, mrna_address_map, cluster_coords_per_seq, coordinate_info_map, block_counter-1);
                        if (inverted_blocks.get(inverted_blocks.size()-1)) {
                            nr_blocks_between_seqs.merge(prev_sequence_combi + "#inverted", 1, Integer::sum);
                        }
                        nr_blocks_between_seqs.merge(prev_sequence_combi, 1, Integer::sum);
                    }

                    int syntenic_genes = Integer. parseInt( line_array[5].replace("N=",""));
                    block_size_map.put(block_counter + "_syntenic_" + sequence1_id, line_array[5].replace("N=",""));
                    block_size_map.put(block_counter + "_syntenic_" + sequence2_id, line_array[5].replace("N=",""));
                    block_sizes_between_seqs.computeIfAbsent(sequence1_id + "#" + sequence2_id + "#syntenic", k -> new ArrayList<>()).add(syntenic_genes);
                    first_last_gene_seq1 = new String[2];
                    first_last_gene_seq2 = new String[2];
                    genes_of_seq1 = "";
                    genes_of_seq2 = "";
                    prev_sequence_combi = sequence_combi;
                } else if (start_counting) {
                    String[] line_array = line.split("\t"); // example  0-  0:    3_43#Soltu.Cru.03_4G021810.1    5_23#C88_C12H3G067260.1       0
                    colinear_genes_counter++;
                    String gene_id_seq1 = line_array[1];
                    String gene_id_seq2 = line_array[2];
                    if (first_last_gene_seq1[0] == null) {
                        first_last_gene_seq1[0] = gene_id_seq1;
                        first_last_gene_seq2[0] = gene_id_seq2;
                    }

                    first_last_gene_seq1[1] = gene_id_seq1;
                    first_last_gene_seq2[1] = gene_id_seq2;
                    genes_of_seq1 += gene_id_seq1 + ",";
                    genes_of_seq2 += gene_id_seq2 + ",";
                    gene_names.add(gene_id_seq1);
                    gene_names.add(gene_id_seq2);
                    collinear_genes_map.computeIfAbsent(sequence_combi + "#" + sequence1_id, k -> new HashSet<>()).add(gene_id_seq1);
                    collinear_genes_map.computeIfAbsent(sequence_combi + "#" + sequence2_id, k -> new HashSet<>()).add(gene_id_seq2);
                    increase_shared_genes_between_seqs(shared_genes_between_seqs, gene_id_seq2, gene_id_seq1);
                    increase_shared_genes_between_seqs(shared_genes_between_seqs, gene_id_seq1, gene_id_seq2);
                    increase_total_shared_genes_between_seqs(total_shared_genes_between_seqs, gene_id_seq2, gene_id_seq1);
                    increase_total_shared_genes_between_seqs(total_shared_genes_between_seqs, gene_id_seq1, gene_id_seq2);
                }
            }
            genes_per_block.put(block_counter, genes_of_seq1 + "##" + genes_of_seq2);
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
        Pantools.logger.info("uit");
        get_coordinates_from_synteny_block(first_last_gene_seq1, first_last_gene_seq2, mrna_address_map, cluster_coords_per_seq, coordinate_info_map, block_counter);
        get_gene_positions_from_synteny_block(first_last_gene_seq1, first_last_gene_seq2, gene_position, block_size_map, block_counter, block_positions,
                sequence_combi, inverted_blocks, block_sizes_between_seqs);
        Pantools.logger.info("blocks: {}", block_counter);
        if (block_counter == 0) {
            return;
        }
        if (inverted_blocks.get(inverted_blocks.size()-1)) {
            nr_blocks_between_seqs.merge(sequence_combi + "#inverted", 1, Integer::sum);
        }
        nr_blocks_between_seqs.merge(sequence_combi, 1, Integer::sum);

        int inv_counter = 0;
        for (String key : nr_blocks_between_seqs.keySet()) {
            if (key.contains("#inverted")) {
                inv_counter += nr_blocks_between_seqs.get(key);
            }
        }

        //temporarily disabled to speed up
        check_if_clusters_fall_in_other_clusters(cluster_coords_per_seq, renamedIdentifiersMap, overlapping_blocks_map, block_identifiers_map);
        report_block_statistics(shared_blocks_map, collinear_genes_map, renamedIdentifiersMap);

        Pantools.logger.info("inverted blocks: {}", inv_counter);
        Pantools.logger.info("collinear gene pairs: {}", colinear_genes_counter);
        Pantools.logger.info("genes in blocks : {}", gene_names.size());

        //temporarily disabled to speed up
        create_mcscan_csv(block_size_map, genes_per_block, coordinate_info_map, overlapping_blocks_map, block_identifiers_map, sequence_info_map, inverted_blocks);

        HashMap<String, String> sequence_covered = determine_sequence_overlap_with_blocks(block_positions, sequence_info_map, gene_on_position, mrna_address_map, renamedIdentifiersMap);


        HashMap<String, ArrayList<String>> block_statistics = new HashMap<>();
        Pantools.logger.info("block sizes.");
        for (String seq_combi : block_sizes_between_seqs.keySet()) {
            ArrayList<Integer> blocks = block_sizes_between_seqs.get(seq_combi);
            double[] median_average_sd = median_average_stdev_from_AL_int(blocks);
            int lowest = 9999, highest = 0;
            for (int block_size : blocks) {
                if (block_size < lowest) {
                    lowest = block_size;
                }
                if (block_size > highest) {
                    highest = block_size;
                }
            }
            //System.out.println(seq_combi + " " + average + " " + median + " " +lowest + "-" + highest + " " + total + " " + blocks);
            block_statistics.computeIfAbsent(seq_combi, k -> new ArrayList<>()).add(lowest + "");
            block_statistics.computeIfAbsent(seq_combi, k -> new ArrayList<>()).add(highest + "");
            block_statistics.computeIfAbsent(seq_combi, k -> new ArrayList<>()).add(median_average_sd[0] + "");
            block_statistics.computeIfAbsent(seq_combi, k -> new ArrayList<>()).add(formatter.format(median_average_sd[1]));
        }

        Pantools.logger.info("block sizes2.");
        for (String seq_combi : block_sizes_between_seqs.keySet()) {
            if (seq_combi.contains("syntenic")) {
                continue;
            }
            //System.out.println(seq_combi + " " +  block_sizes_between_seqs.get(seq_combi));
        }

        StringBuilder block_statistics_csv = new StringBuilder("Sequence 1 identifier,Sequence 2 identifier,");
        if (!renamedIdentifiersMap.isEmpty()) {
            block_statistics_csv.append("Original sequence 1 identifier, Original sequence 2 identifier,");
        }
        block_statistics_csv.append("Homologous genes sequence 1,Homology relationships between sequences,"
                + "Distinct syntenic genes on sequence 1,"
                + "Synteny relationships between two sequences,Number of blocks,Inverted blocks, "
                + "Lowest number of syntenic genes in block,Highest number of syntenic genes in block,Median of syntenic genes in blocks,"
                + "Average number of syntenic genes in block,"
                + "Smallest block (genes),Largest block (genes),Median block size (genes), Average block size (genes),"
                + "Percentage of sequence covered,Percentage of genes from sequence covered\n");


        String[] selectedSequencesArray = new String[selectedSequences.size()];
        selectedSequencesArray = selectedSequences.toArray(selectedSequencesArray);
        for (int i = 0; i < selectedSequences.size(); i++) {
            System.out.print("\rWriting block statistics: sequence " + (i+1) + "/" + selectedSequences.size());
            int sequenceCounter = 0;
            String seq_id1 = selectedSequencesArray[i];
            for (int j = 0; j < selectedSequences.size(); j++) {
                if (i == j) {
                    continue;
                }
                sequenceCounter++;
                if (sequenceCounter % 1000 == 0) {
                    System.out.print("\rWriting block statistics: Sequence " + (i+1) + "/" + selectedSequences.size());
                }
                StringBuilder seq1_info = new StringBuilder();
                StringBuilder seq2_info = new StringBuilder();

                String seq_id2 = selectedSequencesArray[j];
                //System.out.println(seq_id1 + " " + seq_id2);
                String new_id1 = seq_id1;
                String new_id2 = seq_id2;
                if (!renamedIdentifiersMap.isEmpty()) {
                    new_id1 = renamedIdentifiersMap.get(seq_id1);
                    new_id2 = renamedIdentifiersMap.get(seq_id2);
                    seq1_info.append(new_id1).append(",").append(new_id2).append(",");
                    seq2_info.append(new_id2).append(",").append(new_id1).append(",");
                }

                int h_relationships = 0, h_genes_seq1 = 0, h_genes_seq2 = 0 ;
                if (homolog_genes_statistics.containsKey(seq_id1 + "#" + seq_id2)) {
                    h_relationships = homolog_genes_statistics.get(seq_id1 + "#" + seq_id2);
                }
                if (homolog_genes_statistics.containsKey(seq_id1)) {
                    h_genes_seq1 = homolog_genes_statistics.get(seq_id1);
                }
                if (homolog_genes_statistics.containsKey(seq_id2)) {
                    h_genes_seq2 = homolog_genes_statistics.get(seq_id2);
                }

                seq1_info.append(seq_id1).append(",").append(seq_id2).append(",").append(h_genes_seq1).append(",").append(h_relationships).append(",");
                seq2_info.append(seq_id2).append(",").append(seq_id1).append(",").append(h_genes_seq2).append(",").append(h_relationships).append(",");
                if (h_relationships == 0) {
                    continue;
                }

                int blocks = 0, inv_blocks = 0;
                if (nr_blocks_between_seqs.containsKey(new_id1 + "#" + new_id2)) {
                    blocks = nr_blocks_between_seqs.get(new_id1 + "#" + new_id2);
                } else if (nr_blocks_between_seqs.containsKey(new_id2 + "#" + new_id1)) {
                    blocks = nr_blocks_between_seqs.get(new_id2 + "#" + new_id1);
                }

                if (nr_blocks_between_seqs.containsKey(new_id1 + "#" + new_id2 + "#inverted")) {
                    inv_blocks = nr_blocks_between_seqs.get(new_id1 + "#" + new_id2 + "#inverted");
                } else if (nr_blocks_between_seqs.containsKey(new_id2 + "#" + new_id1 + "#inverted")) {
                    inv_blocks = nr_blocks_between_seqs.get(new_id2 + "#" + new_id1 + "#inverted");
                }

                HashSet<String> shared_genes1;
                if (shared_genes_between_seqs.containsKey(seq_id1 + "#" + seq_id2)) {
                    shared_genes1 = shared_genes_between_seqs.get(seq_id1 + "#" + seq_id2);
                    seq1_info.append(shared_genes1.size());
                }

                HashSet<String> shared_genes2;
                if (shared_genes_between_seqs.containsKey(seq_id2 + "#" + seq_id1)) {
                    shared_genes2 = shared_genes_between_seqs.get(seq_id2 + "#" + seq_id1);
                    seq2_info.append(shared_genes2.size());
                }
                seq1_info.append(",");
                seq2_info.append(",");

                ArrayList<String> total_shared_genes1;
                if (total_shared_genes_between_seqs.containsKey(seq_id1 + "#" + seq_id2)) {
                    total_shared_genes1 = total_shared_genes_between_seqs.get(seq_id1 + "#" + seq_id2);
                    seq1_info.append(total_shared_genes1.size());
                }

                ArrayList<String> total_shared_genes2;
                if (total_shared_genes_between_seqs.containsKey(seq_id1 + "#" + seq_id2)) {
                    total_shared_genes2 = total_shared_genes_between_seqs.get(seq_id1 + "#" + seq_id2);
                    seq2_info.append(total_shared_genes2.size());
                }
                seq1_info.append(",").append(blocks).append(",").append(inv_blocks).append(",");
                seq2_info.append(",").append(blocks).append(",").append(inv_blocks).append(",");

                if (blocks == 0) {
                    //System.out.println("\nno blocks " + seq_id1 + " " + seq_id2);
                    continue;
                }

                //System.out.println(blocks + " " +  inv_blocks);
                //System.out.println("aantal genen " + shared_genes1.size() + " " +  shared_genes2.size() + " " +  total_shared_genes1.size() + " " + total_shared_genes2.size());
                ArrayList<String> statistics_syn;
                if (block_statistics.containsKey(new_id1 + "#" + new_id2 + "#syntenic")) {
                    statistics_syn = block_statistics.get(new_id1 + "#" + new_id2 + "#syntenic");
                } else if (block_statistics.containsKey(new_id2 + "#" + new_id1 + "#syntenic")) {
                    statistics_syn = block_statistics.get(new_id2 + "#" + new_id1 + "#syntenic");
                } else {
                    statistics_syn = new ArrayList<>();
                }

                String seq1_covered = sequence_covered.get(new_id1 + "#" + new_id2 + "#length");
                String seq2_covered = sequence_covered.get(new_id2 + "#" + new_id1 + "#length");
                String seq1_genes_covered = sequence_covered.get(new_id1 + "#" + new_id2 + "#genes");
                String seq2_genes_covered = sequence_covered.get(new_id2 + "#" + new_id1 + "#genes");
                ArrayList<String> statistics_total1 = block_statistics.get(new_id1 + "#" + new_id2 + "#total");
                ArrayList<String> statistics_total2 = block_statistics.get(new_id2 + "#" + new_id1 + "#total");

                seq1_info.append(statistics_syn.get(0)).append(",").append(statistics_syn.get(1)).append(",").append(statistics_syn.get(2))
                        .append(",").append(statistics_syn.get(0)).append(",");
                seq2_info.append(statistics_syn.get(0)).append(",").append(statistics_syn.get(1)).append(",").append(statistics_syn.get(2))
                        .append(",").append(statistics_syn.get(0)).append(",");

                seq1_info.append(statistics_total1.get(0)).append(",").append(statistics_total1.get(1)).append(",").append(statistics_total1.get(2))
                        .append(",").append(statistics_total1.get(3)).append(",").append(seq1_covered).append(",").append(seq1_genes_covered).append(",");

                seq2_info.append(statistics_total2.get(0)).append(",").append(statistics_total2.get(1)).append(",").append(statistics_total2.get(2))
                        .append(",").append(statistics_total2.get(3)).append(",").append(seq2_covered).append(",").append(seq2_genes_covered).append(",");
                //System.out.println("\n" + seq1_info);
                // System.out.println(seq2_info);
                block_statistics_csv.append(seq1_info).append("\n")
                        .append(seq2_info).append("\n");
            }
        }
        writeStringToFile(block_statistics_csv.toString(), WORKING_DIRECTORY + "synteny_blocks_statistics.csv", false, false);
    }


    public static void check_if_clusters_fall_in_other_clusters(HashMap<String, ArrayList<Integer>> cluster_coords_per_seq,
                                                                HashMap<String, String> renamedIdentifiersMap, HashMap<Integer, ArrayList<String>> overlapping_blocks_map,
                                                                HashMap<Integer, String[]> block_identifiers_map) {

        Pantools.logger.info("CHECK OVERLAP.");
        for (String key : cluster_coords_per_seq.keySet()) {
            if (key.endsWith("_ali_nr")) {
                continue;
            }
            ArrayList<Integer> clusters = cluster_coords_per_seq.get(key);
            ArrayList<Integer> clusters_nrs = cluster_coords_per_seq.get(key + "_ali_nr"); // ali number is block number
            Pantools.logger.info("OVL for sequence {}. {} blocks. {} {}", key, clusters_nrs.size(), clusters.size(), clusters_nrs);
            if (clusters_nrs.size()*2 !=  clusters.size()) {
                System.exit(1);
            }
            for (int i = 0; i < clusters.size(); i += 2) {
                int cluster_start = clusters.get(i);
                int cluster_end = clusters.get(i+1);
                int current_block_nr = clusters_nrs.get(i/2);
                //System.out.println(current_block_nr + " " + cluster_start + "-" + cluster_end);
                ArrayList<Integer> overlapped_blocks = new ArrayList<>();
                HashSet<String> overlapped_seq_blocks = new HashSet<>();
                ArrayList<Integer> partially_overlapping_blocks = new ArrayList<>();
                ArrayList<Integer> overlapping_blocks = new ArrayList<>();

                HashSet<String> part_ovl_seqs_blocks = new HashSet<>();
                HashSet<String> ovl_seqs_blocks = new HashSet<>();
                for (int j = 0; j < clusters.size(); j += 2) {
                    if (i == j) {
                        continue;
                    }
                    int block_nr = clusters_nrs.get(j/2);
                    int cluster2_start = clusters.get(j);
                    int cluster2_end = clusters.get(j+1);
                    String[] block_identifiers = block_identifiers_map.get(block_nr);
                    String seq_id = block_identifiers[0];
                    String[] seq_id_array = seq_id.split(";"); // first part is new id aa1, second part is old 1_1
                    if (seq_id_array[1].equals(key)) {
                        seq_id = block_identifiers[1];
                        seq_id_array = seq_id.split(";");
                    }

                    seq_id = seq_id_array[0];
                    if (cluster2_start <= cluster_start && cluster2_end >= cluster_end) {
                        // System.out.println(" val ertussen ");
                        overlapped_blocks.add(block_nr);
                        overlapped_seq_blocks.add(seq_id);
                    } else if (cluster2_start >= cluster_start && cluster2_end <= cluster_end) {
                        //System.out.println(" complete ovl " +  block_nr + " " + seq_id + "compl overlapped " + clusters_nrs.get(i/2) );
                        overlapping_blocks.add(block_nr);
                        ovl_seqs_blocks.add(seq_id);
                    } else if (cluster2_start >= cluster_start && cluster2_end >= cluster_end && cluster2_start < cluster_end) {
                        // System.out.println(" last part ovl " +  block_nr + " " + seq_id + "part overlapped " + clusters_nrs.get(i/2));
                        partially_overlapping_blocks.add(block_nr);
                        part_ovl_seqs_blocks.add(seq_id);
                    } else if (cluster2_start <= cluster_start && cluster2_end <= cluster_end && cluster2_end > cluster_start) {
                        // System.out.println(" first part ovl " +  block_nr + " " + seq_id + "part overlapped " + clusters_nrs.get(i/2));
                        partially_overlapping_blocks.add(block_nr);
                        part_ovl_seqs_blocks.add(seq_id);
                    } else {
                        // System.out.println(" no ovl");
                    }
                }
                String original_ids1 = "";
                for (String seq_id : ovl_seqs_blocks) {
                    String original_id = renamedIdentifiersMap.get(seq_id);
                    original_ids1 += original_id  + ",";
                }

                String original_ids2 = "";
                for (String seq_id : part_ovl_seqs_blocks) {
                    String original_id = renamedIdentifiersMap.get(seq_id);
                    original_ids2 += original_id  + ",";
                }

                String original_ids3 = "";
                for (String seq_id : overlapped_seq_blocks) {
                    String original_id = renamedIdentifiersMap.get(seq_id);
                    original_ids3 += original_id  + ",";
                }

                overlapping_blocks_map.computeIfAbsent(current_block_nr, k -> new ArrayList<>()).add(
                        "blocks\t" + key + "\t" + overlapping_blocks + "\t" + ovl_seqs_blocks + ";" +
                                original_ids1.replaceFirst(".$", "") + "\t" + partially_overlapping_blocks + "\t" +
                                part_ovl_seqs_blocks + ";" + original_ids2.replaceFirst(".$", "") + "\t" +
                                overlapped_blocks + "\t" + overlapped_seq_blocks + ";" +
                                original_ids3.replaceFirst(".$", "")
                );
            }
        }
    }

    public void report_block_statistics(HashMap<String, int[]> shared_blocks_map, HashMap<String, HashSet<String>> collinear_genes_map,
                                               HashMap<String, String> renamedIdentifiersMap) {

        int[] total_genes_per_genome = new int[total_genomes]; // zitten ook dubbele tussen
        //System.out.println("colli genes: " + collinear_genes_map);
        for (String key : shared_blocks_map.keySet()) {
            int[] values = shared_blocks_map.get(key);
            Pantools.logger.info("{} {}", key, Arrays.toString(values));
            String[] key_array = key.split("#");
            String id1 = renamedIdentifiersMap.get(key_array[0]);
            String id2 = renamedIdentifiersMap.get(key_array[1]);
            HashSet<String> gene_names_set1 = collinear_genes_map.get(key + "#" + key_array[0]);
            HashSet<String> gene_names_set2 = collinear_genes_map.get(key + "#" + key_array[1]);
            Pantools.logger.info(" distinct genes {}: {}", key_array[0], gene_names_set1.size());
            Pantools.logger.info(" distinct genes {}: {}", key_array[1], gene_names_set2.size());

            String[] id1_array = id1.split("_");
            String[] id2_array = id2.split("_");
            int genome_nr1 = Integer.parseInt(id1_array[0]);
            int genome_nr2 = Integer.parseInt(id2_array[0]);
            total_genes_per_genome[genome_nr1-1] += values[1];
            total_genes_per_genome[genome_nr2-1] += values[1];
        }
        Pantools.logger.info("sum of shared genes per genome {}", Arrays.toString(total_genes_per_genome));
    }

    public static void report_block_statistics2(HashMap<String, int[]> shared_blocks_map) {

        for (String sequence_combi : shared_blocks_map.keySet()) {
            int[] values = shared_blocks_map.get(sequence_combi); // [?????????]
            Pantools.logger.info("{} {}", sequence_combi, Arrays.toString(values));
        }
    }

    public static void get_coordinates_from_synteny_block(String[] first_last_gene_seq1, String[] first_last_gene_seq2,
                                                          HashMap<String, int[]> mrna_address_map, HashMap<String, ArrayList<Integer>> cluster_coords_per_seq,
                                                          HashMap<Integer, ArrayList<String>> coordinate_info_map, int block_nr) {

        //System.out.println(Arrays.toString(first_last_gene_seq1) + " " + Arrays.toString(first_last_gene_seq2));
        if (first_last_gene_seq1[0] == null) {
            return;
        }

        int[] address_seq1_gene1 = mrna_address_map.get(first_last_gene_seq1[0]);
        int[] address_seq1_gene2 = mrna_address_map.get(first_last_gene_seq1[1]);
        if (address_seq1_gene2 == null || address_seq1_gene1 == null) {
            return;
        }
        int[] address_seq2_gene1 = mrna_address_map.get(first_last_gene_seq2[0]);
        int[] address_seq2_gene2 = mrna_address_map.get(first_last_gene_seq2[1]);
        //System.out.println(Arrays.toString(address_seq1_gene1) + " " + Arrays.toString(address_seq1_gene2) + " "
        //    + Arrays.toString(address_seq2_gene1) + " " + Arrays.toString(address_seq2_gene2) );

        int cluster1_length = address_seq1_gene2[3] - address_seq1_gene1[2];
        String seq1 = address_seq1_gene1[0] + "_" + address_seq1_gene1[1];
        String seq2 = address_seq2_gene1[0] + "_" + address_seq2_gene1[1];
        coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq1_gene1[2] + "");
        coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq1_gene2[3] + "");

        cluster_coords_per_seq.computeIfAbsent(seq1, k -> new ArrayList<>()).add(address_seq1_gene1[2]);
        cluster_coords_per_seq.computeIfAbsent(seq1, k -> new ArrayList<>()).add(address_seq1_gene2[3]);
        cluster_coords_per_seq.computeIfAbsent(seq1 + "_ali_nr", k -> new ArrayList<>()).add(block_nr);
        cluster_coords_per_seq.computeIfAbsent(seq2 + "_ali_nr", k -> new ArrayList<>()).add(block_nr);
        int cluster2_length;

        if (address_seq2_gene1[2] > address_seq2_gene2[3]) { // reverse complement, inverted?
            cluster2_length = address_seq2_gene1[2] - address_seq2_gene2[3];
            coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq2_gene2[3] + "");
            coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq2_gene1[2] + "");

            cluster_coords_per_seq.computeIfAbsent(seq2, k -> new ArrayList<>()).add(address_seq2_gene2[3]);
            cluster_coords_per_seq.computeIfAbsent(seq2, k -> new ArrayList<>()).add(address_seq2_gene1[2]);
        } else {
            cluster2_length = address_seq2_gene2[3] - address_seq2_gene1[2];
            coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq2_gene1[2] + "");
            coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(address_seq2_gene2[3] + "");
            cluster_coords_per_seq.computeIfAbsent(seq2, k -> new ArrayList<>()).add(address_seq2_gene1[2]);
            cluster_coords_per_seq.computeIfAbsent(seq2, k -> new ArrayList<>()).add(address_seq2_gene2[3]);
        }

        coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(cluster1_length + "");
        coordinate_info_map.computeIfAbsent(block_nr, k -> new ArrayList<>()).add(cluster2_length + "");


    }

    /**
     *
     * @param block_positions
     * @param sequence_info_map
     * @param gene_on_position
     * @param mrna_address_map
     * @return
     */

    private HashMap<String, String> determine_sequence_overlap_with_blocks(HashMap<String, ArrayList<Integer>> block_positions,
                                                                           HashMap<String, String> sequence_info_map,
                                                                           HashMap<String, String> gene_on_position,
                                                                           HashMap<String, int[]> mrna_address_map,
                                                                           HashMap<String, String> renamedIdentifiersMap) {

        // go over the block locations in (steps of two), until no more overlap can be found between any block
        Pantools.logger.info("overlap with sequence.");
        int counter = 0;
        HashMap<String, String> sequence_covered = new HashMap<>();
        for (String seq_combi : block_positions.keySet()) {
            ArrayList<Integer> positions = block_positions.get(seq_combi);
            Pantools.logger.info("{} {}", seq_combi, positions);
            counter++;
            ArrayList<Integer> final_positions = determine_sequence_overlap_in_AL( positions);
            block_positions.put(seq_combi, final_positions);
        }

        Pantools.logger.info("total overlap of blocks.");
        for (String seq_combi : block_positions.keySet()) {
            String[] seq_combi_array = seq_combi.split("#");
            String seqId1 = renamedIdentifiersMap.get(seq_combi_array[0]);
            String seqId2 = renamedIdentifiersMap.get(seq_combi_array[0]);
            ArrayList<Integer> positions = block_positions.get(seq_combi);
            int overlap_counter = 0;
            long total_overlap_size = 0;
            for (int i = 0; i < positions.size()-1; i += 2) {
                int cluster_size = positions.get(i+1) - (positions.get(i)-1);
                Pantools.logger.info("{}#{}    {}#{}", seq_combi_array[0], positions.get(i), seq_combi_array[0], positions.get(i + 1));
                String seq_1_start_gene = gene_on_position.get(seq_combi_array[0] + "#" + positions.get(i));
                String seq_1_end_gene = gene_on_position.get( seq_combi_array[0] + "#" + positions.get(i+1));
                Pantools.logger.info("{}#{} {}#{}", seqId1, seq_1_start_gene, seqId1, seq_1_end_gene);
                int[] seq_1_start_address = mrna_address_map.get(seqId1 + "#" + seq_1_start_gene);
                int[] seq_1_end_address = mrna_address_map.get(seqId1 + "#" + seq_1_end_gene);

                if (seq_1_start_address[2] < seq_1_end_address[3]) {
                    total_overlap_size += seq_1_end_address[3]- (seq_1_start_address[2]-1);
                }
                overlap_counter += cluster_size;
            }

            String genes_of_seq_str = sequence_info_map.get(seq_combi_array[0] + "#genes");
            String length_of_seq_str = sequence_info_map.get(seq_combi_array[0] + "#length");
            if (genes_of_seq_str == null) {
                continue;
            }
            int genes_of_seq = Integer.parseInt(genes_of_seq_str);
            int seq_length = Integer.parseInt(length_of_seq_str);
            String pc_seq_covered = get_percentage_str(overlap_counter, genes_of_seq, 2);
            String pc_seq_length_covered = get_percentage_str(total_overlap_size,  seq_length , 2);
            //System.out.println(seq_combi + " " + overlap_counter + " - " +  genes_of_seq_str + " " + pc_seq_covered + "% "
            //        + total_overlap_size + " " + pc_seq_length_covered + "%");
            sequence_covered.put(seq_combi  + "#genes" , pc_seq_covered);
            sequence_covered.put(seq_combi  + "#length" , pc_seq_length_covered);
        }
        return sequence_covered;
    }

    /**
     *
     * @param geneFrequenciesPerSequence
     * @param renamedIdentifiersMap
     */
    public void createGeneFrequencyCSV(HashMap<String, HashMap<String, ArrayList<String>>> geneFrequenciesPerSequence,
                                       HashMap<String, String> renamedIdentifiersMap) {

        StringBuilder gene_distribution = new StringBuilder("mRNA identifier,Sequence identifier,MCScanX sequence identifier,"
                + "Found in number of blocks,Found in number of sequences,Found in sequences,"
                + "Found in number of genomes,Found in genomes\n");

        for (String sequenceId : geneFrequenciesPerSequence.keySet()) {
            HashMap<String, ArrayList<String>> geneFrequencies = geneFrequenciesPerSequence.get(sequenceId);
            String mcscanxId = "";
            if (renamedIdentifiersMap != null) {
                mcscanxId = renamedIdentifiersMap.get(sequenceId);
            }
            for (String mrnaId : geneFrequencies.keySet()) {
                ArrayList<String> sequenceIds = geneFrequencies.get(mrnaId);
                HashSet<String> sequenceIdSet = new HashSet<>(sequenceIds);
                HashSet<Integer> genomeNrSet = new HashSet<>();
                for (String sequenceIdB : sequenceIdSet) {
                    String[] sequenceIdArray = sequenceIdB.split("_");
                    int genomeNr = Integer.parseInt(sequenceIdArray[0]);
                    genomeNrSet.add(genomeNr);
                }
                String seqIdsString = sequenceIdSet.toString().replace("[", "").replace("]", "").replace(",", "");
                String genomeNrsString = genomeNrSet.toString().replace("[", "").replace("]", "").replace(",", "");
                gene_distribution.append(mrnaId).append(",").append(sequenceId).append(",").append(mcscanxId).append(",").append(sequenceIds.size()).append(",")
                        .append(sequenceIdSet.size()).append(",").append(seqIdsString).append(",").append(genomeNrSet.size()).append(",").append(genomeNrsString).append("\n");
            }
        }
        writeStringToFile(gene_distribution.toString(), WORKING_DIRECTORY + "synteny/statistics/gene_frequency.csv", false, false);
    }

    private HashMap<String, long[]> calcuateSyntenyGenomeGenomeOverlap(HashMap<String, Node[]> mrnaNodesPerSequence,
                                                                             HashMap<String, ArrayList<Integer>> sequenceGenomePositions) {
        HashMap<String, long[]> overlap_per_genome = new HashMap<>(); // key is genome number + "#mrna" or "#bp"
        //System.out.println("genome genome overlap");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String sequenceGenomeKey : sequenceGenomePositions.keySet()) { // key is sequence identifier + "#" + genome number
                String[] keyArray = sequenceGenomeKey.split("#");
                String[] sequenceIdArray = keyArray[0].split("_");

                int genomeNr = Integer.parseInt(sequenceIdArray[0]);
                int genome2Nr = Integer.parseInt(keyArray[1]);
                long[] genome_shared_mrnas;
                long[] genome_shared_bp;
                if (overlap_per_genome.containsKey(genomeNr + "#mrna")) {
                    genome_shared_mrnas = overlap_per_genome.get(genomeNr + "#mrna");
                    genome_shared_bp = overlap_per_genome.get(genomeNr + "#bp");
                } else {
                    genome_shared_mrnas = new long[total_genomes];
                    genome_shared_bp = new long[total_genomes];
                }

                ArrayList<Integer> mrnaPositions = sequenceGenomePositions.get(sequenceGenomeKey);
                mrnaPositions = PhasedFunctionalities.determine_sequence_overlap_in_AL(mrnaPositions);
                Node[] mrnaNodes = mrnaNodesPerSequence.get(keyArray[0]);
                //System.out.println(mrnaPositions);
                long[] mrna_bases_in_blocks = count_genes_bases_in_blocks(mrnaPositions, mrnaNodes); // [mrnas in blocks, bases in blocks]
                //String pc_seq_covered_mrna = get_percentage_str(mrna_bases_in_blocks[0], mrnaNodes.length, 2);
                //System.out.println(keyArray[0] + " " + keyArray[1] + " " + mrna_bases_in_blocks[0] + "/" + mrnaNodes.length + " % mrna = " + pc_seq_covered_mrna);
                genome_shared_mrnas[genome2Nr - 1] += mrna_bases_in_blocks[0];
                genome_shared_bp[genome2Nr - 1] += mrna_bases_in_blocks[1];
                overlap_per_genome.put(genomeNr + "#mrna", genome_shared_mrnas);
                overlap_per_genome.put(genomeNr + "#bp", genome_shared_bp);
            }
            tx.success();
        }
        return overlap_per_genome;
    }

    private int countSyntenyBlocks() {
        int syntenyNodes = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            syntenyNodes = (int) count_nodes(SYNTENY_LABEL);
            if (syntenyNodes == 0) {
                Pantools.logger.error("No synteny blocks were added to the pangenome yet.");
                System.exit(1);
            }
            tx.success();
        }
        return syntenyNodes;
    }

    public void read_synteny_blocks(HashMap<String, Node[]> mrnaNodesPerSequence, HashMap<Node, Integer> mrna_position_map,
                                    HashMap<String, Integer> homologs_per_seq_and_combi) {

        int syntenyBlocks = countSyntenyBlocks();

        Node[] synteny_nodes_array = new Node[syntenyBlocks];
        int[][] block_mrna_positions = new int[syntenyBlocks][4]; // [block number] [ seq1 block start, seq1 block end, seq2 block start  seq2 block end]
        HashMap<String, int[]> block_mrna_position_map = new HashMap<>();
        int[][] block_sizes = new int[syntenyBlocks][3]; // [block number] [syntenic genes, block size sequence 1, block size sequence 2]
        int[][] block_coordinates = new int[syntenyBlocks][4]; // [block number] [seq1 block start, seq1 block end, seq2 block start  seq2 block end]
        boolean[] invertedBlocks = new boolean[syntenyBlocks];
        HashMap<String, int[]> shared_blocks_map = new HashMap<>();
        // key is two sequence identifiers seperated by a '#'. value is [number of blocks, number of genes in blocks, inverted blocks]
        HashMap<String, HashSet<Node>> syntentic_mrna_nodes_per_sequence = new HashMap<>();
        HashMap<String, ArrayList<Integer>> blocksPerSequence = new HashMap<>(); // key is sequence identifier. value is list with block numbers
        HashMap<String, String> mrnasPerBlock = new HashMap<>(); // key is block number + sequence identifier, value is string with concatenated mRNA identifiers
        HashMap<String, HashSet<Node>> collinearGeneNodes = new HashMap<>();

        HashMap<String, HashMap<String, ArrayList<String>>> geneFrequenciesPerSequence = new HashMap<>(); // key is sequence identifiers (1_1)
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
            while (synteny_nodes.hasNext()) {
                Node synteny_node = synteny_nodes.next();
                int blockNr = (int) synteny_node.getProperty("block_number");
                int block_size = (int) synteny_node.getProperty("block_size");
                ///String renamed_ids = (String) synteny_node.getProperty("sequence_identifiers_mcscanx");
                String sequence_ids = (String) synteny_node.getProperty("sequence_identifiers");
                String[] sequenceIdsArray = sequence_ids.split(",");
                String[] sequenceOneIdArray = sequenceIdsArray[0].split("_");
                String[] sequenceTwoIdArray = sequenceIdsArray[1].split("_");
                int genomeNr1 = Integer.parseInt(sequenceOneIdArray[0]);
                int sequenceNr1 = Integer.parseInt(sequenceOneIdArray[1]);
                int genomeNr2 = Integer.parseInt(sequenceTwoIdArray[0]);
                int sequenceNr2 = Integer.parseInt(sequenceTwoIdArray[1]);
                if (skip_seq_array[genomeNr1-1][sequenceNr1-1] || skip_seq_array[genomeNr2-1][sequenceNr2-1]) {
                    continue;
                }

                boolean block_inverted = (boolean) synteny_node.getProperty("block_inverted");
                invertedBlocks[blockNr] = block_inverted;
                synteny_nodes_array[blockNr] = synteny_node;
                String[] seq_array = sequence_ids.split(",");
                //System.out.println(blockNr + " " + block_size + " " + renamed_ids + " " + sequence_ids + " " + block_inverted);
                String sequence_combi = sequence_ids.replace(",","#");
                blocksPerSequence.computeIfAbsent(seq_array[0], k -> new ArrayList<>()).add(blockNr);
                blocksPerSequence.computeIfAbsent(seq_array[1], k -> new ArrayList<>()).add(blockNr);

                int[] values;
                if (!shared_blocks_map.containsKey(sequence_combi)) {
                    values = new int[3];
                } else {
                    values = shared_blocks_map.get(sequence_combi);
                }
                values[0]++;
                values[1] += block_size;
                if (block_inverted) {
                    values[2]++;
                }
                shared_blocks_map.put(sequence_combi, values);

                Node[][] mrnas_in_order_seq = retrieveOrderMrnasFromBlock(synteny_node, block_size, seq_array[0]);
                Node[] mrnas_in_order_seq1 = mrnas_in_order_seq[0];
                Node[] mrnas_in_order_seq2 = mrnas_in_order_seq[1];

                countGeneFrequencies(mrnas_in_order_seq, seq_array, geneFrequenciesPerSequence, collinearGeneNodes);

                String mrnaIds1 = getConcatenatedIdsOfMrnaNodes(mrnas_in_order_seq1);
                String mrnaIds2 = getConcatenatedIdsOfMrnaNodes(mrnas_in_order_seq2);
                mrnasPerBlock.put(blockNr + "#" + seq_array[0], mrnaIds1);
                mrnasPerBlock.put(blockNr + "#" + seq_array[1], mrnaIds2);

                int seq1_start_pos = mrna_position_map.get(mrnas_in_order_seq1[0]);
                int seq1_end_pos = mrna_position_map.get(mrnas_in_order_seq1[mrnas_in_order_seq1.length-1]);
                int seq2_start_pos = mrna_position_map.get(mrnas_in_order_seq2[0]);
                int seq2_end_pos = mrna_position_map.get(mrnas_in_order_seq2[mrnas_in_order_seq2.length-1]);
                int[] seq1_first_node_address = (int[]) mrnas_in_order_seq1[0].getProperty("address");
                int[] seq1_last_node_address = (int[]) mrnas_in_order_seq1[mrnas_in_order_seq1.length-1].getProperty("address");
                int[] seq2_first_node_address = (int[]) mrnas_in_order_seq2[0].getProperty("address");
                int[] seq2_last_node_address = (int[]) mrnas_in_order_seq2[mrnas_in_order_seq2.length-1].getProperty("address");

                block_mrna_position_map.put(blockNr + "#" + seq_array[0], new int[]{seq1_start_pos, seq1_end_pos}) ;
                block_coordinates[blockNr][0] = seq1_first_node_address[2];
                block_coordinates[blockNr][1] = seq1_last_node_address[3];
                int block1_size = (seq1_end_pos - seq1_start_pos) + 1;
                int block2_size = (seq2_end_pos - seq2_start_pos) + 1;
                if (block_inverted) {
                    block_coordinates[blockNr][2] = seq2_last_node_address[2];
                    block_coordinates[blockNr][3] = seq2_first_node_address[3];
                    block2_size = (seq2_start_pos - seq2_end_pos) + 1;
                    block_mrna_position_map.put(blockNr +"#" + seq_array[1], new int[]{seq2_end_pos, seq2_start_pos}) ;
                } else {
                    block_coordinates[blockNr][2] = seq2_first_node_address[2];
                    block_coordinates[blockNr][3] = seq2_last_node_address[3];
                    block_mrna_position_map.put(blockNr + "#" + seq_array[1], new int[]{seq2_start_pos, seq2_end_pos}) ;
                }
                block_sizes[blockNr][0] = block_size;
                block_sizes[blockNr][1] = block1_size;
                block_sizes[blockNr][2] = block2_size;
            }
            tx.success();
        }

        createGeneFrequencyCSV(geneFrequenciesPerSequence, null);

        HashMap<String, String> blockOverlap = check_if_synteny_blocks_fall_in_other_blocks(blocksPerSequence, block_mrna_position_map);

        int total_syntenic_genes = 0;
        for (String sequence_id : syntentic_mrna_nodes_per_sequence.keySet()) {
            int syntenic_genes_in_seq = syntentic_mrna_nodes_per_sequence.get(sequence_id).size();
            total_syntenic_genes += syntenic_genes_in_seq;
        }

        int total_blocks = 0, total_inv_blocks = 0, gene_pair_counter = 0;
        for (String sequence_combi : shared_blocks_map.keySet()) {
            int[] shared_blocks_genes = shared_blocks_map.get(sequence_combi); // [number of blocks, number of genes in blocks, inverted blocks]
            total_blocks += shared_blocks_genes[0];
            total_inv_blocks += shared_blocks_genes[2];
            gene_pair_counter += shared_blocks_genes[1];
        }

        Pantools.logger.info("blocks : {}", total_blocks);
        Pantools.logger.info("inverted blocks : {}", total_inv_blocks);
        Pantools.logger.info("collinear gene pairs: {}", gene_pair_counter);
        Pantools.logger.info("genes in blocks : {}", total_syntenic_genes);
        report_block_statistics2(shared_blocks_map);

        HashMap<String, Integer> collinearGeneCounts = PhasedFunctionalities.calculate_syntenic_genes_per_sequence_genome(collinearGeneNodes);

        determine_sequence_overlap_with_blocks2(blocksPerSequence, mrnaNodesPerSequence,
                block_mrna_position_map, synteny_nodes_array, collinearGeneCounts, homologs_per_seq_and_combi, mrnasPerBlock, invertedBlocks, blockOverlap);

        // "synteny_blocks_statistics.csv"
    }

    /**
     *
     * @param blocksPerSequence key is sequence identifier. value is list with block numbers
     * @param block_mrna_position_map
     */
    public static HashMap<String, String> check_if_synteny_blocks_fall_in_other_blocks(HashMap<String, ArrayList<Integer>> blocksPerSequence,
                                                                                       HashMap<String, int[]> block_mrna_position_map) {
        HashMap<String, String> blockOverlap = new HashMap<>();
        for (String seq_id : blocksPerSequence.keySet()) {
            ArrayList<Integer> blocks = blocksPerSequence.get(seq_id);
            //System.out.println("\n" + seq_id + " " + blocks);

            for (int i = 0; i < blocks.size(); i++) {
                int block_nr = blocks.get(i);
                StringBuilder overlappedBy = new StringBuilder();
                StringBuilder overlaps = new StringBuilder();
                StringBuilder partialOverlap = new StringBuilder();
                int[] first_last_position_of_block = block_mrna_position_map.get(block_nr + "#" + seq_id);
                int block1_start = first_last_position_of_block[0];
                int block1_end = first_last_position_of_block[1];
                //System.out.println(Arrays.toString(first_last_position_of_block));
                for (int j = 0; j < blocks.size(); j++) {
                    if (i == j) {
                        continue;
                    }
                    int block2_nr = blocks.get(j);
                    int[] first_last_position_of_block2 = block_mrna_position_map.get(block2_nr +"#" + seq_id);
                    int block2_start = first_last_position_of_block2[0];
                    int block2_end = first_last_position_of_block2[1];
                    if (block2_start <= block1_start && block2_end >= block1_end) {
                        overlappedBy.append(block2_nr + " ");
                    } else if (block2_start >= block1_start && block2_end <= block1_end) {
                        overlaps.append(block2_nr + " ");
                    } else if (block2_start >= block1_start && block2_end >= block1_end && block2_start < block1_end) {
                        partialOverlap.append(block2_nr + " ");
                    } else if (block2_start <= block1_start && block2_end <= block1_end && block2_end > block1_start) {
                        partialOverlap.append(block2_nr + " ");
                    } else { // no overlap

                    }
                }
                Pantools.logger.info("{}#{}, {}, {}, {}", block_nr, seq_id, overlappedBy, overlaps, partialOverlap);
                blockOverlap.put(block_nr + "#" + seq_id, overlappedBy + "," + overlaps + "," + partialOverlap);
            }
        }
        return blockOverlap;
    }

    private void countGeneFrequencies(Node[][] mrnas_in_order_seq, String[] sequenceIdArray,
                                      HashMap<String, HashMap<String, ArrayList<String>>> geneFrequenciesPerSequence,
                                      HashMap<String, HashSet<Node>> collinear_genes_map) {

        String sequenceIdA = sequenceIdArray[0];
        String sequenceIdB = sequenceIdArray[1];
        for (int i = 0; i < mrnas_in_order_seq[0].length; i++) {
            String mrnaIdA = (String) mrnas_in_order_seq[0][i].getProperty("protein_ID");
            String mrnaIdB = (String) mrnas_in_order_seq[1][i].getProperty("protein_ID");
            increase_frequencies_for_gene(geneFrequenciesPerSequence, sequenceIdA + "#" + mrnaIdA, sequenceIdB + "#" + mrnaIdB);
            increase_frequencies_for_gene(geneFrequenciesPerSequence, sequenceIdB + "#" + mrnaIdB, sequenceIdA + "#" + mrnaIdA);

            collinear_genes_map.computeIfAbsent(sequenceIdA + "#" + sequenceIdB + "#" + sequenceIdA, k -> new HashSet<>()).add(mrnas_in_order_seq[0][i]);
            collinear_genes_map.computeIfAbsent(sequenceIdA + "#" + sequenceIdB + "#" + sequenceIdB, k -> new HashSet<>()).add(mrnas_in_order_seq[1][i]);
        }
    }

    private Node[][] retrieveOrderMrnasFromBlock(Node syntenyNode, int block_size, String sequenceIdA) {
        Node[][] mrnas_in_order_seq = new Node[2][block_size];
        //Node[] mrnas_in_order_seq1 = new Node[block_size];
        //Node[] mrnas_in_order_seq2 = new Node[block_size];
        Iterable<Relationship> relations = syntenyNode.getRelationships(); // part_of relationships
        for (Relationship rel : relations) {
            int position = (int) rel.getProperty("position"); // position starts at 0
            Node mrnaNode = rel.getStartNode();
            int genomeNr = (int) mrnaNode.getProperty("genome");
            int sequenceNr = (int) mrnaNode.getProperty("sequence");
            String seq_id = genomeNr + "_" + sequenceNr;

            // deze vullen na afloop
            //collinear_genes_map.computeIfAbsent(sequence_combi + "#" + seq_id, k -> new HashSet<>()).add(mrnaNode);
            if (sequenceIdA.equals(seq_id)) {
                mrnas_in_order_seq[0][position] = mrnaNode;
            } else {
                mrnas_in_order_seq[1][position] = mrnaNode;
            }
        }

        return mrnas_in_order_seq;
    }

    /**
     *
     * @param first_last_gene_seq1
     * @param first_last_gene_seq2
     * @param gene_position
     * @param block_size_map
     * @param block_counter
     * @param block_positions
     * @param sequence_combi
     */
    public static void get_gene_positions_from_synteny_block(String[] first_last_gene_seq1, String[] first_last_gene_seq2, HashMap<String, Integer> gene_position,
                                                             HashMap<String, String> block_size_map, int block_counter, HashMap<String, ArrayList<Integer>> block_positions,
                                                             String sequence_combi, ArrayList<Boolean> inverted_blocks, HashMap<String, ArrayList<Integer>> block_sizes_between_seqs) {

        String[] seq_array = sequence_combi.split("#");
        if (!gene_position.containsKey(first_last_gene_seq1[0]) || !gene_position.containsKey(first_last_gene_seq1[1])) {
            return;
        }
        int block1_start_gene_pos = gene_position.get(first_last_gene_seq1[0]);
        int block1_end_gene_pos = gene_position.get(first_last_gene_seq1[1]);

        int block_size = block1_end_gene_pos - (block1_start_gene_pos-1);
        if (block_size < 1) {
            Pantools.logger.error("{} _positions {} {} {}", block_size, block1_start_gene_pos, block1_end_gene_pos, Arrays.toString(first_last_gene_seq1));
            System.exit(1);
        }
        block_size_map.put(block_counter + "_total_" + seq_array[0], block_size +"");
        block_positions.computeIfAbsent(sequence_combi, k -> new ArrayList<>()).add(block1_start_gene_pos);
        block_positions.computeIfAbsent(sequence_combi, k -> new ArrayList<>()).add(block1_end_gene_pos);

        //System.out.println(Arrays.toString(first_last_gene_seq2));
        int block2_start_gene_pos = gene_position.get(first_last_gene_seq2[0]);
        int block2_end_gene_pos = gene_position.get(first_last_gene_seq2[1]);
        int block_size2;
        if (block2_start_gene_pos < block2_end_gene_pos) {
            block_positions.computeIfAbsent(seq_array[1] + "#" + seq_array[0], k -> new ArrayList<>()).add(block2_start_gene_pos);
            block_positions.computeIfAbsent(seq_array[1] + "#" + seq_array[0], k -> new ArrayList<>()).add(block2_end_gene_pos);
            block_size2 = block2_end_gene_pos - (block2_start_gene_pos-1);
            //System.out.println("1. " + block_size + " " + block_size2);
            //System.out.println(" " + block1_start_gene_pos + " " + block2_end_gene_pos);
            inverted_blocks.add(false);
        } else { // block is inverted
            block_positions.computeIfAbsent(seq_array[1] + "#" + seq_array[0], k -> new ArrayList<>()).add(block2_end_gene_pos);
            block_positions.computeIfAbsent(seq_array[1] + "#" + seq_array[0], k -> new ArrayList<>()).add(block2_start_gene_pos);
            block_size2 = block2_start_gene_pos - (block2_end_gene_pos-1);
            //System.out.println("2. " + block_size + " " + block_size2);
            inverted_blocks.add(true);
        }
        block_size_map.put(block_counter + "_total_" + seq_array[1], block_size2 +"");
        // en het blok voor de 2e sequentie?
        //System.out.println(block_counter + "_total_" + seq_array[0] + " EN " + block_counter + "_total_" + seq_array[1]);

        block_sizes_between_seqs.computeIfAbsent(seq_array[0] + "#" + seq_array[1] + "#total", k -> new ArrayList<>()).add(block_size);
        block_sizes_between_seqs.computeIfAbsent(seq_array[1] + "#" + seq_array[0] + "#total", k -> new ArrayList<>()).add(block_size2);

        // doet wat met
        // HashMap<String, ArrayList<Integer>> block_sizes_between_seqs
    }

    /**
     * Find out how many genes (and bases) of a sequence are part of syntenic blocks
     *
     * @param blocksPerSequence key is sequence identifier. value is list with block numbers
     * @param mrnaNodesPerSequence
     * @param block_mrna_position_map
     * @param synteny_nodes_array
     * @param collinearGeneCounts
     * @param homologs_per_seq_and_combi
     * @return
     */
    public HashMap<String, String> determine_sequence_overlap_with_blocks2(HashMap<String, ArrayList<Integer>> blocksPerSequence,
                                                                           HashMap<String, Node[]> mrnaNodesPerSequence,
                                                                           HashMap<String, int[]> block_mrna_position_map,
                                                                           Node[] synteny_nodes_array,
                                                                           HashMap<String, Integer> collinearGeneCounts,
                                                                           HashMap<String, Integer> homologs_per_seq_and_combi,
                                                                           HashMap<String, String> mrnasPerBlock, boolean[] invertedBlocks, HashMap<String, String> blockOverlap) {

        //put in function?
        HashMap<String, Long> sequence_length_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequence_nodes.hasNext()) {
                Node seq_node = sequence_nodes.next();
                String identifier = (String) seq_node.getProperty("identifier");
                long seq_length = (long) seq_node.getProperty("length");
                sequence_length_map.put(identifier, seq_length);
            }
            tx.success();
        }

        HashMap<String, Integer> sequence_position_map = new HashMap<>();
        //System.out.println("all sequences " + selectedSequences);
        int counter = 0;
        for (String sequenceId : selectedSequences) {
            sequence_position_map.put(sequenceId, counter);
            counter++;
        }
        HashMap<Integer, ArrayList<String>> blockInfo = new HashMap<>();
        HashMap<String, String[]> sequence_overlap_per_sequence = new HashMap<>();

        HashMap<String, String> sequence_covered = new HashMap<>();
        HashMap<Integer, TreeSet<Integer>> blocks_per_genome = new HashMap<>();
        String pc_seq_covered_mrna;
        String pc_seq_covered_bp;
        long[] mrnasBasesInBlocks;

        HashMap<String, ArrayList<Integer>> sequenceGenomePositions = new HashMap<>();

        int highest_block_nr = 0;
        long[][] genes_bases_per_genome = new long[total_genomes][4]; // [genome number][total genes, total bases, shared genes, shared bases]
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Pantools.logger.info("new % sequence overlap.");
            for (String sequenceId : blocksPerSequence.keySet()) {
                String[] sequenceIdArray = sequenceId.split("_"); // 1_1 becomes [1,1]
                int genomeNr = Integer.parseInt(sequenceIdArray[0]);
                ArrayList<Integer> blocks = blocksPerSequence.get(sequenceId);
                Node[] mrnaNodes = mrnaNodesPerSequence.get(sequenceId);
                Node firstmrnaNode = mrnaNodes[0];
                Node last_mrna_of_seq = mrnaNodes[mrnaNodes.length-1];
                int[] start_address_seq = (int[]) firstmrnaNode.getProperty("address");
                int[] end_address_seq = (int[]) last_mrna_of_seq.getProperty("address");
                int seq_length_genes = (end_address_seq[3] - start_address_seq[2]) + 1;

                long seq_length = sequence_length_map.get(sequenceId);
                Pantools.logger.info("SEQUENCE {} {} blocks, {} mrnas, {}bp {}bp", sequenceId, blocks.size(), mrnaNodes.length, seq_length, seq_length_genes);
                ArrayList<Integer> all_blocks_start_stop_positions = new ArrayList<>();
                HashMap<String, ArrayList<Integer>> mrnaPositionsTwoSequences = new HashMap<>();
                HashSet<String> other_seq_ids = new HashSet<>(); //
                for (int blockNr : blocks) {
                    if (blockNr > highest_block_nr) {
                        highest_block_nr = blockNr;
                    }
                    blocks_per_genome.computeIfAbsent(genomeNr, k -> new TreeSet<>()).add(blockNr);
                    Node syntenyNode = synteny_nodes_array[blockNr];
                    int syntenic_genes = (int) syntenyNode.getProperty("block_size");
                    String sequenceCombination = (String) syntenyNode.getProperty("sequence_identifiers");
                    String sequence2Id = sequenceCombination.replace(sequenceId + ",", "").replace("," + sequenceId, "");
                    String[] sequence2IdArray = sequence2Id.split("_");
                    int genome2 = Integer.parseInt(sequence2IdArray[0]);
                    ArrayList<Integer> mrna_positions_between_seqs = new ArrayList<>();
                    if (mrnaPositionsTwoSequences.containsKey(sequence2Id)) {
                        mrna_positions_between_seqs = mrnaPositionsTwoSequences.get(sequence2Id);
                    }
                    int[] first_last_position_of_block = block_mrna_position_map.get(blockNr + "#" + sequenceId);
                    int start_position = first_last_position_of_block[0];
                    int end_position = first_last_position_of_block[1];
                    Node start_mrna = mrnaNodes[start_position - 1];
                    Node end_mrna = mrnaNodes[end_position - 1];
                    int[] start_address = (int[]) start_mrna.getProperty("address");
                    int[] end_address = (int[]) end_mrna.getProperty("address");

                    int block_length_bp = (end_address[3] - start_address[2]) + 1;
                    int block_size_mrnas = (end_position - start_position) - 1;
                    String pc_seq_covered = get_percentage_str(block_size_mrnas, mrnaNodes.length, 2);
                    String pc_seq_length_covered = get_percentage_str(block_length_bp, seq_length_genes, 2);
                    Pantools.logger.info(
                            "  {} {} {} {} {}{}-{}={} gene pc {} bases pc {}",
                            blockNr,
                            sequence2Id,
                            Arrays.toString(first_last_position_of_block),
                            start_mrna,
                            end_mrna,
                            start_address[2],
                            end_address[3],
                            block_length_bp,
                            pc_seq_covered,
                            pc_seq_length_covered
                    );

                    blockInfo.computeIfAbsent(blockNr, k -> new ArrayList<>()).add(
                            sequenceId + "#" +
                            syntenic_genes + "#" +
                            block_size_mrnas + "#" +
                            block_length_bp + "#" +
                            pc_seq_covered + "#" +
                            pc_seq_length_covered + "#" +
                            mrnaNodes.length + "#" +
                            seq_length_genes + "#" +
                            mrnasPerBlock.get(blockNr + "#" + sequenceId) + "#" +
                            mrnasPerBlock.get(blockNr + "#" + sequence2Id) + "#" +
                            invertedBlocks[blockNr] + "#" +
                            start_address[2] + "#" +
                            end_address[3]);

                    all_blocks_start_stop_positions.add(start_position);
                    all_blocks_start_stop_positions.add(end_position);
                    other_seq_ids.add(sequence2Id);

                    mrna_positions_between_seqs.add(start_position);
                    mrna_positions_between_seqs.add(end_position);
                    mrnaPositionsTwoSequences.put(sequence2Id, mrna_positions_between_seqs);

                    // store in sequence to genome hashmap so each mrna & bp can only be counted once
                    sequenceGenomePositions.computeIfAbsent(sequenceId + "#" + genome2, k -> new ArrayList<>()).add(start_position);
                    sequenceGenomePositions.computeIfAbsent(sequenceId + "#" + genome2, k -> new ArrayList<>()).add(end_position);
                }

                // how much of the current sequence consists of at least 1 synteny block
                ArrayList<Integer> combined_start_stop_positions = PhasedFunctionalities.determine_sequence_overlap_in_AL(all_blocks_start_stop_positions);
                mrnasBasesInBlocks = count_genes_bases_in_blocks(combined_start_stop_positions, mrnaNodes); // [mrnas in blocks, bases in blocks]
                pc_seq_covered_mrna = get_percentage_str(mrnasBasesInBlocks[0], mrnaNodes.length, 2);
                pc_seq_covered_bp = get_percentage_str(mrnasBasesInBlocks[1], seq_length_genes , 2);

                genes_bases_per_genome[genomeNr-1][0] += mrnaNodes.length; // [genome number][total genes, total bases, shared genes, shared bases]
                genes_bases_per_genome[genomeNr-1][1] += seq_length_genes; // total bases
                genes_bases_per_genome[genomeNr-1][2] += mrnasBasesInBlocks[0]; // shared genes
                genes_bases_per_genome[genomeNr-1][3] += mrnasBasesInBlocks[1]; // shared bases

                Pantools.logger.info("collapsed against everyting {} {} genes, pc {}. {}bp, pc {}",
                        combined_start_stop_positions,
                        mrnasBasesInBlocks[0],
                        pc_seq_covered_mrna,
                        mrnasBasesInBlocks[1],
                        pc_seq_covered_bp
                );
                String[] overlap_per_sequence = new String[selectedSequences.size()];
                int position = sequence_position_map.get(sequenceId);
                String other_seq_ids_str = other_seq_ids.toString().replace("[","").replace("]","").replace(", "," ");
                String block_numbers = blocks.toString().replace("[","").replace("]","").replace(", "," ");
                overlap_per_sequence[position] = blocks.size() + "," + mrnaNodes.length + "," + seq_length_genes + "," +
                        mrnasBasesInBlocks[0] + "," + mrnasBasesInBlocks[1]
                        + "," + pc_seq_covered_mrna + "," + pc_seq_covered_bp + "," + block_numbers + "," + other_seq_ids_str + "," + other_seq_ids.size() + ",";


                // when looking at 1 other sequence. how much of the current sequence consists of at least 1 synteny block
                for (String sequence2Id : mrnaPositionsTwoSequences.keySet()) {
                    //String[] other_seq_array = sequence2Id.split("_");
                    //int genome2 = Integer.parseInt(other_seq_array[0]);
                    int position2 = sequence_position_map.get(sequence2Id);
                    ArrayList<Integer> positions_for_seq = mrnaPositionsTwoSequences.get(sequence2Id);
                    int blocks_with_other_seq = positions_for_seq.size()/2;
                    ArrayList<Integer> combined_positions_for_seq = PhasedFunctionalities.determine_sequence_overlap_in_AL(positions_for_seq);
                    mrnasBasesInBlocks = count_genes_bases_in_blocks(combined_positions_for_seq, mrnaNodes);  // [mrnas in blocks, bases in blocks]
                    pc_seq_covered_mrna = get_percentage_str(mrnasBasesInBlocks[0], mrnaNodes.length, 2);
                    pc_seq_covered_bp = get_percentage_str(mrnasBasesInBlocks[1], seq_length_genes, 2);
                    Pantools.logger.info(
                        "  against {} {} {} genes, pc {}. {}bp, pc {}",
                        sequence2Id,
                        combined_positions_for_seq,
                        mrnasBasesInBlocks[0],
                        pc_seq_covered_mrna,
                        mrnasBasesInBlocks[1],
                        pc_seq_covered_bp
                    );
                    overlap_per_sequence[position2] = blocks_with_other_seq + "," + mrnasBasesInBlocks[0] + "," + mrnasBasesInBlocks[1] + ","
                            + pc_seq_covered_mrna + "," + pc_seq_covered_bp + ",";
                }
                sequence_overlap_per_sequence.put(sequenceId, overlap_per_sequence);
            }
            tx.success();
        }

        HashMap<String, long[]> overlap_per_genome = calcuateSyntenyGenomeGenomeOverlap(mrnaNodesPerSequence, sequenceGenomePositions);
        create_overlap_per_block_file(highest_block_nr, blockInfo, blockOverlap);
        create_overlap_per_sequence_file(sequence_overlap_per_sequence, sequence_position_map);

        HashMap<String, Integer> blockCountsBetweenGenomes = createBlocksPerGenome( blocks_per_genome);
        createOverlapPerGenomeFile(genes_bases_per_genome, overlap_per_genome, blockCountsBetweenGenomes, collinearGeneCounts,
                homologs_per_seq_and_combi);

        Pantools.logger.info("new is done.");
        return sequence_covered;
    }

    private HashMap<String, Integer> createBlocksPerGenome(HashMap<Integer, TreeSet<Integer>> blocks_per_genome) {
        HashMap<String, Integer> blockCountsBetweenGenomes = new HashMap<>();
        StringBuilder csv = new StringBuilder();
        Pantools.logger.info("shared blocks.");
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            TreeSet<Integer> blocksA = blocks_per_genome.get(i);
            TreeSet<Integer> allFoundBlocks = new TreeSet<>();
            TreeSet<Integer> foundBlocksSameGenome = new TreeSet<>();
            blockCountsBetweenGenomes.put(i + "" , blocksA.size());
            for (int j = 1; j <= total_genomes; j++) {
                if (skip_array[j - 1] || i == j) {
                    continue;
                }
                TreeSet<Integer> blocksB = blocks_per_genome.get(j);
                TreeSet<Integer> foundBlocks = new TreeSet<>();
                for (int blockNr : blocksA) {
                    if (blocksB.contains(blockNr)) {
                        foundBlocks.add(blockNr);
                        allFoundBlocks.add(blockNr);
                    }
                }
                //System.out.println(i + " " + j + " " + foundBlocks.size());
                if (i < j) {
                    csv.append("Between ").append(i).append(" and ").append(j).append(". ").append(foundBlocks.size()).append(" groups\n")
                            .append(foundBlocks.toString().replace("[","").replace("]","").replace(" ","")).append("\n");
                }
                blockCountsBetweenGenomes.put(i + "_" + j, foundBlocks.size());
            }

            // blocks that were not found in other genomes are against the same genome
            for (int blockNr : blocksA) {
                if (!allFoundBlocks.contains(blockNr)) {
                    foundBlocksSameGenome.add(blockNr);
                }
            }
            csv.append(i).append(" to itself. ").append(foundBlocksSameGenome.size()).append(" blocks\n")
                    .append(foundBlocksSameGenome.toString().replace("[","").replace("]","").replace(" ","")).append("\n");
            blockCountsBetweenGenomes.put(i + "_" + i, foundBlocksSameGenome.size());
        }
        writeStringToFile(csv.toString(), WORKING_DIRECTORY + "synteny/statistics/blocks_between_genomes.csv", false, false);
        return blockCountsBetweenGenomes;
    }

    // aantal homologen aan tabel toevoegen?

    /**
     *
     * @param genes_bases_per_genome [genome number][total genes, total bases, shared genes, shared bases]
     * @param overlap_per_genome
     * @param blockCountsBetweenGenomes
     * @param collinearGeneCounts
     * @param homologs_per_seq_and_combi
     */
    private void createOverlapPerGenomeFile(long[][] genes_bases_per_genome, HashMap<String, long[]> overlap_per_genome,
                                               HashMap<String, Integer> blockCountsBetweenGenomes,
                                               HashMap<String, Integer> collinearGeneCounts,
                                               HashMap<String, Integer> homologs_per_seq_and_combi) {

        Pantools.logger.info("complete genome with synteny block overlap.");

        // create the header
        StringBuilder csv = new StringBuilder("Genome number,Blocks,Total mRNAs of genome,mRNAs in blocks,% of mRNAs in blocks," +
                "Collinear mRNAs (form at least 1 syntenic pair),% of mRNAs are collinear (form at least 1 syntenic pair)," +
                "homologous mRNAs,% of mRNAs are homologous," +
                "Genome size (bp),Bases in blocks,% of bases in blocks,,");

        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            csv.append("Blocks with genome ").append(i)
                    .append(",mRNAs in blocks to genome ").append(i).append(",% of mRNA's in block to genome " ).append(i)
                    .append(",Collinear mRNAs with genome ").append(i).append(",% of mRNA's collinear to genome " ).append(i)
                    .append(",homologous mRNAs with genome ").append(i).append(",% of mRNA's homologous to genome " ).append(i)
                    .append(",Bases in blocks to genome ").append(i).append(",% of bases in blocks to genome ").append(i).append(",,"); // added two commas so its table is better interpretable in spreadsheet
        }
        csv.append("\n"); // end of output file header

        // i and j are genome numbers
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            int totalBlocks = blockCountsBetweenGenomes.get(i +"");
            long totalMrnasGenome = genes_bases_per_genome[i-1][0]; // all genes of a genome
            long mrnasInBlocks = genes_bases_per_genome[i-1][2]; // mRNAs part of synteny blocks
            long totalBases = genes_bases_per_genome[i-1][1]; // all bases of a genome
            long nucleotidesInblocks = genes_bases_per_genome[i-1][3]; // all bases of a genome

            long homologsGenome = homologs_per_seq_and_combi.getOrDefault(i +"", 0); //
            int totalCollinearGenesGenome = collinearGeneCounts.get(i +""); // genes that are part of syntenic pairs

            String pc_covered_mrna_all = get_percentage_str(mrnasInBlocks, totalMrnasGenome, 2);
            String pc_covered_bp_all = get_percentage_str(nucleotidesInblocks, totalBases, 2);
            String percentageGenomeCollinear = get_percentage_str(totalCollinearGenesGenome, totalMrnasGenome, 2);
            String percentageGenomehomologous = get_percentage_str(homologsGenome, totalMrnasGenome, 2);

            long[] sharedMrnasToGenomes = overlap_per_genome.get(i + "#mrna"); // [ shared mrnas genome 1, shared mrnas genome 2, etc]
            long[] sharedBasesToGenomes = overlap_per_genome.get(i + "#bp");

            csv.append(i).append(",").append(totalBlocks).append(",") .append(totalMrnasGenome).append(",")
                    .append(mrnasInBlocks).append(",").append(pc_covered_mrna_all).append(",")
                    .append(totalCollinearGenesGenome).append(",").append(percentageGenomeCollinear).append(",")
                    .append(homologsGenome).append(",").append(percentageGenomehomologous).append(",")
                    .append(totalBases).append(",").append(nucleotidesInblocks).append(",").append(pc_covered_bp_all).append(",,");

            for (int j = 1; j <= total_genomes; j++) {
                if (skip_array[j-1]) {
                    continue;
                }
                String genome_key = i + "#" + j + "#" + i;
                if (j < i) {
                    genome_key = j + "#" + i + "#" + i;
                }
                System.out.println("between genomes " + genome_key);
                int sharedBlocks = blockCountsBetweenGenomes.get(i +"_" + j);
                int collinearGenes = 0, homologsBetweenGenomes = 0;
                if (collinearGeneCounts.containsKey(genome_key)) {
                    collinearGenes = collinearGeneCounts.get(genome_key);
                    if (homologs_per_seq_and_combi.containsKey(genome_key)) {
                        homologsBetweenGenomes = homologs_per_seq_and_combi.get(genome_key);
                        System.out.println(genome_key + " " + homologsBetweenGenomes);
                    }
                }
                //System.out.println("  " + collinearGenes + " " + genome_combi_homologs);
                String pc_covered_mrna = percentageAsString(sharedMrnasToGenomes[j-1], totalMrnasGenome, 2);
                String pc_seq_covered_bp = percentageAsString(sharedBasesToGenomes[j-1], totalBases , 2);

                String percentageCollinearOtherGenome = get_percentage_str(collinearGenes, totalMrnasGenome, 2);
                String percentageHomologsOtherGenome = get_percentage_str(homologsBetweenGenomes, totalMrnasGenome, 2);

                //System.out.println("!!! covered mrna " + mrna_with_genomes[j-1] + " " + total_mrnas + " -> " + pc_covered_mrna);
                csv.append(sharedBlocks).append(",")
                        .append(sharedMrnasToGenomes[j-1]).append(",").append(pc_covered_mrna).append(",")  // mrnas in blocks & percentage
                        .append(collinearGenes).append(",").append(percentageCollinearOtherGenome).append(",") // collinear genes & percentage
                        .append(homologsBetweenGenomes).append(",").append(percentageHomologsOtherGenome).append(",") // homologous genes & percentage
                        .append(sharedBasesToGenomes[j-1]).append(",").append(pc_seq_covered_bp).append(",,"); // bases in blocks & percentage
            }
            csv.append("\n");
        }
        writeStringToFile(csv.toString(), WORKING_DIRECTORY + "synteny/statistics/genome_overlap.csv", false, false);
    }

    public void create_overlap_per_sequence_file(HashMap<String, String[]> sequence_overlap_per_sequence, HashMap<String, Integer> sequence_position_map) {
        StringBuilder outputBuilder = new StringBuilder();
        outputBuilder.append("Sequence identifier,Number of blocks,Number of mRNAs on sequence,Sequence length (first and last mRNA)," +
                "Blocks overlap number of mRNAs,Blocks overlap bp,% of sequence's mRNAs overlap,% of sequence's mRNAs overlap,Block numbers,Sequences in blocks,"
                + "Syntenic to number of sequences,");

        // selectedSequence contains only sequences that have synteny
        for (String sequenceId : selectedSequences) {
            outputBuilder.append("Number of blocks with ").append(sequenceId).append(",")
                    .append("blocks with ").append(sequenceId).append(" overlap no. mRNAs,")
                    .append("blocks with ").append(sequenceId).append(" blocks overlap bp,")
                    .append("% of mRNA's in ").append(sequenceId).append(" blocks,")
                    .append("% of bp in ").append(sequenceId).append(" blocks,");
        }
        outputBuilder.append("\n");

        int position1 = 0;
        for (String sequenceId : selectedSequences) {
            //System.out.println(sequenceId);
            String[] overlap_info_array = sequence_overlap_per_sequence.get(sequenceId);
            outputBuilder.append(sequenceId).append(",").append(overlap_info_array[position1]);
            int position2 = 0;
            for (String seq_id2 : selectedSequences) {
                String ovl_info = overlap_info_array[position2];
                if (position1 == position2 || ovl_info == null) {
                    outputBuilder.append(",,,,,");
                    position2++;
                    continue;
                }
                position2++;
                outputBuilder.append(ovl_info);
            }
            outputBuilder.append("\n");
            position1++;
        }
        writeStringToFile(outputBuilder.toString(), WORKING_DIRECTORY + "synteny/sequence_overlap_per_sequence.csv", false, false);
    }

    public void create_overlap_per_block_file(int highest_block_nr, HashMap<Integer, ArrayList<String>> blockInfo,
                                              HashMap<String, String> blockOverlap) {

        StringBuilder per_block_builder = new StringBuilder();
        per_block_builder.append("Block number,Syntenic gene pairs,Sequence 1,Sequence 2,Inverted block,Block size (mRNAs) sequence 1,"
                + "Block size (mRNAs) sequence 2,Block size (bp) sequence 1,seq1 start,seq1 stop,Block size (bp) sequence 2,seq2 start,seq2 stop,"
                + "% overlap with sequence 1 (mRNAs),% overlap with sequence 2 (mRNAs),% overlap with sequence 1 (bp),"
                + "% overlap with sequence 2 (bp),Sequence 1 length (mRNAs),Sequence 2 length (mRNAs),Sequence 1 length (bp)," +
                "Sequence 2 length (bp),Sequence 1 this block is overlapped by,Sequence 1 this block overlaps,Sequence 1 this block partially overlaps," +
                "Sequence 2 this block is overlapped by,Sequence 2 this block overlaps,Sequence 2 this block partially overlaps,mRNA identifiers sequence 1,mRNA identifiers sequence 2\n");

        for (int i = 0; i < highest_block_nr; i++) {
            //System.out.println(i);
            ArrayList<String> overlap_info = blockInfo.get(i);
            if (overlap_info == null) {
                continue;
            }
            String[] info_array1 = overlap_info.get(0).split("#");
            String[] info_array2 = overlap_info.get(1).split("#");
            String seq1BlockOverlaps = blockOverlap.get(i + "#" + info_array1[0]);
            String seq2BlockOverlaps = blockOverlap.get(i + "#" + info_array2[0]);
            //  [seq_id, syntenic genes, block size (mrnas), block size (bases), % overlap with seq (mrna), % overlap with seq (bases),
            //      seq length (mrnas), seq_length (bp), mrna ids seq 1, mrna ids seq 2, inverted block ]
            per_block_builder.append(i).append(",")  // Block number
                    .append(info_array1[1]).append(",") // Syntenic genes
                    .append(info_array1[0]).append(",") // Sequence 1 identifier
                    .append(info_array2[0]).append(",") // Sequence 2 identifier
                    .append(info_array1[10]).append(",") // inverted block
                    .append(info_array1[2]).append(",") // Block size (mRNAs) sequence 1
                    .append(info_array2[2]).append(",") // Block size (mRNAs) sequence
                    .append(info_array1[3]).append(",") // Block size (bp) sequence 1
                    .append(info_array1[11]).append(",")
                    .append(info_array1[12]).append(",")
                    .append(info_array2[3]).append(",") // Block size (bp) sequence 2
                    .append(info_array2[11]).append(",")
                    .append(info_array2[12]).append(",")
                    .append(info_array1[4]).append(",") // % overlap with sequence 1 (mRNAs)
                    .append(info_array2[4]).append(",") // % overlap with sequence 2 (mRNAs)
                    .append(info_array1[5]).append(",") // % overlap with sequence 1 (bp)
                    .append(info_array2[5]).append(",") // % overlap with sequence 1 (bp)
                    .append(info_array1[6]).append(",") // Sequence 1 length (mRNAs),
                    .append(info_array2[6]).append(",") // Sequence 2 length (mRNAs),
                    .append(info_array1[7]).append(",") // Sequence 1 length (bp),
                    .append(info_array2[7]).append(",") // Sequence 2 length (bp)
                    .append(seq1BlockOverlaps).append(",")
                    .append(seq2BlockOverlaps).append(",")
                    .append(info_array1[8]).append(",")
                    .append(info_array1[9]).append(", \n");
        }
        writeStringToFile(per_block_builder.toString(), WORKING_DIRECTORY + "synteny/sequence_overlap_per_block.csv", false, false);
    }
}