package nl.wur.bif.pantools.construction.variation.add_pav;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.PROTEOME;
import static picocli.CommandLine.*;

/**
 * Adds PAV to the pantools database.
 *
 * @author Dirk-Jan van Workum, Wageningen University, the Netherlands.
 */
@Command(name = "add_pavs", aliases = "add_pav", sortOptions = false)
public class AddPavCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "pavs-file", index = "0+")
    @InputFile(message = "{file.pavs}")
    Path pavLocationsFile;

    @Override
    public Integer call() throws IOException {
        // initialize logging
        pantools.createLogger(spec);

        // validate command line arguments
        BeanUtils.argValidation(spec, this);

        // initialize the neo4j graph database
        final Path databaseDirectory = pantools.getDatabaseDirectory();
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();

        // validate the neo4j graph database (only for pangenome databases)
        if (!PROTEOME) {
            GraphUtils.validateGenomes();
            GraphUtils.validateAnnotations();
        }

        // run the main code
        new AddPav(pavLocationsFile);

        // return exit code 0 if successful
        return 0;
    }
}
