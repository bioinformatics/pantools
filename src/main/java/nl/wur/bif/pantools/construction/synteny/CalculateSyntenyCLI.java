package nl.wur.bif.pantools.construction.synteny;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools calculate_synteny
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "calculate_synteny", sortOptions = false, abbreviateSynopsis = true)
public class CalculateSyntenyCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;
    @ArgGroup private SelectHmGroups selectHmGroups = new SelectHmGroups(); //needs to be initialized to prevent NullPointerException

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--run"})
    private boolean run;

    @Option(names = {"--longest"})
    private boolean longestTranscripts;

    @Option(names = {"--sequence"})
    private boolean sequence;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes, selectHmGroups);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        new Synteny().calculateSynteny(run, selectHmGroups.getHomologyGroups());
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        compare_sequences = sequence;
        compare_genomes = !sequence;
        longest_transcripts = longestTranscripts;
    }

}
