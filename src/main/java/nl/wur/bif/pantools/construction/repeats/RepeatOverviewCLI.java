package nl.wur.bif.pantools.construction.repeats;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.setGenomeSelectionOptions;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools repeat_overview
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "repeat_overview", sortOptions = false, abbreviateSynopsis = true)
public class RepeatOverviewCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--exclude-repeats"})
    Path repeatSelectionFile;

    @Option(names = "--window-length")
    @Min(value = 0, message = "{min.windowlength}") // TODO set message
    @Max(value = 1000000, message = "{max.windowlength}") // TODO set message
    int windowLength;

    @Option(names = "--upstream")
    @Min(value = 10, message = "{min.upstream}") // 10 bp // TODO set message
    @Max(value = 1000000, message = "{max.upstream}") // 1 Mb // TODO set message
    int upstreamLength;

    @Option(names = "--downstream")
    @Min(value = 10, message = "{min.downstream}") // 10 bp // TODO set message
    @Max(value = 1000000, message = "{max.downstream}") // 1 Mb // TODO set message
    int downstreamLength;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);
        pantools.setPangenomeGraph();
        setGenomeSelectionOptions(selectGenomes);

        new Repeats().repeat_overview(repeatSelectionFile, windowLength, upstreamLength, downstreamLength);
        return 0;
    }
}
