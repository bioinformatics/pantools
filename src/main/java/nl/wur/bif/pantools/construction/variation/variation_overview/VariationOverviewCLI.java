package nl.wur.bif.pantools.construction.variation.variation_overview;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static picocli.CommandLine.*;

/**
 * Writes an overview of the accessions in the database (VCF and/or PAV information).
 *
 * @author Dirk-Jan van Workum, Wageningen University, the Netherlands.
 */
@Command(name = "variation_overview", sortOptions = false)
public class VariationOverviewCLI implements Callable<Integer> {
    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Override
    public Integer call() {
        // initialize logging
        pantools.createLogger(spec);

        // validate command line arguments
        BeanUtils.argValidation(spec, this);

        // validate neo4j graph database
        final Path databaseDirectory = pantools.getDatabaseDirectory();
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();
        GraphUtils.validateAccessions();

        // run the main code
        new VariationOverview(pantools.getDatabaseDirectory());

        // return exit code 0 if successful
        return 0;
    }
}
