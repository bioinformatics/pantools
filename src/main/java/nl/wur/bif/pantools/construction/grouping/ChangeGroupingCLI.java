package nl.wur.bif.pantools.construction.grouping;

import jakarta.validation.constraints.Positive;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.GROUPING_VERSION;
import static nl.wur.bif.pantools.utils.Globals.proLayer;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Change the active version of the homology grouping.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "change_grouping", sortOptions = false)
public class ChangeGroupingCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-v", "--grouping-version"}, required = true)
    @Positive(message = "{positive.grouping-version}")
    int groupingVersion;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        proLayer.change_active_grouping();
        return 0;
    }

    private void setGlobalParameters() {
        GROUPING_VERSION = Integer.toString(groupingVersion);
    }

}
