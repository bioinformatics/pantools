package nl.wur.bif.pantools.construction.grouping;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.FAST;
import static nl.wur.bif.pantools.utils.Globals.proLayer;
import static picocli.CommandLine.*;

/**
 * deactivate the currently active homology grouping.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "deactivate_grouping", sortOptions = false)
public class DeactivateGroupingCLI implements Callable<Integer> {

    @Spec static CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = "--fast")
    boolean fast;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        proLayer.deactivateGrouping(true);
        return 0;
    }

    private void setGlobalParameters() {
        FAST = fast;
    }

}
