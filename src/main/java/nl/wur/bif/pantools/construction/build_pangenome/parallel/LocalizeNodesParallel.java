package nl.wur.bif.pantools.construction.build_pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Input;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import nl.wur.bif.pantools.Pantools;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.utils.Globals.*;

/**
 * Primary class for running localization of the pangenome. The localization step is subdivided into five stages:
 * <p>
 * 1. Traversing the pangenome graph and writing updates relevant to localization to one or more files; 2. Sorting the
 * localization files from stage 1 into a number of buckets (files) by relationship to derive localization information;
 * 3. Writing out the contents of the buckets from stage 2 to the relationships in the pangenome graph; 4. Sorting the
 * localization files from stage 1 into a number of buckets (files) by node to derive node-genome frequencies; 5.
 * Writing out the contents of the buckets from stage 4 to the the nodes in the pangenome graph.
 */
public class LocalizeNodesParallel {
    public final int DEFAULT_EXECUTOR_SERVICE_SHUTDOWN_TIMEOUT_SECONDS = 10;

    private final Path scratchDirectory;
    private final int numBuckets;
    private final int transactionSize;
    private final int numDbWriterThreads;
    private final int nodePropertiesCacheSize;
    private final boolean keepIntermediateFiles;

    // TODO: map environment variables and defaults to configuration object
    public LocalizeNodesParallel(Path scratchDirectory, int numBuckets, int transactionSize,
                                 int numDbWriterThreads, int nodePropertiesCacheSize, boolean keepIntermediateFiles) {
        this.scratchDirectory = scratchDirectory;
        this.numBuckets = numBuckets;
        this.transactionSize = transactionSize;
        this.numDbWriterThreads = numDbWriterThreads;
        this.nodePropertiesCacheSize = nodePropertiesCacheSize;
        this.keepIntermediateFiles = keepIntermediateFiles;
    }

    /**
     * Create output directory for storing the localization output generated in stage 1.
     *
     * @return path to output directory for localization output.
     * @throws IOException if directory cannot be created.
     */
    public Path createLocalizationOutputDirectory() throws IOException {
        final Path path = scratchDirectory.resolve("localizations");
        Files.createDirectory(path);
        return path;
    }

    /**
     * Create output directory for storing the output of the sorted data in stage 2.
     *
     * @return path to output directory for stage 2 output.
     * @throws IOException if directory cannot be created.
     */
    public Path createLocalizationBucketsOutputDirectory() throws IOException {
        final Path path = scratchDirectory.resolve("offset-buckets");
        Files.createDirectory(path);
        return path;
    }

    /**
     * Main method for localizing the pangenome. A scratch directory needs to be provided that will be used to store
     * temporary data in (must exist). Intermediate files will be removed afterwards or retained if specified with the
     * CLI.
     *
     * @param scratchDirectory path to temporary ('scratch') directory to store intermediate files in.
     * @return highest genome frequency across all nucleotide nodes.
     */
    public long run(Path scratchDirectory) throws Exception {
        final long start = System.currentTimeMillis();
        final Logger log = Pantools.logger;

        log.info("Scratch directory: " + scratchDirectory);

        // TODO: pre-flight check for creating #buckets?
        // TODO: combine stage 1 and 2 by directly sorting into offset buckets?
        // TODO: stage 2 and 4 can be run in parallel, if allowed sufficient number of open files
        // Stage 1: localize and write out updates to a file per sequence

        log.info("Stage 1: localizing sequence nodes");
        long stageStart = System.currentTimeMillis();
        final Path localizationOutputDirectory = createLocalizationOutputDirectory();
        final List<Path> localizationFiles = localizeNodes(localizationOutputDirectory, log);
        log.info(String.format("Finished stage 1 in %,d ms.", System.currentTimeMillis() - stageStart));

        // TODO: display number of localizations
        // TODO: create timing method
        // TODO: depending on stage we can ditch some of the localization information to save space and processing time

        // Stage 2: sort localizations into buckets hashed by relationship ID

        stageStart = System.currentTimeMillis();
        log.info("Stage 2: sorting localizations into buckets");
        final Path localizationBucketsDirectory = createLocalizationBucketsOutputDirectory();
        final List<Path> localizationBuckets = sortIntoLocalizationOffsetsBuckets(localizationFiles, localizationBucketsDirectory, numBuckets, log);
        log.info(String.format("Finished stage 2 in %,d ms.", System.currentTimeMillis() - stageStart));

        // Stage 3: read localization updates from stage 2, grouping into base pair offsets and writing them out

        stageStart = System.currentTimeMillis();
        log.info("Stage 3: writing localizations to the database");
        writeLocalizationOffsetsToDatabase(localizationBuckets, numDbWriterThreads, log);
        log.info(String.format("Finished stage 3 in %,d ms.", System.currentTimeMillis() - stageStart));

        // Stage 4: sort localizations into buckets hashed by end node ID

        stageStart = System.currentTimeMillis();
        log.info("Stage 4: sorting nodes into buckets");
        final Path nodeBucketsDirectory = createNodeBucketsOutputDirectory(scratchDirectory);
        final List<Path> nodeBuckets = sortIntoNodeBuckets(localizationFiles, nodeBucketsDirectory, numBuckets, log);
        log.info(String.format("Finished stage 4 in %,d ms.", System.currentTimeMillis() - stageStart));

        // Stage 5: write out node frequencies

        stageStart = System.currentTimeMillis();
        log.info("Stage 5: writing genome frequencies to nodes");
        final long highestFrequency = writeGenomeFrequenciesToDatabase(nodeBuckets, numDbWriterThreads, log);
        log.info(String.format("Finished stage 5 in %,d ms.", System.currentTimeMillis() - stageStart));
        log.info(String.format("Highest genome frequency for node: %,d", highestFrequency));

        // Clean up

        if (!keepIntermediateFiles) {
            final List<Path> filesToCleanUp = new ArrayList<>(localizationFiles);
            filesToCleanUp.addAll(localizationBuckets);
            filesToCleanUp.addAll(nodeBuckets);
            cleanup(filesToCleanUp, log);
        }

        log.info(String.format("Finished localization phase in %,d ms.", System.currentTimeMillis() - start));

        return highestFrequency;
    }

    /**
     * Create output directory for storing the output of the sorted data in stage 4.
     *
     * @param scratchDirectory scratch directory to create output directory in.
     * @return path to output directory for stage 4 output.
     * @throws IOException if directory cannot be created.
     */
    public Path createNodeBucketsOutputDirectory(Path scratchDirectory) throws IOException {
        final Path path = scratchDirectory.resolve("node-buckets");
        Files.createDirectory(path);
        return path;
    }

    /**
     * Return the node properties least-frequently used (LFU) cache. Size is determined by an environment variable or
     * the default. Maps a nucleotide node ID to a {@link NodeProperties} object storing the node's address (genome
     * index, sequence index, base pair offset) and its sequence length.
     *
     * @return Caffeine LFU cache mapping nucleotide node IDs to node properties.
     */
    public Cache<Long, NodeProperties> getNodePropertiesCache() {
        return Caffeine.newBuilder()
            .maximumSize(nodePropertiesCacheSize)
            .recordStats()
            .build();
    }

    /**
     * Entry point for stage 1: localizing the nucleotide nodes and writing localization information to a file per
     * sequence.
     *
     * @param outputDirectory directory to write output files to, must exist.
     * @param log             logger.
     * @return list of paths, each path pointing to the localizations files for a sequence.
     */
    private List<Path> localizeNodes(Path outputDirectory, Logger log) throws Exception {
        final int numThreads = THREADS;
        final Cache<Long, NodeProperties> cache = getNodePropertiesCache();
        log.info("Using node properties cache of size " + nodePropertiesCacheSize);

        final List<Node> sequenceNodes = getSequenceNodes();

        log.info(String.format("Localizing %,d sequence nodes with %d threads", sequenceNodes.size(), numThreads));
        log.info("Output directory: " + outputDirectory);

        final List<NodeLocalizationTask> tasks = sequenceNodes
            .stream()
            .map(sequenceNode -> new NodeLocalizationTask(sequenceNode, outputDirectory, cache, log))
            .collect(Collectors.toList());

        // TODO: show cache metrics
        return runParallel(tasks, numThreads, log);
    }

    /**
     * Return all sequence nodes in the pangenome graph as a list.
     *
     * @return list of all sequence nodes in pangenome graph.
     */
    private List<Node> getSequenceNodes() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> itr = GRAPH_DB.findNodes(SEQUENCE_LABEL)) {
            return itr
                .stream()
                .collect(Collectors.toList());
        }
    }

    /**
     * Sort localization files into buckets according to a specified hashing function that maps a
     *
     * @param localizationFiles list of localization files.
     * @param outputDirectory   output directory to store buckets in.
     * @param hasher            hashing function.
     * @param numBuckets        number of buckets (files).
     * @param log               logger.
     * @return list of paths to the bucket files.
     * @see Localization object to an integer that is used as a hashing key.
     * Note: the integer returned by the mapper can be anything, it will be modulo'ed against the number of buckets to
     * calculate the bucket index.
     */
    private List<Path> sortLocalizationFilesIntoBuckets(List<Path> localizationFiles, Path outputDirectory, Function<Localization, Long> hasher, int numBuckets, Logger log) throws Exception {
        log.debug(String.format("Sorting %,d localization files into %,d buckets in directory %s", localizationFiles.size(), numBuckets, outputDirectory));

        // TODO: make this try-with-resources
        // TODO: class for writing
        // TODO: parallelize

        // Create buckets: path and Kryo output for each

        Kryo kryo = KryoUtils.getKryo();
        // TODO: this logic to Buckets class?
        try (Buckets buckets = new Buckets(outputDirectory, numBuckets)) {
            for (int i = 0; i < localizationFiles.size(); i++) {
                final Path path = localizationFiles.get(i);
                try (Input input = KryoUtils.createInput(path)) {
                    log.debug(String.format("Sorting file %s (%,d/%,d)", path, i, localizationFiles.size()));
                    long counter = 0;
                    while (!input.end()) {
                        final Localization localization = kryo.readObject(input, Localization.class);
                        buckets.write(localization, hasher);
                        counter++;
                    }
                    log.debug(String.format("Sorted %,d localizations for file %s", counter, path));
                } catch (IOException e) {
                    throw new RuntimeException("error creating Kryo input for file " + path + ": " + e);
                }
            }

            log.debug(String.format("Sorted %,d localizations across %,d input files into %,d buckets", buckets.getNumLocalizations(), localizationFiles.size(), numBuckets));
            return buckets.getPaths();
        }
    }

    /**
     * Entry point for stage 2: sorting localization files into buckets keyed by relationship ID, genome index and
     * sequence index.
     *
     * @param localizationFiles list of localization files to bucket.
     * @param outputDirectory   output directory for buckets, must exist.
     * @param numBuckets        number of buckets.
     * @param log               logger.
     * @return list of paths to buckets.
     */
    private List<Path> sortIntoLocalizationOffsetsBuckets(List<Path> localizationFiles, Path outputDirectory, int numBuckets, Logger log) throws Exception {
        // TODO: hashing function to separate class or more descriptive method name
        return sortLocalizationFilesIntoBuckets(
            localizationFiles,
            outputDirectory,
            Localization::getRelationshipId,
            numBuckets,
            log
        );
    }

    /**
     * Entry point for stage 3: write localization bucket files to pangenome database. Buckets are read in parallel,
     * sorted in-memory and written out to Neo4j with the specified level of parallelism.
     *
     * @param localizationBuckets paths to localization files generated in stage 2.
     * @param numThreads          number of threads to write to Neo4j.
     * @param log                 logger.
     */
    private void writeLocalizationOffsetsToDatabase(List<Path> localizationBuckets, int numThreads, Logger log) throws Exception {
        log.info(String.format("Maximum transaction size: %,d", transactionSize));
        log.info("Num. threads: " + numThreads);

        final List<LocalizationsWriterTask> tasks = localizationBuckets
            .stream()
            .map(path -> new LocalizationsWriterTask(path, log, transactionSize))
            .collect(Collectors.toList());

        runParallel(tasks, numThreads, log);
    }

    /**
     * Entry point for stage 4: sorting the localization files from stage 1 into a number of buckets (files) by node to
     * derive node-genome frequencies.
     *
     * @param localizationFiles list of localization files to bucket.
     * @param outputDirectory   output directory for buckets, must exist.
     * @param numBuckets        number of buckets.
     * @param log               logger.
     * @return list of paths to buckets.
     */
    private List<Path> sortIntoNodeBuckets(List<Path> localizationFiles, Path outputDirectory, int numBuckets, Logger log) throws Exception {
        return sortLocalizationFilesIntoBuckets(
            localizationFiles,
            outputDirectory,
            Localization::getEndNodeId,
            numBuckets,
            log
        );
    }

    /**
     * Entry point for stage 5: writing out the contents of the buckets from stage 4 to the the nodes in the pangenome
     * graph.
     *
     * @param nodeBuckets paths to localization files generated in stage 4.
     * @param numThreads  number of threads to write to Neo4j.
     * @param log         logger.
     * @return highest genome frequency observed across all nucleotide nodes.
     */
    private long writeGenomeFrequenciesToDatabase(List<Path> nodeBuckets, int numThreads, Logger log) throws Exception {
        final long maximumTransactionSize = transactionSize;
        log.info(String.format("Maximum transaction size: %,d", maximumTransactionSize));
        log.info("Num. threads: " + numThreads);

        final List<NodeFrequenciesWriterTask> tasks = nodeBuckets
            .stream()
            .map(path -> new NodeFrequenciesWriterTask(path, log, maximumTransactionSize))
            .collect(Collectors.toList());

        final AtomicLong highestFrequency = new AtomicLong(-1);
        runParallel(tasks, numThreads, log, frequency -> {
            if (frequency > highestFrequency.get())
                highestFrequency.set(frequency);
        });

        assert highestFrequency.get() != -1;
        return highestFrequency.get();
    }

    /**
     * Run a collection tasks with the specified level of parallelism.
     *
     * @param tasks      collection of callables returning a value.
     * @param numThreads number of threads in the thread pool.
     * @param log        logger.
     * @param callback   method to call with each result, as they come in.
     * @param <T>        type of return value for each task.
     * @return list of return values, order does not correspond to that of the tasks.
     * TODO: document use of callback vs return value
     * TODO: re-use thread pool
     */
    private <T> List<T> runParallel(Collection<? extends Callable<T>> tasks, int numThreads, Logger log, Consumer<T> callback) throws Exception {
        log.debug("Creating completion service on executor service with fixed thread pool of size " + numThreads);

        // TODO: make thread pool configurable (naming threads might be useful for monitoring/troubleshooting)
        final ExecutorService es = Executors.newFixedThreadPool(numThreads);
        final CompletionService<T> cs = new ExecutorCompletionService<>(es);

        final List<Future<T>> ts =
            tasks
                .stream()
                .map(cs::submit)
                .collect(Collectors.toList());

        log.debug(String.format("Submitted %,d tasks", tasks.size()));

        final List<T> outputs = new ArrayList<>(tasks.size());

        // TODO: use try-with-resources?
        boolean terminatedWithError = false;
        Exception exception = null;
        // TODO: to method
        for (int i = 0; i < tasks.size(); i++) {
            try {
                final T result = cs.take().get();
                outputs.add(result);
                callback.accept(result);
            } catch (InterruptedException e) {
                log.error("Interrupted while waiting for tasks to finish: " + e);
                terminatedWithError = true;
                exception = e;
                break;
            } catch (ExecutionException e) {
                // TODO: show which node localization failed
                log.error("Error retrieving result for task: " + e);
                terminatedWithError = true;
                exception = e;
                break;
            }
        }

        // TODO: fix cleanup logic
        if (terminatedWithError) {
            log.info("Error(s) occurred during task processing. Cancelling all tasks");
            // TODO: show cancel progress?
            for (Future<T> task : ts)
                task.cancel(true);
        }

        log.info("Shutting down executor service. Waiting for " + DEFAULT_EXECUTOR_SERVICE_SHUTDOWN_TIMEOUT_SECONDS + " seconds");
        es.shutdown();
        try {
            // TODO: make configurable
            if (!es.awaitTermination(DEFAULT_EXECUTOR_SERVICE_SHUTDOWN_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                log.warn("Timeout elapsed before executor service was shut down");
        } catch (InterruptedException e) {
            throw new RuntimeException("interrupted while waiting on executor service to shut down: " + e);
        }
        log.debug("Executor service is shut down");

        if (terminatedWithError)
            throw exception;

        return outputs;
    }

    /**
     * Wrapper method for {@link LocalizeNodesParallel#runParallel(Collection, int, Logger, Consumer)} without a
     * callback.
     *
     * @param tasks      collection of callables returning a value.
     * @param numThreads number of threads in the thread pool.
     * @param log        logger.
     * @param <T>        type of return value for each task.
     * @return list of return values, order does not correspond to that of the tasks.
     */
    private <T> List<T> runParallel(Collection<? extends Callable<T>> tasks, int numThreads, Logger log) throws Exception {
        return runParallel(tasks, numThreads, log, t -> {
        });
    }

    /**
     * Clean up files by removing them. Directories housing the files are not removed.
     *
     * @param filesToCleanUp list of files to remove.
     * @param log            logger to display metrics and progress information.
     */
    private void cleanup(List<Path> filesToCleanUp, Logger log) throws IOException {
        // TODO: remove directories as well
        long totalBytesCleanedUp = 0;

        log.info("Cleaning up " + filesToCleanUp.size() + " intermediate files");
        for (int i = 0; i < filesToCleanUp.size(); i++) {
            final Path path = filesToCleanUp.get(i);
            totalBytesCleanedUp += Files.size(path);

            log.debug(String.format("Deleting file %s (%,d/%,d)", path, i + 1, filesToCleanUp.size()));
            Files.delete(filesToCleanUp.get(i));
        }

        final String totalBytesCleanedUpHumanReadable = FileUtils.byteCountToDisplaySize(totalBytesCleanedUp);
        log.info(String.format("Cleaned up %,d bytes (%s) across %,d files", totalBytesCleanedUp, totalBytesCleanedUpHumanReadable, filesToCleanUp.size()));
    }
}
