package nl.wur.bif.pantools.construction.functions.add_functions;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static picocli.CommandLine.*;

/**
 * Add functional annotations to a pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "add_functions", sortOptions = false)
public class AddFunctionsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "functions-file", index = "0+")
    @InputFile(message = "{file.functions}")
    Path functionsFile;

    @Option(names = {"-A", "--annotations-file"})
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Option(names = "--function")
    String function;

    @Option(names = {"-F", "--functional-databases-directory"})
    Path functionalDatabasesPath;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();

        new AddFunctions(functionalDatabasesPath, functionsFile, annotationsFile, function);
        return 0;
    }
}
