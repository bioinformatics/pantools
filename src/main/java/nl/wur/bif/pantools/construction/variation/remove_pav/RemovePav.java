package nl.wur.bif.pantools.construction.variation.remove_pav;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.construction.variation.RemoveVariation;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;

/**
 * This class is used to remove PAV information from the graph database.
 * Information is removed from accession and variant nodes.
 * If the nodes do not contain any VCF information, the nodes are completery removed instead.
 *
 * @author Dirk-Jan van Workum
 */
public class RemovePav extends RemoveVariation {

    public RemovePav() {
        removePavInformation();
    }

    /**
     * Removes all PAV information from the variant nodes and accession nodes
     *
     * @throws NotFoundException if the variant nodes or accession nodes could not be found
     */
    private void removePavInformation() throws NotFoundException {
        Pantools.logger.info("Removing PAV information from the database.");
        removeAccessionInformation("PAV");
        removeVariantInformation("PAV");
        Pantools.logger.info("Finished removing PAV information from the database.");
    }

    /**
     * Remove an accession node if it does not contain VCF information, otherwise remove PAV information.
     *
     * @param accessionNode accession node with PAV information
     */
    @Override
    protected void removeAccessionNode(Node accessionNode) {
        // if VCF information is present, remove PAV information
        if ((boolean) accessionNode.getProperty("VCF")) {
            accessionNode.setProperty("PAV", false);
        } else { // otherwise remove the entire node
            GraphUtils.removeNode(accessionNode);
        }
    }
    /**
     * Remove a variant node if it does not contain VCF information, otherwise remove PAV information.
     *
     * @param variantNode variant node with PAV information
     */
    @Override
    protected void removeVariantNode(Node variantNode) {
        // if VCF information is present, remove PAV information
        if (variantNode.hasProperty("mRNA_sequence")) {
            if (variantNode.hasProperty("present")) {
                variantNode.removeProperty("present");
            }
        } else { // otherwise remove the entire node
            GraphUtils.removeNode(variantNode);
        }
    }
}
