package nl.wur.bif.pantools.construction.build_pangenome;

import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Include additional genomes to an already available pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "add_genomes", sortOptions = false)
public class AddGenomesCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @Mixin private ThreadNumber threadNumber;

    @ParentCommand
    private Pantools pantools;

    @Parameters(descriptionKey = "genomes-file", index = "0+")
    @InputFile(message = "{file.genomes}")
    Path genomesFile;

    @Option(names = "--scratch-directory")
    Path scratchDirectory;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        seqLayer.add_genomes(scratchDirectory);
        return 0;
    }

    private void setGlobalParameters() {
        THREADS = threadNumber.getnThreads();
        PATH_TO_THE_GENOMES_FILE = genomesFile.toString();
    }

}
