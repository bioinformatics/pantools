package nl.wur.bif.pantools.construction.functions.add_antismash;

import nl.wur.bif.pantools.Pantools;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.wur.bif.pantools.analysis.classification.Classification.get_annotation_identifiers;
import static nl.wur.bif.pantools.analysis.classification.Classification.retrieveNamePropertyAsArray;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class AddAntismash {

    public AddAntismash() {
        add_antismash();
    }

    /**
    Read the output from antiSMASH4

    Requirements:
     -dp
     -if:  A text file with on each line a genome number and the full path to the corresponding antiSMASH output file,
                separated by a space.

    Optional
    -af:

    overwrite is currently hardcoded

    How to run antiSMASH?
    With .gbk file
        antismash --cb-general --cb-knownclusters --cb-subclusters --asf --pfam2go --smcog-trees streptomyces_coelicolor.gbk
    With genome fasta and .gff

    Example antismash geneclusters input file.
    Five columns.
        1 Sequence identifier,
        2 Complete fasta header,
        3 Cluster type
        4 The gene original identifiers,
        5 Renamed gene identifiers

    CP004601.1 Saccharomyces cerevisiae YJM993 chromosome II    other   H779_YJM993B00213;H779_YJM993B00214;H779_YJM993B00215
    CP006151.1  CP006151.1 Saccharomyces cerevisiae YJM993 chromosome VIII  terpene H779_YJM993H00245;H779_YJM993H00247;H779_YJM993H00248
    */
    private void add_antismash() {
        Pantools.logger.info("Adding biosynthetic gene cluster nodes to the pangenome.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            stop_if_panproteome("add_antismash_results"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            annotation_identifiers = get_annotation_identifiers(false, false, PATH_TO_THE_ANNOTATIONS_FILE); // do not print, and USE the user input (-af)
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date() );
        StringBuilder log_builder = new StringBuilder("## LOG of run on " + date  + "\n");
        create_directory_in_DB("function");
        create_directory_in_DB("log");

        HashMap<Integer, ArrayList<String>> input_files = read_input_files_for_antismash();
        System.out.println("\rGenome\tClusters added");
        for (int genome_nr :  input_files.keySet()) {
            String annotation_id = annotation_identifiers.get(genome_nr - 1);
            if (annotation_id.endsWith("_0")) {
                Pantools.logger.info("{}\t0\tGene clusters cannot be added because no annotation is available.", genome_nr);
                continue;
            }
            HashMap<String, Node> gene_map = prepare_gene_map_antismash(genome_nr); // retrieve all genes for a genome
            ArrayList<String> input_file_list = input_files.get(genome_nr);
            for (String input_file : input_file_list) {
                AtomicInteger new_counter = new AtomicInteger(0);
                AtomicInteger exists_counter = new AtomicInteger(0);
                String log;
                if (input_file.endsWith(".json")) {
                    log = add_antismash_from_json(input_file, gene_map, genome_nr, exists_counter, new_counter);
                } else {
                    Pantools.logger.info("{} is not a json file.", input_file);
                    //log = add_antismash_from_geneclusters_txt(input_file, gene_map, genome_nr, exists_counter,
                    //        new_counter);
                    continue;
                }
                System.out.print("\r                          ");
                System.out.println("\r" + genome_nr + "\t" + new_counter.get());
                log_builder.append("##Genome ").append(genome_nr).append(". file ").append(input_file).append("\n")
                        .append(new_counter.get()).append(" gene clusters added, ").append(exists_counter).append(" were already known\n")
                        .append(log).append("\n");
            }
        }

        Pantools.logger.info("Log written to {}log/add_antismash.log", WORKING_DIRECTORY);
        append_SB_to_file_full_path(log_builder, WORKING_DIRECTORY + "log/add_antismash.log");
    }

    /**
     * Must have two columns: first genome number, second is a full path to geneclusters.txt
     * @return hashmap. key is genome number. value is (multiple) paths to antismash cluster file
     */
    private HashMap<Integer, ArrayList<String>> read_input_files_for_antismash() {
        if (INPUT_FILE == null) {
            Pantools.logger.error("No gene clusters provided.");
            System.exit(1);
        }
        HashMap<Integer, ArrayList<String>> input_files = new HashMap<>();
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                String[] line_array = line.split(" ");
                if (line_array.length != 2) {
                    System.out.println("\rInput file is not formatted correctly!" +
                            " Should be 2 columns separated by a space. Line has " + line_array.length + " columns\n");
                    System.exit(1);
                }
                int genome_nr = 0;
                try {
                    genome_nr = Integer.parseInt(line_array[0]);
                } catch (NumberFormatException nee) {
                    Pantools.logger.info("{} is not a number.", line_array[0]);
                    continue;
                }
                input_files.computeIfAbsent(genome_nr, i -> new ArrayList<>()).add(line_array[1]);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
        return input_files;
    }


    /**
     * Gather all gene nodes of a genome and put this in a hashmap.
     *       ANTISMASH output must have the 'id' or the information cannot be added.
     * @param genome_nr number of a genome in the pangenome database
     * @return hashmap. key is a gene id, value is the gene node.
     */
    private HashMap<String, Node> prepare_gene_map_antismash(int genome_nr) {
        HashMap<String, Node> gene_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            String annotation_id = annotation_identifiers.get(genome_nr - 1);
            ResourceIterator<Node> gene_nodes = GRAPH_DB.findNodes(GENE_LABEL, "annotation_id", annotation_id);
            int gene_counter = 0;
            while (gene_nodes.hasNext()) {
                gene_counter++;
                if (gene_counter % 10000 == 0 || gene_counter == 1) {
                    System.out.print("\rGathering genes: " + gene_counter + "          "); // spaces are intentional
                }
                Node gene_node = gene_nodes.next();
                String id = (String) gene_node.getProperty("id");
                gene_map.put(id, gene_node);
                gene_map.put(id + "_gene", gene_node);

                String[] geneNames = retrieveNamePropertyAsArray(gene_node);
                for (String name : geneNames) {
                    gene_map.put(name, gene_node);
                }

                if (gene_node.hasProperty("locus_tag")) {
                    String[] locus_tag = (String[]) gene_node.getProperty("locus_tag");
                    gene_map.put(locus_tag[0], gene_node);
                }
            }
            tx.success();
        }
        // This function can be extended with mRNA and CDS nodes when a gene still cannot be found?
        return gene_map;
    }

    /**
     * Parse antismash v6.? json output
     * @param path_to_json file location of json file
     * @return key is cluster number, value is string with cluster type + "#" gene identifiers separated by commas
     */
    private String add_antismash_from_json(String path_to_json, HashMap<String, Node> gene_map,
                                                 int genomeNr, AtomicInteger exists_counter,
                                                 AtomicInteger new_counter) {

        StringBuilder log_builder = new StringBuilder();
        log_builder.append("\nCluster info\ntype,number of genes,address,cluster added?,bgc node identifier,gene node identifiers,gene ids not found?\n");
        String json = "";
        try (BufferedReader in = new BufferedReader(new FileReader(path_to_json))) {
            String line;
            while ((line = in.readLine()) != null) {
                json = line.trim();
            }
        } catch (IOException ioe) {
            Pantools.logger.info("Failed to read: {}", path_to_json);
        }
        JSONObject obj2 = new JSONObject(json);
        JSONArray records = obj2.getJSONArray("records");
        int cluster_end_pos = 0, cluster_start_pos = 0;
        StringBuilder cluster_type = new StringBuilder();
        ArrayList<String> geneIdentiers = new ArrayList<>();
        String earlier_CDS = "";
        int[] earlier_address = new int[0];
        for (int i = 0; i < records.length(); i++) { // j is feature number
            JSONArray features = records.getJSONObject(i).getJSONArray("features");
            boolean in_cluster = false;
            for (int j = 0; j < features.length(); j++) {
                String location = features.getJSONObject(j).getString("location"); // example [1:1000]
                String type = features.getJSONObject(j).getString("type");
                JSONObject qualifiers = features.getJSONObject(j).getJSONObject("qualifiers");
                if (type.equals("protocluster")) {
                    JSONArray product = qualifiers.getJSONArray("product");
                    in_cluster = true;
                    int[] cluster_start_stop_pos = start_stop_from_json_location(location);
                    cluster_start_pos = cluster_start_stop_pos[0];
                    cluster_end_pos = cluster_start_stop_pos[1];

                    Object cluster_product = product.get(0);
                    if (cluster_type.length() == 0) {
                        cluster_type = new StringBuilder(cluster_product.toString());
                    } else { // multiple products for a cluster
                        cluster_type.append(",").append(cluster_product.toString());
                    }
                    if (!earlier_CDS.equals("")){ // for some clusters the CDS property comes before protocluster
                        if (earlier_address[0] >= cluster_start_pos) {
                            geneIdentiers.add(earlier_CDS);
                        }
                    }
                    continue;
                } else if (type.equals("CDS")) {
                    try {
                        earlier_CDS = (String) qualifiers.getJSONArray("gene").get(0);
                    } catch (JSONException jse) {
                        Pantools.logger.error("Unable to retrieve gene identifiers from the antiSMASH output: this is most likely because antiSMASH was run without GFF file.");
                        System.exit(1);
                    }
                    earlier_address = start_stop_from_json_location(location);
                }
                int gene_end_pos;
                if (in_cluster && type.equals("gene")) {
                    String gene_id = (String) qualifiers.getJSONArray("ID").get(0);
                    int[] gene_start_stop_pos = start_stop_from_json_location(location);
                    int gene_start_pos = gene_start_stop_pos[0];
                    gene_end_pos = gene_start_stop_pos[1];
                    if (gene_start_pos < cluster_end_pos && !geneIdentiers.contains(gene_id)) {
                        geneIdentiers.add(gene_id);
                    }
                } else if (in_cluster && type.equals("CDS")) {
                    String gene_id = (String) qualifiers.getJSONArray("gene").get(0);
                    int[] gene_start_stop_pos = start_stop_from_json_location(location);
                    int gene_start_pos = gene_start_stop_pos[0];
                    gene_end_pos = gene_start_stop_pos[1];
                    if (gene_start_pos < cluster_end_pos && !geneIdentiers.contains(gene_id)) {
                        geneIdentiers.add(gene_id);
                    }
                } else {
                    continue;
                }

                if (gene_end_pos >= cluster_end_pos) { // the current gene has a coordinate higher as the cluster
                    in_cluster = false;
                    if (geneIdentiers.isEmpty()) {
                        continue;
                    }

                    StringBuilder missingGenes = new StringBuilder();
                    ArrayList<Node> geneNodes = retrieveGeneNodesFromIdentifiers(geneIdentiers, gene_map, null, missingGenes);
                    int[] bgc_address = create_gene_cluster_address(genomeNr, geneNodes);
                    String cluster_type_str = cluster_type.toString();
                    boolean cluster_exists = check_if_bgc_exists(cluster_type_str, bgc_address, geneNodes, log_builder);
                    cluster_type = new StringBuilder(); // reset for the next cluster
                    geneIdentiers = new ArrayList<>(); // reset for the next cluster
                    if (cluster_exists) {
                        exists_counter.incrementAndGet();
                        continue;
                    }
                    new_counter.incrementAndGet();
                    create_BGC_node(cluster_type_str, geneNodes, genomeNr, bgc_address, log_builder,
                            cluster_start_pos, cluster_end_pos);
                    log_builder.append(missingGenes).append("\n");
                }
            }
        }
        return log_builder.toString();
    }

    private int[] start_stop_from_json_location(String location) {
        int gene_start_pos, gene_end_pos;
        if (location.startsWith("join")) {
            // location example "join{[19678:22672](-), […](-), [18596:19037](-)}"
            String[] loc_array = location.split(":");
            String first_value_str = loc_array[0].replaceAll("[^0-9]",""); // remove all except numbers
            String last_value_str = loc_array[loc_array.length-1].replaceAll("[^0-9]",""); // remove all except numbers
            int first_value = Integer.parseInt(first_value_str);
            int last_value = Integer.parseInt(last_value_str);
            gene_start_pos = first_value;
            if (location.contains("+")) {
                Pantools.logger.trace("{} {}",  + first_value, last_value);
                gene_end_pos = last_value;
            } else {
                Pantools.logger.trace("- {} {}", last_value, first_value);
                gene_end_pos = first_value;
                gene_start_pos = last_value;
            }
        } else {
            String[] loc_array = location.split(":");
            String[] loc_array2 = loc_array[1].split("]");
            gene_start_pos = Integer.parseInt(loc_array[0].replace("[",""));
            gene_end_pos = Integer.parseInt(loc_array2[0].replace("]",""));
        }
        return new int []{gene_start_pos, gene_end_pos};
    }

    /**
     * Create the address of the gene cluster based on the first and last position found of the genes.
     * @param gene_identifiers
     * @param gene_map
     * @param gene_locus_ids
     * @param missingGenes
     * @return
     */
    private ArrayList<Node> retrieveGeneNodesFromIdentifiers(ArrayList<String> gene_identifiers, HashMap<String, Node> gene_map,
                                                                   HashMap<String, String> gene_locus_ids,
                                                                   StringBuilder missingGenes) {

        ArrayList<Node> geneNodes = new ArrayList<>();

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String gene_id : gene_identifiers) {
                Node geneNode = null;
                if (gene_map.containsKey(gene_id)) {
                    geneNode = gene_map.get(gene_id);
                } else {
                    String extra_id = "";
                    if (gene_locus_ids != null && !gene_locus_ids.isEmpty()) {  // when a final_gbk file was present
                        extra_id = gene_locus_ids.get(gene_id);
                        if (gene_map.containsKey(extra_id)) {
                            geneNode = gene_map.get(extra_id);
                        }
                        extra_id = " or " + extra_id;
                    }

                    if (geneNode == null) { // gene still not found
                        String new_gene_id = split_gene_id_try_every_option(gene_id, gene_map);
                        if (new_gene_id.equals(gene_id)) {
                            missingGenes.append(gene_id).append(extra_id).append(" ");
                        } else {
                            geneNode = gene_map.get(new_gene_id);
                        }
                    }
                }

                if (!geneNodes.contains(geneNode) && geneNode != null) {
                    geneNodes.add(geneNode);
                }
            }
            tx.success();
        }
        return geneNodes;
    }

    /**
     *
     * @param genomeNr a genome number
     * @param geneNodes a list of 'gene' nodes
     * @return address of bgc genome number, sequence number, start position, end position
     */
    private int[] create_gene_cluster_address(int genomeNr, ArrayList<Node> geneNodes) {
        int sequenceNr = -1, highest = 0, lowest = 999999999;
        int[] bgcAddress = new int[4];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node geneNode : geneNodes) { // go over all nodes, else incorrect addres if genes are not in correct order
                int[] address = (int[]) geneNode.getProperty("address");
                sequenceNr = address[1];
                if (address[2] < lowest) {
                    lowest = address[2];
                }
                if (address[3] > highest) {
                    highest = address[3];
                }
            }
            bgcAddress[0] = genomeNr;
            bgcAddress[1] = sequenceNr;
            bgcAddress[2] = lowest;
            bgcAddress[3] = highest;
            tx.success();
        }
        return bgcAddress;
    }

    /**
     * Check if there already is a bgc node with same type and address
     * @param cluster_type cluster product
     * @param bgc_address genomic coordinates of cluster
     * @param log_builder
     * @return boolean. whether the cluster is already present or not
     */
    private boolean check_if_bgc_exists(String cluster_type, int[] bgc_address, ArrayList<Node> gene_node_list, StringBuilder log_builder) {
        boolean cluster_present = false;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try {
                Node bgc_node = GRAPH_DB.findNode(BGC_LABEL, "address_genes", bgc_address);
                String type = (String) bgc_node.getProperty("type");
                if (type.equals(cluster_type)) {
                    log_builder.append(type.replace(","," ")).append(",") // cluster product
                            .append(gene_node_list.size()).append(",") // number of genes in cluster
                            .append(bgc_address[0]).append(" ").append(bgc_address[1]).append(" ")
                            .append(bgc_address[2]).append(" ").append(bgc_address[3]) // cluster address
                            .append(",not added, same type (product) and address as bgc node ")
                            .append(bgc_node.getId()).append("\n"); // bgc node identifier
                    cluster_present = true;
                }
            } catch (NullPointerException npe) {
                // if node with the specific address is not found, the exception is caught
                return cluster_present;
            }
            tx.success();
        }
        return cluster_present;
    }

    /**
     *
     * @param cluster_type cluster product
     * @param gene_node_list list of 'gene' nodes
     * @param genome_nr a genome number
     * @param bgc_address genomic coordinates of cluster
     * @param log_builder the log to store in file
     */
    private void create_BGC_node(String cluster_type, ArrayList<Node> gene_node_list, int genome_nr, int[] bgc_address,
                                       StringBuilder log_builder, int cluster_start_pos, int cluster_end_pos) {

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            StringBuilder gene_id_builder = new StringBuilder();
            String annotation_id = annotation_identifiers.get(genome_nr - 1);
            Node bgc_node = GRAPH_DB.createNode(BGC_LABEL);
            bgc_node.setProperty("type", cluster_type);
            String[] cluster_types = cluster_type.split(","); // type can consists of multiple types. example: NRPS,NRPS-like
            for (int j = 0; j < cluster_types.length; j++) {
                bgc_node.setProperty("type_" + (j + 1), cluster_types[j]);
            }
            bgc_node.setProperty("length", gene_node_list.size());
            bgc_node.setProperty("genome", genome_nr);
            bgc_node.setProperty("sequence", bgc_address[1]);
            bgc_node.setProperty("address_genes", bgc_address);
            if (cluster_start_pos == 0) {
                cluster_start_pos ++;
            }
            bgc_address[2] = cluster_start_pos;
            bgc_address[3] = cluster_end_pos;
            bgc_node.setProperty("address", bgc_address);
            bgc_node.setProperty("annotation_id", annotation_id);
            int position = 1;
            for (Node gene_node : gene_node_list) {
                gene_id_builder.append(gene_node.getId()).append(" ");
                if (gene_node.getId() == 0) {
                    continue;
                }
                Relationship rel = gene_node.createRelationshipTo(bgc_node, RelTypes.part_of);
                rel.setProperty("position", position);
                position++;
            }

            log_builder.append(cluster_type.replace(",", " ")) // cluster product
                    .append(",").append(gene_node_list.size()).append(",") // cluster length
                    .append(bgc_address[0]).append(" ").append(bgc_address[1]).append(" ")
                    .append(bgc_address[2]).append(" ").append(bgc_address[3]) // cluster address
                    .append(",yes,") // cluster was added
                    .append(bgc_node.getId()).append(",") // bgc node identifier
                    .append(gene_id_builder.toString().replaceFirst(".$","")).append(","); // gene node identififers of cluster
            tx.success();
        }
    }

    /**
     *
     * @param gene_id a gene identifier (from GFF)
     * @param gene_map
     * @return string. is the same as the input gene_id when no correct id was found
     */
    private String split_gene_id_try_every_option(String gene_id, HashMap<String, Node> gene_map) {
        String[] gene_id_array = gene_id.split("_");
        String new_gene_id = "";
        for (int i=0; i < gene_id_array.length; i++) {
            if (!new_gene_id.equals("")){
                new_gene_id += "_";
            }
            new_gene_id += gene_id_array[i];
            if (gene_map.containsKey(new_gene_id)){
                return new_gene_id;
            }
        }
        return new_gene_id;
    }
}
