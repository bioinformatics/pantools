/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.construction.build_pangenome;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.build_pangenome.parallel.LocalizeNodesParallel;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.construction.index.IndexDatabase;
import nl.wur.bif.pantools.construction.index.IndexPointer;
import nl.wur.bif.pantools.construction.index.IndexScanner;
import nl.wur.bif.pantools.construction.index.kmer;
import nl.wur.bif.pantools.construction.sequence.SequenceScanner;
import nl.wur.bif.pantools.utils.FileUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;

import static nl.wur.bif.pantools.analysis.classification.Classification.genome_overview;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.complement;
import static nl.wur.bif.pantools.utils.Utils.*;


/**
 * Implements all the functionalities related to the sequence layer of the pangenome
 * 
 * @author Siavash Sheikhizadeh, Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class GenomeLayer {
    private static final String NUCLEOTIDE_NODE_IDS_FILE_NAME = "nucleotide-node-ids.csv";
    private Node curr_node;
    private byte curr_side;
    private boolean finish;
    private SequenceScanner genomeSc;
    public long highest_frequency = 0;
     
    /**
     * Implements a class for short sequencing reads
     */
    public class read {
        StringBuilder name;
        StringBuilder forward_seq;
        StringBuilder reverse_seq;
        //StringBuilder quality;
        
        public read() {
            name = new StringBuilder();
            forward_seq = new StringBuilder();
            reverse_seq = new StringBuilder();
            //quality = new StringBuilder();
        }
        
        public void clear() {
            name.setLength(0);
            forward_seq.setLength(0);
            reverse_seq.setLength(0);
            //quality.setLength(0);
        }
        
        public int length() {
            return forward_seq.length();
        }
    }

    /**
     * Multiple quality checks on the data.
     * - Stops when line contains a space
     * - Stop if one of the files does not exist.
     * - Gives a warning when the first line do not only contain A T C G N 
     */
    public void verify_if_all_genome_files_exist() {
        if (PATH_TO_THE_GENOMES_FILE == null) {
            Pantools.logger.error("Please provide a genome file.");
            System.exit(1);
        }
        int line_counter = 0;
        ArrayList<Integer> not_found = new ArrayList<>();
        ArrayList<Integer> not_nucleotide = new ArrayList<>();
        StringBuilder new_genomes_file = new StringBuilder(); // KMC requires Linux formatted textfile with newlines 
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_GENOMES_FILE))) { // first check if all files exist
            for (int c = 0; in.ready();) {
                line_counter ++;
                System.out.print("\rVerifying if all input files exist: " + line_counter);
                String line = in.readLine().trim();
                new_genomes_file.append(line).append("\n");
                String[] line_array = line.split(" ");
                if (line_array.length != 1) {
                    Pantools.logger.error("Line{}: {} is not allowed to have a space character! Please include the full path to the genome fasta file.", line_counter, line);
                    System.exit(1);
                }
                boolean file_exists = check_if_file_exists(line);
                if (!file_exists) {
                    Pantools.logger.info("{} {} does not exist.", line_counter, line);
                    not_found.add(line_counter);
                    continue;
                }
                boolean correct_seq = check_if_nucleotide_sequence(line);
                if (!correct_seq) {
                    not_nucleotide.add(line_counter);
                }
            }
            System.out.println();
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_GENOMES_FILE);
            System.exit(1);
        }
        if (not_found.size() > 0) {
            System.out.println("\rThe genome files for these on these lines were not found: " + not_found.toString().replace(" ","").replace("[","").replace("]",""));
            System.exit(1);
        }
        
        if (not_nucleotide.size() > 0) {
            System.out.println("\rThe first line of the genome files were checked for A, T, C, G, N."
                    + "\nIn the following genomes, the first line contained other characters: " + not_nucleotide.toString().replace(" ","").replace("[","").replace("]","") );
        }
        write_SB_to_file_full_path(new_genomes_file, PATH_TO_THE_GENOMES_FILE); // replace input file
    }
    
    /**
     * @param fasta_file  file location
     * Reads the first line of a .fasta file to check if it's a nucleotide sequence
     * @return
     */
    public boolean check_if_nucleotide_sequence(String fasta_file) {
        boolean correct = true;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(openTextFile(fasta_file)))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith(">")) {
                    // do nothing
                } else { 
                    int position_counter = 0;
                    for (int i=0; i < line.length(); i++) { 
                        char character = line.charAt(i);
                        char fUpper = Character.toUpperCase(character);
                        if (fUpper != 'A' && fUpper != 'T' && fUpper != 'C' && fUpper != 'G' && fUpper != 'N') {
                            correct = false;
                        }
                        position_counter ++;
                        if (position_counter > 500) { 
                            // break the loop after the first 500 positions
                            break;
                        }
                    }
                    break; // break the loop after the first line (that is not the header)
                } 
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", fasta_file);
            System.exit(1);
        }
        return correct;
    }

    /**
     * Open a text file as an input stream. If the path ends with a .gz extension, it is assumed to be gzipped. If not,
     * the file is assumed to be uncompressed.
     * @param path path to (gzip-compressed) text file to open.
     * @return input stream.
     */
    public InputStream openTextFile(String path) throws IOException {
        // TODO: use Path for clarity instead of String
        // TODO: make more robust, e.g. with library (commons compress?)

        final InputStream stream = new FileInputStream(path);
        if (path.toLowerCase().endsWith(".gz"))
            return new GZIPInputStream(stream);

        return stream;
    }
    
    /**
     * Only KMC 3.1.0 and higher works. Stop when using another version
     */
    public void check_kmc_version() {
        StringBuilder exe_output = new StringBuilder();
        String line = "";
        Process p;
        try {
            p = Runtime.getRuntime().exec("kmc -h");
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = reader.readLine()) != null) {
                exe_output.append(line).append("\n");
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }    
        String log_str = exe_output.toString();
        if (log_str.length() < 100) {
            Pantools.logger.error("KMC is not installed ({}).", log_str.length());
            System.exit(1);
        } 
        check_if_program_exists_stdout("kmc_tools", 100, "kmc_tools", true);
        String[] log_array = log_str.split("\n");
        if (!log_array[0].contains("ver. 3.") || log_array[0].contains(" 3.0")) {
            Pantools.logger.error("Requires KMC version 3.1.0 or higher. Current version: {}", log_array[0]);
            System.exit(1);
        }
    }
    
    /**
     * Constructs a pangenome (gDBG) for a set of genomes.
     * build_pangenome()
     */
    public void initialize_pangenome(Path databaseDirectory, Path scratchDirectory, int numBuckets, int transactionSize,
                                     int numDbWriterThreads, int nodePropertiesCacheSize, boolean keepIntermediateFiles)
                                    throws IOException {
        Pantools.logger.info("Constructing the pangenome graph database.");
        // check for heap space (8g multiplied by .8 because only ~85% of the Xmx setting is usually available.)
        if (Runtime.getRuntime().maxMemory() < 8589934592L * .8) {
            Pantools.logger.warn("At least 8g maximum heap space (-Xmx8g) is recommended for build_pangenome.");
        }
        check_kmc_version(); // check if program is set to $PATH and if appropriate version
        Node pangenome_node;
        verify_if_all_genome_files_exist();
        scratchDirectory = FileUtils.createScratchDirectory(scratchDirectory);
        GraphUtils.createPangenomeDatabase(databaseDirectory);
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        Pantools.logger.info("k-size = {}", K_SIZE);
        genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.createNode(PANGENOME_LABEL);
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
            tx.success();
        } catch (Exception e) {
            Pantools.logger.error("Failed to create pangenome node.");
            throw new RuntimeException(e.getMessage());
        }
        num_nodes = 0;
        num_edges = 0;
        num_bases = 0;
        num_degenerates = 0;
        construct_pangenome(pangenome_node);

        add_sequence_properties(scratchDirectory);
        try {
            highest_frequency = new LocalizeNodesParallel(
                scratchDirectory,
                numBuckets,
                transactionSize,
                numDbWriterThreads,
                nodePropertiesCacheSize,
                keepIntermediateFiles
            ).run(scratchDirectory);
        } catch (Exception e) {
            Pantools.logger.error("Error occurred during localization.");
            throw new RuntimeException(e.getMessage());
        } finally {
            if (!keepIntermediateFiles)
                FileUtils.deleteDirectoryRecursively(scratchDirectory);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) {
            Pantools.logger.debug("Setting pangenome node properties.");
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("num_k_mers", INDEX_SC.length());
            pangenome_node.setProperty("num_nodes", num_nodes);
            pangenome_node.setProperty("num_degenerate_nodes", num_degenerates);
            pangenome_node.setProperty("num_edges", num_edges);
            pangenome_node.setProperty("num_genomes", GENOME_DB.num_genomes);
            pangenome_node.setProperty("num_bases", num_bases);
            pangenome_node.setProperty("k_mer_highest_frequency", highest_frequency);
            tx.success();
        } catch (Exception e) {
            Pantools.logger.error("Error adding information to the pangenome node.");
            throw new RuntimeException(e.getMessage());
        }
       
        genome_overview();
        Pantools.logger.info("Number of kmers:   {}", INDEX_SC.length());
        Pantools.logger.info("Number of nodes:   {}", num_nodes);
        Pantools.logger.info("Number of edges:   {}", num_edges);
        Pantools.logger.info("Number of bases:   {}", num_bases);
        Pantools.logger.info("Number of degenerate nodes:   {}", num_degenerates);
        Pantools.logger.info("graph.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH)));
        Pantools.logger.info("index.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + INDEX_DATABASE_PATH)));
        Pantools.logger.info("genome.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GENOME_DATABASE_PATH)));
    }
  
    /**
     * Adds new genomes to an available pangenome.
     */
    public void add_genomes(Path scratchDirectory) throws IOException {
        Pantools.logger.info("Adding additional genomes to an already existing pangenome.");
        check_if_program_exists_stdout("kmc -h", 100, "kmc", true); // check if program is set to $PATH
        int previous_num_genomes;
        Node pangenome_node;
        verify_if_all_genome_files_exist();
        scratchDirectory = FileUtils.createScratchDirectory(scratchDirectory);
        connect_pangenome();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            if (pangenome_node == null) {
                Pantools.logger.error("Can not locate database node.");
                System.exit(1);
            }
        // Reads the properties of the pangenome
            num_nodes = (long) pangenome_node.getProperty("num_nodes");
            num_edges = (long) pangenome_node.getProperty("num_edges");
            num_degenerates = (int) pangenome_node.getProperty("num_degenerate_nodes");
            num_bases = 0;
            previous_num_genomes = (int) pangenome_node.getProperty("num_genomes");
            tx.success();
        }
        GENOME_DB.add_genomes(PATH_TO_THE_GENOMES_FILE);
        INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE, GENOME_DB, GRAPH_DB, INDEX_DB, previous_num_genomes);
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        genomeSc = new SequenceScanner(GENOME_DB, previous_num_genomes + 1, 1, K_SIZE, INDEX_SC.get_pre_len());
    // the sequences should be dropped out as they will change and add_sequence_properties() function will rebuild them.
        drop_nodes_property("sequence");
        drop_nodes_property("frequencies");
        drop_nodes_property("frequency");
    // the edge colors should be dropped out as they will change and localize_nodes() function will rebuild them again.
        drop_edges_colors();
        construct_pangenome(pangenome_node);
        add_sequence_properties(scratchDirectory);
        localize_nodes();
        genome_overview();

        FileUtils.deleteDirectoryRecursively(scratchDirectory);
        Pantools.logger.info("Number of kmers:   {}", INDEX_SC.length());
        Pantools.logger.info("Number of nodes:   {}", num_nodes);
        Pantools.logger.info("Number of edges:   {}", num_edges);
        Pantools.logger.info("Number of bases:   {}", num_bases);
        Pantools.logger.info("Number of degenerate nodes:   {}", num_degenerates);
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node.setProperty("k_mer_size", K_SIZE);
            pangenome_node.setProperty("num_k_mers", INDEX_SC.length());
            pangenome_node.setProperty("num_nodes", num_nodes);
            pangenome_node.setProperty("num_degenerate_nodes", num_degenerates);
            pangenome_node.setProperty("num_edges", num_edges);
            pangenome_node.setProperty("num_genomes", GENOME_DB.num_genomes);
            pangenome_node.setProperty("num_bases", num_bases);
            tx.success();
        }
        boolean exists = true;
        int file_counter = 0;
        while (exists){
            file_counter ++;
            exists = check_if_file_exists(WORKING_DIRECTORY + "log/added_genomes_" + file_counter + ".log");
        }
        write_string_to_file_full_path("Increased the size of the database with " + (GENOME_DB.num_genomes-previous_num_genomes) + " genomes to a total of " + GENOME_DB.num_genomes,
                WORKING_DIRECTORY + "log/added_genomes_" + file_counter + ".log");

        Pantools.logger.info("graph.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH)));
        Pantools.logger.info("index.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + INDEX_DATABASE_PATH)));
        Pantools.logger.info("genome.db size: {} MB", FileUtils.getFolderSize(new File(WORKING_DIRECTORY + GENOME_DATABASE_PATH)));
    }
    
    /**
     * Retrieves a selection of genomic regions from the genome database.
     * 
     */
    public void retrieve_regions() {
        int genome_nr, sequence_nr, begin, end, num_regions = 0, proper_regions = 0;
        Pantools.logger.info("Retrieving genomic regions from the pangenome.");
        if (PATH_TO_THE_REGIONS_FILE == null) {
            Pantools.logger.error("No regions file was provided.");
            System.exit(1);
        }
       
        try {
            BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_REGIONS_FILE));
            while (in.ready()) {
                String line = in.readLine().trim(); 
                if (line.equals("")) {
                    continue;
                }
                ++num_regions;
            }
            in.close();
        } catch (IOException e) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_REGIONS_FILE);
            System.exit(1);
        }
        
        connect_pangenome();
        INDEX_SC = new IndexScanner(INDEX_DB);
        K_SIZE = INDEX_SC.get_K();
        total_genomes = GENOME_DB.num_genomes;
        genomeSc = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_SC.get_pre_len());
        create_directory_in_DB("retrieval/genomes");
        create_directory_in_DB("retrieval/regions");
        int line_counter = 0;
        StringBuilder log_builder = new StringBuilder("# split on semicolon\nline number;region;recognized as;successfull?\n");
        String combi_output_file = WORKING_DIRECTORY + "retrieval/regions/regions_combined.fasta";
        try (Transaction tx = GRAPH_DB.beginTx()) {
            try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_REGIONS_FILE))) {
                BufferedWriter out = new BufferedWriter(new FileWriter(combi_output_file)); // file to combine the sequences 
                while (in.ready()) {
                    String line = in.readLine().trim(); 
                    StringBuilder seq = new StringBuilder();
                    line = line.trim();
                    if (line.equals("")) {
                        continue;
                    }
                    line_counter ++;
                    String region_file = WORKING_DIRECTORY + "retrieval/regions/region_" + line_counter + ".fasta";
                    String[] fields = line.trim().split("\\s");
                    
                    log_builder.append(line_counter).append(";").append(line).append(";");
                    switch (fields.length) {
                        case 1: // a genome number is provided OR the row is not split correctly
                            log_builder.append("genome;");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)){
                                    continue;
                                }
                                log_builder.append("yes\n");
                                proper_regions ++;
                                write_genome_file(genome_nr);
                            } catch(NumberFormatException e) {
                                log_builder.append("no, cannot be converted a number\n");
                                Pantools.logger.info("{} -> Unable to be converted to a genome number.", line);
                            }
                            break;
                        case 2:  // a genome number + sequence number is provided OR the row is not split correctly
                            log_builder.append("genome & sequence;");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                sequence_nr = Integer.parseInt(fields[1]);
                                if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)) {
                                    continue;
                                }
                                if (!rr_check_if_appropriate_sequence(genome_nr, sequence_nr, log_builder, line)) {
                                   continue;
                                }
                                log_builder.append("yes\n");
                                begin = 0;
                                end = (int) GENOME_DB.sequence_length[genome_nr][sequence_nr] - 1;
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, true);
                                out.write(">genome" + genome_nr + "_sequence" + sequence_nr + "\n");
                                write_fasta(out, seq.toString(), 80);
                                
                                BufferedWriter region_writer = new BufferedWriter(new FileWriter(region_file));
                                region_writer.write(">genome" + genome_nr + "_sequence" + sequence_nr + "\n");
                                write_fasta(region_writer, seq.toString(), 80);
                                region_writer.close();
                                proper_regions ++;
                            } catch(NumberFormatException e) {
                                Pantools.logger.info("{} -> Unable to be converted to a genome & sequence number.", line);
                            }
                            break;
                        case 4: case 5:  
                            // 4: a genome number + sequence number + region start + region end position are provided
                            // 5: all numbers AND the strand orientation is provided
                            log_builder.append("genome,sequence, start coordinate, stop coordinate");
                            if (fields.length == 5) {
                                log_builder.append(",strand");
                            }
                            log_builder.append(";");
                            try {
                                genome_nr = Integer.parseInt(fields[0]);
                                sequence_nr = Integer.parseInt(fields[1]);
                                begin = Integer.parseInt(fields[2]);
                                end = Integer.parseInt(fields[3]);
                            } catch(NumberFormatException e) {
                                Pantools.logger.info("{} -> Unable to correctly retrieve four numbers (Integers).", line);
                                continue;
                            }
                            
                            if (!rr_check_if_appropriate_genome(genome_nr, log_builder, line)) {
                                continue;
                            }
                            if (!rr_check_if_appropriate_sequence(genome_nr, sequence_nr, log_builder, line)) {
                                continue;
                            }
                            
                            long last_sequence_position = GENOME_DB.sequence_length[genome_nr][sequence_nr];
                            if (begin < 1 || begin > last_sequence_position) { 
                                Pantools.logger.info("{} ->  The start coordinate should be between 1 and {} for genome {}", line, last_sequence_position, genome_nr);
                                log_builder.append("no, the start coordinate should be between 1 and ").append(last_sequence_position)
                                        .append(" for genome ").append(genome_nr).append("\n");
                                continue;
                            }
                            
                            if (end <= begin || end > GENOME_DB.sequence_length[genome_nr][sequence_nr]) {
                                Pantools.logger.info("{} -> The end coordinate should be between {} and {} for genome {}", line, (begin+1), last_sequence_position, genome_nr);
                                log_builder.append("no, the end coordinate should be between ").append((begin+1)).append(" and ")
                                        .append(last_sequence_position).append(" for genome ").append(genome_nr).append("\n");
                                continue;
                            }
                            
                            log_builder.append("yes\n");
                            proper_regions ++;
                            out.write(">genome" + genome_nr + "_sequence" + sequence_nr + "_" + begin + "_" + end + "\n");
                            begin -= 1;
                            end -= 1;
                                
                            if (line.endsWith("-") || line.endsWith("rv")) { // manual only mentions '-' 
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, false); // reverse complement
                            } else {
                                genomeSc.get_sub_sequence(seq, genome_nr, sequence_nr, begin, end - begin + 1, true);
                            }
                            write_fasta(out, seq.toString(), 80);
                            BufferedWriter region_writer = new BufferedWriter(new FileWriter(region_file));
                            region_writer.write(">genome" + genome_nr + "_sequence" + sequence_nr + "_" + (begin+1) + "_" + (end+1) + "\n");
                            write_fasta(region_writer, seq.toString(), 80);
                            region_writer.close();    
                            break;
                      
                        default: 
                            log_builder.append("region not recognized\n");
                            break;
                    }
                }
                in.close();
                out.close();
                write_SB_to_file_in_DB(log_builder, "retrieval/regions/retrieve_regions.log");
                Pantools.logger.info("{} our of {} genomic regions found and retrieved successfully.", proper_regions, num_regions);
                Pantools.logger.info("Log written to:");
                Pantools.logger.info(" {}retrieval/regions/retrieve_regions.log", WORKING_DIRECTORY);
                Pantools.logger.info("Output written to:");
                Pantools.logger.info(" {}retrieval/", WORKING_DIRECTORY);
                Pantools.logger.info(" {}", combi_output_file);
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to write: {}", combi_output_file);
                System.exit(1);
            }
            tx.success();
        }
    }
    
    /**
     * Part of the retrieve_regions function, check if the genome number is correct
     * @param genome_nr
     * @param log_builder
     * @param line
     * @return 
     */
    public boolean rr_check_if_appropriate_genome(int genome_nr, StringBuilder log_builder, String line) {
        if (genome_nr > total_genomes || genome_nr < 1) {
            Pantools.logger.info("{} -> Genome number should be between 1 and {}", line, total_genomes);
            log_builder.append("no, genome number should be between 1 and ").append(total_genomes).append("\n");
            return false;
        }
        return true;
    }
    
    /**
     * Part of the retrieve_regions function, check if the sequence number is correct
     * @param genome_nr
     * @param sequence_nr
     * @param log_builder
     * @param line
     * @return 
     */
    public boolean rr_check_if_appropriate_sequence(int genome_nr, int sequence_nr, StringBuilder log_builder, String line) {
        int allowed_sequence_nr = GENOME_DB.num_sequences[genome_nr];
        if (sequence_nr > allowed_sequence_nr || sequence_nr < 1) {
            log_builder.append("no, the sequence number should be between 1 and ").append(allowed_sequence_nr).append(" for genome ").append(genome_nr).append("\n");
            Pantools.logger.info("{} -> Sequence number should be between 1 and {} for genome {}", line, allowed_sequence_nr, genome_nr);
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param genome 
     */
    public void write_genome_file(int genome) {
        StringBuilder seq = new StringBuilder();
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genome + ".fasta"));
            for (int sequence = 1; sequence <= GENOME_DB.num_sequences[genome]; ++sequence) {
                out.write(">" + GENOME_DB.sequence_titles[genome][sequence] + "\n");
                int begin = 0;
                int end = (int) GENOME_DB.sequence_length[genome][sequence] - 1;
                genomeSc.get_sub_sequence(seq, genome, sequence, begin, end - begin + 1, true);
                write_fasta(out, seq.toString(), 80);
                seq.setLength(0);
            }
            out.close();
        } catch (IOException e) {
            Pantools.logger.error("Unable to write a genome to {}/retrieval/genome_{}.fasta", WORKING_DIRECTORY, genome);
            System.exit(1);
        }    
    }

    /**
     * Gives the outgoing relationship to take based on the query coordinate.
     * 
     * @param current_node The current node to go out from. 
     * @param origin The string determining query genome and sequence, e.g. "G2S3"
     * @param pos The query position.
     * @return The proper outgoing edge.
     */
    public static Relationship get_outgoing_edge(Node current_node, String origin, int pos) {
        int[] occurrence;
        for (Relationship r_out : current_node.getRelationships(Direction.OUTGOING)) {
            occurrence = (int[])r_out.getProperty(origin, null);
            if (occurrence != null) {
                if (Arrays.binarySearch(occurrence, pos) >= 0)
                    return r_out;
            }
        }
        return null;
    }
    
    /**
     * Appends substring s[from..to] to a given StringBuilder.
     * @param seq The given StringBuilder.
     * @param s The string.
     * @param from Start position in the string.
     * @param to Stop position in the string.
     * @return The length of substring appended to the StringBuilder.
     */
    public static int append_fwd(StringBuilder seq, String s, int from, int to) {
        for (int i = from; i <= to; ++i)
            seq.append(s.charAt(i));
        return to - from + 1;
    }
    
    /**
     * Appends the reverse complement of substring s[from..to] to a given StringBuilder.
     * @param seq The given StringBuilder.
     * @param s The string.
     * @param from Start position in the string.
     * @param to Stop position in the string.
     * @return The length of substring appended to the StringBuilder.
     */
    public static int append_rev(StringBuilder seq, String s, int from, int to) {
        for (int i = to; i >= from; --i)
            seq.append(complement(s.charAt(i)));
        return to - from + 1;
    }
    
    /**
     * 
     * @param address An integer array lile {genome_number, sequence_number, begin_position, end_position}
     * @return A pointer to the genomic position in the pangenome
     */
    
    /**
     * Finds a graph pointer pointing to the specified genomic coordinate in the pangenome.
     * 
     * @param graphDb The pangenome graph database
     * @param genomeSc The genome database
     * @param indexSc The index database
     * @param genome The query genome
     * @param sequence The query sequence
     * @param position The query position
     * @return The graph pointer pointing to the specified genomic coordinate.
     */
    public static IndexPointer locate(GraphDatabaseService graphDb, SequenceScanner genomeSc, IndexScanner indexSc, int genome, int sequence, int position) {
        int i, code, node_start_pos, low, high, mid , node_len, genomic_pos, k_size, pre_len;
        boolean forward, degenerate;
        Node node, neighbor, seq_node;
        Relationship rel;
        String anchor_sides, origin;
        long[] anchor_nodes;
        int[] anchor_positions;
        IndexPointer pointer;
        k_size = indexSc.get_K();
        pre_len = indexSc.get_pre_len();
        kmer first_kmer = new kmer(k_size, pre_len);
        degenerate = false;
        
        if (position <= genomeSc.get_sequence_length(genome, sequence) - k_size) {
            for (i = 0; i < k_size && !degenerate; ++i) {
                code = genomeSc.get_code(genome, sequence, position + i);
                if (code < 4)
                    first_kmer.next_fwd_kmer(code);
                else
                   degenerate = true; 
            }
            if (!degenerate) {
                pointer = new IndexPointer();
                indexSc.get_pointer(pointer, indexSc.find(first_kmer));
                Pantools.logger.trace("voor {}", pointer);
                pointer.canonical ^= first_kmer.get_canonical();
                Pantools.logger.trace("na {}", pointer);
                return pointer;
            }
        }
        origin = "G" + genome + "S" + sequence;
        genomic_pos = genome - 1;
        seq_node = graphDb.findNode(SEQUENCE_LABEL, "identifier", genome + "_" + sequence);
        anchor_nodes = (long[]) seq_node.getProperty("anchor_nodes");
        anchor_positions = (int[]) seq_node.getProperty("anchor_positions");
        anchor_sides = (String) seq_node.getProperty("anchor_sides");
    // Find the immediate preceding anchor_node, searching in the sorted array of anchor positions.      
        for (low = 0, high = anchor_sides.length() - 1, mid = (low + high) / 2; low <= high; mid = (low + high) / 2) {
            if (genomic_pos < anchor_positions[mid]) {
                high = mid - 1;
            } else if (genomic_pos > anchor_positions[mid]) {
                low = mid + 1;
            } else {
                break;
            }
        }
        if (genomic_pos < anchor_positions[mid]) {
            --mid;
        }
        forward = anchor_sides.charAt(mid) == 'F';

        try (Transaction tx = graphDb.beginTx()) {
            node = graphDb.getNodeById(anchor_nodes[mid]);
            node_start_pos = anchor_positions[mid];
            node_len = (int) node.getProperty("length");
        // Traverse the pangenome from the anchor node until reach to the target
            while (node_start_pos + node_len <= genomic_pos) {
                genome = node_start_pos + node_len - k_size + 1;
                rel = get_outgoing_edge(node, origin, genome);
                if (rel == null) {
                    Pantools.logger.info("Failed to locate address : {} {} {}", genome, sequence, genome);
                    break;
                }
                neighbor = rel.getEndNode();
                forward = rel.getType().name().charAt(1) == 'F';
                node_start_pos += node_len - k_size + 1;
                node = neighbor;
                node_len = (int) node.getProperty("length");
            }
            tx.success();
        }
        return new IndexPointer(node.getId(), forward, forward ? genomic_pos - node_start_pos : node_len - 1 - (genomic_pos - node_start_pos), -1l);
    }
    
    /***
     * Creates an edge from source node to destination node.
     * 
     * @param src The source node
     * @param des The destination node
     * @param edge_type One of the four possible edge types: FF, FR, RF, RR
     */
    private void connect(Node src, Node des, RelationshipType edge_type) {
        Pantools.logger.trace("connect {} {} {}", src.getId(), edge_type.name(), des.getId());
        src.createRelationshipTo(des, edge_type);
    }
    
    /**
     * Splits a node at a specified position by creating a new node called 
     * split_node as a part separated from the node.
     * 
     * @param node The node which should be split.
     * @param pos The split position with respect to the start of the node.
     * @return The newly created split node.
     */
    private Node split(Node node, int pos) {
        int split_len, node_len;
        int i, s_id, gen, seq, loc,starts_at;
        long inx,split_first_kmer,node_last_kmer=0;
        int[] address;
        Node neighbor, split_node;
        Relationship rel;
        address = (int[]) node.getProperty("address");
        gen = address[0];
        seq = address[1];
        loc = address[2];
        node_len = (int) node.getProperty("length");
        ++num_nodes;
        split_node = GRAPH_DB.createNode(NUCLEOTIDE_LABEL);
        address[0] = gen;
        address[1] = seq;
        address[2] = loc + pos;
        split_node.setProperty("address", address);
        split_len = node_len - pos;
        split_node.setProperty("length", split_len);
       
        // Updates the 'start' edges comming from the gene node to the nucleotide node.
        Iterable<Relationship> relations = node.getRelationships(RelTypes.starts, Direction.INCOMING);
        for (Relationship r : relations) {
            if (!r.isType(RelTypes.starts)) {
                Pantools.logger.error("This is NOT a 'starts' relationship.");
                System.exit(1);
            }
            starts_at = (int)r.getProperty("offset");
            if (starts_at >= pos) {
                rel = r.getStartNode().createRelationshipTo(split_node, RelTypes.starts);
                rel.setProperty("offset", starts_at - pos);
                rel.setProperty("forward", r.getProperty("forward"));
                rel.setProperty("genomic_position", r.getProperty("genomic_position"));
                r.delete();
            } 
        }     
        
        // Updating the Kmers chain in the index  
        node_last_kmer = INDEX_SC.find(genomeSc.make_kmer(gen,seq,loc+pos-1));
        split_first_kmer = INDEX_SC.get_next_index(node_last_kmer);
        INDEX_SC.put_next_index(-1L, node_last_kmer);
        split_node.setProperty("first_kmer",split_first_kmer);
        split_node.setProperty("last_kmer",node.getProperty("last_kmer"));
        s_id=(int)split_node.getId();
        for(i=0,inx=split_first_kmer;inx!=-1L;inx= INDEX_SC.get_next_index(inx),++i) // update kmer coordinates
        {
            INDEX_SC.put_node_id(s_id, inx);
            INDEX_SC.put_offset(i, inx);
        }  
        // Moving forward-outgoing and reverse-incoming edges from node to split node.    
        for (Relationship r : node.getRelationships(Direction.OUTGOING, RelTypes.FR, RelTypes.FF)) {
            neighbor = r.getEndNode();
            if (neighbor.equals(node)) 
                neighbor = r.isType(RelTypes.FF) ? node : split_node;
            connect(split_node, neighbor, r.getType());
            r.delete();
        }
        for (Relationship r : node.getRelationships(Direction.INCOMING,RelTypes.RR,RelTypes.FR)) {
            neighbor = r.getStartNode();
            if (neighbor.equals(node)) 
                neighbor = r.isType(RelTypes.RR) ? node : split_node;
            connect(neighbor, split_node, r.getType());
            r.delete();
        }
    //  Connecting node to split node
        if (node.hasRelationship(Direction.INCOMING, RelTypes.FF, RelTypes.RF)) {
            connect(node, split_node, RelTypes.FF);
            ++num_edges;
        }
        if (split_node.hasRelationship(Direction.INCOMING, RelTypes.FR, RelTypes.RR)) {
            connect(split_node ,node, RelTypes.RR);
            ++num_edges;
        }
        node.setProperty("last_kmer",node_last_kmer);
        node.setProperty("length", pos + K_SIZE - 1);
        return split_node;
    }
    
    /**
     * Creates and extends a new node till reaching to a visited K-mer or a degenerate region.
     */
    private void create_extend() {
        int[] address;
        long node_id,last_kmer;
        address = genomeSc.get_address();
        address[2] -= K_SIZE - 1;
        ++num_nodes;
        Node new_node = GRAPH_DB.createNode(NUCLEOTIDE_LABEL);
        node_id = new_node.getId();
        Pantools.logger.trace("create {}", new_node.getId());
        new_node.setProperty("address", address);
        new_node.setProperty("length", K_SIZE);
        new_node.setProperty("last_kmer",genomeSc.get_curr_index());
        new_node.setProperty("first_kmer",genomeSc.get_curr_index());
    // Set the pointer to the Kmer in the pointer database    
        INDEX_SC.put_pointer(node_id, 0, genomeSc.get_curr_kmer().get_canonical(), -1l, genomeSc.get_curr_index());
        connect(curr_node ,new_node, RelTypes.values()[curr_side*2]);
        ++num_edges;
        curr_node = new_node;
        curr_side = 0;

        Pantools.logger.trace("extending node {}", curr_node.getId());
        int begin, len;
        last_kmer=(long)curr_node.getProperty("last_kmer");
        boolean broke;
        Node degenerate_node = null;
        len = (int) curr_node.getProperty("length");
        broke = false;
        while (genomeSc.get_position() < genomeSc.get_sequence_length() - 1) { // Not reached to the end of the sequence
            Pantools.logger.trace("extend {}", genomeSc.get_position());
            if (genomeSc.get_code(1) > 3) { // hit a degenerate region
                genomeSc.next_position();
                begin = genomeSc.get_position() - K_SIZE + 1;
                curr_node.setProperty("length", len);
                curr_node.setProperty("last_kmer",last_kmer);                
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(curr_node ,degenerate_node, RelTypes.FF); 
                ++num_edges;
                curr_node = degenerate_node;
                break;
            } else {
                genomeSc.next_position();
                genomeSc.get_curr_kmer().next_fwd_kmer(genomeSc.get_code(0));
                if (SHOW_KMERS) Pantools.logger.info(genomeSc.get_curr_kmer().toString());
            }
            genomeSc.set_curr_index(INDEX_SC.find(genomeSc.get_curr_kmer()));
            if (INDEX_SC.is_empty(genomeSc.get_curr_index())) {
                INDEX_SC.put_next_index(genomeSc.get_curr_index(),last_kmer);
                ++len;
                INDEX_SC.put_pointer(node_id, len - K_SIZE, genomeSc.get_curr_kmer().get_canonical(), -1l, genomeSc.get_curr_index());
                last_kmer=genomeSc.get_curr_index();
            } else {
                broke = true;
                break;
            }
        }
        if (degenerate_node == null) {
            new_node.setProperty("length", len);
            new_node.setProperty("last_kmer",last_kmer);
        }
        if (!broke && genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {// because the last kmer is somewhere in the graph we should get connected to
            finish = true;
        }
    }

    /**
     * Finds, splits and follows (in forward direction) a node pointed by a 
     * pointer till the first k-mer different to what is observed in the scanned sequence.
     * 
     * @param pointer The pointer 
     */
    private void follow_forward(IndexPointer pointer) {
        int l, pos, begin, g, s, loc, side;
        Node node, split_node1, split_node2,des, src, degenerate_node = null;
        RelationshipType rel_type;
        int[] address;
        boolean loop, repeated_edge;
        pos = pointer.offset;
        node = GRAPH_DB.getNodeById(pointer.node_id);
        Pantools.logger.trace("follow_forward {} at {}", pointer.node_id, pos);
    // The first split might be done to seperate the part we need to enter in.
        if (pos > 0) {  
            Pantools.logger.trace("first_split {} at {}", node.getId(), pos);
            split_node1 = split(node, pos);
            if (loop = (curr_node.equals(node) && curr_side == 0)) 
                src = split_node1;
            else 
                src = curr_node;
            node = split_node1; // Note : assigning reference variables is dangerous! if split_node changes node will change as well.
        } else {
            split_node1 = node;
            if (loop = (curr_node.equals(node) && curr_side == 0)) 
                src = split_node1;
            else 
                src = curr_node;
        }
        des = split_node1;
        side=curr_side*2;
        curr_side = 0;
        l = (int) node.getProperty("length") - K_SIZE;
        address = (int[]) node.getProperty("address");
        g = address[0];
        s = address[1];
        loc = address[2];
    // Follow the shared part
        for (pos = 0; pos <= l && genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(g, s, loc + pos + K_SIZE - 1) == genomeSc.get_code(0); ++pos) {
            genomeSc.next_position();
         // If hit a degenarate region aplit and branch to a degenerate node 
            if (genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(0) > 3) {
                begin = genomeSc.get_position() - K_SIZE + 1;
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                if (pos + 1 <= l) {
                    split_node2 = split(node, pos + 1);
                    if (loop)
                        src = split_node2;
                }
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(node ,degenerate_node, RelTypes.FF);
                ++num_edges;
                break;
            }
        }
        if (genomeSc.get_position() == genomeSc.get_sequence_length()) {
            finish = true;
        } else if (degenerate_node == null) // build the Kmer of difference 
            initialize(genomeSc.get_position() - K_SIZE + 1);
    //  A second split might be needed   
        if (degenerate_node == null && pos <= l) {
            Pantools.logger.trace("second_split {} at {}", node.getId(), pos);
            split_node2 = split(node, pos);
            if (loop)
                src = split_node2;
        }
    // connect the current node before doing splits to the split_node1    
        rel_type = RelTypes.values()[side];
        repeated_edge = false;
        for (Relationship r: src.getRelationships(rel_type, Direction.OUTGOING))
            if (r.getEndNode().equals(des)) {
                repeated_edge = true;
                break;
            }
        if (!repeated_edge) {
            connect(src ,des, rel_type);
            ++num_edges;
        }
        if (degenerate_node != null) {
            curr_side = 0; // not really needed
            curr_node = degenerate_node;
        } else
            curr_node = node;
    }
    
    /**
     * Finds, splits and follows (in reverse direction) a node pointed by a 
     * pointer till the first k-mer different to what is observed in the scanned sequence.
     * 
     * @param pointer The pointer 
     */
    private void follow_reverse(IndexPointer pointer) {
        int pos, begin, g, s, loc, side;
        int[] address;
        Node node, split_node1, split_node2 ,des, src, degenerate_node = null;
        boolean loop, first_split = false, repeated_edge;
        pos = pointer.offset;
        node = GRAPH_DB.getNodeById(pointer.node_id);
        Pantools.logger.trace("follow_reverse {} at {}", pointer.node_id, pos);
        RelationshipType rel_type;
        split_node2 = node; //if the second split does not happens remains unchanged
        if (pos < (int) node.getProperty("length") - K_SIZE) {
            Pantools.logger.trace("first_split {} at {}", node.getId(), (pos+1));
            first_split = true;
            split_node1 = split(node, pos+1);
            if (loop = curr_node.equals(node) && curr_side == 0) // might be in reverse side due to a follow reverse
                src = split_node1;
            else 
                src = curr_node;
        } else {
            split_node1 = node;
            if (loop = curr_node.equals(node) && curr_side == 0)
                src = split_node1;
            else 
                src = curr_node;
        }
        des = node;
        side=curr_side*2+1;
        curr_side = 1;
        address = (int[]) node.getProperty("address");
        g = address[0];
        s = address[1];
        loc = address[2];
        for (pos = (int) node.getProperty("length") - K_SIZE; pos >= 0 && genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(g, s, loc + pos) == genomeSc.get_complement_code(0); --pos) {
            genomeSc.next_position();
            if (genomeSc.get_position() <= genomeSc.get_sequence_length() - 1 && genomeSc.get_code(0) > 3) {
                begin = genomeSc.get_position() - K_SIZE + 1;
                genomeSc.jump_forward();
                if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                    genomeSc.next_position();// to acheive the right length for the degenerate node    
                    finish = true;
                }                
                if (pos > 0) {
                    split_node2 = split(node, pos);
                    des = split_node2;
                    if (!first_split && loop)
                        src = split_node2;
                }
                int[] add = genomeSc.get_address();
                add[2] = begin;
                degenerate_node = create_degenerate(add);
                connect(split_node2, degenerate_node, RelTypes.RF);
                ++num_edges;
                break;
            }
        }
        if (genomeSc.get_position() == genomeSc.get_sequence_length()) {
            finish = true;
        } else if (degenerate_node == null) // build the Kmer of difference 
            initialize(genomeSc.get_position() - K_SIZE + 1);
        if (degenerate_node == null && pos >= 0) {
            Pantools.logger.trace("second_split {} at {}", node.getId(), (pos + 1));
            split_node2 = split(node, pos+1);
            des = split_node2;
            if (!first_split && loop)
                src = split_node2;
        }
        rel_type = RelTypes.values()[side];
        repeated_edge = false;
        for (Relationship r: src.getRelationships(rel_type, Direction.OUTGOING))
            if (r.getEndNode().equals(des)) {
                repeated_edge = true;
                break;
            }
        if (!repeated_edge) {
            connect(src ,des, rel_type);
            ++num_edges;
        }
        if (degenerate_node != null) {
            curr_side = 0;
            curr_node = degenerate_node;
        } else
            curr_node = split_node2;
    }

    /**
     * Creates a degenerate node with a given address {genome, sequence, position}.
     * 
     * @param address The genomic position of the degenerate region
     */
    private Node create_degenerate(int[] address) {
        ++num_degenerates;
        ++num_nodes;
        Node degenerate_node = GRAPH_DB.createNode(DEGENERATE_LABEL);
        degenerate_node.addLabel(NUCLEOTIDE_LABEL);
        Pantools.logger.trace("create_degenerate:{} position:{} begin:{}", degenerate_node.getId(), genomeSc.get_position(), address[2]);
        degenerate_node.setProperty("address", address);
        degenerate_node.setProperty("length", genomeSc.get_position() - address[2]);
        return degenerate_node;
    }
    
    /**
     * Initializes the first kmer of the current sequence which can be possibly
     * located immediately after a degenerate region. 
     * 
     * @param start The start position.
     */
    private void initialize(int start) {
        Node degenerate_node;
        if (genomeSc.initialize_kmer(start) < 0) {// start with a degenerate kmer
            if (genomeSc.get_position() >= genomeSc.get_sequence_length() - 1) {
                genomeSc.next_position();// to acheive the right length for the degenerate node    
                finish = true;
            }
            int[] add = genomeSc.get_address();
            add[2] = 0;
            degenerate_node = create_degenerate(add);
            connect(curr_node ,degenerate_node, RelTypes.values()[curr_side*2]);
            ++num_edges;
            curr_node = degenerate_node;
        }
    }
    
    /**
     * Constructs the pangenome starting from the pangenome node.
     * .
     * @param pangenome_node The pangenome node 
     */
    void construct_pangenome(Node pangenome_node) {
        int trsc = 0;
        Node genome_node, sequence_node;
        IndexPointer pointer = new IndexPointer();
        phaseTime = System.currentTimeMillis();
        Transaction tx = GRAPH_DB.beginTx();
        try {
            while (!genomeSc.end_of_scan()) {
                Pantools.logger.info("Processing genome {}, {}", genomeSc.get_genome(), GENOME_DB.genome_names[genomeSc.get_genome()]);
                genome_node = GRAPH_DB.createNode(GENOME_LABEL);
                genome_node.setProperty("path", GENOME_DB.genome_names[genomeSc.get_genome()]);
                genome_node.setProperty("number", genomeSc.get_genome());
                genome_node.setProperty("num_sequences", GENOME_DB.num_sequences[genomeSc.get_genome()]);
                genome_node.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                pangenome_node.createRelationshipTo(genome_node, RelTypes.has);
                while (!genomeSc.end_of_genome()) {
                    sequence_node = GRAPH_DB.createNode(SEQUENCE_LABEL);
                    sequence_node.setProperty("genome", genomeSc.get_genome());
                    sequence_node.setProperty("number", genomeSc.get_sequence());
                    sequence_node.setProperty("identifier", genomeSc.get_genome() + "_" + genomeSc.get_sequence());
                    sequence_node.setProperty("title", genomeSc.get_title());
                    sequence_node.setProperty("length", genomeSc.get_sequence_length());
                    sequence_node.setProperty("offset", genomeSc.get_offset());
                    genome_node.createRelationshipTo(sequence_node, RelTypes.has);
                    finish = false;
                    System.out.print("\rProcessing sequence " + genomeSc.get_sequence() + "/" + GENOME_DB.num_sequences[genomeSc.get_genome()] +
                            " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() + "       ");
                    curr_node = sequence_node;
                    curr_side = 0;
                    //long last_printed_position = 0;
                    if (genomeSc.get_sequence_length() >= K_SIZE) {
                        initialize(0);
                        while (!finish) {
                            genomeSc.set_curr_index(INDEX_SC.find(genomeSc.get_curr_kmer()));
                            INDEX_SC.get_pointer(pointer, genomeSc.get_curr_index());
                            /*if (genomeSc.get_position() > last_printed_position) {
                                System.out.print("\rProcessing sequence " + genomeSc.get_sequence() + "/" + genomeDb.num_sequences[genomeSc.get_genome()] + 
                                    " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() );
                                last_printed_position += 1000000;
                            }*/
                            if (pointer.node_id == -1L) // kmer is new
                                create_extend();
                            else if (genomeSc.get_curr_kmer().get_canonical() ^ pointer.canonical)// if sides don't agree
                                follow_reverse(pointer);
                            else 
                                follow_forward(pointer);
                            ++trsc;
                            if (trsc >= MAX_TRANSACTION_SIZE) {    
                                tx.success();
                                tx.close();
                                tx = GRAPH_DB.beginTx();
                                trsc = 0;
                            }
                        }
                        connect(curr_node, sequence_node, RelTypes.values()[curr_side*2]);// to point to the last k-mer of the sequence located in the other strand
                        ++num_edges;
                    }
                    System.out.println("\rProcessing sequence " + genomeSc.get_sequence() + "/" + GENOME_DB.num_sequences[genomeSc.get_genome()] +
                            " of genome " + genomeSc.get_genome() + "\tlength=" + genomeSc.get_sequence_length() + "                                            " );
                    genomeSc.next_sequence();
                }//sequences
                System.out.println("\r" + (System.currentTimeMillis() - phaseTime) / 1000 + " seconds elapsed.                                   ");
                genomeSc.next_genome(); 
            }//genomes
            tx.success();
        } finally {
            tx.close();
        }
    }
    
    /**
     * Traces each sequence in the pangenome and adds coordinate information 
     * to the edges and list of anchors information to sequence nodes.
     */
    void localize_nodes() {
        ResourceIterator<Node> sequence_iterator;
        LinkedList<Node> sequence_nodes;
        int trsc = 0, i, len, m, neighbor_length = 0;
        long[] frequencies;
        long frequency;
        char node_side, neighbor_side;
        long length, distance;
        long[] anchor_nodes;
        int[] anchor_positions;
        int[] initial_coordinate = new int[1];
        Node node, neighbor = null, sequence_node;
        StringBuilder nds = new StringBuilder();
        StringBuilder pos = new StringBuilder();
        StringBuilder sds = new StringBuilder();
        String[] ids_list, posis_list;
        String rel_name,origin;
        int[] positions;
        int[] new_positions;
        int[] address = new int[3], addr = null;
        boolean is_node = false, is_degenerate = false, found = true;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            sequence_iterator = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            sequence_nodes = new LinkedList();
            while (sequence_iterator.hasNext())
                sequence_nodes.add(sequence_iterator.next());
            sequence_iterator.close();
            tx.success();
        } catch (Exception e) {
            Pantools.logger.error("Failed to query sequence nodes.");
            throw new RuntimeException(e.getMessage());
        }
        Transaction tx = GRAPH_DB.beginTx();
        try {
            while (!sequence_nodes.isEmpty()) {
                sequence_node = sequence_nodes.remove();
                origin = "G" + ((String)sequence_node.getProperty("identifier")).replace('_', 'S');
                address[0] = (int)sequence_node.getProperty("genome");
                address[1] = (int)sequence_node.getProperty("number");
                length = (long) sequence_node.getProperty("length");
                System.out.print("\rLocalizing sequence " + address[1] + "/" + GENOME_DB.num_sequences[address[0]] + " of genome " + address[0] + "                        ");
                if (length >= K_SIZE) {
                    node = sequence_node;
                    node_side = 'F';
                    distance = 0;
                    for (address[2] = 0; address[2] + K_SIZE - 1 < length && found;) { // K-1 bases of the last node not added
                        found = false;
                        for (Relationship r : node.getRelationships(Direction.OUTGOING)) {
                            rel_name = r.getType().name();
                            if (rel_name.charAt(0) != node_side)
                                continue;
                            neighbor = r.getEndNode();
                            neighbor_side = rel_name.charAt(1);
                            is_node = neighbor.hasLabel(NUCLEOTIDE_LABEL) && !neighbor.hasLabel(DEGENERATE_LABEL);
                            is_degenerate = neighbor.hasLabel(DEGENERATE_LABEL);
                            if (is_node || is_degenerate) {
                                addr = (int[]) neighbor.getProperty("address");
                                neighbor_length = (int) neighbor.getProperty("length");
                            }
                           
                            if ((is_node && genomeSc.compare(address, addr, K_SIZE - 1,
                                 neighbor_side == 'F' ? K_SIZE - 1 : neighbor_length - K_SIZE, 1, neighbor_side == 'F'))
                            || (is_degenerate && Arrays.equals(addr, address))) {
                                found = true;
                                positions = (int[]) r.getProperty(origin, null);
                                if (positions != null) {
                                    len = positions.length;
                                    new_positions = new int[len + 1];
                                    for (i = 0; i < len; ++i) {
                                        new_positions[i] = positions[i];
                                    }
                                    new_positions[i] = address[2];
                                    r.setProperty(origin, new_positions);
                                } else {
                                    initial_coordinate[0] = address[2];
                                    r.setProperty(origin, initial_coordinate);
                                }
                                frequencies = (long[])neighbor.getProperty("frequencies", new long[GENOME_DB.num_genomes + 1]);
                                ++frequencies[address[0]];
                                neighbor.setProperty("frequencies", frequencies);
                                frequency = (long)neighbor.getProperty("frequency", 0l);
                                ++frequency;
                                if (frequency > highest_frequency) {
                                    highest_frequency = frequency;
                                }
                                if (frequency >= GENOME_DB.num_genomes * 100) {
                                    if (!neighbor.hasProperty("high_frequency")) {
                                        neighbor.setProperty("high_frequency", true);
                                    }
                                }
                                neighbor.setProperty("frequency", frequency);
                                if (address[2] >= distance) {
                                    nds.append(neighbor.getId()).append(" ");
                                    sds.append(neighbor_side);
                                    pos.append(address[2]).append(" ");
                                    distance += ANCHORS_DISTANCE;
                                }
                                address[2] = address[2] + neighbor_length - K_SIZE + 1;
                                node = neighbor;
                                node_side = neighbor_side;
                                break;
                            }
                        }
                        ++trsc;
                        if (trsc >= 1000 * MAX_TRANSACTION_SIZE) {
                            tx.success();
                            tx.close();
                            tx = GRAPH_DB.beginTx();
                            trsc = 0;
                        }
                    }
                    if (!found) {
                        Pantools.logger.error("Could not locate position {} from node ID={}", address[2], node.getId());
                        System.exit(1);
                    }
                    m = sds.length();
                    ids_list = nds.toString().split("\\s");
                    posis_list = pos.toString().split("\\s");
                    anchor_nodes = new long[m];
                    anchor_positions = new int[m];
                    for (i = 0; i < m; ++i) {
                        anchor_nodes[i] = Long.valueOf(ids_list[i]);
                        anchor_positions[i] = Integer.valueOf(posis_list[i]);
                    }
                    sequence_node.setProperty("anchor_nodes", anchor_nodes);
                    sequence_node.setProperty("anchor_positions", anchor_positions);
                    sequence_node.setProperty("anchor_sides", sds.toString());
                    nds.setLength(0);
                    pos.setLength(0);
                    sds.setLength(0);
                }
            }//while
            Pantools.logger.info("{} seconds elapsed.", (System.currentTimeMillis() - phaseTime) / 1000);
            tx.success();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        } finally {
           tx.close();
        }
    }
     
    /**
     * Extracts the sequence of the nodes from the genome database and store it 
     * in the nodes.
     */
    void add_sequence_properties(Path scratchDirectory) throws IOException {
        final long start = System.currentTimeMillis();
        // TODO: store IDs more efficiently, or find another way to stream in IDs without caching the whole list
        final Path nucleotideNodeIdsFile = scratchDirectory.resolve(NUCLEOTIDE_NODE_IDS_FILE_NAME);
        long numNodes = 0;
        try (Transaction ignored = GRAPH_DB.beginTx();
             BufferedWriter writer = Files.newBufferedWriter(nucleotideNodeIdsFile);
             ResourceIterator<Node> itr = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL)) {
            while (itr.hasNext()) {
                writer.write(Long.toString(itr.next().getId()));
                writer.write('\n');
                numNodes++;
            }
        } catch (Exception e) {
            Pantools.logger.error("Failed to extract sequence properties.");
            throw new RuntimeException(e.getMessage());
        }

        // Read back node IDs, chunk them up into transactions and write the sequences

        try (BufferedReader reader = Files.newBufferedReader(nucleotideNodeIdsFile)) {
            for (long i = 0; i < numNodes; i += MAX_TRANSACTION_SIZE) {
                // Read chunks of max. transaction size
                try (Transaction tx = GRAPH_DB.beginTx()) {
                    for (long j = 0; j < Math.min(MAX_TRANSACTION_SIZE, numNodes - i); j++) {
                        final long id = Long.parseLong(reader.readLine());
                        final Node node = GRAPH_DB.getNodeById(id);
                        final int[] address = (int[]) node.getProperty("address");
                        final int length = (int) node.getProperty("length");
                        num_bases += length;
                        final StringBuilder sequence = new StringBuilder();
                        genomeSc.get_sub_sequence(sequence, address[0], address[1], address[2], length, true);
                        node.setProperty("sequence", sequence.toString());
                    }

                    tx.success();
                }
            }
        } catch (Exception e) {
            Pantools.logger.error("Failed to write sequences.");
            throw new RuntimeException(e.getMessage());
        }

        // Clean up

        Files.delete(nucleotideNodeIdsFile);
        Pantools.logger.info("Finished adding sequence properties phase in {} ms.", System.currentTimeMillis() - start);
    }

    /**
     * Removes the given property from all the nucleotide and degenerate nodes.
     * 
     * @param property 
     */    
    void drop_nodes_property(String property) {
        int i;
        ResourceIterator<Node> nodes_iterator;
        LinkedList<Node> nodes = new LinkedList();
        Node node;
        try(Transaction tx = GRAPH_DB.beginTx()) {
            nodes_iterator = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
            while (nodes_iterator.hasNext()) {
                nodes.add(nodes_iterator.next());
            }
            tx.success();
            nodes_iterator.close();
        }
        while (!nodes.isEmpty()) {
            try (Transaction tx = GRAPH_DB.beginTx()) {
                for (i = 0; i < MAX_TRANSACTION_SIZE && !nodes.isEmpty(); ++i) {
                    node = nodes.remove();
                    node.removeProperty(property);
                }
                tx.success();
            }
        }
    }

    /**
     * Removes the coordinate information from the edges.
     */    
    void drop_edges_colors() {
        int i;
        ResourceIterator<Relationship> rels;
        Relationship r;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            rels = GRAPH_DB.getAllRelationships().iterator();
            tx.success();
        }
        while (rels.hasNext()) {
            try (Transaction tx = GRAPH_DB.beginTx()) {
                for (i = 0; i < MAX_TRANSACTION_SIZE && rels.hasNext(); ++i) {
                    r = rels.next();
                    if (r.isType(RelTypes.FF) || r.isType(RelTypes.FR) || r.isType(RelTypes.RF) || r.isType(RelTypes.RR))
                        for(String p:r.getPropertyKeys())
                            r.removeProperty(p);
                }
                tx.success();
            }
        }
        rels.close();
    }
}
