package nl.wur.bif.pantools.construction.busco;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Identify BUSCO genes in the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "busco_protein", sortOptions = false, abbreviateSynopsis = true)
public class BuscoProteinCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-A", "--annotations-file"}, paramLabel = "<annotationsFile>")
    @InputFile(message = "{file.annotations}")
    private Path annotationsFile;

    @Option(names = {"-v", "--busco-version"}, paramLabel = "4|5")
    @MatchInteger(value = {4, 5}, message = "{match.busco-version}")
    private int buscoVersion;

    @Option(names = "--odb10")
    @Pattern(regexp = "(.+)_odb10$|automatic_lineage", message = "{pattern.busco-set}")
    private String odb10;

    @Option(names = "--skip-busco")
    void setSkip(String value) {
        buscoSkipList = Arrays.asList(value.toUpperCase().split(","));
    }
    List<String> buscoSkipList;

    @Option(names = {"--longest", "--longest-transcripts"}) // names = {"-A", "--annotations-file"}
    boolean longestTranscripts;

    @Option(names = "--phasing")
    boolean phasing;

    @Option(names = "--all-automatic")
    boolean allAutomatic;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        Busco busco = new Busco(buscoVersion, odb10, phasing);
        String[] questionableBuscos = defineQuestionableBUSCOs();
        busco.buscoProtein(questionableBuscos, allAutomatic);
        return 0;
    }

    private String[] defineQuestionableBUSCOs() {
        if (buscoSkipList != null) {
            SELECTED_NAME = buscoSkipList.toString().replaceAll("[\\[\\]]", "");
            return SELECTED_NAME.split(",");
        }
        return new String[0];
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = annotationsFile.toString();
        longest_transcripts = longestTranscripts;
    }

}
