package nl.wur.bif.pantools.construction.repeats;

import nl.wur.bif.pantools.analysis.classification.Classification;
import nl.wur.bif.pantools.construction.index.IndexPointer;
import nl.wur.bif.pantools.analysis.sequence_visualization.SequenceVisualization;
import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.construction.build_pangenome.GenomeLayer.locate;
import static nl.wur.bif.pantools.construction.synteny.Synteny.retrieveSyntenySequenceCombinations;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

// class holds two functionalities: add_repeats and repeat_overview
public class Repeats {

    final public static String[] COLOR_CODES2 = new String[]{"#e6194B", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4",
            "#42d4f4", "#f032e6", "#bfef45", "#fabed4", "#469990", "#dcbeff", "#9A6324", "#fffac8", "#800000", "#aaffc3",
            "#808000", "#ffd8b1", "#000075", "#a9a9a9", "#000000", "#ffffff"}; // length is 22, last one is white.

    /**
     *
     */
    public void addRepeats(Path annotationsFile, boolean strict, boolean connectAnnotations) {
        Pantools.logger.info("Adding repeats and transposable elements to the pangenome.");
        if (!connectAnnotations) {
            Pantools.logger.info("--connect was not included. Repeat nodes will not be connected to the nucleotide layer.");
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            throw new RuntimeException("Unable to start the pangenome database");
        }

        int[] previousRepeatAnnoVersions = getPreviousRepeatAnnoVersion();
        int lineCounter = 0;
        try {
            BufferedReader br = Files.newBufferedReader(annotationsFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                lineCounter++;
                line = line.trim();
                String[] lineArray = line.split(" "); // [ genome number, path to gff]
                if (lineArray.length != 2) {
                    throw new RuntimeException(annotationsFile + " is not correctly formatted! "
                            + "Each line should contain a genome number with the full path to a gff file, separated by a space. "
                            + "Line " + lineCounter + " should have 2 columns instead of " + lineArray.length + "\n");
                }
                int genomeNr = 0;
                try {
                    genomeNr = Integer.parseInt(lineArray[0]);
                } catch (NumberFormatException nfe) {
                    throw new RuntimeException("Did not include a number in the first column of the included file. " +
                            "Found instead: " + lineArray[0]);
                }
                HashMap<String, String> sequenceNodeMap = gatherSequenceNodesForGenome(genomeNr);
                HashMap<String, Long> sequenceLengthMap = createSequenceLengthMap(genomeNr);
                readRepeatGff(Paths.get(lineArray[1]), sequenceNodeMap, sequenceLengthMap, genomeNr,
                        previousRepeatAnnoVersions[genomeNr-1], lineCounter, strict, connectAnnotations);
            }
        } catch (IOException ioe) {
           throw new RuntimeException("Failed to read: " + annotationsFile);
        }
    }

    /**
     *
     */
    public void repeat_overview(Path repeatSelectionFile, int windowLength, int upstreamLength, int downstreamLength) {
        Pantools.logger.info("Calculating repeat densities.");
        compare_sequences = true;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome("repeat_overview"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // sets grouping_version and longest_transcripts
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            throw new RuntimeException("Unable to start the pangenome database");
        }
        createDirectory("repeats/windows", true);
        createDirectory("repeats/plots", true);

        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(10000, false);

        HashSet<String> repeatTypes = retrieveRepeatTypes(selectedSequences);
        ArrayList<String> selected_repeat_types = updateRepeatTypesForAnalysis(repeatTypes, repeatSelectionFile);
        String ggplot_manual_colors = determine_repeat_rscript_colors(selected_repeat_types);
        boolean check_types = false;
        if (repeatTypes.size() != selected_repeat_types.size()) {
            check_types = true;
        }

        HashMap<Node, ArrayList<Node>> mrnas_for_genes_at_same_pos = new HashMap<>(); // key is gene node, value is arraylist with mrna nodes
        HashMap<String, Node[]> repeatNodesPerSequence = getRepeatNodesPerSequenceAscending(check_types, selected_repeat_types,
                selectedGenomes, selectedSequences); // get repeat nodes
        if (repeatNodesPerSequence.isEmpty()) {
            throw new RuntimeException("No repeats found in the pangenome. Please run 'add_repeats' first.");
        }

        HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence = getRepeatAddressesPerSequence(repeatNodesPerSequence); // get the address per node
        HashMap<String, ArrayList<String>> repeat_types_per_seq = get_repeat_types_per_seq(repeatNodesPerSequence); // get the repeat type

        HashMap<String, int[]> gene_start_pos_per_seq = new HashMap<>(); // key is sequence identifier, value is an array with all start positions of genes on a sequence
        HashMap<String, Node[]> gene_nodes_per_seq = Classification.get_ordered_genes_per_seq(false,
                mrnas_for_genes_at_same_pos, gene_start_pos_per_seq, null); // key is sequence identifier, value is array with 'gene' nodes
        HashMap<String, ArrayList<int[]>> gene_addresses_per_seq = get_gene_addresses_per_seq(gene_nodes_per_seq);

        ArrayList<String> sequences_with_genes = new ArrayList<>(gene_nodes_per_seq.keySet()); // add all keys to list
        for (String seq_id : sequences_with_genes) {
            if (!repeatNodesPerSequence.containsKey(seq_id)) {
                gene_addresses_per_seq.remove(seq_id);
                gene_nodes_per_seq.remove(seq_id);
            }
        }

        calcateRepeatDensityCoverageForWindows(windowLength, selected_repeat_types, repeatAddressesPerSequence,
                repeat_types_per_seq, WORKING_DIRECTORY + "/repeats/", selectedSequences);
        find_repeat_gene_overlap(gene_addresses_per_seq, repeatAddressesPerSequence, windowLength, upstreamLength,
                downstreamLength, gene_nodes_per_seq, repeat_types_per_seq);

        create_repeat_density_rscripts(repeatAddressesPerSequence, ggplot_manual_colors, true);
        create_repeat_density_rscripts(repeatAddressesPerSequence, ggplot_manual_colors, false);

        HashMap<String, Integer> sequenceLengthMap = getSequenceLengths(selectedSequences);
        create_repeat_density_rscripts2(ggplot_manual_colors, true, sequenceLengthMap );
        create_repeat_density_rscripts2(ggplot_manual_colors, false,  sequenceLengthMap );
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}repeats/density_plot.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/coverage_plot.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/density_plot_two_sequences.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/coverage_plot_two_sequences.R", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/statistics_genomes_sequences.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/windows_all_sequences.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}repeats/repeats_in_genes.csv", WORKING_DIRECTORY);
    }

    /**
     *
     * @param repeatAddressesPerSequence
     * @param ggplot_manual_colors
     */
    private void create_repeat_density_rscripts(HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence,
                                                      String ggplot_manual_colors, boolean density) {

        String densityOrCoverageStr = "Density";
        if (!density){
            densityOrCoverageStr = "Coverage";
        }
        StringBuilder sequences = new StringBuilder("sequence_identifiers <- c(");
        for (String sequenceId : repeatAddressesPerSequence.keySet()) {
            sequences.append("\"").append(sequenceId).append("\",");
        }

        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder();
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#The next line can be ignored assuming the R libraries were installed via Conda. "
                        + "If not, use it to manually install the required R package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"scales\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("\n")
                .append("main_function <- function(sequence_identifiers) {\n")
                .append("    total_files <- toString(length(sequence_identifiers))\n")
                .append("    for (i in 1:length(sequence_identifiers)) {\n")
                .append("        cat(\"\\rPlot\", i, \"/\", total_files, \"\")\n")
                .append("        sequence_id <- sequence_identifiers[i]\n");

        if (density) {
            rscript.append("        file <- paste(\"").append(WD_full_path).append("repeats/density/\", sequence_id, \"_types.csv\", sep=\"\")\n")
                   .append("        df = read.csv(file)\n")
                   .append("        stacked <- ggplot(df, aes(fill=type, y=density, x=position)) +\n");
        } else { // coverage plot
            rscript.append("        file <- paste(\"").append(WD_full_path).append("repeats/coverage/\", sequence_id, \"_types.csv\", sep=\"\")\n")
                    .append("        df = read.csv(file)\n")
                    .append("        stacked <- ggplot(df, aes(fill=type, y=coverage, x=position)) +\n")
                    .append("        expand_limits(y = c(0, 100)) +\n");
        }

        rscript.append("            theme_classic(base_size = 20) + labs(x = \"Genomic position\") +\n")
               .append("            scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6)) +\n")
               .append("\n")
               .append("            # uncomment line below to remove legend\n")
               .append("            #theme(legend.position = \"none\") +\n")
               .append("            ").append(ggplot_manual_colors).append("\n\n");

        rscript.append("        stacked_absolute <- stacked + geom_bar(position=\"stack\", stat=\"identity\") + labs(y = \"" + densityOrCoverageStr + " per Mb\")\n");
        if (density) {
            rscript.append("        stacked_100 <- stacked + geom_bar(position=\"fill\", stat=\"identity\") + labs(y = \"Percentage abundance\")\n");
        }

        rscript.append("\n")
               .append("        outfile1 <- paste(\"").append(WD_full_path).append("repeats/plots/\", sequence_id, \"_" +  densityOrCoverageStr.toLowerCase() + ".png\", sep=\"\")\n")
               .append("        ggsave(stacked_absolute, file= outfile1, width = 50, height = 15, units = \"cm\")\n");

        if (density) {
             rscript.append("        outfile2 <- paste(\"").append(WD_full_path).append("repeats/plots/\", sequence_id, \"_" + densityOrCoverageStr.toLowerCase() + "_abundance.png\", sep=\"\")\n")
                    .append("        ggsave(stacked_100, file= outfile2, width = 50, height = 15, units = \"cm\")\n");
        }
        rscript.append("    }\n")
               .append("cat(\"\\nOutput written to: ").append(WD_full_path).append("repeats/plots/\\n\\n\")\n")
               .append("}\n")
               .append(sequences.toString().replaceFirst(".$", "")).append(")\n")
               .append("main_function(sequence_identifiers)\n");
        write_SB_to_file_in_DB(rscript, "repeats/" + densityOrCoverageStr.toLowerCase() + "_plot.R");
    }

    private void create_repeat_density_rscripts2(String ggplot_manual_colors, boolean density, HashMap<String, Integer> sequenceLengthMap) {
        HashSet<String> seq_combinations = retrieveSyntenySequenceCombinations("syntelogs",
                false, selectedSequences, selectedGenomes);
        if (seq_combinations.isEmpty()) { // no synteny or no sequence combinations
            return;
        }
        SequenceVisualization visual = new SequenceVisualization();
        ArrayList<String> selectedSequencesList = new ArrayList();
        for (String seqId : selectedSequences) {
            selectedSequencesList.add(seqId);
        }
        createDirectory(WORKING_DIRECTORY + "sequence_visualization/synteny/", false);
        visual.createSyntenyInput(selectedSequencesList);

        String densityOrCoverageStr = "Density";
        if (!density) {
            densityOrCoverageStr = "Coverage";
        }

        StringBuilder renamedSeqCombis = new StringBuilder("renamed_seq_combis <- c(");
        StringBuilder seqCombinations = new StringBuilder("seq_combis <- c(");
        StringBuilder lengths = new StringBuilder("plot_lengths <- c(");
        for (String seq_combi : seq_combinations) {
            seqCombinations.append("\"").append(seq_combi).append("\",");
            String renamed_seq_combi = seq_combi.replace("_","S").replace("#","_");
            renamedSeqCombis.append("\"").append(renamed_seq_combi).append("\",");

            String[] seq_combi_array = seq_combi.split("#");
            int seq1_length = sequenceLengthMap.get(seq_combi_array[0]);
            int seq2_length = sequenceLengthMap.get(seq_combi_array[1]);
            int longest_sequence = Math.max(seq2_length, seq1_length); // take the highest value
            lengths.append(longest_sequence).append(",");
        }

        StringBuilder rscript = new StringBuilder();
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#The next line can be ignored assuming the R libraries were installed via Conda. "
                        + "If not, use it to manually install the required R package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append("/home/user/").append("\")\n")
                .append("#install.packages(\"scales\", lib=\"").append("/home/user/").append("\")\n")
                .append("#install.packages(\"cowplot\", lib=\"").append("/home/user/").append("\")\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("library(cowplot)\n\n")

                .append("main_function <- function(seq_combis, renamed_seq_combis, plot_lengths) {\n")
                .append("    total_files <- toString(length(seq_combis))\n")
                .append("    for (i in 1:length(seq_combis)) {\n")
                .append("        cat(\"\\rPlot\", i, \"/\", total_files, \"\")\n")
                .append("        sequence_combi <- seq_combis[i]\n")
                .append("        seq_combi_list <- strsplit(sequence_combi, split = \"#\")\n")
                .append("        seq_combi_list <- unlist(seq_combi_list)\n")
                .append("        renamed_seq_combi <- renamed_seq_combis[i]\n");

        // first density plot
        rscript.append("        file1 <- paste(\"").append(WD_full_path).append("repeats/" + densityOrCoverageStr.toLowerCase() + "/\", seq_combi_list[1], \"_types.csv\", sep=\"\")\n")
                .append("        df_1 = read.csv(file1, header= TRUE)\n")
                .append("        stacked_1 <- ggplot(df_1, aes(fill=type, y=" + densityOrCoverageStr.toLowerCase() + ", x=position)) +\n")
                .append("            theme_classic(base_size=20) + theme(legend.position = \"none\") +\n")
                .append("            theme(axis.title.x=element_blank(), axis.text.x=element_blank()) +\n")
                .append("            expand_limits(x = c(0, plot_lengths[i])) +\n");

        if (!density) {
            rscript.append("        expand_limits(y = c(0, 100)) +\n");
        }

        rscript.append("            scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6), expand = c(0.005, 0.01)) +\n")
                .append("            theme(plot.margin=unit(c(0.01,0.01,0.01,0.01), \"cm\")) +\n")
                .append("            ").append(ggplot_manual_colors).append("\n")
                .append("\n")
                .append("        stacked_absolute_1 <- stacked_1 + geom_bar(position=\"stack\", stat=\"identity\") + labs(y = \"" + densityOrCoverageStr + " per Mb\")\n");

        if (density) {
            rscript.append("        stacked_100_1 <- stacked_1 + geom_bar(position=\"fill\", stat=\"identity\") + labs(y = \"Percentage abundance\")\n\n");
        }

        // second density plot
        rscript.append("        file2 <- paste(\"").append(WD_full_path).append("repeats/" + densityOrCoverageStr.toLowerCase() + "/\", seq_combi_list[2], \"_types.csv\", sep=\"\")\n")
                .append("        df_2 = read.csv(file2, header= TRUE)\n")
                .append("        stacked_2 <- ggplot(df_2, aes(fill=type, y=" +  densityOrCoverageStr.toLowerCase() +", x=position)) +\n")
                .append("            labs(x = \"Genomic position\") + \n")
                .append("            theme_classic(base_size=20) + theme(legend.position = \"none\") +\n")
                .append("            expand_limits(x = c(0, plot_lengths)) +\n");
        if (!density) {
            rscript.append("        expand_limits(y = c(0, 100)) +\n");
        }
        rscript.append("            scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6), expand = c(0.005, 0.01)) +\n")
                .append("            theme(plot.margin=unit(c(0.01,0.01,0.01,0.01), \"cm\")) +\n")
                .append("            ").append(ggplot_manual_colors).append("\n")
                .append("\n")
                .append("        stacked_absolute_2 <- stacked_2 + geom_bar(position=\"stack\", stat=\"identity\") + labs(y = \"" + densityOrCoverageStr + " per Mb\")\n")
                .append("        stacked_100_2 <- stacked_2 + geom_bar(position=\"fill\", stat=\"identity\") + labs(y = \"Percentage abundance\")\n")
                .append("\n");

        // synteny plot
        //appendSyntenyGgplot(rscript, renamed_seq_combi, longest_sequence, 0);
        rscript.append("        file3 <- paste(\"").append(WD_full_path).append("sequence_visualization/synteny/\", renamed_seq_combi, \"_major_blocks.csv\", sep=\"\")\n");
        rscript.append("        df_synteny = read.csv(file3, header= TRUE)\n")
               .append("        synteny_0 <- ggplot() +\n")
               .append("            geom_polygon(data=df_synteny, mapping=aes(x=x, y=y, group=block_nr, fill=type), alpha = 0.5) +\n")
               .append("            theme_classic(base_size=20) +\n")
               .append("            theme(legend.position = \"none\") +\n")
               .append("            theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.line.x=element_blank(), axis.ticks=element_blank()) +\n")
               .append("            theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.line.y=element_blank() ) +\n")
               .append("            expand_limits(x = c(0, plot_lengths[i])) +\n")
               .append("            theme(plot.margin=unit(c(0.01, 0.01, 0.01, 0.01), \"cm\")) +\n")
               .append("            scale_x_continuous(expand = c(0.005, 0.01)) +\n")
               .append("            scale_fill_manual(values=c(\"major\" = \"#61b2ff\", \"majorRV\" = \"#ff7373\", " +
                                          "\"minor\" = \"#46a5ff\", \"minorRV\" = \"#ff5757\"))\n")
               .append("\n");

        // combine plots
        if (density) {
            rscript.append("        combi_plot2 <- plot_grid(stacked_100_1, synteny_0, stacked_100_2, ncol = 1, align = 'v', axis = 'lr', rel_heights = c(1, 0.5, 1))\n")
                    .append("        combi_file2 <- paste(\"").append(WD_full_path).append("repeats/plots/\", renamed_seq_combi, \"_density_percentage.png\", sep=\"\")\n")
                    .append("        ggsave(combi_plot2, file= combi_file2, width = 40, height = 15, units = \"cm\")\n\n");
        }

        rscript.append("        combi_plot1 <- plot_grid(stacked_absolute_1, synteny_0, stacked_absolute_2, ncol = 1, align = 'v', axis = 'lr', rel_heights = c(1, 0.5, 1))\n")
               .append("        combi_file1 <- paste(\"").append(WD_full_path).append("repeats/plots/\", renamed_seq_combi, \"_" + densityOrCoverageStr.toLowerCase() + ".png\", sep=\"\")\n")
               .append("        ggsave(combi_plot1, file= combi_file1, width = 40, height = 15, units = \"cm\")\n")
               .append("    }\n")
               .append("cat(\"\\nOutput written to: ").append(WD_full_path).append("repeats/plots/\\n\\n\")\n")
               .append("}\n\n");

        rscript.append(seqCombinations.toString().replaceFirst(".$", "")).append(")\n")
            .append(renamedSeqCombis.toString().replaceFirst(".$", "")).append(")\n")
            .append(lengths.toString().replaceFirst(".$", "")).append(")\n")
            .append("main_function(seq_combis, renamed_seq_combis, plot_lengths)\n");
        write_SB_to_file_in_DB(rscript, "repeats/" + densityOrCoverageStr.toLowerCase() + "_plot_two_sequences.R");
    }

    /**
     * Creates repeats_in_genes.csv
     * @param gene_addresses_per_seq
     * @param repeatAddressesPerSequence
     * @param windowLength
     * @param upstreamLength
     * @param downstreamLength
     * @param gene_nodes_per_seq
     * @param repeat_types_per_seq
     */
    private void find_repeat_gene_overlap(HashMap<String, ArrayList<int[]>> gene_addresses_per_seq,
                                                HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence,
                                                int windowLength, int upstreamLength, int downstreamLength,
                                                HashMap<String, Node[]> gene_nodes_per_seq,
                                                HashMap<String, ArrayList<String>> repeat_types_per_seq) {

        if (gene_addresses_per_seq.isEmpty()) {
            write_string_to_file_in_DB("No gene annotations are available in the pangenome\n", "repeats/repeats_in_genes.csv");
            return;
        }

        CollectRepeatsInWindow collect = new CollectRepeatsInWindow();
        DecimalFormat df0 = new DecimalFormat("0.");
        DecimalFormat df2 = new DecimalFormat("0.00");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "repeats/repeats_in_genes.csv"))) {
                out.write("#Upstream region length: " + upstreamLength + "\n" +
                        "#Downstream region length: " + downstreamLength + "\n");
                out.write("Gene identifier,Node identifier,Sequence,Gene start position,Gene stop position,Gene length (bp),");
                out.write("Repeats in gene,Repeat types in gene (seperated by space),Gene positions overlapped with repeats," +
                        "Percentage of gene overlapped with repeats,Gene falls in in window number(s),bases of gene in window,bases of gene in last window,");
                out.write("Repeats in gene upstream region (" + upstreamLength + " bp),Repeat types in upstream (seperated by space)," +
                        "Upstream positions overlapped with repeats,Percentage of upstream overlapped with repeats," +
                        "Upstream falls in in window number(s),Bases of upstream in window,Bases of upstream in last window,");
                out.write("Repeats in gene downstream region (" + downstreamLength + " bp),Repeat types in downstream (seperated by space)," +
                        "Downstream positions overlapped with repeats,Percentage of downstream overlapped with repeats,"
                        + "Downtream falls in in window number(s),Bases of downstream in window,Bases of downstream in last window\n");
                // out.write("Repeat frequency of first window,Percentage of window covered by repeats,frequency (second window),coverage (second window)\n");
                int counter = 0;

                for (String seq_id : gene_addresses_per_seq.keySet()) {
                    ArrayList<int[]> gene_addresses = gene_addresses_per_seq.get(seq_id);
                    ArrayList<int[]> repeat_addresses = repeatAddressesPerSequence.get(seq_id);
                    ArrayList<String> repeat_type_list = repeat_types_per_seq.get(seq_id);
                    //int[] repeat_freq_per_window = repeats_per_block.get(seq_id + "#frequency");
                    //int[] repeat_coverage_per_window = repeats_per_block.get(seq_id + "#covered");
                    Node[] gene_nodes = gene_nodes_per_seq.get(seq_id);

                    if (gene_addresses.size() != gene_nodes.length) {
                        throw new RuntimeException("Gene lists are unequal " + gene_addresses.size() + " " + gene_nodes.length);
                    }
                    AtomicInteger last_position = new AtomicInteger(0);
                    counter++;
                    System.out.print("\r Finding overlap for sequence " + counter  + "/" +  gene_addresses_per_seq.size());
                    for (int i = 0; i < gene_addresses.size(); i++) { // go over the gene adresses

                        collect.collectRepeatsInWindow(repeat_addresses, gene_addresses, downstreamLength, upstreamLength,
                        last_position,  repeat_type_list, i);
                        ArrayList<Integer> repeats_in_upstream = collect.getUpstreamCoordinates();
                        ArrayList<Integer> repeats_in_downstream = collect.getDownstreamCoordinates();
                        ArrayList<Integer> repeats_in_gene = collect.getGeneCoordinates();

                        ArrayList<String> repeat_type_in_upstream = collect.getUpstreamTypes();
                        ArrayList<String> repeat_type_in_downstream = collect.getDownstreamTypes();
                        ArrayList<String> repeat_type_in_gene = collect.getGeneTypes();

                        int[] gene_address = gene_addresses.get(i);
                        int gene_end_downstream = gene_address[3] + downstreamLength;
                        int gene_start_upstream = gene_address[2] - upstreamLength;

                        String gene_window_statistics = find_overlap_with_windows(gene_address[2], gene_address[3], windowLength);
                        String upstream_window_statistics = find_overlap_with_windows(gene_start_upstream, gene_address[2]-1, windowLength);
                        String downstream_window_statistics = find_overlap_with_windows(gene_address[3]+1, gene_end_downstream, windowLength);
                        //System.out.println(gene_start_upstream + "-" + gene_address[0] + " " + repeats_in_upstream);

                        // use original for counting the repeats
                        //when a repeat falls in any of the regions, both start and end are added. therefore divided by 2
                        int nr_repeats_upstream = repeats_in_upstream.size() / 2;
                        //System.out.println(repeat_in_upstream);
                        int nr_repeats_downstream = repeats_in_downstream.size() / 2;
                        int nr_repeats_gene = repeats_in_gene.size() / 2;

                        ArrayList<Integer> collapsed_upstream_repeats = determine_sequence_overlap_in_AL(repeats_in_upstream);
                        ArrayList<Integer> collapsed_downstream_repeats = determine_sequence_overlap_in_AL(repeats_in_downstream);
                        ArrayList<Integer> collapsed_gene_repeats = determine_sequence_overlap_in_AL(repeats_in_gene);

                        // remove the letters from function
                        int gene_length = (gene_address[3] - gene_address[2]) + 1;
                        double[] upstream_coverage = calculate_repeat_coverage_of_region(upstreamLength, collapsed_upstream_repeats, "U");
                        double[] downstream_coverage = calculate_repeat_coverage_of_region(downstreamLength, collapsed_downstream_repeats, "D");
                        double[] gene_coverage = calculate_repeat_coverage_of_region(gene_length, collapsed_gene_repeats, "G");
                        // all three arrays have [number of bp overlap, % of gene overlapped]

                        //System.out.println(collapsed_upstream_repeats + " " + upstream_coverage[0]);
                        String types_in_gene = repeat_type_in_gene.toString().replace(","," ").replace("[","").replace("]","");
                        String types_in_downstream = repeat_type_in_downstream.toString().replace(","," ").replace("[","").replace("]","");
                        String types_in_upstream = repeat_type_in_upstream.toString().replace(","," ").replace("[","").replace("]","");
                        String gene_id = (String) gene_nodes[i].getProperty("id");
                        //System.out.println(window_start_nr + " " + window_end_nr);
                        //String window1_covered = get_percentage_str(repeat_coverage_per_window[window_start_nr], windowLength, 2);
                        out.write(gene_id + "," + gene_nodes[i].getId() + "," + seq_id + "," + gene_address[2] + "," + gene_address[3] + "," +
                                gene_length + "," + // gene length
                                nr_repeats_gene + "," + // number of repeats in gene
                                types_in_gene + "," +
                                df0.format(gene_coverage[0]) + "," + // number of bases in gene overlapped by repeats
                                df2.format(gene_coverage[1]) + "," + // Percentage of gene overlapped with repeats
                                gene_window_statistics + // Gene falls in in window number(s), bases of gene in window, bases of gene in second window
                                nr_repeats_upstream + "," + // number of repeats in upstream region
                                types_in_upstream + "," +
                                df0.format(upstream_coverage[0]) + "," + // number of bases in upstream region overlapped by repeats
                                df2.format(upstream_coverage[1]) + "," + // Percentage of upstream region that is overlapped with repeats
                                upstream_window_statistics +
                                nr_repeats_downstream + "," +
                                types_in_downstream + "," +
                                df0.format(downstream_coverage[0]) + "," + // number of bases in downstream region overlapped by repeats
                                df2.format(downstream_coverage[1]) + "," + // Percentage of downstream region that is overlapped with repeats
                                downstream_window_statistics + "\n");
                    }
                }
            } catch (IOException e) {
                 throw new RuntimeException("Unable to write to: " + WORKING_DIRECTORY + "repeats/repeats_in_genes.csv");
            }
            tx.success(); // transaction successful, commit changes
        }
        System.out.println("");
    }

    private class CollectRepeatsInWindow {

        ArrayList<Integer> repeats_in_upstream; //= new ArrayList<>();
        ArrayList<Integer> repeats_in_downstream;// = new ArrayList<>();
        ArrayList<Integer> repeats_in_gene;// = new ArrayList<>();

        ArrayList<String> repeat_type_in_upstream;// = new ArrayList<>();
        ArrayList<String> repeat_type_in_downstream; // = new ArrayList<>();
        ArrayList<String> repeat_type_in_gene;// = new ArrayList<>();

        private void collectRepeatsInWindow(ArrayList<int[]> repeat_addresses, ArrayList<int[]> gene_addresses, int downstreamLength, int upstreamLength,
                                            AtomicInteger last_position, ArrayList<String> repeat_type_list, int i) {

            repeats_in_upstream = new ArrayList<>();
            repeats_in_downstream = new ArrayList<>();
            repeats_in_gene = new ArrayList<>();

            repeat_type_in_upstream = new ArrayList<>();
            repeat_type_in_downstream = new ArrayList<>();
            repeat_type_in_gene = new ArrayList<>();

            int[] gene_address = gene_addresses.get(i);
            int gene_end_downstream = gene_address[3] + downstreamLength;
            int gene_start_upstream = gene_address[2] - upstreamLength;
            if (gene_start_upstream < 1) {
                gene_start_upstream = 1;
            }

            //System.out.println(i + "/" + gene_addresses.size());
            //System.out.println( Arrays.toString(gene_address) + " upstream " + gene_start_upstream + "-" + (gene_address[2]-1) +
            //         " gene " + gene_address[2] + "-" + gene_address[3] + " downstream " + (gene_address[1]+1) + "-" + gene_end_downstream);
            for (int j = last_position.get(); j < repeat_addresses.size(); j++) {
                //System.out.println("pos " + j);
                int[] repeat_address = repeat_addresses.get(j);
                String repeat_type = repeat_type_list.get(j);
                int[] upstream_ovl = check_if_overlap(gene_start_upstream, gene_address[2] - 1, repeat_address[2], repeat_address[3]); // upstream
                int[] gene_ovl = check_if_overlap(gene_address[2], gene_address[3], repeat_address[2], repeat_address[3]); // gene
                int[] downstream_ovl = check_if_overlap(gene_address[3] + 1, gene_end_downstream, repeat_address[2], repeat_address[3]); // downstream
                if (upstream_ovl[0] > 0) {
                    repeats_in_upstream.add(upstream_ovl[0]);
                    repeats_in_upstream.add(upstream_ovl[1]);
                    repeat_type_in_upstream.add(repeat_type);
                }

                if (gene_ovl[0] > 0) {
                    repeats_in_gene.add(gene_ovl[0]);
                    repeats_in_gene.add(gene_ovl[1]);
                    repeat_type_in_gene.add(repeat_type);
                }

                if (downstream_ovl[0] > 0) {
                    repeats_in_downstream.add(downstream_ovl[0]);
                    repeats_in_downstream.add(downstream_ovl[1]);
                    repeat_type_in_downstream.add(repeat_type);
                }

                last_position.set(j);
                if (repeat_address[2] > gene_end_downstream) {
                    if (last_position.get() > 10) {
                        last_position.set(j - 10); // go back 10 position to get repeats that start in earlier gene/upstream but end in the next
                    } else {
                        last_position.set(0);
                    }
                    break;
                }
            }
        }

        private ArrayList<Integer> getUpstreamCoordinates() {
            return repeats_in_upstream;
        }

        private ArrayList<Integer> getDownstreamCoordinates() {
            return repeats_in_upstream;
        }

        private ArrayList<Integer> getGeneCoordinates() {
            return repeats_in_gene;
        }

        private ArrayList<String> getDownstreamTypes() {
            return repeat_type_in_downstream;
        }

        private ArrayList<String> getUpstreamTypes() {
            return repeat_type_in_upstream;
        }

        private ArrayList<String> getGeneTypes() {
            return repeat_type_in_gene;
        }


    }

    /**
     *
     * @param start_coordinate
     * @param end_coordinate
     * @param windowLength
     * @return
     */
    private String find_overlap_with_windows(int start_coordinate, int end_coordinate, int windowLength) {
        double window_start_double = (double) start_coordinate / windowLength;
        int window_start = (int) window_start_double;
        double window_end_double = (double) end_coordinate / windowLength;
        int window_end = (int) window_end_double;
        window_start++;
        window_end++;
        String window_nr_str = window_start + "";
        String gene_bases_in_windows;
        if (window_start != window_end) { // gene is part of two or more windows.
            window_nr_str += " to " + window_end;
            gene_bases_in_windows = ((windowLength -(start_coordinate % windowLength)) +1) + "," + (end_coordinate % windowLength);
        } else {
            //System.out.println(start_coordinate + " met " + end_coordinate + " " + ((end_coordinate - start_coordinate)+1));
            gene_bases_in_windows = ((end_coordinate - start_coordinate)+1) + ",-";
        }
        return window_nr_str + "," + gene_bases_in_windows + ",";
    }

    /**
     *
     * @param region_length
     * @param repeat_coordinates
     * @param type
     * @return [ bases covered, percentage of region_length covered]
     */
    private double[] calculate_repeat_coverage_of_region(int region_length, ArrayList<Integer> repeat_coordinates, String type) {
        int bp_covered = 0;
        for (int j = 0; j < repeat_coordinates.size()-1; j += 2) {
            bp_covered += (repeat_coordinates.get(j+1) - repeat_coordinates.get(j) +1);
        }

        if (bp_covered > region_length) { // overlap cannot be higher than the actual region, bug somewhere
            throw new RuntimeException(type + " bpcovered is to high " + bp_covered + " " +region_length + "\n"
                    + repeat_coordinates.size() + " " + repeat_coordinates);
        }
        double pc_covered = (double) bp_covered / region_length * 100;
        return new double[]{bp_covered, pc_covered};
    }

    /**
     *
     * @param ref_start_position
     * @param ref_end_position
     * @param target_start_position
     * @param target_end_position
     * @return
     */
    private int[] check_if_overlap(int ref_start_position, int ref_end_position, int target_start_position, int target_end_position) {
        int[] overlapped_positions = new int[2];
        String overlap = "nothing";
        if (target_start_position <= ref_start_position && target_end_position >= ref_end_position) { // complete overlap
            overlap = "complete1 " + ref_start_position + " " + ref_end_position;
            overlapped_positions[0] = ref_start_position;
            overlapped_positions[1] = ref_end_position;
        } else if (target_start_position >= ref_start_position && target_end_position <= ref_end_position) { // complete overlap
            overlap = "complete2 " + target_start_position + " " + target_end_position;
            overlapped_positions[0] = target_start_position;
            overlapped_positions[1] = target_end_position;
        } else if (target_start_position >= ref_start_position && target_end_position >= ref_end_position && target_start_position < ref_end_position) { // last part overlaps
            overlap = "last part " + target_start_position + " " + ref_end_position;
            overlapped_positions[0] = target_start_position;
            overlapped_positions[1] = ref_end_position;
        } else if (target_start_position <= ref_start_position &&  target_end_position <= ref_end_position && target_end_position > ref_start_position) { // first part overlaps
            overlap = "first part " + ref_start_position + " " + target_end_position;
            overlapped_positions[0] = ref_start_position;
            overlapped_positions[1] = target_end_position;
        }
        if (!overlap.equals("nothing")) {
            //System.out.println("   " + type + " " + overlap);
        }
        return overlapped_positions;
    }

    public void calcateRepeatDensityCoverageForWindows(int windowLength, ArrayList<String> repeat_types,

                                                       HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence,
                                                       HashMap<String, ArrayList<String>> repeat_types_per_seq,
                                                       String outputDirectory, LinkedHashSet<String> selectedSequences) {

        createDirectory(outputDirectory + "density/", false);
        createDirectory(outputDirectory + "coverage/", false);
        HashMap<String, int[]> repeats_per_block = new HashMap<>();
        StringBuilder header1 = new StringBuilder("Window number,Window start position,Window end position,Repetitive element frequency,"
                + "Bases of window covered,Percentage of window covered,");
        StringBuilder header2 = new StringBuilder("Genome/Sequence,Length,Total number of repeats,Total bases covered,% of genome/sequence covered,");
        for (int i = 0; i < repeat_types.size(); i++) { // place all repeat types in the header
            String type = repeat_types.get(i);
            header1.append(type).append(" frequency,").append(type).append(" bases covered,")
                    .append(type).append(" % of window covered");
            header2.append(type).append(" frequency,").append(type).append(" bases covered,")
                    .append(type).append(" % of sequence covered");
            if (i != repeat_types.size()-1) {
                header1.append(",");
                header2.append(",");
            }
        }
        header1.append("\n");
        header2.append("\n");
        int sequenceCounter = 0;
        HashMap<String, ArrayList<Integer>> genome_overlap_per_type = new HashMap<>();
        HashMap<String, ArrayList<Integer>> genome_freq_per_type = new HashMap<>();
        HashMap<String, StringBuilder> statistics_per_seq = new HashMap<>();
        long genomeLength = 0; // based on the length of sequences in analysis
        double part_of_MB = 1000000 / (double) windowLength;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "repeats_added", true);
            while (sequenceNodes.hasNext()) {
                Node sequenceNode = sequenceNodes.next();
                StringBuilder density_builder = new StringBuilder("position,type,density\n");
                StringBuilder totalDensityBuilder = new StringBuilder("position,type,density\n"); // all counts combined
                StringBuilder coveragePerTypeBuilder = new StringBuilder("position,type,coverage\n"); // all counts combined
                StringBuilder totalCoverageBuilder = new StringBuilder("position,type,coverage\n"); // all counts combined
                int genomeNr = (int) sequenceNode.getProperty("genome");
                int sequenceNr = (int) sequenceNode.getProperty("number");
                if (!selectedSequences.contains(genomeNr + "_" + sequenceNr)) {
                    continue;
                }
                sequenceCounter++;
                System.out.print("\r Counting repeats: Sequence " + sequenceCounter + "/" + repeatAddressesPerSequence.size() + "       "); // spaces are intentional
                String sequenceId = (String) sequenceNode.getProperty("identifier");
                long sequenceLength = (long) sequenceNode.getProperty("length");
                genomeLength += sequenceLength;
                double total_windows_double = (double) sequenceLength / windowLength;
                int total_windows = (int) total_windows_double + 1; // number of windows is rounded down, therefore +1;
                int windowNr = 0;
                int[] frequency_per_window = new int[total_windows];
                int[] seq_covered_in_window = new int[total_windows];
                ArrayList<int[]> repeat_addresses = repeatAddressesPerSequence.get(sequenceId);
                ArrayList<String> repeat_type_list = repeat_types_per_seq.get(sequenceId);
                //System.out.println(repeat_addresses.size() + " " + repeat_type_list.size());
                AtomicInteger last_position = new AtomicInteger(0);
                StringBuilder csv_builder = new StringBuilder(header1.toString());
                HashMap<String, Integer> overlap_per_type = new HashMap<>();
                HashMap<String, Integer> freq_per_type = new HashMap<>();

                while (windowNr < total_windows) {
                    int window_start_position = (windowNr * windowLength) +1;
                    int window_end_position = ((windowNr+1) * windowLength);
                    //System.out.println(window_nr + " " + window_start_position + "-" + window_end_position);
                    windowNr++;

                    HashMap<String, ArrayList<Integer>> repeats_in_window = collectRepeatsForWindow(last_position,
                            window_start_position, window_end_position, repeat_addresses, repeat_type_list);

                    csv_builder.append(windowNr).append(",").append(window_start_position).append(",").append(window_end_position).append(",");
                    //System.out.println("\n\nwindow end #" + window_nr);
                    if (repeats_in_window.containsKey("any")) {
                        ArrayList<Integer> addresses_in_window = repeats_in_window.get("any");
                        int nr_of_repeats = (addresses_in_window.size()/2);
                        ArrayList<Integer> collapsedRepeatAddresses = determine_sequence_overlap_in_AL(addresses_in_window);
                        double[] window_coverage = calculate_repeat_coverage_of_region(windowLength, collapsedRepeatAddresses, "U");
                        //System.out.println(" any " + nr_of_repeats + " " + window_coverage[0] + " " + window_coverage[1]);
                        csv_builder.append(nr_of_repeats).append(",").append(window_coverage[0]).append(",").append(window_coverage[1]).append(",");
                        frequency_per_window[windowNr-1] = nr_of_repeats;
                        seq_covered_in_window[windowNr-1] = (int) window_coverage[0];
                        overlap_per_type.merge("any", (int) window_coverage[0], Integer::sum);
                        freq_per_type.merge("any", nr_of_repeats, Integer::sum);
                        int density_per_MB = (int) (nr_of_repeats * part_of_MB); // round the number
                        totalDensityBuilder.append(window_start_position).append(",repeat,").append(density_per_MB).append("\n");
                        totalCoverageBuilder.append(window_start_position).append(",repeat,").append(window_coverage[1]).append("\n");
                    } else {
                        csv_builder.append(",,,");
                    }

                    for (String repeat_type : repeat_types) {
                        if (repeats_in_window.containsKey(repeat_type)) {
                            ArrayList<Integer> addresses_in_window = repeats_in_window.get(repeat_type);
                            int nr_of_repeats = (addresses_in_window.size()/2);
                            ArrayList<Integer> collapsed_addresses = determine_sequence_overlap_in_AL(addresses_in_window);
                            double[] window_coverage = calculate_repeat_coverage_of_region(windowLength, collapsed_addresses, "U");
                            //System.out.println(" " + repeat_type + " " +  nr_of_repeats + " " + window_coverage[0] + " " + window_coverage[1]);
                            csv_builder.append(nr_of_repeats).append(",").append(window_coverage[0]).append(",").append(window_coverage[1]).append(",");
                            overlap_per_type.merge(repeat_type, (int) window_coverage[0], Integer::sum);
                            freq_per_type.merge(repeat_type, nr_of_repeats, Integer::sum);
                            int density_per_MB = (int) (nr_of_repeats * part_of_MB); // round the number
                            //System.out.println( window_start_position + " " + repeat_type + " " + nr_of_repeats+ " -> " + density_per_MB);
                            density_builder.append(window_start_position).append(",").append(repeat_type).append(",").append(density_per_MB).append("\n");
                            coveragePerTypeBuilder.append(window_start_position).append(",").append(repeat_type).append(",").append((int) window_coverage[1]).append("\n");
                        } else {
                            csv_builder.append(",,,");
                        }
                    }
                    csv_builder.append("\n");
                    repeats_per_block.put(sequenceId + "#frequency", frequency_per_window); // currently not used by any function
                    repeats_per_block.put(sequenceId + "#covered", seq_covered_in_window); // currently not used by any function
                }

                // if the function is run with repeat_overview
                if (outputDirectory.equals(WORKING_DIRECTORY + "/repeats/")) {
                    write_SB_to_file_in_DB(csv_builder, "repeats/windows/" + sequenceId + ".csv");
                }

                if (overlap_per_type.containsKey("any")) {
                    int overlap = overlap_per_type.get("any");
                    int frequency = freq_per_type.get("any");
                    String pc_seq_covered_type = get_percentage_str(overlap, sequenceLength, 2);
                    genome_overlap_per_type.computeIfAbsent(genomeNr + "#any", k -> new ArrayList<>()).add(overlap);
                    genome_freq_per_type.computeIfAbsent(genomeNr + "#any", k -> new ArrayList<>()).add(frequency);
                    statistics_per_seq.computeIfAbsent(sequenceId, k -> new StringBuilder()).append(sequenceLength + "," +frequency + "," + overlap + "," + pc_seq_covered_type + ",");
                    for (String repeat_type : repeat_types) {
                        if (overlap_per_type.containsKey(repeat_type)) {
                            overlap = overlap_per_type.get(repeat_type);
                            frequency = freq_per_type.get(repeat_type);
                            pc_seq_covered_type = get_percentage_str(overlap, sequenceLength, 2);
                            //System.out.println(repeat_type + " " + frequency + " " + overlap + " " + pc_seq_covered_type);
                            statistics_per_seq.computeIfAbsent(sequenceId, k -> new StringBuilder()).append(frequency + "," + overlap + "," + pc_seq_covered_type + ",");
                            genome_overlap_per_type.computeIfAbsent(genomeNr + "#" + repeat_type, k -> new ArrayList<>()).add(overlap);
                            genome_freq_per_type.computeIfAbsent(genomeNr + "#" + repeat_type, k -> new ArrayList<>()).add(frequency);
                        } else {
                            statistics_per_seq.computeIfAbsent(sequenceId, k -> new StringBuilder()).append(",,,");
                        }
                    }
                }
                writeStringToFile(density_builder.toString(), outputDirectory + "density/" + sequenceId + "_types.csv", false, false);
                writeStringToFile(totalDensityBuilder.toString(), outputDirectory + "density/" + sequenceId + ".csv", false, false);
                writeStringToFile(totalCoverageBuilder.toString(), outputDirectory + "coverage/" + sequenceId + ".csv", false, false);
                writeStringToFile(coveragePerTypeBuilder.toString(), outputDirectory + "coverage/" + sequenceId + "_types.csv", false, false);
            }
            tx.success();
        }
        System.out.println("");

        // if the function is run with repeat_overview
        if (outputDirectory.equals(WORKING_DIRECTORY + "/repeats/")) {
            combine_repeat_window_files();
            create_overlap_per_sequence_genome_file(statistics_per_seq, genome_overlap_per_type, genome_freq_per_type,
                    header2, repeat_types, genomeLength);
        }
    }


    private HashMap<String, ArrayList<Integer>> collectRepeatsForWindow(AtomicInteger last_position, int window_start_position,
                                                                        int window_end_position,
                                                                        ArrayList<int[]> repeat_addresses,
                                                                        ArrayList<String> repeat_type_list) {

        HashMap<String, ArrayList<Integer>> repeats_in_window = new HashMap<>();
        for (int j = last_position.get(); j < repeat_addresses.size(); j++) {
            int[] repeat_address = repeat_addresses.get(j);
            String repeat_type = repeat_type_list.get(j);
            //System.out.println("  pos " + j  + " " + repeat_address[0] + "-" + repeat_address[1]);
            int[] window_ovl = check_if_overlap(window_start_position, window_end_position, repeat_address[2], repeat_address[3]);
            if (window_ovl[0] > 0) { // repeat overlaps with the window
                repeats_in_window.computeIfAbsent(repeat_type, k -> new ArrayList<>()).add(window_ovl[0]);
                repeats_in_window.computeIfAbsent(repeat_type, k -> new ArrayList<>()).add(window_ovl[1]);
                repeats_in_window.computeIfAbsent("any", k -> new ArrayList<>()).add(window_ovl[0]);
                repeats_in_window.computeIfAbsent("any", k -> new ArrayList<>()).add(window_ovl[1]);
            }
            last_position.set(j);
            if (repeat_address[2] > window_end_position) {
                if (last_position.get() > 10) {
                    last_position.set(j - 10); // go back 10 position to get repeats that start in earlier window but end in the next
                } else {
                    last_position.set(0);
                }
                break;
            }
        }
        return repeats_in_window;
    }

    /**
     *
     * @param statistics_per_seq
     * @param genome_overlap_per_type
     * @param genome_freq_per_type
     * @param header2
     * @param repeat_types
     * @param genome_length
     */
    private void create_overlap_per_sequence_genome_file(HashMap<String, StringBuilder> statistics_per_seq,
                                                               HashMap<String, ArrayList<Integer>> genome_overlap_per_type,
                                                               HashMap<String, ArrayList<Integer>> genome_freq_per_type,
                                                               StringBuilder header2, ArrayList<String> repeat_types,
                                                               long genome_length) {

        String per_seq_genome_file = WORKING_DIRECTORY + "repeats/statistics_genomes_sequences.csv";
        try (BufferedWriter out = new BufferedWriter(new FileWriter(per_seq_genome_file))) {
            out.write(header2.toString());
            for (int genomeNr : selectedGenomes) {
                out.write(genomeNr +"");
                if (genome_overlap_per_type.containsKey(genomeNr + "#any")) {
                    ArrayList<Integer> overlap_per_seq = genome_overlap_per_type.get(genomeNr + "#any");
                    long total_overlap = 0;
                    for (int overlap : overlap_per_seq) {
                        total_overlap += overlap;
                    }
                    ArrayList<Integer> freq_per_seq = genome_freq_per_type.get(genomeNr + "#any");
                    long total_freq = 0;
                    for (int overlap : freq_per_seq) {
                        total_freq += overlap;
                    }
                    String pc_genome_covered = get_percentage_str(total_overlap, genome_length, 2);
                    out.write("," + genome_length + "," +total_freq + "," + total_overlap + "," + pc_genome_covered + ",");

                    for (String repeat_type : repeat_types) {
                        if (genome_overlap_per_type.containsKey(genomeNr + "#" + repeat_type)) {
                            overlap_per_seq = genome_overlap_per_type.get(genomeNr + "#" + repeat_type);
                            total_overlap = 0;
                            for (int overlap : overlap_per_seq) {
                                total_overlap += overlap;
                            }
                            freq_per_seq = genome_freq_per_type.get(genomeNr + "#" + repeat_type);
                            total_freq = 0;
                            for (int overlap : freq_per_seq) {
                                total_freq += overlap;
                            }
                            pc_genome_covered = get_percentage_str(total_overlap, genome_length, 2);
                            out.write( total_freq + "," + total_overlap + "," + pc_genome_covered + ",");
                        } else {
                            out.write(",,,");
                        }
                    }
                }
                out.write("\n");
                for (int j = 1; j <= GENOME_DB.num_sequences[genomeNr]; ++j) {
                    if (!selectedSequences.contains(genomeNr + "_" + j)) {
                        continue;
                    }
                    if (statistics_per_seq.containsKey(genomeNr + "_" + j)) {
                        StringBuilder statistics = statistics_per_seq.get(genomeNr + "_" + j);
                        out.write(genomeNr + "_" + j + ","+ statistics.toString().replaceFirst(".$","") + "\n");
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to: " + per_seq_genome_file);
        }
    }

    /**
     * Combine the created window statistics files into a single file
     */
    private void combine_repeat_window_files() {
        String line;
        boolean first_row = true;
        String combined_file = WORKING_DIRECTORY + "repeats/windows_all_sequences.csv";
        try (BufferedWriter out = new BufferedWriter(new FileWriter(combined_file))) {
            for (String sequenceId : selectedSequences) {
                if (!checkIfFileExists(WORKING_DIRECTORY + "repeats/windows/" + sequenceId + ".csv")) {
                    continue;
                }
                BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "repeats/windows/" + sequenceId + ".csv"));
                while ((line = in.readLine()) != null) {
                    line = line.trim();
                    if (first_row) {
                        first_row = false;
                        out.write("Sequence identifier," + line + "\n");
                    } else {
                        out.write(sequenceId + "," + line + "\n");
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to: " + combined_file + "\n");
        }
    }

    /**
     * Create hashmap where value is a list of repeat start & stop positions
     * @param repeatNodesPerSequence
     * @return
     */
    public HashMap<String, ArrayList<int[]>> getRepeatAddressesPerSequence(HashMap<String, Node[]> repeatNodesPerSequence) {
        HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String sequenceId : repeatNodesPerSequence.keySet()) {
                Node[] repeat_nodes = repeatNodesPerSequence.get(sequenceId);
                for (Node repeat_node : repeat_nodes) {
                    int[] address = (int[]) repeat_node.getProperty("address");
                    ArrayList<int[]> list = repeatAddressesPerSequence.get(sequenceId);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(address);
                    repeatAddressesPerSequence.put(sequenceId, list);
                }
            }
            tx.success();
        }
        return repeatAddressesPerSequence;
    }

    /**
     * Create hashmap where value is a list of repeat types.
     * @param repeatNodesPerSequence key is sequence identifier, value is list of repeat nodes
     * @return
     */
    public HashMap<String, ArrayList<String>> get_repeat_types_per_seq(HashMap<String, Node[]> repeatNodesPerSequence) {
        HashMap<String, ArrayList<String>> repeat_types_per_seq = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String sequenceId : repeatNodesPerSequence.keySet()) {
                Node[] repeat_nodes = repeatNodesPerSequence.get(sequenceId);
                for (Node repeat_node : repeat_nodes) {
                    String type = (String) repeat_node.getProperty("type");
                    repeat_types_per_seq.computeIfAbsent(sequenceId, k -> new ArrayList<>()).add(type);
                }
            }
            tx.success();
        }
        return repeat_types_per_seq;
    }

    /**
     * Retrieve all 'repeat' nodes per sequence. Use the start position of the repeats to order the nodes
     * @return key is sequence id . value is array with 'repeat' nodes ordered by start coordinate
     */
    public HashMap<String, Node[]> getRepeatNodesPerSequenceAscending(boolean check_types, ArrayList<String> selected_repeat_types,
                                                                      LinkedHashSet<Integer> selectedGenomes,
                                                                      LinkedHashSet<String> selectedSequences) {

        HashMap<String, Node[]> repeatNodesPerSequence = new HashMap<>();
        System.out.print("\rFinding correct order of repeats:");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL, "repeats_added", true);
            while (genomeNodes.hasNext()) {
                Node genomeNode = genomeNodes.next();
                int genomeNr = (int) genomeNode.getProperty("number");
                int repeatCounter = 0;
                if (!selectedGenomes.contains(genomeNr)) {
                    continue;
                }
                System.out.print("\rFinding correct order of repeats: Genome " + genomeNr + " ");
                int num_sequences = (int) genomeNode.getProperty("num_sequences");
                TreeSet<Integer> repeat_start_positions = new TreeSet<>(); // automatically ordered, contains starting locations of genes
                HashMap<Integer, ArrayList<Node>> address_with_node = new HashMap<>();
                for (int j = 1; j <= num_sequences; j++) {
                    if (!selectedSequences.contains(genomeNr + "_" + j)) {
                        continue;
                    }
                    ResourceIterator<Node> repeatNodes = GRAPH_DB.findNodes(REPEAT_LABEL, "sequence_identifier",genomeNr + "_" + j);
                    while (repeatNodes.hasNext()) {
                        Node repeatNode = repeatNodes.next();
                        String repeat_type = (String) repeatNode.getProperty("type");
                        if (check_types && !selected_repeat_types.contains(repeat_type)) {
                            continue;
                        }
                        repeatCounter++;
                        if (repeatCounter % 10000 == 0) {
                            System.out.print("\rFinding correct order of repeats: Genome " + genomeNr + " sequence " + j + ". "
                                    + repeatCounter + " repeats         ");
                        }
                        int[] address = (int[]) repeatNode.getProperty("address");
                        repeat_start_positions.add(address[2]);
                        address_with_node.computeIfAbsent(address[2], k -> new ArrayList<>()).add(repeatNode); // used to find repeats with the same start position
                    }
                    put_ordered_repeats_in_map(repeat_start_positions, genomeNr + "_" + j, address_with_node,
                            repeatNodesPerSequence); // order the genes by start position
                    repeat_start_positions = new TreeSet<>(); // reset variables for next sequence
                    address_with_node = new HashMap<>();
                }
            }
            tx.success();
        }
        System.out.println("");
        return repeatNodesPerSequence;
    }

    private void put_ordered_repeats_in_map(TreeSet<Integer> repeat_start_positions, String sequence_id,
                                                  HashMap<Integer, ArrayList<Node>> address_with_node,
                                                  HashMap<String, Node[]> repeatNodesPerSequence) {

        if (repeat_start_positions.isEmpty()) {
            return;
        }
        ArrayList<Node> final_node_list = new ArrayList<>();
        int[] repeat_start_array = new int[repeat_start_positions.size()];
        int repeat_counter = 0;
        for (int gene_start : repeat_start_positions) {
            repeat_start_array[repeat_counter] = gene_start;
            ArrayList<Node> repeat_node_list = address_with_node.get(gene_start);
            if (repeat_node_list.size() > 1) { // multiple genes at the same positon, inlude all mrna identifiers in a hashmap with first gene as key
                //System.out.println(repeat_node_list);
                int highest = 0;
                Node selected_repeat_node = repeat_node_list.get(0);
                for (Node repeat_node : repeat_node_list) {
                    int[] address = (int[]) repeat_node.getProperty("address");
                    //System.out.println(" " + Arrays.toString(address));
                    if (address[3] > highest) {
                        selected_repeat_node = repeat_node;
                    }
                }
                final_node_list.add(selected_repeat_node);
            } else {
                final_node_list.add(repeat_node_list.get(0));
            }
            repeat_counter++;
        }
        //repeat_start_pos_per_seq.put(sequence_id, repeat_start_array);
        Node[] final_node_array = final_node_list.toArray(new Node[final_node_list.size()]); // list_to_array list to array
        repeatNodesPerSequence.put(sequence_id, final_node_array);
    }

    /**
     *
     * @param selected_repeat_types repeat types that are in the current analysis
     */
    private String determine_repeat_rscript_colors(ArrayList<String> selected_repeat_types) {
        if (selected_repeat_types.size() > COLOR_CODES2.length-1) { // last color is unusable because it is white
            throw new RuntimeException("Only able to color 20 different repeat types");
        }
        StringBuilder manual_colors = new StringBuilder("scale_fill_manual(values=c(");
        for (int i = 0; i < selected_repeat_types.size(); i++) { // go over the gene adresses
            manual_colors.append("\"").append(selected_repeat_types.get(i)).append("\" = \"").append(COLOR_CODES2[i]).append("\", ");
        }
        String manual_colors_str = manual_colors.toString().replaceFirst(".$","").replaceFirst(".$","");
        return manual_colors_str + "))";
    }

    /**
     *Expects input file with one line. Either INCLUDE or EXCLUDE
     * INCLUDE = LTR_retrotransposon, LINE_element, Copia_LTR_retrotransposon
     * EXCLUDE = Gypsy_LTR_retrotransposon
     *
     * @param originalRepeatTypes
     * @return a list with repeat types included in the current analysis
     */
   public ArrayList<String> updateRepeatTypesForAnalysis(HashSet<String> originalRepeatTypes, Path file) {
        ArrayList<String> selectedRepeatTypes = new ArrayList<>();
        ArrayList<String> initialSelection = new ArrayList<>();
        if (file == null) {
            selectedRepeatTypes.addAll(originalRepeatTypes);
            Pantools.logger.info("No --exclude-repeats file was included. All " + selectedRepeatTypes.size() + " repeat types in selection.");
            return selectedRepeatTypes;
        }

        // read the input file and extract the repeat types
        boolean include = false, exclude = false;
        try (BufferedReader in = Files.newBufferedReader(file)) {
           for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                String[] line_array = line.split("=");
                String[] line_array2 = line_array[1].replace(" ","").split(",");
                if (line.startsWith("INCLUDE =") && !line.equals("INCLUDE =")) {
                    initialSelection.addAll(Arrays.asList(line_array2));
                    include = true;
                } else if (line.startsWith("EXCLUDE =") && !line.equals("EXCLUDE =")) {
                    initialSelection.addAll(Arrays.asList(line_array2));
                    exclude = true;
                } else {
                    throw new RuntimeException("A line should start with INCLUDE or EXCLUSIVE in the --exclude-repeats file");
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Something went wrong while reading: " + file);
        }

        if (include && exclude) {
            throw new RuntimeException("INCLUDE and EXCLUSIVE mutually exclusive in the --exclude-repeats file");
        }

        // determine which types to include based on INCLUDE or EXCLUDE booleans
        for (String repeatType : originalRepeatTypes) {
           if (include && initialSelection.contains(repeatType)) {
               selectedRepeatTypes.add(repeatType);
           } else if (exclude && !initialSelection.contains(repeatType)) {
               selectedRepeatTypes.add(repeatType);
           }
        }
        Pantools.logger.info(selectedRepeatTypes.size() + " repeat types in selection: "
               + selectedRepeatTypes.toString().replace("[","").replace("]",""));
        return selectedRepeatTypes;
    }

    /**
     *
     * @return repeat type in the pangenome (selection)
     *
     */
    public HashSet<String> retrieveRepeatTypes(LinkedHashSet<String> selectedSequences) {
        HashSet<String> repeatTypes = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> repeatNodes = GRAPH_DB.findNodes(REPEAT_LABEL);
            while (repeatNodes.hasNext()) {
                Node repeatNode = repeatNodes.next();
                int[] address = (int[]) repeatNode.getProperty("address");
                if (!selectedSequences.contains(address[0] + "_" + address[1])){
                    continue;
                }
                String type = (String) repeatNode.getProperty("type");
                repeatTypes.add(type);
            }
            tx.success();
        }
        return repeatTypes;
    }

    /**
     * Gather all original FASTA headers of a genome place them together with its sequence identifier in a hashmap
     * @param genomeNr a genome number
     * @return key is the FASTA header, value is the sequence ID (=genome number + "_" + sequence number)
     */
    private HashMap<String, String> gatherSequenceNodesForGenome(int genomeNr) {
        long counter = 0;
        HashMap<String, String> sequenceNodeMap = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genomeNr);
            while (sequenceNodes.hasNext()) {
                Node sequenceNode = sequenceNodes.next();
                String title = (String) sequenceNode.getProperty("title");
                String[] titleArray = title.split(" ");
                int sequenceNr = (int) sequenceNode.getProperty("number");
                sequenceNodeMap.put(title, genomeNr + "_" + sequenceNr);
                sequenceNodeMap.put(titleArray[0], genomeNr + "_" + sequenceNr);
                counter++;
                //TODO replace counter with progress bar
                if (counter % 1000 == 0 || counter == 1) {
                    System.out.print("\rGathering sequence nodes: Genome " + genomeNr + ", sequence " + counter);
                }
            }
            tx.success();
        }
        System.out.println("");
        return sequenceNodeMap;
    }

    /**
     * Obtain the size of all sequences of a genome and place in a hashmap
     * @param genomeNr a genome number
     * @return key is sequence ID (=genome number + "_" + sequence number), value is sequence length
     */
    private HashMap<String, Long> createSequenceLengthMap(int genomeNr) {
        long counter = 0;
        HashMap<String, Long> sequenceLengthMap = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genomeNr);
            while (sequenceNodes.hasNext()) {
                Node sequenceNode = sequenceNodes.next();
                long sequenceLength = (long) sequenceNode.getProperty("length");
                int sequenceNr = (int) sequenceNode.getProperty("number");
                sequenceLengthMap.put(genomeNr + "_" + sequenceNr, sequenceLength);
                counter++;
                //TODO replace counter with progress bar
                if (counter % 1000 == 0 || counter == 1) {
                    System.out.print("\rGathering sequence nodes: Genome " + genomeNr + ", sequence " + counter);
                }
            }
            tx.success();
        }
        return sequenceLengthMap;
    }

    private int[] getPreviousRepeatAnnoVersion() {
        HashMap<String, HashSet<String>> idsPerGenome = new HashMap<>(); // key is a genome number string, value is annotation number
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> repeatNodes = GRAPH_DB.findNodes(REPEAT_LABEL);
            while (repeatNodes.hasNext()) {
                Node repeatNode = repeatNodes.next();
                String annotationId = (String) repeatNode.getProperty("annotation_id");
                String[] annotationIdArray = annotationId.split("_");
                idsPerGenome.computeIfAbsent(annotationIdArray[0], k -> new HashSet<>()).add(annotationIdArray[1]);
            }
            tx.success();
        }
        int[] highestAnnotations = new int[total_genomes];
        for (int i = 0; i < total_genomes; i++) {
            highestAnnotations[i] = 0;
        }
        for (String genome_nr_str : idsPerGenome.keySet()) {
            int genomeNr = Integer.parseInt(genome_nr_str);
            HashSet<String> annotation_ids = idsPerGenome.get(genome_nr_str);
            for (String annotation_id_str : annotation_ids) {
                int annotation_id = Integer.parseInt(annotation_id_str);
                if (annotation_id > highestAnnotations[genomeNr-1]) {
                    highestAnnotations[genomeNr-1] = annotation_id;
                }
            }
        }
        return highestAnnotations;
    }

    /**
     * Read a gff and add every feature as repeat node
     * TODO verify repeat types. if a regular annotation is included (mRNA, CDS, etc.) it should not work
     * TODO replace with actual counter
     *
     * @param gffFile expects GFF. Format consists of one line per feature, each containing 9 tab-separated columns
     * @param sequenceNodeMap key is the FASTA header, value is the sequence ID (=genome number + "_" + sequence number)
     * @param sequenceLengthMap key is sequence ID (=genome number + "_" + sequence number), value is sequence length
     * @param genomeNr a genome number
     * @param previousRepeatAnnoVersion the highest repeat annotation ID of earlier analysis
     * @param lineCounter equals the file number. Error message depends on this if a problem occurs
     * @param strict if true, stop if coordinates don't match to a sequence
     */
    private void readRepeatGff(Path gffFile, HashMap<String, String> sequenceNodeMap, HashMap<String, Long> sequenceLengthMap,
                               int genomeNr, int previousRepeatAnnoVersion, int lineCounter, boolean strict, boolean connectAnnotations) {

        String repeatAnnoVersion = genomeNr + "_" + (previousRepeatAnnoVersion + 1);
        Pantools.logger.debug("Genome " + genomeNr);
        Pantools.logger.debug("Annotation identifier " + repeatAnnoVersion);

        HashSet<String> missingSequenceNames = new HashSet<>(); // store all sequence Ids GFF that are missing
        HashSet<String> sequencesWithRepeatsAdded = new HashSet<>();
        long outOfRangeCounter = 0;
        int repeatCounter = 0;

        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            BufferedReader br = Files.newBufferedReader(gffFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.startsWith("#")) {
                    continue;
                }
                String[] line_array = line.split("\\t"); //split on tab
                if (line_array.length != 9) {
                    throw new RuntimeException("The provided GFF file split in " + line_array.length + " columns whereas it should split to 9 columns.");
                }
                String sequence_name = line_array[0];
                String sequenceId = sequenceNodeMap.get(sequence_name);
                if (sequenceId == null) { // gff chromosome identifier does not match to any FASTA header in the pangenome
                    missingSequenceNames.add(sequence_name);
                    continue;
                }
                long highestAllowedCoordinate = sequenceLengthMap.get(sequenceId);
                sequencesWithRepeatsAdded.add(sequenceId);
                String[] seqIdArray = sequenceId.split("_");
                int sequenceNr = Integer.parseInt(seqIdArray[1]);
                int startPosition = Integer.parseInt(line_array[3]);
                int endPosition = Integer.parseInt(line_array[4]);
                if (startPosition > highestAllowedCoordinate || endPosition > highestAllowedCoordinate) {
                    if (!strict) {
                        outOfRangeCounter++;
                        continue;
                    }
                    Pantools.logger.error("Unable to add the following line! Highest genomic coordinate for "
                            + sequence_name + " is allowed to be " + highestAllowedCoordinate +
                            ". Line: " + line);
                    undoFailedRepeatAnnotation(repeatAnnoVersion);
                    Pantools.logger.info("Rolled back the repeat annotation from this GFF file.");
                    if (lineCounter == 1) {
                        Pantools.logger.info("No changes were made to the pangenome.");
                    } else {
                        Pantools.logger.info("Annotations from the first " + (lineCounter - 1) +
                                " GFF files were still successfully added to the pangenome.");
                    }
                    throw new RuntimeException("Repeat coordinates do not match to the database");
                }
                int[] address = new int[]{genomeNr, sequenceNr, startPosition, endPosition};
                Node repeatNode = GRAPH_DB.createNode(REPEAT_LABEL);
                repeatNode.setProperty("sequence_identifier", sequenceId); // example 1_1, 1_2, 1_3
                repeatNode.setProperty("source", line_array[1]); // EDTA, repeatmasker etc.
                repeatNode.setProperty("type", line_array[2]); // LINE_element, SINE_element, helitron etc
                //TODO verify repeat types. if a regular annotation is included (mRNA, CDS, etc.) it should not work
                repeatNode.setProperty("address", address);
                repeatNode.setProperty("length", address[3] - address[2] + 1);
                repeatNode.setProperty("genome", genomeNr);
                repeatNode.setProperty("sequence_number", sequenceNr);
                repeatNode.setProperty("sequence_identifier", genomeNr + "_" + sequenceNr);
                repeatNode.setProperty("annotation_id", repeatAnnoVersion);
                repeatCounter++;
                if (repeatCounter % 100000 == 0) { // commit changes every 100k repeat nodes
                    tx.success();
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
                if (repeatCounter % 10000 == 0 || (connectAnnotations && repeatCounter % 1000 == 0)) {
                    //TODO replace with actual counter
                    System.out.print("\r" + genomeNr + "\t" + repeatCounter);
                }
                if (connectAnnotations) {
                    IndexPointer start_ptr = locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[2] - 1);
                    int offset = start_ptr.offset;
                    Node nucleotideNode = GRAPH_DB.getNodeById(start_ptr.node_id);
                    Relationship rel = repeatNode.createRelationshipTo(nucleotideNode, RelTypes.starts);
                    rel.setProperty("offset", offset);
                    rel.setProperty("genomic_position", address[2]);
                    rel.setProperty("forward", start_ptr.canonical);
                    IndexPointer stop_ptr = locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[3] - 1);
                    rel = repeatNode.createRelationshipTo(GRAPH_DB.getNodeById(stop_ptr.node_id), RelTypes.stops);
                    rel.setProperty("offset", stop_ptr.offset);
                    rel.setProperty("genomic_position", address[3]);
                    rel.setProperty("forward", stop_ptr.canonical);
                }
            }
            tx.success();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            tx.close();
        }
        if (outOfRangeCounter > 0) {
            Pantools.logger.warn(outOfRangeCounter + " repeat coordinates were out of range and were not added.");
        }

        for (String sequence_name : missingSequenceNames) {
            Pantools.logger.warn("Sequence ID = " + sequence_name + " not found.");
        }
        Pantools.logger.info("Genome " + genomeNr + ". Added repeats: " + repeatCounter);
        addPropertyToGenomeNode(genomeNr);
        addPropertyToSequencesNodesWithRepeats(genomeNr, sequencesWithRepeatsAdded);
    }

    private void addPropertyToGenomeNode(int genomeNr) {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
            if (genomeNode.hasProperty("repeats_added")) {
                genomeNode.removeProperty("repeats_added");
            }
            genomeNode.setProperty("repeats_added", true);
            tx.success();
        }
    }

    /**
     * Set a property to 'sequence' nodes whether they have repeats
     * @param genomeNr a genome number
     * @param sequencesWithRepeatsAdded set with sequence identifiers. Example 1_1, 1_2, 2_1
     */
    private void addPropertyToSequencesNodesWithRepeats(int genomeNr, HashSet<String> sequencesWithRepeatsAdded) {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL, "genome", genomeNr);
            while (sequence_nodes.hasNext()) {
                Node sequenceNode = sequence_nodes.next();
                String identifier = (String) sequenceNode.getProperty("identifier");
                boolean added = false;
                if (sequencesWithRepeatsAdded.contains(identifier)) {
                    added  = true;
                    sequencesWithRepeatsAdded.remove(identifier); // remove so the next search is faster
                }
                if (sequenceNode.hasProperty("repeats_added")) {
                    boolean previous_added_bool = (boolean) sequenceNode.getProperty("repeats_added");
                    if (previous_added_bool != added) {
                        sequenceNode.removeProperty("repeats_added");
                        sequenceNode.setProperty("repeats_added", added);
                    }
                } else {
                    sequenceNode.setProperty("repeats_added", added);
                }
            }
            tx.success();
        }
    }

    /**
     * Use in case something went wrong during the annotation and you want to rollback the latest annotation
     * @param repeatAnnoVersion the version ID of repeats that should be removed
     */
    private void undoFailedRepeatAnnotation(String repeatAnnoVersion) {
        int trsc = 0;
        long counter = 0;
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            ResourceIterator<Node> repeatNodes = GRAPH_DB.findNodes(REPEAT_LABEL, "annotation_id", repeatAnnoVersion);
            while (repeatNodes.hasNext()) {
                counter++;
                Node repeatNode = repeatNodes.next();
                Iterable<Relationship> relations = repeatNode.getRelationships();
                for (Relationship rel : relations) {
                    rel.delete();
                    trsc++;
                    if (trsc >= 100000) { // 100k actions
                        tx.success();
                        tx.close();
                        trsc = 0;
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                    }
                }
                trsc++;
                if (trsc >= 100000) { // 100k actions
                    tx.success();
                    tx.close();
                    trsc = 0;
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }
                //TODO replace with progress bar
                if (counter % 5000 == 0) {
                    System.out.print("\rRolling back annotated repeats: " + counter);
                }
            }
        } finally {
            tx.close();
        }
    }

    /**
     *
     * @param log repeat annotation log file
     */
    public void writeAddRepeatsLog(StringBuilder log) {
        log.append("\n\n");
        append_SB_to_file_in_DB(log, "log/repeat_annotation.log");
        Pantools.logger.info("Log file written to:");
        Pantools.logger.info(" {}log/repeat_annotation.log", WORKING_DIRECTORY);
    }
}
