package nl.wur.bif.pantools.construction.build_pangenome;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.MinOrZero;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Build a pangenome out of a set of genomes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "build_pangenome", sortOptions = false)
public class BuildPangenomeCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @Mixin private ThreadNumber threadNumber;

    @ParentCommand
    private Pantools pantools;

    @Parameters(descriptionKey = "genomes-file", index = "0+")
    @InputFile(message = "{file.genomes}")
    Path genomesFile;

    @Option(names = "--kmer-size")
    @MinOrZero(value = 6, message = "{min.ksize}")
    @Max(value = 255, message = "{max.ksize}")
    int kSize;

    @Option(names = "--scratch-directory")
    Path scratchDirectory;

    @Option(names = "--num-buckets")
    @Min(value = 1, message = "{min.num-buckets}")
    int numBuckets;

    @Option(names = "--transaction-size")
    @Min(value = 10, message = "{min.transaction-size}")
    int transactionSize;

    @Option(names = "--num-db-writer-threads")
    @Min(value = 1, message = "{min.num-db-writer-threads}")
    int numDbWriterThreads;

    @Option(names = "--cache-size")
    @Min(value = 0, message = "{min.cache-size}")
    int cacheSize;

    @Option(names = "--keep-intermediate-files")
    boolean keepIntermediateFiles;

    @Override
    public Integer call() throws IOException {
        pantools.createDatabaseDirectory();
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber);

        setGlobalParameters(); //TODO: use local parameters instead

        seqLayer.initialize_pangenome(
            pantools.getDatabaseDirectory(),
            scratchDirectory,
            numBuckets,
            transactionSize,
            numDbWriterThreads,
            cacheSize,
            keepIntermediateFiles
        );
        return 0;
    }

    private void setGlobalParameters() {
        PATH_TO_THE_GENOMES_FILE = genomesFile.toString();
        K_SIZE = (kSize == 0) ? -1 : kSize;
        THREADS = threadNumber.getnThreads();
    }

    public Pantools getPantools() {return pantools;}

}
