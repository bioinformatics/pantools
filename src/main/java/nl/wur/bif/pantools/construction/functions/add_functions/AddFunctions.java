package nl.wur.bif.pantools.construction.functions.add_functions;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.analysis.function_analysis.FunctionalAnalysis;
import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static nl.wur.bif.pantools.analysis.classification.Classification.create_mrna_node_map_for_genome;
import static nl.wur.bif.pantools.analysis.classification.Classification.get_annotation_identifiers;
import static nl.wur.bif.pantools.utils.FileUtils.decompressGzipFile;
import static nl.wur.bif.pantools.utils.FileUtils.unarchiveTarball;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class AddFunctions {
    
    public AddFunctions(Path functionalDatabasesPath, Path functionsFile, Path annotationsFile, String function) {
        add_functional_annotations(functionalDatabasesPath, functionsFile, annotationsFile, function);
    }

    /**
     * Main method for adding functional gene annotations to the graph database.
     * @param functionalDatabasesPath Path to the folder where the function databases will be stored.
     * @param functionsFile Path to the file containing the functional annotations.
     * @param annotationsFile Path to the file containing annotations.
     * @param function String containing a function to be added.
     */
    private void add_functional_annotations(Path functionalDatabasesPath, Path functionsFile, Path annotationsFile, String function) {
        Pantools.logger.info("Including functional annotations in the graph database.");
        check_database(); // starts up the graph database if needed

        if (functionalDatabasesPath == null) {
            functionalDatabasesPath = Paths.get(WORKING_DIRECTORY).resolve("functional_databases");
            Pantools.logger.warn("No path to the functional databases was provided. Using the default path: {}. This may result in unnecessary downloads.", functionalDatabasesPath);
        }

        ArrayList<String> selectedLabels = readLabelsforAddFunctions(function);
        try (Transaction tx = GRAPH_DB.beginTx()) {
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            annotation_identifiers = get_annotation_identifiers(false, false, annotationsFile); // do not print, USE the input via -af
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        HashMap<String, Node> annotation_node_map;
        String date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date() );
        StringBuilder log_builder = new StringBuilder("## LOG of run on " + date  + "\n"); // TODO: replace by logger
        try (Transaction tx = GRAPH_DB.beginTx()) {
            int total_go = (int) count_nodes(GO_LABEL); // count nodes with a specific label
            if (total_go == 0) {
                checkIfDatabasesInputExists(functionalDatabasesPath);
                includeFunctionalDatabases(functionalDatabasesPath);
            }
            annotation_node_map = FunctionalAnalysis.find_all_F_annotation_nodes();
            tx.success();
        }

        Pantools.logger.info("Adding functional annotations to the graph database.");
        create_directory_in_DB("function");
        TreeSet<Integer> genomes_with_missing_mrna = new TreeSet<>();
        TreeSet<Integer> genomes_with_added_functions = new TreeSet<>(); // genome were at least 1 annotation got added
        try (BufferedReader in = new BufferedReader(new FileReader(functionsFile.toFile()))) { // read the GFF file
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                String[] line_array = line.split(" ");
                HashMap<String, HashSet<String>> not_found_ids_mrnas = new HashMap<>(); // key is 'function_id' or 'protein_id'
                HashMap<String, HashSet<String>> all_added_ids = new HashMap<>(); // distinct set of go, pfam, interpro and tigrfam identifiers
                HashMap<String, int[]> phobius_node_map = new HashMap<>();
                HashMap<String, HashMap<String, Integer[]>> pfam_node_map = new HashMap<>();
                HashMap<String, HashMap<String, Integer[]>> tigrfam_node_map = new HashMap<>();
                HashMap<String, HashMap<String, Integer[]>> goterm_node_map = new HashMap<>();
                HashMap<String, HashMap<String, Integer[]>> interpro_node_map = new HashMap<>();
                HashMap<String, String> signalp_map = new HashMap<>();
                if (line_array.length != 2 && line_array.length != 3) {
                    throw new RuntimeException("Error in the functions file. Line " + c + " does not have 2 or 3 columns: " + line);
                }
                if (line_array[1].equals(".")) {
                    continue;
                }
                String genome_nr_str = line_array[0];
                String path_to_file = line_array[1];

                String type = checkAnnotationInputName(line_array);
                HashMap<String, Node> mrna_node_map = new HashMap<>();
                try (Transaction tx = GRAPH_DB.beginTx()) { // start transaction
                    int genome_nr = Integer.parseInt(line_array[0]);
                    mrna_node_map = create_mrna_node_map_for_genome(genome_nr, "Adding functional annotations");
                    tx.success();
                }
                if (mrna_node_map.isEmpty()) {
                    Pantools.logger.warn("No gene annotations are present for genome {}", genome_nr_str);
                    continue;
                }
                if (type.equals("interpro")) {
                    log_builder.append("#Genome ").append(line_array[0]).append(" InterProScan file\n");
                    FunctionalAnalysis.read_interpro_gff(goterm_node_map, interpro_node_map, tigrfam_node_map, pfam_node_map, phobius_node_map, signalp_map, path_to_file, all_added_ids); // InterProScan
                } else if (type.equals("custom")) {
                    log_builder.append("#Genome ").append(line_array[0]).append(". Custom input file\n");
                    FunctionalAnalysis.read_custom_annot_input(goterm_node_map, interpro_node_map, tigrfam_node_map, pfam_node_map, all_added_ids, path_to_file, log_builder); // custom input file
                } else if (type.equals("eggnog")) {
                    log_builder.append("#Genome ").append(line_array[0]).append(". eggNOG input file\n");
                    FunctionalAnalysis.read_eggnogmapper_input(path_to_file, mrna_node_map, goterm_node_map, all_added_ids, log_builder, not_found_ids_mrnas, selectedLabels); // eggNOG-mapper
                } else if (type.equals("signalp")) {
                    log_builder.append("#Genome ").append(line_array[0]).append(". SignalP input file\n");
                    FunctionalAnalysis.read_signalp_output(path_to_file, signalp_map);
                } else if (type.contains("phobius")) {
                    log_builder.append("#Genome ").append(line_array[0]).append(". Phobius input file\n");
                    FunctionalAnalysis.read_phobius_output(path_to_file, phobius_node_map);
                } else { // type is "unknown"
                    if (line_array.length == 2) {
                        Pantools.logger.info("{} is not recognized as annotation file.", line_array[1]);
                    } else { // 3 columns
                        Pantools.logger.info("{} annotation type was not recognized.", line_array[2]);
                    }
                    log_builder.append("#Genome ").append(line).append(" was not recognized as annotation file\n");
                    continue;
                }

                // COG information is directly added in read_eggnogmapper_input() function
                FunctionalAnalysis.connect_mrnas_to_function(tigrfam_node_map, TIGRFAM_LABEL, RelTypes.has_tigrfam, mrna_node_map, annotation_node_map,
                        log_builder, "TIGRFAM", genome_nr_str, not_found_ids_mrnas, selectedLabels);
                FunctionalAnalysis.connect_mrnas_to_function(interpro_node_map, INTERPRO_LABEL, RelTypes.has_interpro, mrna_node_map, annotation_node_map,
                        log_builder, "INTERPRO", genome_nr_str, not_found_ids_mrnas, selectedLabels);
                FunctionalAnalysis.connect_mrnas_to_function(pfam_node_map, PFAM_LABEL, RelTypes.has_pfam, mrna_node_map, annotation_node_map,
                        log_builder, "PFAM", genome_nr_str, not_found_ids_mrnas, selectedLabels);
                FunctionalAnalysis.connect_mrnas_to_function(goterm_node_map, GO_LABEL, RelTypes.has_go, mrna_node_map, annotation_node_map,
                        log_builder, "GO", genome_nr_str, not_found_ids_mrnas, selectedLabels);
                if (selectedLabels.contains("PHOBIUS")) {
                    FunctionalAnalysis.add_phobius_information(phobius_node_map, mrna_node_map, not_found_ids_mrnas, log_builder);
                }
                if (selectedLabels.contains("SIGNALP")) {
                    FunctionalAnalysis.add_signalp_information(signalp_map, mrna_node_map, not_found_ids_mrnas, log_builder);
                }

                FunctionalAnalysis.increase_add_fa_log_with_missing_info(not_found_ids_mrnas, log_builder, genome_nr_str);
                if (not_found_ids_mrnas.containsKey("protein_id")) {
                    genomes_with_missing_mrna.add(Integer.parseInt(genome_nr_str));
                }
                if (!all_added_ids.isEmpty()) {
                    genomes_with_added_functions.add(Integer.parseInt(genome_nr_str));
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to process: {}", functionsFile);
        }

        add_fa_present_property_to_genome_nodes(genomes_with_added_functions);
        FunctionalAnalysis.update_all_FA_nodes_frequency();
        if (Mode.equals("SHARED-FUNCTIONS")) {
            FunctionalAnalysis.find_shared_functions_between_mrnas(); //TODO the code never reaches this point
        }
        if (!genomes_with_missing_mrna.isEmpty()) {
            String genomes_str = genomes_with_missing_mrna.toString().replace("[","").replace("]","");
            Pantools.logger.warn("Not all genes in the annotation file can be found in genome(s): {}; Please check the log file.", genomes_str);
        }

        Path logFile = Paths.get(WORKING_DIRECTORY).resolve("log").resolve("add_functional_annotations.log");
        String header = "";
        if (!logFile.toFile().exists()) {
            header = "#This file is updated after each run\n";
        }
        appendStringToFileFullPath(header + log_builder + "\n", logFile.toFile().getAbsolutePath());
        Pantools.logger.info("Log file written to: {}", logFile);
    }

    /**
     * Selected which annotation type can be included for the current run.
     * Input --label is case insensitive because the they are set to uppercase
     * Allowed labels: go, pfam, tigrfam, interpro, phobius, cog, signalp
     * @return
     */
    private ArrayList<String> readLabelsforAddFunctions(String function) {
        ArrayList<String> selectedLabels = new ArrayList<>();
        if (function != null) {
            function = function.toUpperCase();
            String[] labelArray = function.split(",");
            for (String label : labelArray) {
                switch (label) {
                    case "GO":
                        selectedLabels.add("GO");
                        break;
                    case "PFAM":
                        selectedLabels.add("PFAM");
                        break;
                    case "TIGRFAM":
                        selectedLabels.add("TIGRFAM");
                        break;
                    case "INTERPRO":
                        selectedLabels.add("INTERPRO");
                        break;
                    case "PHOBIUS":
                        selectedLabels.add("PHOBIUS");
                        break;
                    case "COG":
                        selectedLabels.add("COG");
                        break;
                    case "SIGNALP":
                        selectedLabels.add("SIGNALP");
                        break;
                    default:
                        Pantools.logger.error("{} is not a valid (function) node label.", label);
                        System.exit(1);
                }
            }
            String selectedStr = selectedLabels.toString().replace("[","").replace("]","");
            Pantools.logger.info("Only including {} annotations that match: {}", selectedLabels.size(), selectedStr);
        } else { // no argument was included
            selectedLabels.add("GO");
            selectedLabels.add("PFAM");
            selectedLabels.add("TIGRFAM");
            selectedLabels.add("INTERPRO");
            selectedLabels.add("PHOBIUS");
            selectedLabels.add("COG");
            selectedLabels.add("SIGNALP");
        }
        return selectedLabels;
    }

    /**
     * If the line has 2 columns the output is recognized from the filename or extension
     * With 3 columns the annotation type is provided in the 3rd column
     * @param inputLineArray one line from user provided file via --input-file. File is tab-separated with two to three columns
     */
    private String checkAnnotationInputName(String[] inputLineArray) {
        String type = "unknown";
        String fileToCheck;
        if (inputLineArray.length == 2) { // check the type in the file name
            String[] pathArray = inputLineArray[1].split("/");
            fileToCheck = pathArray[pathArray.length-1];
        } else {
            fileToCheck = inputLineArray[inputLineArray.length-1];
        }
        fileToCheck = fileToCheck.toLowerCase();

        if (fileToCheck.endsWith(".gff") || fileToCheck.endsWith(".gff3") || fileToCheck.contains("interpro")) {
            type = "interpro";
        } else if (fileToCheck.endsWith(".annot") || fileToCheck.contains("custom")) {
            type = "custom";
        } else if (fileToCheck.endsWith(".annotations") || fileToCheck.contains("eggnog")) {
            type = "eggnog";
        } else if (fileToCheck.contains("signalp")) {
            type = "signalp";
        } else if (fileToCheck.contains("phobius")) {
            type = "phobius";
        }
        return type;
    }

    /**
     * Interpro file (~200 Mb) must be downloaded by user to avoid a large git repository
     * four databases, stop if none of them exist
     * @param functionalDatabasesPath path to the functional databases
     */

    private void checkIfDatabasesInputExists(Path functionalDatabasesPath) {
        Pantools.logger.debug("Checking if databases exist in {}", functionalDatabasesPath);

        // check if GO database exists (go-basic.obo)
        boolean goDatabaseExists = functionalDatabasesPath.resolve("go-basic.obo").toFile().exists();
        if (!goDatabaseExists) {
            Pantools.logger.info("GO database not found in {}", functionalDatabasesPath);
        }

        // check if Pfam database exists (Pfam-A.clans.tsv, gene_ontology.txt)
        boolean pfamDatabaseExists = functionalDatabasesPath.resolve("Pfam-A.clans.tsv").toFile().exists() &&
                functionalDatabasesPath.resolve("gene_ontology.txt").toFile().exists();
        if (!pfamDatabaseExists) {
            Pantools.logger.info("Pfam database not found in {}", functionalDatabasesPath);
        }

        // check if Interpro database exists (interpro.xml)
        boolean interproDatabaseExists = functionalDatabasesPath.resolve("interpro.xml").toFile().exists();
        if (!interproDatabaseExists) {
            Pantools.logger.info("Interpro database not found in {}", functionalDatabasesPath);
        }

        // check if TIGRFAM database exists (TIGRFAMS_GO_LINK, TIGERFAMS_ROLE_LINK, TIGR_ROLE_NAMES, TIGER000001.INFO, COMBINED_INFO_FILES)
        boolean tigrfamDatabaseExists = functionalDatabasesPath.resolve("TIGRFAMS_GO_LINK").toFile().exists() &&
                functionalDatabasesPath.resolve("TIGRFAMS_ROLE_LINK").toFile().exists() &&
                functionalDatabasesPath.resolve("TIGR_ROLE_NAMES").toFile().exists() &&
                (functionalDatabasesPath.resolve("TIGR00001.INFO").toFile().exists() ||
                        functionalDatabasesPath.resolve("COMBINED_INFO_FILES").toFile().exists());
        if (!tigrfamDatabaseExists) {
            Pantools.logger.info("TIGRFAM database not found in {}", functionalDatabasesPath);
        }

        // download non-existing databases
        downloadDatabases(functionalDatabasesPath, goDatabaseExists, pfamDatabaseExists, interproDatabaseExists, tigrfamDatabaseExists);
    }

    /**
     * Download functional annotation databases from the web
     * @param functionalDatabasesPath directory where databases will be downloaded
     * @param goDatabaseExists whether GO database exists (http://purl.obolibrary.org/obo/go/go-basic.obo)
     * @param pfamDatabaseExists whether Pfam database exists (ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases//Pfam35.0/Pfam-A.clans.tsv.gz
     *                           ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases//Pfam35.0/database_files/gene_ontology.txt.gz)
     * @param interproDatabaseExists whether Interpro database exists (https://ftp.ebi.ac.uk/pub/databases/interpro/current_release/interpro.xml.gz)
     * @param tigrfamDatabaseExists whether TIGRFAM database exists (https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMS_GO_LINK
     *                              https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMS_ROLE_LINK
     *                              https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGR_ROLE_NAMES
     *                              https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMs_15.0_INFO.tar.gz)
     */
    private void downloadDatabases(Path functionalDatabasesPath,
                                   boolean goDatabaseExists,
                                   boolean pfamDatabaseExists,
                                   boolean interproDatabaseExists,
                                   boolean tigrfamDatabaseExists) {
        if (!goDatabaseExists) {
            try {
                downloadDatabase(functionalDatabasesPath, "go-basic.obo", "https://purl.obolibrary.org/obo/go/go-basic.obo");
            } catch (IOException e) {
                Pantools.logger.error("Error while downloading GO database.");
            }
        }
        if (!pfamDatabaseExists) {
            try {
                downloadDatabase(functionalDatabasesPath, "Pfam-A.clans.tsv.gz", "ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases//Pfam35.0/Pfam-A.clans.tsv.gz");
                downloadDatabase(functionalDatabasesPath, "gene_ontology.txt.gz", "ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases//Pfam35.0/database_files/gene_ontology.txt.gz");
            } catch (IOException e) {
                Pantools.logger.error("Unable to download Pfam database.", e);
            }
        }
        if (!interproDatabaseExists) {
            try {
                downloadDatabase(functionalDatabasesPath, "interpro.xml.gz", "https://ftp.ebi.ac.uk/pub/databases/interpro/current_release/interpro.xml.gz");
            } catch (IOException e) {
                Pantools.logger.error("Unable to download Interpro database.", e);
            }
        }
        if (!tigrfamDatabaseExists) {
            try {
                downloadDatabase(functionalDatabasesPath, "TIGRFAMS_GO_LINK", "https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMS_GO_LINK");
                downloadDatabase(functionalDatabasesPath, "TIGRFAMS_ROLE_LINK", "https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMS_ROLE_LINK");
                downloadDatabase(functionalDatabasesPath, "TIGR_ROLE_NAMES", "https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGR_ROLE_NAMES");
                downloadDatabase(functionalDatabasesPath, "TIGRFAMs_15.0_INFO.tar.gz", "https://ftp.ncbi.nlm.nih.gov/hmm/TIGRFAMs/release_15.0/TIGRFAMs_15.0_INFO.tar.gz");
            } catch (IOException e) {
                Pantools.logger.error("Unable to download TIGRFAM database.", e);
            }
        }
    }

    /**
     * Download a database from the web and gunzip it if needed (or untar it if needed)
     * @param functionalDatabasesPath directory where database will be downloaded
     * @param databaseName name of the database
     * @param databaseUrl url of the database
     * @throws IOException if an error occurs while downloading the database
     */
    private void downloadDatabase(Path functionalDatabasesPath, String databaseName, String databaseUrl) throws IOException {
        Pantools.logger.info("Downloading {} database from {}", databaseName, databaseUrl);

        // download database
        URL url = new URL(databaseUrl);
        FileUtils.copyURLToFile(url, functionalDatabasesPath.resolve(databaseName).toFile());

        // check if download was successful
        if (!functionalDatabasesPath.resolve(databaseName).toFile().exists()) {
            throw new RuntimeException("Unable to download " + databaseName + " database.");
        }

        // decompress database
        if (databaseName.endsWith(".tar.gz")) {
            Pantools.logger.debug("Unarchiving database.");
            unarchiveTarball(functionalDatabasesPath.resolve(databaseName), functionalDatabasesPath);
        } else if (databaseName.endsWith(".gz")) {
            Pantools.logger.debug("Decompressing database.");
            decompressGzipFile(functionalDatabasesPath.resolve(databaseName).toFile());
        }
    }

    /** Only needs to be run once per database
     * Requires the following files
     * TIGRFAMS_GO_LINK, TIGRFAMS_ROLE_LINK, TIGR_ROLE_NAMES and the .info file
     * @param functionalDatabasesPath path to the directory that contains the functional databases
     */
    private void includeFunctionalDatabases(Path functionalDatabasesPath) {
        Pantools.logger.info("First time running this function. Including PFAM, InterPro annotations and TIGRFAM and building the GO hierarchy.");
        HashMap<String, Node> go_id_node_id = new HashMap<>();
        int total_pfam_nodes = (int) count_nodes(PFAM_LABEL); // count nodes with a specific label
        if (total_pfam_nodes > 0) {
            throw new RuntimeException("PFAM nodes already exist in the database. Please delete them before running this function.");
        }
        int total_go_nodes = (int) count_nodes(GO_LABEL);
        if (total_go_nodes > 0) {
            throw new RuntimeException("GO nodes already exist in the database. Please delete them before running this function.");
        }
        int total_tigrfam_nodes = (int) count_nodes(TIGRFAM_LABEL);
        if (total_tigrfam_nodes > 0) {
            throw new RuntimeException("TIGRFAM nodes already exist in the database. Please delete them before running this function.");
        }
        int total_ipro_nodes = (int) count_nodes(INTERPRO_LABEL);
        if (total_ipro_nodes > 0) {
            throw new RuntimeException("InterPro nodes already exist in the database. Please delete them before running this function.");
        }
        build_go_hierarchy(go_id_node_id, functionalDatabasesPath);
        include_interpro(go_id_node_id, functionalDatabasesPath);
        include_pfam(go_id_node_id, functionalDatabasesPath);
        include_tigrfam(go_id_node_id, functionalDatabasesPath);
    }

    /**
     *
     * @param go_id_node_id
     */
    private void build_go_hierarchy(HashMap<String, Node> go_id_node_id, Path functionalDatabasesPath) {
        Path goPath = functionalDatabasesPath.resolve("go-basic.obo");
        if (!goPath.toFile().exists()) {
            Pantools.logger.debug("Could not find GO database at {}", goPath.toAbsolutePath());
            return;
        }
        HashMap<String, ArrayList<String>> go_relations = new HashMap<>();
        int go_count = 0;
        try (BufferedReader in2 = new BufferedReader(new FileReader(goPath.toFile()))) {
            Node go_node = GRAPH_DB.getNodeById(0);
            String go_str = "a";
            for (int c = 0; in2.ready();) {
                String line = in2.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.contains("[Typedef]")) {
                    break;
                }
                if (line.startsWith("id: GO")) {
                    line = line.replace("id: ", "");
                    go_node = GRAPH_DB.createNode(GO_LABEL);
                    String go_node_str = go_node.toString();
                    go_id_node_id.put(line, go_node);
                    go_str = go_node_str.replaceAll("[^\\d.]", "");
                    go_node.setProperty("id", line);
                    go_count ++;
                }

                // a GO term can have zero or more alt_ids
                if (line.contains("alt_id:")) {
                    String altId = line.replace("alt_id: ", "");
                    String[] altIds;
                    if (go_node.hasProperty("alt_id")) {
                        String[] oldAltIds = (String[]) go_node.getProperty("alt_id");
                        altIds = new String[oldAltIds.length + 1];
                        System.arraycopy(oldAltIds, 0, altIds, 0, oldAltIds.length);
                        altIds[oldAltIds.length] = altId;
                    } else {
                        altIds = new String[]{altId};
                    }
                    go_node.setProperty("alt_id", altIds);
                    go_id_node_id.put(altId, go_node);
                }

                if (line.startsWith("name:")) {
                    line = line.replace("name: ", "");
                    go_node.setProperty("name", line);
                }

                if (line.startsWith("namespace:")) {
                    line = line.replace("namespace: ", "");
                    go_node.setProperty("category", line);
                }

                if (line.startsWith("def:")) {
                    go_node.setProperty("description", line);
                }

                if (line.contains("!")) {
                    String key, value;
                    String[] line_array = line.split(" ! ");
                    if (line_array[0].startsWith("is_a")) {
                        String[] line_array2 = line_array[0].split(": ");
                        key = go_str + "#" + line_array2[0];
                        value = line_array2[1];
                    } else {
                        String[] line_array2 = line_array[0].split(" ");
                        key = go_str + "#" + line_array2[1];
                        value = line_array2[2];
                    }
                    go_relations.computeIfAbsent(key, s -> new ArrayList<>()).add(value);
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Could not read file " + goPath.toFile().getAbsolutePath(), ioe);
        }

        // loop is used to count the number of relations between GO nodes
        int rel_count = 0;
        for (String key : go_relations.keySet()) {
            String[] key_array = key.split("#");
            ArrayList<String> go_rels_list = go_relations.get(key);
            long node_id = Long.parseLong(key_array[0]);
            Node current_go = GRAPH_DB.getNodeById(node_id);
            String relation_type = key_array[1];
            for (String go_id : go_rels_list) {
                Node current_go_node = go_id_node_id.get(go_id);
                connect_go_nodes(current_go, current_go_node, relation_type);
                rel_count ++;
            }
        }
        Pantools.logger.info("Created the GO hierarchy: {} 'GO' nodes (terms) with {} relations.", go_count, rel_count);
        go_sub();
    }

    /**
     *
     * @param startNode
     * @param endNode
     * @param relationType
     */
    private void connect_go_nodes(Node startNode, Node endNode, String relationType) {
        if (relationType.equals("is_a")) {
            startNode.createRelationshipTo(endNode, RelTypes.is_a);
        } else if (relationType.equals("part_of")) {
            startNode.createRelationshipTo(endNode, RelTypes.part_of);
        } else if (relationType.equals("positively_regulates")) {
            startNode.createRelationshipTo(endNode, RelTypes.positively_regulates);
        } else if (relationType.equals("negatively_regulates")) {
            startNode.createRelationshipTo(endNode, RelTypes.negatively_regulates);
        } else if (relationType.equals("regulates")) {
            startNode.createRelationshipTo(endNode, RelTypes.regulates);
        } else {
            throw new RuntimeException("Did not recognize the relation type: " + relationType);
        }
    }

    /**
     *
     */
    private void go_sub() {
        HashMap<String, String> subclass_id_name_map = new HashMap<>();
        Node biop_node = GRAPH_DB.findNode(GO_LABEL, "name", "biological_process");
        Node molf_node = GRAPH_DB.findNode(GO_LABEL, "name", "molecular_function");
        Node celc_node = GRAPH_DB.findNode(GO_LABEL, "name", "cellular_component");
        StringBuilder output_builder = new StringBuilder();
        get_go_names_layer_below_put_map(biop_node, output_builder, subclass_id_name_map);
        get_go_names_layer_below_put_map(molf_node, output_builder, subclass_id_name_map);
        get_go_names_layer_below_put_map(celc_node, output_builder, subclass_id_name_map);
        biop_node.setProperty("sub category", "");
        molf_node.setProperty("sub category", "");
        celc_node.setProperty("sub category", "");
        ResourceIterator<Node> go_nodes = GRAPH_DB.findNodes(GO_LABEL);
        while (go_nodes.hasNext()) {
            output_builder = new StringBuilder();
            Node go_node = go_nodes.next();
            String go_name = (String) go_node.getProperty("name");
            int counter = 1;
            String all_go = go_node.toString();
            StringBuilder all_go_builder = new StringBuilder();
            if (go_name.equals("biological_process") || go_name.equals("molecular_function") || go_name.equals("cellular_component")) {
                output_builder.append("The selected node is on top of the GO hierarchy\n\n");
            } else {
                HashSet<String> subclasses = new HashSet<>();
                boolean stop = false;
                while (stop == false) {
                    String new_all_go = move_up_go_layer_get_subclass(all_go, counter, all_go_builder, output_builder);
                    if (new_all_go.length() < 2) {
                        stop = true;
                    }
                    String[] go_array = new_all_go.split(",");
                    for (String go_str: go_array) {
                        if (subclass_id_name_map.containsKey(go_str)) {
                            String name = subclass_id_name_map.get(go_str);
                            subclasses.add(name);
                        }
                    }
                    counter ++;
                    all_go = new_all_go;
                }
                String all_go_str = all_go_builder.toString();
                all_go_str = all_go_str.replaceFirst(".$","").replaceFirst(".$","");
                output_builder.append("All GO's found higher in the hierarchy:\n").append(all_go_str).append("\n");
                String subclass_str = "";
                for (String subclass: subclasses) {
                    subclass_str += subclass + " & ";
                }
                subclass_str = subclass_str.replaceFirst(".$","").replaceFirst(".$","").replaceFirst(".$","");
                go_node.setProperty("sub category", subclass_str);
            }
        }
    }

    /**
     *
     * @param target_node
     * @param output_builder
     * @param subclass_id_name_map
     */
    private void get_go_names_layer_below_put_map(Node target_node, StringBuilder output_builder, HashMap<String, String> subclass_id_name_map) {
        output_builder.append("Layer below\n");
        Iterable<Relationship> all_relations = target_node.getRelationships(Direction.INCOMING, RelTypes.is_a);
        for (Relationship rel: all_relations) {
            Node node1 = rel.getStartNode();
            Iterable<Label> labels = node1.getLabels();
            for (Label label1 : labels) {
                if (label1.equals(GO_LABEL)) {
                    Node go_node = rel.getStartNode();
                    String node_str = go_node.toString();
                    String go_id = (String) go_node.getProperty("id");
                    String go_name = (String) go_node.getProperty("name");
                    output_builder.append(rel).append(" ").append(go_id).append(", ").append(go_name).append("\n");
                    subclass_id_name_map.put(node_str, go_name);
                    go_node.setProperty("sub category", "");
                }
            }
        }
        output_builder.append("\n");
    }

    /**
     *
     * @param all_go
     * @param layer
     * @param all_go_builder
     * @param output_builder
     * @return
     */
    private String move_up_go_layer_get_subclass(String all_go, int layer, StringBuilder all_go_builder, StringBuilder output_builder) {
        output_builder.append("Layer ").append(layer).append("\n");
        String[] node_array = all_go.split(",");
        Set<String> numbersSet = new HashSet<>(Arrays.asList(node_array));
        String[] uniqueNumbers = numbersSet.toArray(new String[0]);
        String new_all_go = "";
        int counter = 0;
        int last_node_counter = 0;
        for (String node_str : uniqueNumbers) {
            node_str = node_str.replaceAll("[^\\d.]","");
            long node_id_long = Long.parseLong(node_str);
            Node new_target_node = GRAPH_DB.getNodeById(node_id_long);
            Iterable<Relationship> all_relations2 = new_target_node.getRelationships(Direction.OUTGOING);
            boolean printed = false;
            for (Relationship rel2 : all_relations2) {
                if (!rel2.isType(RelTypes.is_a)) {
                    continue;
                }
                Node parent_node2 = rel2.getEndNode();
                String go_id = (String) parent_node2.getProperty("id");
                String go_name = (String) parent_node2.getProperty("name");
                output_builder.append(" ").append(rel2).append(" ").append(go_id).append(", ").append(go_name).append("\n");
                String temp_str = all_go_builder.toString();
                if (!temp_str.contains(go_id)) {
                    all_go_builder.append(go_id).append(", ");
                }
                new_all_go += parent_node2 + ",";
                printed = true;
                counter ++;
                if (go_name.equals("biological_process") || go_name.equals("molecular_function") || go_name.equals("cellular_component")) {
                    last_node_counter ++;
                }
            }
            if (printed == true) {
                output_builder.append("\n");
            }
        }
        if (counter == last_node_counter) {
            new_all_go = "";
        }
        return new_all_go;
    }

    /**
     *
     * @param go_id_node_id
     */
    private void include_interpro(HashMap<String, Node> go_id_node_id, Path functionalDatabasesPath) {
        Path interproPath = functionalDatabasesPath.resolve("interpro.xml");
        if (!interproPath.toFile().exists()) {
            Pantools.logger.debug("Could not find InterPro database at {}", interproPath.toAbsolutePath());
            return;
        }
        HashMap<Node, ArrayList<String>> interpro_GO = new HashMap<>();
        HashMap<String, String> interpro_id_function_map = new HashMap<>();
        int interpro_counter = 0, go_counter = 0;
        boolean name_line = false, abstract_line = false, skip_line = false;
        String description = "", interpro_term = "", interpro_type = "", interpro_name = "";
        Node interpro_node = GRAPH_DB.getNodeById(0);
        try (BufferedReader in2 = new BufferedReader(new FileReader(interproPath.toFile()))) {
            for (int c = 0; in2.ready();) {
                String line = in2.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.contains("<interpro id=")) {
                    String[] line_array = line.split(" ");
                    interpro_term = line_array[1].replace("id=","").replace("\"","");
                    interpro_node = GRAPH_DB.createNode(INTERPRO_LABEL);
                    interpro_type = line_array[4].replace("type=","").replace("\"","").replace(">","");
                    name_line = true;
                    interpro_node.setProperty("id", interpro_term);
                    interpro_node.setProperty("type", interpro_type);
                    interpro_counter ++;
                    continue;
                }
                if (name_line) {
                    name_line = false;
                    abstract_line = true;
                    interpro_name = line.replace("<name>","").replace("</name>", "");
                    interpro_node.setProperty("name", interpro_name);
                    continue;
                }

                if (line.contains("class_type=\"GO\"")) {
                    String[] line_array = line.split("\"");
                    interpro_GO.computeIfAbsent(interpro_node, n -> new ArrayList<>()).add(line_array[1]);
                    go_counter ++;
                }

                if (abstract_line) {
                    if (line.contains("<pre>")) {
                        skip_line = true;
                    } else if (line.contains("</pre>")) {
                        skip_line = false;
                        continue;
                    }
                    if (skip_line) {
                        continue;
                    }
                    if (line.contains("/abstract")) {
                        abstract_line = false;
                        description = description.replaceFirst(".$","");
                        interpro_id_function_map.put(interpro_term, interpro_type + "SPLIT" + interpro_name + "SPLIT" + description);
                        description = "";
                        continue;
                    }
                    else if (line.contains("<abstract>")) {
                        continue;
                    }

                    boolean print = true;
                    String newline = "";
                    for (int i = 0; i < line.length(); i++) {
                        char temp_char = line.charAt(i);
                        if (temp_char == '<' || temp_char == '[') {
                            print = false;
                        } else if (temp_char == '>' || temp_char == ']') {
                            print = true;
                            continue;
                        }
                        if (print) {
                            newline += line.charAt(i);
                        }
                    }
                    newline = newline.replace(" ()","").replace(" ,",",").replace("  ", " ").replace(",,", ",").replace(",.", ".").replace("  ", " ").replace(" .", ".");
                    description += newline + " ";
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Could not read file " + interproPath.toFile().getAbsolutePath(), ioe);
        }
        Pantools.logger.info("Created {} 'interpro' nodes and connected them to {} 'GO' nodes.", interpro_counter, go_counter);
        for (Node interpro_node1 : interpro_GO.keySet()) {
            ArrayList<String> go_terms = interpro_GO.get(interpro_node1);
            for (String go_term : go_terms) {
                Node go_node = go_id_node_id.get(go_term);
                if (go_node == null){
                    continue; // this can happen when one of the databases is older than the other
                }
                interpro_node1.createRelationshipTo(go_node, RelTypes.is_similar_to); //TODO: USE A DIFFERENT RELATIONSHIP
            }
        }
    }

    /**
     *
     * @param go_id_node_id
     */
    private void include_pfam(HashMap<String, Node> go_id_node_id, Path functionalDatabasesPath) {
        Path pfamPath = functionalDatabasesPath.resolve("Pfam-A.clans.tsv");
        Path gene_ontologyPath = functionalDatabasesPath.resolve("gene_ontology.txt");

        boolean pfamExistsA = pfamPath.toFile().exists();
        boolean pfamExistsB = gene_ontologyPath.toFile().exists();
        if (!pfamExistsA || !pfamExistsB) {
            Pantools.logger.debug("Could not find Pfam databases at {} and {}", pfamPath.toAbsolutePath(), gene_ontologyPath.toAbsolutePath());
            return; // one of the input files does not exist
        }

        HashMap<String, ArrayList<String>> pfam_go_map = new HashMap<>();
        try (BufferedReader in = new BufferedReader(new FileReader(gene_ontologyPath.toFile()))) {
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                String[] line_array = line.split("\t");
                pfam_go_map.computeIfAbsent(line_array[0], s -> new ArrayList<>()).add(line_array[1]);
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read " + gene_ontologyPath.toFile().getAbsolutePath(), ioe);
        }

        int counter = 0, go_counter = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(pfamPath.toFile()))) {
            for (int c = 0; in.ready();) {
                counter ++;
                Node pfam_node = GRAPH_DB.createNode(PFAM_LABEL);
                String line = in.readLine().trim();
                String[] line_array = line.split("\t");
                pfam_node.setProperty("id", line_array[0]);
                pfam_node.setProperty("clan", line_array[1]);
                pfam_node.setProperty("name", line_array[3]);
                pfam_node.setProperty("description", line_array[4]);
                ArrayList<String> go_terms = pfam_go_map.get(line_array[0]);
                if (go_terms == null) {
                    continue;
                }
                for (String go_term : go_terms) {
                    go_counter++;
                    Node go_node = go_id_node_id.get(go_term);
                    if (go_node == null){
                        continue; // this can happen when one of the databases is older than the other
                    }
                    pfam_node.createRelationshipTo(go_node, RelTypes.is_similar_to); //TODO: USE A DIFFERENT RELATIONSHIP
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read " + pfamPath.toFile().getAbsolutePath(), ioe);
        }
        Pantools.logger.info("Created {} 'pfam' nodes and connected them to {} 'GO' nodes.", counter, go_counter);
    }

    /**
     * TIGRFAMs_15.0_INFO.tar.gz TIGRFAMS_GO_LINK TIGRFAMS_ROLE_LINK TIGR_ROLE_NAMES
     * @param go_id_node_id
     */
    private void include_tigrfam(HashMap<String, Node> go_id_node_id, Path functionalDatabasesPath) {
        Path tigrfamGoLink = functionalDatabasesPath.resolve("TIGRFAMS_GO_LINK");
        Path tigrfamRoleLink = functionalDatabasesPath.resolve("TIGRFAMS_ROLE_LINK");
        Path tigrfamRoleNames = functionalDatabasesPath.resolve("TIGR_ROLE_NAMES");
        Path tigrfamInfo = functionalDatabasesPath.resolve("TIGR00001.INFO");
        Path tigrfamCombinedInfoFiles = functionalDatabasesPath.resolve("COMBINED_INFO_FILES");
        boolean tigrfamGoLinkExists = tigrfamGoLink.toFile().exists();
        boolean tigrfamRoleLinkExists = tigrfamRoleLink.toFile().exists();
        boolean tigrfamRoleNamesExists = tigrfamRoleNames.toFile().exists();
        boolean tigrfamInfoExists = tigrfamInfo.toFile().exists();
        boolean tigrfamCombinedInfoFilesExists = tigrfamCombinedInfoFiles.toFile().exists();

        if (!(tigrfamGoLinkExists && tigrfamRoleLinkExists && tigrfamRoleNamesExists && (tigrfamInfoExists || tigrfamCombinedInfoFilesExists))) {
            Pantools.logger.warn("Could not find TIGRFAM databases at {}, {}, {}, {} and {}",
                    tigrfamGoLink.toAbsolutePath(), tigrfamRoleLink.toAbsolutePath(), tigrfamRoleNames.toAbsolutePath(), tigrfamInfo.toAbsolutePath(), tigrfamCombinedInfoFiles.toAbsolutePath());
            return; // one of the input files does not exist
        }

        HashMap<String, String> tigrfam_roleId = new HashMap<>();
        HashMap<String, Node> tigrfam_nodeId = new HashMap<>();
        HashMap<String, String> tigrfam_role_name = new HashMap<>();
        HashMap<String, ArrayList<String>> tigrfam_go = new HashMap<>();
        boolean first_info_file = true;
        String[] infoFiles = functionalDatabasesPath.toFile().list();
        assert infoFiles != null;
        for (String file : infoFiles) {
            if (!file.startsWith("TIGR")) {
                continue;
            }
            if (file.contains("TIGRFAMS_GO_LINK")) { // the go terms have either NULL or contributes_to. for NULL I use is similar_to
                read_tigrfams_go_link(tigrfam_go, functionalDatabasesPath);
            } else if (file.contains("TIGRFAMS_ROLE_LINK")) {
                read_tigrfams_role_link(tigrfam_roleId, functionalDatabasesPath);
            } else if (file.contains("TIGR_ROLE_NAMES")) {
                read_tigrfam_role_names(tigrfam_role_name, functionalDatabasesPath);
            } else if (file.contains(".INFO")) {
                if (first_info_file) { // when .info files are found, a new COMBINED_INFO_FILES is made
                    delete_file_full_path(functionalDatabasesPath.resolve("COMBINED_INFO_FILES").toFile().getAbsolutePath());
                    Pantools.logger.info("New TIGRFAM .INFO files are found. Combining all of them into a single file.");
                    first_info_file = false;
                }
                read_tigrfam_info(file, functionalDatabasesPath);
            }
        }

        int tigrfam_nodes = create_tigrfam_nodes(tigrfam_nodeId, tigrfam_roleId, tigrfam_role_name, functionalDatabasesPath);
        int created_relations = create_tigrfam_relations_to_go(tigrfam_nodeId, tigrfam_go, go_id_node_id);
        Pantools.logger.info("Created {} 'tigrfam' nodes and connected them to {} 'GO' nodes.", tigrfam_nodes, created_relations);
    }

    /**
     * All tigrfam information is read from COMBINED_INFO_FILES
     * @param tigrfam_nodeId
     * @param tigrfam_roleId
     * @param tigrfam_role_name
     * @param tigrfamPath
     * @return
     */
    private int create_tigrfam_nodes(HashMap<String, Node> tigrfam_nodeId, HashMap<String, String> tigrfam_roleId,
                                           HashMap<String, String> tigrfam_role_name, Path tigrfamPath) {
        int total_nodes = 0;
        try (BufferedReader in = new BufferedReader(new FileReader(tigrfamPath.resolve("COMBINED_INFO_FILES").toFile()))) {
            Node tigrfam_node = GRAPH_DB.getNodeById(0);
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    if (tigrfam_node.hasLabel(TIGRFAM_LABEL) || !tigrfam_node.hasProperty("description")) {
                        tigrfam_node.setProperty("description", "");
                    }
                    continue;
                }
                if (line.startsWith("AC")) { // name
                    String id = line.replace("AC  ", "");
                    tigrfam_node = GRAPH_DB.createNode(TIGRFAM_LABEL);
                    tigrfam_node.setProperty("id", id);
                    tigrfam_nodeId.put(id, tigrfam_node);
                    String roleId = tigrfam_roleId.get(id);
                    String role = tigrfam_role_name.get(roleId);
                    if (role != null) {
                        String[] role_array = role.split("#");
                        tigrfam_node.setProperty("main_role", role_array[0]);
                        tigrfam_node.setProperty("sub_role", role_array[1]);
                    }
                    total_nodes ++;
                } else if (line.startsWith("ID")) { // the tigrfam id
                    line = line.replace("ID  ", "");
                    tigrfam_node.setProperty("domain", line);
                } else if (line.startsWith("EN")) { // the function
                    line = line.replace("EN  ", "");
                    tigrfam_node.setProperty("name", line);
                } else if (line.startsWith("CC")) { // the function
                    line = line.replace("CC  ", "");
                    tigrfam_node.setProperty("description", line);
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Could not read " + tigrfamPath.resolve("COMBINED_INFO_FILES").toFile() +
                    "Did you replace the tigrfam files? If so, check the manual for the correct way to do this.", ioe);
        }
        return total_nodes;
    }

    /**
     *
     * @param tigrfam_nodeId
     * @param tigrfam_go
     * @param go_id_node_id
     * @return
     */
    private int create_tigrfam_relations_to_go(HashMap<String, Node> tigrfam_nodeId,
                                              HashMap<String, ArrayList<String>> tigrfam_go, HashMap<String, Node> go_id_node_id) {
        int go_connect_count = 0;
        for (String key : tigrfam_nodeId.keySet()) {
            Node tigrNode = tigrfam_nodeId.get(key);
            ArrayList<String> go_terms = tigrfam_go.get(key);
            if (go_terms == null) {
                continue;
            }
            for (String go_str : go_terms) {
                go_connect_count ++;
                String[] go_array = go_str.split("#");
                Node go_node = go_id_node_id.get(go_array[0]);
                if (go_array[1].contains("similar_to")) {
                    tigrNode.createRelationshipTo(go_node, RelTypes.is_similar_to); //TODO: USE A DIFFERENT RELATIONSHIP
                } else if (go_array[1].contains("contributes")) {
                    tigrNode.createRelationshipTo(go_node, RelTypes.contributes_to);
                } else {
                    Pantools.logger.error("Make function for {}", go_array[1]);
                    System.exit(1);
                }
            }
        }
        return go_connect_count;
    }

    /**
     *
     * @param tigrfamRoleName
     */
    private void read_tigrfam_role_names(HashMap<String, String> tigrfamRoleName, Path tigrfamPath) {
        try (BufferedReader in = new BufferedReader(new FileReader(tigrfamPath.resolve("TIGR_ROLE_NAMES").toFile()))) {
            boolean mainRole = true;
            StringBuilder bothRoles = new StringBuilder();
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                String[] line_array = line.split("\t");
                if (mainRole) {
                    mainRole = false;
                    bothRoles.append(line_array[3]).append("#");
                } else {
                    bothRoles.append(line_array[3]);
                    tigrfamRoleName.put(line_array[1], bothRoles.toString());
                    bothRoles = new StringBuilder();
                    mainRole = true;
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read " + tigrfamPath.resolve("TIGR_ROLE_NAMES").toFile().getAbsolutePath(), ioe);
        }
    }

    /**
     *
     * @param tigrId_GO
     */
    private void read_tigrfams_go_link(HashMap<String, ArrayList<String>> tigrId_GO, Path tigrfamPath) {
        try (BufferedReader in = new BufferedReader(new FileReader(tigrfamPath.resolve("TIGRFAMS_GO_LINK").toFile()))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                String[] line_array = line.split("\t");
                String relation;
                if (line_array[2].equals("NULL")) {
                    relation = "is_similar_to"; //TODO: USE A DIFFERENT RELATIONSHIP
                } else {
                    relation = line_array[2];
                }
                tigrId_GO.computeIfAbsent(line_array[0], s -> new ArrayList<>()).add(line_array[1] + "#" + relation);
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read " + tigrfamPath.resolve("TIGRFAMS_GO_LINK").toFile().getAbsolutePath(), ioe);
        }
    }

    /**
     *
     * @param tigrfam_roleId
     */
    private void read_tigrfams_role_link(HashMap<String, String> tigrfam_roleId, Path tigrfamPath) {
        try (BufferedReader in = new BufferedReader(new FileReader(tigrfamPath.resolve("TIGRFAMS_ROLE_LINK").toFile()))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                String[] line_array = line.split("\t");
                tigrfam_roleId.put(line_array[0], line_array[1]);
            }
            in.close();
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read " + tigrfamPath.resolve("TIGRFAMS_ROLE_LINK").toFile().getAbsolutePath(), ioe);
        }
    }

    /**
     * In case a new tigrfam database is downloaded, combine all the .info files in a single file
     * @param file
     * @param tigrfamPath
     */
    private void read_tigrfam_info(String file, Path tigrfamPath) {
        StringBuilder file_builder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new FileReader(tigrfamPath.resolve(file).toFile()))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                file_builder.append(line).append("\n");
            }
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        file_builder.append("\n");
        append_SB_to_file_full_path(file_builder, tigrfamPath.resolve("COMBINED_INFO_FILES").toFile().getAbsolutePath());
        delete_file_full_path(tigrfamPath.resolve(file).toFile().getAbsolutePath());
    }

    /**
     Set "has_functional_annotations" property to genome nodes when at least 1 function was added to the pangenome
     * @param genomes_with_added_functions
     */
    private void add_fa_present_property_to_genome_nodes(TreeSet<Integer> genomes_with_added_functions) {
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                Node genome_node = genome_nodes.next();
                int genome_nr = (int) genome_node.getProperty("number");
                if (genomes_with_added_functions.contains(genome_nr) && !genome_node.hasProperty("has_functional_annotations")) {
                    genome_node.setProperty("has_functional_annotations", true);
                }
            }
            tx.success();
        }
    }
}
