package nl.wur.bif.pantools.construction.busco;

import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.construction.grouping.Grouping;
import nl.wur.bif.pantools.Pantools;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.Utils.createDirectory;
import static nl.wur.bif.pantools.utils.Utils.*;
import static nl.wur.bif.pantools.utils.Globals.*;

public class Busco {

    // still uses some global variables:
    // 1. longest_transcripts
    // 2. THREADS
    // 3. PROTEOME
    // 4. phasingInfoMap
    // 5. annotation_identifiers -> PATH_TO_THE_ANNOTATIONS_FILE

    private final int buscoVersion;
    private String database; // name of the (odb9 or odb10) database
    private final String buscoLocation; // location to the executable busco file
    private final boolean subgenomeLevel;

    public Busco(int buscoVersion, String odb10, boolean useSubgenomes) {
        this.buscoVersion = buscoVersion;
        this.database = odb10;
        this.subgenomeLevel = useSubgenomes;
        this.buscoLocation = getBuscoLocation();
    }

    /**
     * @param allAutomatic if ODB10 is set to automatic-lineage, all analyses will be on this setting
     */
    public void buscoProtein(String[] questionableBuscos, boolean allAutomatic) {
        Pantools.logger.info("Running BUSCO in protein mode.");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            skip.create_skip_arrays(false, true); // create skip array if --skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            throw new RuntimeException("Unable to start the pangenome database");
        }

        skip.retrieveSelectedGenomes();

        if (!PROTEOME) {
            update_skip_array_based_on_annotation();
            skip.retrieveSelectedSequences(0, false);
        }

        HashMap<String, ArrayList<String>> hmmMap = new HashMap<>();
        HashMap<String, String> buscoScoresMap = new HashMap<>();
        String proteinSet = determineProteinInputSet();
        ArrayList<String> genomeOrSubgenomeIds = prepareProteinsForBusco();
        String buscoBaseDirectory = WD_full_path + "/busco/" + database + "/" + proteinSet + "/";
        deleteDirectory("busco/" + database + "/" + proteinSet + "/BUSCO_summary_files/", true);
        createDirectory("busco/" + database + "/" + proteinSet + "/results/", true);
        createDirectory("busco/" + database + "/" + proteinSet + "/BUSCO_summary_files", true);
        ArrayList<String> selectedDatabases = new ArrayList<>();

        int[][] buscoCounts = new int[6][genomeOrSubgenomeIds.size()]; // [Complete, Single copy, Duplicated, Fragmented, Missing, total]
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (int i = 0; i < genomeOrSubgenomeIds.size(); i++) {
                String genomeOrSubgenome =  genomeOrSubgenomeIds.get(i);
                String selectedDatabase = "";
                String[] genomeOrSubgenomeArray = genomeOrSubgenome.split("_");
                int genomeNr = Integer.parseInt(genomeOrSubgenomeArray[0]);
                selectedDatabase = findBestMatchingDatabase(database, proteinSet, genomeOrSubgenome);

                if (!allAutomatic) {
                   database = selectedDatabase;
                   buscoBaseDirectory = WD_full_path + "/busco/" + database + "/" + proteinSet + "/";// update directory with new dbName property
                }

                boolean exists = checkIfBuscoOutputExists(selectedDatabase, buscoBaseDirectory, genomeOrSubgenome);
                Pantools.logger.info("BUSCO against proteome: " + (i+1) + "/" + genomeOrSubgenomeIds.size());
                HashMap<String, Node> mrnaIdNodeMap = create_mrna_node_map_for_genome(genomeNr, "Retrieving mRNA's");
                if (mrnaIdNodeMap.isEmpty()) { // genome is not annotated
                    continue;
                }
                if (!exists) {
                    runBusco(proteinSet, genomeOrSubgenome);
                    selectedDatabase = findBestMatchingDatabase(database, proteinSet, genomeOrSubgenome);
                    if (!allAutomatic) {
                        database = selectedDatabase;
                    }
                }
                selectedDatabases.add(selectedDatabase);
                int[] counts = readBuscoFullTableTsv(selectedDatabase, hmmMap, genomeOrSubgenome, mrnaIdNodeMap, buscoBaseDirectory);
                updateBuscoCountsArray(genomeOrSubgenome, buscoCounts, genomeOrSubgenomeIds, counts);
            }
            tx.success(); // transaction successful, commit changes
        }
        double[][] buscoScores = calculatePercentagesFromCounts(buscoCounts);
        String outputDirectory = WORKING_DIRECTORY + "busco/" + database + "/" + proteinSet + "/";
        HashMap<String, ArrayList<String>> hmmPerGenome = createHmmOverview(hmmMap, outputDirectory, buscoCounts, genomeOrSubgenomeIds, selectedDatabases);
        createBuscoUpsetInput(hmmMap, genomeOrSubgenomeIds, buscoBaseDirectory);
        createBuscoCsvOverview(questionableBuscos, buscoCounts, hmmPerGenome, outputDirectory, genomeOrSubgenomeIds, selectedDatabases);
        createBuscoScorestxt(outputDirectory, buscoScores, genomeOrSubgenomeIds, buscoScoresMap, buscoCounts, selectedDatabases);

        createBuscoNodeIfMissing();
        updateBuscoNodes("protein", buscoScoresMap);
        createDirectoryWithShortSummaries(outputDirectory, genomeOrSubgenomeIds, selectedDatabases);

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}busco/{}/{}/results", WORKING_DIRECTORY, database, proteinSet);
        Pantools.logger.info(" {}busco/{}/{}/busco_overview.cs", WORKING_DIRECTORY, database, proteinSet);
        Pantools.logger.info(" {}busco/{}/{}/hmm_overview.tx", WORKING_DIRECTORY, database, proteinSet);
        Pantools.logger.info(" {}busco/{}/{}/busco_scores.tx", WORKING_DIRECTORY, database, proteinSet);

        if (subgenomeLevel) {
            Pantools.logger.info("Visualize overlap between subgenomes:");
            Pantools.logger.info(" Rscript {}busco/{}/{}/upset/upset_plot.R", WORKING_DIRECTORY, database, proteinSet);
        }
    }

    /**
     * Convert number of counts for a certain category to a percentage
     * @param buscoCounts [Complete, Single copy, Duplicated, Fragmented, Missing, TOTAL]
     * @return
     */
    private double[][] calculatePercentagesFromCounts(int[][] buscoCounts) {
        double[][] buscoScores = new double[5][buscoCounts[0].length]; // [Complete, Single copy, Duplicated, Fragmented, Missing]
        for (int i = 0; i < buscoCounts[0].length; i++) {
            buscoScores[0][i] = ((double)buscoCounts[0][i] / buscoCounts[5][i]) * 100; // complete (single copy & duplicated)
            buscoScores[1][i] = ((double)buscoCounts[1][i] / buscoCounts[5][i]) * 100; // single-copy
            buscoScores[2][i] = ((double)buscoCounts[2][i] / buscoCounts[5][i]) * 100; // duplicated
            buscoScores[3][i] = ((double)buscoCounts[2][i] / buscoCounts[5][i]) * 100; // fragmented
            buscoScores[4][i] = ((double)buscoCounts[2][i] / buscoCounts[5][i]) * 100; // missing
        }
        return buscoScores;
    }

    /**
     * Set of protein sequences is dependant on --phased and --longest-transcripts arguments
     *
     * 1. No arguments
     * 2. --longest-transcripts
     * 3. --phasing
     * 4. --phasing & --longest-transcripts
     * @return
     */
    private String determineProteinInputSet() {
        String subgenome = "";
        if (subgenomeLevel) {
            subgenome = "_subgenome";
        }

        String proteinSet;
        if (longest_transcripts) {
            proteinSet = "protein" + subgenome + "_longest_transcript";
        } else {
            proteinSet = "protein" + subgenome;
        }
        return proteinSet;
    }

    /**
     * Update 'selectedGenomes' if genomes do not have an 'annotation node'
     * TODO: this should be done in CLI class
     */
    private void update_skip_array_based_on_annotation() {
        int additional_skipped = 0;
        for (int i = 0; i < annotation_identifiers.size(); i++) {
            if (annotation_identifiers.get(i).endsWith("_0") && selectedGenomes.contains((i+1))) {
                // there is no annotation available but the genome wasn't skipped by the user.
                additional_skipped ++;
                selectedGenomes.remove((i+1));
            }
        }

        if (additional_skipped > 0) {
            Pantools.logger.info("{} genomes were automatically excluded because these don't have an annotation.", additional_skipped);
        }
        if (selectedGenomes.size() == 0) {
            throw new RuntimeException("No (selected) genomes available with an annotation. Please run 'add_annotations' first");
        }
    }

    /**
     * Check if BUSCO output already exists.
     * Name of the summary output file is different depending BUSCO version  (3 vs 4 5)
     * @param buscoDatabaseName
     * @param buscoBaseDirectory
     * @param genomeOrSubgenome
     * @return
     */
    private boolean checkIfBuscoOutputExists(String buscoDatabaseName, String buscoBaseDirectory, String genomeOrSubgenome) {
        return  (checkIfFileExists(buscoBaseDirectory + "results/" + genomeOrSubgenome + "/short_summary.specific." +
                buscoDatabaseName + "." + genomeOrSubgenome + ".txt"));
    }

    private String getBuscoLocation() {
        final String buscoPath = find_program_location("busco");
        check_if_program_exists_stdout("busco -h", 100, "BUSCO", true); // check if program is set to $PATH
        return buscoPath;
    }

    /**
     * Copy all the summary files of the current genome selection to a separate directory.
     *
     * From this directory, users can generate a visual output of the analyses

     * python3 scripts/generate_plot.py –wd BUSCO_summaries
     */
    private void createDirectoryWithShortSummaries(String directory, ArrayList<String> identifiersForBusco, ArrayList<String> selectedDatabases) {
         if (buscoVersion != 5) {
             return;
         }

         String location1 = "", location2 = "";
         if (buscoLocation.endsWith("busco")) {
             location1 = buscoLocation.substring(0, buscoLocation.length()-5);
             if (location1.endsWith("bin/")) {
                 location2 = location1.substring(0, location1.length() - 4);
                 location2 += "scripts/generate_plot.py";
             }
             location1 += "generate_plot.py";
         }

         boolean scriptfound1 = checkIfProgramExistsUsingStdout(location1 + " -h", 100, "BUSCO Rscript", false, false);
         boolean scriptfound2 = checkIfProgramExistsUsingStdout(location2 + " -h", 100, "BUSCO Rscript", false, false);
         String location = "Script location is dependant on how BUSCO was installed. However, its location was not detected by Pantools.\n " +
                 "python3 busco_directory/scripts/generate_plot.py –wd " + directory + "BUSCO_summary_files/";
         if (scriptfound1) {
             location = " python3 " + location1 + " -wd " + directory + "BUSCO_summary_files/\n";
         } else if (scriptfound2) {
             location = " python3 " + location2 + " -wd " + directory + "BUSCO_summary_files/\n";
         }

         for (int i= 0; i < identifiersForBusco.size(); i++) {
             String genomeOrSubgenome = identifiersForBusco.get(i);
             String dbName = selectedDatabases.get(i);
             String fileName = "short_summary.specific." + dbName + "." + genomeOrSubgenome + ".txt";
             String summaryFile = directory + "results/" + genomeOrSubgenome + "/" + fileName;
             if (!checkIfFileExists(summaryFile)) { 
                 continue;
             }
             copyFile(summaryFile, directory + "BUSCO_summary_files/" + fileName);
         }
        Pantools.logger.info("Visualize the BUSCO completeness:");
        Pantools.logger.info(location);
    }

    /**
     * @param outputDirectory
     * @param buscoScores
     * @return
     */
    private void createBuscoScorestxt(String outputDirectory, double[][] buscoScores,
                                     ArrayList<String> identifiersForBusco, HashMap<String, String> buscoScoresMap,
                                     int[][] buscoCounts, ArrayList<String> selectedDatabases) {

        StringBuilder output = new StringBuilder();
        double[] buscoLowestHighest = new double[]{101, 0, 101, 0, 101, 0, 101, 0, 101, 0}; // C lowest [0] and highest [1] ,S,D,F,M
        DecimalFormat formatter = new DecimalFormat("0.00");
        for (String genomeOrSubgenome : identifiersForBusco) {
            int index = identifiersForBusco.indexOf(genomeOrSubgenome);
            output.append("\nGenome ").append(genomeOrSubgenome).append(". ").append(selectedDatabases.get(index)).append(" ").append(buscoCounts[5][index]).append(" HMMs\n");
            String complete = formatter.format(buscoScores[0][index]);
            String single = formatter.format(buscoScores[1][index]);
            String duplicated = formatter.format(buscoScores[2][index]);
            String fragmented = formatter.format(buscoScores[3][index]);
            String missing = formatter.format(buscoScores[4][index]);
            output.append("C:").append(complete).append(" S:").append(single).append(" D:").append(duplicated).append(" F:")
                    .append(fragmented).append(" M:").append(missing).append("\n");
            check_if_lower_higher_array(buscoLowestHighest, buscoScores[0][index], 0);
            check_if_lower_higher_array(buscoLowestHighest, buscoScores[1][index], 2);
            check_if_lower_higher_array(buscoLowestHighest, buscoScores[2][index], 4);
            check_if_lower_higher_array(buscoLowestHighest, buscoScores[3][index], 6);
            check_if_lower_higher_array(buscoLowestHighest, buscoScores[4][index], 8);
            buscoScoresMap.put(genomeOrSubgenome, selectedDatabases.get(index) + "#C:" + complete + " S:" + single + " D:" + duplicated + " F:" + fragmented + " M:" + missing);
        }

        String statistics = calculateAverageMedianStatisticsBusco(buscoLowestHighest, buscoScores);
        writeStringToFile(statistics + output, outputDirectory + "busco_scores.txt", false, false);
    }

    /**
     *
     * @param buscoLowestHighest
     * @param buscoScores
     * @return
     */
    private String calculateAverageMedianStatisticsBusco(double[] buscoLowestHighest, double[][] buscoScores){
        DecimalFormat df = new DecimalFormat("0.000");
        ArrayList<Double> completeList = new ArrayList<>();
        ArrayList<Double> singleCopyList = new ArrayList<>();
        ArrayList<Double> duplicatedList = new ArrayList<>();
        ArrayList<Double> fragmentedList = new ArrayList<>(); // fragmented
        ArrayList<Double> missingList = new ArrayList<>(); // fragmented
        for (int i= 0; i < buscoScores[0].length; i++) {
            completeList.add(buscoScores[0][i]);
            singleCopyList.add(buscoScores[1][i]);
            duplicatedList.add(buscoScores[2][i]);
            fragmentedList.add(buscoScores[3][i]);
            missingList.add(buscoScores[4][i]);
        }

        // [median, average, standard deviation, shortest, longest, total, count]
        double[] completeStats = median_average_stdev_from_AL_double(completeList);
        double[] singleStats = median_average_stdev_from_AL_double(singleCopyList);
        double[] duplicatedStats = median_average_stdev_from_AL_double(duplicatedList);
        double[] fragmentedStats = median_average_stdev_from_AL_double(fragmentedList);
        double[] missingStats = median_average_stdev_from_AL_double(missingList);

        return "Category                            : average, median, lowest, highest " +
                "\nComplete BUSCOs (C)                 : " + df.format(completeStats[1]) + ", " + df.format(completeStats[0]) +
                ", " + df.format(buscoLowestHighest[0]) + ", " + df.format(buscoLowestHighest[1]) +
                "\nComplete and single-copy BUSCOs (S) : " + df.format(singleStats[1]) + ", " + df.format(singleStats[0]) +
                ", " + df.format(buscoLowestHighest[2]) + ", " + df.format(buscoLowestHighest[3]) +
                "\nComplete and duplicated BUSCOs (D)  : " + df.format(duplicatedStats[1]) + ", " + df.format(duplicatedStats[0]) +
                ", " + df.format(buscoLowestHighest[4]) + ", " + df.format(buscoLowestHighest[5]) +
                "\nFragmented BUSCOs (F)               : " + df.format(fragmentedStats[1]) + ", " + df.format(fragmentedStats[0]) +
                ", " + df.format(buscoLowestHighest[6]) + ", " + df.format(buscoLowestHighest[7]) +
                "\nMissing BUSCOs (M)                  : " + df.format(missingStats[1]) + ", " + df.format(missingStats[0]) +
                ", " + df.format(buscoLowestHighest[8]) + ", " + df.format(buscoLowestHighest[9]) + "\n";
    }

    /**
     * Put the BUSCO scores in the pangenome
     * Removes previous scores that match the orthodb name

     * @param buscoMode is currently always 'protein'.
     * @param buscoScoresMap
     */
    private void updateBuscoNodes(String buscoMode, HashMap<String, String> buscoScoresMap) {
        Pantools.logger.info("Updating BUSCO nodes.");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> buscoNodes = GRAPH_DB.findNodes(BUSCO_LABEL);
            while (buscoNodes.hasNext()) {
                Node buscoNode = buscoNodes.next();
                int genomeNr = (int) buscoNode.getProperty("genome");
                for (String key : buscoScoresMap.keySet()) {
                    if (key.startsWith(genomeNr + "_") || key.equals(genomeNr +"")) {
                        String score = buscoScoresMap.get(key);
                        String[] scoreArray = score.split("#"); // split into [benchmark set name, scores]
                        if (buscoNode.hasProperty(scoreArray[0] + "_" + buscoMode + "_score")) {
                            buscoNode.removeProperty(scoreArray[0] + "_" + buscoMode);
                        }
                        buscoNode.setProperty(scoreArray[0] + "_" + buscoMode + "_score", score);
                    }
                }
            }
            tx.success();
        }
    }

    /**
     * @param duplicatedList used to contain gene node identifier but now its mrna nodes.
     *                       in this way isoforms of the same gene were not counted as duplicated
     *
     *                       because off this change, the use of this function is obsolete. the gene is always duplicated
     * @param mrnaIdNodeMap
     * @param counts
     * @param hmmMap
     * @param buscoId
     * @param genomeOrSubgenome
     * @return
     */
    private int[] checkDuplicatedBusco(ArrayList<String> duplicatedList, HashMap<String, Node> mrnaIdNodeMap, int[] counts,
                                             HashMap<String, ArrayList<String>> hmmMap, String buscoId, String genomeOrSubgenome) {

        HashSet<Node> mrnaNodes = new HashSet<>();
        for (String mrnaId : duplicatedList) {
            Node geneNode = mrnaIdNodeMap.get(mrnaId);
            if (geneNode == null) {
                throw new RuntimeException(mrnaId + " (mRNA identifier) cannot be found...");
            }
            mrnaNodes.add(geneNode);
        }

        if (mrnaNodes.size() == 1) { // single copy
            counts[0] ++;
            hmmMap.computeIfAbsent(buscoId + "#Single copy", k -> new ArrayList<>()).add(genomeOrSubgenome);
        } else if (mrnaNodes.size() > 1) {
            counts[1] ++; // duplicated
            hmmMap.computeIfAbsent(buscoId + "#Duplicated", k -> new ArrayList<>()).add(genomeOrSubgenome);
        }
        return counts;
    }
    /**
     * @param dbName
     * @param hmmMap
     * @param genomeOrSubgenome
     * @param mrnaIdNodeMap
     * @return int
     */
    private int[] readBuscoFullTableTsv(String dbName, HashMap<String, ArrayList<String>> hmmMap,
                                            String genomeOrSubgenome,
                                            HashMap<String, Node> mrnaIdNodeMap,
                                            String buscoBaseDirectory) {

        String inputFile = buscoBaseDirectory + "results/" + genomeOrSubgenome + "/run_" + dbName + "/full_table.tsv";
        if (!checkIfFileExists(inputFile)) {
            throw new RuntimeException("\nThe BUSCO run failed, please check your installation.\n ");
        }

        int[] counts = new int[4]; // complete single copy, complete duplicated, fragmented, missing
        String prevBuscoId = "";
        ArrayList<String> duplicatedList = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.startsWith("#")) {
                    continue;
                }

                String[] lineArray = line.split("\t");
                String buscoId = lineArray[0];
                if (!buscoId.equals(prevBuscoId)) {
                    if (duplicatedList.size() > 1) {
                        counts = checkDuplicatedBusco(duplicatedList, mrnaIdNodeMap, counts, hmmMap, prevBuscoId, genomeOrSubgenome);
                    }
                    duplicatedList = new ArrayList<>();
                }
                if (lineArray[1].equals("Duplicated")) {
                    duplicatedList.add(lineArray[2]);
                } else {
                    if (lineArray[1].equals("Missing")) {
                        counts[3] ++;
                        hmmMap.computeIfAbsent(buscoId + "#" + lineArray[1], k -> new ArrayList<>()).add(genomeOrSubgenome);
                    } else if (lineArray[1].equals("Complete")) {
                        counts[0] ++;
                        hmmMap.computeIfAbsent(buscoId + "#Single copy", k -> new ArrayList<>()).add(genomeOrSubgenome);
                    } else { // fragmented
                        counts[2] ++;
                        hmmMap.computeIfAbsent(buscoId + "#" + lineArray[1], k -> new ArrayList<>()).add(genomeOrSubgenome);
                    }
                }
                prevBuscoId = buscoId;
            }
            counts = checkDuplicatedBusco(duplicatedList, mrnaIdNodeMap, counts, hmmMap, prevBuscoId, genomeOrSubgenome);
        } catch (IOException ioe) {
            throw new RuntimeException("\nFailed to read busco output: " + inputFile);
        }
        return counts;
    }

    /**
     *
     * @param genomeOrSubgenome
     * @param buscoScores Array1: Complete, Single copy, Duplicated, Fragmented, Missing .array2 genome numbers.
     * @param identifiersForBusco
     * @param counts
     */
    private void updateBuscoCountsArray(String genomeOrSubgenome, int[][] buscoScores,
                                        ArrayList<String> identifiersForBusco, int[] counts) {

        int index = identifiersForBusco.indexOf(genomeOrSubgenome);
        buscoScores[0][index] = counts[0] + counts[1]; // complete
        buscoScores[1][index] = counts[0]; // single copy
        buscoScores[2][index] = counts[1]; // duplicated
        buscoScores[3][index] = counts[2]; // fragmented
        buscoScores[4][index] = counts[3]; // missing
        buscoScores[5][index] = counts[0] + counts[1] + counts[2] + counts[3];
    }

    /**
     * Create 'busco' nodes for all genomes if they do not exist yet
     */
    private void createBuscoNodeIfMissing() {
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> buscoNodes = GRAPH_DB.findNodes(BUSCO_LABEL);
            if (buscoNodes.hasNext()) {
                return; // busco nodes already exist
            }

            // busco nodes do not exist, create one for every genome (even when skipped)
            if (PROTEOME) { // panproteome does not have genome nodes
                for (int i = 1; i <= total_genomes; i++) {
                    Node buscoNode = GRAPH_DB.createNode(BUSCO_LABEL);
                    buscoNode.setProperty("genome", i);
                }
            } else {
                ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL);
                while (genomeNodes.hasNext()) {
                    Node genomeNode = genomeNodes.next();
                    int genomeNr = (int) genomeNode.getProperty("number");
                    Node buscoNode = GRAPH_DB.createNode(BUSCO_LABEL);
                    buscoNode.setProperty("genome", genomeNr);
                    genomeNode.createRelationshipTo(buscoNode, RelTypes.has_busco);
                }
            }
            tx.success();
        }
    }

    /**
     * Determine which lineage was used by BUSCO's automated lineage selection
     * @param dbName
     * @param outputName
     * @param genomeOrSubgenome
     * @return
     */
    private String findBestMatchingDatabase(String dbName, String outputName, String genomeOrSubgenome) {
        if (!dbName.equals("automatic_lineage")) { // lineage is already known, skip this function
            return dbName;
        }
        String pathStr = WORKING_DIRECTORY + "/busco/" + dbName + "/" + outputName + "/results/" + genomeOrSubgenome;
        if (!checkIfFileExists(pathStr)) { // BUSCO has not been run yet
            return dbName;
        }
        File path = new File(pathStr);
        //List of all files and directories
        ArrayList<String> specific = new ArrayList<>();
        ArrayList<String> generic = new ArrayList<>();
        String[] files = path.list();
        assert files != null;
        for (String file : Objects.requireNonNull(files)) {
            if (file.startsWith("short_summary.specific") && file.endsWith(".txt")){
                String[] fileArray = file.split("\\.");
                specific.add(fileArray[2]);
            } else if (file.startsWith("short_summary.generic") && file.endsWith(".txt")){
                String[] fileArray = file.split("\\.");
                generic.add(fileArray[2]);
            }
        }
        if (specific.size() > 1) {
            throw new RuntimeException(specific.size() + " specific lineages are found. Check the output and select 1");
        } else if (specific.isEmpty() && generic.size() > 1) {
            throw new RuntimeException("No specific lineages but " + generic.size() + " generic are found. Check the output and select one");
        }
        if (specific.size() == 1) {
            copy_directory(WORKING_DIRECTORY + "/busco/automatic_lineage/" + outputName + "/results/" + genomeOrSubgenome,
                    WORKING_DIRECTORY + "/busco/" + specific.get(0) + "/" + outputName + "/results/" + genomeOrSubgenome);
            Pantools.logger.info("Selected BUSCO lineage database: " + specific.get(0));
            return specific.get(0);
        }
        if (generic.isEmpty() ) {
            throw new RuntimeException("Automatic lineage selection ouput directory was found but without correct output files.\n" +
                    "Please delete the following directory: " + pathStr);
        }

        // default option, generic size is 1
        copy_directory(WORKING_DIRECTORY + "/busco/automatic_lineage/" + outputName + "/results/" + genomeOrSubgenome,
                WORKING_DIRECTORY + "/busco/" + generic.get(0) + "/" + outputName + "/results/" + genomeOrSubgenome);
        Pantools.logger.info("Selected BUSCO lineage database: " + generic.get(0));
        return generic.get(0);
    }

    /**
     * Generate possible BUSCO commands based on:
     * 1. busco version
     * 2. automatic lineage
     *
     * @param outputName
     * @param genomeOrSubgenome
     */
    private void runBusco(String outputName, String genomeOrSubgenome ) {
        delete_file_full_path(genomeOrSubgenome); // prevents crash when a file exists with an identical name to the (sub)genome number

        final String proteinInputFile = String.format("%s/proteins/for_busco/proteins_%s%s.fasta",
                WORKING_DIRECTORY,
                genomeOrSubgenome,
                longest_transcripts ? "_longest_transcripts" : "");

        String tarStr = "";
        String lineageStr = "";
        if (database.equals("automatic_lineage")) {
            lineageStr = " --auto-lineage";
        } else {
            lineageStr = " -l " + database;
        }

        if (buscoVersion == 5) {  // --tar compressions has not been implemented yet in busco 4
            tarStr = " --tar";
        }

        runCommand("busco -f -i " + proteinInputFile + " -o " + genomeOrSubgenome + " -m proteins --cpu " + THREADS + tarStr + lineageStr);
        copy_directory(genomeOrSubgenome, WORKING_DIRECTORY + "/busco/" + database + "/" + outputName + "/results/" + genomeOrSubgenome);
        delete_directory(genomeOrSubgenome);
    }

    /**
     *
     * @return
     */
    private ArrayList<String> prepareProteinsForBusco() {
        createDirectory(WORKING_DIRECTORY + "/proteins/for_busco/" , false);
        if (longest_transcripts) {
            Grouping proLayer = new Grouping();
            proLayer.find_longest_transcript_per_gene(false, 0);
        }
        ArrayList<String> genomeOrSubgenomes;
        if (subgenomeLevel) {
            PhasedFunctionalities phasedClass = new PhasedFunctionalities();
            phasedClass.preparePhasedGenomeInformation(false, selectedSequences);
            if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
                throw new RuntimeException("--phasing was included but no phasing information present");
            }
            genomeOrSubgenomes = phasedClass.obtainPhasingSeqIdentifiers();
            BufferedWriter[] proteinWriters = startSubgenomeProteinWriters(genomeOrSubgenomes);
            boolean retrieveProteins = checkIfProteinsAreMissing(proteinWriters);
            if (retrieveProteins) {
                writeProteinsPerPhase(proteinWriters, genomeOrSubgenomes);
                closeProteinWriters(proteinWriters);
            }
        } else {
            BufferedWriter[] proteinWriters = startGenomeProteinWriters();
            boolean retrieveProteins = checkIfProteinsAreMissing(proteinWriters);
            if (retrieveProteins) {
                writeProteinsPerGenome(proteinWriters);
                closeProteinWriters(proteinWriters);
            }
            return fillListWithGenomeNumbers();
        }
        return genomeOrSubgenomes;
    }

    /**
     *
     * @param proteinWriters
     * @return
     */
    private boolean checkIfProteinsAreMissing(BufferedWriter[] proteinWriters){
        boolean retrieveProteins = false;
        for (BufferedWriter bw : proteinWriters) {
            if (bw != null) {
                retrieveProteins = true;
            }
        }
        return retrieveProteins;
    }

    /**
     *
     * @return
     */
    private ArrayList<String> fillListWithGenomeNumbers() {
        ArrayList<String> identifiersForBusco = new ArrayList<>();
        for (int genomeNr : selectedGenomes) {
            identifiersForBusco.add(genomeNr +"");
        }
        return identifiersForBusco;
    }

    /**
     * TODO add progress bar
     * @param proteinWriters
     * @param genomeOrSubgenomes
     */
    private void writeProteinsPerPhase(BufferedWriter[] proteinWriters, ArrayList<String> genomeOrSubgenomes) {
        //int mrnaCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL);
            while (mrnaNodes.hasNext()) {
//                mrnaCounter++;
//                if (mrnaCounter % 1000 == 0 || mrnaCounter < 11) {
//                    System.out.print("\rPreparing proteins for BUSCO: " + mrnaCounter + "                       "); // spaces are intentional
//                }
                Node mrnaNode = mrnaNodes.next();
                if (longest_transcripts && !mrnaNode.hasProperty("longest_transcript")) {
                    continue;
                }
                int[] address = (int[]) mrnaNode.getProperty("address");
                if (!selectedSequences.contains(address[0] + "_" + address[1])) {
                    continue;
                }
                String requiredAnnotationId = annotation_identifiers.get(address[0]-1);
                String annotationId = (String) mrnaNode.getProperty("annotation_id");
                if (!requiredAnnotationId.equals(annotationId)) {
                    continue;
                }

                if (mrnaNode.hasProperty("protein_ID")) { // protein coding
                    String proteinId = (String) mrnaNode.getProperty("protein_ID");
                    proteinId = proteinId.replace("|","_").replace(":","_");
                    String proteinSequence = get_protein_sequence(mrnaNode);
                    proteinSequence = splitSeqOnLength(proteinSequence, 80);
                    String[] phasingInfo = phasingInfoMap.get(address[0] + "_" + address[1]); // example [1, D, 1D, 1_D]
                    String phasing = "unphased";
                    if (phasingInfo != null) {
                        String[] splitId = phasingInfo[3].split("_"); // 1_D becomes [1,D]
                        phasing = splitId[1];
                    }
                    int index = genomeOrSubgenomes.indexOf(address[0] + "_" + phasing);
                    if (index == -1) {
                        continue;
                    }
                    String[] protArray = proteinSequence.split("\n");
                    if (protArray[protArray.length-1].length() == 80) {
                        proteinWriters[index].write(">" + proteinId + "\n" + proteinSequence);
                    } else {
                        proteinWriters[index].write(">" + proteinId + "\n" + proteinSequence + "\n");
                    }
                }
            }
            tx.success();
        } catch (IOException e) {
            throw new RuntimeException("Failed to create files in: " + WORKING_DIRECTORY + "proteins/for_busco/");
        }
    }

    /**
     * TODO add progress bar
     * @param proteinWriters
     */
    private void writeProteinsPerGenome(BufferedWriter[] proteinWriters) {
        //int mrnaCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL);
            while (mrnaNodes.hasNext()) {
//                mrnaCounter++;
//                if (mrnaCounter % 1000 == 0 || mrnaCounter < 11) {
//                    System.out.print("\rPreparing proteins for BUSCO: "+ mrnaCounter + "             "); // spaces are intentional
//                }
                Node mrnaNode = mrnaNodes.next();
                if (longest_transcripts && !mrnaNode.hasProperty("longest_transcript")) {
                    continue;
                }

                int genomeNr;
                if (PROTEOME) {
                    genomeNr = (int) mrnaNode.getProperty("genome");
                } else {
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    genomeNr = address[0];
                    if (proteinWriters[genomeNr - 1] == null) {
                        continue;
                    }
                    if (!selectedSequences.contains(genomeNr + "_" + address[1])) {
                        continue;
                    }
                    String requiredAnnotationId = annotation_identifiers.get(genomeNr - 1);
                    String annotationId = (String) mrnaNode.getProperty("annotation_id");
                    if (!requiredAnnotationId.equals(annotationId)) {
                        continue;
                    }
                }
                if (proteinWriters[genomeNr - 1] == null) {
                    continue;
                }

                if (mrnaNode.hasProperty("protein_ID")) { // protein coding
                    String proteinId = (String) mrnaNode.getProperty("protein_ID");
                    proteinId = proteinId.replace("|","_").replace(":","_");
                    String proteinSequence = get_protein_sequence(mrnaNode);
                    proteinSequence = splitSeqOnLength(proteinSequence, 80);
                    String[] protArray = proteinSequence.split("\n");
                    if (protArray[protArray.length-1].length() == 80) {
                        proteinWriters[genomeNr-1].write(">" + proteinId + "\n" + proteinSequence);
                    } else {
                        proteinWriters[genomeNr-1].write(">" + proteinId + "\n" + proteinSequence + "\n");
                    }
                }
            }
            tx.success();
        } catch (IOException e) {
            throw new RuntimeException("Failed to create files in: " + WORKING_DIRECTORY + "proteins/for_busco/");
        }
    }

    private void closeProteinWriters(BufferedWriter[] proteinWriters) {
        try { // close the file writers
            for (int i= 0; i < proteinWriters.length; i++) {
                if (proteinWriters[i] == null) {
                    continue;
                }
                proteinWriters[i].close();
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to create files in: " + WORKING_DIRECTORY + "proteins/for_busco/");
        }
    }

    /**
     * Only create files that do not exist yet.
     * @param genomeOrSubgenomes
     * @return
     */
    private BufferedWriter[] startSubgenomeProteinWriters(ArrayList<String> genomeOrSubgenomes) {
        BufferedWriter[] proteinWriters = new BufferedWriter[genomeOrSubgenomes.size()];
        String longestTranscripts = "";
        if (longest_transcripts) {
            longestTranscripts = "_longest_transcripts";
        }
        try { // start the file writers
            for (int i= 0; i < genomeOrSubgenomes.size(); i++) {
                String file = WORKING_DIRECTORY + "proteins/for_busco/proteins_" + genomeOrSubgenomes.get(i) + longestTranscripts + ".fasta";
                if (!checkIfFileExists(file)) {
                    proteinWriters[i] = new BufferedWriter(new FileWriter(file));
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to create files in: " + WORKING_DIRECTORY + "proteins/for_busco/");
        }
        return proteinWriters;
    }

    /**
     * Only create files that do not exist yet.

     * @return
     */
    private BufferedWriter[] startGenomeProteinWriters() {
        create_directory_in_DB("proteins/for_busco/");
        BufferedWriter[] proteinWriters = new BufferedWriter[total_genomes];
        String longestTranscripts = "";
        if (longest_transcripts) {
            longestTranscripts = "_longest_transcripts";
        }
        try { // start the file writers
            for (int genomeNr : selectedGenomes) {
                String file = WORKING_DIRECTORY + "proteins/for_busco/proteins_" + genomeNr + longestTranscripts + ".fasta";
                if (!checkIfFileExists(file)) {
                    proteinWriters[genomeNr-1] = new BufferedWriter(new FileWriter(file));
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to create files in: " + WORKING_DIRECTORY + "proteins/for_busco/");
        }
        return proteinWriters;
    }

    /**
     * hmm_overview.txt is a list of BUSCO genes showing the assigned categories per genome.
     *
     * Can only be generated if all genomes had the the same benchmark set.
     * This might not be the case if with --all-automatic.
     *
     * @param hmmMap
     * @param outputPath
     * @param buscoCounts
     * @param selectedDatabases
     * @return
     */
    private HashMap<String, ArrayList<String>> createHmmOverview(HashMap<String, ArrayList<String>> hmmMap,
                                                             String outputPath,
                                                             int[][] buscoCounts, ArrayList<String> identifiersForBusco,
                                                             ArrayList<String> selectedDatabases) {

        HashSet<String> hmms = new HashSet<>(); // put all HMM identifiers in set
        for (String hmmKey : hmmMap.keySet()) {
            String[] keyArray = hmmKey.split("#"); // HMM ID + # + single copy /missing/fragmented/duplicated
            hmms.add(keyArray[0]);
        }

        String[] categories = {"Single copy", "Missing", "Fragmented", "Duplicated"};
        HashMap<String, ArrayList<String>> hmmPerGenome = new HashMap<>();
        StringBuilder output = new StringBuilder();
        ArrayList<String> perfectHmms = new ArrayList<>();

        for (String hmm : hmms) {
            boolean first = true;
            for (String category : categories) {
                ArrayList<String> genomesSequences = hmmMap.get(hmm + "#" + category);
                if (genomesSequences == null) { // the HMM has no (sub)genome in this category
                    continue;
                }

                String idHeader = "";
                if (first) {
                    idHeader = "\nBUSCO id: " + hmm + "\n";
                    first = false;
                }

                if (genomesSequences.size() == identifiersForBusco.size()) {
                    if (category.equals("Single copy")) {
                        perfectHmms.add(hmm);
                    } else {
                        output.append(idHeader).append(category).append(" (").append(genomesSequences.size()).append("): all genomes\n");
                    }
                } else {
                    output.append(idHeader).append(category).append(" (").append(genomesSequences.size()).append("): ")
                            .append(genomesSequences.toString().replace("[","").replace("]","")).append("\n");
                }

                for (String genomeOrSubgenome : genomesSequences) {
                    hmmPerGenome.computeIfAbsent(genomeOrSubgenome + "#" + category, k -> new ArrayList<>()).add(hmm);
                }
            }
        }

        HashSet<String> selectedDatabasesSet = new HashSet<>(selectedDatabases);
        if (selectedDatabasesSet.size() == 1) {
            int totalHMM = buscoCounts[5][0]; // all genomes have the same set, total HMMs is equal for any set
            String header = "HMMs 'Single Copy' in " + selectedGenomes.size() + " genomes: " + perfectHmms.size() + "/" + totalHMM ;
            try {
                int genomeNr = Integer.parseInt(identifiersForBusco.get(0));
            } catch (NumberFormatException nfe) { // no genome numbers, so phased genomes were included
                header = "HMMs 'Complete' in all " + identifiersForBusco.size() + " genomes or phases: " + perfectHmms.size() + "/" + totalHMM + "\n" +
                        "Phased and unphased genomes: " + identifiersForBusco + "\n";
            }
            writeStringToFile(header + "\n" + output +
                            "\nHHMs single copy in every (sub)genome:\n" +
                            perfectHmms.toString().replace("[","").replace("]","").replace(", ",","),
                     outputPath + "/hmm_overview.txt", false, false);
        } else {
            writeStringToFile("Unable to create this file with more than a single benchmark set." +
                            selectedDatabases.size() + " different benchmarks sets were used in the most recent analysis.",
                     outputPath + "/hmm_overview.txt", false, false);
        }
        return hmmPerGenome;
    }

    /**
     *
     * @param hmmMap // key is BUSCO ID + # BUSCO category. Value are the genomes or subgenomes
     * @param identifiersForBusco
     * @param buscoDirectory
     */
    private void createBuscoUpsetInput(HashMap<String, ArrayList<String>> hmmMap, ArrayList<String> identifiersForBusco,
                                      String buscoDirectory) {
        if (!subgenomeLevel) {
            return;
        }

        HashMap<String, ArrayList<String>> subgenomesPerGenome = retrieveSubgenomeFromBuscoIdentifiers(identifiersForBusco);
        HashMap<String, StringBuilder> upsetInputBuilders = initializeBuscoUpsetInputFiles(subgenomesPerGenome);
        writeSubgenomeCountsToUpsetInput(hmmMap, subgenomesPerGenome, upsetInputBuilders);
        createUpsetDirectories(subgenomesPerGenome, buscoDirectory);
        writeUpsetInputToFile(upsetInputBuilders, subgenomesPerGenome, buscoDirectory);
        createBuscoUpsetRscript(buscoDirectory, subgenomesPerGenome);
    }

    private void writeUpsetInputToFile(HashMap<String, StringBuilder> upsetInputBuilders,
                                       HashMap<String, ArrayList<String>> subgenomesPerGenome, String buscoDirectory) {

        for (String genomeKey : upsetInputBuilders.keySet()) { // genome number + BUSCO category
            StringBuilder output = upsetInputBuilders.get(genomeKey);
            genomeKey = genomeKey.replace("#", "_").replace(" ", "");
            String[] keyArray = genomeKey.split("_");
            int nrRows = output.toString().split("\n").length;
            if (subgenomesPerGenome.get(keyArray[0] +"").size() == 1 ||  nrRows == 1){
                continue;
            }
            writeStringToFile(output.toString(), buscoDirectory + "/upset/" + keyArray[0] + "/" + keyArray[1] + ".csv", false, false);
        }
    }


    private void writeSubgenomeCountsToUpsetInput(HashMap<String, ArrayList<String>> hmmMap,
                                                  HashMap<String, ArrayList<String>> subgenomesPerGenome,
                                                  HashMap<String, StringBuilder> upsetInputBuilders) {

        String[] categories = {"Single copy", "Missing", "Fragmented", "Duplicated"};
        HashSet<String> alreadyAddedIds = new HashSet<>();
        for (String hmmKey : hmmMap.keySet()) {
            String[] keyArray = hmmKey.split("#"); // BUSCO ID + # + single copy /missing/fragmented/duplicated
            String buscoId = keyArray[0];
            if (alreadyAddedIds.contains(buscoId)) { // to ensure we go over each BUSCO gene only once
                continue;
            } else {
                alreadyAddedIds.add(buscoId);
            }
            HashMap<String, int[]> countMap = new HashMap<>();
            for (String category : categories) {
                ArrayList<String> genomesSequences = hmmMap.get(buscoId + "#" + category);
                if (genomesSequences == null) {
                    continue;
                }

                // key is a genome nr (str) + missing/fragmented/duplicated - value is count per phase
                for (String subgenomeId : genomesSequences) {
                    String[] array = subgenomeId.split("_"); // 1A becomes [1,A]
                    String genomeKey = array[0] + "#" + category; // genomeNr with category
                    ArrayList<String> phasesPerId = subgenomesPerGenome.get(array[0]);
                    int[] values;
                    if (countMap.containsKey(genomeKey)) {
                        values = countMap.get(genomeKey);
                    } else {
                        values = new int[phasesPerId.size()];
                    }
                    int index = phasesPerId.indexOf(subgenomeId);
                    values[index] = 1;
                    countMap.put(genomeKey, values);
                }
            }

            for (String genomeKey : countMap.keySet()) { // key is genomeNr + "#" + BUSCO category
                upsetInputBuilders.computeIfAbsent(genomeKey, k -> new StringBuilder()).append(buscoId + "," + arrayToCSVString(countMap.get(genomeKey)) + "\n");
            }
        }
    }

    private void createUpsetDirectories(HashMap<String, ArrayList<String>> subgenomesPerGenome, String buscoDirectory) {
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1] || subgenomesPerGenome.get(i +"").size() == 1){
                continue;
            }
            deleteDirectory(buscoDirectory + "/upset/" + i, false);
            createDirectory(buscoDirectory + "/upset/" + i, false);
        }
    }

    /**
     *
     * @param identifiersForBusco
     */
    private HashMap<String, ArrayList<String>> retrieveSubgenomeFromBuscoIdentifiers(ArrayList<String> identifiersForBusco){
        HashMap<String, ArrayList<String>> subgenomesPerGenome = new HashMap<>();
        for (String genomeOrSubgenome : identifiersForBusco) {
            String[] genomeOrSubgenomeArray = genomeOrSubgenome.split("_");
            subgenomesPerGenome.computeIfAbsent(genomeOrSubgenomeArray[0], k -> new ArrayList<>()).add(genomeOrSubgenome);
        }
        return subgenomesPerGenome;
    }

    /**
     * writes the header to alle possible upset combinations: genome numbers with BUSCO categories
     * @param subgenomesPerGenome
     * @return
     */
    private HashMap<String, StringBuilder> initializeBuscoUpsetInputFiles(HashMap<String, ArrayList<String>> subgenomesPerGenome) {
        String[] categories = {"Single copy", "Missing", "Fragmented", "Duplicated"};
        HashMap<String, StringBuilder> outputPerFile = new HashMap<>(); // key is a genome number with one of the BUSCO categories
        for (String genomeNrStr : subgenomesPerGenome.keySet()) {
            if (subgenomesPerGenome.get(genomeNrStr).size() == 1) {
                continue; // its an unphased genome
            }

            for (String category : categories) {
                StringBuilder header = new StringBuilder("busco,");
                ArrayList<String> subgenomes = subgenomesPerGenome.get(genomeNrStr);
                for (String subgenome : subgenomes) {
                    header.append(subgenome).append(",");
                }
                outputPerFile.computeIfAbsent(genomeNrStr + "#" + category, k -> new StringBuilder()).append(header.toString().replaceFirst(".$","") + "\n");
            }
        }
        return outputPerFile;
    }

    private void createBuscoUpsetRscript(String buscoDirectory, HashMap<String, ArrayList<String>> subgenomesPerGenome) {
        int plotCounter = 1;
        StringBuilder rscript = new StringBuilder();
        String R_LIB = check_r_libraries_environment();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"UpSetR\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(UpSetR)\n\n");

        rscript.append("plot_height <- 5\n")
                .append("plot_width <- 10\n")
                .append("text_size <- 1.8\n\n");

        String[] categories = {"Singlecopy", "Missing", "Fragmented", "Duplicated"};
        //String[] colors = {"#56B4E9","#ff0202", "#f4ff02", "#2b03cc"}; // original
        String[] colors = {"#56B4E9","#F04442","#F0E442", "#3492C7"}; // colors matching BUSCO generate_plot (python) output
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1] || subgenomesPerGenome.get(i +"").size() == 1) {
                continue;
            }
            rscript.append("cat ('Genome ").append(i).append("\\n')\n");
            for (int j = 0; j < categories.length; j++) {
                String category = categories[j];
                String color = colors[j];
                String file = buscoDirectory + "upset/" + i + "/" + category + ".csv";
                if (!checkIfFileExists(file)) {
                    continue;
                }

                rscript.append("df").append(plotCounter).append(" <- read.csv(file = '").append(file).append("', sep = ',', header = TRUE, check.names=FALSE)\n")
                        .append("plot").append(plotCounter).append(" <-upset(df").append(plotCounter).append(", sets.bar.color = \"").append(color).append("\", text.scale = text_size)\n")
                        .append("pdf(file = \"").append(buscoDirectory).append("upset/").append(i).append("/").append(category).append(".pdf\", width = plot_width, height = plot_height)\n")
                        .append("plot").append(plotCounter).append("\n")
                        .append("dev.off()\n\n");
                plotCounter++;
            }
        }
        rscript.append("cat ('\\nOutput files written to: ").append(buscoDirectory).append("upset/\\n\\n')\n");
        writeStringToFile(rscript.toString(), buscoDirectory + "upset/upset_plot.R", false, false);
    }

    /**
     *
     * @param questionableBuscos
     * @param buscoCounts
     * @param hmmPerGenome
     * @param genomeOrSubgenomeIds list with genome numbers or sequence/phasing identifiers
     */
    private void createBuscoCsvOverview(String[] questionableBuscos, int[][] buscoCounts,
                                              HashMap<String, ArrayList<String>> hmmPerGenome, String outputLocation,
                                              ArrayList<String> genomeOrSubgenomeIds, ArrayList<String> selectedDatabases) {

        String[] categories = {"Single copy", "Missing", "Fragmented", "Duplicated"};
        StringBuilder outputBuilder = new StringBuilder("Genome,BenchMark set,total HMM,Complete single copy (total),Complete single copy (total percentage),Missing HMMs (total),"
                + "Missing HMMs (total percentage),Missing HMMs,Fragmented HMMs (total),Fragmented HMMs (total percentage),"
                + "Fragmented HMMs,Duplicated HMMs (total),Duplicated HMMs (total percentage),Duplicated HMMs");

        if (questionableBuscos.length > 0) {
            outputBuilder.append(",Questionable BUSCOs not correct (max ").append(questionableBuscos.length).append(")\n");
        } else {
            outputBuilder.append("\n");
        }

        for (int i = 0; i < genomeOrSubgenomeIds.size(); i++) {
            String genomeOrSubgenome = genomeOrSubgenomeIds.get(i);
            int totalHmm = buscoCounts[5][i];
            outputBuilder.append(genomeOrSubgenome).append(",").append(selectedDatabases.get(i)).append(",").append(totalHmm).append(",");
            int wrongBuscoCounter = 0;
            for (int j=0; j < categories.length; j++) {
                ArrayList<String> HMMs = hmmPerGenome.get(genomeOrSubgenome + "#" + categories[j]);
                if (HMMs == null) {
                    outputBuilder.append("-,-,-");
                } else if (categories[j].equals("Single copy")) {
                    String percentage = percentageAsString(HMMs.size(), totalHmm, 3);
                    outputBuilder.append(HMMs.size()).append(",").append(percentage);
                } else {
                    for (String busco : HMMs) {
                        if (ArrayUtils.contains(questionableBuscos, busco)) {
                            wrongBuscoCounter++;
                        }
                    }
                    String percentage = percentageAsString(HMMs.size(), totalHmm, 3);
                    outputBuilder.append(HMMs.size()).append(",").append(percentage).append(",")
                            .append(HMMs.toString().replace("[","").replace("]","").replace(", "," "));
                }
                outputBuilder.append(",");
            }
            if (questionableBuscos.length > 0) {
                outputBuilder.append(wrongBuscoCounter);
            }
            outputBuilder.append("\n");
        }
        writeStringToFile(outputBuilder.toString(), outputLocation + "busco_overview.csv", false, false);
    }

    /**
     * TODO output must be redirected to logger
     * @param command
     */
    private void runCommand(String command) {
        List<String> cmd = new ArrayList<>();
        cmd.add("sh");
        cmd.add("-c");
        cmd.add(command);
        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT); // to stdout
        try {
            Process p = pb.start();
            p.waitFor(1, TimeUnit.DAYS);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong while running BUSCO");
        }
    }
}
