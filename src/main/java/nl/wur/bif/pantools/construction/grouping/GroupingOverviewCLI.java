package nl.wur.bif.pantools.construction.grouping;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.FAST;
import static nl.wur.bif.pantools.utils.Globals.proLayer;
import static picocli.CommandLine.*;

/**
 * Create an overview table for every homology grouping in the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "grouping_overview", sortOptions = false)
public class GroupingOverviewCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = "--fast")
    boolean fast;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        proLayer.grouping_overview();
        return 0;
    }

    private void setGlobalParameters() {
        FAST = fast;
    }

}
