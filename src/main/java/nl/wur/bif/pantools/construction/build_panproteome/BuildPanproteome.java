package nl.wur.bif.pantools.construction.build_panproteome;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;

import static nl.wur.bif.pantools.analysis.classification.Classification.genome_overview;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class BuildPanproteome {

    public BuildPanproteome(Path databaseDirectory) throws IOException {
        initialize_panproteome(databaseDirectory);
    }

    /**
     * Function initialized from commandline: build_panproteome
     *
     * Constructs the proteome layer of the pan-genome from a set of proteins.
     */
      private void initialize_panproteome(Path databaseDirectory) throws IOException {
        check_if_program_exists_stdout("kmc -h", 100, "kmc", true); // check if program is set to $PATH
        if (PATH_TO_THE_PROTEOMES_FILE == null) {
            Pantools.logger.error("No protein file provided via --proteomes-file or -pf.");
            System.exit(1);
        }
        String file_path, file_type, line, protein_ID;
        StringBuilder protein = new StringBuilder();
        Node protein_node, panproteome;
        int trsc, num_proteins = 0, genome;
        String[] fields;
        check_if_all_proteome_files_exist();
        GraphUtils.createPanproteomeDatabase(databaseDirectory);
        try(BufferedReader protein_paths = new BufferedReader(new FileReader(PATH_TO_THE_PROTEOMES_FILE))) {
            for (genome = 1; (file_path = protein_paths.readLine()) != null; ++genome) {
                String previous_ID_line = "";
                file_path = file_path.trim();
                if (file_path.equals("")) {// if line is empty
                    continue;
                }
                fields = file_path.split("\\.");
                file_type = fields[fields.length - 1].toLowerCase();
                if (file_type.equals("fasta") || file_type.equals("faa") || file_type.equals("fas") || file_type.equals("fa")) {
                    copyFile(file_path, WORKING_DIRECTORY + "proteins/proteins_" + genome + ".fasta");
                    try (Transaction tx = GRAPH_DB.beginTx()) {
                        try (BufferedReader in = new BufferedReader(new FileReader(file_path))) {
                            for (int c = 0; in.ready();) {
                                line = in.readLine().trim();
                                if (line.equals("")) {// if line is empty
                                    continue;
                                } else if (line.charAt(0) == '>') {
                                    ++num_proteins;
                                    if (previous_ID_line.equals("")) {
                                        previous_ID_line = line;
                                        continue;
                                    }

                                    String[] line_array = previous_ID_line.split(" ");

                                    protein_ID = line_array[0].substring(1);
                                    protein_node = GRAPH_DB.createNode(MRNA_LABEL);
                                    protein_node.setProperty("protein_ID", protein_ID);
                                    protein_node.setProperty("header", previous_ID_line);
                                    protein_node.setProperty("protein_sequence", protein.toString());
                                    protein_node.setProperty("protein_length", protein.length());
                                    protein_node.setProperty("genome", genome);
                                    protein.setLength(0);
                                    previous_ID_line = line;

                                } else {
                                    protein.append(line);
                                }
                                if (num_proteins % 100 == 1) {
                                    System.out.print("\r" + genome + " proteomes. " + num_proteins + " proteins ");
                                }
                            }
                        } catch (IOException ioe) {
                            Pantools.logger.error("Unable to read: {}", file_path);
                            System.exit(1);
                        }
                        tx.success();
                    }

                // For the last protein
                    try (Transaction tx = GRAPH_DB.beginTx()) {
                        String[] line_array = previous_ID_line.split(" ");
                        protein_ID = line_array[0].substring(1);
                        protein_node = GRAPH_DB.createNode(MRNA_LABEL);
                        protein_node.setProperty("protein_ID", protein_ID);
                        protein_node.setProperty("header", previous_ID_line);
                        protein_node.setProperty("protein", protein.toString());
                        protein_node.setProperty("protein_length", protein.length());
                        protein_node.setProperty("genome",genome);
                        protein.setLength(0);
                        tx.success();
                    }
                } else {
                    Pantools.logger.error("{} does not have a valid extention (fasta, faa, fas or fa).", file_path);
                    System.exit(1);
                }
            }
            System.out.print("\r" + (genome-1) + " proteomes. " + num_proteins + " proteins\n\n");
            try (Transaction tx1 = GRAPH_DB.beginTx()) {
                panproteome = GRAPH_DB.createNode(PANGENOME_LABEL);
                panproteome.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
                panproteome.setProperty("num_genomes", genome - 1);
                panproteome.setProperty("num_proteins", num_proteins);
                tx1.success();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        copyFile(PATH_TO_THE_PROTEOMES_FILE, WORKING_DIRECTORY + "log/panproteome_input_files.txt");
        genome_overview();
    }

    /**
     * Stops the program if one of the files does not exist
     */
    private void check_if_all_proteome_files_exist() {
        try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_PROTEOMES_FILE))) { // first check if all files exist
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (!check_if_file_exists(line)) {
                    Pantools.logger.error("Checking files: {} does not exist.", line);
                    System.exit(1);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", PATH_TO_THE_PROTEOMES_FILE);
            System.exit(1);
        }
    }
}
