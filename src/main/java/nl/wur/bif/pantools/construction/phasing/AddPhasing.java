package nl.wur.bif.pantools.construction.phasing;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class AddPhasing {

    // still uses some global variables:
    // WORKING DIRECTORY
    // the skip class
    // try_incr_hashmap & try_incr_AL_hashmap

    private final String[] letters = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    /**
     *
     * previous phasing information is always removed
     */
    public void addPhasing(Path phasingFile, boolean allUnphased) {
        Pantools.logger.info("Assigning phasing identifiers to pangenome.");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success();
        }
        skip.retrieveSelectedGenomes(); // sets selectedGenomes
        skip.retrieveSelectedSequences(0, false); // sets selectedSequences

        boolean chromosomeNumbersOnly = checkIfFileHasOnlyChromosomeNumbers(phasingFile);
        HashMap<String, String> sequenceHaplotypeId; // key is sequence Id, value is haplotype id (= chromosome number with letter or "_unphased)
        if (chromosomeNumbersOnly) { // filetype 1
            Pantools.logger.info("The second column of the file contains only (chromosome) numbers.");
            sequenceHaplotypeId = readFileWithChromosomeNumbersOnly(phasingFile, allUnphased);
        } else {  // filetype 1, directly assign identifiers
            Pantools.logger.info("Detected input format 2. Directly assign identifiers.");
            sequenceHaplotypeId = readDirectlyAddPhasingFile(phasingFile);
        }

        createAddPhasingLog(sequenceHaplotypeId);
        writePhasingInfoToNodes(sequenceHaplotypeId);
        Pantools.logger.info("Log written to:");
        Pantools.logger.info(" {}assigned_phasing_identifiers.csv", WORKING_DIRECTORY);
    }

    /**
     * treecluster is installed via pip (available in busco_env)
     * TreeCluster.py -i phased_genomes/blueberry/blueber/kmer_classification/kmer_distance.tree_RENAMED -m avg_clade -t 0.04
     **
     * File must have to two columns. Column 1, the genome number or sequence identifier. Column 2, the cluster (or chromosome) number.
     * Haplotypes letters are assigned by order of first appearance

     * @param phasingFile
     */
    private HashMap<String, String> readFileWithChromosomeNumbersOnly(Path phasingFile, boolean allUnphased) {
        HashMap<String, String> sequenceHaplotypeId = new HashMap<>();
        HashMap<String, ArrayList<String>> sequencesPerChromosome = new HashMap<>(); // key is cluster/chromosome number + "#" + a genome number
        try {
            BufferedReader br = Files.newBufferedReader(phasingFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.startsWith("SequenceName") || line.equals("")) { // header from Treecluster.py or empty line
                    continue;
                }
                String[] lineArray = splitLineOnEitherSpaceTabComma(line, 2);
                String sequenceId = lineArray[0];
                int[] genomeSequenceNumbers = checkIfCorrectSequenceIdentifier(sequenceId); // [genome number, sequence number]

                int chromosomeNr = Integer.parseInt(lineArray[1]);
                if (chromosomeNr < 0) { // -1 in treecluster.py output means it does not belong to any other cluster
                    chromosomeNr = 0;
                }
                sequencesPerChromosome.computeIfAbsent(
                        chromosomeNr + "#" + genomeSequenceNumbers[0], s -> new ArrayList<>()).add(sequenceId);

                ArrayList<String> sequencesOfChromosome = sequencesPerChromosome.get(chromosomeNr + "#" + genomeSequenceNumbers[0]);
                int sequenceCount = sequencesOfChromosome.size();
                if (sequenceCount > letters.length) {
                    throw new RuntimeException("The current limit for haplotypes per chromosome is " + letters.length );
                }

                if (chromosomeNr > 0) {
                    if (allUnphased) {
                        sequenceHaplotypeId.put(sequenceId, chromosomeNr + "_unphased");
                    } else {
                        sequenceHaplotypeId.put(sequenceId, chromosomeNr + "_" + letters[sequenceCount - 1].toUpperCase());
                    }

                } else { // cluster/chromosome number is 0
                    sequenceHaplotypeId.put(sequenceId, "0_unphased");
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to read: " + phasingFile);
        }

        if (sequenceHaplotypeId.isEmpty()) {
            throw new RuntimeException("Something went wrong with reading " + phasingFile);
        }

        // for chromosomes that have only a single phase
        // replace phase 'A' with 'unphased' for genomes with only 1 sequences in a cluster
        for (String chromosomeKey : sequencesPerChromosome.keySet()) {
            String[] keyArray = chromosomeKey.split("#"); // [ chromosome number, genome number]
            if (sequencesPerChromosome.get(chromosomeKey).size() == 1) {
                ArrayList<String> sequences = sequencesPerChromosome.get(chromosomeKey);
                for (String sequenceId : sequences) {
                    if (sequenceId.startsWith(keyArray[1] + "_")) {
                        sequenceHaplotypeId.put(sequenceId, keyArray[0] + "_unphased");
                    }
                }
            }
        }
        return sequenceHaplotypeId;
    }

    /**
     * Check if the first column contains a correct sequence identifier. example 1_1
     * @return
     */
    private int[] checkIfCorrectSequenceIdentifier(String testSequenceId) {
        if (!testSequenceId.contains("_")) { // a sequence identifier was included in the first column
            throw new RuntimeException(testSequenceId + " is not a correct sequence identifier");
        }
        String[] sequenceIdArray = testSequenceId.split("_");
        if (sequenceIdArray.length != 2) {
            throw new RuntimeException(testSequenceId + " is not a correct sequence identifier");
        }

        int genomeNr = 0;
        try {
            genomeNr = Integer.parseInt(sequenceIdArray[0]);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("\rThe genome number or sequence identifier is not correct. " +
                    "This should be a number: " + sequenceIdArray[0]);
        }

        int sequenceNr = 0;
        try {
            sequenceNr = Integer.parseInt(sequenceIdArray[1]);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("The genome number or sequence identifier is not correct. " +
                    "This should be a number: " + sequenceIdArray[1]);
        }
        String sequenceId = genomeNr + "_" + sequenceNr;
        if (!selectedSequences.contains(sequenceId)) {
            throw new RuntimeException(sequenceId + " cannot be found in the current sequence selection. " +
                    "Adjust the selection with --include, exclude or --selection-file");
        }
        return new int[]{genomeNr, sequenceNr};
    }

    /**
     * Check if the second column of the included (phasing info) file contains numbers only
     */
    private boolean checkIfFileHasOnlyChromosomeNumbers(Path phasingFile) {
        try {
            BufferedReader br = Files.newBufferedReader(phasingFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.equals("") || (line.startsWith("SequenceName"))) { // empty line or cluster file from treecluster (with only numbers)
                    continue;
                }
                String[] lineArray = splitLineOnEitherSpaceTabComma(line, 2);
                try {
                    Integer.parseInt(lineArray[1]);
                } catch (NumberFormatException nfe) {
                    return false; // failed to convert to a number
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to read: " + phasingFile);
        }
        return true;
    }

    /**
     *  split a string on either a space tab or comma
     */
    private String[] splitLineOnEitherSpaceTabComma(String string, int allowedColumns) {
        String[] array = string.split("\\s+"); // split on 1 or multiple spaces
        if (array.length != allowedColumns) {
            array = string.split(","); // split on comma
            if (array.length != allowedColumns) {
                array = string.split("\\t"); // split on tab
                if (array.length != allowedColumns) {
                    throw new RuntimeException("File must have " + allowedColumns + " columns, seperated by a space, comma or tab.\n" +
                            "Line: " + string);
                }
            }
        }
        return array;
    }

    /**
     *
     * @param sequenceHaplotypeId key is sequence Id, value is haplotype id (= chromosome number with letter or "_unphased)
     */
    private void createAddPhasingLog(HashMap<String, String> sequenceHaplotypeId) {
        StringBuilder log = new StringBuilder("Sequence identifier,phasing_chromosome,phasing_ID\n");
        ArrayList<String> added_sequences = new ArrayList<>();
        ArrayList<String> missingSeqIds = new ArrayList<>(); // sequence identifiers that were in the input file but are not found in the pangenome
        for (String sequenceId : selectedSequences) {
            if (!sequenceHaplotypeId.containsKey(sequenceId)) {
                continue;
            }
            String haplotypeId = sequenceHaplotypeId.get(sequenceId);
            String[] idArray = haplotypeId.split("_");
            int chromosomeNr = 0;
            try {
                chromosomeNr = Integer.parseInt(idArray[0]);
            } catch (NumberFormatException nfe) {
                // do nothing, chromosomeNr remains 0
            }
            log.append(sequenceId).append(",").append(chromosomeNr).append(",").append(haplotypeId).append("\n");
            added_sequences.add(sequenceId);
        }

        for (String sequenceId : sequenceHaplotypeId.keySet()) {
            if (!added_sequences.contains(sequenceId)) {
                missingSeqIds.add(sequenceId);
            }
        }

        if (!missingSeqIds.isEmpty()) {
            throw new RuntimeException(missingSeqIds.size() + " sequence identifiers were not found in the pangenome!\n" +
                    "List of missing sequence identifiers: " + missingSeqIds);
        }
        Pantools.logger.info("Phasing information can be added to all " + sequenceHaplotypeId.size() + " selected sequences.");
        writeStringToFile(log.toString(), WORKING_DIRECTORY + "assigned_phasing_identifiers.csv", false, false);
    }

    /**
     * TODO function goes over all sequence nodes of the pangenome. requires progress bar
     * @param sequenceHaplotypeId  key is sequence Id, value is haplotype id (= chromosome number with letter or "_unphased)
     */
    private void writePhasingInfoToNodes(HashMap<String, String> sequenceHaplotypeId) {
        int[] addedPhasingCounter = new int[GENOME_DB.num_genomes];
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequencenodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            long nodeCount = count_nodes(SEQUENCE_LABEL);
            while (sequencenodes.hasNext()) {
                Node sequenceNode = sequencenodes.next();
                counter++;
                String sequenceId = (String) sequenceNode.getProperty("identifier");
                String[] sequenceIdArray = sequenceId.split("_");
                int genomeNr = Integer.parseInt(sequenceIdArray[0]);
                if (counter % 10000 == 0 || counter == nodeCount || counter == 1) { // TODO progress bar
                    System.out.print("\rAdding phasing information sequence nodes: " + counter + "/" + nodeCount + " ");
                }
                if (sequenceNode.hasProperty("phasing_chromosome")) {
                    // always remove identifiers from a previous run
                    sequenceNode.removeProperty("phasing_chromosome");
                    sequenceNode.removeProperty("phasing_ID");
                    sequenceNode.removeProperty("phasing_assigned");
                }

                if (sequenceHaplotypeId.containsKey(sequenceId)) {
                    String haplotypeId = sequenceHaplotypeId.get(sequenceId);
                    String[] idArray = haplotypeId.split("_");
                    int chromosomeNr = 0;
                    try {
                        chromosomeNr = Integer.parseInt(idArray[0]);
                    } catch (NumberFormatException nfe) {
                        // do nothing, chromosomeNr remains 0
                    }
                    addedPhasingCounter[genomeNr-1]++;
                    sequenceNode.setProperty("phasing_chromosome", chromosomeNr);
                    sequenceNode.setProperty("phasing_ID", haplotypeId);
                    sequenceNode.setProperty("phasing_assigned", true);
                }
            }
            tx.success();
        }
        System.out.println(""); // TODO remove when there is progress bar

        Pantools.logger.info("Adding phasing information to the genomes:");
        for (int i = 0; i < addedPhasingCounter.length; i++) {
            if (addedPhasingCounter[i] != 0) {
                Pantools.logger.info(" " + (i+1) + ": " + addedPhasingCounter[i] + " sequences.");
            }
        }
    }

    /**
     * 2 possible lines
     *
     * 1. sequence identifier with only a (chromosome) number. Automatically becomes unphased
     * 2. sequence identifier with phasing identifier (=chromosome number and haplotype letter)
     *
     * Multiple unphased are allowed
     * Sequences with haplotype ID is not allowed
     */
    private HashMap<String, String> readDirectlyAddPhasingFile(Path phasingFile) {
        HashMap<String, String> sequenceHaplotypeId = new HashMap<>();
        HashMap<String, Integer> countsPerPhase = new HashMap<>();
        try {
            BufferedReader br = Files.newBufferedReader(phasingFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.equals("")) {
                    continue;
                }
                String[] lineArray = splitLineOnEitherSpaceTabComma(line, 2);
                int[] genomeSequenceNumbers = checkIfCorrectSequenceIdentifier(lineArray[0]); // [genome Nr, sequence Nr]
                if (lineArray[1].equals("unphased")) {
                    countsPerPhase.merge(genomeSequenceNumbers[0] + "_unphased", 1, Integer::sum);
                } else {
                    int chromosomeNr = checkIfIdentifiersHasChromosomeNr(lineArray[1]);
                    String haplotypeLetter = checkIfIdentifiersHasPhase(lineArray[1]);
                    countsPerPhase.merge(genomeSequenceNumbers[0] + "_" + chromosomeNr + "_" + haplotypeLetter, 1, Integer::sum);
                    sequenceHaplotypeId.put(genomeSequenceNumbers[0] + "_" + genomeSequenceNumbers[1], chromosomeNr + "_" + haplotypeLetter);
                }
            }
        }  catch (IOException ioe) {
            throw new RuntimeException("Failed to read: " + phasingFile + "\n");
        }
        checkHaplotypeFrequency(countsPerPhase);
        return sequenceHaplotypeId;
    }

    /**
     * Only allow unique haplotype identifiers.
     * Example: Genome 1 can have only one 1_A
     * @param countsPerPhase
     */
    private void checkHaplotypeFrequency(HashMap<String, Integer> countsPerPhase) {
        boolean stop = false; // show all non unique haplotype IDs before stopping
        for (String key : countsPerPhase.keySet()) { // key is a genome number + chromosome number + haplotype letter
            if (countsPerPhase.get(key) > 1 && !key.contains("unphased")) {
                stop = true;
                Pantools.logger.error(key + " is found on " + countsPerPhase.get(key) + " sequences!");
            }
        }
        if (stop) {
            throw new RuntimeException("Only unphased are allowed to non-unique identifier");
        }
    }

    /**
     * Throw exception if haplotype identifier does not end with a letter (or is not a number, checked earlier)
     */
    private String checkIfIdentifiersHasPhase(String identifier) {
        String[] chromosomeArray = identifier.split("_");
        String haplotypeLetter;
        if (chromosomeArray.length == 1) { // it wasn't split, therefore it must be a number only and its unphased
            haplotypeLetter = "unphased";
        } else if (chromosomeArray.length == 2) { // its a haplotype letter
            if (chromosomeArray[1].length() > 1) {
                throw new RuntimeException(identifier + " may only contain a single letter. Example 1_A");
            }
            haplotypeLetter = chromosomeArray[1];
        } else {
            throw new RuntimeException(identifier + " should be chromosome number and haplotype letter. Example 1_A");
        }
        return haplotypeLetter;
    }

    /**
     * Throw exception if haplotype identifier does not start with a number
     */
    private int checkIfIdentifiersHasChromosomeNr(String identifier) {
        try {
            return Integer.parseInt(identifier);
        } catch (NumberFormatException numberFormatException) {
            // its ok, do nothing
        }

        String[] chromosomeArray = identifier.split("_");
        if (chromosomeArray.length != 2) {
            throw new RuntimeException("The identifier " + identifier + " should be a chromosome number with haplotype letter. Example 1_A");
        }
        // if length is 1, a chromosome number. else it's a chromosome number with haplotype letter
        int chromosomeNr = 0;
        try {
            chromosomeNr = Integer.parseInt(chromosomeArray[0]);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException(identifier + " does not start with a (chromosome) number");
        }
        return chromosomeNr;
    }
}
