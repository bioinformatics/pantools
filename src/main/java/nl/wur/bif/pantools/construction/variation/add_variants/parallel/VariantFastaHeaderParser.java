package nl.wur.bif.pantools.construction.variation.add_variants.parallel;

import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.io.template.SequenceHeaderParserInterface;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;

/**
 * Class for parsing the header of a variant fasta file
 *
 * @param <S> the sequence
 * @param <C> the compound class for the type of sequence
 */
public class VariantFastaHeaderParser<S extends AbstractSequence<C>, C extends Compound>
        implements SequenceHeaderParserInterface<S, C> {

    /**
     * Parse the header and set original header and accession id for the sequence
     *
     * @param header   the header of the sequence
     * @param sequence the sequence
     *                 NB: This method expects the header to be in the format ">sequence_name:start-end featureId". Only the
     *                 featureId is set as the accession id.
     */
    @Override
    public void parseHeader(String header, S sequence) {
        final String[] fields = header.split("\\s+");
        if (fields.length != 2) {
            throw new RuntimeException("The header of the fasta file is not in the correct format.");
        }

        // set the minimal values for sequence
        sequence.setOriginalHeader(header);
        sequence.setAccession(new AccessionID(fields[1]));
    }
}