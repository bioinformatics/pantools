package nl.wur.bif.pantools.construction.variation;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Math.max;
import static nl.wur.bif.pantools.utils.Globals.*;

/**
 * This class is used to add and remove variation from the pangenome graph.
 * Variation can be in the form of VCF files or in the form of a PAV file.
 * Here, we handle accession nodes and variant nodes.
 * Accession nodes contain metadata about the added variation and are connected to the genome node in case of a pangenome.
 * Variant nodes contain the actual variation and are connected to the mRNA nodes.
 * In case of a pangenome, the variant nodes also contain their consensus (nucleotide) sequence.
 *
 * @author Dirk-Jan van Workum
 */
public abstract class AddVariation {

    /**
     * Constructor for the Variation class.
     */
    public AddVariation() {

        // Check if the "variation" directory exists in WORKING_DIRECTORY, if not create it
        final Path variationPath = Paths.get(WORKING_DIRECTORY, "variation");
        Pantools.logger.debug("Setting variationPath to {}", variationPath);
        if (!variationPath.toFile().exists()) {
            Pantools.logger.debug("Creating variation directory.");
            boolean mkdirSuccess = variationPath.toFile().mkdir();
            if (!mkdirSuccess) {
                Pantools.logger.error("Unable to create variation directory.");
                throw new RuntimeException("Unable to access database.");
            }
        }
    }

    /**
     * Checks if the provided files are incompatible with one another and the pangenome
     * @param variantFileMap a HashMap with path to a variant file as key and genome number as value
     * @param type the type of variation (PAV or VCF)
     * @throws IOException if a PAV file cannot be read
     * @throws NotFoundException if the graph database cannot be found
     */
    protected void validateVariationFiles(Map<Path, Integer> variantFileMap, String type)
            throws IOException, NotFoundException {
        assert (type.matches("VCF|PAV"));

        Pantools.logger.debug("Checking compatibility of the provided {} files.", type);

        final Map<Integer, Set<String>> genomeSampleMap = getSamplesFromPangenome(type);

        // check if the provided variant files are compatible with one another and the pangenome
        for (Map.Entry<Path, Integer> entry : variantFileMap.entrySet()) {
            final int genomeNr = entry.getValue();
            final Path file = entry.getKey();

            // get the sample names depending on the variation type
            final Set<String> samples = new HashSet<>(getSamples(file));

            Pantools.logger.debug("Checking compatibility of {} (genome {}) with the other variant files for this genome and the pangenome.", file, genomeNr);

            // check if the samples from the variant files are compatible with the pangenome and the other variant files
            if (genomeSampleMap.containsKey(genomeNr)) {
                final Set<String> genomeSamples = genomeSampleMap.get(genomeNr);
                for (String sample : samples) {
                    if (genomeSamples.contains(sample)) {
                        Pantools.logger.error("Sample {} for {} is already in the pangenome or in multiple provided variant files.", sample, genomeNr);
                        throw new RuntimeException("The provided VCF files are not compatible.");
                    } else {
                        genomeSamples.add(sample);
                    }
                }
            } else {
                genomeSampleMap.put(genomeNr, samples);
            }
        }
    }

    /**
     * Read and validate the variation locations file.
     * @param locationsFile file containing paths to variation files ant their reference genome.
     * @param type variation type (VCF or PAV)
     * @return map of variation file paths and their reference genome
     */
    protected Map<Path, Integer> readVariationLocationsFile(Path locationsFile, String type) {
        final Map<Path, Integer> variantLocations = new HashMap<>();
        assert (type.matches("VCF|PAV"));
        Pantools.logger.info("Reading the provided {} file list.", type);
        Pantools.logger.debug("Reading {} files from {}", type, locationsFile);

        try (BufferedReader reader = new BufferedReader(new FileReader(locationsFile.toFile()))) {
            while (reader.ready()) {
                final String line = reader.readLine().trim();
                if (line.equals("")) continue;

                Pantools.logger.debug("Processing line: {}", line);
                final String[] fields = line.split("\\s+"); //every line should be "<genome number> <path to vcf file>"
                //check that
                // 1) there are only two fields
                if (fields.length != 2) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a {} file per line. " +
                                    "This line does not contain two fields: {}",
                            locationsFile, type, line);
                    throw new RuntimeException("Invalid input file");
                }

                // 2) the first field is an integer
                final int genomeNr;
                try {
                    genomeNr = Integer.parseInt(fields[0]);
                } catch (NumberFormatException nfe) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a {} file per line. " +
                                    "This line does not contain a valid genome number: {}",
                            locationsFile, type, line);
                    throw new RuntimeException("Invalid input file");
                }

                // 3) the second field is a valid Path
                final Path variantFile;
                try {
                    variantFile = Paths.get(fields[1]);
                } catch (NullPointerException npe) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a {} file per line. " +
                                    "This line does not contain a valid path to a VCF file: {}",
                            locationsFile, type, line);
                    throw new RuntimeException("Invalid input file");
                }

                Pantools.logger.debug("Checking validity of input: {} {}", genomeNr, variantFile);

                // 4) the genome number is in the pangenome
                if (genomeNr > total_genomes) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a {} file per line. " +
                                    "This line contains a genome number that is not in the pangenome: {}",
                            locationsFile, type, line);
                    throw new RuntimeException("Invalid input file");
                }

                // 5) the file is a vcf/pav file (ends with .vcf.gz or .tsv)
                final String extension = (type.equals("PAV")) ? ".tsv" : ".vcf.gz";
                if (!variantFile.toString().endsWith(extension)) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a {} file per line. " +
                                    "This line contains a path to a VCF file that does not end with .vcf.gz: {}",
                            locationsFile, type, line);
                    throw new RuntimeException("Invalid input file");
                }

                // 6) the file exists
                if (!variantFile.toFile().exists()) {
                    Pantools.logger.error(
                            "Every line of {} has to contain a genome number and a path to a VCF file per line. " +
                                    "This line contains a path to a VCF file that does not exist: {}",
                            locationsFile, line);
                    throw new RuntimeException("Invalid input file");
                }

                Pantools.logger.debug("Genome {} has VCF file {}", genomeNr, variantFile);
                variantLocations.put(variantFile, genomeNr);
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Could not read {}", locationsFile);
            throw new RuntimeException("Invalid input file");
        }

        //check that hashmap is not empty
        if (variantLocations.isEmpty()) {
            Pantools.logger.error("The VCF list file {} is empty.", locationsFile);
            throw new RuntimeException("Invalid input file");
        }

        return variantLocations;
    }

    /**
     * Create a variant node in the pangenome and connect it to the mRNA node.
     * @param mrnaNode the mRNA node
     * @param sample the sample
     * @return the variant node
     * @throws NotFoundException if the database gives an error
     */
    protected Node createVariantNode(Node mrnaNode, String sample) throws NotFoundException {
        // throw an error if the mRNA node is null
        if (mrnaNode == null) {
            Pantools.logger.error("Cannot create variant node for null mRNA node.");
            throw new NotFoundException("mRNA node is null");
        }

        // throw an error if the mRNA node does not have the "protein_ID" property
        if (!mrnaNode.hasProperty("protein_ID")) {
            Pantools.logger.error("Cannot create variant node for mRNA node {} ({}) without protein_ID property.",
                    mrnaNode.getId(), mrnaNode.getProperty("id"));
            throw new NotFoundException("mRNA node without protein_ID property");
        }

        // create the variant node
        final Node variantNode = GRAPH_DB.createNode(VARIANT_LABEL);
        variantNode.setProperty("id", mrnaNode.getProperty("genome") + "|" + sample);
        variantNode.setProperty("protein_ID", mrnaNode.getProperty("protein_ID"));
        variantNode.setProperty("sample", sample);
        variantNode.setProperty("type", "mRNA");
        variantNode.setProperty("genome", mrnaNode.getProperty("genome"));
        if (!PROTEOME) {
            variantNode.setProperty("strand", mrnaNode.getProperty("strand"));
            variantNode.setProperty("phase", mrnaNode.getProperty("phase"));
            if (mrnaNode.hasProperty("locus_tag")) {
                variantNode.setProperty("locus_tag", mrnaNode.getProperty("locus_tag"));
            }
            if (mrnaNode.hasProperty("name")) {
                variantNode.setProperty("name", mrnaNode.getProperty("name"));
            }
        }

        // create the relationship between the variant and the mRNA
        mrnaNode.createRelationshipTo(variantNode, RelTypes.has_variant);

        return variantNode;
    }

    protected abstract Set<String> getSamples(Path file) throws IOException;

    /**
     * Connects accession nodes to a given genome node
     *
     * @param genomeNode genome node
     * @param accessionNodes list of accession nodes
     * @throws NotFoundException if the database gives an error
     */
    protected void connectAccessionNodes(Node genomeNode, Set<Node> accessionNodes) throws NotFoundException {
        // connect the accession node to the genome node
        final int genomeNr = (int) genomeNode.getProperty("number");
        Pantools.logger.debug("Creating relationship between genome node {} and accession nodes.", genomeNr);
        for (Node accessionNode : accessionNodes) {
            genomeNode.createRelationshipTo(accessionNode, RelTypes.has);
        }
    }

    /**
     * Creates an accession node in the graph database for each sample in the vcf/pav file
     *
     * @param genomeNr genome node
     * @param samples array of all samples in the vcf/pav file
     * @param type whether we are currently creating an accession node for VCF or PAV
     * @return the newly created accession nodes
     * @throws NotFoundException if the database gives an error
     */
    protected Set<Node> createAccessionNodes(int genomeNr, Set<String> samples, String type) throws NotFoundException {
        Pantools.logger.debug("Creating accession nodes.");
        assert (type.matches("VCF|PAV"));

        final Map<String, Node> accessionNodeMap = new HashMap<>();
        int highestAccession = 0;

        // loop over all accessions
        try (ResourceIterator<Node> accessionNodesIterator = GRAPH_DB.findNodes(ACCESSION_LABEL, "genome", genomeNr)) {
            while (accessionNodesIterator.hasNext()) {
                final Node accessionNode = accessionNodesIterator.next();

                // keep track of which samples have already been added
                final String sample = (String) accessionNode.getProperty("sample");
                accessionNodeMap.put(sample, accessionNode);

                // keep track of all accession numbers
                final String identifier = (String) accessionNode.getProperty("identifier");
                final int accessionNumber = Integer.parseInt(identifier.split("_")[1]);
                highestAccession = max(highestAccession, accessionNumber);
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Error while getting accession nodes for genome {}", genomeNr);
            throw nfe;
        }

        for (final String sample : samples) {
            Node accessionNode;

            if (accessionNodeMap.containsKey(sample)) { // add to an existing accession node
                accessionNode = accessionNodeMap.get(sample);
                if ((boolean) accessionNode.getProperty(type)) {
                    Pantools.logger.error("Accession node for sample {} of genome {} already contains {} information.",
                            accessionNode.getProperty("sample"),
                            accessionNode.getProperty("genome"),
                            type);
                    throw new RuntimeException(String.format("Nodes already contain %s information", type));
                }
            } else { // create new accession node
                highestAccession++;
                accessionNode = createAccessionNode(genomeNr, highestAccession, sample);
                accessionNodeMap.put(sample, accessionNode);
            }
            addAccessionNodeInformation(accessionNode);
        }
        return new HashSet<>(accessionNodeMap.values());
    }

    private Node createAccessionNode(int genomeNr, int accession, String sample) {
        final Node accessionNode = GRAPH_DB.createNode(ACCESSION_LABEL);
        final String id = String.format("%s_%s", genomeNr, accession);
        accessionNode.setProperty("identifier", id);
        accessionNode.setProperty("sample", sample);
        accessionNode.setProperty("genome", genomeNr);
        accessionNode.setProperty("date", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
        return accessionNode;
    }

    protected abstract void addAccessionNodeInformation(Node accessionNode);

    /**
     * Get all samples from the pangenome for a given variation type
     * @param type the type of variation (PAV or VCF)
     * @return a HashMap with genome number as key and a list of samples as value
     */
    private Map<Integer, Set<String>> getSamplesFromPangenome(String type) {

        // create a hashmap of all the samples in the pangenome per genome
        final Map<Integer, Set<String>> genomeSampleMap = new HashMap<>();

        try (Transaction tx = GRAPH_DB.beginTx()) {
            // iterate over all accession nodes in the pangenome
            try (ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, type, true)) {
                while (accessionNodes.hasNext()) {
                    final Node accessionNode = accessionNodes.next();
                    final int genomeNr = (int) accessionNode.getProperty("genome");
                    final String sample = (String) accessionNode.getProperty("sample");

                    // add sample to the sample map
                    genomeSampleMap.putIfAbsent(genomeNr, new HashSet<>());
                    genomeSampleMap.get(genomeNr).add(sample);

                    Pantools.logger.debug("Accession {} from genome {} is in the pangenome.", sample, genomeNr);
                }
            }
        }
        return genomeSampleMap;
    }
}