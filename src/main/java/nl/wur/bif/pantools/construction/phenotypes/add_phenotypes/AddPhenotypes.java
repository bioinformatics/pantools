package nl.wur.bif.pantools.construction.phenotypes.add_phenotypes;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.phenotypes.Phenotypes;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static nl.wur.bif.pantools.utils.Globals.*;

public class AddPhenotypes extends Phenotypes {

    public AddPhenotypes(Path phenotypesFile, int bins, boolean updateNodes) {
        addPhenotype(phenotypesFile, bins, updateNodes);
    }

    /**
     * Requires:
     * -dp
     * --phenotype
     *
     * Optional:
     * --append
     * --skip or --reference
     */
    private void addPhenotype(Path phenotypesFile, int bins, boolean updateNodes) {
        Pantools.logger.info("Adding phenotype information to the pangenome database.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            throw new RuntimeException("Database error");
        }

        int columnsInHeader = 0, lineCounter = 0;
        HashMap<Integer, String> propertyPerColumn = new HashMap<>();
        HashSet<String> phenotypeProperties = new HashSet<>();
        if (!updateNodes) {
            removePreviousPhenotypeNodes();
        }

        Node[] phenotypeNodes = getExistingPhenotypeNodes();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try {
                BufferedReader br = Files.newBufferedReader(phenotypesFile);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    line = line.trim();
                    String[] lineArray = line.replace(", ", ",").replace(" ,", ",").split(",");
                    if (lineCounter == 0) { // first line is the header
                        if (line.contains(",,")) { // header is not allowed to have empty columns
                            throw new RuntimeException("The header of your csv file is not correctly formatted, no empty column double commas allowed!");
                        }
                        if (lineArray.length < 2) {
                            throw new RuntimeException("The input csv file must have at least two columns. The first column must always be the genome numbers");
                        }
                        columnsInHeader = lineArray.length;
                        for (int i = 1; i < lineArray.length; i++) { // starts at 1 because 0 is 'Genome'
                            String phenotypeProperty = lineArray[i].trim();
                            propertyPerColumn.put(i, phenotypeProperty);
                        }
                    } else {
                        if (columnsInHeader < lineArray.length) {
                            throw new RuntimeException("The header (first line) contained less columns as line " +
                                    (line+1) + ":" + columnsInHeader + " against" + lineArray.length);
                        }

                        int genomeNr;
                        try {
                            genomeNr = Integer.parseInt(lineArray[0]);
                        } catch (NumberFormatException numberFormatException) {
                            throw new RuntimeException("Only genome numbers are allowed in the leftmost column. Found " + lineArray[0]);
                        }
                        Node phenotypeNode = getPhenotypeNode(genomeNr, phenotypeNodes);
                        for (int i = 1; i < columnsInHeader; i++) { // skip first column because it holds genome numbers
                            String phenotypeValue;
                            try {
                                phenotypeValue = lineArray[i];
                            } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) { // This happens when the last column is empty on some rows
                                phenotypeValue = "";
                            }
                            verifyPhenotypeValue(phenotypeValue);
                            String phenotypeProperty = propertyPerColumn.get(i);
                            writeValueToPhenotypeNode(phenotypeValue, phenotypeNode, phenotypeProperty);
                            phenotypeProperties.add(phenotypeProperty);
                        }
                    }
                    lineCounter ++;
                }
            } catch (IOException ioe) {
                throw new RuntimeException("Something went wrong while reading: " + phenotypesFile);
            }
            tx.success(); // transaction successful, commit changes
        }
        binPhenotypeValues(phenotypeProperties, bins, phenotypeNodes);
        String createdOrUpdated = "created";
        if (updateNodes) {
            createdOrUpdated = "updated";
        }

        System.out.println("\rSuccessfully " + createdOrUpdated + " phenotype nodes for " + (lineCounter-1) + " genomes!");
        phenotypeOverview();
    }

    /**
     *
     * @return array with phenotype nodes on genomeNr-1 positions
     */
    private Node[] getExistingPhenotypeNodes() {
        Node[] phenotypeNodes = new Node[total_genomes];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodeIterator = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodeIterator.hasNext()) {
                Node phenotypeNode = phenotypeNodeIterator.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                phenotypeNodes[genomeNr - 1] = phenotypeNode;
                phenotypeNode.getPropertyKeys();
            }
            tx.success();
        }
        return phenotypeNodes;
    }

    /**
     *
     */
    private void removePreviousPhenotypeNodes() {
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodeIterator = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodeIterator.hasNext()) {
                Node phenotypeNode = phenotypeNodeIterator.next();
                Iterable<Relationship> relations = phenotypeNode.getRelationships();
                for (Relationship relation : relations) {
                    relation.delete();
                }
            }
            tx.success();
        }
    }


    /**
     * Check if the phenotype value (string) contains any disallowed characters.
     * Currently only semicolon ;
     * @param phenotypeValue a phenotype value
     */
    private void verifyPhenotypeValue(String phenotypeValue) {
        if (phenotypeValue.contains(";")) {
            throw new RuntimeException("Phenotype values not allowed to have semicolon characters");
        }
    }

    /**
     * Three checks: is it an Integer? Double? Boolean? If not it stays a string.
     * @param phenotypeValue
     * @param phenotypeNode
     * @param phenotypeProperty
     * @return
     */
    private void writeValueToPhenotypeNode(String phenotypeValue, Node phenotypeNode, String phenotypeProperty) {
        if (phenotypeNode.hasProperty(phenotypeProperty)) {
            phenotypeNode.removeProperty(phenotypeProperty); // remove the current phenotype value
        }

        try { // is it an Integer?
            int i = Integer.parseInt(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, i);
            return;
        } catch (NumberFormatException nfe) {

        }

        try { // is it a Double?
            double d= Double.parseDouble(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, d);
            return;
        } catch (NumberFormatException nfe) {

        }

        // is it an Boolean?
        if (phenotypeValue.toUpperCase().equals("TRUE") || phenotypeValue.toUpperCase().equals("FALSE") ){
            boolean bool = Boolean.parseBoolean(phenotypeValue);
            phenotypeNode.setProperty(phenotypeProperty, bool);
            return;
        }
        // its just a string
        phenotypeNode.setProperty(phenotypeProperty, phenotypeValue);
    }

    /**
     * Retrieve or create 'phenotype' node of a specific genome
     * @param genomeNr
     * @param phenotypeNodes array with phenotype nodes on genomeNr-1 positions
     * @return
     */
    private Node getPhenotypeNode(int genomeNr, Node[] phenotypeNodes) {
        Node phenotypeNode;
        if (phenotypeNodes[genomeNr-1] != null) { // node already exists from previous run
            phenotypeNode = phenotypeNodes[genomeNr-1];
        } else {
            phenotypeNode = GRAPH_DB.createNode(PHENOTYPE_LABEL);
            phenotypeNode.setProperty("genome", genomeNr);
            if (!PROTEOME) {
                Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
                if (genomeNode == null) {
                    throw new RuntimeException("No genome has been found with number: " + genomeNr);
                }
                genomeNode.createRelationshipTo(phenotypeNode, RelTypes.has_phenotype);
            }
            phenotypeNodes[genomeNr-1] = phenotypeNode;
        }
        return phenotypeNode;
    }

    /**
     * only go over the recently added properties
     * @param phenotypeProperties all phenotype properties part of the current analysis
     * @param bins number of bins
     * @param phenotypeNodes array with phenotype nodes on genomeNr-1 positions
     */
    private void binPhenotypeValues(HashSet<String> phenotypeProperties, int bins, Node[] phenotypeNodes) {
        HashMap<String, Object[]> valuesPerPhenotype = retrieveAllPhenotypesAsObject();
        boolean first = true;

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction

            for (String phenotypeProperty : phenotypeProperties) {
                Object[] phenotypeValues = valuesPerPhenotype.get(phenotypeProperty);
                double[] lowestHighestValue = findHighestLowestValue(phenotypeValues);
                if (lowestHighestValue == null) { // null when not every value is a number
                    continue;
                }

                ArrayList<Double> levels = determineBinLevels(lowestHighestValue[0], lowestHighestValue[1], bins);
                if (first) {
                    Pantools.logger.info("Phenotypes with only numerical values recognized. Numbers are considered different phenotypes if they do not exactly match.");
                    Pantools.logger.info("Extra phenotypes are generated which place these values in {} equally sized bins.", bins);
                    first = false;
                }
                Pantools.logger.info(" Phenotype '{}_binned' range of bins: {}", phenotypeProperty, levels.toString().replace("[","").replace("]",""));
                for (int i = 0; i < phenotypeValues.length; i++) {
                    Object value = phenotypeValues[i];
                    if (value == null) {
                        continue;
                    }
                    Node phenotypeNode = phenotypeNodes[i];
                    phenotypeNode.removeProperty(phenotypeProperty + "_binned");
                    try {
                        double number = Double.parseDouble((String) value);
                        for (int l = 0; l < levels.size() - 1; l++) {
                            if (number >= levels.get(l) && number <= levels.get(l + 1)) {
                                phenotypeNode.setProperty(phenotypeProperty + "_binned", "bin " + (l + 1));
                                break;
                            }
                        }
                    } catch (NumberFormatException ex) {
                        phenotypeNode.setProperty(phenotypeProperty + "_binned", "Unknown");
                    }
                }
            }
            tx.success();
        }
    }

    /**
     * Retrieve phenotypes names and values from the phenotype nodes
     * @return
     */
    private HashMap<String, Object[]> retrieveAllPhenotypesAsObject() {
        HashMap<String, Object[]> valuesPerPhenotype = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotypeNodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotypeNodes.hasNext()) {
                Node phenotypeNode = phenotypeNodes.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                for (String phenotypeProperty : phenotypeNode.getPropertyKeys()) {
                    if (phenotypeProperty.equals("genome")) {
                        continue;
                    }
                    updateValuesPerPhenotype(genomeNr, valuesPerPhenotype, phenotypeProperty, String.valueOf(phenotypeNode.getProperty(phenotypeProperty)));
                }
            }
            tx.success();
        }
        return valuesPerPhenotype;
    }

    private void updateValuesPerPhenotype(int genomeNr, HashMap<String, Object[]> valuesPerPhenotype,
                                          String phenotypeProperty, String phenotypeValue) {

        Object[] array = valuesPerPhenotype.get(phenotypeProperty);
        if (array == null) {
            array = new Object[total_genomes];
        }
        array[genomeNr-1] = phenotypeValue;
        valuesPerPhenotype.put(phenotypeProperty, array);
    }

    private double[] findHighestLowestValue(Object[] numbers) {
        double lowest = Double.MAX_VALUE, highest = Double.MIN_VALUE;
        for (Object value : numbers) {
            if (value == null) { // genome was not included, skip entirely
                continue;
            }
            try {
                double doubleValue = Double.parseDouble((String) value);
                if (doubleValue > highest) {
                    highest = doubleValue;
                }
                if (doubleValue < lowest) {
                    lowest = doubleValue;
                }
            } catch (NumberFormatException numberFormatException) {
                return null;
            }
        }
        return new double[]{lowest, highest};
    }

    private ArrayList<Double> determineBinLevels(double lowest, double highest, int bins) {
        double difference = highest - lowest;
        double step = difference / bins;
        ArrayList<Double> levels = new ArrayList<>();
        levels.add(lowest);
        for (int i = 1; i < bins; i++) {
            levels.add((i * step) + lowest);
        }
        levels.add(highest);
        return levels;
    }
}
