package nl.wur.bif.pantools.construction.variation.remove_variants;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.construction.variation.RemoveVariation;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;

/**
 * This class is used to remove PAV information from the graph database.
 * Information is removed from accession and variant nodes.
 * If the nodes do not contain any VCF information, the nodes are completery removed instead.
 *
 * @author Dirk-Jan van Workum
 */
public class RemoveVariants extends RemoveVariation {
    public RemoveVariants() {
        removeVariantInformation();
    }

    /**
     * Removes all variant information from the variant nodes and accession nodes
     *
     * @throws NotFoundException if the variant nodes or accession nodes could not be found
     */
    private void removeVariantInformation() throws NotFoundException {
        Pantools.logger.info("Removing VCF information from the database.");
        removeAccessionInformation("VCF");
        removeVariantInformation("VCF");
        Pantools.logger.info("Finished removing VCF information from the database.");
    }

    /**
     * Remove an accession node if it does not contain PAV information, otherwise remove VCF information.
     *
     * @param accessionNode accession node with VCF information
     */
    @Override
    protected void removeAccessionNode(Node accessionNode) {
        // if PAV information is present, remove VCF information
        if ((boolean) accessionNode.getProperty("PAV")) {
            accessionNode.setProperty("VCF", false);
        } else { // otherwise remove the entire node
            GraphUtils.removeNode(accessionNode);
        }
    }

    /**
     * Remove a variant node if it does not contain PAV information, otherwise remove VCF information.
     *
     * @param variantNode variant node with VCF information
     */
    @Override
    protected void removeVariantNode(Node variantNode) {
        // if VCF information is present, remove PAV information
        if (variantNode.hasProperty("present")) {
            if (variantNode.hasProperty("mRNA_sequence")) {
                variantNode.removeProperty("mRNA_sequence");
            }
        } else { // otherwise remove the entire node
            GraphUtils.removeNode(variantNode);
        }
    }
}
