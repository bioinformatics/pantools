package nl.wur.bif.pantools.construction.build_pangenome.parallel;

import com.esotericsoftware.kryo.kryo5.Kryo;
import com.esotericsoftware.kryo.kryo5.io.Input;
import com.esotericsoftware.kryo.kryo5.io.Output;
import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorOutputStream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Utility functions for creating Kryo inputs and outputs. By default will create outputs with XZ compression.
 */
public class KryoUtils {
    private static final int DEFAULT_XZ_PRESET = 6;

    public static Kryo getKryo() {
        final Kryo kryo = new Kryo();
        kryo.register(Localization.class);
        return kryo;
    }

    public static Output createOutput(Path path) throws IOException {
        // Default buffer size for Kryo is 4kb, see:
        // https://github.com/EsotericSoftware/kryo/blob/e7170a1da8de84dfc5214a7d20029e33d10b3e8a/src/com/esotericsoftware/kryo/io/Output.java#L81
        return new Output(new XZCompressorOutputStream(Files.newOutputStream(path), DEFAULT_XZ_PRESET));
    }

    public static Output createOutput(Path path, int preset) throws IOException {
        // Default buffer size for Kryo is 4kb, see:
        // https://github.com/EsotericSoftware/kryo/blob/e7170a1da8de84dfc5214a7d20029e33d10b3e8a/src/com/esotericsoftware/kryo/io/Output.java#L81
        return new Output(new XZCompressorOutputStream(Files.newOutputStream(path), preset));
    }

    public static Input createInput(Path path) throws IOException {
        return new Input(new XZCompressorInputStream(Files.newInputStream(path)));
    }
}
