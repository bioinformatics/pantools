package nl.wur.bif.pantools.construction.phasing;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools add_phasing
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "add_phasing", sortOptions = false)
public class AddPhasingCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "phasingInput", index = "0+")
    @InputFile(message = "{file.phasing-input}")
    private Path phasingFile;

    @Option(names = "--assume-unphased")
    private boolean allUnphased;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();

        new AddPhasing().addPhasing(phasingFile, allUnphased);
        return 0;
    }
}
