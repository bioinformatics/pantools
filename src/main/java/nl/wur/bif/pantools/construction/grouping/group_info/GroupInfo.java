package nl.wur.bif.pantools.construction.grouping.group_info;

import nl.wur.bif.pantools.construction.index.IndexPointer;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.Utils;
import org.neo4j.graphdb.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.analysis.function_analysis.FunctionalAnalysis.function_overview_per_group;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.letters;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Globals.GRAPH_DB;
import static nl.wur.bif.pantools.utils.Utils.*;

public class GroupInfo {

    /**
     * #
     * Requires:
     *
     * -dp
     *
     * Requires one of the following
     * -hm
     * --node. the node id for the hmgroup
     *
     * Optional
     * -label
     */
    public void homology_group_info(List<Long> homologyGroups) throws RuntimeException {
        Pantools.logger.info("Reporting all information from selected homology groups.");
        StringBuilder output_builder = new StringBuilder();
        HashSet<String> groups_with_function = new HashSet<>();
        HashMap<String, HashSet<String>> groups_with_function_or_name = new HashMap<>();

        ArrayList<Node> hmNodeList;
        BufferedWriter nodeBuilder = null;
        createDirectory("group_info/haplotype_frequency", true);
        delete_file_in_DB("group_info/group_functional_annotations.txt");

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            check_current_grouping_version();
            hmNodeList = Utils.findHmNodes(homologyGroups, 0);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Unable to start the database.", nfe);
        }

        if (Mode.contains("K-MER")) { // user (that does not look at code) cannot see this mode
            Pantools.logger.info("K-MER mode was selected. Extracting nucleotide nodes from the graph.");
            try {
                nodeBuilder = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/group_nodes.txt"));
                nodeBuilder.write("#To visualize in the Neo4j browser:\n MATCH (n) where id(n) in [ PLACE,NODES,HERE ] return n\n\n");
            } catch (IOException e) {
                Pantools.logger.error("Unable to create {}", WORKING_DIRECTORY + "group_info/group_nodes.txt");
                e.printStackTrace();
                System.exit(1);
            }
        }

        ArrayList<String> name_list = new ArrayList<>();
        if (SELECTED_NAME != null) {
            name_list = new ArrayList<>(Arrays.asList(SELECTED_NAME.split("\\s*,\\s*")));
        }

        PhasedFunctionalities pf = new PhasedFunctionalities();
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(0, false);
        pf.preparePhasedGenomeInformation(true, selectedSequences);

        ArrayList<String> genomeList = new ArrayList<>();
        for (int genome : selectedGenomes) {
            genomeList.add(genome + "");
        }
        HashMap<String, Integer> ploidyPerGenome = pf.determinePloidyPerGenome(selectedSequences, genomeList);
        ArrayList<String> identifiersOfTableHeader = createHeaderOfSubgenomeIds(ploidyPerGenome, true);

        StringBuilder csv = createCSVHeader();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            function_overview_per_group(hmNodeList, WORKING_DIRECTORY + "group_info/");
            int counter = 0;
            for (Node homologyNode : hmNodeList) {
                StringBuilder allMrnaNucleotideNodes = new StringBuilder();
                StringBuilder gene_info_builder = new StringBuilder();

                final String group_id = String.valueOf(homologyNode.getId());
                csv.append("#Homology group ").append(homologyNode.getId()).append("\n");
                tableOfSubgenomePresenceGroup(homologyNode, identifiersOfTableHeader);
                tableOfSubgenomePresenceGroup2(homologyNode, identifiersOfTableHeader);
                String[] subgenomePresence = countSubgenomePresenceGroup(homologyNode);
                HashSet<Node> nucleotideNodes = new HashSet<>();
                ArrayList<Node> mrna_nodes = new ArrayList<>();
                counter ++;
                boolean correct = test_if_correct_label(homologyNode, HOMOLOGY_GROUP_LABEL, false);
                if (!correct) {
                    correct = test_if_correct_label(homologyNode, INACTIVE_HOMOLOGY_GROUP_LABEL, false);
                    if (!correct) {
                        Pantools.logger.error("{} is not a homology group.", homologyNode);
                        continue;
                    }
                }
                int num_members = (int) homologyNode.getProperty("num_members");
                int[] temp_copy_number = (int[]) homologyNode.getProperty("copy_number");
                int[] copy_number = remove_first_position_array(temp_copy_number);

                gene_info_builder
                        .append("Number of genes/proteins: ").append(num_members).append("\n")
                        .append("Gene copy number: ").append(Arrays.toString(copy_number)).append("\n");

                if (!PROTEOME) {
                    HashMap<String, int[]> haplotypeCounts = calculateUniqueHaplotypes(homologyNode);
                    int[] geneProteinHaplotypes = haplotypeCounts.get("pangenome");
                    int[] geneHaplotypesPerGenome = haplotypeCounts.get("genes");
                    int[] proteinHaplotypesPerGenome = haplotypeCounts.get("proteins");

                    gene_info_builder
                            .append("Gene haplotypes   : ").append(geneProteinHaplotypes[0]).append("\n")
                            .append("Protein haplotypes: ").append(geneProteinHaplotypes[1]).append("\n")
                            .append("gene haplotypes per genome: ").append(Arrays.toString(geneHaplotypesPerGenome)).append("\n")
                            .append("protein haplotypes per genome: ").append(Arrays.toString(proteinHaplotypesPerGenome)).append("\n");
                }

                if (subgenomePresence != null) {
                    gene_info_builder.append("subgenome presence: ").append(Arrays.toString(subgenomePresence)).append("\n");
                    gene_info_builder.append("For allele counts per subgenome, see ").append(WORKING_DIRECTORY).append("group_info/haplotype_frequency/").append(homologyNode.getId()).append("_protein.csv\n");
                }

                HashSet<String> names_set = new HashSet<>();
                HashSet<String> function_set = new HashSet<>();
                HashSet<Node> function_node_set = new HashSet<>();
                ArrayList<Integer> proteinSizes = new ArrayList<>();
                Iterable<Relationship> relations = homologyNode.getRelationships();
                Pantools.logger.info("Retrieving group information: {}/{} homology groups.", counter, hmNodeList.size());
                for (Relationship rel : relations) {
                    Node mrnaNode = rel.getEndNode();
                    if (PROTEOME) {
                        obtainGroupInformationPanproteome(groups_with_function, mrna_nodes, group_id,
                                gene_info_builder, function_set, function_node_set, proteinSizes, mrnaNode);
                    } else {
                        obtainGroupInformationPangenome(groups_with_function, groups_with_function_or_name, name_list,
                                nucleotideNodes, mrna_nodes, group_id, allMrnaNucleotideNodes, gene_info_builder,
                                names_set, function_set, function_node_set, proteinSizes, mrnaNode, csv);
                    }
                }

                String prot_size_freq = determine_frequency_list_int(proteinSizes);
                if (SELECTED_LABEL != null) { // user has given --label with a function identifier
                    check_function_output_matches(function_node_set, homologyNode.getId() +"", groups_with_function_or_name);
                }

                output_builder.append("#Homology group ").append(homologyNode.getId())
                        .append("\nAll gene names: ").append(names_set.toString().replace("[","").replace("]",""))
                        .append("\nAll functions : ").append(function_set.toString().replace("[","").replace("]",""))
                        .append("\nProtein sizes : ").append(prot_size_freq.replace(" ","")).append("\n")
                        .append(gene_info_builder).append("\n\n");

                if (Mode.contains("K-MER")) {
                    try {
                        assert nodeBuilder != null;
                        nodeBuilder.write("#Homology group " + homologyNode.getId() + "\n"); // do not remove, used by function that is currently disabled
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    addNodeIdentifiersToGroupInfoOutput(nodeBuilder, allMrnaNucleotideNodes, nucleotideNodes, mrna_nodes);
                }
                csv.append("\n");
            }

            if (!PROTEOME) {
                writeStringToFile(csv.toString(), WORKING_DIRECTORY + "group_info/group_info.csv", false, false);
            }
            writeStringToFile(output_builder.toString(), WORKING_DIRECTORY + "group_info/group_info.txt", false, false);
            write_groups_with_specific_functions(groups_with_function_or_name);
            write_groups_with_specific_gene_name(groups_with_function_or_name);
            if (Mode.contains("K-MER")) {
                try {
                    assert nodeBuilder != null;
                    nodeBuilder.close();
                } catch (IOException e) {
                    Pantools.logger.error("Unable to close {}", WORKING_DIRECTORY + "/group_info/nodes.txt");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            tx.success(); // transaction successful, commit changes
        }
        printGroupInfoOutputFiles();
    }

    /**
     * Method that obtains the group information for a panproteome
     * @param groupsWithFunction set of groups with a specific function
     * @param mrnaNodes list of mRNA nodes
     * @param groupId group id
     * @param geneInfoBuilder string builder for gene information
     * @param functionSet set of functions
     * @param functionNodeSet set of function nodes
     * @param proteinSizeList list of protein sizes
     * @param mrnaNode current mRNA node
     */
    private void obtainGroupInformationPanproteome(HashSet<String> groupsWithFunction,
                                                   ArrayList<Node> mrnaNodes,
                                                   String groupId,
                                                   StringBuilder geneInfoBuilder,
                                                   HashSet<String> functionSet,
                                                   HashSet<Node> functionNodeSet,
                                                   ArrayList<Integer> proteinSizeList,
                                                   Node mrnaNode) {
        long mrnaNodeId = mrnaNode.getId();
        int genome = (int) mrnaNode.getProperty("genome");
        if (skip_array[genome-1]) {
            return;
        }
        String proteinId = (String) mrnaNode.getProperty("protein_ID");
        int proteinLength = (int) mrnaNode.getProperty("protein_length");
        proteinSizeList.add(proteinLength);
        geneInfoBuilder
                .append("\nmRNA id: ").append(proteinId)
                .append("\nGenome: ").append(genome)
                .append("\nProtein length: ").append(proteinLength)
                .append("\nmRNA node id: ").append(mrnaNodeId)
                .append("\n");
        group_info_get_f_annotations_panproteome(mrnaNode, functionSet, functionNodeSet, geneInfoBuilder, groupsWithFunction, groupId);
        mrnaNodes.add(mrnaNode);
    }
    /**
     * Method that obtains the group information for a pangenome
     * @param groupsWithFunction set of groups with a specific function
     * @param groupsWithFunctionOrName map of groups with a specific function or name
     * @param nameList list of names
     * @param nucleotideNodes set of nucleotide nodes
     * @param mrnaNodes list of mRNA nodes
     * @param groupId group id
     * @param allMrnaNucleotideNodes string builder for all mRNA nucleotide nodes
     * @param geneInfoBuilder string builder for gene information
     * @param nameSet set of names
     * @param functionSet set of functions
     * @param functionNodeSet set of function nodes
     * @param proteinSizeList list of protein sizes
     * @param mrnaNode current mRNA node
     */
    private void obtainGroupInformationPangenome(HashSet<String> groupsWithFunction,
                                                 HashMap<String, HashSet<String>> groupsWithFunctionOrName,
                                                 ArrayList<String> nameList,
                                                 HashSet<Node> nucleotideNodes,
                                                 ArrayList<Node> mrnaNodes,
                                                 String groupId,
                                                 StringBuilder allMrnaNucleotideNodes,
                                                 StringBuilder geneInfoBuilder,
                                                 HashSet<String> nameSet,
                                                 HashSet<String> functionSet,
                                                 HashSet<Node> functionNodeSet,
                                                 ArrayList<Integer> proteinSizeList,
                                                 Node mrnaNode,
                                                 StringBuilder csv) {
        long mrnaNodeId = mrnaNode.getId();
        int[] mrnaAddress = (int[]) mrnaNode.getProperty("address");

        if (skip_array[mrnaAddress[0]-1]) {
            return;
        }
        Relationship mrnaRel = mrnaNode.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
        Node geneNode = mrnaRel.getStartNode();
        int[] geneAddress = (int[]) geneNode.getProperty("address");
        long geneNodeId = geneNode.getId();
        String strand = (String) mrnaNode.getProperty("strand");
        String geneName = retrieveNamePropertyAsString(geneNode);
        String[] geneNameArray = geneName.split("/");
        if (geneName.equals("")) {
            geneName = "-";
        }
        String geneGffId = (String) geneNode.getProperty("id");
        String mrnaName = retrieveNamePropertyAsString(geneNode);
        if (mrnaName.equals("")) {
            mrnaName = "-";
        }
        String mrnaGffId = (String) mrnaNode.getProperty("protein_ID");
        int nucleotideSize = (int) mrnaNode.getProperty("length");
        int proteinSize = (int) mrnaNode.getProperty("protein_length");
        for (String name : geneNameArray) {
            if (nameList.contains(name)) { // name_list is only filled when --name was included as argument
                groupsWithFunctionOrName.computeIfAbsent(geneName + "#name", k -> new HashSet<>()).add(groupId);
            }
            nameSet.add(name);
        }
        proteinSizeList.add(proteinSize);
        geneInfoBuilder
                .append("\nGene id: ").append(geneGffId)
                .append("\nGene name: ").append(geneName)
                .append("\nmRNA id: ").append(mrnaGffId)
                .append("\nmRNA name: ").append(mrnaName)
                .append("\nGenome: ").append(mrnaAddress[0])
                .append("\nAddress ").append(mrnaAddress[0]).append(" ").append(mrnaAddress[1]).append(" ").append(mrnaAddress[2]).append(" ").append(mrnaAddress[3])
                .append("\nStrand: ").append(strand)
                .append("\nNucleotide length: ").append(nucleotideSize)
                .append("\nProtein length: ").append(proteinSize)
                .append("\nGene node id: ").append(geneNodeId)
                .append("\nmRNA node id: ").append(mrnaNodeId).append("\n");
        group_info_get_f_annotations_pangenome(geneNode, mrnaNode, functionSet, functionNodeSet, geneInfoBuilder, groupsWithFunction, groupId);
        if (Mode.contains("K-MER")) {
            allMrnaNucleotideNodes.append(mrnaGffId).append("\n");
            find_nuc_nodes_of_hmgroup(nucleotideNodes, allMrnaNucleotideNodes, mrnaAddress);
        }
        mrnaNodes.add(mrnaNode);


        String chromosome = "";
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
            String[] phasingInfo = phasingInfoMap.get(mrnaAddress[0] + "_" + mrnaAddress[1]); // [1, B, 1B, 1_B]
            if (phasingInfo!= null) {
                chromosome = phasingInfo[3];
            } else {
                chromosome = "unphased";
            }
        }

        csv.append(geneGffId).append(",").append(geneName).append(",").append(geneGffId)
                .append(",").append(mrnaName)
                .append(",").append(mrnaAddress[0]) // genome number
                .append(",").append(mrnaAddress[1]) // sequence number
                .append(",").append(mrnaAddress[2]) // mrna start position
                .append(",").append(mrnaAddress[3]) // mrna end
                .append(",").append(geneAddress[2]) // gene start position
                .append(",").append(geneAddress[3]); // gene end

        if (chromosome.length() > 0) {
            csv.append(",").append(chromosome);
        }
        int geneLength = (int) geneNode.getProperty("length");
        int mrnaLength = (int) mrnaNode.getProperty("length");
        int proteinLength = (int) mrnaNode.getProperty("protein_length");

        csv.append(",").append(strand)
                .append(",").append(geneLength) // gene length
                .append(",").append(mrnaLength)
                .append(",").append((proteinLength * 3)+3)
                .append(",").append(proteinLength)
                .append(",").append(geneNode.getId())
                .append(",").append(mrnaNode.getId()).append("\n");
    }

    /**
     * Get the functional annotation for a given mRNA node in a panproteome
     * @param mrna_node
     * @param function_set
     * @param function_node_set
     * @param gene_info_builder
     * @param groups_with_function
     * @param hm_id_str
     * @return
     * NB: the same as group_info_get_f_annotation_pangenome with the exception that this method doesn't check for BGCs
     */
    public static String group_info_get_f_annotations_panproteome(Node mrna_node, HashSet<String> function_set, HashSet<Node> function_node_set,
        StringBuilder gene_info_builder, HashSet<String> groups_with_function, String hm_id_str) {
        Pantools.logger.debug("Obtain functional annotations for mRNA node {}", mrna_node.getId());
        StringBuilder output_builder = new StringBuilder("Functional annotations: ");
        get_specific_FA_append_output(RelTypes.has_go, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_pfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_tigrfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_interpro, mrna_node, output_builder, function_set, function_node_set);
        get_phobius_signalp_append_output(mrna_node, output_builder, function_set, function_node_set);
        get_cog_append_output(mrna_node, output_builder, function_set, function_node_set);
        String functions = output_builder.toString();
        if (functions.length() > 24) {
            groups_with_function.add(hm_id_str);
            gene_info_builder.append(functions).append("\n");
        } else {
            gene_info_builder.append("No functional annotations connected to mRNA nodes\n");
        }
        return output_builder.toString();
    }

    public ArrayList<String> createHeaderOfSubgenomeIds(HashMap<String, Integer> ploidyPerGenome, boolean useSpace) {
        String space = "_";
        if (useSpace) {
            space = " ";
        }
        ArrayList<String> identifiersOfTableHeader = new ArrayList<>();
        for (String genome : ploidyPerGenome.keySet()) {
            try {
                if (skip_array[Integer.parseInt(genome)-1]) {
                    continue;
                }
            } catch (NumberFormatException ignored) {
            }
            int ploidy = ploidyPerGenome.get(genome);

            if (ploidy > 1) {
                for (int j = 0; j < ploidy; j++) {
                    identifiersOfTableHeader.add(genome + space + letters[j].toUpperCase());
                }
            }
            identifiersOfTableHeader.add(genome + space + "unphased");
        }
        return identifiersOfTableHeader;
    }


    private StringBuilder createCSVHeader() {
        StringBuilder csv = new StringBuilder("Gene id,Gene name,mRNA id,mRNA name,Genome number,Sequence number," +
                "mRNA start position,mRNA end position,gene start position,gene end position,");

        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()){
            csv.append("Chromosome,");
        }
        csv.append("Strand,Gene length (nucleotides),mRNA length (nucleotides),CDS length (nucleotides),Protein length (amino acids),Gene node identifier,mRNA node identifier\n");
        return csv;
    }

    /**
     *
     * @param homologyNode
     * @param identifiersOfTableHeader
     */
    public void tableOfSubgenomePresenceGroup(Node homologyNode, ArrayList<String> identifiersOfTableHeader) {
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return;
        }

        HashMap<String, ArrayList<String>> mrnaIdsPerPhase = new HashMap<>();
        HashMap<String, ArrayList<String>> phasesPerSequence = new HashMap<>();
        HashMap<String,HashSet<Node>> geneNodesPerPhase = new HashMap<>();
        HashMap<String, HashSet<String>> protSeqPerPhase = new HashMap<>();
        HashMap<String, String> proteinNucleotide = new HashMap<>();
        ArrayList<String> nucleotideSequences = new ArrayList<>();
        ArrayList<String> proteinSequences = new ArrayList<>();
        Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
        for (Relationship rel : relationships) {
            Node mrnaNode = rel.getEndNode();
            Node geneNode = mrnaNode.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING).getStartNode();
            int[] address = (int[]) mrnaNode.getProperty("address");
            String id = (String) mrnaNode.getProperty("protein_ID");
            String proteinSequence = (String) mrnaNode.getProperty("protein_sequence");
            if (!proteinSequences.contains(proteinSequence)){
                proteinSequences.add(proteinSequence);
            }
            String nucleotideSequence = (String) mrnaNode.getProperty("nucleotide_sequence");
            proteinNucleotide.put(proteinSequence, nucleotideSequence);
            if (!nucleotideSequences.contains(nucleotideSequence)){
                nucleotideSequences.add(nucleotideSequence);
            }

            int genomeNr = address[0];
            int sequenceNr = address[1];
            if (phasingInfoMap == null) {
                continue;
            }

            String[] phasingInfo = null;
            if (phasingInfoMap.containsKey(genomeNr + "_" + sequenceNr)) {
                phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr); // [1, B, 1B, 1_B]
            }
            String phase = "unphased";
            if (phasingInfo != null) {
                phase = phasingInfo[1];
            }
            phasesPerSequence.computeIfAbsent(proteinSequence, k -> new ArrayList<>()).add(genomeNr + " " + phase);
            mrnaIdsPerPhase.computeIfAbsent(proteinSequence + "#" + genomeNr + " " + phase, k -> new ArrayList<>()).add(id);
            geneNodesPerPhase.computeIfAbsent(genomeNr + " " + phase, k -> new HashSet<>()).add(geneNode);
            protSeqPerPhase.computeIfAbsent(genomeNr + " " + phase, k -> new HashSet<>()).add(proteinSequence);
        }

        StringBuilder alleleCountRow = new StringBuilder("allele count,,,,,,,,,");
        StringBuilder geneCountRow = new StringBuilder("gene count,,,,,,,,,");
        StringBuilder table = new StringBuilder();
        StringBuilder table2 = new StringBuilder();
        int counter = 0;
        for (String proteinSequence : proteinSequences){
            counter++;
            ArrayList<String> phases = phasesPerSequence.get(proteinSequence);

            HashSet<String> genomeNumbers = gatherGenomesFromPhases(phases);
            String nucleotideSequence = proteinNucleotide.get(proteinSequence);
            int nucleotideCounter = nucleotideSequences.indexOf(nucleotideSequence);
            nucleotideCounter++;
            table.append(counter).append(",").append(proteinSequence).append(",").append(proteinSequence.length()).append(",")
                    .append(nucleotideCounter).append(",").append(nucleotideSequence).append(",").append(nucleotideSequence.length()).append(",,").append(genomeNumbers.size()).append(",")
                    .append(phases.size()).append(",");
            table2.append(counter).append(",,,,,,,,,");
            for (String phase : identifiersOfTableHeader) {
                int occurrences = Collections.frequency(phases, phase);
                table.append(occurrences).append(",");
                if (mrnaIdsPerPhase.containsKey(proteinSequence + "#" + phase)) {
                    ArrayList<String> ids = mrnaIdsPerPhase.get(proteinSequence + "#" +phase);
                    table2.append(ids.toString().replace(", "," ").replace("[","").replace("]"," "));
                }
                table2.append(",");
            }
            table.append("\n");
            table2.append("\n");
        }

        for (String phase : identifiersOfTableHeader) {
            if (protSeqPerPhase.containsKey(phase)) {
                geneCountRow.append(geneNodesPerPhase.get(phase).size());
                alleleCountRow.append(protSeqPerPhase.get(phase).size());
            }
            alleleCountRow.append(",");
            geneCountRow.append(",");
        }

        String header = "protein number,protein sequence,protein length,gene number,nucleotide sequence,nucleotide length,,found in number of genomes,protein frequency," + identifiersOfTableHeader.toString().replace("[","").replace("]","") + "\n";
        writeStringToFile(header +table + "\n" + alleleCountRow + "\n" + geneCountRow  + "\n\n" + table2,
                WORKING_DIRECTORY + "group_info/haplotype_frequency/" + homologyNode.getId() + "_protein.csv", false, false);
    }

    private HashSet<String> gatherGenomesFromPhases(ArrayList<String> phases){
        HashSet<String> genomeNrs = new HashSet<>();
        for (String phase : phases){ // example '1 A', '1 unphased'. number is a genome number. letter is haplotype
            String[] phaseArray = phase.split(" ");
            genomeNrs.add(phaseArray[0]);
        }
        return genomeNrs;
    }

    public void tableOfSubgenomePresenceGroup2(Node homologyNode, ArrayList<String> identifiersOfTableHeader) {
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return;
        }

        HashMap<String, ArrayList<String>> mrnaIdsPerPhase = new HashMap<>();
        HashMap<String, ArrayList<String>> phasesPerSequence = new HashMap<>();
        HashMap<String, String> nucleotideProtein = new HashMap<>();
        Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
        ArrayList<String> proteinSequences = new ArrayList<>();
        ArrayList<String> nucleotideSequences = new ArrayList<>();
        HashMap<String,HashSet<Node>> geneNodesPerPhase = new HashMap<>();
        HashMap<String, HashSet<String>> nucSeqPerPhase = new HashMap<>();
        for (Relationship rel : relationships) {
            Node mrnaNode = rel.getEndNode();
            Node geneNode = mrnaNode.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING).getStartNode();
            int[] address = (int[]) mrnaNode.getProperty("address");
            String id = (String) mrnaNode.getProperty("protein_ID");
            String nucleotideSequence = (String) mrnaNode.getProperty("nucleotide_sequence");
            if (!nucleotideSequences.contains( nucleotideSequence)){
                nucleotideSequences.add(nucleotideSequence);
            }
            String proteinSequence = (String) mrnaNode.getProperty("protein_sequence");
            if (!proteinSequences.contains(proteinSequence)){
                proteinSequences.add(proteinSequence);
            }
            int genomeNr = address[0];
            int sequenceNr = address[1];
            if (phasingInfoMap == null) {
                continue;
            }

            String[] phasingInfo = null;
            if (phasingInfoMap.containsKey(genomeNr + "_" + sequenceNr)) {
                phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr);  // [1, B, 1B, 1_B]
            }
            String phase = "unphased";
            if (phasingInfo != null) {
                phase = phasingInfo[1];
            }
            phasesPerSequence.computeIfAbsent(nucleotideSequence, k -> new ArrayList<>()).add(genomeNr + " " + phase);
            mrnaIdsPerPhase.computeIfAbsent(nucleotideSequence + "#" + genomeNr + " " + phase, k -> new ArrayList<>()).add(id);
            geneNodesPerPhase.computeIfAbsent(genomeNr + " " + phase, k -> new HashSet<>()).add(geneNode);
            nucSeqPerPhase.computeIfAbsent(genomeNr + " " + phase, k -> new HashSet<>()).add(nucleotideSequence);
            nucleotideProtein.put(nucleotideSequence, proteinSequence);
        }

        StringBuilder table = new StringBuilder();
        StringBuilder alleleCountRow = new StringBuilder("allele count,,,,,,,,,");
        StringBuilder geneCountRow = new StringBuilder("gene count,,,,,,,,,");
        StringBuilder table2 = new StringBuilder();
        int counter = 0;
        for (String nucleotideSequence : nucleotideSequences) {
            counter++;
            String proteinSequence = nucleotideProtein.get(nucleotideSequence);
            int proteinCounter = proteinSequences.indexOf(proteinSequence);
            proteinCounter++; // increase by 1 because index is zero based
            ArrayList<String> phases = phasesPerSequence.get(nucleotideSequence);
            HashSet<String> genomeNumbers = gatherGenomesFromPhases(phases);
            table.append(counter).append(",").append(nucleotideSequence).append(",").append(nucleotideSequence.length()).append(",").append(proteinCounter).append(",").append(proteinSequence).append(",").append(proteinSequence.length()).append(",,").append(genomeNumbers.size()).append(",").append(phases.size()).append(",");
            table2.append(counter).append(",,,,,,,,");

            for (String phase : identifiersOfTableHeader) {
                int occurrences = Collections.frequency(phases, phase);
                table.append(occurrences).append(",");
                if (mrnaIdsPerPhase.containsKey(nucleotideSequence + "#" + phase)) {
                    ArrayList<String> ids = mrnaIdsPerPhase.get(nucleotideSequence + "#" + phase);
                    table2.append(ids.toString().replace(", "," ").replace("[","").replace("]"," "));
                }
                table2.append(",");
            }
            table.append("\n");
            table2.append("\n");
        }

        for (String phase : identifiersOfTableHeader) {
            if (nucSeqPerPhase.containsKey(phase)){
                geneCountRow.append(geneNodesPerPhase.get(phase).size());
                alleleCountRow.append(nucSeqPerPhase.get(phase).size());
            }
            alleleCountRow.append(",");
            geneCountRow.append(",");
        }

        String header = "gene number,nucleotide sequence,nucleotide length,protein number,protein sequence,protein length,,found in number of genomes,gene frequency," +
                identifiersOfTableHeader.toString().replace("[","").replace("]","") + "\n";

        writeStringToFile(header + table + "\n" + alleleCountRow + "\n" + geneCountRow  + "\n\n" + table2, WORKING_DIRECTORY + "group_info/haplotype_frequency/" + homologyNode.getId() + "_nucleotide.csv", false, false);
    }


    public String[] countSubgenomePresenceGroup(Node homologyNode) {
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return null;
        }
        String[] subgenomePresence = new String[total_genomes];
        HashMap<Integer, HashSet<String>> phasesPerGenome = new HashMap<>();
        Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
        for (Relationship rel : relationships) {
            Node mrnaNode = rel.getEndNode();
            int[] address = (int[]) mrnaNode.getProperty("address");
            int genomeNr = address[0];
            int sequenceNr = address[1];
            if (phasingInfoMap == null) {
                continue;
            }

            String[] phasingInfo = null;
            if (phasingInfoMap.containsKey(genomeNr + "_" + sequenceNr)) {
                phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr);  // [1, B, 1B, 1_B]
            }
            if (phasingInfo == null) {
                phasesPerGenome.computeIfAbsent(genomeNr, k -> new HashSet<>()).add("unphased");
            } else {
                phasesPerGenome.computeIfAbsent(genomeNr, k -> new HashSet<>()).add(phasingInfo[1]);
            }
        }

        for (int i = 1; i <= total_genomes; i++) {
            if (phasesPerGenome.containsKey(i)){
                subgenomePresence[i-1] = phasesPerGenome.get(i).toString().replace("[","").replace("]","").replace(", "," ");
            }
        }
        //System.out.println("kijk " + Arrays.toString(subgenomePresence));
        return subgenomePresence;
    }

    public HashMap<String, int[]> calculateUniqueHaplotypes(Node homologyNode) {

        HashMap<String, int[]> haplotypeCounts = new HashMap<>(); // has variety of keys

        HashMap<Integer, HashSet<String>> nucleotideSequencesGenome = new HashMap<>();
        HashMap<Integer, HashSet<String>> proteinSequencesGenome = new HashMap<>();

        HashMap<String, ArrayList<String>> nucleotideSequencesPhase = new HashMap<>();
        HashMap<String, ArrayList<String>> proteinSequencesPhase = new HashMap<>();

        HashSet<String> nucleotideSequences = new HashSet<>();
        HashSet<String> proteinSequences = new HashSet<>();
        Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);

        int counter = 0;
        for (Relationship rel : relationships) {
            Node mrnaNode = rel.getEndNode();
            int[] address = (int[]) mrnaNode.getProperty("address");
            int genomeNr = address[0];
            int sequenceNr = address[1];
            if (skip_seq_array[genomeNr-1][sequenceNr-1]) {
                continue;
            }
            counter++;
            String nucleotideSequence = (String) mrnaNode.getProperty("nucleotide_sequence");
            String proteinSequence = (String) mrnaNode.getProperty("protein_sequence");
            nucleotideSequences.add(nucleotideSequence);
            proteinSequences.add(proteinSequence);
            nucleotideSequencesGenome.computeIfAbsent(genomeNr, k -> new HashSet<>()).add(nucleotideSequence);
            proteinSequencesGenome.computeIfAbsent(genomeNr, k -> new HashSet<>()).add(proteinSequence);
            if (phasingInfoMap == null) {
                continue;
            }

            String[] phasingInfo = null;
            if (phasingInfoMap.containsKey(genomeNr + "_" + sequenceNr)) {
                phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr);  // [1, B, 1B, 1_B]
            }
            if (phasingInfo == null) {
                nucleotideSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(nucleotideSequence);
                proteinSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(proteinSequence);
            } else {
                nucleotideSequencesPhase.computeIfAbsent(genomeNr + "_" + phasingInfo[1], k -> new ArrayList<>()).add(nucleotideSequence);
                proteinSequencesPhase.computeIfAbsent(genomeNr + "_" + phasingInfo[1], k -> new ArrayList<>()).add(proteinSequence);
            }
        }
        //System.out.println(" " + counter + " \ngelukt " + nucleotideSequences.size() + " " + proteinSequences.size());
        //System.out.println(proteinSequences);
        haplotypeCounts.put("pangenome", new int[]{nucleotideSequences.size(), proteinSequences.size()});
        int[] geneHaplotypes = new int[total_genomes];
        int[] proteinHaplotypes = new int[total_genomes];
        for (int i = 1; i <= total_genomes; i++) {
            if (skip_array[i-1]){
                continue;
            }
            int nuc_counts = 0;
            if (nucleotideSequencesGenome.containsKey(i)) {
                nuc_counts = nucleotideSequencesGenome.get(i).size();
            }
            int prot_counts = 0;
            if (proteinSequencesGenome.containsKey(i)) {
                prot_counts = proteinSequencesGenome.get(i).size();
            }
            //System.out.println(i + " " + nuc_counts + " " + prot_counts );
            geneHaplotypes[i-1] = nuc_counts;
            proteinHaplotypes[i-1] = prot_counts;
        }
        haplotypeCounts.put("genes", geneHaplotypes);
        haplotypeCounts.put("proteins", proteinHaplotypes);
        return haplotypeCounts;
    }

    /**
     *
     * @param nucleotideNodes
     * @param allMrnaNucleotideNodes
     * @param address
     */
    public void find_nuc_nodes_of_hmgroup(HashSet<Node> nucleotideNodes, StringBuilder allMrnaNucleotideNodes, int[] address) {
        StringBuilder seq = new StringBuilder();
        IndexPointer start_ptr = seqLayer.locate(GRAPH_DB, GENOME_SC, INDEX_SC, address[0], address[1], address[2]-1);
        HashSet<Node> node_set = extract_sequence_return_unique_nodes(seq, start_ptr, address);
        StringBuilder node_str = new StringBuilder();
        for (Node nucleotideNode : node_set) {
            nucleotideNodes.add(nucleotideNode);
            node_str.append(nucleotideNode.getId()).append(",");
        }
        allMrnaNucleotideNodes.append(node_str.toString().replaceFirst(".$","")).append("\n\n"); // remove last character
    }

    /**
     *
     * @param seq
     * @param start_ptr
     * @param address
     * @return
     */
    public static HashSet<Node> extract_sequence_return_unique_nodes(StringBuilder seq, IndexPointer start_ptr, int[] address) {
        address[3] += K_SIZE;
        Relationship rel;
        Node neighbor, node;
        HashSet<Node> node_set = new HashSet<>();
        int[] addr = new int[]{address[0], address[1], address[2], address[3]};
        int begin = addr[2] - 1, end = addr[3] - 1;
        int len = 0, node_len, neighbor_len = 0, seq_len, position;
        String rel_name, origin = "G" + address[0] + "S" + address[1], neighbor_str , node_str;
        seq_len = end - begin + 1;
        seq.setLength(0);
        position = start_ptr.offset;
        node = GRAPH_DB.getNodeById(start_ptr.node_id);
        node_set.add(node);
        node_len = (int) node.getProperty("length");

        // Takes the part of the region lies in the first node of the path that region takes in the graph
        if (start_ptr.canonical) {
            if (position + seq_len - 1 <= node_len - 1) { // The whole sequence lies in this node
                len += seqLayer.append_fwd(seq, (String) node.getProperty("sequence"), position, position + seq_len - 1);
            } else {
                len += seqLayer.append_fwd(seq, (String) node.getProperty("sequence"), position, node_len - 1);
            }
        } else {
            if (position - (seq_len - 1) >= 0) { // The whole sequence lies in this node
                len += seqLayer.append_rev(seq, (String) node.getProperty("sequence"), position - (seq_len - 1), position);
            } else {
                len += seqLayer.append_rev(seq, (String) node.getProperty("sequence"), 0, position);
            }
        }

        if (start_ptr.canonical) {
            int real_length = node_len - (K_SIZE-1);
            real_length -= (position+1);
            len = node_len - real_length;
        } else {
            len = node_len - position;
        }

        while (len < seq_len) {
            addr[2] = (begin + len) - K_SIZE + 1;
            rel = seqLayer.get_outgoing_edge(node, origin, addr[2]);
            if (rel == null) {
                Pantools.logger.warn("This function is interrupted because there is a bug in locate() function!! Region {}", Arrays.toString(address));
                return node_set;
            }
            neighbor = rel.getEndNode();
            node_set.add(neighbor);
            rel_name = rel.getType().name();
            neighbor_len = (int) neighbor.getProperty("length");

            if (rel_name.charAt(1) == 'F') {// Enterring forward side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) {// neighbor is the last node of the path
                    len += seqLayer.append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, seq_len - len + K_SIZE - 2);
                } else {
                    len += seqLayer.append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, neighbor_len - 1);
                }
            } else { // Enterring reverse side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) {// neighbor is the last node of the pat
                    len += seqLayer.append_rev(seq, (String) neighbor.getProperty("sequence"), neighbor_len - K_SIZE - (seq_len - len) + 1, neighbor_len - K_SIZE);
                } else {
                    len += seqLayer.append_rev(seq, (String) neighbor.getProperty("sequence"), 0, neighbor_len - K_SIZE);
                }
            }
            node = neighbor;
        } // while
        node_set.add(node);
        return node_set;
    }

    /*
    Assumes copy number array does not have the first zero!
   */
    public static String create_readable_copy_number_with_skip(int[] copy_number) {
        int[] new_copy_number = new int[total_genomes];
        for (int i = 0; i < copy_number.length; ++i) {
            if (skip_array[i]) {
                continue;
            }
            new_copy_number[i] = copy_number[i];
        }
        return determine_genome_freq_absence_presence_freq(new_copy_number);
    }

    /**
     *
     * @param array
     * @return
     */
    public static String determine_genome_freq_absence_presence_freq(int[] array) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            if (array[i] > 0) {
                output.append((i+1)).append("(").append(array[i]).append("x),");
            }
        }
        String output_str = output.toString().replaceFirst(".$", "");
        return output_str;
    }

    /**
     *
     * @param node_builder
     * @param allMrnaNucleotideNodes
     * @param nucleotideNodes
     * @param mrna_nodes
     */
    public void addNodeIdentifiersToGroupInfoOutput(BufferedWriter node_builder, StringBuilder allMrnaNucleotideNodes,
                                                    HashSet<Node> nucleotideNodes, ArrayList<Node> mrna_nodes) {

        try {
            StringBuilder builder = new StringBuilder("Distinct node identifiers of all genes in group:\n");
            for (Node nucleotideNode : nucleotideNodes) {
                builder.append(nucleotideNode.getId()).append(",");
            }
            node_builder.append(builder.toString().replaceFirst(".$", "\n\n"));

            builder = new StringBuilder("mRNA node identifiers part of group:\n");
            for (Node mrna_node : mrna_nodes) {
                builder.append(mrna_node.getId()).append(",");
            }
            node_builder.append(builder.toString().replaceFirst(".$", "\n\n"));
            node_builder.append(allMrnaNucleotideNodes.toString());
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The function id from GO, TIGRFAM, PFAM or InterPRO must exactly match one of the user input IDs
     * For phobius/signalp there are 2 user input options: 'signal_peptide' and 'transmembrane'
     *
     * @param function_node_set
     * @param hm_id_str
     * @param groups_with_function_or_name
     */
    public static void check_function_output_matches(HashSet<Node> function_node_set, String hm_id_str,
                                                     HashMap<String, HashSet<String>> groups_with_function_or_name) {

        if (function_node_set.isEmpty() && (SELECTED_LABEL.equals("NONE") || SELECTED_LABEL.equals("none"))) {
            groups_with_function_or_name.computeIfAbsent("No function#function", k -> new HashSet<>()).add(hm_id_str);
        }
        ArrayList<String> function_list = new ArrayList<>(Arrays.asList(SELECTED_LABEL.split("\\s*,\\s*"))); // multiple functions can be provided
        for (Node function_node : function_node_set) {
            if (function_node.hasLabel(PHOBIUS_LABEL)) {
                String signalpep = (String) function_node.getProperty("phobius_signal_peptide");
                int transmembrane = (int) function_node.getProperty("phobius_signal_peptide");
                if (function_list.contains("signal_peptide") && signalpep.equals("yes")) {
                    groups_with_function_or_name.computeIfAbsent("signal_peptide#function", k -> new HashSet<>()).add(hm_id_str);
                }
                if (function_list.contains("transmembrane") && transmembrane > 0) {
                    groups_with_function_or_name.computeIfAbsent("transmembrane#function", k -> new HashSet<>()).add(hm_id_str);
                }
            } else if (function_node.hasLabel(SIGNALP_LABEL) && function_list.contains("signal_peptide")) {
                groups_with_function_or_name.computeIfAbsent("signal_peptide#function", k -> new HashSet<>()).add(hm_id_str);
            } else if (function_node.hasLabel(BGC_LABEL)) {
                String type = (String) function_node.getProperty("type");
                if (function_list.contains(type)) {
                    groups_with_function_or_name.computeIfAbsent(type + "#function", k -> new HashSet<>()).add(hm_id_str);
                }
            } else { // pfam, go, tigrfam, interpro
                String id = (String) function_node.getProperty("id");
                if (function_list.contains(id)) {
                    groups_with_function_or_name.computeIfAbsent(id + "#function", k -> new HashSet<>()).add(hm_id_str);
                }
            }
        }
    }

    /**
     * Get the functional annotation for a given gene node in a pangenome
     * @param gene_node
     * @param mrna_node
     * @param function_set
     * @param function_node_set
     * @param gene_info_builder
     * @param groups_with_function
     * @param hm_id_str
     * @return
     * NB: the same as group_info_get_f_annotation_panproteome with the exception that this method also checks for BGCs
     */
    public static String group_info_get_f_annotations_pangenome(Node gene_node, Node mrna_node, HashSet<String> function_set, HashSet<Node> function_node_set,
                                                      StringBuilder gene_info_builder, HashSet<String> groups_with_function, String hm_id_str) {

        StringBuilder output_builder = new StringBuilder("Functional annotations: ");
        get_specific_FA_append_output(RelTypes.has_go, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_pfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_tigrfam, mrna_node, output_builder, function_set, function_node_set);
        get_specific_FA_append_output(RelTypes.has_interpro, mrna_node, output_builder, function_set, function_node_set);
        get_bgc_append_output(gene_node, output_builder, function_set, function_node_set);
        get_phobius_signalp_append_output(mrna_node, output_builder, function_set, function_node_set);
        get_cog_append_output(mrna_node, output_builder, function_set, function_node_set);
        String functions = output_builder.toString();
        if (functions.length() > 24) {
            groups_with_function.add(hm_id_str);
            gene_info_builder.append(functions).append("\n");
        } else {
            gene_info_builder.append("No functional annotations connected to the gene and mRNA nodes\n");
        }
        return output_builder.toString();
    }

    /**
     *
     * @param hms_per_id
     */
    public void write_groups_with_specific_functions(HashMap<String, HashSet<String>> hms_per_id) {
        if (SELECTED_LABEL == null) {
            return;
        }
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/groups_with_function.txt"))) {
            for (String key : hms_per_id.keySet()) {
                if (!key.endsWith("#function")) {
                    continue;
                }
                HashSet<String> group_set = hms_per_id.get(key);
                if (group_set.size() == 1) {
                    out.write("#" + key.replace("#function","") + ", 1 group\n");
                } else {
                    out.write("#" + key.replace("#function","") + ", " + group_set.size() + " groups\n");
                }
                for (String group : group_set) {
                    out.write(group + ",");
                }
                out.write("\n");
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}group_info/groups_with_function.txt", WORKING_DIRECTORY);
        }
    }

    /**
     * Groups with have at least one gene names that matches the one given by the user --name (SELECTED_NAME)
     * @param hms_per_id
     */
    public static void write_groups_with_specific_gene_name(HashMap<String, HashSet<String>> hms_per_id) {
        if (SELECTED_NAME == null) {
            return;
        }
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "group_info/groups_with_name.txt"))) {
            for (String key : hms_per_id.keySet()) {
                if (!key.endsWith("#name")) {
                    continue;
                }
                HashSet<String> group_set = hms_per_id.get(key);
                if (group_set.size() == 1) {
                    out.write("#" + key.replace("#name","") + ", 1 group\n");
                } else {
                    out.write("#" + key.replace("#name","") + ", " + group_set.size() + " groups\n");
                }

                String groups_str = group_set.toString().replace("[","").replace("]","").replace(" ","");
                out.write(groups_str + "\n");
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}group_info/groups_with_name.txt", WORKING_DIRECTORY);
        }
    }

    /**
     *
     */
    public void printGroupInfoOutputFiles() {
        Pantools.logger.info("Output written to:");

        if (!PROTEOME) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/group_info.csv");
        }

        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/group_info.txt");
        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/functions_per_group_and_mrna.csv");
        Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/function_counts_per_group.csv");

        if (SELECTED_LABEL != null) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/groups_with_function.txt");
        }
        if (SELECTED_NAME != null) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/groups_with_name.txt");
        }
        if (Mode.contains("K-MER")) {
            Pantools.logger.info(" {}", WORKING_DIRECTORY + "group_info/group_nodes.txt");
        }
    }

}
