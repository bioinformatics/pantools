package nl.wur.bif.pantools.analysis.phylogeny;

import nl.wur.bif.pantools.utils.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.cli.validation.Constraints.MatchInteger;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create a consensus tree by combining gene trees from homology groups using ASTRAL-Pro.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "consensus_tree", sortOptions = false)
public class ConsensusTreeCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectHmGroups selectHmGroups = new SelectHmGroups(); //needs to be initialized to prevent NullPointerException

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    private static boolean alignNucleotide;
    private static boolean alignProtein;
    private static boolean alignVariants;
    private static boolean pavs;

    @ArgGroup
    AlignmentMode alignmentMode;
    private static class AlignmentMode {

        @Option(names = "--align-nucleotide")
        void setAlignNucleotide(boolean value) {
            alignNucleotide = value;
        }

        @Option(names = "--align-protein")
        void setAlignProtein(boolean value) {
            alignProtein = value;
        }

        @ArgGroup(exclusive = false)
        VariationOptions variationOptions;
    }

    private static class VariationOptions {
        @Option(names = {"-v", "--variants"}, required = true)
        void setVariants(boolean value) {
            alignVariants = value;
        }

        @Option(names = "--pavs")
        void setPavs(boolean value) {
            pavs = value;
        }
    }

    @Option(names = "--blosum")
    @MatchInteger(value = {45, 50, 62, 80, 90}, message = "match.blosum")
    int blosum;

    @Option(names = {"--polytomies"})
    boolean polytomies;

    @Option(names = {"--phasing"})
    boolean phasing;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectHmGroups);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        setAlignmentMode();
        phylogeny.consensus_tree(selectHmGroups.getHomologyGroups(), alignNucleotide, alignProtein, alignVariants, pavs, polytomies, phasing);
        return 0;
    }

    private void setAlignmentMode() { //TODO: remove msa from this command
        // Set default alignment mode if none were provided
        if (!alignNucleotide && !alignProtein && !alignVariants) {
            if (PROTEOME) {
                Pantools.logger.info("No alignment mode was provided, defaulting to protein alignment.");
                alignProtein = true;
            } else if (GraphUtils.containsVariantInformation()) {
                Pantools.logger.info("No alignment mode was provided, defaulting to variant alignment.");
                alignVariants = true;
            } else {
                Pantools.logger.info("No alignment mode was provided, defaulting to nucleotide alignment.");
                alignNucleotide = true;
            }
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignNucleotide && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Nucleotide alignment is unavailable for panproteomes");
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignVariants && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Variant alignment is unavailable for panproteomes");
        }
        // Assert that the database contains variants if the method is "variants"
        if (alignVariants) {
            GraphUtils.validateAccessions("VCF");
        }
        // Assert that the database contains trimmed protein alignments per homology-group if alignProtein is used
        if (alignProtein && !GraphUtils.containsAlignmentOutput("per-group", "prot", true)) {
            Pantools.logger.error("Missing trimmed protein alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-protein' first.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed protein alignments per homology group");
        }
        // Assert that the database contains trimmed nucleotide alignments per homology-group if alignNucleotide is used
        if (alignNucleotide && !GraphUtils.containsAlignmentOutput("per-group", "nuc", true)) {
            Pantools.logger.error("Missing trimmed nucleotide alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-nucleotide' first; preferably including --trim-using-proteins.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed nucleotide alignments per homology group");
        }
        // Assert that the database contains trimmed variant alignments per homology-group if alignVariants is used
        if (alignVariants && !GraphUtils.containsAlignmentOutput("per-group", "var", true)) {
            Pantools.logger.error("Missing trimmed variant alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-variants' first.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed variant alignments per homology group");
        }
    }

    private void setGlobalParameters() {
        THREADS = threadNumber.getnThreads();
        BLOSUM = blosum;
    }
}
