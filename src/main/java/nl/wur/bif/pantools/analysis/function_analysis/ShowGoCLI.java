package nl.wur.bif.pantools.analysis.function_analysis;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.Patterns;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;

/**
 * For a given GO term, show the child terms, all parent terms higher in the hierarchy, and connected mRNA nodes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "show_go", sortOptions = false)
public class ShowGoCLI implements Callable<Integer> {

    @Spec static CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup(multiplicity = "1")
    Identifiers identifiers;
    private static class Identifiers {
        @Option(names = "--functions", required = true)
        void setFunctions(String value) {
            functions = Arrays.asList(value.toUpperCase().split(","));
        }
        @Patterns(regexp = "GO:[0-9]+", message = "{patterns.go}")
        List<String> functions;

        @Option(names = {"-n", "--nodes"}, required = true)
        void setNodes(String value) {
            nodes = stringToIntegerList(value);
        }
        @Size(min = 1, message = "{size.empty.node}")
        List<Integer> nodes;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, identifiers);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        GraphUtils.validateHomologyGroups();

        functionalAnnotations.show_go();
        return 0;
    }

    private void setGlobalParameters() {
        if (identifiers.nodes != null) {
            NODE_ID = identifiers.nodes.toString().replaceAll("[\\[\\]]", "");
            if (NODE_ID.matches("^[0-9]*$") && NODE_ID.length() > 0) NODE_ID_long = Long.parseLong(NODE_ID);
        }
        if (identifiers.functions != null) SELECTED_NAME = identifiers.functions.toString().replaceAll("[\\[\\]]", "");
    }
}
