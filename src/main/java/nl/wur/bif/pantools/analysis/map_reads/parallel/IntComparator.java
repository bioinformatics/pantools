package nl.wur.bif.pantools.analysis.map_reads.parallel;

import java.util.Comparator;

/**
 * Implements a comparator for integers
 */
public class IntComparator  implements Comparator<Integer> {
    @Override
    public int compare(Integer v1, Integer v2) {
        return v1.compareTo(v2);
    }
}
