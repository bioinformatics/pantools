package nl.wur.bif.pantools.analysis.gene_classification;

import nl.wur.bif.pantools.analysis.classification.Classification;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.*;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.*;
import static nl.wur.bif.pantools.utils.Utils.*;
import static nl.wur.bif.pantools.utils.GraphUtils.*;
import static nl.wur.bif.pantools.utils.Globals.*;

public class GeneClassification {

    // still uses some global variables:
    // skip class
    // longest_transcripts
    // compare_sequences
    // core_threshold
    // and more ...?

    private AtomicInteger setPropertyCounter;
    private HashMap<String, Integer> subgenomeCount = new HashMap<>();
    public HashMap<String, HashSet<String>> haplotypesWithinChromosome;
    PhasedFunctionalities pf = new PhasedFunctionalities();
    // key is homology node, value is []
    // position 1: category = "core, accessory or unique"
    // position 2: single copy ortholog ??
    // position 2: (optional) phenotype category = "exclusive, shared or specific"
    // position 3: (optional) presence in haplotype phase = can be multiple "A B C D E F G unphased"

    private ArrayList<String> genomeList;

    /**
     * Main method for gene_classification
     * Creates all files in <database dir>/gene_classification
     * @param pavs whether to use PAV information
     * NB: When variation is included, phenotype related calculations can NOT be used.
     * NB: Prepare CIRCOS input file when the `--mode 1` argument is included (Unavailable for the panproteome). Currently disabled!
    */
    public void geneClassification(boolean pavs) throws IOException {
        Pantools.logger.info("Calculating CORE, UNIQUE, and ACCESSORY homology groups.");

        try (Transaction tx = GRAPH_DB.beginTx();
             ResourceIterator<Node> nodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) { // start database transaction
            Node pangenome_node = nodes.next(); // retrieve pangenome node
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // check which version of homology grouping is active
            classification.update_skip_array_based_on_grouping(pangenome_node);
            retrieve_phenotypes(); // fills geno_pheno_map, phenotype_map and phenotype_threshold when a --phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
            this.genomeList = Classification.create_genome_list(pavs);
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database error");
        }

        createDirectory("gene_classification/distances_for_tree", true);
        createDirectory("gene_classification/group_identifiers/genome", true);

        createDirectory(WORKING_DIRECTORY + "gene_classification/upset/output", false);
        createDirectory(WORKING_DIRECTORY + "gene_classification/upset/input", false);

        longest_transcripts = true;

        skip.retrieveSelectedGenomes(); // sets selectedGenomes (not used here but needed for retrieveSelectedSequences)
        skip.retrieveSelectedSequences(0, pavs); // sets selectedSequences
        pf.preparePhasedGenomeInformation(PHASED_ANALYSIS, selectedSequences); // sets phasingInfoMap

        HashMap<String, StringBuilder> groupsPerGenomeCombination = new HashMap<>();
        HashMap<String, HashSet<String>> seqCombiWithinGenomeCombi = new HashMap<>();
        HashMap<String, StringBuilder> groupsPerSequenceCombination = new HashMap<>();

        ArrayList<Node> singleCopyGroupNodes = new ArrayList<>();
        ArrayList<Node> phasedSingleCopyGroupNodes = new ArrayList<>();

        HashMap<String, HashSet<Long>> uniqueGroupsPerGenome = new HashMap<>(); // homology group ids per genome for for singletons
        HashMap<String, int[]> sharedGenesBetweenSequences = new HashMap<>();

        HashMap<Integer, StringBuilder> accessoryGroupsPerGenomeCount = new HashMap<>(); // key is total number of genomes,
        HashMap<String, Integer> groupSizeFrequency = new HashMap<>();

        HashMap<String, ArrayList<Node>> groupsPerClass = new HashMap<>(); // holds homology_group nodes 7 possible keys: core, accessory, unique, phenotypeValue + "#total" / "#shared" / "#exclusive" "#specific"

        HashMap<String, ArrayList<Node>> phenotype_cnv = new HashMap<>();// key is phenotype
        HashMap<String, Integer> pheno_disrupt_count_map = new HashMap<>();
        HashMap<Double, ArrayList<String>> pheno_xsquare_map = new HashMap<>();

        StringBuilder pheno_cnv_builder = new StringBuilder();
        StringBuilder additional_copies = new StringBuilder();

        //HashMap<String, ArrayList<Long>> nodes_per_phased_class_map = new HashMap<>();
        BufferedWriter genomeUpsetInput = startUpsetBetweenGenomes();
        HashMap<String, BufferedWriter> groupsPerGenome = startGroupsPerGenomeWriters();

        int groupCounter = 0;
        setPropertyCounter = new AtomicInteger(0);
        int write_threshold = 1000000 / genomeList.size(); // prevents additional_copies from becoming to large
        StringBuilder disrupted_builder = new StringBuilder();

        HashMap<String, Integer> ploidyPerGenome = null;
        HashMap<String, ArrayList<String>> haplotypeLettersPerGenome = null;
        ArrayList<Integer> chromosomeNumbers = null;
        HashMap<String, ArrayList<String>> sequencesPerChromosomeGenome = null;
        HashMap<Integer, ArrayList<String>> sequencesPerChromosomePangenome = null;
        BufferedWriter[] upsetWriters = null;
        HashMap<String, ArrayList<Integer>> chromosomesPerGenome = null;
        BufferedWriter[] phasing_count_writers = null;
        HashMap<String, Integer> phasing_count_position = null;
        HashMap<String, int[]> sharedGenesBetweenSubgenomes = null;
        HashMap<String, ArrayList<String>> groups_per_phased_chrom = null;
        HashMap<Integer, ArrayList<Long>> groupsPerSubGenomeCount = null; // key is total number of subgenomes

        if (PHASED_ANALYSIS) {
            skip.retrieveSelectedSubgenomes();
            compare_sequences = true;
            deleteDirectory("gene_classification/haplotype_presence", true);
            createDirectory("gene_classification/haplotype_presence", true);
            deleteDirectory("gene_classification/other_sequence_counts", true);
            createDirectory("gene_classification/other_sequence_counts", true);
            haplotypeLettersPerGenome = createHaplotypeLettersPerGenome();
            ploidyPerGenome = pf.determinePloidyPerGenome(selectedSequences, genomeList);
            chromosomeNumbers = determineChromosomeNumber();
            sequencesPerChromosomeGenome = createSequencesPerChromosomeGenome();
            sequencesPerChromosomePangenome = createSequencesPerChromosomeMapInteger();
            upsetWriters = startUpsetWriters(chromosomeNumbers, sequencesPerChromosomePangenome);
            chromosomesPerGenome = createChromosomesPerGenome();
            phasing_count_writers = startPhasingCountWriters(chromosomesPerGenome);
            phasing_count_position = createPhasingCountMap(chromosomesPerGenome);
            sharedGenesBetweenSubgenomes = createSharedGenesBetweenSubgenomesMap();
            groups_per_phased_chrom = new HashMap<>();
            groupsPerSubGenomeCount = new HashMap<>();
        }

        if (compare_sequences) {
            Pantools.logger.info("--sequence was included. Comparing sequences to each other. Sequences in analysis: " + selectedSequences.size());
            if (selectedSequences.size() > 10000) {
                Pantools.logger.warn("A lot of sequences were selected! Please consider making a selection through --selection-file");
            }
        } else {
            Pantools.logger.info("No --sequence argument was included. Only comparing genomes.");
        }

        if (PHENOTYPE != null) {
            Pantools.logger.info("The '" + PHENOTYPE + "' phenotype is included. Finding phenotype shared/specific mRNA's.");
        } else {
            Pantools.logger.info("No --phenotype was included. No phenotype specific genes can be identified.");
        }

        HashMap<String, String> homologyTableLines = new HashMap<>(); // key is homologyNode.getId()  with optional "_phased"
        HashMap<String, int[]> sharedGenesBetweenGenomes = initializeSharedGenesBetweenGenomesMap();
        HashMap<String, int[]> core_access_uni_map = initializeCoreAccessUniMap();

        HashMap<String, Integer> mrnasWithSubgenomePresence = new HashMap<>(); // key is combination of genomeNr with number of subgenomes
        HashMap<String, Integer> mrnaWithoutHomology = new HashMap<>(); // key is genomeNr, value is number of mRNAs (per genome) that do not cluster to any other mRNA
        for (String genome : genomeList) {
            mrnasWithSubgenomePresence.put(genome, 0);
            mrnaWithoutHomology.put(genome, 0);
        }

        core_unique_thresholds_for_classification(genomeList, "mRNAs/proteins"); // sets core_threshold and unique_threshold

        ArrayList<Node> homologyNodes = retrieveHomologyNodes();
        ArrayList<Node> updatedHomologyNodes = new ArrayList<>(); // holds only groups in current genome/sequence selection
        int totalHomologyGroups;
        Transaction tx = GRAPH_DB.beginTx(); // start database transaction
        try {
            proLayer.find_longest_transcript_per_gene(false, 0);
            totalHomologyGroups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
            for (Node homologyNode : homologyNodes) {
                if (groupCounter == 1 || (groupCounter % 100 == 0 && groupCounter != 0) || groupCounter == totalHomologyGroups) {
                    printProgressBar(totalHomologyGroups, groupCounter, "homology groups", false);
                }

                int[][] geneCopiesPerSequenceArray = getGeneCopiesPerSequence(homologyNode);
                if (setPropertyCounter.get() >= 500) { // written "copy_number_genome_" + genome number property 500 times
                    tx.success();
                    tx.close();
                    setPropertyCounter.set(0);
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                }

                int[] copy_number_array;
                HashMap<String, Integer> geneCopiesPerSequenceMap = null;
                if (!compare_sequences) {
                    copy_number_array = getCopyNumberArray(homologyNode, pavs);
                } else { // the original copy_number_array stored in homology nodes could be inaccurate when sequences are skipped
                    if (pavs) {
                        //TODO: this block doesn't work with PAVs!
                        Pantools.logger.warn("PAVs are not supported in combination with --sequence. Skipping PAVs.");
                    }
                    geneCopiesPerSequenceMap = convertGeneCopiesArrayToMap(geneCopiesPerSequenceArray);
                    copy_number_array = createCorrectedCopyNumberArray(geneCopiesPerSequenceMap);
                    countSubgenomes(geneCopiesPerSequenceMap, sequencesPerChromosomeGenome);
                }

                HashMap<String, int[]> phenotype_min_max = new HashMap<>(); // initializePhenotypeMinMax();
                ArrayList<String> genesPerGenome = countGenesPerGenome(copy_number_array, genomeList);
                TreeSet<String> presentGenomes = new TreeSet<>(genesPerGenome);
                boolean unique = false; // this has to be known already so 'informative' genes can be added in a later function
                if (genesPerGenome.isEmpty()) { // due to skipped genomes/sequences, no proteins are present in the group
                    totalHomologyGroups--;
                    continue;
                } else if (genesPerGenome.size() == 1) { // genes present in 1 genome are not informative for the gene distance tree
                    unique = true;
                    // add one to the number of genes in the group for the genome that is present
                    String genome = genesPerGenome.get(0);
                    mrnaWithoutHomology.putIfAbsent(genesPerGenome.get(0), 0);
                    mrnaWithoutHomology.put(genome, mrnaWithoutHomology.get(genome) + 1);
                }

                if (genesPerGenome.size() == presentGenomes.size() && genesPerGenome.size() >= genomeList.size()) {
                    singleCopyGroupNodes.add(homologyNode);
                }

                groupCounter++;
                updatedHomologyNodes.add(homologyNode);

                StringBuilder cnv_builder = new StringBuilder();
                groupSizeFrequency.merge(genesPerGenome.size() + "_mRNAs", 1, Integer::sum); // number of genes in group
                groupSizeFrequency.merge(presentGenomes.size() + "_genomes", 1, Integer::sum); // number of genomes in group

                for (String genome : presentGenomes) {
                    groupsPerGenome.get(genome).write(homologyNode.getId() + ",");
                }
                updateGenomeUpsetFile(homologyNode.getId(), copy_number_array, genomeUpsetInput);
                HashMap<String, Integer> phenotypeGenomeCounts = countPhenotypeGenomes(copy_number_array, phenotype_min_max);

                String presentGenomesStr = presentGenomes.toString().replace("[", "").replace("]", "");
                groupsPerGenomeCombination.computeIfAbsent(presentGenomesStr, k -> new StringBuilder()).append(homologyNode.getId() + " ");
                if (compare_sequences) {
                    assert geneCopiesPerSequenceMap != null;
                    ArrayList<String> present_sequences = new ArrayList<>(geneCopiesPerSequenceMap.keySet());
                    String presentSequencesStr = present_sequences.toString().replace("[", "").replace("]", "");
                    seqCombiWithinGenomeCombi.computeIfAbsent(presentGenomesStr, k -> new HashSet<>()).add(presentSequencesStr);
                    groupsPerSequenceCombination.computeIfAbsent(presentSequencesStr, k -> new StringBuilder()).append(homologyNode.getId() + " ");
                }

                if (PHASED_ANALYSIS) {
                    updateUpsetGClassInput(upsetWriters, geneCopiesPerSequenceMap, sequencesPerChromosomePangenome, chromosomeNumbers, homologyNode.getId());
                    String phasedHomologyTableLine = createPhasedHomologyTableLine(selectedSubgenomes, ploidyPerGenome);
                    homologyTableLines.put(homologyNode.getId() + "_phased", phasedHomologyTableLine);
                    boolean sco_group_phased = determineSingleCopyGroupWhenPhased(geneCopiesPerSequenceMap, ploidyPerGenome, haplotypeLettersPerGenome);
                    if (sco_group_phased) {
                        phasedSingleCopyGroupNodes.add(homologyNode);
                    }
                    for (String chromosome_key : haplotypesWithinChromosome.keySet()) {
                        groups_per_phased_chrom.computeIfAbsent(chromosome_key + "#" + haplotypesWithinChromosome.get(chromosome_key).size(), k -> new ArrayList<>()).add(homologyNode.getId() + "");
                    }
                    summarize_phasing_counts(phasing_count_position, homologyNode.getId(), phasing_count_writers);
                }

                // should be (temporarily) disabled for high number of apple sequences
                updateCNV(copy_number_array, cnv_builder);
                updateSharedGenesBetweenGenomes(copy_number_array, sharedGenesBetweenGenomes, unique);
                updateSharedGenesBetweenSequences(sharedGenesBetweenSequences, geneCopiesPerSequenceMap, unique);

                ArrayList<String> phenotypeCategories = findPhenotypeSharedSpecificExclusive(phenotypeGenomeCounts, homologyNode,
                        pheno_disrupt_count_map, phenotype_min_max, disrupted_builder, groupsPerClass);

                String copyNumberStr = Arrays.toString(copy_number_array).replace("[", "").replace("]", "").replace(" ", "");
                String phenotypeInfoStr = phenotypeCategories.toString().replace("[", "").replace("]", "").replace(",", ";");
                if (phenotypeInfoStr.equals("")) {
                    phenotypeInfoStr = "-";
                }

                homologyTableLines.put(homologyNode.getId() + "", copyNumberStr + "#" + phenotypeInfoStr);
                fisherExactOnPhenotypes(phenotypeGenomeCounts, pheno_xsquare_map, homologyNode);
                String groupCategory = determineGroupCategory(presentGenomes);
                if (groupCategory.equals("unique")) { // unique
                    for (String genome : presentGenomes) {
                        uniqueGroupsPerGenome.computeIfAbsent(genome, k -> new HashSet<>()).add(homologyNode.getId());
                    }
                } else { // core and accessory
                    if (groupCategory.equals("accessory")) {
                        accessoryGroupsPerGenomeCount.computeIfAbsent(presentGenomes.size(), k -> new StringBuilder()).append(homologyNode.getId() + ",");
                    }
                    find_additional_pheno_gene_copies(phenotype_min_max, phenotype_cnv, groupCategory, homologyNode, pheno_cnv_builder);
                    add_additional_copies(cnv_builder, additional_copies, groupCategory, homologyNode.getId());
                }
                groupsPerClass.computeIfAbsent(groupCategory, k -> new ArrayList<>()).add(homologyNode);
                increase_core_access_uni_map2(copy_number_array, core_access_uni_map, groupCategory, geneCopiesPerSequenceMap);
                create_gclass_additional_copies(additional_copies, groupCounter, false, write_threshold);

                if (PHASED_ANALYSIS) {
                    assert geneCopiesPerSequenceMap != null;
                    assert groupsPerSubGenomeCount != null;
                    HashMap<String, Integer> geneCountPerSubgenome = countGenesPerSubgenome(geneCopiesPerSequenceMap);
                    int subgenomeCount = countTotalSubgenomesInGroup(geneCountPerSubgenome, groupSizeFrequency, groupCategory);
                    groupsPerSubGenomeCount.computeIfAbsent(subgenomeCount, k -> new ArrayList<>()).add(homologyNode.getId());
                    countGenesBetweenSubgenomes(geneCountPerSubgenome, sharedGenesBetweenSubgenomes);
                    countGenesOnTotalSubgenomes(geneCountPerSubgenome, mrnasWithSubgenomePresence);
                }

                if (groupCounter % write_threshold == 0) {
                    additional_copies = new StringBuilder();
                }
            }

            if (groupCounter == 0) {
                return;
            }
            tx.success(); // transaction successful, commit changes
        } finally {
            tx.close();
        }
        printProgressBar(totalHomologyGroups, groupCounter, "homology groups", false);

        Pantools.logger.info("Start extracting and counting sequences.");
        HashMap<String, double[]> alleleCountStatistics = null;
        HashMap<String, double[]> alleleCountStatisticsLT = null;
        HashMap<String, double[]> geneCopyStatisticsMap = null;
        HashMap<String, double[]> geneCopyStatisticsMapLT = null;

        if (!PROTEOME) {
            alleleCountStatistics = alleleStatistics(updatedHomologyNodes, false, true);
            alleleCountStatisticsLT = alleleStatistics(updatedHomologyNodes, true, true);
            geneCopyStatisticsMap = alleleStatistics(updatedHomologyNodes, false, false);
            geneCopyStatisticsMapLT = alleleStatistics(updatedHomologyNodes, true, false);
        }

        genomeUpsetInput.close();

        createSkippedInfoFile();
        createTheHomologyTable(groupsPerClass, singleCopyGroupNodes, homologyTableLines);
        createThePhasedHomologyTable(homologyTableLines, selectedSubgenomes, ploidyPerGenome);
        writeSingleCopyGroups(singleCopyGroupNodes,"");
        writeMlsaSuggestions(singleCopyGroupNodes);
        writeAllHomologyGroupNodes(updatedHomologyNodes);
        find_significant_pheno_groups(pheno_xsquare_map);
        writeGroupSizeFrequencies(groupSizeFrequency, (genomeList.size() * 1000), groupCounter);

        writePhenotypeGroupIdentifiers(groupsPerClass, "shared");
        writePhenotypeGroupIdentifiers(groupsPerClass, "specific");
        writePhenotypeGroupIdentifiers(groupsPerClass, "exclusive");
        int uniqueGroupCount = writeUniqueGroups(uniqueGroupsPerGenome);
        create_gclass_additional_copies(additional_copies, groupCounter, true, write_threshold);

        writeGroupsPerGenomeSequenceCombi(groupsPerGenomeCombination, seqCombiWithinGenomeCombi, groupsPerSequenceCombination);
        createSharedGenesMatrices(sharedGenesBetweenGenomes, genomeList, "genome", null);
        createGeneDistanceRscript("genome");

        if (compare_sequences) {
            createSharedGenesMatrices(sharedGenesBetweenSequences, new ArrayList<>(selectedSequences), "sequence", null);
            createGeneDistanceRscript("sequence");
        }

        writeAccessoryGroups(accessoryGroupsPerGenomeCount);
        int coreGroupCount = writeCoreHomologyGroups(groupsPerClass);
        create_gclass_syntelog_output();

        if (PHENOTYPE != null) {
            create_gclass_phenotype_cnv(pheno_cnv_builder);
            create_gclass_phenotype_additional_copies(phenotype_cnv);
            createPhenotypeOverview(pheno_disrupt_count_map, phenotype_cnv, groupsPerClass);
            create_gclass_pheno_disrupted(disrupted_builder);
        }

        HashMap<String, ArrayList<Integer>> subgenomeCountsPerGenome = null;
        int highestPloidyLevel = 0;
        if (PHASED_ANALYSIS) {
            writeSingleCopyGroups(phasedSingleCopyGroupNodes, "subgenome_");
            assert upsetWriters != null;
            closeUpsetWriters(upsetWriters);
            writeGroupsPerSubgenomeCount(groupsPerSubGenomeCount);
            assert ploidyPerGenome != null;
            highestPloidyLevel = getHighestPloidylevel(ploidyPerGenome);
            subgenomeCountsPerGenome = subgenomePresenceFromHomologyGroups(updatedHomologyNodes);
            createSubgenomePresenceCSV(subgenomeCountsPerGenome, highestPloidyLevel, core_access_uni_map, mrnasWithSubgenomePresence);
            countGeneHaplotypes(9999, homologyNodes);
            createGroupsPersubgenomePresence(homologyNodes, ploidyPerGenome);
            assert phasing_count_writers != null;
            closePhasingCountWriters(phasing_count_writers);
            createSharedGenesMatrices(sharedGenesBetweenSubgenomes, new ArrayList<>(selectedSubgenomes), "subgenome", ploidyPerGenome);
            createGeneDistanceRscript("subgenome");
        }

        createGeneClassificationOverviewTxtCsv(groupCounter, singleCopyGroupNodes.size(), phasedSingleCopyGroupNodes.size(), core_access_uni_map,
                coreGroupCount, uniqueGroupCount, subgenomeCountsPerGenome, mrnaWithoutHomology,
                alleleCountStatistics, alleleCountStatisticsLT, geneCopyStatisticsMap, geneCopyStatisticsMapLT, highestPloidyLevel);

        writeGroupsPerChromosomeHaplotypes(chromosomesPerGenome, sequencesPerChromosomeGenome, groups_per_phased_chrom);

        if (genomeList.size() < 11) {  // don't bother making upset plots with many genomes. so much combinations they become uninterpretable
            ArrayList<String> genomeCombinations = createGenomeCombinations();
            createChromosomeUpsetRscript(chromosomeNumbers, sequencesPerChromosomePangenome, genomeCombinations, ploidyPerGenome);
            createGenomeCombinationsUpsetInput(chromosomeNumbers, genomeCombinations);
        }

        closeGroupsPerGenomeWriters(groupsPerGenome);
        printOutputFiles();
    }

    /**
     * Count the number of mRNA nodes for a given homology group node
     * @param hmNode homology group node
     * @param pavs whether to use PAV information
     * @return an array with the copy number per genome, ordered by the genomeList
     */
    private int[] getCopyNumberArray(Node hmNode, boolean pavs) {
        Pantools.logger.debug("Getting copy number for homology group {}", hmNode.getId());

        int[] copyNumbers = new int[genomeList.size()];

        // loop over all mRNA nodes attached to the homology group
        for (Relationship rel : hmNode.getRelationships(Direction.OUTGOING, RelTypes.has_homolog)) {
            Node mrnaNode = rel.getEndNode();
            String mrnaGenome = String.valueOf((int) mrnaNode.getProperty("genome"));

            if (genomeList.contains(mrnaGenome)) {
                int index = genomeList.indexOf(mrnaGenome);
                copyNumbers[index]++;
                Pantools.logger.debug("{} in genome {} has {} copies.", mrnaNode.getProperty("protein_ID"), mrnaGenome, copyNumbers[index]);
            } else {
                Pantools.logger.debug("Genome {} not found in genome list.", mrnaGenome);
            }

            if (pavs) {
                // loop over all variant nodes attached to the mRNA node
                for (Relationship rel2 : mrnaNode.getRelationships(Direction.OUTGOING, RelTypes.has_variant)) {
                    Node variantNode = rel2.getEndNode();
                    String variantGenome = (String) variantNode.getProperty("id");
                    Pantools.logger.trace("Found variant {} for sample {}", variantNode.getProperty("protein_ID"), variantNode.getProperty("sample"));

                    // only add variant count if it is present
                    if (variantNode.hasProperty("present")) {
                        if ((boolean) variantNode.getProperty("present")) {
                            Pantools.logger.trace("Variant is present.");
                            int index2 = genomeList.indexOf(variantGenome);
                            copyNumbers[index2]++;
                        } else {
                            Pantools.logger.trace("Variant is absent.");
                        }
                    } else {
                        Pantools.logger.trace("Variant does not have PAV information.");
                    }
                }
            }
        }

        Pantools.logger.debug("Copy numbers: {}", Arrays.toString(copyNumbers));

        return copyNumbers;
    }

    /**
     *
     */
    private void updateGenomeUpsetFile(long homologyNodeId, int[] copy_number_array, BufferedWriter genomeUpsetInput) throws IOException {
        StringBuilder upsetLine1 = new StringBuilder(homologyNodeId + ",");
        for (int genomeNr = 0; genomeNr < genomeList.size(); genomeNr++) {
            if (copy_number_array[genomeNr] > 0) {
                upsetLine1.append( "1,");
            } else {
                upsetLine1.append( "0,");
            }
        }
        genomeUpsetInput.write(upsetLine1.toString().replaceFirst(".$","") + "\n");
    }

    /**
     *
     * @param cnv_builder
     * @param additional_copies
     * @param gene_class
     * @param hm_id
     */
    private void add_additional_copies(StringBuilder cnv_builder, StringBuilder additional_copies, String gene_class, long hm_id) {
        String cnv = cnv_builder.toString();
        if (cnv.length() > 3) { // these genomes has an extra copy of a gene
            String[] cnv_array = cnv.split("\n");
            for (String line : cnv_array) {
                additional_copies.append(hm_id).append(",").append(gene_class).append(",").append(line).append("\n");
            }
        }
    }

    /**
     * count_haplotypes() haplotype_counts
     */
    private void countGeneHaplotypes(int maximumCopyNumber, ArrayList<Node> homologyNodes) {
        if (PROTEOME) {
            return;
        }

        createDirectory("gene_classification", true);
        HashMap<Integer, Integer> groupSizeCountsMap = new HashMap<>();
        HashMap<String, Integer> haplotypeCountsMap = new HashMap<>(); // key is haplotype count + "_nucleotide" or "_protein"

        StringBuilder groupSizeVsHaplotypes = new StringBuilder("set,group_size,haplotypes\n");
        StringBuilder groupSizeVsProteinHaplotypes = new StringBuilder("set,group_size,haplotypes\n");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int groupCounter = 0;
            for (Node homologyNode : homologyNodes) {
                groupCounter++;
                if (groupCounter % 100 == 0 || groupCounter < 11 || groupCounter == homologyNodes.size()) {
                    System.out.print("\r Counting distinct haplotypes: group " + groupCounter + "/" + homologyNodes.size() + "   ");
                }
                int[] copyNumber = (int[]) homologyNode.getProperty("copy_number");
                HashSet<String> nucleotideSequencesPangenome = new HashSet<>();
                HashSet<String> proteinSequencesPangenome = new HashSet<>();

                HashMap<Integer, ArrayList<String>> nucleotideSequencesGenome = new HashMap<>();
                HashMap<Integer, ArrayList<String>> proteinSequencesGenome = new HashMap<>();
                HashMap<String, ArrayList<String>> nucleotideSequencesPhase = new HashMap<>();
                HashMap<String, ArrayList<String>> proteinSequencesPhase = new HashMap<>();
                int groupSize = 0;
                Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
                for (Relationship rel : relationships) {
                    Node mrnaNode = rel.getEndNode();
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    int genomeNr = address[0];
                    int sequenceNr = address[1];
                    if (skip_seq_array[genomeNr-1][sequenceNr-1]) {
                        continue;
                    }
                    if (copyNumber[genomeNr] > maximumCopyNumber) {
                        continue;
                    }
                    groupSize++;
                    String nucleotideSequence = (String) mrnaNode.getProperty("nucleotide_sequence");
                    String proteinSequence = (String) mrnaNode.getProperty("protein_sequence");
                    nucleotideSequencesGenome.computeIfAbsent(genomeNr, k -> new ArrayList<>()).add(nucleotideSequence);
                    nucleotideSequencesPangenome.add(nucleotideSequence);
                    proteinSequencesPangenome.add(proteinSequence);
                    proteinSequencesGenome.computeIfAbsent(genomeNr, k -> new ArrayList<>()).add(proteinSequence);
                    if (PHASED_ANALYSIS && phasingInfoMap.containsKey(genomeNr + "_" + sequenceNr)) {
                        String[] phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr);  // [1, B, 1B, 1_B]
                        if (phasingInfo == null) {
                            nucleotideSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(nucleotideSequence);
                            proteinSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(proteinSequence);
                        } else {
                            nucleotideSequencesPhase.computeIfAbsent(genomeNr + phasingInfo[1], k -> new ArrayList<>()).add(nucleotideSequence);
                            proteinSequencesPhase.computeIfAbsent(genomeNr + phasingInfo[1], k -> new ArrayList<>()).add(proteinSequence);
                        }
                    }
                }
                if (groupSize == 0) {
                    continue;
                }
                groupSizeVsHaplotypes.append("pangenome,").append(groupSize).append(",").append(nucleotideSequencesPangenome.size()).append("\n");
                groupSizeVsProteinHaplotypes.append("pangenome,").append(groupSize).append(",").append(proteinSequencesPangenome.size()).append("\n");

                for (int genomeNr : proteinSequencesGenome.keySet()) {
                    ArrayList<String> proteinSequences = proteinSequencesGenome.get(genomeNr);
                    ArrayList<String> nucleotideSequences = nucleotideSequencesGenome.get(genomeNr);
                    HashSet<String> distinctProtSequences = new HashSet<>(proteinSequences);
                    HashSet<String> distinctNucSequences = new HashSet<>(nucleotideSequences);
                    groupSizeVsHaplotypes.append("G" + genomeNr + "," + nucleotideSequences.size() + "," + distinctNucSequences.size() + "\n");
                    groupSizeVsProteinHaplotypes.append("G" + genomeNr + "," + nucleotideSequences.size() + "," + distinctProtSequences.size() + "\n");

                    groupSizeCountsMap.merge(nucleotideSequences.size(), 1, Integer::sum);
                    haplotypeCountsMap.merge(distinctNucSequences.size() + "_nucleotide", 1, Integer::sum);
                    haplotypeCountsMap.merge(distinctProtSequences.size() + "_protein", 1, Integer::sum);
                }
            }
            tx.success(); // transaction successful, commit changes
        }

        StringBuilder bothCounts = new StringBuilder("size,frequency,type\n");
        for (int i = 1; i <= 1000; i++) {
            if (!groupSizeCountsMap.containsKey(i) && !haplotypeCountsMap.containsKey(i + "")) {
                continue;
            }

            if (groupSizeCountsMap.containsKey(i)) {
                bothCounts.append(i).append(",").append(groupSizeCountsMap.get(i)).append(",group_size\n");
            }

            if (haplotypeCountsMap.containsKey(i + "_nucleotide")) {
                bothCounts.append(i).append(",").append(haplotypeCountsMap.get(i + "_nucleotide")).append(",nuc_haplotypes\n");
            }

            if (haplotypeCountsMap.containsKey(i + "_protein")) {
                bothCounts.append(i).append(",").append(haplotypeCountsMap.get(i + "_protein")).append(",prot_haplotypes\n");
            }
        }

        writeStringToFile(bothCounts.toString(), "gene_classification/haplotype_groupsize_counts.csv", true, false);
        writeStringToFile(groupSizeVsHaplotypes.toString(), "gene_classification/group_vs_haplotypes.csv", true, false);
        writeStringToFile(groupSizeVsProteinHaplotypes.toString(), "gene_classification/group_vs_prot_haplotypes.csv", true, false);
    }

    private int getHighestPloidylevel(HashMap<String, Integer> ploidyPerGenome) {
        int highest = 0;
        for (String genome : ploidyPerGenome.keySet()) {
            int ploidy = ploidyPerGenome.get(genome);
            if (ploidy > highest) {
                highest = ploidy;
            }
        }
        return highest;
    }

    /**
     * @param homologyNodes
     */
    private void createGroupsPersubgenomePresence(ArrayList<Node> homologyNodes, HashMap<String, Integer> ploidyPerGenome) {
        if (!PHASED_ANALYSIS) {
            return;
        }
        HashMap<String, ArrayList<String>> subgenomeCountsPerGenome = new HashMap<>();
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node homologyNode : homologyNodes) {
                counter ++;
                if (counter % 100 == 0) {
                    System.out.print("\r Counting group subgenome presence: " + counter + "/" + homologyNodes.size() + "   ");
                }
                Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
                HashMap<Integer, HashSet<String>> presentSubgenomesPerGenome = new HashMap<>();
                for (Relationship rel : relationships) {
                    Node mrnaNode = rel.getEndNode();
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    if (skip_seq_array[address[0] - 1][address[1] - 1]) {
                        continue;
                    }
                    String[] phasingInfo = phasingInfoMap.get(address[0] + "_" + address[1]); // example [1, D, 1D, 1_D]
                    String phase = "unphased";
                    if (phasingInfo != null) {
                        phase = phasingInfo[1];
                    }
                    presentSubgenomesPerGenome.computeIfAbsent(address[0], k -> new HashSet<>()).add(phase);
                }

                // done counting subgenomes in current homology group. add to
                for (int genomeNr : presentSubgenomesPerGenome.keySet()) {
                    HashSet<String> subgenomeIdentifiers = presentSubgenomesPerGenome.get(genomeNr);
                    int size =  subgenomeIdentifiers.size();
                    if (subgenomeIdentifiers.contains("unphased")) {
                        size--;
                    }
                    subgenomeCountsPerGenome.computeIfAbsent(genomeNr + "_" + size, k -> new ArrayList<>()).add(homologyNode.getId() +"");
                }
            }
            tx.success();
        }

        StringBuilder output = new StringBuilder();
        for (String genome : genomeList) {
            output.append("#Genome ").append(genome).append("\n");
            for (int haplotypes = 0; haplotypes <= ploidyPerGenome.get(genome); haplotypes++) {
                if (subgenomeCountsPerGenome.containsKey(genome + "_" + haplotypes)) {
                    if (haplotypes == 0) {
                        output.append("#Genome ").append(genome).append(". Only found on unphased sequences. ");
                    } else {
                        output.append("#Genome ").append(genome).append(". ").append(haplotypes).append("/").append(ploidyPerGenome.get(genome)).append(" haplotypes. ");
                    }
                    output.append(subgenomeCountsPerGenome.get(genome + "_" + haplotypes).size()).append(" groups\n")
                            .append(subgenomeCountsPerGenome.get(genome + "_" + haplotypes).toString().replace(", ", ",")
                                    .replace("[", "").replace("]", "")).append("\n");
                }
            }
        }
        writeStringToFile(output.toString(), WORKING_DIRECTORY + "gene_classification/group_identifiers/per_genome_and_subgenome_presence.csv", false, false);
    }

    /**
     * @param homologyNodes list with homology_group nodes
     */
    private HashMap<String, ArrayList<Integer>> subgenomePresenceFromHomologyGroups(ArrayList<Node> homologyNodes) {
        if (!PHASED_ANALYSIS) {
            return null;
        }
        HashMap<String, ArrayList<Integer>> subgenomeCountsPerGenome = new HashMap<>();
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node homologyNode : homologyNodes) {
                counter ++;
                if (counter % 100 == 0) {
                    System.out.print("\r Counting group subgenome presence: " + counter + "/" + homologyNodes.size() + "   ");
                }
                Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
                HashMap<String, HashSet<String>> presentSubgenomesPerGenome = new HashMap<>();
                for (Relationship rel : relationships) {
                    Node mrnaNode = rel.getEndNode();
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    if (skip_seq_array[address[0] - 1][address[1] - 1]) {
                        continue;
                    }
                    String[] phasingInfo = phasingInfoMap.get(address[0] + "_" + address[1]); // example [1, D, 1D, 1_D]
                    if (phasingInfo == null) {
                        continue;
                    }
                    String subgenome = pf.getSubgenomeIdentifier(address[0] + "_" + address[1]);
                    if (subgenome.endsWith("_unphased")) {
                        continue;
                    }
                    presentSubgenomesPerGenome.computeIfAbsent(address[0] + "", k -> new HashSet<>()).add(phasingInfo[1]);
                }

                // done counting subgenomes in current homology group. add to
                for (String genome : presentSubgenomesPerGenome.keySet()) {
                    HashSet<String> subgenomeIdentifiers = presentSubgenomesPerGenome.get(genome);
                    subgenomeCountsPerGenome.computeIfAbsent(genome, k -> new ArrayList<>()).add(subgenomeIdentifiers.size());
                }
            }
            tx.success();
        }
        return subgenomeCountsPerGenome;
    }

    private void writeGroupsPerSubgenomeCount(HashMap<Integer, ArrayList<Long>> groupsPerSubGenomeCount) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/group_identifiers/per_total_subgenomes.csv"));
        int totalSubgenomes = subgenomesInAnalysis(selectedSubgenomes);
        for (int i = totalSubgenomes; i > 0; i--) {
            if (groupsPerSubGenomeCount.containsKey(i)) {
                ArrayList<Long> groupIds = groupsPerSubGenomeCount.get(i);
                out.write("#" + i + " subgenomes, " + groupIds.size() + " groups\n" +
                        groupIds.toString().replace("[","").replace("]","").replace(" ","") + "\n");
            }
        }
    }

    /**
     *
     */
    private int countTotalSubgenomesInGroup(HashMap<String, Integer> geneCountPerSubgenome, HashMap<String, Integer> groupSizeFrequency,
                                            String groupCategory) {

        HashMap<Integer, ArrayList<String>> subgenomesPerGenome = new HashMap<>();
        for (String subgenomeId : geneCountPerSubgenome.keySet()) {
            String[] subgenomeArray = subgenomeId.split("_"); // [genome number, haplotype phase]
            subgenomesPerGenome.computeIfAbsent(Integer.parseInt(subgenomeArray[0]), k -> new ArrayList<>()).add(subgenomeArray[1]);
        }

        // 'unphased' is not allowed if there are other phases
        int totalSubgenomes = 0;
        for (int genomeNr : subgenomesPerGenome.keySet()) {
            ArrayList<String> subgenomes = subgenomesPerGenome.get(genomeNr);
            if (subgenomes.size() > 1) {
                subgenomes.remove("unphased");
            }
            totalSubgenomes += subgenomes.size();
        }
        groupSizeFrequency.merge(totalSubgenomes + "_subgenomes", 1, Integer::sum); // number of genomes in group
        groupSizeFrequency.merge(totalSubgenomes + "_subgenomes_" + groupCategory, 1, Integer::sum); // number of genomes in group
        return totalSubgenomes;
    }

    private void writeGroupsPerChromosomeHaplotypes(HashMap<String, ArrayList<Integer>> chromosomesPerGenome,
                                                    HashMap<String, ArrayList<String>> sequencesPerChromosomeGenome,
                                                    HashMap<String, ArrayList<String>> groups_per_phased_chrom) {

        if (!PHASED_ANALYSIS) {
            return;
        }
        
        StringBuilder output = new StringBuilder();
        for (String genome : genomeList) {
            output.append("#Genome ").append(genome).append("\n");
            if (!chromosomesPerGenome.containsKey(genome)) {
                continue;
            }
            ArrayList<Integer> chromosomesOfGenome = chromosomesPerGenome.get(genome);
            for (int chromosomeNr : chromosomesOfGenome) {
                if (chromosomeNr == 0) {
                    continue;
                }
                int sequencesCountOfChromosome = 1; // the current chromosome consists of this many sequences
                if (sequencesPerChromosomeGenome.containsKey(genome + "_" + chromosomeNr)) {
                    sequencesCountOfChromosome = sequencesPerChromosomeGenome.get(genome + "_" + chromosomeNr).size();
                }
                for (int i = 0; i <= sequencesCountOfChromosome; ++i) { // i is the number of haplotypes
                    if (groups_per_phased_chrom.containsKey(genome + "_" + chromosomeNr + "#" + i)) {
                        ArrayList<String> groups = groups_per_phased_chrom.get(genome + "_" + chromosomeNr + "#" + i);
                        output.append("#Genome ").append(genome).append(". Chromosome ").append(chromosomeNr).append(". ").append(i).append("/")
                                .append(sequencesCountOfChromosome).append(" haplotypes. ").append(groups.size()).append(" groups\n")
                                .append(groups.toString().replace("[", "").replace("]", "").replace(" ", "")).append("\n");
                    }
                }
            }
        }
        write_SB_to_file_in_DB(output, "gene_classification/group_identifiers/per_genome_and_chromosome.csv");
    }

    private int subgenomesInAnalysis(LinkedHashSet<String> selectedSubgenomes) {
        HashMap<Integer, ArrayList<String>> subgenomesPerGenome = new HashMap<>();
        for (String subgenomeId : selectedSubgenomes) {
            String[] subgenomeArray = subgenomeId.split("_"); // [genome number, haplotype phase]
            subgenomesPerGenome.computeIfAbsent(Integer.parseInt(subgenomeArray[0]), k -> new ArrayList<>()).add(subgenomeArray[1]);
        }

        // 'unphased' is not allowed if there are other phases
        int totalSubgenomes = 0;
        for (int genomeNr : subgenomesPerGenome.keySet()) {
            ArrayList<String> subgenomes = subgenomesPerGenome.get(genomeNr);
            if (subgenomes.size() > 1) {
                subgenomes.remove("unphased");
            }
            totalSubgenomes += subgenomes.size();
        }
        return totalSubgenomes;
    }

    /**
     * Creates group_size_frequency.csv
     * The size of an homology group based on:
     * - number of mRNAs
     * - number of genomes
     * - number of subgenomes
     * @param groupSizeFrequency
     * @param largestGroup number of mRNAs in the largest homology group
     * @param totalGroups
     */
    private void writeGroupSizeFrequencies(HashMap<String, Integer> groupSizeFrequency, int largestGroup, int totalGroups) {
        String proteinSizeFrequencies = calculateGroupFrequency(groupSizeFrequency, "mRNAs", largestGroup, "", totalGroups);
        String genomeSizeFrequencies = calculateGroupFrequency(groupSizeFrequency,  "genomes", genomeList.size(), "",  totalGroups);
        String output = "#Tables for different size types:\n" +
                "#1 Number of proteins (members)\n" +
                "#2 Number of genomes\n";

        if (selectedSubgenomes != null && !selectedSubgenomes.isEmpty()) {
            output += "#3 Number of subgenomes\n" +
                      "#4 Number of subgenomes per category\n\n";
        } else {
            output += "\n";
        }

        output += "#1 Number of proteins\n" +
                "proteins,frequency,percentage of total groups (" + totalGroups + ")\n" +
                proteinSizeFrequencies +
                "\n\n#2 Number of genomes (maximum of " + genomeList.size() + ")\ngenomes,frequency,percentage of total groups (" + totalGroups + ")\n" +
                genomeSizeFrequencies;

        if (selectedSubgenomes != null && !selectedSubgenomes.isEmpty()) {
            String subgenomeSizeFrequencies = calculateGroupFrequency(groupSizeFrequency, "subgenomes",
                    selectedSubgenomes.size(), "",  totalGroups);
            int totalSubgenomes = subgenomesInAnalysis(selectedSubgenomes);
            output += "\n\n#3 Number of subgenomes\n" +
                    "Number of subgenomes (maximum of " + totalSubgenomes + "),frequency,percentage of total groups (" + totalGroups + ")\n" +
                    subgenomeSizeFrequencies;

            String subgenomeSizeFreqCore = calculateGroupFrequency(groupSizeFrequency, "subgenomes_core",
                    selectedSubgenomes.size(), "core", totalGroups);
            String subgenomeSizeFreqAccessory = calculateGroupFrequency(groupSizeFrequency, "subgenomes_accessory",
                    selectedSubgenomes.size(), "accessory", totalGroups);
            String subgenomeSizeFreqUnique = calculateGroupFrequency(groupSizeFrequency, "subgenomes_unique",
                    selectedSubgenomes.size(), "unique", totalGroups);
            output += "\n#4 Number of subgenomes per category\nNumber of subgenomes (maximum of " + totalSubgenomes + "),frequency," +
                    "percentage of total groups (" + totalGroups + "),category\n" +
                    subgenomeSizeFreqCore + subgenomeSizeFreqAccessory + subgenomeSizeFreqUnique;
        }
        writeStringToFile(output, WORKING_DIRECTORY + "gene_classification/group_size_frequency.csv", false, false);
    }

    /**
     *
     * @param groupSizeFrequency
     * @param groupSizeKey
     * @param highestValue
     * @param category either "", "core", "accessory", or "unique"
     * @return
     */
    private String calculateGroupFrequency(HashMap<String, Integer> groupSizeFrequency, String groupSizeKey, int highestValue,
                                           String category, int totalGroups) {

        StringBuilder output = new StringBuilder();
        for (int i = 1; i <= highestValue; i++) {
            if (groupSizeFrequency.containsKey(i + "_"  + groupSizeKey)) {
                int value = groupSizeFrequency.get(i + "_" + groupSizeKey);
                output.append(i).append(",").append(value).append(",").append(percentageAsString(value, totalGroups, 2));
                if (!category.equals("")) { // when "core", "accessory", or "unique".
                    output.append(",").append(category);
                }
                output.append("\n");
            }
        }
        return output.toString();
    }

    private void countGenesOnTotalSubgenomes(HashMap<String, Integer> geneCountPerSubgenome,
                                              HashMap<String, Integer> mrnasWithSubgenomePresence) {

        HashMap<String, Integer> counts = new HashMap<>();
        HashMap<String, Integer> unphasedCounts = new HashMap<>();
        HashMap<String, HashSet<String>> subgenomes = new HashMap<>();

        for (String key : geneCountPerSubgenome.keySet()) {
            if (!key.contains("unphased")) {
                String[] keyArray = key.split("_");
                counts.merge(keyArray[0], geneCountPerSubgenome.get(key), Integer::sum);
                subgenomes.computeIfAbsent(keyArray[0], k -> new HashSet<>()).add(key);
            } else {
                subgenomes.computeIfAbsent(key, k -> new HashSet<>()).add(key);
                unphasedCounts.merge(key, geneCountPerSubgenome.get(key), Integer::sum);
            }
        }

        // go over unphased gene count. count them as if there are no subgenomes
        for (String key : unphasedCounts.keySet()) {
            String[] keyArray = key.split("_");
            if (counts.containsKey(keyArray[0])) { // other mRNAs of genome are found in subgenomes
                counts.merge(keyArray[0], unphasedCounts.get(key), Integer::sum);
            } else { // found in zero subgenomes
                mrnasWithSubgenomePresence.merge(keyArray[0] + "_0", unphasedCounts.get(key), Integer::sum);
            }
        }

        for (String key : counts.keySet()) {
            mrnasWithSubgenomePresence.merge(key + "_" + subgenomes.get(key).size(), counts.get(key), Integer::sum);
        }

    }

    /**
     *
     * @param phenotype_cnv
     */
    private void create_gclass_phenotype_additional_copies(HashMap<String, ArrayList<Node>> phenotype_cnv) {
        String more_copies = "he phenotype has more gene copies (in all genomes) compared to all genomes of the other phenotype.";
        String part1 = "#Part 1. T" + more_copies + " Other phenotype has at least 1 gene copy in every genome\n";
        String part2 = "#Part 2. T" + more_copies + "Other phenotype may not always have a gene copy in every genome\n";
        String header = "#This file consist of two parts. In both parts t" + more_copies
                + " In part 1 the other phenotype has at least 1 gene copy in every genome where "
                + " in part 2 not every genome will have a gene copy for the other phenotype."
                + " Use phenotype_additional_copies.csv to identify the exact number of extra copies\n\n";
        StringBuilder extra_builder = new StringBuilder(part1);
        StringBuilder more_builder = new StringBuilder(part2);
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            for (String phenotype2 : phenotype_map.keySet()) {
                if (phenotype2.equals("?") || phenotype.equals(phenotype2)) {
                    continue;
                }
                String key1 = phenotype + "#" + phenotype2 + "#extra";
                if (phenotype_cnv.containsKey(key1)) {
                    extra_builder.append(phenotype).append(" has more than ").append(phenotype2).append("\n");

                    ArrayList<Node> hm_nodes = phenotype_cnv.get(key1);
                    for (int i = 0; i < hm_nodes.size(); i++) {
                        if (i == hm_nodes.size()-1) {
                            extra_builder.append( hm_nodes.get(0).getId());
                        } else {
                            extra_builder.append( hm_nodes.get(0).getId()).append(",");
                        }
                    }
                    extra_builder.append("\n\n");
                }

                String key2 = phenotype + "#" + phenotype2 + "#more";
                if (phenotype_cnv.containsKey(key2)) {
                    more_builder.append(phenotype).append("  has more than ").append(phenotype2).append("\n");
                    ArrayList<Node> hm_nodes = phenotype_cnv.get(key2);
                    for (int i = 0; i < hm_nodes.size(); i++) {
                        if (i == hm_nodes.size()-1) {
                            more_builder.append( hm_nodes.get(0).getId());
                        } else {
                            more_builder.append(hm_nodes.get(0).getId()).append(",");
                        }
                    }
                    more_builder.append("\n\n");
                }
            }
        }
        write_string_to_file_in_DB(header + extra_builder + "\n" + more_builder,
                "gene_classification/group_identifiers/phenotype_additional_copy_groups.csv");
    }

    /**
     * Used to work with only stringbuilders. Function ran out of memory with 11679 potato sequences
     *
     * @param sharedGenesBetween genomes, sequences or subgenomes
     * @param selection the selected sequences, subgenomes or genomes
     * @param type
     */
    private void createSharedGenesMatrices(HashMap<String, int[]> sharedGenesBetween,
                                           ArrayList<String> selection, String type,
                                           HashMap<String, Integer> ploidyPerGenome) throws IOException {

        // distance builders
        BufferedWriter distanceSemi = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/distances_for_tree/" + type + "_distance_all_genes.csv"));
        BufferedWriter distanceDistinct = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/distances_for_tree/" + type + "_distance_distinct_genes.csv"));
        BufferedWriter distanceInformativeDistinct = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/distances_for_tree/" + type + "_distance_inf_distinct_genes.csv"));

        //percentage builders
        new File(WORKING_DIRECTORY +"/tmp").mkdirs(); // create tmp directory
        BufferedWriter percentageSemiCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageSemiCounts"));
        BufferedWriter percentageDistinctCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageDistinctCounts"));
        BufferedWriter percentageInformativeDistinctCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageInformativeDistinctCounts"));
        BufferedWriter percentageEverythingCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageEverythingCounts"));

        // gene count builders
        BufferedWriter countSemi = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/countSemi"));
        BufferedWriter countDistinct = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/countDistinct"));
        BufferedWriter countInformativeDistinct = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/countInformativeDistinct"));
        BufferedWriter unsharedSemi = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/unsharedSemi"));
        BufferedWriter unsharedDistinct = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/unsharedDistinct"));
        BufferedWriter unsharedInformativeDistinct = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/unsharedInformativeDistinct"));
        BufferedWriter countEverything = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/countEverything"));

        BufferedWriter percentageSemiCountsOneGenome = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageSemiCountsOneGenome"));
        BufferedWriter percentageDistinctCountsOneGenome = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp/percentageDistinctCountsOneGenome"));

        HashMap<String, ArrayList<Double>> percentagesWithinGenomes = new HashMap<>();

        String header = createHeaderSharedGeneMatrix(selection, type, sharedGenesBetween, ploidyPerGenome);
        String header2 = header.replace(" ","_");

        distanceSemi.write(header2);
        distanceDistinct.write(header2);
        distanceInformativeDistinct.write(header2);

        for (int i = 0; i < selection.size(); i ++) {
            String idA = selection.get(i); // example 1, 1_1 and 1_unphased
            String idAforHeader = idA;
            if (!sharedGenesBetween.containsKey(idA + "#" + idA)) {
                continue;
            }
            int[] shared_with_itself_i = sharedGenesBetween.get(idA + "#" + idA);
            String add = "";
            if (type.equals("sequence")) {
                add = retrievePhasingIdForMatrices(idA);
            } else if (type.equals("genome")) {
                try {
                    int genomeNr = Integer.parseInt(selection.get(i));
                    add = get_phenotype_for_genome(genomeNr, true);
                } catch (NumberFormatException ignored) {
                }
            } else if (type.equals("subgenome")) {
                idAforHeader = renameSubgenomeIdForUnphasedGenomes(selection.get(i), ploidyPerGenome);
            }

            distanceSemi.write( idAforHeader + add.replace(" ","_") + ",");
            distanceDistinct.write( idAforHeader + add.replace(" ","_") + ",");
            distanceInformativeDistinct.write( idAforHeader + add.replace(" ","_") + ",");

            percentageSemiCounts.write( idAforHeader + add + ",");
            percentageDistinctCounts.write( idAforHeader + add + ",");
            percentageInformativeDistinctCounts.write( idAforHeader + add + ",");
            percentageEverythingCounts.write( idAforHeader + add + ",");
            percentageSemiCountsOneGenome.write( idAforHeader + add + ",");
            percentageDistinctCountsOneGenome.write( idAforHeader + add + ",");

            countEverything.write( idAforHeader + add + ",");
            countSemi.write( idAforHeader + add + ",");
            countDistinct.write( idAforHeader + add + ",");
            countInformativeDistinct.write(idAforHeader + add + ",");
            unsharedSemi.write( idAforHeader + add + ",");
            unsharedDistinct.write( idAforHeader + add + ",");
            unsharedInformativeDistinct.write( idAforHeader + add + ",");

            for (int j = 0; j < selection.size(); j ++) {
                String idB = selection.get(j);
                int[] geneCounts = new int[8]; // total semi, total distinct, shared semi, shared distinct, total informative distinct, shared informative distinct, EVERYTHING a shared to b, EVERYTHING a shared to b
                double everythingPercentage = 0;
                if (sharedGenesBetween.containsKey(idA + "#" + idB)) {
                    geneCounts = sharedGenesBetween.get(idA + "#" + idB);
                    countEverything.write(geneCounts[6] + "");
                    everythingPercentage = percentage(geneCounts[6], shared_with_itself_i[0]);
                    percentageEverythingCounts.write(percentageAsString(geneCounts[6], shared_with_itself_i[0], 2));
                } else if (sharedGenesBetween.containsKey(idB + "#" + idA)) {
                    geneCounts = sharedGenesBetween.get(idB + "#" + idA);
                    countEverything.write(geneCounts[7] + "");
                    everythingPercentage = percentage(geneCounts[7], shared_with_itself_i[0]);
                    percentageEverythingCounts.write(percentageAsString(geneCounts[7], shared_with_itself_i[0], 2));
                }

                percentageSemiCountsOneGenome.write(percentageAsString(geneCounts[2], shared_with_itself_i[0], 2));
                percentageDistinctCountsOneGenome.write(percentageAsString(geneCounts[3], shared_with_itself_i[1], 2));
                percentageSemiCounts.write(percentageAsString(geneCounts[2], geneCounts[0], 2));
                percentageDistinctCounts.write(percentageAsString(geneCounts[3], geneCounts[1], 2));
                percentageInformativeDistinctCounts.write(percentageAsString(geneCounts[5], geneCounts[4], 2));

                double semiFraction = divide(geneCounts[2], geneCounts[0]);
                double distinctFraction = divide(geneCounts[3], geneCounts[1]);
                double informativeDistinctFraction = divide(geneCounts[5], geneCounts[4]);
                if (idA.equals(idB)) { // is required when a genome does not have genes
                    semiFraction = 1;
                    distinctFraction = 1;
                    informativeDistinctFraction = 1;
                }

                if (type.equals("subgenome")) {
                    String[] subgenomeAArray = idA.split("_");
                    String[] subgenomeBArray = idB.split("_");
                    if (subgenomeAArray[0].equals(subgenomeBArray[0]) &&
                            !idA.contains("unphased") && !idB.contains("unphased") && i != j) { // its the same genome and its not unphased
                        percentagesWithinGenomes.computeIfAbsent(subgenomeAArray[0] + "#semi", k -> new ArrayList<>()).add(percentage(geneCounts[2], geneCounts[0]));
                        percentagesWithinGenomes.computeIfAbsent(subgenomeAArray[0] + "#distinct" , k -> new ArrayList<>()).add(percentage(geneCounts[3], geneCounts[1]));
                        percentagesWithinGenomes.computeIfAbsent(subgenomeAArray[0] + "#everything", k -> new ArrayList<>()).add(everythingPercentage);
                    }
                }

                distanceSemi.write( (1 - semiFraction) +"");
                distanceDistinct.write( (1 - distinctFraction) + "");
                distanceInformativeDistinct.write((1 - informativeDistinctFraction ) + "");

                countSemi.write(geneCounts[2] + "");
                countDistinct.write(geneCounts[3] + "");
                countInformativeDistinct.write(geneCounts[5] + "");

                unsharedSemi.write((geneCounts[0] - geneCounts[2]) + "");
                unsharedDistinct.write((geneCounts[1] - geneCounts[3]) + "");
                unsharedInformativeDistinct.write((geneCounts[4] - geneCounts[5]) + "");
                if (j != selection.size()-1) {
                    distanceSemi.write(",");
                    distanceDistinct.write(",");
                    distanceInformativeDistinct.write(",");

                    percentageSemiCountsOneGenome.write(",");
                    percentageDistinctCountsOneGenome.write(",");
                    percentageSemiCounts.write(",");
                    percentageDistinctCounts.write(",");
                    percentageInformativeDistinctCounts.write(",");
                    percentageEverythingCounts.write(",");

                    countEverything.write(",");
                    countSemi.write(",");
                    countDistinct.write(",");
                    countInformativeDistinct.write(",");

                    unsharedSemi.write(",");
                    unsharedDistinct.write(",");
                    unsharedInformativeDistinct.write(",");
                }
            }
            distanceSemi.write("\n");
            distanceDistinct.write("\n");
            distanceInformativeDistinct.write("\n");

            percentageSemiCounts.write("\n");
            percentageDistinctCounts.write("\n");
            percentageInformativeDistinctCounts.write("\n");
            percentageEverythingCounts.write("\n");

            countEverything.write("\n");
            countSemi.write("\n");
            countDistinct.write("\n");
            countInformativeDistinct.write("\n");
            unsharedSemi.write("\n");
            unsharedDistinct.write("\n");
            unsharedInformativeDistinct.write("\n");

            percentageSemiCountsOneGenome.write("\n");
            percentageDistinctCountsOneGenome.write("\n");
        }

        distanceSemi.close();
        distanceDistinct.close();
        distanceInformativeDistinct.close();

        percentageSemiCounts.close();
        percentageDistinctCounts.close();
        percentageInformativeDistinctCounts.close();
        percentageEverythingCounts.close();

        countEverything.close();
        countSemi.close();
        countDistinct.close();
        countInformativeDistinct.close();
        unsharedSemi.close();
        unsharedDistinct.close();
        unsharedInformativeDistinct.close();

        percentageSemiCountsOneGenome.close();
        percentageDistinctCountsOneGenome.close();

        String[] averageWithinGenomes = null;
        if (type.equals("subgenome")) {
            averageWithinGenomes = calculateAverageFractionInGenomes(percentagesWithinGenomes);
        }

        BufferedWriter out = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/percentage_genes_shared_" + type + ".csv"));
        out.write("#13 matrices: 7 with percentages and 6 with gene counts\n"
                        + "\n"
                        + createGeneCountExplanation(type)
                        + "\n"
                        + createTableExplanations(type, averageWithinGenomes)
                        + "\n");

        out.write("\n#1 Percentage based on SEMI gene counts of both " + type + "s (#11 & #12)\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/percentageSemiCounts", out);

        out.write("\n#2 Percentage based on SEMI gene counts and the " + type + " in the left column (#11 & diagonal #11)\n" + header);
        appendOpenFileToOpenFile( WORKING_DIRECTORY + "tmp/percentageSemiCountsOneGenome", out);

        out.write( "\n#3 Percentage based on DISTINCT gene counts of both " + type + "s (#7 & #8)\n" + header);
        appendOpenFileToOpenFile( WORKING_DIRECTORY + "tmp/percentageDistinctCounts", out);

        out.write("\n#4 Percentage based on DISTINCT gene counts and the " + type + " in the left column (#7 & diagonal #7)\n" + header);
        appendOpenFileToOpenFile( WORKING_DIRECTORY + "tmp/percentageDistinctCountsOneGenome", out);

        out.write("\n#5 Percentage based on informative DISTINCT gene counts of both " + type + "s (#9 & #10)\n" + header);
        appendOpenFileToOpenFile( WORKING_DIRECTORY + "tmp/percentageInformativeDistinctCounts", out);

        out.write( "\n#6 Percentage based on EVERYTHING gene counts and the " + type + " in left column (#13 & diagonal #13)\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/percentageEverythingCounts", out);

        out.write( "\n#7 shared DISTINCT genes\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/countDistinct", out);

        out.write(  "\n#8 unshared DISTINCT genes\n" + header );
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/unsharedDistinct", out);

        out.write("\n#9 shared informative DISTINCT genes\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/countInformativeDistinct", out);

        out.write("\n#10 unshared informative DISTINCT genes\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/unsharedInformativeDistinct", out);

        out.write("\n#11 shared SEMI genes\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/countSemi", out);

        out.write( "\n#12 unshared SEMI genes\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/unsharedSemi", out);

        out.write("\n#13 shared EVERYTHING genes based on left column\n" + header);
        appendOpenFileToOpenFile(WORKING_DIRECTORY + "tmp/countEverything", out);

        out.close();
        delete_directory(WORKING_DIRECTORY + "tmp/");
    }

    private void appendOpenFileToOpenFile(String file,  BufferedWriter out) throws IOException {
        BufferedReader br = open_file(file);
        for (String line = br.readLine(); line != null; line = br.readLine()) {
            out.write(line + "\n");
        }
    }

    private String createHeaderSharedGeneMatrix(ArrayList<String> selection, String type, HashMap<String, int[]> sharedGenesBetween,
                                                HashMap<String, Integer> ploidyPerGenome) {
        String typeCapitalised = capitalize_first_letter(type);
        StringBuilder header = new StringBuilder(typeCapitalised + "s,");
        for (int i = 0; i < selection.size(); i ++) {
            String idA = selection.get(i); // example 1_1
            if (!sharedGenesBetween.containsKey(idA + "#" + idA)) {
                continue;
            }
            String add = "";
            if (type.equals("sequence")) {
                add = retrievePhasingIdForMatrices(idA);
            } else if (type.equals("genome")) {
                try {
                    int genomeNr = Integer.parseInt(selection.get(i));
                    add = get_phenotype_for_genome(genomeNr, true);
                } catch (NumberFormatException ignored) {
                }
            } else if (type.equals("subgenome")) {
                idA = renameSubgenomeIdForUnphasedGenomes(selection.get(i), ploidyPerGenome);
            }

            header.append(idA).append(add);
            if (i != selection.size()-1) {
                header.append(",");
            }
        }
        header.append("\n");
        return header.toString();
    }

    /**
     * Calculate average of shared gene between subgenomes.
     * genome with 4 phases. calculate from A-B, A-C, A-D, B-C, B-D, C-D.
     * @param percentagesWithinGenomes key is genome combination, value is list of percentages
     * @return
     */
    private String[] calculateAverageFractionInGenomes(HashMap<String, ArrayList<Double>> percentagesWithinGenomes) {
        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder semi = new StringBuilder("# Average between subgenomes within a genome:\n");
        StringBuilder distinct = new StringBuilder("# Average between subgenomes within a genome:\n");
        StringBuilder everything = new StringBuilder("# Average between subgenomes within a genome:\n");
        for (String genome : genomeList) {
            if (!percentagesWithinGenomes.containsKey(genome + "#semi")) {
                continue;
            }
            double[] totalStatistics = median_average_stdev_from_AL_double(percentagesWithinGenomes.get(genome + "#semi")); // [median, average, standard deviation, shortest, longest, total, count]
            double[] distinctStatistics = median_average_stdev_from_AL_double(percentagesWithinGenomes.get(genome + "#distinct"));
            double[] everythingStatistics = median_average_stdev_from_AL_double(percentagesWithinGenomes.get(genome + "#everything"));
            semi.append("# genome ").append(genome).append(": ").append(df.format(totalStatistics[1])).append("\n");
            distinct.append("# genome ").append(genome).append(": ").append(df.format(distinctStatistics[1])).append("\n");
            everything.append("# genome ").append(genome).append(": ").append(df.format(everythingStatistics[1])).append("\n");
        }
        return new String[]{semi.toString(), distinct.toString(), everything.toString()};
    }

    private String retrievePhasingIdForMatrices(String sequenceId) {
        String add = "";
        if (PHASED_ANALYSIS) {
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            if (phasingInfo == null) {
                add = " unphased";
            } else {
                add = " " + phasingInfo[2] ;
            }
        }
        return add;
    }

    private HashMap<String, double[]> alleleStatistics(ArrayList<Node> homologyNodes, boolean useLongestTranscripts,
                                                                boolean calculateAlleles) {

        String copiesOrAllelesString = "copies";
        if (calculateAlleles) {
            copiesOrAllelesString = "alleles";
        }

        String longestTranscriptsString = "";
        if (useLongestTranscripts) {
            longestTranscriptsString = " of longest transcripts";
        }

        HashMap<String, ArrayList<Integer>> geneCopiesGenome = new HashMap<>();
        HashMap<String, ArrayList<Integer>> proteinCopiesGenome = new HashMap<>();
        HashMap<String, ArrayList<Integer>> geneCopiesPhase = new HashMap<>();
        HashMap<String, ArrayList<Integer>> proteinCopiesPhase = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int groupCounter = 0;
            for (Node homologyNode : homologyNodes) {
                groupCounter++;
                if (groupCounter % 100 == 0 || groupCounter < 11 || groupCounter == homologyNodes.size()) {
                    System.out.print("\r Counting gene " + copiesOrAllelesString + longestTranscriptsString + ": group "
                            + groupCounter + "/" + homologyNodes.size() + "   ");
                }
                HashMap<Integer, ArrayList<String>> nucleotideSequencesGenome = new HashMap<>();
                HashMap<Integer, ArrayList<String>> proteinSequencesGenome = new HashMap<>();
                HashMap<String, ArrayList<String>> nucleotideSequencesPhase = new HashMap<>();
                HashMap<String, ArrayList<String>> proteinSequencesPhase = new HashMap<>();
                int groupSize = 0;
                Iterable<Relationship> relationships = homologyNode.getRelationships(RelTypes.has_homolog);
                for (Relationship rel : relationships) {
                    Node mrnaNode = rel.getEndNode();
                    if (useLongestTranscripts && !mrnaNode.hasProperty("longest_transcript")) {
                        continue;
                    }
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    int genomeNr = address[0];
                    int sequenceNr = address[1];
                    if (!selectedSequences.contains(genomeNr + "_" + sequenceNr)) {
                        continue;
                    }
                    groupSize++;
                    String nucleotideSequence = (String) mrnaNode.getProperty("nucleotide_sequence");
                    String proteinSequence = (String) mrnaNode.getProperty("protein_sequence");
                    nucleotideSequencesGenome.computeIfAbsent(genomeNr, k -> new ArrayList<>()).add(nucleotideSequence);
                    proteinSequencesGenome.computeIfAbsent(genomeNr, k -> new ArrayList<>()).add(proteinSequence);

                    if (PHASED_ANALYSIS) {
                        String[] phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr);  // [1, B, 1B, 1_B]
                        if (phasingInfo == null) {
                            nucleotideSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(nucleotideSequence);
                            proteinSequencesPhase.computeIfAbsent(genomeNr + "_unphased", k -> new ArrayList<>()).add(proteinSequence);
                        } else {
                            nucleotideSequencesPhase.computeIfAbsent(genomeNr + "_" + phasingInfo[1], k -> new ArrayList<>()).add(nucleotideSequence);
                            proteinSequencesPhase.computeIfAbsent(genomeNr + "_" + phasingInfo[1], k -> new ArrayList<>()).add(proteinSequence);
                        }
                    }
                }
                if (groupSize == 0) {
                    continue;
                }

                for (int genomeNr : proteinSequencesGenome.keySet()) {
                    ArrayList<String> proteinSequences = proteinSequencesGenome.get(genomeNr);
                    ArrayList<String> nucleotideSequences = nucleotideSequencesGenome.get(genomeNr);
                    HashSet<String> distinctProtSequences = new HashSet<>(proteinSequences);
                    HashSet<String> distinctNucSequences = new HashSet<>(nucleotideSequences);
                    if (calculateAlleles) {
                        geneCopiesGenome.computeIfAbsent(genomeNr + "", k -> new ArrayList<>()).add(distinctNucSequences.size());
                        proteinCopiesGenome.computeIfAbsent(genomeNr + "", k -> new ArrayList<>()).add(distinctProtSequences.size());
                    } else {
                        geneCopiesGenome.computeIfAbsent(genomeNr + "", k -> new ArrayList<>()).add(nucleotideSequences.size());
                        proteinCopiesGenome.computeIfAbsent(genomeNr + "", k -> new ArrayList<>()).add(proteinSequences.size());
                    }
                }

                for (String phaseKey : proteinSequencesPhase.keySet()) {
                    ArrayList<String> proteinSequences = proteinSequencesPhase.get(phaseKey);
                    ArrayList<String> nucleotideSequences = nucleotideSequencesPhase.get(phaseKey);
                    HashSet<String> distinctProtSequences = new HashSet<>(proteinSequences);
                    HashSet<String> distinctNucSequences = new HashSet<>(nucleotideSequences);
                    if (calculateAlleles) {
                        geneCopiesPhase.computeIfAbsent(phaseKey, k -> new ArrayList<>()).add(distinctNucSequences.size());
                        proteinCopiesPhase.computeIfAbsent(phaseKey, k -> new ArrayList<>()).add(distinctProtSequences.size());
                    } else {
                        geneCopiesGenome.computeIfAbsent(phaseKey, k -> new ArrayList<>()).add(nucleotideSequences.size());
                        proteinCopiesGenome.computeIfAbsent(phaseKey, k -> new ArrayList<>()).add(proteinSequences.size());
                    }
                }
            }
            tx.success();
        } catch (Exception e) {
            Pantools.logger.error("Encountered an issue querying the pangenome for allele statistics.");
            throw new RuntimeException(e.getMessage());
        }

        HashMap<String, double[]> alleleCountStatistics = new HashMap<>();
        StringBuilder csv = createHeaderAlleleCountCSV(useLongestTranscripts, calculateAlleles);
        calculateAlleStatistics(alleleCountStatistics, geneCopiesGenome, proteinCopiesGenome);
        StringBuilder genomeCsv = createAlleleCountCSV(proteinCopiesGenome, geneCopiesGenome, genomeList, alleleCountStatistics, calculateAlleles);
        csv.append(genomeCsv).append("\n");
        if (PHASED_ANALYSIS) {
            calculateAlleStatistics(alleleCountStatistics, geneCopiesPhase, proteinCopiesPhase);
            StringBuilder subgenomeCsv = createAlleleCountCSV(proteinCopiesPhase, geneCopiesPhase, new ArrayList<>(selectedSubgenomes), alleleCountStatistics, calculateAlleles);
            csv.append(subgenomeCsv);
        }

        String geneOrAllele = "gene_copies";
        if (calculateAlleles) {
            geneOrAllele = "allele_counts";
        }
        writeStringToFile(csv.toString(), WORKING_DIRECTORY + "gene_classification/" + geneOrAllele + ".csv", false, useLongestTranscripts);
        return alleleCountStatistics;
    }

    private void calculateAlleStatistics(HashMap<String, double[]> alleleCountStatistics,
                                         HashMap<String, ArrayList<Integer>> geneCopies,
                                         HashMap<String, ArrayList<Integer>> proteinCopies) {

        for (String id : proteinCopies.keySet()) {
            ArrayList<Integer> proteinCounts = proteinCopies.get(id);
            ArrayList<Integer> geneCounts = geneCopies.get(id);
            double[] proteinStatistics = median_average_stdev_from_AL_int(proteinCounts);
            double[] geneStatistics = median_average_stdev_from_AL_int(geneCounts);
            alleleCountStatistics.put(id + "#protein", proteinStatistics);
            alleleCountStatistics.put(id + "#cds", geneStatistics);
        }
    }

    private StringBuilder createHeaderAlleleCountCSV(boolean useLongestTranscripts, boolean calculateAlleles) {
        StringBuilder groupCountsHeader = new StringBuilder();
        StringBuilder percentagesHeader = new StringBuilder();
        String allele = "distinct allele";
        if (!calculateAlleles) {
            allele = "gene copy";
        }
        for (int i = 1; i <= 10; i++) {
            if (i == 2) { // make allele or gene plural
                if (calculateAlleles) {
                    allele = "distinct alleles";
                } else {
                    allele = "gene copies";
                }
            }
            groupCountsHeader.append("Groups with ").append(i).append(" ").append(allele).append(",");
            percentagesHeader.append("% of groups with ").append(i).append(" ").append(allele).append(",");
        }
        groupCountsHeader.append("Groups with 11 or more ").append(allele).append(",");
        percentagesHeader.append("% of groups with 11 or more ").append(allele).append(",");

        String longestTranscriptsStr = "\n#Included only the longest transcripts\n";

        if (!useLongestTranscripts) {
            longestTranscriptsStr = "";
        }

        String sequenceType = "Sequence type,";
        if (!calculateAlleles) {
            sequenceType = "";
        }

        StringBuilder csv = new StringBuilder();
        csv.append(longestTranscriptsStr).append("Genome/Subgenome,").append(sequenceType).append(percentagesHeader).append(groupCountsHeader)
                .append("total groups,median (number of alleles),average (number of alleles), standard deviation").append("\n");
        return csv;
    }

    private StringBuilder createAlleleCountCSV(HashMap<String, ArrayList<Integer>> proteinCopies,
                                            HashMap<String, ArrayList<Integer>> geneCopies,
                                            ArrayList<String> genomesOrSubgenomes, HashMap<String, double[]> alleleCountStatistics, boolean calculateAlleles) {

        String nucleotide = "";
        String protein = "";
        if (calculateAlleles) {
            nucleotide = "nucleotide,";
            protein = "protein,";
        }
        StringBuilder csv = new StringBuilder();
        // example: 1 or 1_A
        for (String id : genomesOrSubgenomes) {
            if (!proteinCopies.containsKey(id)) {
                continue;
            }
            ArrayList<Integer> proteinCounts = proteinCopies.get(id);
            ArrayList<Integer> geneCounts = geneCopies.get(id);
            double[] proteinStatistics = alleleCountStatistics.get(id + "#protein");
            double[] geneStatistics = alleleCountStatistics.get(id + "#cds");
            if (id.contains("_") && !id.contains("unphased")) { // subgenome id is transformed from 1_A to 1A
                id = id.replace("_", "");
            }
            csv.append(id).append(",").append(nucleotide).append(addAlleleCountsToBuilder(geneStatistics, geneCounts));
            if (calculateAlleles) {
                csv.append(id).append(",").append(protein).append(addAlleleCountsToBuilder(proteinStatistics, proteinCounts));
            }
        }
        return csv;
    }

    /**
     *
     * @param statistics [median, average, standard deviation, shortest, longest, total, count]
     * @param counts
     * @return
     */
    private String addAlleleCountsToBuilder(double[] statistics, ArrayList<Integer> counts) {
        double percentageSum = 0, groupCounts = 0;
        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder alleleCounts = new StringBuilder();
        StringBuilder percentages = new StringBuilder();
        for (int i = 1; i <= 10; i++) {
            int occurrences = Collections.frequency(counts, i);
            if (occurrences > 0) {
                double percentage = percentage(occurrences, (int) statistics[6]);
                alleleCounts.append(occurrences);
                groupCounts += occurrences;
                percentages.append(df.format(percentage));
                percentageSum += percentage;
            }
            percentages.append(",");
            alleleCounts.append(",");
        }
        return percentages + df.format(100-percentageSum) + "," + alleleCounts +
                (statistics[6]- groupCounts) + "," + statistics[6] + "," + df.format(statistics[0]) + "," +
                df.format(statistics[1]) + "," + df.format(statistics[2]) + "\n";
    }

    /**
     *
     * @param fields
     * @param core_access_uni_map
     * @param class_str
     * @param geneCountPerSequence
     */
    private void increase_core_access_uni_map2(int[] fields, HashMap<String, int[]> core_access_uni_map, String class_str,
                                                     HashMap<String, Integer> geneCountPerSequence) {

        for (String genome : genomeList) { // cnv array of homology node
            int position = genomeList.indexOf(genome);

            if (fields[position] == 0) {
                continue;
            }
            if (class_str.equals("core")) {
                incr_array_hashmap(core_access_uni_map, genome + "_total", fields[position], 0); // position 0 of array
                incr_array_hashmap(core_access_uni_map, genome + "_distinct", 1, 0);
            } else if (class_str.equals("accessory"))  {
                incr_array_hashmap(core_access_uni_map, genome + "_total", fields[position], 1); // position 1 of array
                incr_array_hashmap(core_access_uni_map, genome + "_distinct", 1, 1);
            } else { // unique
                incr_array_hashmap(core_access_uni_map, genome + "_total", fields[position], 2); // position 2 of array
                incr_array_hashmap(core_access_uni_map, genome + "_distinct", 1, 2);
            }
        }

        if (compare_sequences) {
            for (String sequenceIdentifier : geneCountPerSequence.keySet()) {
                int number_of_genes = geneCountPerSequence.get(sequenceIdentifier);
                if (number_of_genes == 0) {
                    continue;
                }
                if (class_str.equals("core")) {
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_total", number_of_genes, 0); // position 0 of array
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_distinct", 1, 0);
                } else if (class_str.equals("accessory"))  {
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_total", number_of_genes, 1); // position 1 of array
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_distinct", 1, 1);
                } else if (class_str.equals("unique")) {
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_total", number_of_genes, 2); // position 2 of array
                    incr_array_hashmap(core_access_uni_map, sequenceIdentifier + "_distinct", 1, 2);
                }
            }
        }
    }

    /**
     * Initializes the map holding number of core, accessory, unique genes/groups for every genome and sequence
     * key with "total" considers every gene.
     * "distinct" counts a maximum of 1 per group.
     * @return map with array of length 3 for every genome/sequence in current analysis
     */
    private  HashMap<String, int[]> initializeCoreAccessUniMap() {
        HashMap<String, int[]> core_access_uni_map = new HashMap<>(); // key is genome number + '_total' or '_distinct'
        for (String genome : genomeList) {
            int[] empty1 = new int[3]; // [core, accessory, unique]
            int[] empty2 = new int[3]; // [core, accessory, unique]
            core_access_uni_map.put(genome + "_total", empty1);
            core_access_uni_map.put(genome + "_distinct", empty2);
            if (compare_sequences) {
                try {
                    int genomeNr = Integer.parseInt(genome); // only works if genome is a number
                    for (int sequenceNr = 1; sequenceNr <= GENOME_DB.num_sequences[genomeNr]; ++sequenceNr) {
                        if (!selectedSequences.contains(genome + "_" + sequenceNr)) {
                            continue;
                        }
                        int[] empty3 = new int[3]; // [core, accessory, unique]
                        int[] empty4 = new int[3]; // [core, accessory, unique]
                        core_access_uni_map.put(genome + "_" + sequenceNr + "_total", empty3);
                        core_access_uni_map.put(genome + "_" + sequenceNr + "_distinct", empty4);
                    }
                } catch (NumberFormatException ignored) {
                }
            }
        }
        return core_access_uni_map;
    }

    /**
     * Set empty array to every combination in the current genome selection
     * @return key is combination of genome (numbers), value is  [total all, total distinct, shared all, shared distinct, total informative distinct, shared informative distinct]
     */
    private HashMap<String, int[]> initializeSharedGenesBetweenGenomesMap() {
        HashMap<String, int[]> sharedGenesBetweenGenomes = new HashMap<>();
        for (String genomeA : genomeList) {
            for (String genomeB : genomeList) {
                int[] empty = {0, 0, 0, 0, 0, 0, 0, 0}; // [total all, total distinct, shared all, shared distinct, total informative distinct, shared informative distinct, every shared genome A, every shared genome B]
                if (genomeA.compareTo(genomeB) < 0) {
                    sharedGenesBetweenGenomes.put(genomeA + "#" + genomeB, empty);
                } else {
                    sharedGenesBetweenGenomes.put(genomeB + "#" + genomeA, empty);
                }
            }
        }
        Pantools.logger.trace("Created sharedGenesBetweenGenomes map: {}", sharedGenesBetweenGenomes);
        return sharedGenesBetweenGenomes;
    }

    private void find_additional_pheno_gene_copies(HashMap<String, int[]> phenotype_min_max,
                                                   HashMap<String, ArrayList<Node>> phenotype_cnv,
                                                   String groupCategory, Node hm_node, StringBuilder pheno_cnv_builder) {

        if (PHENOTYPE == null) {
            return;
        }
        for (String phenotypeValue : phenotype_threshold_map.keySet()) {
            if (phenotype_threshold_map.get(phenotypeValue) == 0) {
                continue;
            }
            if (!phenotype_min_max.containsKey(phenotypeValue)) {
                continue;
            }

            int[] phenotypeCategory = phenotype_min_max.get(phenotypeValue + "_class"); // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
            if (phenotypeCategory[0] == 0) { // when this value is 0, not all phenotype members have the gene
                continue;
            }
            int[] pheno_1_lowest_highest = phenotype_min_max.get(phenotypeValue); // [lowest, highest number of genes copies]
            StringBuilder line;
            if (pheno_1_lowest_highest[0] != pheno_1_lowest_highest[1]) {
                line = new StringBuilder(hm_node.getId() + "," + groupCategory + "," + phenotypeValue + "," + pheno_1_lowest_highest[0] + "-" + pheno_1_lowest_highest[1] + ",");
            } else {
                line = new StringBuilder(hm_node.getId() + "," + groupCategory + "," + phenotypeValue + "," + pheno_1_lowest_highest[0] + ",");
            }

            boolean more_copies = false; // becomes true when the current phenotype has more copies to at least one other phenotypes
            for (String phenotypeValue2 : phenotype_threshold_map.keySet()) {
                if (phenotype_threshold_map.get(phenotypeValue2) == 0) {
                    continue;
                }
                if (phenotypeValue.equals(phenotypeValue2)) { // itself
                    line.append(",");
                    continue;
                }
                // does the other phenotype also needs to be shared? i don't think so
                //int[] phenotypeCategory2 = phenotype_min_max.get(phenotypeValue2 + "_class"); // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                //if (phenotypeCategory2[0] == 0) { // when this value is 0, not all phenotype members have the gene
                //    continue;
                //}

                int[] pheno_2_lowest_highest;
                boolean otherPhenotypeHasNoGene = false;
                if (!phenotype_min_max.containsKey(phenotypeValue2)) {
                    otherPhenotypeHasNoGene = true;
                    pheno_2_lowest_highest = new int[]{0,0};
                } else {
                    pheno_2_lowest_highest = phenotype_min_max.get(phenotypeValue2); // [lowest, highest number of genes copies]
                }
                if (pheno_2_lowest_highest[1] >= pheno_1_lowest_highest[0]) {
                    line.append(","); // the other phenotype is equal or higher
                    continue;
                }
                more_copies = true;
                int difference = pheno_1_lowest_highest[0] - pheno_2_lowest_highest[1];
                int difference2 = pheno_1_lowest_highest[0] - pheno_2_lowest_highest[0];
                if (!otherPhenotypeHasNoGene) {
                    phenotype_cnv.computeIfAbsent(phenotypeValue + "#" + phenotypeValue2 + "#extra", k -> new ArrayList<>()).add(hm_node);
                }
                phenotype_cnv.computeIfAbsent(phenotypeValue + "#" + phenotypeValue2 + "#more", k -> new ArrayList<>()).add(hm_node);
                if (difference != difference2) {
                    line.append(difference).append("-").append(difference2).append(",");
                } else {
                    line.append(difference).append(",");
                }
            }
            if (more_copies) {
                pheno_cnv_builder.append(line).append("\n");
            }
        }
    }

    /**
     *
     * @param pheno_cnv_builder
     */
    private void create_gclass_phenotype_cnv(StringBuilder pheno_cnv_builder) {
        String header = "#Genomes of a phenotype with additional copies compared to other phenotypes in core and acccessory homology groups.\n"
                + "#These are all phenotype SHARED groups and not SPECIFIC because there at least one copy present in a genome from a different phenotype\n\n";

        StringBuilder header2 = new StringBuilder("Homology group, class,Phenotype with more copies,Number of copies,");
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            if (phenotype_threshold_map.get(phenotype) == 0) {
                continue; // all genomes were skipped
            }
            int[] genomes = phenotype_map.get(phenotype);
            int genomeCount = 0;
            for (int genomeNr : genomes) {
                if (genomeList.contains(genomeNr + "")) {
                    genomeCount++;
                }
            }
            header2.append(phenotype).append(" (").append(genomeCount).append(" genomes)").append(",");
        }
        header2.append("\n");
        writeStringToFile(header + header2 + pheno_cnv_builder.toString(),
                WORKING_DIRECTORY + "gene_classification/phenotype_additional_copies.csv", false, false);
    }


    private void updateCNV(int[] copyNumberArray, StringBuilder cnv_builder) {
        for (String genomeA : genomeList) {
            int positionA = genomeList.indexOf(genomeA);
            int highest_gain = 0;
            boolean zero = false;
            StringBuilder line = new StringBuilder(genomeA + "," + copyNumberArray[positionA] + ",");
            for (String genomeB : genomeList) { // first value is a zero
                int positionB = genomeList.indexOf(genomeB);
                if (highest_gain < copyNumberArray[positionA] - copyNumberArray[positionB]) {
                    highest_gain = copyNumberArray[positionA] - copyNumberArray[positionB];
                }
                if (0 >= copyNumberArray[positionA] - copyNumberArray[positionB]) {
                    if (-1 >= copyNumberArray[positionA] - copyNumberArray[positionB] && positionA != positionB) {
                        zero = true;
                    }
                    line.append(",");
                } else {
                    line.append(copyNumberArray[positionA] - copyNumberArray[positionB]).append(",");
                }
            }
            if (highest_gain > 0 && !zero) {
                cnv_builder.append(line).append("\n");
            }
        }
    }

    private void updateSharedGenesBetweenGenomes(int[] copyNumberArray, HashMap<String, int[]> sharedGenesBetweenGenomes,
                                                 boolean unique) {

        for (String genomeA : genomeList) {
            int positionA = genomeList.indexOf(genomeA);
            for (String genomeB : genomeList) {
                int positionB = genomeList.indexOf(genomeB);
                if (genomeA.compareTo(genomeB) >= 0) {
                    continue;
                }

                if (copyNumberArray[positionA] == 0 && copyNumberArray[positionB] == 0) {
                    continue;
                }

                String genomeCombiKey = genomeA + "#" + genomeB;
                int[] shared_genes = sharedGenesBetweenGenomes.get(genomeCombiKey); //total all, total distinct, shared all, shared distinct, distinct informative all, distinct informative distinct, homologous genes of genome A to B,  homologous genes of genome B to A]

                shared_genes[1] += 1; // total distinct
                if (!unique) {
                    shared_genes[4] += 1;
                }
                if (copyNumberArray[positionA] >= copyNumberArray[positionB]) {
                    shared_genes[0] += copyNumberArray[positionA]; // total all
                    if (copyNumberArray[positionB] != 0) {
                        shared_genes[2] += copyNumberArray[positionB];
                        shared_genes[3] += 1;
                        if (!unique) {
                            shared_genes[5] += 1;
                        }
                    }
                } else { // fields[i] < fields[j]
                    shared_genes[0] += copyNumberArray[positionB];
                    if (copyNumberArray[positionA] != 0) {
                        shared_genes[2] += copyNumberArray[positionA];
                        shared_genes[3] += 1;
                        if (!unique) {
                            shared_genes[5] += 1;
                        }
                    }
                }
                if (copyNumberArray[positionB] != 0) { // if genome B has a copy, all of genome's A genes are considered homologous
                    shared_genes[6] += copyNumberArray[positionA];
                }
                if (copyNumberArray[positionA] != 0) { // vice versa
                    shared_genes[7] += copyNumberArray[positionB];
                }
                sharedGenesBetweenGenomes.put(genomeCombiKey, shared_genes);
            }
        }
    }

    private void fisherExactOnPhenotypes(HashMap<String, Integer> phenotypeGenomeCounts,
                                         HashMap<Double, ArrayList<String>> pheno_xsquare_map,
                                         Node homologyNode) {

        DecimalFormat formatter = new DecimalFormat("0.00");
        for (String phenotype : phenotypeGenomeCounts.keySet()) {
            int phenotypeGenomeCount = phenotypeGenomeCounts.get(phenotype); // found in number of genomes
            if ("?".equals(phenotype) || "Unknown".equals(phenotype) || phenotypeGenomeCount == 0) {
                continue;
            }
            int pheno_threshold = phenotype_threshold_map.get(phenotype); // requires to be present in this number of genomes
            int rest_current_pheno = pheno_threshold - phenotypeGenomeCount;
            int present_rest = 0, total_rest = 0;
            for (String other_phenotype : phenotype_threshold_map.keySet()) {
                int threshold_other_phenotype = phenotype_threshold_map.get(other_phenotype);
                if (other_phenotype.equals(phenotype)) {
                    continue;
                }
                if (other_phenotype.equals("?") || other_phenotype.equals("Unknown")) {
                    continue;
                }
                int count_other_pheno = 0;
                if (phenotypeGenomeCounts.containsKey(other_phenotype)) {
                    count_other_pheno = phenotypeGenomeCounts.get(other_phenotype);
                }
                total_rest += threshold_other_phenotype;
                if (count_other_pheno == 0) {
                    continue;
                }
                present_rest += count_other_pheno;
            }

            int not_present_rest = total_rest - present_rest;
            double[] ratios = classification.calculate_odds_ratios((double) phenotypeGenomeCount, (double) rest_current_pheno, (double) present_rest, (double) not_present_rest);
            String info = homologyNode.getId() + "," + phenotype + "," + phenotypeGenomeCount + "," + rest_current_pheno + "," + present_rest
                    + "," + not_present_rest + "," + formatter.format(ratios[0]);
            if (ratios[1] < 1.35 || ratios[2] > 0.65) {
                pheno_xsquare_map.computeIfAbsent(9999.0, k -> new ArrayList<>()).add(info);
            } else {
                double pval = classification.fisher_exact(phenotypeGenomeCount, rest_current_pheno, present_rest, not_present_rest);
                pheno_xsquare_map.computeIfAbsent(pval * 2, k -> new ArrayList<>()).add(info); //multiply p-value by 2 to make it two sided
            }
        }
    }

    /**
     * Create gene_classification_phenotype_overview.txt. Overview of the number
     * of TOTAL, EXCLUSIVE, SHARED and SPECIFIC groups per phenotype
     *
     * @param pheno_disrupt_count_map
     * @param phenotype_cnv
     * @param groupsPerClass holds homology_group nodes for 7 possible keys: "core", "accessory", "unique", a phenotypeValue with "#total" / "#shared" / "#exclusive" "#specific"
     */
    private void createPhenotypeOverview(HashMap<String, Integer> pheno_disrupt_count_map,
                                         HashMap<String, ArrayList<Node>> phenotype_cnv,
                                         HashMap<String, ArrayList<Node>> groupsPerClass) {

        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder output_builder = new StringBuilder();

        for (String phenotypeValue : phenotype_map.keySet()) {
            if (phenotypeValue.equals("?") || phenotypeValue.equals("Unknown") ) {
                output_builder.append("#Phenotype ").append(phenotypeValue).append(" are automatically excluded because they are unknown.\n\n");
                continue;
            }

            if (!groupsPerClass.containsKey(phenotypeValue + "#total")) {
                output_builder.append("#Phenotype ").append(phenotypeValue).append(" was excluded because all its genomes were skipped.\n\n");
                continue;
            }

            int total_shared = 0;
            if (groupsPerClass.containsKey(phenotypeValue  + "#shared")) {
                total_shared = groupsPerClass.get(phenotypeValue + "#shared").size();
            }

            int total_groups = groupsPerClass.get(phenotypeValue + "#total").size(); // total number of groups where genomes with this phenotype have at least 1 gene
            int pheno_threshold = phenotype_threshold_map.get(phenotypeValue);
            int[] genomesWithPhenotypeValue = phenotype_map.get(phenotypeValue);

            if (pheno_threshold == 0) {
                continue;
            }
            int total_exclusive = 0;
            if (groupsPerClass.containsKey(phenotypeValue + "#exclusive")) {
                total_exclusive = groupsPerClass.get(phenotypeValue + "#exclusive").size();
            }

            String percentage_ex = percentageAsString(total_exclusive, total_groups, 2);
            String percentage_sh = percentageAsString(total_shared, total_groups, 2);

            output_builder.append("#Phenotype ").append(phenotypeValue).append("\n")
                    .append(genomesWithPhenotypeValue.length).append(" genomes, a threshold of ").append(pheno_threshold).append(" genome(s) was used for shared and specific groups\n")
                    .append("Total: ").append(total_groups).append("\n")
                    .append("Exclusive: ").append(total_exclusive).append(", ").append(percentage_ex).append("%\n")
                    .append("Shared: ").append(total_shared).append(", ").append(percentage_sh).append("%\n")
                    .append("Specific: ");

            if (!groupsPerClass.containsKey(phenotypeValue + "#specific")) {
                output_builder.append(0);
            } else {
                int total_specific = groupsPerClass.get(phenotypeValue + "#specific").size();
                String percentage_sp = percentageAsString(total_specific, total_groups, 2);
                output_builder.append(total_specific).append(", ").append(percentage_sp).append("%");
            }

            output_builder.append("\nShared groups that are not specific as these were disrupted by:\n");
            for (String pheno_disrupt_key : pheno_disrupt_count_map.keySet()) {
                if (!pheno_disrupt_key.startsWith(phenotypeValue + "_")) {
                    continue;
                }
                int disrupt = pheno_disrupt_count_map.get(pheno_disrupt_key);
                output_builder.append(" ").append(pheno_disrupt_key.replace(phenotypeValue + "_", "")).append(": ").append(disrupt).append("\n");
            }

            output_builder.append("Extra copy/copies in number of groups compared to other phenotypes: ");
            boolean more_copies = false;
            for (String key : phenotype_cnv.keySet()) {
                if (!key.contains("#extra") || !key.startsWith(phenotypeValue)) {
                    continue;
                }
                String[] key_array = key.split("#");
                output_builder.append("\n ").append(key_array[1]).append(" ").append(phenotype_cnv.get(key).size());
                more_copies = true;
            }

            if (!more_copies) {
                output_builder.append("0 groups\n\n");
            } else {
                output_builder.append("\n\n");
            }
        }

        writeStringToFile("Threshold for phenotype specific & shared genes was " + phenotype_threshold +"%\n\n" + output_builder,
                WORKING_DIRECTORY + "gene_classification/gene_classification_phenotype_overview.txt", false, false);
    }

    /**
     *
     * @param geneCountPerSequence
     * @param unique unique groups cannot be informative
     */
    private void updateSharedGenesBetweenSequences(HashMap<String, int[]> sharedGenesBetweenSequences,
                                               HashMap<String, Integer> geneCountPerSequence, boolean unique) {

        if (!compare_sequences) {
            return;
        }
        ArrayList<String> addedSequences = new ArrayList<>();
        for (String sequenceIdA : geneCountPerSequence.keySet()) {
            int countSeqA = geneCountPerSequence.get(sequenceIdA);
            for (String sequenceIdB : selectedSequences) {
                if (addedSequences.contains(sequenceIdB)) {
                    continue;
                }
                int countSeqB = 0;
                if (geneCountPerSequence.containsKey(sequenceIdB)) {
                    countSeqB = geneCountPerSequence.get(sequenceIdB);
                }
                int[] geneCounts; // total all, total distinct, shared all, shared distinct, total informative distinct, shared informative distinct, all homologous genes seqA, all homologous genes seqB]
                String seqIdCombi = sequenceIdA + "#" + sequenceIdB;
                if (sharedGenesBetweenSequences.containsKey(sequenceIdA + "#" + sequenceIdB)) {
                    geneCounts = sharedGenesBetweenSequences.get(sequenceIdA + "#" + sequenceIdB);
                } else if (sharedGenesBetweenSequences.containsKey(sequenceIdB + "#" + sequenceIdA)) {
                    geneCounts = sharedGenesBetweenSequences.get(sequenceIdB + "#" + sequenceIdA);
                    seqIdCombi = sequenceIdB + "#" + sequenceIdA;
                } else {
                    geneCounts = new int [8];
                }

                geneCounts[1] += 1; // total distinct
                if (!unique) {
                    geneCounts[4] += 1;
                }
                if (countSeqA >= countSeqB) {
                    geneCounts[0] += countSeqA; // total all
                    if (countSeqB != 0) {
                        geneCounts[2] += countSeqB;
                        geneCounts[3] += 1;
                        if (!unique) {
                            geneCounts[5] += 1;
                        }
                    }
                } else { // fields[i] < fields[j]
                    geneCounts[0] += countSeqB;
                    if (countSeqA != 0) {
                        geneCounts[2] += countSeqA;
                        geneCounts[3] += 1;
                        if (!unique) {
                            geneCounts[5] += 1;
                        }
                    }
                }

                if (countSeqB != 0) {
                    geneCounts[6] += countSeqA;
                }
                if (countSeqA != 0) {
                    geneCounts[7] += countSeqB;
                }
                sharedGenesBetweenSequences.put(seqIdCombi, geneCounts);
            }
            addedSequences.add(sequenceIdA);
        }
    }

    /**
     * Based on skipped sequences, the original copy_number_array stored in homology nodes could be inaccurate
     */
    private int[] createCorrectedCopyNumberArray(HashMap<String, Integer> geneCopiesPerSequenceMap) {
        int[] correctedCopyNumberArray = new int[GENOME_DB.num_genomes]; //TODO: replace by genomeList.size() ?
        for (String sequenceIdentifier : geneCopiesPerSequenceMap.keySet()) {
            String[] sequenceIdArray = sequenceIdentifier.split("_");
            int genomeNr = Integer.parseInt(sequenceIdArray[0]);
            correctedCopyNumberArray[genomeNr-1] += geneCopiesPerSequenceMap.get(sequenceIdentifier);
        }
        return correctedCopyNumberArray;
    }

    private void printOutputFiles() {
        String dir = WORKING_DIRECTORY + "gene_classification/";
        String phasedHomologyTable = "", subgenomeDistanceRscript = "";
        if (PHASED_ANALYSIS) {
            phasedHomologyTable = dir + "the_phased_homology_table.csv";
            subgenomeDistanceRscript = dir + "subgenome_gene_distance_tree.R (Select one of the two distances)";
        }

        String mlsaSuggestions = "";
        if (!PROTEOME && Mode.contains("MLSA")) {
            mlsaSuggestions = dir + "mlsa_suggestions.txt";
        }
        String upset;
        if (genomeList.size() < 11) {
            upset = dir + "upset/upset_plot.R ";
        } else {
            upset = dir + "No UpSet plot was created because more than 10 genomes were included! ";
        }

        if (compare_sequences) {
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}gene_classification_overview.txt ", dir);
            Pantools.logger.info(" {}gene_classification_overview.csv ", dir);
            Pantools.logger.info(" {}classified_groups.csv", dir);
            Pantools.logger.info(" {}", phasedHomologyTable);
            Pantools.logger.info(" {}group_size_frequency.csv", dir);
            Pantools.logger.info(" {}allele_counts.csv", dir);
            Pantools.logger.info(" {}gene_copies.csv", dir);
            Pantools.logger.info(" {}additional_copies.csv", dir);
            Pantools.logger.info(" {}", mlsaSuggestions);
            Pantools.logger.info(" {}percentage_genes_shared_genome.csv", dir);
            Pantools.logger.info(" {}percentage_genes_shared_sequence.csv", dir);
            Pantools.logger.info(" {}", upset);
            if (PHASED_ANALYSIS) {
                Pantools.logger.info(" {}percentage_genes_shared_subgenome.csv", dir);
                Pantools.logger.info(" {}subgenome_presence.csv", dir);
            }
            Pantools.logger.info(" {}genome_gene_distance_tree.R (Select one of the three distances)", dir);
            Pantools.logger.info(" {}sequence_gene_distance_tree.R (Select one of the three distances)", dir);
            Pantools.logger.info(" {}", subgenomeDistanceRscript);
        } else {
            Pantools.logger.info("Output written to:");
            Pantools.logger.info(" {}gene_classification_overview.txt", dir);
            Pantools.logger.info(" {}gene_classification_overview.csv", dir);
            Pantools.logger.info(" {}classified_groups.csv", dir);
            Pantools.logger.info(" {}group_size_frequency.csv", dir);
            Pantools.logger.info(" {}allele_counts.csv", dir);
            Pantools.logger.info(" {}gene_copies.csv", dir);
            Pantools.logger.info(" {}additional_copies.csv", dir);
            Pantools.logger.info(" {}percentage_genes_shared_genome.csv", dir);
            Pantools.logger.info(" {}genome_gene_distance_tree.R (Select one of the three distances)", dir);
            Pantools.logger.info(" {}", upset);
        }
        
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}gene_classification_phenotype_overview.txt", dir);
            Pantools.logger.info(" {}phenotype_disrupted.csv", dir);
            Pantools.logger.info(" {}phenotype_additional_copies.csv ", dir);
            Pantools.logger.info(" {}phenotype_association.csv", dir);
        }

        Pantools.logger.info("Files with 'homology_group' node identifiers.");
        Pantools.logger.info(" {}group_identifiers/", dir);
    }

    private String determineGroupCategory(TreeSet<String> presentGenomes) {
        String groupCategory = null;
        if (presentGenomes.size() <= unique_threshold && presentGenomes.size() > 0 && core_threshold > 1) { // unique
            groupCategory = "unique";
        } else if (presentGenomes.size() >= core_threshold) { // core
            groupCategory = "core";
        } else if (presentGenomes.size() > 1) { // accessory
            groupCategory = "accessory";
        }
        if (groupCategory == null) {
            throw new RuntimeException("Something went really wrong. Issue lies with developer");
        }
        return groupCategory;
    }


    private String createPhasedHomologyTableLine(LinkedHashSet<String> subgenomeIdentifiers, HashMap<String, Integer> ploidyPerGenome) {
        if (!PHASED_ANALYSIS) {
            return "";
        }
        StringBuilder lastPartOfLne = new StringBuilder();
        HashSet<String> genomeNumbers = new HashSet<>(); //
        HashSet<String> subgenomesInGroup = new HashSet<>();
        int frequency = 0;
        for (String subgenomeId : subgenomeIdentifiers) { // example 4 A,4 B,4 C,4 D,4_unphased (genome 4)
            if (subgenomeCount.containsKey(subgenomeId)) {
                String[] subgenomeIdArray = subgenomeId.split("_"); // phase consists of genome number and haplotype phase (or 'unphased')
                genomeNumbers.add(subgenomeIdArray[0]);
                String genomrNr = subgenomeIdArray[0];
                int geneCount = subgenomeCount.get(subgenomeId);
                frequency += geneCount;
                lastPartOfLne.append(geneCount);
                if (!subgenomeId.contains("unphased") && ploidyPerGenome.get(genomrNr) > 1) { // unphased subgenomes in phased genomes are not allowed
                    subgenomesInGroup.add(subgenomeId);
                } else if (subgenomeId.contains("unphased") && ploidyPerGenome.get(genomrNr) == 1) {
                    // this is an unphased genome. genome is considered a subgenome
                    subgenomesInGroup.add(subgenomeId);
                }
            }
            lastPartOfLne.append(",");
        }
        return frequency + "," + genomeNumbers.size() + "," + subgenomesInGroup.size() + "," + lastPartOfLne;
    }

    /**
     * Gene distance (absence/presence) NJ tree
     */
    private void createGeneDistanceRscript(String genomeOrSubgenome) {
        String path = WD_full_path + "gene_classification/";
        String genomeCapitalised = capitalize_first_letter(genomeOrSubgenome);
        String R_LIB = check_r_libraries_environment();
        StringBuilder rscript = new StringBuilder ();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("# Use one of the following files\n")
                .append("# ").append(path).append("distances_for_tree/").append(genomeOrSubgenome).append("_distance_distinct_genes.csv\n");

       if (genomeOrSubgenome.equals("genome") || genomeOrSubgenome.equals("sequence")) {
            rscript.append("# ").append(path).append("distances_for_tree/").append(genomeOrSubgenome).append("_distance_inf_distinct_genes.csv\n");
        }
        rscript.append("# ").append(path).append("distances_for_tree/").append(genomeOrSubgenome).append("_distance_all_genes.csv\n\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"ape\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ape)\n\n")
                .append("input = read.csv(\"").append(path).append("distances_for_tree/").append(genomeOrSubgenome).append("_distance_distinct_genes.csv\", sep=\",\", header = TRUE)\n")
                .append("dataframe = subset(input, select = -c(").append(genomeCapitalised).append("s))\n")
                .append("df.distance = as.matrix(dataframe, labels=TRUE)\n")
                .append("colnames(df.distance) <- rownames(df.distance) <- input[['").append(genomeCapitalised).append("s']]\n")
                .append("NJ_tree <- nj(df.distance)\n")
                .append("write.tree(NJ_tree, tree.names = TRUE, file=\"").append(path).append( genomeOrSubgenome).append("_gene_distance.tree\")\n")
                .append("cat(\"\\nGene distance tree written to: ").append(path).append(genomeOrSubgenome).append("_gene_distance.tree\\n\\n\")");
        writeStringToFile(rscript.toString(), path + genomeOrSubgenome + "_gene_distance_tree.R", false, false);
    }

    // still need to include info like present in all subgenomes
    private void createThePhasedHomologyTable(HashMap<String, String> homologyTableLines,
                                              LinkedHashSet<String> selectedSubgenomes,
                                              HashMap<String, Integer> ploidyPerGenome) {
        if (!PHASED_ANALYSIS) {
            return;
        }

        // create the header. remove '_unphased' from unphased genomes. only leave it for subgenomes of unphased genomes
        StringBuilder header = new StringBuilder();
        int maxSubgenomes = 0;
        for (String subgenomeId : selectedSubgenomes) {
            if (subgenomeId.contains("unphased")) {
                String[] subgenomeIdArray = subgenomeId.split("_");
                if (ploidyPerGenome.get(subgenomeIdArray[0]) == 1) {
                    subgenomeId = "Genome " + subgenomeId.replace("_unphased","");
                    maxSubgenomes++;
                }
                header.append(subgenomeId).append(",");
            } else {
                header.append(subgenomeId).append(",");
                maxSubgenomes++;
            }
        }

        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/the_phased_homology_table.csv"))) {
            out.write("Homology group,Frequency,Found in number of genomes (maximum of " + genomeList.size() + "),Found in number of subgenomes (maximum of " + maxSubgenomes + ")," + header + "\n");
            for (String homologyIdKey : homologyTableLines.keySet()) {
                if (!homologyIdKey.contains("phased")) {
                    continue;
                }
                out.write(homologyIdKey.replace("_phased","") + "," + homologyTableLines.get(homologyIdKey) + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to: " + WORKING_DIRECTORY + "gene_classification/the_phased_homology_table.csv");
        }
    }

    /**
     * Genomes that are unphased (ploidy of 1) have "_unphased" in the identifier
     */
    private String renameSubgenomeIdForUnphasedGenomes(String subgenomeId, HashMap<String, Integer> ploidyPerGenome) {
        if (subgenomeId.contains("unphased")) {
            String[] subgenomeIdArray = subgenomeId.split("_");
            String genome = subgenomeIdArray[0];
            if (ploidyPerGenome.get(genome) == 1) {
                subgenomeId = subgenomeId.replace("_unphased","");
            }
        }
        return subgenomeId;
    }

    /**
     * * Creates classified_groups.csv, overview of the groups with the class
     * (core,accessory, unique) and phenotype class (if applicable) and phased
     * information (if applicable).
     *
     * @param groupsPerClass holds homology_group nodes for 7 possible keys: "core", "accessory", "unique", a phenotypeValue with "#total" / "#shared" / "#exclusive" "#specific"
     * @param singleCopyGroupNodes
     * @param homologyTableLines
     */
    private void createTheHomologyTable(HashMap<String, ArrayList<Node>> groupsPerClass, ArrayList<Node> singleCopyGroupNodes,
                                         HashMap<String, String> homologyTableLines) {

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/classified_groups.csv"))) {
                out.write("Homology group node id,class,");
                if (PHENOTYPE != null) {
                    out.write("phenotype info,");
                }
                for (String genome : genomeList) {
                    String add = get_phenotype_for_genome(genome, true);
                    if (!genomeList.contains(genome)) {
                        out.write("Skipped genome ");
                    } else {
                        out.write("Genome ");
                    }
                    out.write(genome + add);
                    if (genomeList.indexOf(genome) != genomeList.size() - 1) {
                        out.write(",");
                    }
                }
                out.write("\n");
                String[] class_array = {"core", "accessory", "unique"};
                for (String class1 : class_array) {
                    ArrayList<Node> homologyNodes = groupsPerClass.get(class1);
                    if (homologyNodes == null) {
                        continue;
                    }
                    for (Node homologyNode : homologyNodes) {
                        String[] info = homologyTableLines.get(homologyNode.getId() + "").split("#"); // currently holds the gene copies per genome
                        String phenotypeInfo = "";
                        if (PHENOTYPE != null) {
                            phenotypeInfo = info[1] + ",";
                        }

                        if (class1.equals("core") && singleCopyGroupNodes.contains(homologyNode)) {
                            out.write(homologyNode.getId() + ",core & single copy orthologous"  + "," + phenotypeInfo);
                        } else {
                            out.write(homologyNode.getId() + "," + class1 + "," + phenotypeInfo);
                        }
                        out.write(info[0] + "\n");
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to write to: " + WORKING_DIRECTORY + "gene_classification/classified_groups.csv");
            }
            tx.success(); // transaction successful, commit changes
        }
    }

    /**
     *
     * @return key is the genome number with chromosome number, value is a list with sequence identifiers
     */
    private HashMap<String, ArrayList<String>> createSequencesPerChromosomeGenome() {
        HashMap<String, ArrayList<String>> sequencesPerChromosome = new HashMap<>();
        if (phasingInfoMap.isEmpty()) {
            return sequencesPerChromosome;
        }
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] seqIdArray = sequenceId.split("_");
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            sequencesPerChromosome.computeIfAbsent(seqIdArray[0] + "_" + phasingInfo[0], k -> new ArrayList<>()).add(sequenceId);
        }
        return sequencesPerChromosome;
    }

    private String createGeneCountExplanation(String genomeOrSequence) {
        return "#Three different gene counts:\n"
                + "# DISTINCT  : always count a maximum 1\n"
                + "# SEMI      : Counts gene copies of " + genomeOrSequence + " A up to number of copies in " + genomeOrSequence + " B.\n"
                + "# EVERYTHING: Count all genes of " + genomeOrSequence + " A as long as the other " +  genomeOrSequence + " has a copy.\n";
    }

    private String createTableExplanations(String genomeOrSequence, String[] averageWithinGenomes) {
        String semiAverage = "", distinctAverage = "", everythingAverage = "";
        if (averageWithinGenomes != null) {
            semiAverage = averageWithinGenomes[0] + "\n";
            distinctAverage = averageWithinGenomes[1] + "\n";
            everythingAverage = averageWithinGenomes[2] + "\n";
        }
        return "#Percentages of genes shared between two " + genomeOrSequence + "s. Gene counts are divided either by the sum of one or both " + genomeOrSequence + "s\n"
                + "#1 Percentage SEMI gene counts. Derived from #11 & #12\n"
                + semiAverage
                + "#2 Percentage SEMI gene counts, genome left column. Derived from #11 & #11 diagonal\n"
                + "#3 Percentage DISTINCT gene counts. Derived from #7 & #8\n"
                + distinctAverage
                + "#4 Percentage DISTINCT based on " + genomeOrSequence + " in left column. Derived from #7 & #7 diagonal\n"
                + "#5 Percentage informative DISTINCT based on both " + genomeOrSequence + "s. Derived from #9 & #10\n"
                + "#6 Percentage EVERYTHING gene counts based on " + genomeOrSequence + " in left column. Derived from #13 & #13 diagonal\n"
                + everythingAverage
                + "\n"
                + "#Gene counts between two " + genomeOrSequence + "s\n"
                + "#7 shared DISTINCT genes\n"
                + "#8 unshared DISTINCT genes\n"
                + "#9 shared informative DISTINCT genes\n"
                + "#10 unshared informative DISTINCT genes\n"
                + "#11 SEMI shared genes\n"
                + "#12 SEMI unshared genes\n"
                + "#13 EVERYTHING shared genes\n";
    }

    /**
     *
     * @param geneCountPerSequence


     */
    private HashMap<String, Integer> countGenesPerSubgenome(HashMap<String, Integer> geneCountPerSequence) {
        // count all genes belonging to a subgenome
        HashMap<String, Integer> geneCountPerSubgenome = new HashMap<>();
        for (String sequenceId : geneCountPerSequence.keySet()) {
            String subgenome = getSubgenomeId(sequenceId, true);
            geneCountPerSubgenome.merge(subgenome, geneCountPerSequence.get(sequenceId), Integer::sum);
        }
        return geneCountPerSubgenome;
    }

    private void countGenesBetweenSubgenomes(HashMap<String, Integer> geneCountPerSubgenome,
                                                            HashMap<String, int[]> sharedGenesBetweenSubgenomes) {

        boolean unique = true;
        ArrayList<String> addedSequences = new ArrayList<>();
        for (String subgenomeA : geneCountPerSubgenome.keySet()) {
            int gene_count_seq1 = geneCountPerSubgenome.get(subgenomeA);
            for (String seq_id2 : selectedSubgenomes) {
                if (addedSequences.contains(seq_id2)) {
                    continue;
                }
                int gene_count_seq2 = 0;
                if (geneCountPerSubgenome.containsKey(seq_id2)) {
                    gene_count_seq2 = geneCountPerSubgenome.get(seq_id2);
                }
                int[] geneCounts; // total all, total distinct, shared all, shared distinct, total informative distinct, shared informative distinct, all homologous genes seqA, all homologous genes seqB]
                String seq_combi = subgenomeA + "#" + seq_id2;
                if (sharedGenesBetweenSubgenomes.containsKey(subgenomeA + "#" + seq_id2)) {
                    geneCounts = sharedGenesBetweenSubgenomes.get(subgenomeA + "#" + seq_id2);
                } else if (sharedGenesBetweenSubgenomes.containsKey(seq_id2 + "#" + subgenomeA)) {
                    geneCounts = sharedGenesBetweenSubgenomes.get(seq_id2 + "#" + subgenomeA);
                    seq_combi = seq_id2 + "#" + subgenomeA;
                } else {
                    geneCounts = new int[8];
                }

                geneCounts[1] += 1; // total distinct
                if (!unique) {
                    geneCounts[4] += 1;
                }
                if (gene_count_seq1 >= gene_count_seq2) {
                    geneCounts[0] += gene_count_seq1; // total all
                    if (gene_count_seq2 != 0) {
                        geneCounts[2] += gene_count_seq2;
                        geneCounts[3] += 1;
                        if (!unique) {
                            geneCounts[5] += 1;
                        }
                    }
                } else { // fields[i] < fields[j]
                    geneCounts[0] += gene_count_seq2;
                    if (gene_count_seq1 != 0) {
                        geneCounts[2] += gene_count_seq1;
                        geneCounts[3] += 1;
                        if (!unique) {
                            geneCounts[5] += 1;
                        }
                    }
                }

                if (gene_count_seq2 != 0) {
                    geneCounts[6] += gene_count_seq1;
                }
                if (gene_count_seq1 != 0) {
                    geneCounts[7] += gene_count_seq2;
                }
                sharedGenesBetweenSubgenomes.put(seq_combi, geneCounts);
            }
            addedSequences.add(subgenomeA);
        }
    }

    /**
     * Check which genome has at least one gene/kmer/function
     * @param copy_number_array
     * @param genomeList genome (numbers) of current analysis
     * @return list with genome numbers. genome number represents a gene in that specific genome
     */
    private ArrayList<String> countGenesPerGenome(int[] copy_number_array, ArrayList<String> genomeList) {
        ArrayList<String> genomeNumbers = new ArrayList<>();
        for (int i = 0; i < copy_number_array.length; ++i) {

            if (copy_number_array[i] > 0) {
                for (int j = 0; j < copy_number_array[i]; ++j) {
                    genomeNumbers.add(genomeList.get(i));
                }
            }
        }
        return genomeNumbers;
    }

    private HashMap<String, Integer> convertGeneCopiesArrayToMap(int[][] geneCopiesPerSequenceArray) {
        HashMap<String, Integer> geneCopiesPerSequenceMap = new HashMap<>();
        if (!compare_sequences) {
            return geneCopiesPerSequenceMap;
        }

        for (String genome : genomeList) {
            try {
                int genomeNr = Integer.parseInt(genome);
                for (int sequenceNr = 1; sequenceNr <= GENOME_DB.num_sequences[genomeNr]; ++sequenceNr) {
                    if (geneCopiesPerSequenceArray[genomeNr - 1][sequenceNr - 1] == 0 || !selectedSequences.contains(genome + "_" + sequenceNr)) {
                        continue;
                    }
                    geneCopiesPerSequenceMap.merge(genome + "_" + sequenceNr, geneCopiesPerSequenceArray[genomeNr - 1][sequenceNr - 1], Integer::sum);
                }
            } catch (NumberFormatException ignored) {
            }
        }
        return geneCopiesPerSequenceMap;
    }

    /**
     *
     * @param geneCopiesPerSequenceMap
     */
    private void countSubgenomes(HashMap<String, Integer> geneCopiesPerSequenceMap,
                                 HashMap<String, ArrayList<String>> sequencesPerChromosomeGenome) {
        if (!PHASED_ANALYSIS) {
            return;
        }
        subgenomeCount = new HashMap<>();
        haplotypesWithinChromosome = new HashMap<>();

        for (String sequenceIdentifier : geneCopiesPerSequenceMap.keySet()) {
            String[] sequenceIdArray = sequenceIdentifier.split("_"); // 1_1 becomes [1,1] [genome number, sequence number]
            int geneCopies = geneCopiesPerSequenceMap.get(sequenceIdentifier);
            String[] phasingInfo = phasingInfoMap.get(sequenceIdentifier);  // [1, B, 1B, 1_B]
            String chromosome_key = null;
            String phasing_id = "unphased";
            String haplotypeLetter = "unphased";
            int chromosome_threshold = 0;
            if (phasingInfo != null) {
                haplotypeLetter = phasingInfo[1];
                if (sequencesPerChromosomeGenome.containsKey(sequenceIdArray[0] + "_" + phasingInfo[0])) {
                    chromosome_threshold = sequencesPerChromosomeGenome.get(sequenceIdArray[0] + "_" + phasingInfo[0]).size();
                }
                phasing_id = phasingInfo[3]; // 1_A, chromosome 1 haplotype A
                chromosome_key = sequenceIdArray[0] + "_" + phasingInfo[0]; // 1_1, genome 1 chromosome 1
            }

            subgenomeCount.merge(sequenceIdArray[0] + "_" + haplotypeLetter, geneCopies, Integer::sum);
            if (chromosome_key == null || phasing_id.equals("unphased") || (chromosome_threshold > 1 && haplotypeLetter.equals("unphased"))) {
                // if a chromosome is phased, don't count the unphased
                // if not phased, its ok
                continue;
            }
            String[] phasing_id_array = phasing_id.split("_"); // 1_A split into [1,A]
            haplotypesWithinChromosome.computeIfAbsent(chromosome_key, k -> new HashSet<>()).add(phasing_id_array[1]);

        }
    }

    /**
     * Write to haplotype_presence output files required for the PanTools function sequence_visualization
     * @param phasing_count_position
     * @param hm_node_id
     * @param phasing_count_writers WORKING_DIRECTORY + "gene_classification/haplotype_presence/
     */
    private void summarize_phasing_counts(HashMap<String, Integer> phasing_count_position, long hm_node_id,
                                                BufferedWriter[] phasing_count_writers) {

        if (!PHASED_ANALYSIS) {
            return;
        }
        for (String chromosome_key : haplotypesWithinChromosome.keySet()) {
            int file = phasing_count_position.get(chromosome_key);
            BufferedWriter writer = phasing_count_writers[file];
            try {
                writer.write(hm_node_id + "," + haplotypesWithinChromosome.get(chromosome_key).size() + "\n");
            } catch (IOException ignored) {
            }
        }
    }

    /**
     * Check if the number of genomes from phenotype values have reached the threshold
     * @param phenotypeGenomeCounts
     * @param homologyNode
     * @param pheno_disrupt_count_map
     * @param phenotype_min_max
     * @param disrupted_builder
     * @param groupsPerClass holds homology_group nodes for 7 possible keys: "core", "accessory", "unique", a phenotypeValue with "#total" / "#shared" / "#exclusive" "#specific"
     * @return list where phenotype values are combined with "#" "specific", "exclusive" or "shared"
     */
    public ArrayList<String> findPhenotypeSharedSpecificExclusive(HashMap<String, Integer> phenotypeGenomeCounts, Node homologyNode,
                                                                  HashMap<String, Integer> pheno_disrupt_count_map,
                                                                  HashMap<String, int[]> phenotype_min_max, StringBuilder disrupted_builder,
                                                                  HashMap<String, ArrayList<Node>> groupsPerClass) {

        ArrayList<String> categories = new ArrayList<>();
        for (String phenotypeValue : phenotypeGenomeCounts.keySet()) {
            StringBuilder pheno_disrupted_builder = new StringBuilder();
            int phenotypeGenomeCount = phenotypeGenomeCounts.get(phenotypeValue); // found in number of genomes
            if ("?".equals(phenotypeValue) || "Unknown".equals(phenotypeValue) || phenotypeGenomeCount == 0) {
                continue;
            }
            groupsPerClass.computeIfAbsent(phenotypeValue + "#total", k -> new ArrayList<>()).add(homologyNode);
            int pheno_threshold = phenotype_threshold_map.get(phenotypeValue); // requires to be present in this number of genomes
            boolean enough = false, disrupted = false;
            if (phenotypeGenomeCount >= pheno_threshold) {
                enough = true; // number of phenotype members meets the threshold
                pheno_disrupted_builder.append(homologyNode.getId()).append(",").append(phenotypeValue).append(",");
            }

            for (String other_phenotype : phenotype_threshold_map.keySet()) {
                if (other_phenotype.equals(phenotypeValue)) {
                    pheno_disrupted_builder.append(",");
                    continue;
                }
                if (other_phenotype.equals("?") || other_phenotype.equals("Unknown")) { // unknown phenotype cannot disrupt
                    continue;
                }
                int count_other_pheno = 0;
                if (phenotypeGenomeCounts.containsKey(other_phenotype)) {
                    count_other_pheno = phenotypeGenomeCounts.get(other_phenotype);
                }
                if (count_other_pheno == 0) {
                    pheno_disrupted_builder.append(",");
                    continue;
                }
                if (enough) {
                    pheno_disrupt_count_map.merge(phenotypeValue + "_" + other_phenotype, 1, Integer::sum);
                    pheno_disrupted_builder.append(count_other_pheno).append(",");
                }
                disrupted = true;
            }

            if (enough && !disrupted) { // group is phenotype specific
                groupsPerClass.computeIfAbsent(phenotypeValue + "#specific", k -> new ArrayList<>()).add(homologyNode);
                int[] class_value = {2}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotypeValue + "_class", class_value);
                categories.add(phenotypeValue + " specific");
            }
            if (!enough && !disrupted) { // group is exclusive
                groupsPerClass.computeIfAbsent(phenotypeValue + "#exclusive", k -> new ArrayList<>()).add(homologyNode);
                categories.add(phenotypeValue + " exclusive");
            }

            if (enough) {
                int[] class_value = {1}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotypeValue + "_class", class_value);
                groupsPerClass.computeIfAbsent(phenotypeValue + "#shared", k -> new ArrayList<>()).add(homologyNode);
                if (disrupted) {
                    disrupted_builder.append(pheno_disrupted_builder).append("\n");
                    categories.add(phenotypeValue + " shared");
                }
            } else {
                int[] class_value = {0}; // 0 means not enough for shared or specific, 1 is shared, 2 is specific.
                phenotype_min_max.put(phenotypeValue + "_class", class_value);
            }
        }
        return categories;
    }

    /**
     * Creates and appends additional_copies.csv. As the size of this file can
     * become very big, the file is written & appended every XX homology groups,
     * depending on the pangenome size.
     *
     * @param additional_copies
     * @param hmgroup_counter
     * @param ignore_counts
     * @param write_threshold output is written every this many groups
     */
    private void create_gclass_additional_copies(StringBuilder additional_copies, int hmgroup_counter, boolean ignore_counts, int write_threshold) {
        if (hmgroup_counter == write_threshold || (ignore_counts && hmgroup_counter <= write_threshold)) {
            if (genomeList.size() == 1) {
                write_string_to_file_full_path("No additional copies can be found when only 1 genome is selected",
                        WORKING_DIRECTORY + "gene_classification/additional_copies.csv");
            } else {
                String header = create_header_for_additional_copies();
                write_string_to_file_full_path(header + "\n" + additional_copies,  WORKING_DIRECTORY + "gene_classification/additional_copies.csv");
            }
        } else if (hmgroup_counter % write_threshold == 0 || ignore_counts) {
            if (genomeList.size() > 1) {
                writeStringToFile("\n" + additional_copies.toString(), WORKING_DIRECTORY + "gene_classification/additional_copies.csv", false, true);
            }
        }
    }

    /**
     * @return a header
     */
    private String create_header_for_additional_copies() {
        StringBuilder header = new StringBuilder("#Please be aware that not every genome will have a gene in accessory groups\n" +
                "Homology group,Class,Genome number,Number of copies,");
        for (String genome : genomeList) {
            String phenotype = get_phenotype_for_genome(genome, true);
            header.append("Genome ").append(genome).append(phenotype).append(",");
        }
        return header.toString().replaceFirst(".$","");
    }

    public HashMap<String, ArrayList<String>> createHaplotypeLettersPerGenome() {
        HashMap<String, ArrayList<String>> genomePhases = new HashMap<>();
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] sequenceIdArray = sequenceId.split("_"); // 1_1 becomes [1, 1]
            String[] phasingInfo = phasingInfoMap.get(sequenceId);  // [1, B, 1B, 1_B]
            String genome = sequenceIdArray[0];
            ArrayList<String> haplotypeLetters;
            if (genomePhases.containsKey(genome)) {
                haplotypeLetters = genomePhases.get(genome);
            } else {
                haplotypeLetters = new ArrayList<>();
            }
            if (!haplotypeLetters.contains(phasingInfo[1])) {
                haplotypeLetters.add(phasingInfo[1]);
            }
            genomePhases.put(genome, haplotypeLetters);
        }
        return genomePhases;
    }

    /**
     * @return a list of chromosome numbers found in the pangenome
     */
    private ArrayList<Integer> determineChromosomeNumber() {
        TreeSet<Integer> chromosomeNumbers = new TreeSet<>();
        if (phasingInfoMap.isEmpty()) {
            return new ArrayList<>();
        }
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            chromosomeNumbers.add(Integer.parseInt(phasingInfo[0]));
        }
        return new ArrayList<>(chromosomeNumbers);
    }

    /**
     * Creates gene_classification_overview.txt/csv , file that summerizes most statistics of the gene_classification analysis
     * @param total_hmgroups number of homology groups in current analysis
     * @param totalScoNodes number of single-copy homology groups
     * @param totalPhasedScoNodes number of single-copy homology groups when considering subgenomes instead of genome
     * @param core_access_uni_map
     * @param total_core_groups  number core homology groups
     * @param total_unique_groups number unique homology groups
     * @param subgenomeCountsPerGenome
     * @param mrnasWithoutHomology array with the number of mRNAs (per genome) that do not cluster to any other mRNA
     * @param alleleCountStatistics
     * @param alleleCountStatisticsLT
     * @param geneCopyStatisticsMap
     * @param geneCopyStatisticsMapLT
     * @param highestPloidyLevel
     */
    private void createGeneClassificationOverviewTxtCsv(int total_hmgroups, int totalScoNodes, int totalPhasedScoNodes,
                                                        HashMap<String, int[]> core_access_uni_map, int total_core_groups, int total_unique_groups,
                                                        HashMap<String, ArrayList<Integer>> subgenomeCountsPerGenome,
                                                        HashMap<String, Integer> mrnasWithoutHomology,
                                                        HashMap<String, double[]> alleleCountStatistics,
                                                        HashMap<String, double[]> alleleCountStatisticsLT,
                                                        HashMap<String, double[]> geneCopyStatisticsMap,
                                                        HashMap<String, double[]> geneCopyStatisticsMapLT, int highestPloidyLevel) {

        int sequencesInPangenome = 0;
        if (!PROTEOME) { // a panproteome doesn't have sequences
            for (int i = 1; i <= GENOME_DB.num_genomes; ++i) { // don't replace by genomeList since this is about the sequences
                sequencesInPangenome += GENOME_DB.num_sequences[i];
            }
        }

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder output = new StringBuilder();
        StringBuilder csv_seq_builder = new StringBuilder();
        String genomeString = "Genome";
        if (compare_sequences) {
            genomeString = "Genome/Sequence";
            csv_seq_builder.append("\n");
        }

        StringBuilder csvGenome = new StringBuilder(genomeString + ",Total groups,Core groups,Accessory groups,Unique groups," +
                "Total mRNAs,Core mRNAs,% of genome core,Accessory mRNAs,% genome accessory,Unique mRNAs,% of genome unique,Singleton groups (unique groups with only one mRNA)," +
                "Median gene copies per group,Average gene copies per group,Standard deviation - gene copies per group," +
                "Median gene copies per group (longest transcripts),Average gene copies per group (longest transcripts),Standard deviation - Gene copies per group (longest transcripts)," +
                "Median CDS alleles per group,Average CDS alleles per group,Standard deviation - CDS alleles per group," +
                "Median CDS alleles per group (longest transcripts),Average CDS alleles per group (longest transcripts),Standard deviation - CDS alleles per group (longest transcripts)," +
                "Median protein alleles per group,Average protein alleles per group,Standard deviation - protein alleles per group," +
                "Median protein alleles per group (longest transcripts),Average protein alleles per group (longest transcripts),Standard deviation - protein alleles per group (longest transcripts),");

        if (PHASED_ANALYSIS) {
            for (int i = 1; i <= highestPloidyLevel; i++) {
                csvGenome.append("Groups with genes in " + i + " subgenomes,");
            }
            for (int i = 1; i <= highestPloidyLevel; i++) {
                csvGenome.append("% of groups with genes in " + i + " subgenomes,");
            }
            csvGenome.append("\n");
        } else {
            csvGenome.append("\n");
        }

        int mrna_pangenome_count = 0;
        HashMap<String, ArrayList<Double>> percentagesPerCategory = new HashMap<>();
        // hashmaps has 6 keys, combination of 'core', 'accessory' 'unique' with 'genome' or 'sequence'

        for (String genome : genomeList) {
            if (!genomeList.contains(genome)) {
                output.append("Skipped genome ").append(genome).append("\n\n");
                continue;
            }
            int[] total_gene_counts = core_access_uni_map.get(genome + "_total"); // core, accessory, unique
            int[] distinct_gene_counts = core_access_uni_map.get(genome + "_distinct"); // core, accessory, unique
            int total_mrnas = total_gene_counts[2] + total_gene_counts[1] + total_gene_counts[0];
            mrna_pangenome_count += total_mrnas;
            String phenotype = get_phenotype_for_genome(genome, false);
            if (phenotype.length() != 0) {
                phenotype = ", (" + phenotype + ")";
            }

            output.append("#Genome ").append(genome).append(phenotype).append(". ");
            if (!PROTEOME) {
                int sequencesInAnalysis = countIncludedSequenceOfGenome(genome);
                int nrSequences = 0;
                try {
                    nrSequences = GENOME_DB.num_sequences[Integer.parseInt(genome)];
                } catch (NumberFormatException ignored) {
                }
                output.append(sequencesInAnalysis).append("/").append(nrSequences).append(" sequences");
            }

            if (total_mrnas == 0) { // this genome has no genes
                output.append("\nTotal mRNAs: 0\n\n");
                continue;
            }
            csvGenome.append(genome).append(",");

            calculate_c_a_u_percentages_and_append(total_gene_counts, distinct_gene_counts, output, csvGenome,
                    percentagesPerCategory, "genome");
            output.append("mRNAs without homology: " + mrnasWithoutHomology.get(genome) + " " + percentageAsString(mrnasWithoutHomology.get(genome), total_mrnas, 2) + "%\n");
            csvGenome.append(",").append(mrnasWithoutHomology.get(genome)); // singleton groups

            if (genome != null && genome.matches("[0-9]+") && !PROTEOME) { //basically, check if genome number (and not accession) in a pangenome
                double[] alleleProtStatistics = alleleCountStatistics.get(genome + "#protein");
                double[] alleleCDSStatistics = alleleCountStatistics.get(genome + "#cds");
                double[] geneCopyStatistics = geneCopyStatisticsMap.get(genome + "#cds");
                output.append("\nAlleles/gene copies per group: median, average, standard deviation\n");
                output.append("Gene copies             : " + geneCopyStatistics[0] + ", " + df.format(geneCopyStatistics[1]) + ", " + df.format(geneCopyStatistics[2]) + "\n");
                csvGenome.append(",").append(geneCopyStatistics[0]).append(",").append(df.format(geneCopyStatistics[1])).append(",").append(df.format(geneCopyStatistics[2]));
                output.append("Distinct CDS alleles    : " + alleleCDSStatistics[0] + ", " + df.format(alleleCDSStatistics[1]) + ", " + df.format(alleleCDSStatistics[2]) + "\n");
                csvGenome.append(",").append(alleleCDSStatistics[0]).append(",").append(df.format(alleleCDSStatistics[1])).append(",").append(df.format(alleleCDSStatistics[2]));
                output.append("Distinct protein alleles: " + alleleProtStatistics[0] + ", " + df.format(alleleProtStatistics[1]) + ", " + df.format(alleleProtStatistics[2]) + "\n\n");
                csvGenome.append(",").append(alleleProtStatistics[0]).append(",").append(df.format(alleleProtStatistics[1])).append(",").append(df.format(alleleProtStatistics[2]));

                double[] alleleProtStatisticsLT = alleleCountStatisticsLT.get(genome + "#protein");
                double[] alleleCDSStatisticsLT = alleleCountStatisticsLT.get(genome + "#cds");
                double[] geneCopyStatisticsLT = geneCopyStatisticsMapLT.get(genome + "#cds");
                output.append("Only counting the longest transcripts\n");
                if (geneCopyStatisticsLT != null) {
                    output.append("Gene copies             : " + geneCopyStatisticsLT[0] + ", " + df.format(geneCopyStatisticsLT[1]) + ", " + df.format(geneCopyStatisticsLT[2]) + "\n");
                    csvGenome.append(",").append(geneCopyStatisticsLT[0]).append(",").append(df.format(geneCopyStatisticsLT[1])).append(",").append(df.format(geneCopyStatisticsLT[2]));
                }
                if (alleleCDSStatisticsLT != null) {
                    output.append("Distinct CDS alleles    : " + alleleCDSStatisticsLT[0] + ", " + df.format(alleleCDSStatisticsLT[1]) + ", " + df.format(alleleCDSStatisticsLT[2]) + "\n");
                    csvGenome.append(",").append(alleleCDSStatisticsLT[0]).append(",").append(df.format(alleleCDSStatisticsLT[1])).append(",").append(df.format(alleleCDSStatisticsLT[2]));
                }
                if (alleleProtStatisticsLT != null) {
                    output.append("Distinct protein alleles: " + alleleProtStatisticsLT[0] + ", " + df.format(alleleProtStatisticsLT[1]) + ", " + df.format(alleleProtStatisticsLT[2]) + "\n\n");
                    csvGenome.append(",").append(alleleProtStatisticsLT[0]).append(",").append(df.format(alleleProtStatisticsLT[1])).append(",").append(df.format(alleleProtStatisticsLT[2]));
                }
            }

            if (PHASED_ANALYSIS) {
                String subgenomePresence = subgenomePresenceReadableText(subgenomeCountsPerGenome.get(genome));
                String subgenomePresenceCSV = subgenomePresenceCSV(subgenomeCountsPerGenome.get(genome),
                        (distinct_gene_counts[0] +  distinct_gene_counts[1] + distinct_gene_counts[2]), highestPloidyLevel);
                output.append("Homology groups with genes in number of subgenome\n").append(subgenomePresence).append("\n");
                csvGenome.append(",").append(subgenomePresenceCSV);
            }
            csvGenome.append("\n");


            if (!compare_sequences) {
                output.append("\n");
                continue;
            }
            int genomeNr;
            try {
                genomeNr = Integer.parseInt(genome);
            } catch (NumberFormatException ignored) {
                Pantools.logger.debug("Could not parse genome number {} to integer. Skipping genome for sequence comparison.", genome);
                continue;
            }

            StringBuilder seqOutputBuilder = new StringBuilder();
            for (int sequenceNr = 1; sequenceNr <= GENOME_DB.num_sequences[genomeNr]; ++sequenceNr) {
                if (!selectedSequences.contains(genome + "_" + sequenceNr)) {
                    continue;
                }
                String add = "";
                if (PHASED_ANALYSIS) {
                    String[] phasingInfo = phasingInfoMap.get(genome + "_" + sequenceNr); // [1, B, 1B, 1_B]
                    if (phasingInfo == null) {
                        add = ". Has no phasing information";
                    } else {
                        add = ". Phasing identifier " + phasingInfo[3];
                    }
                }
                seqOutputBuilder.append("Sequence ").append(genome).append("_").append(sequenceNr).append(add);
                int[] total_gene_counts_seq = core_access_uni_map.get(genome + "_" + sequenceNr + "_total");
                int[] distinct_gene_counts_seq = core_access_uni_map.get(genome + "_" + sequenceNr + "_distinct");
                csv_seq_builder.append(genome).append("_").append(sequenceNr).append(",");
                calculate_c_a_u_percentages_and_append(total_gene_counts_seq, distinct_gene_counts_seq, seqOutputBuilder,
                        csv_seq_builder, percentagesPerCategory, "sequence_" + genome);
                csv_seq_builder.append("\n");
            }

            double[] coreSequenceStatistics;
            double[] accessorySequenceStatistics;
            double[] uniqueSequenceStatistics;
            if (percentagesPerCategory.containsKey("sequence_" + genome + "_core")) {
                ArrayList<Double> corePercentagesGenome = percentagesPerCategory.get("sequence_" + genome + "_core");
                coreSequenceStatistics = median_average_stdev_from_AL_double(corePercentagesGenome);
                percentagesPerCategory.remove("sequence_" + genome + "_core");
                percentagesPerCategory.put("sequence_core", corePercentagesGenome);
            } else {
                coreSequenceStatistics = new double[7];
            }

            if (percentagesPerCategory.containsKey("sequence_" + genome + "_accessory")) {
                ArrayList<Double> accessoryPercentagesGenome = percentagesPerCategory.get("sequence_" + genome + "_accessory");
                accessorySequenceStatistics = median_average_stdev_from_AL_double(accessoryPercentagesGenome);
                percentagesPerCategory.remove("sequence_" + genome + "_accessory");
                percentagesPerCategory.put("sequence_accessory", accessoryPercentagesGenome);
            } else {
                accessorySequenceStatistics = new double[7];
            }

            if (percentagesPerCategory.containsKey("sequence_" + genome + "_unique")) {
                ArrayList<Double> uniquePercentagesGenome = percentagesPerCategory.get("sequence_" + genome + "_unique");
                uniqueSequenceStatistics = median_average_stdev_from_AL_double(uniquePercentagesGenome);
                percentagesPerCategory.remove("sequence_" + genome + "_unique");
                percentagesPerCategory.put("sequence_unique", uniquePercentagesGenome);
            } else {
                uniqueSequenceStatistics = new double[7];
            }

            output.append("Average % of genes per class for SEQUENCES of genome " + genome + ", standard deviation, median, range\n"
                        + "Core     : " + df.format(coreSequenceStatistics[1]) + ", " + df.format(coreSequenceStatistics[2]) + ", " + df.format(coreSequenceStatistics[0]) + ", "
                        + df.format(coreSequenceStatistics[3]) + "-" + df.format(coreSequenceStatistics[4]) + "%\n"
                        + "Accessory: " + df.format(accessorySequenceStatistics[1]) + ", " + df.format(accessorySequenceStatistics[2]) + ", " + df.format(accessorySequenceStatistics[0]) + ", "
                        + df.format(accessorySequenceStatistics[3]) + "-" + df.format(accessorySequenceStatistics[4]) + "%\n"
                        + "Unique   : " + df.format(uniqueSequenceStatistics[1]) + ", " + df.format(uniqueSequenceStatistics[2]) + ", " + df.format(uniqueSequenceStatistics[0]) + ", "
                        + df.format(uniqueSequenceStatistics[3]) + "-" + df.format(uniqueSequenceStatistics[4]) + "%\n\n");

            output.append(seqOutputBuilder).append("\n\n\n");
        }


        int totalAccessoryGroups = total_hmgroups - total_core_groups - total_unique_groups;
        String sco_pc = percentageAsString(totalScoNodes, total_hmgroups, 2);
        String sco_phased_pc = percentageAsString(totalPhasedScoNodes, total_hmgroups, 2);
        String total_uni_pc = percentageAsString(total_unique_groups, total_hmgroups, 2);
        String total_access_pc = percentageAsString(totalAccessoryGroups, total_hmgroups, 2);
        String total_core_pc = percentageAsString(total_core_groups, total_hmgroups, 2);

        double[] coreGenomeStatistics;
        double[] accessoryGenomeStatistics;
        double[] uniqueGenomeStatistics;
        // three arrays have [median, average, standard deviation, shortest, longest, total, count]
        if (percentagesPerCategory.containsKey("genome_core")) {
            ArrayList<Double> corePercentagesGenome = percentagesPerCategory.get("genome_core");
            coreGenomeStatistics = median_average_stdev_from_AL_double(corePercentagesGenome);
        } else {
            coreGenomeStatistics = new double[7];
        }

        if (percentagesPerCategory.containsKey("genome_accessory")) {
            ArrayList<Double> accessoryPercentagesGenome = percentagesPerCategory.get("genome_accessory");
            accessoryGenomeStatistics = median_average_stdev_from_AL_double(accessoryPercentagesGenome);
        } else {
            accessoryGenomeStatistics = new double[7];
        }

        if (percentagesPerCategory.containsKey("genome_unique")) {
            ArrayList<Double> uniquePercentagesGenome = percentagesPerCategory.get("genome_unique");
            uniqueGenomeStatistics = median_average_stdev_from_AL_double(uniquePercentagesGenome);
        } else {
            uniqueGenomeStatistics = new double[7];
        }

        String grouping_info = "Minimum required protein ";
        long totalGroupNodes;
        try (Transaction tx = GRAPH_DB.beginTx();
             ResourceIterator<Node> nodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) { // start database transaction
            Node pangenome_node = nodes.next();
            totalGroupNodes = count_nodes(HOMOLOGY_GROUP_LABEL);
            String info = (String) pangenome_node.getProperty("grouping_v" + grouping_version);
            String[] info_array = info.split(",");
            grouping_info += info_array[0];
            tx.success();
        }
        String phasedScoStr = "", totalSubgenomesStr = "";
        if (PHASED_ANALYSIS) {
            int totalSubgenomes = subgenomesInAnalysis(selectedSubgenomes);
            totalSubgenomesStr = "Subgenomes: " + totalSubgenomes + "\n";
            phasedScoStr = "Single-copy ortholog groups (with phasing considered): " + totalPhasedScoNodes + " " + sco_phased_pc + "%\n";
        }

        String output_header = "Grouping version: " + grouping_version + "\n"
                + grouping_info + "%\n"
                + "Genomes: " + genomeList.size() + " out of " + (PROTEOME ? total_genomes : GENOME_DB.num_genomes) + "\n" // panproteomes don't have GENOME_DB.num_genomes set
                + "Sequences: " + selectedSequences.size() + " out of " + sequencesInPangenome + "\n"
                + totalSubgenomesStr
                + "mRNAs in analysis: " + mrna_pangenome_count + "\n"
                + "Homology groups: " + total_hmgroups + "/" + totalGroupNodes + "\n"
                + "Single-copy ortholog groups: " + totalScoNodes + " " + sco_pc + "%\n"
                + phasedScoStr;

        output_header += "\n"
                + "Core groups     : " + total_core_groups    + " " + total_core_pc + "%\n"
                + "Accessory groups: " + totalAccessoryGroups + " " + total_access_pc + "%\n"
                + "Unique groups   : " + total_unique_groups  + " " + total_uni_pc + "%\n"
                + "\n"
                + "Average % of genes per class for individual GENOMES, standard deviation, median, range\n"
                + "Core     : " + df.format(coreGenomeStatistics[1]) + ", " + df.format(coreGenomeStatistics[2]) + ", " + df.format(coreGenomeStatistics[0]) + ", "
                + df.format(coreGenomeStatistics[3]) + "-" + df.format(coreGenomeStatistics[4]) + "%\n"
                + "Accessory: " + df.format(accessoryGenomeStatistics[1]) + ", " + df.format(accessoryGenomeStatistics[2]) + ", " + df.format(accessoryGenomeStatistics[0]) + ", "
                + df.format(accessoryGenomeStatistics[3]) + "-" + df.format(accessoryGenomeStatistics[4]) + "%\n"
                + "Unique   : " + df.format(uniqueGenomeStatistics[1]) + ", " + df.format(uniqueGenomeStatistics[2]) + ", " + df.format(uniqueGenomeStatistics[0]) + ", "
                + df.format(uniqueGenomeStatistics[3]) + "-" + df.format(uniqueGenomeStatistics[4]) + "%\n\n";

        if (compare_sequences) {
            double[] coreSequenceStatistics; // [median, average, standard deviation, shortest, longest, total, count]
            double[] accessorySequenceStatistics;
            double[] uniqueSequenceStatistics;

            // three arrays have [median, average, standard deviation, shortest, longest, total, count]
            if (percentagesPerCategory.containsKey("sequence_core")) {
                ArrayList<Double> corePercentagesSequence = percentagesPerCategory.get("sequence_core");
                coreSequenceStatistics = median_average_stdev_from_AL_double(corePercentagesSequence);
            } else {
                coreSequenceStatistics = new double[7];
            }

            if (percentagesPerCategory.containsKey("sequence_accessory")) {
                ArrayList<Double> accessoryPercentagesGenome = percentagesPerCategory.get("sequence_accessory");
                accessorySequenceStatistics = median_average_stdev_from_AL_double(accessoryPercentagesGenome);
            } else {
                accessorySequenceStatistics = new double[7];
            }

            if (percentagesPerCategory.containsKey("sequence_unique")) {
                ArrayList<Double> uniquePercentagesGenome = percentagesPerCategory.get("sequence_unique");
                uniqueSequenceStatistics = median_average_stdev_from_AL_double(uniquePercentagesGenome);
            } else {
                uniqueSequenceStatistics = new double[7];
            }

            output_header += "Average % of genes per class for individual SEQUENCES, standard deviation, median, range\n"
                    + "Core     : " + df.format(coreSequenceStatistics[1]) + ", " + df.format(coreSequenceStatistics[2]) + ", " + df.format(coreSequenceStatistics[0]) + ", "
                    + df.format(coreSequenceStatistics[3]) + "-" + df.format(coreSequenceStatistics[4]) + "%\n"
                    + "Accessory: " + df.format(accessorySequenceStatistics[1]) + ", " + df.format(accessorySequenceStatistics[2]) + ", " + df.format(accessorySequenceStatistics[0]) + ", "
                    + df.format(accessorySequenceStatistics[3]) + "-" + df.format(accessorySequenceStatistics[4]) + "%\n"
                    + "Unique   : " + df.format(uniqueSequenceStatistics[1]) + ", " + df.format(uniqueSequenceStatistics[2]) + ", " + df.format(uniqueSequenceStatistics[0]) + ", "
                    + df.format(uniqueSequenceStatistics[3]) + "-" + df.format(uniqueSequenceStatistics[4]) + "%\n\n\n";
        }

        writeStringToFile(output_header + output, "gene_classification/gene_classification_overview.txt", true, false);
        writeStringToFile(csvGenome + csv_seq_builder.toString(), "gene_classification/gene_classification_overview.csv", true, false);
    }

    private int countIncludedSequenceOfGenome(String genome) {
        int sequencesInAnalysis = 0;
        try {
            int genomeNr = Integer.parseInt(genome);
            for (int sequenceNr = 1; sequenceNr <= GENOME_DB.num_sequences[genomeNr]; ++sequenceNr) {
                if (selectedSequences.contains(genome + "_" + sequenceNr)) {
                    sequencesInAnalysis++;
                }
            }
        } catch (NumberFormatException ignored) {
        }
        return sequencesInAnalysis;
    }

    /**
     *
     * @param total_gene_counts [ core, accessory, unique]
     * @param distinctGeneCounts [ core, accessory, unique]
     * @param output
     * @param csv_builder
     * @param percentagesPerCategory
     */
    private void calculate_c_a_u_percentages_and_append(int[] total_gene_counts, int[] distinctGeneCounts,
                                                              StringBuilder output, StringBuilder csv_builder,
                                                              HashMap<String, ArrayList<Double>> percentagesPerCategory,
                                                              String genomeOrSequence) {

        int totalMrnas = total_gene_counts[2] + total_gene_counts[1] + total_gene_counts[0];
        int totalGroups = distinctGeneCounts[2] + distinctGeneCounts[1] + distinctGeneCounts[0];
        csv_builder.append(totalGroups);
        if (totalGroups == 0) {
            csv_builder.append("\n");
            output.append("\nTotal mRNAs: 0\n\n");
            return;
        }
        double corePercentage = percentage(total_gene_counts[0], totalMrnas);
        percentagesPerCategory.computeIfAbsent(genomeOrSequence + "_core", k -> new ArrayList<>()).add(corePercentage);
        String corePercentageStr = String.format("%.2f", corePercentage);

        double accessoryPercentage = percentage(total_gene_counts[1], totalMrnas);
        percentagesPerCategory.computeIfAbsent(genomeOrSequence + "_accessory", k -> new ArrayList<>()).add(accessoryPercentage);
        String accessoryPercentageStr = String.format("%.2f", accessoryPercentage);

        double uniquePercentage = percentage(total_gene_counts[2], totalMrnas);
        percentagesPerCategory.computeIfAbsent(genomeOrSequence + "_unique", k -> new ArrayList<>()).add(uniquePercentage);
        String uniquePercentageStr = String.format("%.2f", uniquePercentage);

        String coreGroupPercentage = percentageAsString(distinctGeneCounts[0], totalGroups, 2 );
        String accessoryGroupPercentage = percentageAsString(distinctGeneCounts[1], totalGroups, 2);
        String uniqueGroupPercentage = percentageAsString(distinctGeneCounts[2], totalGroups, 2);

        output.append("\nTotal mRNAs: ").append(totalMrnas)
                .append("\nCore       : ").append(total_gene_counts[0]).append(" ").append(corePercentageStr).append("%\n")
                .append("Accessory  : ").append(total_gene_counts[1]).append(" ").append(accessoryPercentageStr).append("%\n")
                .append("Unique     : ").append(total_gene_counts[2]).append(" ").append(uniquePercentageStr).append("%\n")
                .append("\n")
                .append("Total groups: ").append(totalGroups)
                .append("\nCore        : ").append(distinctGeneCounts[0]).append(" ").append(coreGroupPercentage).append("%\n")
                .append("Accessory   : ").append(distinctGeneCounts[1]).append(" ").append(accessoryGroupPercentage).append("%\n")
                .append("Unique      : ").append(distinctGeneCounts[2]).append(" ").append(uniqueGroupPercentage).append("%\n\n");

        csv_builder.append(",").append(distinctGeneCounts[0]).append(",").append(distinctGeneCounts[1]).append(",")
                    .append(distinctGeneCounts[2]); // core, accessory, unique groups
        //this is a better place for the singletons, but it cannot be easily calculated for sequences... would look weird
        // currently after all CAU statistics
        //if (mrnasWithoutHomology > -1) {
        //    csv_builder.append(mrnasWithoutHomology).append(","); // singleton groups
        //}
        csv_builder.append(",").append(totalMrnas).append(",").append(total_gene_counts[0])
                    .append(",").append(corePercentageStr).append(",").append(total_gene_counts[1]).append(",").append(accessoryPercentageStr)
                    .append(",").append(total_gene_counts[2]).append(",").append(uniquePercentageStr);
    }

    /**
     * Creates all_homology_groups.csv, file with node identifiers of all homology groups for the selected genomes.
     * @param updatedHomologyNodes list with homology_group nodes
     */
    public void writeAllHomologyGroupNodes(ArrayList<Node> updatedHomologyNodes) {
        String groupsStr = updatedHomologyNodes.toString().replace("[","").replace("Node","")
                .replace("]","").replace(" ","");
        writeStringToFile("# All " +  updatedHomologyNodes.size() + " homology groups of " + genomeList.size() + " genomes\n"
                        + groupsStr,
                WORKING_DIRECTORY + "gene_classification/group_identifiers/all_homology_groups.csv", false, false);
    }

    /**
     * Creates phenotype_shared/specific/exclusive_groups.csv, a file with homology_group node identifiers of phenotype shared/specific/exclusive homology groups.
     * @param groupsPerClass holds homology_group nodes for 7 possible keys: "core", "accessory", "unique", a phenotypeValue with "#total" / "#shared" / "#exclusive" "#specific"
     * @param category is either "shared", "exclusive" or "specific"
     */
    private void writePhenotypeGroupIdentifiers(HashMap<String, ArrayList<Node>> groupsPerClass, String category) {
        if (PHENOTYPE == null) { // no --phenotype was included
            return;
        }

        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "gene_classification/group_identifiers/phenotype_" + category + "_groups.csv"))) {
            for (String phenotypeValue : phenotype_threshold_map.keySet()) {
                if (!groupsPerClass.containsKey(phenotypeValue + "#" + category)) {
                    continue;
                } else if (phenotypeValue.equals("?") || phenotypeValue.equals("Unknown")) {
                    continue;
                }
                ArrayList<Node> homologyNodes = groupsPerClass.get(phenotypeValue + "#" + category);
                int[] genomes = phenotype_map.get(phenotypeValue);
                int pheno_threshold = phenotype_threshold_map.get(phenotypeValue);
                out.write("Phenotype: " + phenotypeValue + ", " + genomes.length + " genomes, threshold of " + pheno_threshold + " genomes\n");
                for (int i = 0; i < homologyNodes.size(); i++) {
                    out.write(homologyNodes.get(i).getId() + "");
                    if (i != homologyNodes.size() - 1) {
                        out.write(",");
                    }
                }
                out.write("\n\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to create: " +
                    WORKING_DIRECTORY + "gene_classification/group_identifiers/phenotype_" + category + "_groups.csv");
        }
    }

    /**
     *
     * @return value [total all, shared all, total distanct, shared distinct]
     * replaced with [total semi, total distinct, shared semi, shared distinct, total informative distinct, shared informative distinct, EVERYTHING a shared to b, EVERYTHING a shared to b]
     */
    public HashMap<String, int[]> createSharedGenesBetweenSubgenomesMap() {
        HashMap<String, int[]> sharedGenesBetweenSubgenomes = new HashMap<>();
        if (!PHASED_ANALYSIS) {
            return sharedGenesBetweenSubgenomes;
        }

        for (String subgenomeA : selectedSubgenomes) {
            for (String subgenomeB : selectedSubgenomes) {
                String[] ordered = orderTwoGenomePhasingIdentifiers(subgenomeA, subgenomeB);
                int[] sharedTotalGenes = new int[8];
                sharedGenesBetweenSubgenomes.put(ordered[0] + "#" + ordered[1], sharedTotalGenes);
            }
        }
        return sharedGenesBetweenSubgenomes;
    }

    /**
     * Expected input: two identifiers consisting of genome number and subgenome identifier
     * @param id1
     * @param id2
     * @return
     */
    private String[] orderTwoGenomePhasingIdentifiers(String id1, String id2) {
        String[] id1First = new String[]{id1, id2};
        String[] id2First = new String[]{id2, id1};
        String[] array1 = id1.split("_");
        String[] array2 = id2.split("_");
        int genomeNr1 = Integer.parseInt(array1[0]);
        int genomeNr2 = Integer.parseInt(array2[0]);

        if (genomeNr1 < genomeNr2) {
            return id1First;
        } else if (genomeNr1 > genomeNr2) {
            return id2First;
        }

        // genome numbers are equal
        if (array1[1].compareTo(array2[1]) < 0) {
            return id1First;
        } else if (array1[1].compareTo(array2[1]) > 0) {
            return id2First;
        }

        // its the same letter
        return id1First;
    }

    /**
     * Allows some functions to give a warning or stop running when not all genomes were included in determining single copy orthologs
     */
    private void createSkippedInfoFile() {
        StringBuilder outputBuilder = new StringBuilder();
//        for (int genomeNr = 1; genomeNr <= GENOME_DB.num_genomes; genomeNr++) {
        for (int genomeNr = 1; genomeNr <= total_genomes; genomeNr++) { // TODO: GENOME_DB.num_genomes doesn't work for panproteomes!
            if (!genomeList.contains(genomeNr + "")) {
                outputBuilder.append(genomeNr).append(",");
            }
        }

        // if no genomes were skipped, write "None"
        if (outputBuilder.length() == 0) {
            outputBuilder.append("None,");
        }

        writeStringToFile(outputBuilder.toString().replaceFirst(",$", ""),
                "gene_classification/skipped_genomes.info", true, false);
        writeStringToFile(selectedSequences.toString().replace("[","").replace("]","").replace(" ",""),
                "gene_classification/selected_sequences.info", true, false);
        if (PHASED_ANALYSIS) {
            writeStringToFile(selectedSequences.toString().replace("[","").replace("]","").replace(" ",""),
                    WORKING_DIRECTORY + "gene_classification/selected_sequences_phased_analysis.info", false, false);
        }
    }

    private void createChromosomeUpsetRscript(ArrayList<Integer> chromosomeNumbers,
                                              HashMap<Integer, ArrayList<String>> sequencesPerChromosome,
                                              ArrayList<String> genomeCombinations,
                                              HashMap<String, Integer> ploidyPerGenome) {


        int plotCounter = 1;
        StringBuilder rscript = new StringBuilder();
        String R_LIB = check_r_libraries_environment();
        rscript.append("#! /usr/bin/env RScript\n\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n")
                .append("#install.packages(\"UpSetR\", \"").append(R_LIB).append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(UpSetR)\n\n");

        rscript.append("allowed_sets <- 50\n")
                .append("allowed_intersects <- NA # NA visualizes all intersections. Include a number to limit intersections in plot\n")
                .append("font_size <- 1.5\n")
                .append("plot_width <- 25\n")
                .append("plot_height <- 5\n\n");

        String inFile1 = WD_full_path + "gene_classification/upset/input/genomes.txt";
        rscript.append("df").append(plotCounter).append(" <- read.csv(file = '").append(inFile1).append("', sep = ',', header = TRUE, check.names=FALSE)\n")
                .append("plot").append(plotCounter).append(" <-upset(df").append(plotCounter).append(", nsets = allowed_sets, nintersects = allowed_intersects, text.scale = font_size)\n")
                .append("pdf(file = \"").append(WD_full_path).append("gene_classification/upset/output/genomes.pdf\", width = plot_width, height = plot_height)\n")
                .append("plot").append(plotCounter).append("\n")
                .append("dev.off()\n\n");
        plotCounter++;

        if (PHASED_ANALYSIS) {
            for (int chromosomeNr : chromosomeNumbers) {
                ArrayList<String> sequences = sequencesPerChromosome.get(chromosomeNr);
                if (sequences.size() == 1) {
                    continue;
                }
                rscript.append("cat ('Chromosome ").append(chromosomeNr).append("\\n')\n");
                String inFile = WD_full_path + "gene_classification/upset/input/chromosome_" + chromosomeNr + "/";
                String outFile = WD_full_path + "gene_classification/upset/output/chromosome_" + chromosomeNr + "/";
                rscript.append("df").append(plotCounter).append(" <- read.csv(file = '").append(inFile).append("all_genomes.txt', sep = ',', header = TRUE, check.names=FALSE)\n")
                    .append("plot").append(plotCounter).append(" <-upset(df").append(plotCounter).append(", nsets = allowed_sets, nintersects = allowed_intersects, text.scale = font_size)\n")
                    .append("pdf(file = \"").append(outFile).append("all_genomes.pdf\", width = plot_width, height = plot_height)\n")
                    .append("plot").append(plotCounter).append("\n")
                    .append("dev.off()\n\n");
                plotCounter++;

                for (String genomeCombi : genomeCombinations) {
                    if (!genomeCombi.contains(",")) { //when single genome, verify if phased
                        if (ploidyPerGenome.get(genomeCombi) < 2) {
                            continue;
                        }
                    }
                    genomeCombi = genomeCombi.replace(",", "_");
                    rscript.append("df").append(plotCounter).append(" <- read.csv(file = '").append(inFile).append(genomeCombi).append(".txt', sep = ',', header = TRUE, check.names=FALSE)\n")
                        .append("plot").append(plotCounter).append(" <-upset(df").append(plotCounter).append(", nsets = allowed_sets, nintersects = allowed_intersects, text.scale = font_size)\n")
                        .append("pdf(file = \"").append(outFile).append(genomeCombi).append(".pdf\", width = plot_width, height = plot_height)\n")
                        .append("plot").append(plotCounter).append("\n")
                        .append("dev.off()\n\n");
                    plotCounter++;
                }
            }
            rscript.append("cat(\"\\nOutput written to: ").append(WD_full_path).append("gene_classification/upset/output\\n\\n\")");
        } else {
            rscript.append("cat(\"\\nOutput written to: ").append(WD_full_path).append("gene_classification/upset/output/genomes.pdf\\n\\n\")");
        }

        writeStringToFile(rscript.toString(), "gene_classification/upset/upset_plot.R", true, false);
    }

    /**
     *
     * @return key is a chromosome number
     */
    public HashMap<Integer, ArrayList<String>> createSequencesPerChromosomeMapInteger() {
        HashMap<Integer, ArrayList<String>> sequencesPerChromosome = new HashMap<>();
        if (phasingInfoMap.isEmpty()) {
            return sequencesPerChromosome;
        }
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] phasingInfo = phasingInfoMap.get(sequenceId);
            int chromosomeNr = Integer.parseInt(phasingInfo[0]);
            sequencesPerChromosome.computeIfAbsent(chromosomeNr, k -> new ArrayList<>()).add(sequenceId);
        }
        return sequencesPerChromosome;
    }

    private void closePhasingCountWriters(BufferedWriter[] phasing_count_writers) {
        for (BufferedWriter phasingCountWriter : phasing_count_writers) {
            try {
                if (phasingCountWriter != null) {
                    phasingCountWriter.close();
                }
            } catch (IOException ioe) {
                Pantools.logger.warn("file cannot be closed.");
            }
        }
    }

    /**
     * Start to the haplotype_presence output files required for the PanTools function sequence_visualization
     * @param chromosomesPerGenome
     * @return
     */
    private BufferedWriter[] startPhasingCountWriters(HashMap<String, ArrayList<Integer>> chromosomesPerGenome) {
        BufferedWriter[] phasing_count_writers = new BufferedWriter[selectedSequences.size()];
        if (!PHASED_ANALYSIS || phasingInfoMap.isEmpty()) {
            return phasing_count_writers;
        }
        int fileCounter = 0;
        try {
            for (String genome : genomeList) { // i is the genome number
                if (!chromosomesPerGenome.containsKey(genome)) {
                    continue;
                }
                ArrayList<Integer> chromosomeNrs = chromosomesPerGenome.get(genome);
                for (int chromsomeNr : chromosomeNrs) { // j is a chromosome number, chromosome 0 also included
                    phasing_count_writers[fileCounter] = new BufferedWriter(new FileWriter(
                            WORKING_DIRECTORY + "gene_classification/haplotype_presence/" + genome + "_" + chromsomeNr + ".txt" ));
                    fileCounter++;
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to create files in : " + WORKING_DIRECTORY + "gene_classification/haplotype_presence/");
        }
        return phasing_count_writers;
    }

    private HashMap<String, Integer> createPhasingCountMap(HashMap<String, ArrayList<Integer>> chromosomesPerGenome) {
        HashMap<String, Integer> phasingCountPosition = new HashMap<>();
        if (!PHASED_ANALYSIS || phasingInfoMap.isEmpty()) {
            return phasingCountPosition;
        }
        int fileCounter = 0;
        for (String genome : genomeList) {
            if (!chromosomesPerGenome.containsKey(genome)) {
                continue;
            }
            ArrayList<Integer> chromosomeNumbers = chromosomesPerGenome.get(genome);
            for (int chromosomeNr : chromosomeNumbers) {
                phasingCountPosition.put(genome + "_" + chromosomeNr, fileCounter);
                fileCounter++;
            }
        }
        return phasingCountPosition;
    }

    public HashMap<String, ArrayList<Integer>> createChromosomesPerGenome() {
        HashMap<String, ArrayList<Integer>> chromosomesPerGenome = new HashMap<>();
        if (!PHASED_ANALYSIS) {
            return chromosomesPerGenome;
        }
        HashMap<String, TreeSet<Integer>> chromosomesPerGenomeSet = new HashMap<>();
        for (String genome : genomeList) { // i is the genome number
            for (String sequenceId : selectedSequences) {
                if (!sequenceId.startsWith(genome + "_")) {
                    continue;
                }
                String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
                int chromosomeNr = 0;
                if (phasingInfo != null) {
                    chromosomeNr = Integer.parseInt(phasingInfo[0]);
                }
                chromosomesPerGenomeSet.computeIfAbsent(genome, k -> new TreeSet<>()).add(chromosomeNr);
            }
        }
        for (String genome : chromosomesPerGenomeSet.keySet()) {
            TreeSet<Integer> chromosomeNrs = chromosomesPerGenomeSet.get(genome);
            chromosomesPerGenome.put(genome, new ArrayList<>(chromosomeNrs));
        }
        return chromosomesPerGenome;
    }

    /**
     * Phased genomes must have 1 copy per haplotype
     * Unphased genomes must have allowed 1 copy per genome
     * @param geneCountPerSequence
     * @param ploidyPerGenome phases per genome. Example [1, 2, 4]
     * @param haplotypeLettersPerGenome
     */
    private boolean determineSingleCopyGroupWhenPhased(HashMap<String, Integer> geneCountPerSequence,
                                                       HashMap<String, Integer> ploidyPerGenome,
                                                       HashMap<String, ArrayList<String>> haplotypeLettersPerGenome) {

        if (!PHASED_ANALYSIS || phasingInfoMap.isEmpty()) {
            return false;
        }

        // loop to check if more than 1 copy is present in a phase. stop if this is the case.
        HashMap<String, ArrayList<String>> phases_count_per_genome = new HashMap<>();
        for (String sequenceId : geneCountPerSequence.keySet()) {
            int count = geneCountPerSequence.get(sequenceId);
            if (count != 1) { // already to many copies on sequence, let alone genome
                return false;
            }
            String[] seqIdArray = sequenceId.split("_");
            String phase = "unphased";
            if (phasingInfoMap.containsKey(sequenceId)) {
                String[] phasing_info = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
                phase = phasing_info[1];
            }
            String genome = seqIdArray[0];
            ArrayList<String> phases;
            if (phases_count_per_genome.containsKey(genome)) {
                phases = phases_count_per_genome.get(genome);
            } else {
                phases = new ArrayList<>();
            }
            if (phases.contains(phase) && !phase.equals("unphased")) { // already a gene found in this phase. "unphased" can be included unlimited
                return false;
            }
            phases.add(phase);
            phases_count_per_genome.put(genome, phases);
        }

        // now check if all phases are present
        for (String genome : genomeList) {
            if (!phases_count_per_genome.containsKey(genome)) { // gene not present in genome
                return false;
            }

            ArrayList<String> foundPhases = phases_count_per_genome.get(genome);
            if (ploidyPerGenome.get(genome) == 1) { // genome is unphased or haploid. allow 1 copy, also 1 unphased
                if (foundPhases.size() > 1) {
                    return false;
                }
            } else { // ploidy of > 2
                ArrayList<String> haplotypeLetters = haplotypeLettersPerGenome.get(genome);
                for (String haplotypeLetter : haplotypeLetters) {
                    if (haplotypeLetter.equals("unphased")) {
                        continue;
                    }
                    if (!foundPhases.contains(haplotypeLetter)) { // all haplotype of the genome must be found
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     *
     * @return
     */
    private String subgenomePresenceReadableText(ArrayList<Integer> subgenomeCounts) {
        if (subgenomeCounts == null) {
            return " Genome is not phased or haploid\n";
        }
        StringBuilder output = new StringBuilder();
        double[] statistics = median_average_stdev_from_AL_int(subgenomeCounts); // [median, average, standard deviation, shortest, longest, total, count (groups)]
        DecimalFormat df = new DecimalFormat("0.00");
        for (int i = 1; i <= 20; i++) {
            int occurrences = Collections.frequency(subgenomeCounts, i);
            if (occurrences > 0) {
                double percentage = percentage(occurrences, (int) statistics[6]);
                output.append(i).append(" subgenome ").append(occurrences).append(", groups (").append(df.format(percentage)).append("%)\n");
            }
        }
        return output.toString();
    }

    private void createSubgenomePresenceCSV(HashMap<String, ArrayList<Integer>> subgenomeCountsPerGenome,
                                            int highestPloidyLevel,
                                            HashMap<String, int[]> core_access_uni_map,
                                            HashMap<String, Integer> mrnasWithSubgenomePresence) {

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder csvOutput = new StringBuilder();
        StringBuilder groupCountsHeader = new StringBuilder();
        StringBuilder percentagesHeader = new StringBuilder();
        StringBuilder mrnaCountHeader = new StringBuilder();
        StringBuilder mrnaPercentageHeader = new StringBuilder();
        // if 1 gene in unphased but 5 others are located on 4 subgenomes. 6 mrna's ar counted on 4 subgenomes
        for (int i = 1; i <= highestPloidyLevel; i++) {
            groupCountsHeader.append("Groups with genes in ").append(i).append(" subgenomes,");
            percentagesHeader.append("% of groups with genes in ").append(i).append(" subgenomes,");
            mrnaCountHeader.append("mRNAs in ").append(i).append(" subgenomes,");
            mrnaPercentageHeader.append("% of mRNAs in ").append(i).append(" subgenomes,");
        }
        csvOutput.append("Genome,").append(percentagesHeader).append(groupCountsHeader)
                .append("Groups only on unphased sequences,total groups,median (number of subgenomes),average (number of subgenomes),standard deviation,")
                .append(mrnaPercentageHeader).append(mrnaCountHeader).append("mRNAs only on unphased sequences").append("\n");

        for (String genome : subgenomeCountsPerGenome.keySet()) {
            int[] total_gene_counts = core_access_uni_map.get(genome + "_total"); // core, accessory, unique
            int[] distinct_gene_counts = core_access_uni_map.get(genome + "_distinct"); // core, accessory, unique
            int total_mrnas = total_gene_counts[2] + total_gene_counts[1] + total_gene_counts[0];
            int groups = distinct_gene_counts[2] + distinct_gene_counts[1] + distinct_gene_counts[0];

            ArrayList<Integer> subgenomeCounts = subgenomeCountsPerGenome.get(genome);
            double[] statistics = median_average_stdev_from_AL_int(subgenomeCounts); // [median, average, standard deviation, shortest, longest, total, count (groups)]

            StringBuilder groupCounts = new StringBuilder();
            StringBuilder mrnaCounts = new StringBuilder();
            StringBuilder mrnaPercentages = new StringBuilder();
            StringBuilder percentages = new StringBuilder();

            for (int i = 1; i <= highestPloidyLevel; i++) {
                int occurrences = Collections.frequency(subgenomeCounts, i);
                if (occurrences > 0) {
                    int mrnaCount = mrnasWithSubgenomePresence.get(genome + "_" + i);
                    groupCounts.append(occurrences);
                    percentages.append(percentageAsString(occurrences, groups, 2));
                    mrnaCounts.append(mrnaCount);
                    mrnaPercentages.append(percentageAsString(mrnaCount, total_mrnas, 2));
                }
                groupCounts.append(",");
                percentages.append(",");
                mrnaCounts.append(",");
                mrnaPercentages.append(",");
            }

            int mrnaCountOnUnphased = 0;
            if (mrnasWithSubgenomePresence.containsKey(genome + "_0")) {
                mrnaCountOnUnphased = mrnasWithSubgenomePresence.get(genome + "_0");
            }
            csvOutput.append(genome).append(",")
                    .append(percentages) // percentage of groups in 1 to XX subgenomes
                    .append(groupCounts) // number of groups per 1 to XX subgenomes
                    .append((groups-statistics[6])).append(",") // total_groups - groups with at least one gene in subgenome = groups only on unphased sequences
                    .append(groups).append(",") // total groups
                    .append(df.format(statistics[0])).append(",") // median
                    .append(df.format(statistics[1])).append(",") // average
                    .append(df.format(statistics[2])).append(",") // stdev
                    .append(mrnaPercentages) // percentage of mRNAs in 1 to XX subgenomes
                    .append(mrnaCounts) // mRNAs per 1 to XX subgenomes
                    .append(mrnaCountOnUnphased).append("\n");
        }
        writeStringToFile(csvOutput.toString(), WORKING_DIRECTORY + "gene_classification/subgenome_presence.csv", false, false);
    }

    private String subgenomePresenceCSV(ArrayList<Integer> subgenomeCounts, int totalGroups, int highestPloidyLevel) {
        StringBuilder groups = new StringBuilder();
        StringBuilder percentages = new StringBuilder();
        if (subgenomeCounts == null) {
            groups.append(totalGroups);
            percentages.append("100");
            for (int i = 1; i <= highestPloidyLevel; i++) {
                groups.append(",");
                percentages.append(",");
            }
        } else {
            double[] statistics = median_average_stdev_from_AL_int(subgenomeCounts); // [median, average, standard deviation, shortest, longest, total, count (groups)]
            for (int i = 1; i <= highestPloidyLevel; i++) {
                int occurrences = Collections.frequency(subgenomeCounts, i);
                if (occurrences > 0) {
                    groups.append(occurrences);
                    percentages.append(percentageAsString(occurrences, (int) statistics[6], 2));
                }
                groups.append(",");
                percentages.append(",");
            }
        }

        return groups + percentages.toString();
    }

    /**
     * Increase various maps by going over the 'copy_number' array of an
     * homology group node
     *
     * @param copy_number_array
     * @param phenotype_min_max
     * @return
     */
    private HashMap<String, Integer> countPhenotypeGenomes(int[] copy_number_array, HashMap<String, int[]> phenotype_min_max) {
        HashMap<String, Integer> phenotypeGenomeCounts = new HashMap<>();
        if (PHENOTYPE == null) {
            return phenotypeGenomeCounts;
        }

        for (String genome : genomeList) {
            int genomeNr;
            try {
                genomeNr = Integer.parseInt(genome);
            } catch (NumberFormatException ignored) {
                Pantools.logger.debug("Genome {} is not a number and thus has no phenotype", genome);
                continue;
            }
            String phenotypeValue = geno_pheno_map.get(genomeNr);
            if (phenotypeValue.equals("Unknown") || phenotypeValue.equals("?")) {
                continue;
            }

            int position = genomeList.indexOf(genome);
            if (copy_number_array[position] == 0) {
                continue;
            }
            phenotypeGenomeCounts.merge(phenotypeValue, 1, Integer::sum);
            int[] min_max;
            if (phenotype_min_max.containsKey(phenotypeValue)) {
                min_max = phenotype_min_max.get(phenotypeValue);
            } else {
                min_max = new int[]{9999,0}; // [lowest, highest] is automatically updated below
            }
            if (copy_number_array[position] < min_max[0]) { // new lowest value
                min_max[0] = copy_number_array[position];
            }
            if (copy_number_array[position] > min_max[1]) { // new highest value
                min_max[1] = copy_number_array[position];
            }
            phenotype_min_max.put(phenotypeValue, min_max);
        }
        return phenotypeGenomeCounts;
    }

    /**
     * Creates unique_groups.csv, the node identifiers of unique homology groups
     * ordered by genome.
     *
     * @param uniqueGroupsPerGenome unique groups per genome
     * @return the total number of unique groups
     */
    private int writeUniqueGroups(HashMap<String, HashSet<Long>> uniqueGroupsPerGenome) {
        StringBuilder output = new StringBuilder();
        HashSet<Long> allUniqueGroupIds = new HashSet<>();
        for (String genome : genomeList) {
            if (uniqueGroupsPerGenome.containsKey(genome)) {
                HashSet<Long> uniqueGroupIds = uniqueGroupsPerGenome.get(genome);
                String groups_str = uniqueGroupIds.toString().replace("[", "").replace("]", "").replace(", ", ",");
                output.append("Genome ").append(genome).append("\n").append(groups_str).append("\n\n");
                allUniqueGroupIds.addAll(uniqueGroupIds);
            } else {
                output.append("Genome ").append(genome).append("\n\n");
            }
        }
        writeStringToFile(output.toString(), WORKING_DIRECTORY + "gene_classification/group_identifiers/unique_groups.csv", false, false);
        return allUniqueGroupIds.size();
    }

    /**
     * Creates genome_combinations.csv The homology groups node identifiers
     * ordered by the combination of genomes by which they are shared.
     *
     * @param groupsPerGenomeCombination
     * @param seq_combi_within_genome_combi
     * @param groupsPerSequenceCombination
     */
    private void writeGroupsPerGenomeSequenceCombi(HashMap<String, StringBuilder> groupsPerGenomeCombination,
                                                   HashMap<String, HashSet<String>> seq_combi_within_genome_combi,
                                                   HashMap<String, StringBuilder> groupsPerSequenceCombination) throws IOException {

        String header = "#This file cannot be read by other PanTools functions. "
                + "Split the file on the comma and use the homology group node identifiers in the right column\n";
        BufferedWriter sequenceCombiOut = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/group_identifiers/sequence_combinations.csv"));
        sequenceCombiOut.write(header + "#genome combination,sequence combination,number of groups,homology group node identifiers\n\n");
        String genomeCombiOutput = WORKING_DIRECTORY + "gene_classification/group_identifiers/genome_combinations.csv";
        BufferedWriter genomeCombiOut = new BufferedWriter(new FileWriter(genomeCombiOutput));
        genomeCombiOut.write(header + "#genome combination,number of groups,homology group node identifiers\n\n");

        for (int i = genomeList.size(); i > 0; i--) {
            String percentage = percentageAsString(i, core_threshold, 2);
            if (i == 1) {
                genomeCombiOut.write("#" + i + " genome " + percentage + "%\n");
                sequenceCombiOut.write("#" + i + " genome " + percentage + "%\n");
            } else {
                genomeCombiOut.write("#" + i + " genomes " + percentage + "%\n");
                sequenceCombiOut.write("#" + i + " genomes " + percentage + "%\n");
            }

            ArrayList<String> remove_keys = new ArrayList<>(); // to speed up the extraction every iteration
            for (String genomeCombi : groupsPerGenomeCombination.keySet()) {
                if (genomeCombi.split(",").length != i) { // not the correct number of genomes
                    continue;
                }
                remove_keys.add(genomeCombi);
                String homologyNodeIds = groupsPerGenomeCombination.get(genomeCombi).toString();
                String[] homologyNodeIdsArray = homologyNodeIds.split(" ");
                genomeCombiOut.write(genomeCombi.replace("[", "").replace("]", "").replace(",", "")
                        + "," + homologyNodeIdsArray.length + " groups,"
                        + homologyNodeIds.replaceFirst(".$", "") + "\n"); // removes last letter
            }
            for (String key : remove_keys) {
                groupsPerGenomeCombination.remove(key);
            }
            if (compare_sequences) {
                addSequenceCombinations(groupsPerSequenceCombination, sequenceCombiOut, i, remove_keys,
                        seq_combi_within_genome_combi);
            }
        }

        if (!compare_sequences) {
            delete_file_full_path(WORKING_DIRECTORY + "gene_classification/group_identifiers/sequence_combinations.csv");
        }
    }

    private void addSequenceCombinations(HashMap<String, StringBuilder> groupsPerSequenceCombination,
                                         BufferedWriter sequence_combi_builder,
                                         int number_of_genomes, ArrayList<String> currentGenomeCombinations,
                                         HashMap<String, HashSet<String>> seq_combi_within_genome_combi) throws IOException {

        String genomes = "genomes";
        if (number_of_genomes == 1) {
            genomes = "genome";
        }

        HashMap<String, String> genomeCombiFromSeqCombi = new HashMap<>();
        TreeSet<Integer> seqCombiSizes = new TreeSet<>();
        HashMap<Integer, ArrayList<String>> seqCombiPerSize = new HashMap<>();
        for (String genomeCombi : currentGenomeCombinations) {
            HashSet<String> seqCombinations = seq_combi_within_genome_combi.get(genomeCombi);
            for (String seqCombi : seqCombinations) {
                String[] seqCombiArray = seqCombi.split(",");
                seqCombiPerSize.computeIfAbsent(seqCombiArray.length, k -> new ArrayList<>()).add(seqCombi);
                seqCombiSizes.add(seqCombiArray.length);
                genomeCombiFromSeqCombi.put(seqCombi, genomeCombi);
            }
        }

        TreeSet<Integer> reverseSizes = (TreeSet<Integer>) seqCombiSizes.descendingSet();
        for (int size : reverseSizes) {
            ArrayList<String> seqCombis = seqCombiPerSize.get(size);
            sequence_combi_builder.write("#" + number_of_genomes + " " + genomes + ". " + size + " sequences\n");
            for (String sequenceCombi : seqCombis) {
                String homologyGroupNodeIds = groupsPerSequenceCombination.get(sequenceCombi).toString();
                String[] homologyNodeIdsArray = homologyGroupNodeIds.split(" ");
                sequence_combi_builder.write(genomeCombiFromSeqCombi.get(sequenceCombi).replace(",", "") + "," +
                        sequenceCombi.replace(",", "") +
                        "," + homologyNodeIdsArray.length + " groups," + homologyGroupNodeIds.replaceFirst(".$","") + "\n");
            }
        }
    }

    /**
     *
     * @param homologyNode a 'homology_group' node
     * @return zero-based array
     */
    public int[][] getGeneCopiesPerSequence(Node homologyNode) {
        int[][] geneCopiesPerSequence = new int[genomeList.size()][0];
        if (!compare_sequences) {
            return geneCopiesPerSequence;
        }

        ArrayList<String> missingGenomes = new ArrayList<>(); // genomes without the "copy_number_genome_" + genome number property
        for (String genome : genomeList) {
            int genomeNr;
            try {
                genomeNr = Integer.parseInt(genome);
            } catch (NumberFormatException ignored) {
                Pantools.logger.debug("Not obtaining gene copies per sequence because genome {} is not a number.", genome);
                continue;
            }
            if (homologyNode.hasProperty("copy_number_genome_" + genome )) {
                geneCopiesPerSequence[genomeNr-1] = (int[]) homologyNode.getProperty("copy_number_genome_" + genome );
            } else {
                geneCopiesPerSequence[genomeNr-1] = new int [GENOME_DB.num_sequences[genomeNr]];
                missingGenomes.add(genome);
            }
        }
        if (missingGenomes.isEmpty()) { // gene copies were retrieved for all genomes in analysis
            return geneCopiesPerSequence;
        }

        // The "copy_number_genome_" properties are not yet available for every genome.
        Iterable<Relationship> relations = homologyNode.getRelationships(RelTypes.has_homolog);
        for (Relationship relation : relations) {
            Node mrnaNode = relation.getEndNode();
            int[] address = (int[]) mrnaNode.getProperty("address");
            if (!genomeList.contains(address[0] + "") || !missingGenomes.contains(address[0] + "")) {
                continue;
            }
            geneCopiesPerSequence[address[0] - 1][address[1] - 1]++;
        }

        for (String genome : missingGenomes) {
            int genomeNr;
            try {
                genomeNr = Integer.parseInt(genome);
            } catch (NumberFormatException ignored) {
                Pantools.logger.debug("Not writing gene copies per sequence to homology node because genome {} is not a number.", genome);
                continue;
            }
            //homologyNode.removeProperty("copy_number_genome_" + genomeNr); // removing was used for development, do not remove
            homologyNode.setProperty("copy_number_genome_" + genome, geneCopiesPerSequence[genomeNr-1]);
            setPropertyCounter.getAndIncrement(); // new transcaction started after writing 500 arrays
        }
        return geneCopiesPerSequence;
    }

    /**
     * Creates phenotype_disrupted.csv
     *
     * @param disrupted_builder
     */
    private void create_gclass_pheno_disrupted(StringBuilder disrupted_builder) {
        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder header = new StringBuilder("#The reason why a phenotype SHARED homology group is not SPECIFIC.\n"
                + "#The first two columns are the homology group identifier the phenotype. "
                + "The remaining columns contain the number of genes found for other phenotypes.\n"
                + "homology group,current phenotype,");
        for (String phenotype : phenotype_threshold_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            int[] genomes = phenotype_map.get(phenotype);
            header.append(phenotype).append(" ").append(genomes.length).append(" genomes,");
        }
        writeStringToFile(header + "\n" + disrupted_builder, WORKING_DIRECTORY + "gene_classification/phenotype_disrupted.csv", false, false);
    }

    private ArrayList<String> createGenomeCombinations() {
        ArrayList<String> genomeCombinations = new ArrayList<>();
        String[] genomes = new String[genomeList.size()];
        int counter = 0;
        for (String genome : genomeList) {
            genomes[counter] = genome;
            counter++;
            genomeCombinations.add(genome +"");
        }

        for (int i = 2; i < genomeList.size(); ++i) {
            // make all possible combinations of i genomes
            String[] data = new String[i]; //
            combinationUtil(genomes, data, 0, (genomeList.size()-1), 0, i, genomeCombinations);
        }
        return genomeCombinations;
    }

    private void combinationUtil(String[] arr, String[] data, int start, int end, int index, int r, ArrayList<String> combis) {
        // Current combination is ready to be printed, print it
        if (index == r) {
            StringBuilder str = new StringBuilder();
            for (int j=0; j<r; j++) {
                str.append(data[j]).append(",");
            }
            combis.add(str.toString().replaceFirst(".$","")); // remove last character);
            return;
        }

        // replace index with all possible elements. The condition end-i+1 >= r-index" makes sure that including one element
        // at index will make a combination with remaining elements at remaining positions
        for (int i=start; i<=end && end-i+1 >= r-index; i++) {
            data[index] = arr[i];
            combinationUtil(arr, data, i+1, end, index+1, r, combis);
        }
    }

    /**
     *
     * @param chromosomeNumbers
     * @param genomeCombinations
     */
    private void createGenomeCombinationsUpsetInput(ArrayList<Integer> chromosomeNumbers, ArrayList<String> genomeCombinations) {
        if (!PHASED_ANALYSIS) { // only create rscript when there are chromosomes/phased sequences in pangenome
            return;
        }
        String line;
        for (int chromosomeNr : chromosomeNumbers) {
            File file = new File(WORKING_DIRECTORY + "gene_classification/upset/input/chromosome_" + chromosomeNr + "/all_genomes.txt");
            if (!checkIfFileExists(file)) {
                continue; // file does not exists because chromosome has 1 sequence
            }

            // example: 1,2,5
            for (String selectedGenomesStr : genomeCombinations) {
                String[] genomeList = selectedGenomesStr.split(","); // [1,2,5]
                boolean firstLine = true;
                ArrayList<Integer> selectedRows = new ArrayList<>();
                StringBuilder newInputBuilder = new StringBuilder();
                try (BufferedReader in = new BufferedReader(new FileReader(file))) {
                    while ((line = in.readLine()) != null) { // go over the lines of the input fileStringBuilder newHeader = new StringBuilder("busco");
                        String[] lineArray = line.split(","); // example header 'homology_group,2_17,2_18,3_17,3_18,1_18,1_17,4_9'
                        StringBuilder lineBuilder = new StringBuilder();
                        if (firstLine) {
                            lineBuilder.append("homology_group,");

                            // select columns from which the sequence belongs to the selected genomes
                            for (int j = 1; j < lineArray.length; ++j) {
                                for (String genomeNrStr : genomeList) {
                                    if (lineArray[j].startsWith(genomeNrStr + "_")) {
                                        selectedRows.add(j);
                                        lineBuilder.append(lineArray[j]).append(",");
                                        break;
                                    }
                                }
                            }
                            firstLine = false;
                            newInputBuilder.append(lineBuilder.toString().replaceFirst(".$", "")).append("\n");
                        } else {
                            lineBuilder.append(lineArray[0]).append(",");
                            for (int j = 0; j < selectedRows.size(); ++j) {
                                int index = selectedRows.get(j);
                                lineBuilder.append(lineArray[index]);
                                if (j != selectedRows.size() - 1) {
                                    lineBuilder.append(",");
                                }
                            }
                            newInputBuilder.append(lineBuilder).append("\n");
                        }
                    }
                } catch (IOException ioe) {
                    throw new RuntimeException("Unable to read: " + file);
                }
                writeStringToFile(newInputBuilder.toString(),
                        "gene_classification/upset/input/chromosome_" + chromosomeNr + "/" + selectedGenomesStr.replace(",", "_") + ".txt",
                        true, false);
            }
        }
    }

    private void updateUpsetGClassInput(BufferedWriter[] upsetWriters, HashMap<String, Integer> geneCountPerSequence,
                                       HashMap<Integer, ArrayList<String>> sequencesPerChromosome,
                                       ArrayList<Integer> chromosomeNumbers, long hmNodeId) {
        if (!PHASED_ANALYSIS) {
            return;
        }
        HashMap<Integer, int[]> countPerChromMap = new HashMap<>();
        for (String seqId : geneCountPerSequence.keySet()) {
            String[] phasingInfo = phasingInfoMap.get(seqId);
            if (phasingInfo == null) {
                continue;
            }
            int chromosomeNr = Integer.parseInt(phasingInfo[0]);
            ArrayList<String> sequences = sequencesPerChromosome.get(chromosomeNr);
            int index = sequences.indexOf(seqId);
            //System.out.println(seqId + " " + geneCountPerSequence.get(seqId) + " " + chromosomeNr + " " + index );
            int[] counts;
            if (countPerChromMap.containsKey(chromosomeNr)) {
                counts = countPerChromMap.get(chromosomeNr);
            } else {
                counts = new int[sequences.size()];
            }
            counts[index] = 1;
            countPerChromMap.put(chromosomeNr, counts);
        }
        for (int chromosomeNr : countPerChromMap.keySet()) {
            int[] counts = countPerChromMap.get(chromosomeNr);
            int index = chromosomeNumbers.indexOf(chromosomeNr);
            try {
                upsetWriters[index].write(hmNodeId + "," + Arrays.toString(counts).replace("[","").replace("]","").replace(" ","") + "\n");
            } catch (IOException ioe) {
                throw new RuntimeException("Something went wrong writing to " + WORKING_DIRECTORY + "gene_classification/upset");
            } catch (NullPointerException npe) {
                // do nothing. happens when a chromosome only has 1 sequence
            }
        }
    }

    private BufferedWriter startUpsetBetweenGenomes() throws IOException {
        BufferedWriter genomeUpsetInput = new BufferedWriter(new FileWriter( WORKING_DIRECTORY + "gene_classification/upset/input/genomes.txt"));
        StringBuilder upsetLine = new StringBuilder("homology_group,");
        for (String genome : genomeList) {
            upsetLine.append(genome).append(",");
        }
        genomeUpsetInput.write( upsetLine.toString().replaceFirst(".$","") + "\n");
        return genomeUpsetInput;
    }

    private HashMap<String, BufferedWriter> startGroupsPerGenomeWriters() throws IOException {
        HashMap<String, BufferedWriter> groupsPerGenome = new HashMap<>();
        for (String genome : genomeList) {
            if (genomeList.contains(genome)) {
                Path file = Paths.get(WORKING_DIRECTORY).resolve("gene_classification").resolve("group_identifiers").resolve("genome").resolve(genome + ".csv");
                groupsPerGenome.put(genome, Files.newBufferedWriter(file));
            }
        }
        return groupsPerGenome;
    }

    private void closeGroupsPerGenomeWriters(HashMap<String, BufferedWriter> groupsPerGenome) throws IOException {
        for (String genome : genomeList) {
            if (genomeList.contains(genome)) {
                groupsPerGenome.get(genome).close();
            }
        }
    }

    private BufferedWriter[] startUpsetWriters(ArrayList<Integer> chromosomeNumbers,
                                              HashMap<Integer, ArrayList<String>> sequencesPerChromosome) {

        // a chromosome number is also the index in the array
        if (!PHASED_ANALYSIS) {
            return null;
        }
        ArrayList<Integer> chromosomeNumbersList = new ArrayList<>(chromosomeNumbers);
        BufferedWriter[] upsetWriters = new BufferedWriter[chromosomeNumbers.size() + 1]; // +1 because of chromosome zero
        String file;
        try { // start the file writers for phasing counts
            for (int chromosomeNr : chromosomeNumbers) {
                createDirectory("gene_classification/upset/input/chromosome_" + chromosomeNr, true);
                createDirectory("gene_classification/upset/output/chromosome_" + chromosomeNr, true);
                ArrayList<String> sequences = sequencesPerChromosome.get(chromosomeNr);
                if (sequences.size() == 1) {
                    continue;
                }
                int index = chromosomeNumbersList.indexOf(chromosomeNr);
                file = WORKING_DIRECTORY + "gene_classification/upset/input/chromosome_" + chromosomeNr + "/all_genomes.txt";
                upsetWriters[index] = new BufferedWriter(new FileWriter(file));
                upsetWriters[index].write("homology_group,");

                // create and add the header
                for (int i = 0; i < sequences.size(); i++) {
                    String[] phasingInfo = phasingInfoMap.get(sequences.get(i)); // [1, B, 1B, 1_B]
                    String[] sequenceIdArray = sequences.get(i).split("_");
                    int genomeNr = Integer.parseInt( sequenceIdArray[0]);
                    String phenotype = get_phenotype_for_genome(genomeNr, true);
                    phenotype = phenotype.replace("_", " ");
                    if (phasingInfo == null) {
                        upsetWriters[index].write(sequences.get(i) + " Unphased" + phenotype);
                    } else if (phasingInfo[1].equals("unphased")) {
                        upsetWriters[index].write(sequences.get(i) + " " + phasingInfo[3] + phenotype);
                    } else {
                        upsetWriters[index].write(sequences.get(i) + " " + phasingInfo[2] + phenotype);
                    }

                    if (i != sequences.size()-1) { // don't addd comma on last sequence
                        upsetWriters[index].write(",");
                    }
                }
                upsetWriters[index].write("\n");
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Something went wrong trying to write to: " +
                    WORKING_DIRECTORY + "gene_classification/upset/");
        }
        return upsetWriters;
    }

    private void closeUpsetWriters(BufferedWriter[] upsetWriters) {
        for (BufferedWriter upsetWriter : upsetWriters) {
            try {
                upsetWriter.close();
            } catch (IOException ioe) {
                throw new RuntimeException("Something went wrong writing to " + WORKING_DIRECTORY + "gene_classification/upset");
            } catch (NullPointerException npe) {
                // do nothing. happens when a chromosome only has 1 sequence
            }
        }
    }

    /**
     *
     */
    private void create_gclass_syntelog_output() {
        int[][] genome_shared_syntelogs = new int[genomeList.size()][genomeList.size()];
        int[][] genome_total_syntelogs = new int[genomeList.size()][genomeList.size()];
        int[][] sequence_shared_syntelogs = null;
        int[][] sequence_total_syntelogs = null;
        if (compare_sequences) {
            sequence_shared_syntelogs = new int[selectedSequences.size()][selectedSequences.size()];
            sequence_total_syntelogs = new int[selectedSequences.size()][selectedSequences.size()];
        }
        genomeSyntelogDistance(genome_shared_syntelogs, genome_total_syntelogs);
        sequenceSyntelogDistance(sequence_shared_syntelogs, sequence_total_syntelogs);
    }


    private void genomeSyntelogDistance(int[][] genome_shared_syntelogs, int[][] genome_total_syntelogs) {
        StringBuilder genome_syn_dis = new StringBuilder();
        StringBuilder header = new StringBuilder("Genomes,");
        for (String genome : genomeList) {
            if (genomeList.indexOf(genome) != genomeList.size()) {
                header.append(genome).append(",");
            } else {
                header.append(genome).append("\n");
            }
        }

        for (int i = 0; i < genome_total_syntelogs.length; i++) {
            genome_syn_dis.append((i+1)).append(",");
            for (int j = 0; j < genome_total_syntelogs.length; j++) {
                double shared;
                if (j < i) {
                    shared = divide(genome_shared_syntelogs[j][i], genome_total_syntelogs[j][i]);
                } else {
                    shared = divide(genome_shared_syntelogs[i][j], genome_total_syntelogs[i][j]);
                }
//                if (j != GENOME_DB.num_genomes-1) { // Not exactly sure what this does, so not replacing by genomeList.size()
                if (j != total_genomes-1) { // Not exactly sure what this does, so not replacing by genomeList.size() // TODO: GENOME_DB.num_genomes assumes pangenome, doesn't work for panproteome!
                    genome_syn_dis.append((1-shared)).append(",");
                } else {
                    genome_syn_dis.append((1-shared)).append("\n");
                }
            }
        }
        write_string_to_file_in_DB(header + genome_syn_dis.toString(), "gene_classification/distances_for_tree/genome_distance_syntelogs.csv");
    }

    /**
     *
     * @param sequence_shared_syntelogs
     * @param sequence_total_syntelogs
     */
    private void sequenceSyntelogDistance(int[][] sequence_shared_syntelogs, int[][] sequence_total_syntelogs) {
        if (!compare_sequences) {
            return;
        }
        ArrayList<String> selectedSequencesList = new ArrayList<>(selectedSequences);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(
                WORKING_DIRECTORY + "gene_classification/distances_for_tree/sequence_distance_syntelogs.csv"))) {
            out.write("Sequences,");

            for (int i = 0; i < selectedSequences.size(); i++) {
                if (i != selectedSequences.size()-1) {
                    out.write( selectedSequencesList.get(i) + ",");
                } else {
                    out.write(selectedSequencesList.get(i) + "\n");
                }
            }

            int counter = 0;
            for (int i = 0; i < selectedSequences.size(); i++) {
                counter++;
                if (counter % 100 == 0 || counter == selectedSequences.size()) {
                    System.out.print("\rWriting syntelog gene distance: Sequence " + counter + "/" + selectedSequences.size() + "   ");
                }
                out.write(selectedSequencesList.get(i) + ",");
                for (int j = 0; j < selectedSequences.size(); j++) {
                    double shared;
                    if (j < i) {
                        shared = divide(sequence_shared_syntelogs[j][i], sequence_total_syntelogs[j][i]);
                    } else {
                        shared = divide(sequence_shared_syntelogs[i][j], sequence_total_syntelogs[i][j]);
                    }
                    if (j != selectedSequences.size()-1) {
                        out.write((1-shared) + ",");
                    } else {
                        out.write((1-shared) + "\n");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create core_groups.csv, contains the homology_group node identifiers of the CORE homology groups.
     * @param groupsPerClass holds homology_group nodes for 7 possible keys: "core", "accessory", "unique", a phenotypeValue with "#total" / "#shared" / "#exclusive" "#specific"
     * @return
     */
    public int writeCoreHomologyGroups(HashMap<String, ArrayList<Node>> groupsPerClass) {
        if (!groupsPerClass.containsKey("core")) {
            write_string_to_file_in_DB("", "gene_classification/core_groups.csv");
            return 0;
        }

        StringBuilder header = new StringBuilder("# Core threshold was " + core_threshold + " genomes. Included genomes: " );
        for (String genome : genomeList) {
            header.append(genome).append(",");
        }

        StringBuilder output = new StringBuilder();
        ArrayList<Node> hmgroups = groupsPerClass.get("core");
        for (Node hmgroup : hmgroups) {
            long hm_id = hmgroup.getId();
            output.append(hm_id).append(",");
        }
        writeStringToFile(header.toString().replaceFirst(".$","") + " \n" +
                output.toString().replaceFirst(".$",""), WORKING_DIRECTORY + "gene_classification/group_identifiers/core_groups.csv", false, false);
        return hmgroups.size();
    }


    private void writeSingleCopyGroups(ArrayList<Node> singleCopyGroupNodes, String subgenomeString) {
        StringBuilder output = new StringBuilder();
        if (singleCopyGroupNodes.isEmpty()) {
            output.append("No single-copy orthologs were found");
        } else {
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                for (Node homologyNode : singleCopyGroupNodes) {
                    output.append(homologyNode.getId()).append(",");
                }
                tx.success(); // transaction successful, commit changes
            }
        }
        write_SB_to_file_in_DB(output, "gene_classification/group_identifiers/" + subgenomeString + "single_copy_orthologs.csv");
    }

    /**
     * Creates mlsa_suggestions.txt when --mlsa argument is included
     * @param singleCopyGroupNodes
     */
    private void writeMlsaSuggestions(ArrayList<Node> singleCopyGroupNodes) {
        if (PROTEOME && !Mode.contains("MLSA")) {
            return;
        }

        int counter = 0;
        StringBuilder mlsa_builder = new StringBuilder("# Suitable genes for 'mlsa_find_genes'\n");
        StringBuilder mlsa_builder2 = new StringBuilder("# Possible gene names when using 'mlsa_find_genes' with --extensive\n");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node homlogyNode : singleCopyGroupNodes) {
                counter++;
                if (counter % 100 == 0 || counter == singleCopyGroupNodes.size()) {
                    System.out.print("\r Gathering single-copy gene info: " + counter + "/" + singleCopyGroupNodes.size());
                }
                Iterable<Relationship> relationships = homlogyNode.getRelationships(RelTypes.has_homolog);
                HashSet<String> values_set = new HashSet<>();
                HashSet<String> values_set2 = new HashSet<>();
                for (Relationship rel : relationships) {
                    if (values_set.size() > 1 && values_set2.size() > 1 || values_set.contains("unknown")) { // when higher than 1, no longer a suitable group
                        break;
                    }
                    Node mrna_node = rel.getEndNode();
                    String mrna_name = getMrnaNodeName(mrna_node);
                    if (mrna_name.equals("-")) {
                        mrna_name = "unknown";
                    }
                    values_set.add(mrna_name);
                    String[] mrna_array = mrna_name.split("_");
                    if (!mrna_array[0].equals(mrna_name)) {
                        values_set2.add(mrna_array[0]);
                    }
                }
                if (values_set.contains("unknown")) {
                    continue;
                }
                if (values_set.size() == 1) {
                    mlsa_builder.append(values_set.iterator().next()).append("\n");
                }
                if (values_set2.size() == 1) { // it is the same gene but has additional copies
                    mlsa_builder2.append(values_set2.iterator().next()).append(": ");
                    for (String name : values_set) {
                        mlsa_builder2.append(name).append(", ");
                    }
                    mlsa_builder2.append("\n");
                }
            }
            tx.success(); // transaction successful, commit changes
        }
        write_string_to_file_in_DB(mlsa_builder + mlsa_builder2.toString(), "gene_classification/mlsa_suggestions.txt");
    }

    /**
     * Creates accessory_groups.csv, contains the node identifiers of ACCESSORY homology groups.
     * The groups are ordered (descendingly) by the group size based on the total number of genomes present.
     * @param accessoryGroupsPerGenomeCount key is number of genomes the group is found. value are homology node identifiers
     */
    private void writeAccessoryGroups(HashMap<Integer, StringBuilder> accessoryGroupsPerGenomeCount) {
        StringBuilder output = new StringBuilder();
        for (int i = core_threshold-1; i > 1; --i) {
            if (accessoryGroupsPerGenomeCount.containsKey(i)) {
                String groupIds = accessoryGroupsPerGenomeCount.get(i).toString().replaceFirst(".$", ""); // retrieve & immediately remove last character
                String[] groupsIdsArray = groupIds.split(",");
                output.append("#").append(i).append(" genomes ").append(percentageAsString(i, core_threshold, 2)).append("%. ").append(groupsIdsArray.length).append(" groups\n")
                        .append(groupIds).append("\n\n");
            } else {
                output.append("#").append(i).append(" genomes ").append(percentageAsString(i, core_threshold, 2)).append("%. 0 groups\n\n");
            }
        }
        writeStringToFile(output.toString(), WORKING_DIRECTORY + "gene_classification/group_identifiers/accessory_groups.csv", false, false);
    }
}
