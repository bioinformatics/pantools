package nl.wur.bif.pantools.analysis.map_reads;

import htsjdk.samtools.*;
import htsjdk.samtools.fastq.FastqReader;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.analysis.map_reads.parallel.MapReadsParallel;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class MapReads {
    private AtomicLong[] num_shared_mapping;
    private AtomicLong[] num_unique_mapping;
    private AtomicLong[] num_unmapped;

    public MapReads() {
        map_reads();
    }

    /**
     * Maps a single or paired-end library to the pangenome, generating a SAM/BAM
     * file for each genome.
     */
    private void map_reads() {
        Pantools.logger.info("Mapping single or paired-end short reads to the pangenome.");
        if (OUTPUT_PATH.equals(WORKING_DIRECTORY)) { // no --output-path given, write to database
            OUTPUT_PATH += "read_mapping/";
            new File(WORKING_DIRECTORY + "read_mapping").mkdir();
        }
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            throw new RuntimeException("Database not found");
        }
        report_number_of_threads();
        int i, j, t, genome;
        Node pangenome_node;
        BufferedWriter out;
        SAMFileWriter[] sams;
        SAMFileHeader[] headers;
        int total_unique, total_mapped, total_unmapped;
        boolean paired;
        if (PATH_TO_THE_FIRST_SRA == null) {
            Pantools.logger.error("PATH_TO_THE_FIRST_SRA is empty.");
            System.exit(1);
        }
        paired = PATH_TO_THE_SECOND_SRA != null;
        if (INTERLEAVED) {
            paired = true; // both read pairs are stored in the same file
        }

        FastqReader[] reader = new FastqReader[2];
        reader[0] = new FastqReader(new File(PATH_TO_THE_FIRST_SRA), true);
        if (paired && !INTERLEAVED) {
            reader[1] = new FastqReader(new File(PATH_TO_THE_SECOND_SRA), true);
        }
        if (paired && MAX_FRAGMENT_LENGTH == 4998) { // 4998 is the default value
            MAX_FRAGMENT_LENGTH = 5000;
            Pantools.logger.info("No --max-fragment-length was set. Default is {}", MAX_FRAGMENT_LENGTH);
        }
        connect_pangenome();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome("map"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            if (pangenome_node == null) {
                Pantools.logger.error("Can not locate database node.");
                System.exit(1);
            }
            tx.success();
        }

        int extra = 0;
        if (ALIGNMENT_MODE < 0) { // when competitive mapping
            extra = 1;
        }
        num_shared_mapping = new AtomicLong[GENOME_DB.num_genomes + 1];
        num_unique_mapping = new AtomicLong[GENOME_DB.num_genomes + 1];
        num_unmapped = new AtomicLong[GENOME_DB.num_genomes + 1 + extra];
        AtomicLong number_of_reads = new AtomicLong(0);
        AtomicLong number_of_alignments = new AtomicLong(0);
        AtomicLong number_of_hits = new AtomicLong(0);
        ArrayList<Integer>[] genome_numbers = retrieve_genomes_to_map_against();
        adj_total_genomes = genome_numbers[0].size();
        Pantools.logger.info("OUT FORMAT: {}", OUTFORMAT);
        if (OUTFORMAT.equals("BAM") || OUTFORMAT.equals("SAM")) {
            sams = new SAMFileWriter[GENOME_DB.num_genomes + 1 + extra];
            headers = new SAMFileHeader[GENOME_DB.num_genomes + 1 + extra];
            for (i = 0; i < genome_numbers[0].size(); ++i) {
                genome = genome_numbers[0].get(i);
                headers[genome] = new SAMFileHeader();
                for (j = 1; j <= GENOME_DB.num_sequences[genome]; ++j) {
                    headers[genome].addSequence(new SAMSequenceRecord(GENOME_DB.sequence_titles[genome][j].split("\\s")[0],
                            (int) GENOME_DB.sequence_length[genome][j]));
                }
                headers[genome].addProgramRecord(new SAMProgramRecord("PanTools"));
                // add read group header line in SAM
                if(READ_GROUP != null) headers[genome].addReadGroup(READ_GROUP);
                if (OUTFORMAT.equals("BAM")) {
                    sams[genome] = new SAMFileWriterFactory().makeBAMWriter(headers[genome], false,
                            new File(OUTPUT_PATH + "/pantools_" + genome + ".bam"));
                } else { // SAM
                    sams[genome] = new SAMFileWriterFactory().makeSAMWriter(headers[genome], false,
                            new File(OUTPUT_PATH + "/pantools_" + genome + ".sam"));
                }
            }
            if (ALIGNMENT_MODE < 0) { // when competitive mapping
                headers[genome_numbers[0].size()+1] = new SAMFileHeader();
                sams[genome_numbers[0].size()+1] = new SAMFileWriterFactory().makeSAMWriter(headers[genome_numbers[0].size()+1], false,
                            new File(OUTPUT_PATH + "/unmapped.sam"));
            }
        } else { // --output-format none
            sams = null;
        }

        if (!genome_numbers[0].isEmpty()) {
            String single_or_paired = "SINGLE-END";
            if (paired) {
                single_or_paired = "PAIRED-END";
            }
            Pantools.logger.info("Mapping {} reads on {} genome(s).", single_or_paired, genome_numbers[0].size());
            try {
                ExecutorService es = Executors.newFixedThreadPool(THREADS);
                for (t = 0; t < THREADS; ++t) {
                    es.execute(new MapReadsParallel(t, genome_numbers[t], paired, sams, reader, number_of_reads,
                            number_of_hits, number_of_alignments, num_shared_mapping, num_unique_mapping, num_unmapped));
                }
                es.shutdown();
                es.awaitTermination(10, TimeUnit.DAYS);
            } catch (InterruptedException e) {

            }
            if (paired) {
                System.out.println("\rProcessed " + number_of_reads.get()*2 + " paired-end reads, " + number_of_reads.get() + " read pairs.");
            } else {
                System.out.println("\rProcessed " + number_of_reads.get() + " single-end reads");
            }

            total_unique = total_mapped = total_unmapped = 0;
            try {
                out = new BufferedWriter(new FileWriter(OUTPUT_PATH + "/mapping_summary.txt"));
                out.write("Genome\tTotal\tUnique\tUnmapped\n");
                Pantools.logger.info("Genome\tTotal\tUnique\tUnmapped");
                for (i = 0; i < genome_numbers[0].size(); ++i) {
                    genome = genome_numbers[0].get(i);
                    total_unique += num_unique_mapping[genome].get();
                    total_mapped += num_shared_mapping[genome].get() +
                                    num_unique_mapping[genome].get();
                    total_unmapped += num_unmapped[genome].get();
                    out.write(genome + "\t" + (num_shared_mapping[genome].get() + num_unique_mapping[genome].get())
                            + "\t" + num_unique_mapping[genome].get() + "\t" + num_unmapped[genome].get() + "\n");
                    Pantools.logger.info("{}\t{}\t{}\t{}", genome, (num_shared_mapping[genome].get() + num_unique_mapping[genome].get()), num_unique_mapping[genome].get(), num_unmapped[genome].get());
                    if (sams != null) {
                        sams[genome].close();
                    }
                }
                if (ALIGNMENT_MODE < 0) { // competitive mapping
                    sams[genome_numbers[0].size()+1].close();
                    System.out.println("........................................");
                    if (paired) {
                        Pantools.logger.info("+ Reads where none of the pair mapped: {}", num_unmapped[genome_numbers[0].size()+1].get());
                    } else {
                        Pantools.logger.info("+ Unmapped reads: {}", num_unmapped[genome_numbers[0].size()+1].get());
                    }
                    total_unmapped += num_unmapped[genome_numbers[0].size()+1].get();
                    out.write("Unmapped reads:\t\t\t" + num_unmapped[genome_numbers[0].size()+1].get() + "\n");
                }
                out.close();
            } catch (IOException ex) {
                Pantools.logger.info(ex.getMessage());
            }
            System.out.println("........................................");
            Pantools.logger.info("\t{}\t{}\t{}", total_mapped, total_unique, total_unmapped);
            Pantools.logger.info("Number of hits = {}", number_of_hits);
            Pantools.logger.info("Number of alignments = {}", number_of_alignments);
            Pantools.logger.info("Mapping statitics written to: {}mapping_summary.txt", OUTPUT_PATH);
            if (OUTFORMAT.equals("BAM") || OUTFORMAT.equals("SAM")) {
                Pantools.logger.info("Output {} files written to {}", OUTFORMAT, OUTPUT_PATH);
            }
        }
    }

    /**
     * Read file provided via --genome-numbers or read genomes directly from command line via --reference
     * @return
     */
    public ArrayList<Integer>[] retrieve_genomes_to_map_against() {
        ArrayList<Integer>[] genome_numbers;
        genome_numbers = new ArrayList[THREADS];
        int t, genome_nr;
        for (t = 0; t < THREADS; ++t) {
            genome_numbers[t] = new ArrayList();
        }
        for (genome_nr = 1; genome_nr <= GENOME_DB.num_genomes; genome_nr++) {
            if (!skip_list.contains(genome_nr)) {
                for (t = 0; t < THREADS; ++t) {
                    genome_numbers[t].add(genome_nr);
                }
                num_shared_mapping[genome_nr] = new AtomicLong(0);
                num_unique_mapping[genome_nr] = new AtomicLong(0);
                num_unmapped[genome_nr] = new AtomicLong(0);
            } else {
                Pantools.logger.warn("Genome {} is not found in the database.", genome_nr);
            }
        }
        if (ALIGNMENT_MODE < 0) { // when competitive mapping
            num_unmapped[genome_numbers[0].size()+1] = new AtomicLong(0);
        }
        return genome_numbers;
    }
}
