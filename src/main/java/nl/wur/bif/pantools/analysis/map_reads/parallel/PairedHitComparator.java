package nl.wur.bif.pantools.analysis.map_reads.parallel;

import java.util.Comparator;

/**
 * Implements a comparator for paired-end genomic hits
 */
public class PairedHitComparator implements Comparator<PairedHit> {
    @Override
    public int compare(PairedHit x, PairedHit y) {
        if (x.get_score() > y.get_score())
            return -1;
        else if (x.get_score() < y.get_score())
            return 1;
        else if (x.fragment_length < y.fragment_length)
            return -1;
        else if (x.fragment_length > y.fragment_length)
            return 1;
        else
            return 0;
    }
}
