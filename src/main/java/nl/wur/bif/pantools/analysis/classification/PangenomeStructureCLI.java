package nl.wur.bif.pantools.analysis.classification;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Determine the openness of the pangenome based on k-mer sequences.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "pangenome_structure", sortOptions = false, abbreviateSynopsis = true)
public class PangenomeStructureCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--seed"})
    @Min(value = 0, message = "{min.seed}")
    long seed;

    @ArgGroup
    VariationOptions variationOptions;
    static boolean pavs = false;
    static boolean kmer = false;

    static class VariationOptions {
        @Option(names = {"--pavs"})
        void isVariation(boolean value) {
            pavs = value;
        }

        @Option(names = {"-k", "--kmer"})
        void isKmer(boolean value) {
            kmer = value;
        }
    }

    @Option(names = "--loops")
    @Min(value = 0, message = "{min.loops}") // 0 is default
    @Max(value = 1000000, message = "{max.loops}")
    long loops;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph();

        crossValidate();

        // set seed if not specified
        if (seed == 0) seed = System.currentTimeMillis();
        Pantools.logger.debug("Seed for random number generator: " + seed);

        if (kmer) {
            if (loops == 0) loops = 100; // set default value based on -k
            setGlobalParameters(); //TODO: use local parameters instead
            classification.pangenome_size_kmer(seed); //cannot use pavs
        } else {
            if (loops == 0) loops = 10000; // set default value based on -k
            setGlobalParameters(); //TODO: use local parameters instead
            classification.pangenome_size_genes(pavs, seed);
        }
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        NODE_VALUE = Long.toString(loops);
    }

    /**
     * Cross validate the options: when --seed is used and --threads is higher than 1, we cannot guarantee reproducibility (only for --kmer).
     * @throws ParameterException when the options are not valid.
     */
    private void crossValidate() throws ParameterException {
        if (seed > 0 && threadNumber.getnThreads() > 1 && kmer) {
            throw new ParameterException(spec.commandLine(), "Cannot guarantee reproducibility when using --seed and --threads > 1");
        }
    }
}
