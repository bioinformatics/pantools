package nl.wur.bif.pantools.analysis.map_reads.parallel;


/**
 * Implements a class for genomic hit of a single-layout read
 */
public class SingleHit {
    public int genome;
    public int sequence;
    public double identity;
    public int score;
    public int start;
    public int offset;
    public int length;
    public int deletions;
    public boolean forward;
    public String cigar;
    public String reference;

    public SingleHit(int gn, int sq, double idn, int sc, int st, int off, int len, int del, boolean fw, String cg, String r) {
        genome = gn;
        sequence = sq;
        identity = idn;
        score = sc;
        start = st;
        offset = off;
        length = len;
        deletions = del;
        forward = fw;
        cigar = cg;
        reference = r;
    }

    public SingleHit(SingleHit h) {
        genome = h.genome;
        sequence = h.sequence;
        identity = h.identity;
        score = h.score;
        start = h.start;
        offset = h.offset;
        length = h.length;
        deletions = h.deletions;
        forward = h.forward;
        cigar = h.cigar;
        reference = h.reference;
    }

    public SingleHit() {

    }


    @Override
    public String toString() {
        return "(genome:" + genome +
                ",sequence:" + sequence +
                ",identity:" + identity +
                ",score:" + score +
                ",start:" + start +
                ",offset:" + offset +
                ",length:" + length +
                ",deletions:" + deletions +
                ",forward:" + forward +
                ",reference:" + reference +
                ",cigar:" + cigar +")";
    }
}
