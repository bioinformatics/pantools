package nl.wur.bif.pantools.analysis.gene_retention;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools gene_retention
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "gene_retention", sortOptions = false, abbreviateSynopsis = true)
public class GeneRetentionCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--input-sequences"})
    private Path inputSequences;

    @Option(names = {"--window-length"})
    @Min(value = 10)
    @Max(value = 1000)
    private int windowLength;

    @Option(names = {"--sequences-plot"})
    @Min(value = 1)
    @Max(value = 1000)
    private int allowedSequences;

    @Option(names = {"--sequences-genome"})
    @Min(value = 1)
    @Max(value = 100)
    private int sequencesPerGenome;

    @Option(names = "--coloring")
    @Pattern(regexp = "distinct|phasing", message = "{pattern.coloring}")
    private String coloring;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        new GeneRetention().geneRetentionVisualization(
                windowLength, allowedSequences, sequencesPerGenome, inputSequences, coloring);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        if (coloring.equals("phasing")) {PHASED_ANALYSIS = true;}
        if (coloring.equals("distinct")) {Mode = "COLOR";}
    }
}
