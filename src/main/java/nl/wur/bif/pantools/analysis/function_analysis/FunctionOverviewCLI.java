package nl.wur.bif.pantools.analysis.function_analysis;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create an overview table for each functional annotation type in the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "function_overview", sortOptions = false)
public class FunctionOverviewCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-A", "--annotations-file"})
    @InputFile(message = "{file.annotations}")
    Path annotationsFile;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        functionalAnnotations.functional_annotation_overview();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (annotationsFile != null) PATH_TO_THE_ANNOTATIONS_FILE = annotationsFile.toString();
    }
}
