package nl.wur.bif.pantools.analysis.kmer_classification;

import nl.wur.bif.pantools.analysis.classification.Classification;
import nl.wur.bif.pantools.utils.ExecCommand;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class KmerClassification {

    boolean compressed_kmers = false;
    ConcurrentHashMap<String, ArrayList<Integer>> sequences_per_genome;
    private static String percentage_counter;

    private int[][] seq_distinct_total_kmers;
    private int[][] seq_distinct_shared_kmers;
    private int[][] seq_all_total_kmers;
    private int[][] seq_all_shared_kmers;

    private int[][] genome_distinct_total_kmers;
    private int[][] genome_distinct_shared_kmers;
    private int[][] genome_all_total_kmers;
    private int[][] genome_all_shared_kmers;

    private int[][] subgenome_distinct_total_kmers;
    private int[][] subgenome_distinct_shared_kmers;
    private int[][] subgenome_all_total_kmers;
    private int[][] subgenome_all_shared_kmers;

    private AtomicLong nodeCounter;
    private AtomicLong fileCounter;
    private AtomicLong distinct_node_count;
    private AtomicLong total_node_count;

    private BlockingQueue<String> string_queue;
    private BlockingQueue<int[]> frequency_queue;
    private BlockingQueue<Node> node_queue;
    private BlockingQueue<HashMap<Node, Integer>> hashmapQueue;

    PhasedFunctionalities pf;

    /*

    */
    public void kmerClassification(boolean compareSequences) {
        if (compareSequences) {
            compare_sequences = true;
        }
        compressed_kmers = false;
        pf = new PhasedFunctionalities();
        long highest_frequency = 0;
        Pantools.logger.info("Calculating CORE, UNIQUE, and ACCESSORY k-mer sequences.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome("kmer_classification"); // stops the progam when run on a panproteome, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if --skip/--reference is provided by user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        if (Mode.equals("0")) {
            System.out.println("\rNo --mode compressed has been selected, counting all k-mers in compressed k-mers."
                    + " K-mer size is " + K_SIZE + ", extracting " + (K_SIZE-1) + " of every compressed k-mer");
        } else if (Mode.contains("COMPRESSED")) {
            Pantools.logger.info("--mode {} is selected. Considering a compressed k-mer as a single k-mer.", Mode);
            compressed_kmers = true;
        } else if (Mode.contains("OCCURRENCE")) {

        } else {
            Pantools.logger.info("--mode {} is not recognized.", Mode);
            return;
        }

        if (PHENOTYPE == null) {
            System.out.println("\rNo --phenotype was provided. Unable to find phenotype shared/specific k-mers.");
        }

        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(10000, false);
        pf.preparePhasedGenomeInformation(false, selectedSequences);
        skip.retrieveSelectedSubgenomes();
        retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user

        if (compareSequences) {
            if (selectedSequences.size() > 50000) {
                Pantools.logger.error("Can only analyze up to 50.000 sequences. Please make a selection using the --skip argument.");
                System.exit(1);
            }
        }
        Classification.core_unique_thresholds_for_classification("k-mers"); // set 'core_threshold' and 'unique_threshold' variables

        delete_directory_in_DB("kmer_classification/temp");
        delete_directory_in_DB("kmer_classification/kmer_identifiers");
        create_directory_in_DB("kmer_classification/distances_for_tree/");
        create_directory_in_DB("kmer_classification/kmer_identifiers");
        create_directory_in_DB("kmer_classification/temp");

        ConcurrentHashMap<Integer, long[]> kmer_freq_map = initializeKmer_freq_map();
        ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map = new ConcurrentHashMap<>(); // key is phenotype
        ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map = new ConcurrentHashMap<>(); // key is frequency of kmer. This can become quite large, written to log every 250k keys
        ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome = initializeFreq_per_genome();
        long[] kmer_class_count = new long[3]; // distinct: [core, accessory, unique]
        retrieve_selected_sequences();
        if (!Mode.contains("OCCURRENCE")) {
            kmer_occur_map = null;
        }

        if (compareSequences) {
            seq_distinct_total_kmers = new int[selectedSequences.size()][selectedSequences.size()];
            seq_distinct_shared_kmers = new int[selectedSequences.size()][selectedSequences.size()];
            seq_all_total_kmers = new int[selectedSequences.size()][selectedSequences.size()];
            seq_all_shared_kmers = new int[selectedSequences.size()][selectedSequences.size()];
        }

        if (!phasingInfoMap.isEmpty()) {
            subgenome_distinct_total_kmers = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
            subgenome_distinct_shared_kmers = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
            subgenome_all_total_kmers = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
            subgenome_all_shared_kmers = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
        }

        genome_distinct_total_kmers = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        genome_distinct_shared_kmers = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        genome_all_total_kmers = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        genome_all_shared_kmers = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];

        HashMap<String, long[]> degen_node_per_seq_genome = gatherDegenerateNodes(compareSequences);
        long[] node_count = gather_kmer_information(kmer_occur_map, kmer_freq_map, pheno_kmer_map, freq_per_genome, kmer_class_count);
        Pantools.logger.info("Generating output files.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            create_kmer_occurrence_output(kmer_occur_map, highest_frequency, freq_per_genome);
            create_kmer_classification_overview(kmer_freq_map, node_count, kmer_class_count, degen_node_per_seq_genome);
            kmer_classi_pheno_overview(pheno_kmer_map);
            tx.success();
        }

        combine_core_kmer_output_files();
        combine_accessory_kmer_output_files();
        combine_unique_kmer_output_files();

        try {
            ArrayList<String> selectGenomeString = new ArrayList<>();
            for (int genomeNr : selectedGenomes) {
                selectGenomeString.add(genomeNr +"");
            }
            createSharedGenesMatrices(genome_all_shared_kmers, genome_all_total_kmers, genome_distinct_shared_kmers, genome_distinct_total_kmers, "genome", selectGenomeString);
            createKmerDistanceRscript("genome");
            if (compareSequences) {
                createKmerDistanceRscript("sequence");
                createSharedGenesMatrices(seq_all_shared_kmers, seq_all_total_kmers, seq_distinct_shared_kmers, seq_distinct_total_kmers, "sequence", new ArrayList<>(selectedSequences));
            }
            if (!phasingInfoMap.isEmpty()) {
                createSharedGenesMatrices(subgenome_all_shared_kmers, subgenome_all_total_kmers, subgenome_distinct_shared_kmers, subgenome_distinct_total_kmers, "subgenome", new ArrayList<>(selectedSubgenomes));
                createKmerDistanceRscript("subgenome");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        delete_directory_in_DB("kmer_classification/temp");
        print_kmer_classification_output(compareSequences);
        GRAPH_DB.shutdown();
    }

    private ConcurrentHashMap<Integer, HashMap<Long, Long>> initializeFreq_per_genome() {
        if (!Mode.contains("OCCURRENCE")) {
            return null;
        }
        ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome = new ConcurrentHashMap<>(); // key is genome. second key is the frequency.
        for (int genomeNr : selectedGenomes) {
            if (Mode.contains("OCCURRENCE")) {
                HashMap<Long, Long> empty_map = new HashMap<>();
                freq_per_genome.put(genomeNr, empty_map);
            }
        }
        return freq_per_genome;
    }

    private ConcurrentHashMap<Integer, long[]> initializeKmer_freq_map() {
        ConcurrentHashMap<Integer, long[]> kmer_freq_map = new ConcurrentHashMap<>(); // key is genome number, long[] with total, core, access, unique, distinct total, distinct core, distinct access, distinct uni
        for (int genomeNr : selectedGenomes) {
            long[] empty_array = new long[8]; // total, core, access, unique, distinct total, distinct core, distinct access, distinct uni
            kmer_freq_map.put(genomeNr, empty_array);
        }
        return kmer_freq_map;
    }

    /**
     * Print which files are created depending on input arguments
     */
    private void print_kmer_classification_output(boolean compareSequences) {
        String dir = WORKING_DIRECTORY + "kmer_classification/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}kmer_classification_overview.txt", dir);
        Pantools.logger.info(" {}kmer_classification_overview.csv", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}kmer_classification_phenotype_overview.txt", dir);
        }
        Pantools.logger.info(" {}shared_kmers_between_genomes.csv", dir);
        if (Mode.contains("OCCURRENCE")) {
            Pantools.logger.info(" {}kmer_occurrence.txt", dir);
        }
        if (NODE_VALUE != null) {
            Pantools.logger.info(" {}highly_occuring_kmers.txt", dir);
        }
        if (compare_sequences) {
            Pantools.logger.info(" {}shared_kmers_between_sequences.csv", dir);
            Pantools.logger.info(" {}sequence_kmer_distance_tree.R (Select one of the three distances)", dir);
            if (!phasingInfoMap.isEmpty()){
                Pantools.logger.info(" {}shared_kmers_between_subgenomes.csv");
            }
        }
        Pantools.logger.info(" {}genome_kmer_distance_tree.R (Select one of the three distances)", dir);
        Pantools.logger.info("Files with 'nucleotide' node identifiers:");
        Pantools.logger.info(" {}kmer_identifiers/core_kmers.csv", dir);
        Pantools.logger.info(" {}kmer_identifiers/accessory_kmers.csv", dir);
        Pantools.logger.info(" {}kmer_identifiers/unique_kmers.csv", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}kmer_identifiers/phenotype_shared_kmers.csv", dir);
            Pantools.logger.info(" {}kmer_identifiers/phenotype_specific_kmers.csv", dir);
            Pantools.logger.info(" {}kmer_identifiers/phenotype_exclusive_kmers.csv", dir);
        }
    }

    /**
     * Number of shared k-mers between sequences
     */
    private void createSharedGenesMatrices(int[][] all_shared_kmers, int[][] all_total_kmers, int[][] distinct_shared_kmers,
                                           int[][] distinct_total_kmers, String type, ArrayList<String> selection) throws IOException {

        DecimalFormat df = new DecimalFormat("0.00000");
        BufferedWriter mash_distance = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/distances_for_tree/" + type + "_mash_distance.csv"));
        BufferedWriter all_kmer_distance = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/distances_for_tree/" + type + "_distance_all_kmers.csv"));
        BufferedWriter distinct_kmer_distance = new BufferedWriter(new FileWriter(WORKING_DIRECTORY +"kmer_classification/distances_for_tree/" + type + "_distance_distinct_kmers.csv"));

        BufferedWriter allSharedCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/temp/all_shared_kmers.csv"));
        BufferedWriter distinctSharedCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/temp/distinct_shared_kmers.csv"));
        BufferedWriter distinctTotalCounts = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/temp/distinct_total_kmers.csv"));
        BufferedWriter kmer_count_all_total = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/temp/all_total_kmers.csv"));

        BufferedWriter distinctPercentage = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_" + type + "s.csv"));
        BufferedWriter semiPercentage = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/temp/semi_percentage.csv"));

        String typeCapitalised = capitalize_first_letter(type);
        StringBuilder headerBuilder = new StringBuilder(typeCapitalised + "s,");

        for (int i = 0; i < selection.size(); i ++) {
            String sequenceId = selection.get(i);
            String add2 = "";
            if (PHASED_ANALYSIS && type.equals("sequence")) {
                add2 = "_" + pf.getPhasingIdentifier(sequenceId, false);
            }
            headerBuilder.append(sequenceId).append(add2);
            if (i != selection.size()-1) {
                headerBuilder.append(",");
            }
        }
        String header = headerBuilder.toString() + "\n";
        mash_distance.write(header);
        all_kmer_distance.append(header);
        distinct_kmer_distance.append(header);

        distinctPercentage.write("#This file contains six matrices: " +
                "#Two different k-mer counts:\n" +
                "# DISTINCT  : always count a maximum of 1\n" +
                "# SEMI      : Counts k-mers of " + type + " A up to number of k-mers in " + type + " B.\n" +
                //"# EVERYTHING: Count all genes of " + type+ " A as long as the other " +  type + " has a copy.\n" +
                "\n" +
                "#1 Percentages from DISTINCT k-mer counts. Derived from #3, #5\n" +
                "#2 Percentages from SEMI k-mer counts. Derived from #4, #6\n" +
                "#3 TOTAL DISTINCT k-mers \n" +
                "#4 TOTAL SEMI k-mer counts \n" +
                "#5 SHARED DISTINCT k-mer counts\n" +
                "#6 SHARED SEMI k-mer counts\n"
                + "\n" +
                "#1 Percentage DISTINCT k-mer counts\n" + header
        );
        semiPercentage.write("\n#2 Percentage SEMI k-mer counts\n" + header );
        distinctTotalCounts.write( "\n#2 TOTAL distinct k-mers\n" + header );
        distinctSharedCounts.write("\n#5 SHARED distinct k-mers\n" + header);
        allSharedCounts.write("\n#6 SHARED (all) k-mers\n"+ header);
        kmer_count_all_total.write("\n#3 TOTAL (all) k-mers\n"+ header);

        int sequence_counter = 0;
        for (int i = 0; i < selection.size(); i ++) {
            String id = selection.get(i);
            String add2 = "";
            if (PHASED_ANALYSIS && type.equals("sequence")) {
                add2 = "_" + pf.getPhasingIdentifier(id, false);
            }

            mash_distance.write(id + add2 + ",");
            all_kmer_distance.write(id + add2 + ",");
            distinct_kmer_distance.write(id + add2 + ",");
            allSharedCounts.write(id + add2 + ",");
            distinctSharedCounts.write(id + add2 + ",");
            distinctTotalCounts.write(id + add2 + ",");
            kmer_count_all_total.write(id + add2 + ",");
            distinctPercentage.write(id + add2 + ",");
            semiPercentage.write(id + add2 + ",");

            sequence_counter ++;
            if (sequence_counter % 100 == 0 ) {
                System.out.print("\rWriting shared k-mers between " + type + "s " + (i+1));
            }
            for (int j = 0; j < selection.size(); j ++) {
                int all_shared_kmersCounts = all_shared_kmers[i][j];
                int all_total_kmersCounts = all_total_kmers[i][j];
                int distinct_shared_kmersCounts = distinct_shared_kmers[i][j];
                int distinct_total_kmersCounts = distinct_total_kmers[i][j];
                if (j < i) {
                    all_shared_kmersCounts = all_shared_kmers[j][i];
                    all_total_kmersCounts = all_total_kmers[j][i];
                    distinct_shared_kmersCounts = distinct_shared_kmers[j][i];
                    distinct_total_kmersCounts = distinct_total_kmers[j][i];
                }
                double percentage_distinct_shared = divide(distinct_shared_kmersCounts, distinct_total_kmersCounts); // distinct
                double percentage_all_shared = divide(all_shared_kmersCounts, all_total_kmersCounts); // all kmers
                if (i == j) {
                    percentage_distinct_shared = 1;
                    percentage_all_shared = 1;
                }
                if (percentage_distinct_shared == 0) {
                    mash_distance.write("1");
                } else {
                    double mash_dist = -1.0/ K_SIZE * Math.log(percentage_distinct_shared); // 𝑑=−1/𝑘 * ln(𝑤/𝑡) where k is 17; w/t is the jaccard index.
                    mash_distance.write(Math.abs(mash_dist) +"");
                }

                distinctPercentage.write(df.format(percentage_distinct_shared*100) +"");
                semiPercentage.write(df.format(percentage_all_shared*100) +"");

                distinct_kmer_distance.write((1 - percentage_distinct_shared) +"");
                all_kmer_distance.write((1 - percentage_all_shared) + "");
                allSharedCounts.write(all_shared_kmersCounts + "");
                distinctSharedCounts.write(distinct_shared_kmersCounts +"");
                kmer_count_all_total.write(all_total_kmersCounts +"");
                distinctTotalCounts.write(distinct_total_kmersCounts +"");
                if (j != selection.size()-1) {
                    mash_distance.write(",");
                    distinct_kmer_distance.write(",");
                    all_kmer_distance.write(",");

                    allSharedCounts.write(",");
                    distinctSharedCounts.write(",");
                    kmer_count_all_total.write(",");
                    distinctTotalCounts.write(",");

                    distinctPercentage.write(",");
                    semiPercentage.write(",");
                }
            }

            mash_distance.write("\n");
            all_kmer_distance.write("\n");
            distinct_kmer_distance.write("\n");
            allSharedCounts.write("\n");
            distinctSharedCounts.write("\n");
            kmer_count_all_total.write("\n");
            distinctTotalCounts.write("\n");
            distinctPercentage.write("\n");
            semiPercentage.write("\n");
        }

        mash_distance.close();
        all_kmer_distance.close();
        distinct_kmer_distance.close();

        allSharedCounts.close();
        distinctSharedCounts.close();
        kmer_count_all_total.close();
        distinctTotalCounts.close();
        distinctPercentage.close();
        semiPercentage.close();

        append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/semi_percentage.csv",
                WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_" + type + "s.csv");
        append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/distinct_total_kmers.csv",
                WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_" + type + "s.csv");
        append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/all_total_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_" + type + "s.csv");
        append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/distinct_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_" + type + "s.csv");
        append_file_to_other_file(WORKING_DIRECTORY + "kmer_classification/temp/all_shared_kmers.csv",
                    WORKING_DIRECTORY + "kmer_classification/shared_kmers_between_"+ type + "s.csv");
        System.out.print("\r                                                      ");
    }

    /**
     *
     * @param kmer_occur_map
     */
    private synchronized void write_kmer_occurrence_output(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map) {
        TreeSet<Long> current_frequencies = new TreeSet<>(kmer_occur_map.keySet());
        StringBuilder output = new StringBuilder();
        for (Long frequency : current_frequencies) {
            output.append("#").append(frequency).append("#,");
            ArrayList<Node> nodes = kmer_occur_map.get(frequency);
            kmer_occur_map.remove(frequency);
            for (Node nuc_node : nodes) {
                output.append(nuc_node.getId()).append(",");
            }
            output.append("\n");
        }
        if (!check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/kmer.log")) {
            write_SB_to_file_full_path(output, WORKING_DIRECTORY + "kmer_/kmer.log");
        } else {
            append_SB_to_file_full_path(output, WORKING_DIRECTORY + "kmer_classification/kmer.log");
        }
    }

    /**
     *
     * @param kmer_occur_map
     * @param highest_frequency
     * @param freq_per_genome
     */
    private void create_kmer_occurrence_output(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
                                              long highest_frequency, ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome) {
        if (!Mode.contains("OCCURRENCE")) {
            return;
        }
        long[] compressed_length_freq = new long[10000];
        StringBuilder high_freq_builder = new StringBuilder("#K-mer nodes with a frequency above ");
        int threshold = 0;
        if (NODE_VALUE != null) {
            threshold = Integer.parseInt(NODE_VALUE);
            high_freq_builder.append(NODE_VALUE).append("\n");
        }

        kmer_occur_map = read_kmer_occurrence_output(kmer_occur_map);
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "kmer_classification/kmer_occurrence.txt"))) {
            out.write("##This file consists of three parts\n##Part1\n"
                    + "#occurrence of a k-mer in the pangenome, number of k-mers with X occurrence for COMPRESSED k-mers, "
                    + "number of k-mers with X occurrence for TOTAL k-mers\n");

            for (long i=1; i <= highest_frequency; i++) {
                System.out.print("\rWriting k-mers with frequency of " + i + "/" + highest_frequency + "      "); // spaces are intentional
                ArrayList<Node> node_list = kmer_occur_map.get(i);
                if (node_list == null) {
                    continue;
                }
                long actual_freq = 0;
                int freq = node_list.size();
                for (Node nuc_node : node_list) {
                    int kmer_size = (int) nuc_node.getProperty("length");
                    actual_freq += kmer_size - K_SIZE + 1;
                }
                out.write(i + ", " + freq + ", " + actual_freq + "\n");
            }

            out.write("\n##Part2\n#occurrence of a k-mer within a single genome, number of k-mers with X occurrence\n");
            for (int i=1; i <= GENOME_DB.num_genomes; i++) {
                out.write("#Genome " + i + "\n");
                HashMap<Long, Long> found_frequencies_map = freq_per_genome.get(i);
                for (long j = 1; j <= highest_frequency; j++) {
                    if (found_frequencies_map.containsKey(j)) {
                        long freq = found_frequencies_map.get(j);
                        out.write(j + ", " + freq + "\n");
                    }
                }
                out.write("\n");
            }

            out.write("\n##Part 3. Frequency array per kmer\n"
                    + "#Node identifier, total frequency, k-mer length, k-mer sequence, [frequency per genome]\n");
            for (long i=1; i <= highest_frequency; i++) {
                System.out.print("\rWriting k-mers with frequency of " + i + "/" + highest_frequency + "       "); // spaces are intentional
                ArrayList<Node> node_list = kmer_occur_map.get(i);
                kmer_occur_map.remove(i);
                if (node_list == null) {
                    continue;
                }
                out.write("Frequency of " + i + "\n");
                for (Node nuc_node : node_list) {
                    long node_id = nuc_node.getId();
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    long[] freq_array2 = remove_first_position_array(freq_array);
                    int freq1 = get_kmer_frequency(nuc_node);
                    if (NODE_VALUE != null && freq1 > threshold) {
                        high_freq_builder.append(node_id).append(",");
                    }
                    String sequence = (String) nuc_node.getProperty("sequence");
                    out.write(node_id + ", " + freq1 + ", " + sequence.length() + ", " + sequence +
                            ", " + Arrays.toString(freq_array2) + "\n");
                    compressed_length_freq[sequence.length()] += 1;
                }
            }

            out.write("\n#Compressed k-mer, Count of distinct compressed kmer lengths\n");
            for (int i= 0; i < compressed_length_freq.length; i++) {
                if (0 == compressed_length_freq[i]) {
                    continue;
                }
                out.write(i + ", " + compressed_length_freq[i] + "\n");
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}kmer_classification/kmer_occcurance.txt", WORKING_DIRECTORY);
            System.exit(1);
        }

        if (NODE_VALUE != null) {
            write_string_to_file_in_DB(high_freq_builder.toString().replaceFirst(".$",""),
                    "kmer_classification/highly_occurring_kmers.txt" );
        }
        System.out.print("\r                                                           "); // spaces are intentional
    }

    /**
     *
     * @param kmer_occur_map
     * @return
     */
    private synchronized ConcurrentHashMap<Long, ArrayList<Node>> read_kmer_occurrence_output(
            ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map) {

        if (!check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/kmer.log")) {
            return kmer_occur_map;
        }
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "kmer_classification/kmer.log"))) {
            System.out.print("\rReading k-mer frequencies...");
            while (in.ready()) {
                String line = in.readLine().trim();
                String[] line_array = line.split(",");
                long freq = Long.parseLong(line_array[0].replace("#",""));
                for (int i=1; i < line_array.length; i++) {
                    long node_id = Long.parseLong(line_array[i]);
                    Node nuc_node = GRAPH_DB.getNodeById(node_id);
                    try_incr_AL_chashmap(kmer_occur_map, freq, nuc_node);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}kmer_classification/kmer.log", WORKING_DIRECTORY);
            System.exit(1);
        }
        delete_file_full_path(WORKING_DIRECTORY + "kmer_classification/kmer.log");
        return kmer_occur_map;
    }

    /**
     *
     * @param pheno_kmer_map
     */
    private void kmer_classi_pheno_overview(ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map) {
        if (PHENOTYPE == null) {
            return;
        }
        StringBuilder overview_builder = new StringBuilder("Phenotype, threshold, total members: members (genome numbers)\n");
        for (String phenotype : phenotype_map.keySet()) {
            int[] value = phenotype_map.get(phenotype);
            int threshold = phenotype_threshold_map.get(phenotype);
            overview_builder.append(phenotype).append(", ").append(threshold).append(", ").append(value.length).append(": ") // header of kmer_classification_phenotype_overview.txt
                    .append(Arrays.toString(value).replace(" ","").replace("[","").replace("]","")).append("\n");
        }
        overview_builder.append("\nOn each line. The phenotype: number of nodes (compressed k-mers), total number of k-mers\n"
                + "Phenotype shared\n");
        write_pheno_kmer_classi_file(overview_builder, "kmer_classification/kmer_identifiers/phenotype_shared_kmers.csv", pheno_kmer_map, "#shared");
        overview_builder.append("\nPhenotype specific\n");
        write_pheno_kmer_classi_file(overview_builder,"kmer_classification/kmer_identifiers/phenotype_specific_kmers.csv", pheno_kmer_map, "#specific");
        overview_builder.append("\nPhenotype exclusive\n");
        write_pheno_kmer_classi_file(overview_builder,"kmer_classification/kmer_identifiers/phenotype_exclusive_kmers.csv", pheno_kmer_map, "#exclusive");
        write_SB_to_file_in_DB(overview_builder, "kmer_classification/kmer_classification_phenotype_overview.txt");
        System.out.print("\r                                        "); // spaces are intentional
    }

    /**
     * Creates phenotype_specific_kmers.csv and phenotype_shared_kmers.csv
     * @param overview_builder
     * @param filename
     * @param pheno_kmer_map
     * @param phenotype_class
     */
    private void write_pheno_kmer_classi_file(StringBuilder overview_builder, String filename, ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map,
                                             String phenotype_class) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + filename))) {
            for (String phenotype : phenotype_map.keySet()) {
                if (!pheno_kmer_map.containsKey(phenotype + phenotype_class)) {
                    out.write("#Phenotype " + phenotype + ": 0 nodes and k-mers\n");
                    overview_builder.append(phenotype).append(": 0\n");
                    continue;
                }
                int[] genomes = phenotype_map.get(phenotype);
                int threshold = phenotype_threshold_map.get(phenotype);
                ArrayList<Node> node_list = pheno_kmer_map.get(phenotype + phenotype_class);
                StringBuilder node_builder = new StringBuilder();
                long total_kmers = 0;
                for (Node nuc_node : node_list) {
                    int length = (int) nuc_node.getProperty("length");
                    total_kmers += length - K_SIZE + 1;
                    long node_id = nuc_node.getId();
                    node_builder.append(node_id).append(",");
                    if (total_kmers % 1001 == 0 || total_kmers == node_list.size()) {
                        System.out.print("\rCreating phenotype output: " + total_kmers);
                    }
                }
                String total_kmers_str = "";
                if (!compressed_kmers) {
                    total_kmers_str = " & " + total_kmers + " distinct k-mers";
                }
                out.write("#Phenotype " + phenotype + ", " + genomes.length + " genomes, threshold of " + threshold + " genomes: "
                        + node_list.size() + " nodes" + total_kmers_str + "\n" + node_builder.toString() + "\n");
                overview_builder.append(phenotype).append(": ").append(node_list.size()).append(", ").append(total_kmers).append("\n");
                out.write("\n");
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}", WORKING_DIRECTORY + filename);
            System.exit(1);
        }
    }

    /**
     * Check if a node/kmer meets the threshold and is not shared by other phenotypes.
     * If both yes, phenotype specific
     * If only threshold is met, phenotype shared
     *
     * @param nuc_node
     * @param pheno_count_map
     * @param pheno_kmer_map
     */
    private void determine_kmer_pheno_shared_specific(Node nuc_node, HashMap<String, Integer> pheno_count_map,
                                                            ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map) {
        if (PHENOTYPE == null) {
            return;
        }
        for (String phenotype : pheno_count_map.keySet()) {
            if (phenotype.equals("?") || phenotype.equals("Unknown")) {
                continue;
            }
            int threshold = phenotype_threshold_map.get(phenotype);
            int value = pheno_count_map.get(phenotype);
            boolean pass_threshold = value >= threshold; // if value is higher or equal to threshold
            boolean no_other_pheno = false;
            for (String other_phenotype : pheno_count_map.keySet()) {
                if (phenotype.equals(other_phenotype) || other_phenotype.equals("?") || other_phenotype.equals("Unknown")) {
                    continue;
                }
                no_other_pheno = true;// disrupted by presence of other phenotype
            }
            if (pass_threshold) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#shared", nuc_node); // k-mer is phenotype shared
            }
            if (no_other_pheno && pass_threshold) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#specific", nuc_node); // k-mer is phenotype specific
            }
            if (no_other_pheno) {
                try_incr_AL_chashmap(pheno_kmer_map, phenotype + "#exclusive", nuc_node); // k-mer is phenotype exclusive
            }
        }
    }

    /**
     * Loop over all nucleotide nodes of the pangenome
     * @param kmer_occur_map is null except when --mode OCCURENCE is included
     * @param kmer_freq_map
     * @param pheno_kmer_map
     * @param freq_per_genome is null except when --mode OCCURENCE is included
     * @param kmer_class_count
     *
     * @return
     */
    private long[] gather_kmer_information(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
                                          ConcurrentHashMap<Integer, long[]> kmer_freq_map, ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map,
                                          ConcurrentHashMap<Integer, HashMap<Long,Long>> freq_per_genome,
                                          long[] kmer_class_count) {

        distinct_node_count = new AtomicLong(0);
        total_node_count = new AtomicLong(0);
        nodeCounter = new AtomicLong(0);
        fileCounter = new AtomicLong(0);
        frequency_queue = new LinkedBlockingQueue<>();
        string_queue = new LinkedBlockingQueue<>();
        node_queue = new LinkedBlockingQueue<>();

        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);
            es.execute(new find_shared_kmers(kmer_occur_map, kmer_freq_map, pheno_kmer_map, freq_per_genome, kmer_class_count));
            if (compare_sequences) {
                hashmapQueue = new LinkedBlockingQueue<>();
                es.execute(new add_freq_per_seq(1));
                for (int i = 1; i <= THREADS-2; i++) {
                    es.execute(new count_kmers_per_sequence_class(i));
                }
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {

        }
        long[] node_count_array = new long[2];
        node_count_array[0] = distinct_node_count.get(); // distinct
        node_count_array[1] = total_node_count.get(); // total
        return node_count_array;
    }

    /**
     * Is only executed when --sequence argument is included
     */
    private class count_kmers_per_sequence_class implements Runnable {
        int thread_nr;
        int[][] distinct_shared_array = new int[selectedSequences.size()][selectedSequences.size()];
        int[][] distinct_total_array = new int[selectedSequences.size()][selectedSequences.size()];
        int[][] all_shared_array = new int[selectedSequences.size()][selectedSequences.size()];
        int[][] all_total_array = new int[selectedSequences.size()][selectedSequences.size()];
        int[] freq_array = new int[selectedSequences.size()];

        int[][] subgenomeDistinctTotal = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
        int[][] subgenomeDistinctShared = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
        int[][] subgenomeEVERYTotal = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];
        int[][] subgenomeEVERYShared = new int[selectedSubgenomes.size()][selectedSubgenomes.size()];

        private count_kmers_per_sequence_class(int thread_nr) {
            this.thread_nr = thread_nr;
        }

        public void run() {
            long nr_of_kmers = determine_appropriate_nr_for_printing();
            HashMap<String, Integer> sequenceIndexInArray = createSequenceIndexInArrayMap();
            HashMap<String, Integer> subgenomeIndexInArray = createSubgenomeIndexInArrayMap();
            ArrayList<String> selectedSequencesList = new ArrayList<>(selectedSequences);
            ArrayList<String> selectedSubgenomeList = new ArrayList<>(selectedSubgenomes);

            if (!compare_sequences) {
                return;
            }
            HashMap<Node, Integer> hashmap = new HashMap<>();
            try {
                hashmap = hashmapQueue.take();
            } catch (InterruptedException e) {
                // do nothing (this never happens)
            }
            long num_nodes = 0, one_procent = 0, local_counter = 0;
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            try {
                num_nodes = (long) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_nodes");
                one_procent = (num_nodes + 100) / 100;
                Map.Entry<Node, Integer> entry = hashmap.entrySet().stream().findFirst().get();
                Node nucleotideNode = entry.getKey();
                nodeCounter.getAndIncrement();
                int actual_kmers = entry.getValue();
                long last_printed = 0;
                while (actual_kmers != -1) {
                    if (thread_nr == 1 && (nodeCounter.get() > last_printed || nodeCounter.get() < 1000)) {
                        double nodes_d = (double) nodeCounter.get();
                        percentage_counter = String.format("%.2f", nodes_d / one_procent);
                        System.out.print("\rGathering k-mer nodes: " + nodeCounter.get() + "/" + num_nodes + " (" + percentage_counter + "%) ");
                        last_printed += nr_of_kmers;
                    }

                    HashMap<String, Integer> kmersPerSequence = getKmersPerSequence(nucleotideNode);
                    increaseSharedKmersPerSequence(kmersPerSequence, actual_kmers, sequenceIndexInArray,
                            distinct_shared_array, distinct_total_array, freq_array, all_shared_array, all_total_array, selectedSequencesList);
                    if (!phasingInfoMap.isEmpty()) {
                        HashMap<String, Integer> kmersPerSubgenome = countKmersPerSubgenome(kmersPerSequence);
                        increaseSharedKmersPerSequence(kmersPerSubgenome, actual_kmers, subgenomeIndexInArray,
                                subgenomeDistinctShared, subgenomeDistinctTotal, null, subgenomeEVERYShared, subgenomeEVERYTotal, selectedSubgenomeList);
                    }
                    local_counter++;
                    try {
                        hashmap = hashmapQueue.take();
                    } catch (InterruptedException e) {
                        // do nothing (this never happens)
                    }

                    entry = hashmap.entrySet().stream().findFirst().get();
                    nucleotideNode = entry.getKey(); // retrieve new node and number of k-mers for next loop
                    nodeCounter.getAndIncrement();
                    actual_kmers = entry.getValue();
                    if (local_counter % 1000 == 0) {
                        local_counter = 0;
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx(); // start a new database transaction
                    }
                }
                tx.success();
            } finally {
                tx.close();
            }

            int[] empty = new int[]{-1};
            frequency_queue.add(empty); // -1 in the array will make the "add_freq_per_seq" function stop
            if (thread_nr == 1) {
                Pantools.logger.trace("ending of 1 {} ", frequency_queue.size());
                double nodes_d = (double) nodeCounter.get();
                percentage_counter = String.format("%.2f", nodes_d / one_procent);
                System.out.println("\rGathering k-mer nodes: " + num_nodes + "/" + num_nodes + " (100%)    ");
                while (frequency_queue.size() > 1000) {
                    System.out.print("\rAdding frequencies to the k-mer nodes: " + frequency_queue.size() + "                   ");
                    try {
                        Thread.sleep(1000); // 1 second
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
            fill_shared_kmer_array_sequence(distinct_shared_array, distinct_total_array, all_shared_array, all_total_array, thread_nr);
            fill_shared_kmer_array_subgenome(subgenomeDistinctShared, subgenomeDistinctTotal, subgenomeEVERYShared, subgenomeEVERYTotal);
        }

        /**
         *
         */
        private long determine_appropriate_nr_for_printing() {
            long nr_for_printing = 10000;
            if (selectedSequences.size() >= 1000) {
                nr_for_printing = 1000;
            }
            if (selectedSequences.size() >= 10000) {
                nr_for_printing = 100;
            }
            return nr_for_printing;
        }

        /**
         * @param kmersPerSequence
         * @param actual_kmers
         * @param sequenceIndexInArray
         * @param distinct_shared_array
         * @param distinct_total_array
         * @param freq_array
         * @param all_shared_array
         * @param all_total_array
         */
        private void increaseSharedKmersPerSequence(HashMap<String, Integer> kmersPerSequence,
                                                    int actual_kmers, HashMap<String, Integer> sequenceIndexInArray, int[][] distinct_shared_array, int[][] distinct_total_array,
                                                    int[] freq_array, int[][] all_shared_array, int[][] all_total_array, ArrayList<String> selectedSequencesList) {

            HashSet<String> addedSequences = new HashSet<>(); // prevents increasing multiple times
            for (String sequenceIdA : kmersPerSequence.keySet()) {
                int originalIndexA = sequenceIndexInArray.get(sequenceIdA);
                int frequencyA = kmersPerSequence.get(sequenceIdA);
                for (String sequenceIdB : selectedSequencesList) {
                    if (addedSequences.contains(sequenceIdB)) {
                        continue;
                    }
                    int frequencyB = 0;
                    if (kmersPerSequence.containsKey(sequenceIdB)) {
                        frequencyB = kmersPerSequence.get(sequenceIdB);
                    }
                    int[] lowestHighest = orderTwoIntegers(frequencyA, frequencyB);
                    int indexA = originalIndexA;
                    int indexB = sequenceIndexInArray.get(sequenceIdB);
                    if (indexB < originalIndexA) {
                        int temp = indexA;
                        indexA = indexB;
                        indexB = temp;
                    }

                    if (compressed_kmers) {
                        if (lowestHighest[0] > 0) { // lowest value must be at least 1
                            distinct_shared_array[indexA][indexB]++;
                            all_shared_array[indexA][indexB] += lowestHighest[0];
                        }
                        distinct_total_array[indexA][indexB]++;
                        all_total_array[indexA][indexB] += lowestHighest[1];
                    } else {
                        if (lowestHighest[0] > 0) { // lowest value must be at least 1
                            distinct_shared_array[indexA][indexB] += actual_kmers;
                            all_shared_array[indexA][indexB] += (lowestHighest[0] * actual_kmers);
                        }
                        distinct_total_array[indexA][indexB] += actual_kmers;
                        all_total_array[indexA][indexB] += (lowestHighest[1] * actual_kmers);
                    }
                }
                addedSequences.add(sequenceIdA);
            }
        }

        private HashMap<String, Integer> countKmersPerSubgenome(HashMap<String, Integer> kmersPerSequence) {
            HashMap<String, Integer> kmersPerSubgenome = new HashMap<>();
            for (String sequenceId : kmersPerSequence.keySet()) {
                String subgenome = getSubgenomeIdentifier(sequenceId);
                kmersPerSubgenome.merge(subgenome, kmersPerSequence.get(sequenceId), Integer::sum);
            }
            return kmersPerSubgenome;
        }

        /**
         * synchronized
         *
         * @param distinct_shared_array
         * @param distinct_total_array
         * @param all_shared_array
         * @param all_total_array
         * @param thread
         */
        private synchronized void fill_shared_kmer_array_sequence(int[][] distinct_shared_array, int[][] distinct_total_array,
                                                                  int[][] all_shared_array, int[][] all_total_array, int thread) {

            for (int i = 0; i < distinct_shared_array.length; i++) {
                for (int j = 0; j < distinct_shared_array.length; j++) {
                    seq_distinct_shared_kmers[i][j] += distinct_shared_array[i][j];
                    seq_distinct_total_kmers[i][j] += distinct_total_array[i][j];
                }
            }

            for (int i = 0; i < all_shared_array.length; i++) {
                for (int j = 0; j < all_shared_array.length; j++) {
                    seq_all_total_kmers[i][j] += all_total_array[i][j];
                    seq_all_shared_kmers[i][j] += all_shared_array[i][j];
                }
            }
        }

        /**
         * synchronized
         *
         * @param distinct_shared_array
         * @param distinct_total_array
         * @param all_shared_array
         * @param all_total_array
         */
        private synchronized void fill_shared_kmer_array_subgenome(int[][] distinct_shared_array, int[][] distinct_total_array,
                                                                  int[][] all_shared_array, int[][] all_total_array) {

            for (int i = 0; i < distinct_shared_array.length; i++) {
                for (int j = 0; j < distinct_shared_array.length; j++) {
                    subgenome_distinct_shared_kmers[i][j] += distinct_shared_array[i][j];
                    subgenome_distinct_total_kmers[i][j] += distinct_total_array[i][j];
                }
            }

            for (int i = 0; i < all_shared_array.length; i++) {
                for (int j = 0; j < all_shared_array.length; j++) {
                    subgenome_all_total_kmers[i][j] += all_total_array[i][j];
                    subgenome_all_shared_kmers[i][j] += all_shared_array[i][j];
                }
            }
        }


        private HashMap<String, Integer> createSequenceIndexInArrayMap() {
            HashMap<String, Integer> sequenceIndexInArray = new HashMap<>();
            int counter = 0;
            for (String sequenceId : selectedSequences) {
                sequenceIndexInArray.put(sequenceId, counter);
                counter++;
            }
            return sequenceIndexInArray;
        }

        private HashMap<String, Integer> createSubgenomeIndexInArrayMap() {
            HashMap<String, Integer> subgenomeIndexInArray = new HashMap<>();
            int counter = 0;
            for (String sequenceId : selectedSubgenomes) {
                subgenomeIndexInArray.put(sequenceId, counter);
                counter++;
            }
            return subgenomeIndexInArray;
        }
    }

    /**
     *
     * @return key is a string: "genome_" + genome number or sequence id
     */
    private HashMap<String, long[]> gatherDegenerateNodes(boolean compareSequences) {
        frequency_queue = new LinkedBlockingQueue<>();
        string_queue = new LinkedBlockingQueue<>();
        node_queue = new LinkedBlockingQueue<>();
        HashMap<String, long[]> degen_node_count = new HashMap<>();

        try {
            ExecutorService es = Executors.newFixedThreadPool(THREADS);
            if (compareSequences) {
                es.execute(new find_degen_nodes_sequence(degen_node_count));
                es.execute(new add_freq_per_seq(1));
            } else { // genome level
                es.execute(new find_degen_nodes_genome(degen_node_count));
            }
            es.shutdown();
            es.awaitTermination(10, TimeUnit.DAYS);
        } catch (InterruptedException e) {

        }
        return degen_node_count;
    }

    /**
     *
     * @param sequenceId
     * @return genome number with phasing letter
     */
    private String getSubgenomeIdentifier(String sequenceId) {
        String[] phasingInfo = phasingInfoMap.get(sequenceId); // example [1, D, 1D, 1_D]
        String[] sequenceIdArray = sequenceId.split("_");
        String genomePhase = sequenceIdArray[0] + "_unphased";
        if (phasingInfo != null) {
            genomePhase = sequenceIdArray[0] + "_" + phasingInfo[1];
        }
        return genomePhase;
    }

    private int[] orderTwoIntegers(int numberA, int numberB) {
        if (numberA > numberB) {
            return new int[]{numberB, numberA};
        }
        return new int[]{numberA, numberB};
    }

    /**
     *
     */
    private class find_shared_kmers implements Runnable {
        ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map; // is null except when --mode OCCURRENCE is included
        ConcurrentHashMap<Integer, long[]> kmer_freq_map;
        ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map;
        ConcurrentHashMap<Integer, HashMap<Long,Long>> freq_per_genome; // is null except when --mode OCCURRENCE is included
        long[] kmer_class_count;
        int[][] distinct_shared_array = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        int[][] distinct_total_array = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        int[][] all_shared_array = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];
        int[][] all_total_array = new int[GENOME_DB.num_genomes][GENOME_DB.num_genomes];

        private find_shared_kmers(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map, ConcurrentHashMap<Integer, long[]> kmer_freq_map,
                                  ConcurrentHashMap<String, ArrayList<Node>> pheno_kmer_map, ConcurrentHashMap<Integer,
                                    HashMap<Long,Long>> freq_per_genome, long[] kmer_class_count) {
            this.kmer_occur_map = kmer_occur_map;
            this.kmer_freq_map = kmer_freq_map;
            this.pheno_kmer_map = pheno_kmer_map;
            this.freq_per_genome = freq_per_genome;
            this.kmer_class_count = kmer_class_count;
        }

        public void run() {
            ArrayList<Integer> selectedGenomesList = new ArrayList<>(selectedGenomes);
            HashMap<Integer, ArrayList<String>> kmer_genome_presence = new HashMap<>();
            HashMap<Integer, ArrayList<String>> unique_kmers = new HashMap<>();
            long node_counter = 0;
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            int kmer_print_nr = get_number_of_kmers_for_printing();
            try {
                long num_nodes = (long) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_nodes");
                long one_procent = (num_nodes+100)/100;
                ResourceIterator<Node> nuc_nodes = GRAPH_DB.findNodes(NUCLEOTIDE_LABEL);
                while (nuc_nodes.hasNext()) {
                    node_counter ++;
                    if (!compare_sequences && (node_counter % kmer_print_nr == 0 || node_counter == num_nodes || node_counter < 1001)) {
                        double nodes_d = (double) node_counter;
                        percentage_counter = String.format("%.2f", nodes_d/one_procent);
                        System.out.print("\rGathering k-mer nodes: " + node_counter + "/" + num_nodes + " (" + percentage_counter + "%)    ");
                    }

                    if (node_counter % 1000000 == 0 && node_counter > 1) { // 1 million
                        write_core_kmers_to_file(kmer_genome_presence);
                        write_accessory_kmers_to_file(kmer_genome_presence);
                        write_unique_kmers_to_file(unique_kmers);
                    }

                    // when calculating distance between k-mers. prevent the queue for counting between sequences from becoming to large
                    while (compare_sequences && hashmapQueue.size() > 50000) {
                        try {
                            Thread.sleep(100); // 100 milliseconds
                        } catch (InterruptedException ex) {
                            Thread.currentThread().interrupt();
                        }
                    }

                    Node nuc_node = nuc_nodes.next();
                    HashMap<String, Integer> pheno_count_map = new HashMap<>();
                    if (nuc_node.hasLabel(DEGENERATE_LABEL)) {
                        if (compare_sequences) {
                            // no degenerate nodes are given to the other function, count them here already
                            nodeCounter.getAndIncrement();
                        }
                        continue;
                    }

                    //int[] freq_array = get_kmer_frequencies_property(nuc_node);
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    int kmer_frequency = get_kmer_frequency(nuc_node);
                    int kmer_size = (int) nuc_node.getProperty("length");
                    int actual_kmers = kmer_size - K_SIZE + 1;
                    Pantools.logger.trace("{} {} {}", nuc_node, kmer_size, actual_kmers + Arrays.toString(freq_array));

                    increase_kmer_occur_map(kmer_occur_map, kmer_frequency, nuc_node, 1);

                    boolean core = true, accessory = false, unique = false;
                    int counter = 0;
                    for (int genomeNr : selectedGenomes) {
                        long frequency = freq_array[genomeNr];
                        if (compressed_kmers) {
                            total_node_count.getAndIncrement(); // node_count2 is total
                        } else {
                            total_node_count.addAndGet((frequency * actual_kmers));
                        }

                        if (frequency == 0) {
                            core = false;
                            continue;
                        }
                        increase_freq_per_genome_map(freq_per_genome, genomeNr, frequency);
                        counter ++; //relevant for distinct kmers and thresholds
                        if (PHENOTYPE != null) {
                            String pheno = geno_pheno_map.get(genomeNr);
                            pheno_count_map.merge(pheno, 1, Integer::sum);
                        }
                    }
                    // add one to distinct kmers if they exist for the current subset
                    if (counter > 0) {
                        if (compressed_kmers) {
                            distinct_node_count.getAndIncrement(); // node_count1 is distinct
                        } else {
                            distinct_node_count.addAndGet(actual_kmers);
                        }
                    }
                    if (compare_sequences) {
                        HashMap<Node, Integer> map = new HashMap<>();
                        map.put(nuc_node, actual_kmers);
                        hashmapQueue.add(map);
                        //continue;
                    }
                    kmer_genome_presence.computeIfAbsent(counter, k -> new ArrayList<>())
                            .add(String.valueOf(nuc_node.getId()));
                    if (counter >= core_threshold) { // core
                        if (compressed_kmers) {
                            kmer_class_count[0] ++; // kmer_count is distinct
                        } else {
                            kmer_class_count[0] += actual_kmers;
                        }
                    } else if (counter <= unique_threshold) { // unique
                        unique = true;
                        if (compressed_kmers) {
                            kmer_class_count[2] ++;
                        } else {
                            kmer_class_count[2] += actual_kmers;
                        }
                    } else { // accessory
                        if (compressed_kmers) {
                            kmer_class_count[1] ++;
                        } else {
                            kmer_class_count[1] += actual_kmers;
                        }
                        accessory = true;
                    }
                    increaseSharedKmersPerGenome(actual_kmers, distinct_shared_array, distinct_total_array, freq_array, all_shared_array, all_total_array, selectedGenomesList);
                    determine_kmer_pheno_shared_specific(nuc_node, pheno_count_map, pheno_kmer_map);
                    for (int genomeNr : selectedGenomes) {
                        // hashmap replaced by twodimensional array
                        //increase_shared_kmers_map(freq_array, i, shared_kmers_map, actual_kmers, unique); // synchronized function
                        if (freq_array[genomeNr] == 0) {
                            continue;
                        }
                        increase_kmer_freq_map(kmer_freq_map, genomeNr, core, accessory, freq_array, nuc_node, actual_kmers);
                        if (unique) {
                            unique_kmers.computeIfAbsent(genomeNr, k -> new ArrayList<>())
                                    .add(String.valueOf(nuc_node.getId()));
                        }
                    }
                }

                if (!compare_sequences) {
                    double nodes_d = (double) node_counter;
                    percentage_counter = String.format("%.2f", nodes_d/one_procent);
                    System.out.print("\rGathering k-mer nodes: " + node_counter + "/" + num_nodes + " (" + percentage_counter + "%)");
                } else {
                    for(int i = 1; i <= THREADS-2; i++) {
                        HashMap<Node, Integer> map = new HashMap<>();
                        Node nuc_node = GRAPH_DB.getNodeById(0);
                        map.put(nuc_node, -1);
                        hashmapQueue.add(map);
                    }
                }
                tx.success();
            } finally {
                tx.close();
            }
            write_core_kmers_to_file(kmer_genome_presence);
            write_accessory_kmers_to_file(kmer_genome_presence);
            write_unique_kmers_to_file(unique_kmers);
            fill_shared_kmer_array_genome(distinct_shared_array, distinct_total_array, all_shared_array, all_total_array);
        }


        /**
         * Synchronized
         *
         * @param distinct_shared_array
         * @param distinct_total_array
         * @param all_shared_array
         * @param all_total_array
         */
        private synchronized void fill_shared_kmer_array_genome(int[][] distinct_shared_array, int[][] distinct_total_array,
                                                                int[][] all_shared_array, int[][] all_total_array) {

            for (int i = 0; i < distinct_shared_array.length; i++) {
                for (int j = 0; j < distinct_shared_array.length; j++) {
                    genome_distinct_shared_kmers[i][j] += distinct_shared_array[i][j];
                    genome_distinct_total_kmers[i][j] += distinct_total_array[i][j];
                }
            }

            for (int i = 0; i < all_shared_array.length; i++) {
                for (int j = 0; j < all_shared_array.length; j++) {
                    genome_all_total_kmers[i][j] += all_total_array[i][j];
                    genome_all_shared_kmers[i][j] += all_shared_array[i][j];
                }
            }
        }

        /**

         */
        private int get_number_of_kmers_for_printing() {
            int nr_kmers = 10000;
            if (GENOME_DB.num_genomes > 100 ) {
                nr_kmers = 5000;
            }
            if (GENOME_DB.num_genomes > 1000 ) {
                nr_kmers = 1000;
            }
            if (GENOME_DB.num_genomes >= 10000 ) {
                nr_kmers = 100;
            }
            return nr_kmers;
        }

        private void write_unique_kmers_to_file(HashMap<Integer, ArrayList<String>> unique_kmers) {
            for (int i=1; i <= GENOME_DB.num_genomes; i++) {
                if (!unique_kmers.containsKey(i)) {
                    continue;
                }
                ArrayList<String> nodes = unique_kmers.get(i);
                String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
                boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
                if (!exists) {
                    write_string_to_file_in_DB("#Genome " + i + "\n" + node_str, "kmer_classification/temp_unique_kmers_" + i + ".csv");
                } else {
                    appendStringToFileFullPath(node_str, WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
                }
                unique_kmers.remove(i);
            }
        }

        /**
         *
         * @param kmer_genome_presence
         */
        private void write_accessory_kmers_to_file(HashMap<Integer, ArrayList<String>> kmer_genome_presence) {
            for (int i= selectedGenomes.size(); i > 1; i--) {
                if (!kmer_genome_presence.containsKey(i)) {
                    continue;
                }
                ArrayList<String> nodes = kmer_genome_presence.get(i);
                kmer_genome_presence.remove(i);
                String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
                boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
                if (!exists) {
                    String percentage = percentageAsString(i, selectedGenomes.size(), 1);
                    write_string_to_file_in_DB("#" + i + " genomes " + percentage + "%\n" + node_str ,
                            "kmer_classification/temp/accessory_kmers_" + i + ".csv");
                } else {
                    appendStringToFileFullPath(node_str,
                            WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
                }
            }
        }

        private void write_core_kmers_to_file(HashMap<Integer, ArrayList<String>> kmer_genome_presence) {
            long file_nr = fileCounter.getAndIncrement();
            ArrayList<String> nodes = kmer_genome_presence.get(core_threshold);
            if (nodes == null) {
                return;
            }
            kmer_genome_presence.remove(core_threshold);
            String node_str = nodes.toString().replace("[", "").replace("]", "").replace(" ", "");
            write_string_to_file_in_DB(node_str, "kmer_classification/temp_core_kmers_" + file_nr + ".csv");
        }
    }

    /**
     *
     * @param actual_kmers
     * @param distinct_shared_array
     * @param distinct_total_array
     * @param freq_array
     * @param all_shared_array
     * @param all_total_array
     */
    private void increaseSharedKmersPerGenome(int actual_kmers, int[][] distinct_shared_array, int[][] distinct_total_array,
                                              long[] freq_array, int[][] all_shared_array, int[][] all_total_array, ArrayList<Integer> selectedGenomesList) {


        for (int i = 0; i < selectedGenomesList.size(); i++) {
            int genomeNrA = selectedGenomesList.get(i);
            for (int j = i; j < selectedGenomesList.size(); j++) {
                int genomeNrB = selectedGenomesList.get(j);
                int highest = (int) freq_array[genomeNrA];
                int lowest = (int) freq_array[genomeNrB];
                if (lowest > highest) {
                    highest = (int) freq_array[genomeNrB];
                    lowest = (int) freq_array[genomeNrA];
                }
                if (highest == 0) { // neither genome have the k-mer
                    continue;
                }
                if (compressed_kmers) {
                    if (lowest > 0) {
                        distinct_shared_array[genomeNrA - 1][genomeNrB - 1]++;
                    }
                    distinct_total_array[genomeNrA-1][genomeNrB-1] ++;
                    all_shared_array[genomeNrA-1][genomeNrB-1] += lowest;
                    all_total_array[genomeNrA-1][genomeNrB-1] += highest;
                } else {
                    if (lowest > 0) {
                        distinct_shared_array[genomeNrA-1][genomeNrB-1] += actual_kmers;
                    }
                    distinct_total_array[genomeNrA-1][genomeNrB-1] += actual_kmers;
                    all_shared_array[genomeNrA-1][genomeNrB-1] += (lowest * actual_kmers);
                    all_total_array[genomeNrA-1][genomeNrB-1] += (highest * actual_kmers);
                }
            }
        }
    }

    private synchronized void increase_kmer_freq_map(ConcurrentHashMap<Integer, long[]> kmer_freq_map, int i, boolean core, boolean accessory,
                                                    long[] freq_array, Node nuc_node, int actual_kmers) {

        long[] current_freq = kmer_freq_map.get(i); // total, core, access, unique, distinct total, distinct core, distinct access, distinct uni
        int pos1 = 3, pos2 = 7; // unique positions in array
        if (core) {
            pos1 = 1;
            pos2 = 5;
        } else if (accessory) {
            pos1 = 2;
            pos2 = 6;
        }
        if (compressed_kmers) {
            current_freq[0] += freq_array[i];
            current_freq[4] += 1;
            current_freq[pos1] += freq_array[i];
            current_freq[pos2] += 1;
        } else {
            current_freq[0] += freq_array[i] * actual_kmers;
            current_freq[4] += actual_kmers;
            current_freq[pos1] += (freq_array[i] * actual_kmers);
            current_freq[pos2] += actual_kmers;
        }
        kmer_freq_map.put(i, current_freq);
    }

    /**
     * Synchronized
     * @param freq_per_genome
     * @param genome_nr
     * @param frequency
     */
    private synchronized void increase_freq_per_genome_map(ConcurrentHashMap<Integer, HashMap<Long, Long>> freq_per_genome, int genome_nr, long frequency) {
        if (freq_per_genome == null) {
            return;
        }
        HashMap<Long,Long> freq_map = freq_per_genome.get(genome_nr);
        freq_map.merge(frequency, 1L, Long::sum);
        freq_per_genome.put(genome_nr, freq_map);
    }

    /**
     *
     * @param kmer_occur_map
     * @param kmer_frequency
     * @param nuc_node
     * @param thread_nr
     */
    private synchronized void increase_kmer_occur_map(ConcurrentHashMap<Long, ArrayList<Node>> kmer_occur_map,
                                                     long kmer_frequency, Node nuc_node, int thread_nr) {
        if (kmer_occur_map == null) {
            return;
        }
        try_incr_AL_chashmap(kmer_occur_map, kmer_frequency, nuc_node);
        if (thread_nr == 1) {
            if (kmer_occur_map.size() > 250000) { // prevents the hasmap from becoming too large
                write_kmer_occurrence_output(kmer_occur_map);
            }
        }
    }

    /**
     *
     * @return kmersPerSequence key is sequence Id, value is the k-mer frequency
     */
    private HashMap<String, Integer> getKmersPerSequence(Node nucleotideNode) {
        HashMap<String, Integer> kmersPerSequence = new HashMap<>();
        LinkedHashSet<Integer> genomesMissingFrequencies = new LinkedHashSet<>();

        // check if frequencies were calculated in an earlier run
        for (int genomeNr : selectedGenomes) {
            if (nucleotideNode.hasProperty("frequency_per_sequence_genome_" + genomeNr)) {
                int[] frequencyPerSequence = (int[]) nucleotideNode.getProperty("frequency_per_sequence_genome_" + genomeNr);
                ArrayList<Integer> selectedSequencesOfGenome = sequences_per_genome.get(genomeNr + "_selected");
                for (int sequenceNr : selectedSequencesOfGenome) {
                    if (frequencyPerSequence[sequenceNr-1] == 0 || !selectedSequences.contains(genomeNr + "_" + sequenceNr)) {
                        continue;
                    }
                    kmersPerSequence.merge(genomeNr + "_" + sequenceNr, frequencyPerSequence[sequenceNr-1], Integer::sum);
                }
            } else {
                genomesMissingFrequencies.add(genomeNr);
            }
        }

        if (genomesMissingFrequencies.isEmpty()) { // the frequencies were already calculated and stored
            return kmersPerSequence;
        }

        // frequencies are not known yet. need to be calculated and possibly stored
        int[][] frequencyPerSequence = null;
        if (frequency_queue.size() < 50000) { // prepare output to be written to nodes only if there is not a large queue already
            frequencyPerSequence = new int[GENOME_DB.num_genomes][0];
            for (int genomeNr : genomesMissingFrequencies) {
                ArrayList<Integer> sequences = sequences_per_genome.get(genomeNr +""); // retrieve ALL sequences of the genome to initialize array
                frequencyPerSequence[genomeNr-1] = new int[sequences.size()];
            }
        }

        // calculate the k-mer frequency per sequence
        // genomes can be skipped. sequences of a genome may not be skipped!
        Iterable<Relationship> incomingRelations = nucleotideNode.getRelationships(Direction.INCOMING, RelTypes.FF, RelTypes.FR, RelTypes.RR, RelTypes.RF);
        for (Relationship relation : incomingRelations) {
            Iterable<String> sequenceIdPropertyKeys = relation.getPropertyKeys(); // genome 1 sequence 1 is G1S1
            for (String sequenceIdKey : sequenceIdPropertyKeys) { // do not skip anything here so the array is always complete
                int[] genomicCoordinates = (int[]) relation.getProperty(sequenceIdKey);
                String sequenceId = sequenceIdKey.replace("G","").replace("S", "_"); // transform G1S1 to 1_1
                String[] sequenceIdArray = sequenceId.split("_");
                int genomeNr = Integer.parseInt(sequenceIdArray[0]);
                if (!genomesMissingFrequencies.contains(genomeNr)) { // frequencies were already added to this genome
                    continue;
                }
                int sequenceNr = Integer.parseInt(sequenceIdArray[1]);
                if (frequencyPerSequence != null) {
                    frequencyPerSequence[genomeNr - 1][sequenceNr - 1] += genomicCoordinates.length;
                }
                if (selectedSequences.contains(sequenceId)) {
                    kmersPerSequence.merge(sequenceId, genomicCoordinates.length, Integer::sum);
                }
            }
        }
        addValuesToQueue(nucleotideNode, frequencyPerSequence, genomesMissingFrequencies); //synchronized function
        return kmersPerSequence;
    }

    /**
     * synchronized function
     *
     * @param nucleotideNode
     * @param frequencyPerSequence is null when the queue is already very large
     */
    private synchronized void addValuesToQueue(Node nucleotideNode, int[][] frequencyPerSequence, LinkedHashSet<Integer> genomesMissingFrequencies) {
        if (frequencyPerSequence == null) {
            return;
        }
        for (int genomeNr : genomesMissingFrequencies) {
            frequency_queue.add(frequencyPerSequence[genomeNr-1]);
            string_queue.add("frequency_per_sequence_genome_" + genomeNr);
            node_queue.add(nucleotideNode);
        }
    }

    private void combine_core_kmer_output_files() {
        long total_files = fileCounter.get();
        StringBuilder script = new StringBuilder();
        appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp_core_kmers_" + total_files+ ".csv");
        for (int i=0; i <= total_files; i++) {
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp_core_kmers_").append(i).append(".csv >> ")
                    .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/core_kmers.csv\n");
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);
        delete_file_full_path("script.sh");
        for (int i=0; i <= total_files; i++) {
            delete_file_in_DB("kmer_classification/temp_core_kmers_" + i + ".csv");
        }
    }

    /**
     * Creates unique_kmers.csv, the node identifiers of unique k-mers ordered by genome.
     */
    private void combine_unique_kmer_output_files() {
        if (selectedGenomes.size() == 1) {
            write_string_to_file_in_DB("No unique k-mers are found with only one genome is selected", "kmer_classification/kmer_identifiers/unique_kmers.csv");
            return;
        }
        StringBuilder script = new StringBuilder();
        for (int i=1; i <= GENOME_DB.num_genomes; i++) {
            boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
            if (!exists) {
                continue;
            }
            appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp_unique_kmers_" + i + ".csv");
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp_unique_kmers_").append(i).append(".csv >> ")
                    .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/unique_kmers.csv\n");
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);
        delete_file_full_path("script.sh");
        for (int i=1; i <= GENOME_DB.num_genomes; i++) {
            delete_file_in_DB("kmer_classification/temp_unique_kmers_" + i + ".csv");
        }
    }

    /**
     *  Creates accessory_kmers.csv,
     */
    private void combine_accessory_kmer_output_files() {
        if (selectedGenomes.size() == 1) {
            write_string_to_file_in_DB("No accessory k-mers are found with only one genome is selected", "kmer_classification/kmer_identifiers/accessory_kmers.csv");
            return;
        }
        StringBuilder script = new StringBuilder();
        for (int i= selectedGenomes.size(); i > 1; i--) {
            boolean exists = check_if_file_exists(WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
            if (!exists) {
                continue;
            }
            appendStringToFileFullPath("\n", WORKING_DIRECTORY + "kmer_classification/temp/accessory_kmers_" + i + ".csv");
            script.append("cat ").append(WORKING_DIRECTORY).append("kmer_classification/temp/accessory_kmers_").append(i).append(".csv >> ")
                    .append(WORKING_DIRECTORY).append("kmer_classification/kmer_identifiers/accessory_kmers.csv\n");
        }
        write_SB_to_file_full_path(script, "script.sh");
        String[] command = new String[]{"/bin/sh", "script.sh"};
        ExecCommand.ExecCommand(command);

        delete_file_full_path("script.sh");
        for (int i = selectedGenomes.size(); i > 1; i--) {
            delete_file_in_DB("kmer_classification/temp/accessory_kmers_" + i + ".csv");
        }
    }

    /**
     * Creates kmer_classification_overview.txt
     * @param kmer_freq_map
     * @param node_count
     * @param kmer_class_count
     * @param degen_node_per_seq_genome
     */
    private void create_kmer_classification_overview(ConcurrentHashMap<Integer, long[]> kmer_freq_map, long[] node_count,
                                                     long[] kmer_class_count, HashMap<String, long[]> degen_node_per_seq_genome) {

        StringBuilder output_builder = new StringBuilder();
        StringBuilder csv_builder = new StringBuilder("Genome,Total number of k-mers,Core k-mers,% of genome core k-mers,Accessory k-mers,"
                + "% of genome accessory k-mers,Unique k-mers,% of genome unique k-mers,Degenerate k-mers,% of genome degenerate k-mers,"
                + "Number of distinct k-mers,Distinct core k-mers,% of genome distinct core k-mers,Distinct accessory k-mers,% of genome distinct accessory k-mers,"
                + "Distinct unique k-mers,% of genome distinct unique k-mers,Distinct degenerate k-mers,% of genome distinct degenerate k-mers\n");

        long total_kmers = kmer_class_count[0] + kmer_class_count[1] + kmer_class_count[2];
        String core_pc = percentageAsString(kmer_class_count[0], total_kmers, 2);
        String accessory_pc = percentageAsString(kmer_class_count[1], total_kmers, 2);
        String unique_pc = percentageAsString(kmer_class_count[2], total_kmers, 2);
        long[] degen_counts = degen_node_per_seq_genome.get("nodes_in_pangenome"); // [nodes, distinct kmers, total kmers]
        if (degen_counts == null) {
            degen_counts = new long[3]; // is required because the degenerate node counting is currently skipped
        }

        if (!compressed_kmers) {
            output_builder.append("Uncompressed the k-mers. K-mer size is ").append(K_SIZE).append(", extracting ").append(K_SIZE-1)
                    .append(" of every compressed k-mer\n");
        } else {
            output_builder.append("Counted compressed k-mers. Each k-mer node is counted once\n");
        }
        output_builder.append("To obtain the distinct k-mer counts, the frequency of k-mers was ignored\n\n");
        output_builder.append("Total k-mers: ").append(node_count[1])
                .append("\nTotal degenerate k-mers: ").append(degen_counts[2])
                .append("\n\nDistinct k-mers: ").append(node_count[0])
                .append("\nDistinct degenerate k-mers: ").append(degen_counts[1])
                .append("\nCore: ").append(kmer_class_count[0]).append(" (").append(core_pc).append("%)")
                .append("\nAccessory: ").append(kmer_class_count[1]).append(" (").append(accessory_pc).append("%)")
                .append("\nUnique: ").append(kmer_class_count[2]).append(" (").append(unique_pc).append("%)").append("\n\n");

        for (int genomeNr : selectedGenomes) {
            long[] value_array = kmer_freq_map.get(genomeNr);
            String percentage1 = percentageAsString(value_array[1], value_array[0], 2);
            String percentage2 = percentageAsString(value_array[2], value_array[0], 2);
            String percentage3 = percentageAsString(value_array[3], value_array[0], 2);
            String percentage5 = percentageAsString(value_array[5], value_array[4], 2);
            String percentage6 = percentageAsString(value_array[6], value_array[4], 2);
            String percentage7 = percentageAsString(value_array[7], value_array[4], 2);

            long[] genome_degen_counts = new long [3]; // [nodes, distinct kmers, total kmers]
            if (degen_node_per_seq_genome.containsKey("genome_" + genomeNr)) {
                genome_degen_counts = degen_node_per_seq_genome.get("genome_" + genomeNr);
            }
            String percentage8 = percentageAsString(genome_degen_counts[1], value_array[0], 2);
            String percentage9 = percentageAsString(genome_degen_counts[2], value_array[4], 2);
            output_builder.append("Genome ").append(genomeNr)
                    .append("\nAll k-mers: ").append(value_array[0])
                    .append("\nDegenerate k-mers: ").append(genome_degen_counts[1]).append(", ").append(percentage8).append("% of all k-mers")
                    .append("\nCore: ").append(value_array[1]).append(", ").append(percentage1)
                    .append("%\nAccessory: ").append(value_array[2]).append(", ").append(percentage2)
                    .append("%\nUnique: ").append(value_array[3]).append(", ").append(percentage3).append("%\n\n")
                    .append("Distinct k-mers: ").append(value_array[4])
                    .append("\nDistinct degenerate k-mers: ").append(genome_degen_counts[2]).append(", ").append(percentage9).append("% of distinct k-mers")
                    .append("\nCore: ").append(value_array[5]).append(", ").append(percentage5)
                    .append("%\nAccessory: ").append(value_array[6]).append(", ").append(percentage6)
                    .append("%\nUnique: ").append(value_array[7]).append(", ").append(percentage7).append("%\n\n");

            csv_builder.append(genomeNr).append(",").append(value_array[0]).append(",").append(value_array[1]).append(",").append(percentage1)
                    .append(",").append(value_array[2]).append(",").append(percentage2).append(",").append(value_array[3]).append(",").append(percentage3).append(",")
                    .append(genome_degen_counts[1]).append(",").append(percentage8).append(",").append(value_array[4]).append(",").append(value_array[5]).append(",")
                    .append(percentage5).append(",").append(value_array[6]).append(",").append(percentage6).append(",").append(value_array[7]).append(",")
                    .append(percentage7).append(",").append(genome_degen_counts[2]).append(",").append(percentage9).append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "kmer_classification/kmer_classification_overview.txt");
        write_SB_to_file_in_DB(csv_builder, "kmer_classification/kmer_classification_overview.csv");
    }

    /**
     * k-mer distance NJ tree
     */
    private void createKmerDistanceRscript(String type) {
        String outputPath = WD_full_path + "kmer_classification/";
        String typeCapitalised = type.substring(0, 1).toUpperCase() + type.substring(1);
        String rLibrary = check_r_libraries_environment();
        String rscript = "#! /usr/bin/env RScript\n\n" +
                "# Use one of the following files\n" +
                "# " + WD_full_path + "kmer_classification/distances_for_tree/" + type + "_mash_distance.csv\n" +
                "# " + WD_full_path + "kmer_classification/distances_for_tree/" + type + "_distance_distinct_kmers.csv\n" +
                "# " + WD_full_path + "kmer_classification/distances_for_tree/" + type + "_distance_all_kmers.csv\n\n" +
                "\n" +
                "#Assuming the R libraries were installed via Conda, the next line can be ignored. If not, use it to manually install the required package\n" +
                "#install.packages(\"ape\", \"" + rLibrary + "\", \"https://cran.us.r-project.org\")\n" +
                "library(ape)\n" +
                "\n" +
                "input = read.csv(\"" + WD_full_path + "kmer_classification/distances_for_tree/" + type + "_mash_distance.csv\", sep=\",\", header = TRUE)\n" +
                "dataframe = subset(input, select = -c(" + typeCapitalised + "s))\n" +
                "df.distance = as.matrix(dataframe, labels=TRUE)\n" +
                "colnames(df.distance) <- rownames(df.distance) <- input[['" + typeCapitalised + "s']]\n" +
                "NJ_tree <- nj(df.distance)\n" +
                "write.tree(NJ_tree, tree.names = TRUE, file=\"" + outputPath + type + "_kmer_distance.tree\")\n" +
                "cat(\"\\nK-mer distance tree written to: " + outputPath + type + "_kmer_distance.tree\\n\\n\")";
        writeStringToFile(rscript, outputPath + type + "_kmer_distance_tree.R", false, false);
    }

    /**
     * When this is run with multiple threads at the same time, it sometimes happens that the 3 values belonging together end up in different threads
     */
    private class add_freq_per_seq implements Runnable {

        int thread_nr;
        private add_freq_per_seq(int thread_nr) {
            this.thread_nr = thread_nr;
        }

        public void run() {
            Transaction tx = GRAPH_DB.beginTx(); // start database transaction
            long transaction_counter = 0;
            try {
                try {
                    int[] frequencies = frequency_queue.take();
                    if (frequencies[0] == -1) {  // everything was already known
                        return;
                    }
                    String property_name = string_queue.take();
                    Node node = node_queue.take();
                    while (frequencies[0] != -1) {
                        transaction_counter ++;
                        node.removeProperty(property_name);
                        node.setProperty(property_name, frequencies);
                        if (transaction_counter % 5000 == 0 && transaction_counter > 1) {
                            tx.success();
                            tx.close();
                            tx = GRAPH_DB.beginTx(); // start a new database transaction
                        }
                        frequencies = frequency_queue.take();
                        if (frequencies[0] == -1) {
                            continue;
                        }
                        property_name = string_queue.take();
                        node = node_queue.take();
                    }
                    tx.success();
                } finally {
                    tx.close();
                }
            } catch(InterruptedException e) {

            }
        }
    }

    /**
     *
     */
    private class find_degen_nodes_sequence implements Runnable {
        HashMap<String, long[]> degen_node_count;

        private find_degen_nodes_sequence(HashMap<String, long[]> degen_node_count) {
            this.degen_node_count = degen_node_count;
        }

        public void run() {
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                long degen_nodes = count_nodes(DEGENERATE_LABEL);
                ResourceIterator<Node> all_kmers = GRAPH_DB.findNodes(DEGENERATE_LABEL);
                long node_counter = 0; // number of degen nodes
                long distinct_counter = 0; // number of k-mers in degen nodes
                long total_counter = 0; // total frequency of the degenerate k-mers in all (selected) genomes of the pangenome
                while (all_kmers.hasNext()) {
                    node_counter ++;
                    Node nuc_node = all_kmers.next();
                    int node_len = (int) nuc_node.getProperty("length");
                    HashMap<String, Integer> kmer_count_per_seq = getKmersPerSequence(nuc_node);
                    int actual_kmers = node_len - K_SIZE + 1;
                    if (node_counter % 1000 == 0 || node_counter == degen_nodes || node_counter < 101) {
                        System.out.print("\rGathering degenerate nodes " + node_counter + "/" + degen_nodes);
                    }
                    distinct_counter += actual_kmers;
                    for (String seq_id : kmer_count_per_seq.keySet()) {
                        // count k-mers for an entire genome, based on individual sequences
                        String[] seq_array = seq_id.split("_");
                        kmer_count_per_seq.merge("genome_" + seq_array[0], kmer_count_per_seq.get(seq_id), Integer::sum);
                        total_counter += kmer_count_per_seq.get(seq_id) * actual_kmers;
                    }

                    for (String seq_id : kmer_count_per_seq.keySet()) {
                        long[] values = new long[]{1, actual_kmers, actual_kmers * kmer_count_per_seq.get(seq_id)};
                        if (degen_node_count.containsKey(seq_id)) {
                            values = degen_node_count.get(seq_id);
                            values[0] += 1;
                            if (compressed_kmers) {
                                values[1] += 1;
                                values[2] += kmer_count_per_seq.get(seq_id);
                            } else {
                                values[1] += actual_kmers;
                                values[2] += actual_kmers * kmer_count_per_seq.get(seq_id);
                            }
                        }
                        degen_node_count.put(seq_id, values);
                    }
                }
                long[] degen_values = new long[]{node_counter, distinct_counter, total_counter};
                degen_node_count.put("nodes_in_pangenome", degen_values);
                tx.success(); // end of neo4j transaction
            }
            for (int i = 1; i <= THREADS-1; i++) {
                int [] empty = new int[] {-1};
                frequency_queue.add(empty); // -1 in the array will make the "add_freq_per_seq" function stop
            }

            while (frequency_queue.size() > 1000) {
                System.out.print("\rAdding frequencies to the k-mer nodes: " + frequency_queue.size() + "                   ");
                try {
                    Thread.sleep(1000); // 1 second
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            System.out.print("\r                                                        ");
        }
    }

    /**
     *
     */
    private class find_degen_nodes_genome implements Runnable {
        HashMap<String, long[]> degen_node_count;

        private find_degen_nodes_genome(HashMap<String, long[]> degen_node_count) {
            this.degen_node_count = degen_node_count;
        }

        public void run() {
            for (int i = 1; i <= GENOME_DB.num_genomes; i++) { // first value is always a 0
                long[] counts = new long[3];
                degen_node_count.put("genome_" + i, counts);
            }
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                long degen_nodes = count_nodes(DEGENERATE_LABEL);
                ResourceIterator<Node> all_kmers = GRAPH_DB.findNodes(DEGENERATE_LABEL);
                long node_counter = 0; // number of degen nodes in pangenome
                long distinct_counter = 0; // number of k-mers in degen nodes (in pangenome)
                long total_counter = 0; // total frequency of the degenerate k-mers in all (selected) genomes of the pangenome
                while (all_kmers.hasNext()) {
                    node_counter ++;
                    Node nuc_node = all_kmers.next();
                    int node_len = (int) nuc_node.getProperty("length");
                    int actual_kmers = node_len - K_SIZE + 1;
                    long[] freq_array = (long[]) nuc_node.getProperty("frequencies");
                    if (node_counter % 1000 == 0 || node_counter == degen_nodes || node_counter < 101) {
                        System.out.print("\rGathering degenerate nodes " + node_counter + "/" + degen_nodes);
                    }
                    if (compressed_kmers) {
                        distinct_counter ++;
                    } else {
                        distinct_counter += actual_kmers;
                    }

                    for (int genomeNr : selectedGenomes) { // first value is always a 0
                        if (freq_array[genomeNr] == 0) {
                            continue;
                        }
                        long[] degen_counts = degen_node_count.get("genome_" + genomeNr);
                        degen_counts[0] += 1;
                        if (compressed_kmers) {
                            total_counter += freq_array[genomeNr];
                            degen_counts[1] += 1;
                            degen_counts[2] += freq_array[genomeNr];
                        } else {
                            total_counter += actual_kmers * freq_array[genomeNr];
                            degen_counts[1] += actual_kmers;
                            degen_counts[2] += actual_kmers * freq_array[genomeNr];
                        }
                    }
                }
                long[] degen_values = new long[]{node_counter, distinct_counter, total_counter};
                degen_node_count.put("nodes_in_pangenome", degen_values);
                tx.success(); // end of neo4j transaction
            }
            System.out.print("\r                                                        ");
        }
    }

    /**
     * in the newest pantools version, a frequency is changed from a long to an integer
     * @param nuc_node
     * @return
     */
    private int get_kmer_frequency(Node nuc_node) {
        int frequency;
        try {
            frequency = (int) nuc_node.getProperty("frequency");
        } catch (ClassCastException classcast) {
            long freq_long = (long) nuc_node.getProperty("frequency");
            frequency = (int) freq_long;
        }
        return frequency;
    }

    /**
     Genome nodes are not always retrieved in the correct order (1,2,3)
     */
    private void retrieve_selected_sequences() {
        sequences_per_genome = new ConcurrentHashMap<>();
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                Node genome_node = genome_nodes.next();
                int genomeNr = (int) genome_node.getProperty("number");
                counter ++;
                if (counter % 10 == 0) {
                    System.out.print("\rRetrieving sequences: genome " + counter);
                }
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                for (int sequenceNr = 1; sequenceNr <= num_sequences; ++sequenceNr) {
                    try_incr_AL_chashmap(sequences_per_genome, genomeNr +"", sequenceNr); // holds all sequences per genome, use skip_seq_array to through list faster
                    if (selectedSequences.contains(genomeNr + "_" + sequenceNr)) {
                        try_incr_AL_chashmap(sequences_per_genome, genomeNr + "_selected", sequenceNr); // holds all sequences per genome, use skip_seq_array to through list faster
                    }
                }
            }
            tx.success();
        }
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     */
    private synchronized void try_incr_AL_chashmap(ConcurrentHashMap <String, ArrayList<Integer>> hashmap, String key, int value) {
        ArrayList<Integer> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     */
    private synchronized void try_incr_AL_chashmap(ConcurrentHashMap <String, ArrayList<Node>> hashmap, String key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     */
    private synchronized void try_incr_AL_chashmap(ConcurrentHashMap<Long, ArrayList<Node>> hashmap, Long key, Node value) {
        ArrayList<Node> value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new ArrayList<>();
        }
        value_array.add(value);
        hashmap.put(key, value_array);
    }
}
