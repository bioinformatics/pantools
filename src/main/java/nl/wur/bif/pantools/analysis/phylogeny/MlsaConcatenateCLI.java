package nl.wur.bif.pantools.analysis.phylogeny;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Step 2/3 of MLSA. Concatenate the gene selection into a single continuous sequence.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "mlsa_concatenate", sortOptions = false)
public class MlsaConcatenateCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-g", "--genes"}, required = true)
    void setGenes(String value) {
        genes = Arrays.asList(value.split(","));
    }
    List<String> genes;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.mlsa_concatenate(genes);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
    }

}
