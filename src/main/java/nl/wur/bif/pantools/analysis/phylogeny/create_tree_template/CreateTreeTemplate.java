package nl.wur.bif.pantools.analysis.phylogeny.create_tree_template;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.analysis.phylogeny.Phylogeny;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class CreateTreeTemplate {

    //final public static String[] COLOR_CODES2 = new String[]{"#e6194B", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4",
    //        "#42d4f4", "#f032e6", "#bfef45", "#fabed4", "#469990", "#dcbeff", "#9A6324", "#fffac8", "#800000", "#aaffc3",
    //        "#808000", "#ffd8b1", "#000075", "#a9a9a9", "#000000", "#ffffff"}; // length is 22, last one is white.

    final static public String[] COLOR_CODES = new String[]{"#fabebe","#bfef45","#42d4f4","#ffd8b1","#aaffc3","#fffac8",
            "#e6beff","#469990","#e6194B","#f58231","#ffe119","#3cb44b","#4363d8","#911eb4","#a9a9a9","#800000","#808000",
            "#9A6324","#000075","#f032e6"};

    final static public String[] COLORBLIND_CODES = new String[]{"#E69F00","#56B4E9","#009E73","#F0E442","#D55E00","#0072B2"
           ,"#CC79A7","#999999","#000000"}; // colourblind friendly palette up to 8 colors (9th value is black)

    private PhasedFunctionalities pf;

    /**
     * create_tree_templates()
     *
     Create ITOL templates
     https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/

     Requires either one of the following arguments
     --sequence
     --genome
     --gene-tree

     Optional
     --phenotype
     --value the number of genomes a phenotype should have before a color is assigned
     --no-numbers
     */
    public void createTreeTemplates(String templateType, Path geneTree, boolean excludeGenomeNumbers, int coloringRequiredGenomes) {
        Pantools.logger.info("Creating templates for phylogenetic trees in ITOL.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(true, true); // create skip array if -skip/-ref is provided by user
            retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        pf = new PhasedFunctionalities();
        ///pf.checkGenomeSequenceArguments();
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(0, false);

        //int required_genomes = templates_check_required_genomes();
        Pantools.logger.info("--color is {}. A phenotype must have this many genomes before a color is assigned.", coloringRequiredGenomes);

        HashMap<String, ArrayList<Integer>> genomesPerPhenotype = retrieve_phenotypes_for_tree_templates2();
        if (genomesPerPhenotype.isEmpty()) {
            Pantools.logger.error("No phenotype information was included in the pangenome.");
            System.exit(1);
        }
        HashMap<String, HashSet<String>> phenotypePropertyMap = create_pheno_properties_map(genomesPerPhenotype);
        String[] spacesLongestPhenotype = create_extra_spaces_for_outlining(phenotypePropertyMap);

        String phasing = "";
        if (PHASED_ANALYSIS) {
            pf.preparePhasedGenomeInformation(true, selectedSequences);
            phasing = "_phasing";
            if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
                Pantools.logger.error("No phasing information is present in the pangenome. Please exclude --phasing");
                System.exit(1);
            }
        }

        String genome_or_sequence1 = "genome", genome_or_sequence2 = "genome numbers";
        if (compare_sequences) {
            genome_or_sequence1 = "sequence";
            genome_or_sequence2 = "sequence identifiers";
        }

        String output_path;
        if (PHENOTYPE != null) {
            if (excludeGenomeNumbers) {
                Pantools.logger.info("The phenotype '{}' was included and {} are exluded!", PHENOTYPE, genome_or_sequence2);
                Pantools.logger.info("The created templates will match trees where nodes only contain a phenotype value.");
            } else {
                Pantools.logger.info("The phenotype '{}' was included.", PHENOTYPE);
                Pantools.logger.info("The created templates will match trees where nodes have {} combined with the '{}' phenotype values!", genome_or_sequence2, PHENOTYPE);
                Pantools.logger.info("Include the --mode no-numbers argument to EXCLUDE {} from templates", genome_or_sequence2);
            }
            output_path = WORKING_DIRECTORY + "tree_templates/" + genome_or_sequence1 + "_" + PHENOTYPE + phasing + "/";
        } else {
            if (excludeGenomeNumbers) {
                Pantools.logger.error("--mode no-numbers cannot be selected when no phenotype is included.");
                System.exit(1);
            }
            Pantools.logger.info("No --phenotype included. The created templates will only match a tree with genome numbers.");
            output_path = WORKING_DIRECTORY + "tree_templates/" + genome_or_sequence1 + "/";
        }

        Pantools.logger.info("Phenotype name {} Phenotype values Used colors", spacesLongestPhenotype[0]);
        if (templateType.equals("gene_tree")) {
            // --color does not have an effect on gene trees
            // --no-numbers can soon  be used on gene trees. not now.. why? this function requires the genome number to find the correct phenotype
            HashMap<String, String> phenotypeValueGenome = retrieve_phenotypes_for_tree_templates3();
            ArrayList<String> labels = getLabelsFromNewick(geneTree);
            String fileName = geneTree.getFileName().toString();
            String[] fileNameArray = fileName.split("\\.");
            createDirectory(WORKING_DIRECTORY  + "/tree_templates/gene_tree/" + fileNameArray[0], false);
            for (String phenotypeName : phenotypePropertyMap.keySet()) {
                boolean hasGenomeNumbers = true;
                for (String treeLabel : labels) { // all labels starts with a genome number
                    String[] labelArray = treeLabel.split("_");
                    String phenotypeValue = phenotypeValueGenome.get(labelArray[0] + "_" + phenotypeName);
                    if (phenotypeValue == null) {
                        hasGenomeNumbers = false;
                    }
                }

                HashSet<String> phenotypeValues = phenotypePropertyMap.get(phenotypeName);
                if (hasGenomeNumbers) {
                    createLabelRingTemplatesForGeneTree(phenotypeName, phenotypeValues, labels, phenotypeValueGenome,
                            fileNameArray[0], coloringRequiredGenomes, genomesPerPhenotype, spacesLongestPhenotype[1].length());
                } else {
                    createLabelRingTemplatesForGeneTreeComplex(phenotypeName, phenotypeValues, labels, phenotypeValueGenome,
                            fileNameArray[0], coloringRequiredGenomes, genomesPerPhenotype, spacesLongestPhenotype[1].length());
                }
            }
            Pantools.logger.info("Template files written to:");
            Pantools.logger.info(" {}/tree_templates/gene_tree/{}/", WORKING_DIRECTORY, fileNameArray[0]);
            return;
        }

        createDirectory(output_path + "label", false);
        createDirectory(output_path + "ring", false);
        for (String phenotype : phenotypePropertyMap.keySet()) {
            HashSet<String> phenotype_values = phenotypePropertyMap.get(phenotype);
            create_label_ring_templates(phenotype, phenotype_values, genomesPerPhenotype, coloringRequiredGenomes,
                    output_path, spacesLongestPhenotype[1].length(), excludeGenomeNumbers);
        }
        Phylogeny.create_color_code_text_file();
        Pantools.logger.info("Template files written to:");
        Pantools.logger.info(" {}ring/", output_path);
        Pantools.logger.info(" {}label/", output_path);
    }

    public ArrayList<String> getLabelsFromNewick(Path file) {
        ArrayList<String> labels = new ArrayList<>();
        try {
            BufferedReader br = Files.newBufferedReader(file);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                  line =  line.replace(",", "#").replace(";", "#").replace(")", "").replace("(", "");
                  String[] lineArray = line.split("#");
                  for (String label : lineArray) {
                     String[] labelArray = label.split(":");
                     labels.add(labelArray[0]);
                 }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read the newick file: {}", file);
            System.exit(1);
        }
        return labels;
    }



    /**
     * With --value the minium required number of genomes to color to a phenotypes is set.
     * Default is two genomes.
     * @return minimum number of required genomes/sequences with phenotype
     */
    /*
    public static int templates_check_required_genomes() {
        int required_genomes = 2;
        if (NODE_VALUE != null) {
            try {
                required_genomes = Integer.parseInt(NODE_VALUE);
            } catch (NumberFormatException no) {
                System.out.println("'" + NODE_VALUE + "' is not a numerical value\n");
                System.exit(1);
            }
            System.out.println("\rSelected --value is " + NODE_VALUE + ". A phenotype must have this many genomes before a color is assigned");
        } else {
            System.out.println("\rNo --value was selected. A color is assigned to phenotype values present in least 2 genomes");
        }
        return required_genomes;
    }*/

    /**
     * @param genomesPerPhenotype key is phenotype property + "#" + phenotype value. Value is list with genome numbers
     * @return key is phenotype property. value is set with phenotype properties
     */
    public static HashMap<String, HashSet<String>> create_pheno_properties_map(HashMap<String, ArrayList<Integer>> genomesPerPhenotype) {
        HashMap<String, HashSet<String>> pheno_property_map = new HashMap<>();
        for (String pheno_property_value : genomesPerPhenotype.keySet()) {
            String[] pheno_array = pheno_property_value.split("#");
            pheno_property_map.computeIfAbsent(pheno_array[0], k -> new HashSet<>()).add(pheno_array[1]);
        }
        return pheno_property_map;
    }

    /**
     * Extracts all available phenotypes from 'phenotype' nodes
     * @param pheno_property_map key is phenotype property, value is the different phenotype properties
     * @param genomesPerPhenotype key is phenotype property + "#" + phenotype value. Value is list with genome numbers
     */
    
    /**
     * @param pheno_property_map key is phenotype property. value is set with phenotype properties
     * @return string [ extra spaces , the longest phenotype string]
     */
    public static String[] create_extra_spaces_for_outlining(HashMap<String, HashSet<String>> pheno_property_map) {
        String longest_pheno = "Phenotype name"; // 'Phenotype name' has 14 characters
        for (String phenotype : pheno_property_map.keySet()) {
            if (phenotype.length() > longest_pheno.length()) {
                longest_pheno = phenotype;
            }
        }
        int extra_spaces_count = longest_pheno.length()-14;
        StringBuilder extra_spaces = new StringBuilder();
        for (int i = 0; i < extra_spaces_count; i++) {
            extra_spaces.append(" ");
        }
        return new String[]{extra_spaces.toString(), longest_pheno};
    }

    /**
     * Extracts all available phenotypes from 'phenotype' nodes
     * @return genomesPerPhenotype,
     *     - key is a combination of phenotype property and phenotype value.
     *     - value is list of genome numbers
     */
    public HashMap<String, ArrayList<Integer>> retrieve_phenotypes_for_tree_templates2() {
        HashMap<String, ArrayList<Integer>> genomesPerPhenotype = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotype_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotype_nodes.hasNext()) {
                Node pheno_node = phenotype_nodes.next();
                int genomeNr = (int) pheno_node.getProperty("genome");
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                Iterable<String> node_properties = pheno_node.getPropertyKeys();
                for (String pheno_property : node_properties) {
                    if (pheno_property.equals("genome")) {
                        continue;
                    }
                    String valueString = "";
                    Object ph_value = pheno_node.getProperty(pheno_property);
                    if (ph_value instanceof Integer || ph_value instanceof String || ph_value instanceof Boolean) {
                        valueString = ph_value.toString();
                    } else if (ph_value instanceof Double || ph_value instanceof Long) {
                        valueString = String.valueOf(ph_value);
                    } else {
                        Pantools.logger.error("Something unexpected happened. Please contact the development team. 50623");
                        System.exit(1);
                    }
                    if (ph_value.equals("") || ph_value.equals("?")) {
                        continue;
                    }
                    genomesPerPhenotype.computeIfAbsent(pheno_property + "#" + valueString, k -> new ArrayList<>()).add(genomeNr);
                }
            }
            tx.success();
        }
        return genomesPerPhenotype;
    }

    /**
     * Extracts all available phenotypes from 'phenotype' nodes
     * @return
     */
    public HashMap<String, String> retrieve_phenotypes_for_tree_templates3() {
        HashMap<String, String> phenotypeValueGenome = new HashMap<>(); // key is a genome number + phenotype name . value is phenotype value
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> phenotype_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
            while (phenotype_nodes.hasNext()) {
                Node phenotypeNode = phenotype_nodes.next();
                int genomeNr = (int) phenotypeNode.getProperty("genome");
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                Iterable<String> node_properties = phenotypeNode.getPropertyKeys();
                for (String pheno_property : node_properties) {
                    if (pheno_property.equals("genome")) {
                        continue;
                    }
                    String ph_value_str = "";
                    Object ph_value = phenotypeNode.getProperty(pheno_property);
                    if (ph_value instanceof Integer || ph_value instanceof String || ph_value instanceof Boolean) {
                        ph_value_str = ph_value.toString();
                    } else if (ph_value instanceof Double || ph_value instanceof Long) {
                        ph_value_str = String.valueOf(ph_value);
                    } else {
                        Pantools.logger.error("Something unexpected happened. Please contact the development team. 50623");
                        System.exit(1);
                    }
                    if (ph_value.equals("") || ph_value.equals("?")) {
                        continue;
                    }
                    //pheno_property_map.computeIfAbsent(property, k -> new HashSet<>()).add(value_str);
                    phenotypeValueGenome.put(genomeNr + "_" + pheno_property,ph_value_str);
                }
            }
            tx.success();
        }
        return phenotypeValueGenome;
    }

    /**
     *
     * @param phenotypeName
     * @param phenotypeValues
     * @param labels
     * @param phenotypeValueGenome
     * @param fileName
     * @param coloringRequiredGenomes
     */
    private void createLabelRingTemplatesForGeneTree(String phenotypeName, HashSet<String> phenotypeValues,
                                                     ArrayList<String> labels, HashMap<String, String> phenotypeValueGenome, String fileName,
                                                     int coloringRequiredGenomes, HashMap<String, ArrayList<Integer>> genomesPerPhenotype,
                                                     int longest_pheno) {

        //int requiredColors = determineRequiredColors(phenotypeName, phenotypeValues, genomesPerPhenotype, coloringRequiredGenomes);
        String label = phenotypeName;
        if (PHENOTYPE != null) {
            label = PHENOTYPE + " colored_by " + phenotypeName;
        }
        StringBuilder label_builder = new StringBuilder("TREE_COLORS\nSEPARATOR COMMA\nDATA\n\n");
        StringBuilder ringTemplate = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL," + label
                + "\nCOLOR,#ff0000\nSTRIP_WIDTH,40\nSHOW_INTERNAL,1\nDATA\n\n");

        HashMap<String, String> colorMap = prepareColorMapGeneTree(phenotypeName, phenotypeValues, genomesPerPhenotype,
                coloringRequiredGenomes);

        if (colorMap.isEmpty()) {
            return;
        }

        for (String treeLabel : labels) { // all labels starts with a genome number
            String[] labelArray = treeLabel.split("_");
            String phenotypeValue = phenotypeValueGenome.get(labelArray[0] + "_" + phenotypeName);
            if (phenotypeValue == null) {
                Pantools.logger.error("Genome {} does not have the phenotype '{}'", labelArray[0], phenotypeName);
                System.exit(1);
            }
            String colorCode = colorMap.get(phenotypeValue);
            if (colorCode == null) {
                Pantools.logger.error("{} cannot be found. Not allowed to happen...", phenotypeValue);
                System.exit(1);
            }
            ringTemplate.append(treeLabel).append(",").append(colorCode).append(",").append(phenotypeValue).append("\n");
            label_builder.append(treeLabel).append(",range,").append(colorCode).append(",").append(phenotypeValue).append(",1.0\n");
        }
        printUsedColors(longest_pheno, phenotypeName, phenotypeValues, phenotypeValues.size());
        writeStringToFile(ringTemplate.toString(), WORKING_DIRECTORY  + "/tree_templates/gene_tree/" + fileName + "/" + phenotypeName + "_ring.txt", false, false);
        writeStringToFile(label_builder.toString(), WORKING_DIRECTORY  + "/tree_templates/gene_tree/" + fileName + "/" + phenotypeName + "_label.txt", false, false);
    }

    /**
     *
     * @param phenotypeName
     * @param phenotypeValues
     * @param labels
     * @param phenotypeValueGenome
     * @param fileName
     * @param coloringRequiredGenomes
     */
    private void createLabelRingTemplatesForGeneTreeComplex(String phenotypeName, HashSet<String> phenotypeValues,
                                                     ArrayList<String> labels, HashMap<String, String> phenotypeValueGenome, String fileName,
                                                     int coloringRequiredGenomes, HashMap<String, ArrayList<Integer>> genomesPerPhenotype,
                                                     int longest_pheno) {

        //int requiredColors = determineRequiredColors(phenotypeName, phenotypeValues, genomesPerPhenotype, coloringRequiredGenomes);
        String label = phenotypeName;
        if (PHENOTYPE != null) {
            label = PHENOTYPE + " colored_by " + phenotypeName;
        }
        StringBuilder label_builder = new StringBuilder("TREE_COLORS\nSEPARATOR COMMA\nDATA\n\n");
        StringBuilder ringTemplate = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL," + label
                + "\nCOLOR,#ff0000\nSTRIP_WIDTH,40\nSHOW_INTERNAL,1\nDATA\n\n");
        HashMap<String, String> colorMap = prepareColorMapGeneTree(phenotypeName, phenotypeValues, genomesPerPhenotype,
                coloringRequiredGenomes);
        if (colorMap.isEmpty()) {
            return;
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 1; i <= total_genomes; i++) { //
                if (skip_array[i - 1]) {
                    continue;
                }
                HashMap<String, Node> mrnaNodeMap = create_mrna_node_map_for_genome(i, "Making template");
                for (String treeLabel : labels) { // all labels starts with a genome number
                    String[] labelArray = treeLabel.split("_");
                    boolean found = true;
                    String mrnaId;
                    for (int j = 0; j < labelArray.length; j++) {
                        String id = getMrnaIdFromlabel(labelArray, j);
                        if (mrnaNodeMap.containsKey(id)) {
                            Node mrnaNode = mrnaNodeMap.get(id);
                            int genomerNr = (int) mrnaNode.getProperty("genome");
                            String phenotypeValue = phenotypeValueGenome.get(genomerNr + "_" + phenotypeName);
                            if (phenotypeValue == null) {
                                Pantools.logger.error("Genome {} does not have the phenotype '{}'", genomerNr, phenotypeName);
                                System.exit(1);
                            }
                            String colorCode = colorMap.get(phenotypeValue);
                            if (colorCode == null) {
                                Pantools.logger.error("{} cannot be found. Not allowed to happen...", phenotypeValue);
                                System.exit(1);
                            }
                            ringTemplate.append(treeLabel).append(",").append(colorCode).append(",").append(phenotypeValue).append("\n");
                            label_builder.append(treeLabel).append(",range,").append(colorCode).append(",").append(phenotypeValue).append(",1.0\n");
                            break;
                        }
                    }
                    if (!found) {
                        Pantools.logger.info("cannot find {}", treeLabel);
                    }
                }
            }
            tx.success();
        }

        printUsedColors(longest_pheno, phenotypeName, phenotypeValues, phenotypeValues.size());
        writeStringToFile(ringTemplate.toString(), WORKING_DIRECTORY  + "/tree_templates/gene_tree/" + fileName + "/" + phenotypeName + "_ring.txt", false, false);
        writeStringToFile(label_builder.toString(), WORKING_DIRECTORY  + "/tree_templates/gene_tree/" + fileName + "/" + phenotypeName + "_label.txt", false, false);
    }

    private String getMrnaIdFromlabel(String[] labelArray, int startPosition) {
        StringBuilder idBuilder = new StringBuilder();
        for (int i = startPosition; i < labelArray.length; i++) {
            idBuilder.append(labelArray[i]).append("_");
        }
        return idBuilder.toString().replaceFirst(".$",""); // remove last character
    }

    private int determineRequiredColors(String phenotypeName, HashSet<String> phenotypeValues,
                                        HashMap<String, ArrayList<Integer>> genomesPerPhenotype,
                                        int coloringRequiredGenomes) {

        int requiredColors = 0;
        for (String value : phenotypeValues) {
            ArrayList<Integer> genomes = genomesPerPhenotype.get(phenotypeName + "#" + value);
            if (genomes.size() < coloringRequiredGenomes) {
                continue;
            }
            requiredColors++;
        }
        //System.out.println(phenotypeName + " required colors: " + requiredColors);
        return requiredColors;
    }

    private HashMap<String, String> prepareColorMapGeneTree(String phenotypeName, HashSet<String> phenotypeValues,
                                                            HashMap<String, ArrayList<Integer>> genomesPerPhenotype,
                                                            int coloringRequiredGenomes) {

        int valueCounter = 0; // phenotype values with more than required number of genomes
        HashMap<String, String> colorMap = new HashMap<>();
        for (String phenotypeValue : phenotypeValues) {
            if (valueCounter >= COLOR_CODES.length) { // can only visualize up to 20 colors, allow 0-19
                continue;
            }

            //ArrayList<Integer> genomes = genomesPerPhenotype.get(phenotypeName + "#" + phenotypeValue);
            //if (genomes.size() < coloringRequiredGenomes) {
            //    continue;
            //}
            //System.out.println(phenotypeValue + " " + COLORBLIND_CODES[valueCounter]);
            if (phenotypeValues.size() < COLORBLIND_CODES.length) { // when less than 9 (?) phenotypes, use the colorblind friendly palette
                colorMap.put(phenotypeValue, COLORBLIND_CODES[valueCounter]);
            } else {
                colorMap.put(phenotypeValue, COLOR_CODES[valueCounter]);
            }
            valueCounter++;
        }
        return colorMap;
    }

    /**
     * Create the ring and label templates for ITOL
     * @param phenotypeName phenotype property
     * @param phenotypeValues all availible values for the phenotype
     * @param genomesPerPhenotype key is phenotype property + "#" + phenotype value. Value is list with genome numbers
     * @param coloringRequiredGenomes default is 2, can be adjusted by user with --color
     */
    public void create_label_ring_templates(String phenotypeName, HashSet<String> phenotypeValues,
                                            HashMap<String, ArrayList<Integer>> genomesPerPhenotype,
                                            int coloringRequiredGenomes, String output_path, int longest_pheno, boolean excludeGenomeNumbers) {

        // first loop to check if the number of phenotype colors exceeds 8
        int requiredColors = determineRequiredColors(phenotypeName, phenotypeValues, genomesPerPhenotype, coloringRequiredGenomes);
        int value_counter = 0; // phenotype values with more than required number of genomes
        String label = phenotypeName;
        if (PHENOTYPE != null) {
            label = PHENOTYPE + " colored_by " + phenotypeName;
        }
        StringBuilder label_builder = new StringBuilder("TREE_COLORS\nSEPARATOR COMMA\nDATA\n\n");
        StringBuilder ring_builder = new StringBuilder("DATASET_COLORSTRIP\nSEPARATOR COMMA\nDATASET_LABEL," + label
                + "\nCOLOR,#ff0000\nSTRIP_WIDTH,40\nSHOW_INTERNAL,1\nDATA\n\n");

        for (String phenotypeValue : phenotypeValues) {
            ArrayList<Integer> genomes = genomesPerPhenotype.get(phenotypeName + "#" + phenotypeValue);
            if (genomes.size() < coloringRequiredGenomes) { // do not color this value
                continue;
            }
            if (compare_genomes) {
                for (int genomeNr : genomes) {
                    if (skip_array[genomeNr-1]) {
                        continue;
                    }
                    if (value_counter >= COLOR_CODES.length) { // can only visualize up to 20 colors, allow 0-19
                        continue;
                    }
                    String pheno_label = ""; // this phenotype value must match the node label in the tree
                    if (PHENOTYPE != null && geno_pheno_map != null) { // --phenotype was included
                        pheno_label = geno_pheno_map.get(genomeNr);
                    }
                    if (excludeGenomeNumbers) { // can only be used in combination with --phenotype
                        label_builder.append(pheno_label);
                        ring_builder.append(pheno_label);
                    } else {
                        if (!pheno_label.equals("")) {
                            pheno_label = "_" + pheno_label;
                        }
                        label_builder.append(genomeNr).append(pheno_label);
                        ring_builder.append(genomeNr).append(pheno_label);
                    }
                    if (requiredColors < 9) { // use colourblind friendly pallette
                        label_builder.append(",range,").append(COLORBLIND_CODES[value_counter]).append(",").append(phenotypeValue).append(",1.0\n");
                        ring_builder.append(",").append(COLORBLIND_CODES[value_counter]).append(",").append(phenotypeValue).append("\n");
                    } else {
                        label_builder.append(",range,").append(COLOR_CODES[value_counter]).append(",").append(phenotypeValue).append(",1.0\n");
                        ring_builder.append(",").append(COLOR_CODES[value_counter]).append(",").append(phenotypeValue).append("\n");
                    }
                }
            } else { // --sequence
                for (String sequenceId : selectedSequences) {
                    String phasingId = pf.createPhasingRenamingPhylogeny(sequenceId, true);
                    String[] seq_array = sequenceId.split("_");
                    int genome_nr = Integer.parseInt(seq_array[0]);
                    if (!genomes.contains(genome_nr)) {
                        continue;
                    }
                    if (skip_array[genome_nr-1]) {
                        continue;
                    }
                    if (value_counter >= COLOR_CODES.length) { // can only visualize up to 20 colors, allow 0-19
                        continue;
                    }
                    String pheno_label = ""; // this phenotype value must match the node label in the tree
                    if (PHENOTYPE != null && geno_pheno_map != null) { // --phenotype was included
                        pheno_label = geno_pheno_map.get(genome_nr);
                    }
                    if (excludeGenomeNumbers) {
                        label_builder.append(phenotypeValue).append(phasingId);
                        ring_builder.append(phenotypeValue).append(phasingId);
                    } else {
                        if (!pheno_label.equals("")) {
                            pheno_label = "_" + pheno_label;
                        }
                        label_builder.append(sequenceId).append(pheno_label).append(phasingId);
                        ring_builder.append(sequenceId).append(pheno_label).append(phasingId);
                    }
                    if (requiredColors < 9) { // use colourblind friendly pallette
                        label_builder.append(",range,").append(COLORBLIND_CODES[value_counter]).append(",").append(phenotypeValue).append(phasingId).append(",1.0\n");
                        ring_builder.append(",").append(COLORBLIND_CODES[value_counter]).append(",").append(phenotypeValue).append(phasingId).append("\n");
                    } else {
                        label_builder.append(",range,").append(COLOR_CODES[value_counter]).append(",").append(phenotypeValue).append(phasingId).append(",1.0\n");
                        ring_builder.append(",").append(COLOR_CODES[value_counter]).append(",").append(phenotypeValue).append(phasingId).append("\n");
                    }
                }
            }
            value_counter++;
        }

        printUsedColors(longest_pheno, phenotypeName, phenotypeValues, value_counter);
        writeStringToFile(label_builder.toString(), output_path + "label/" + phenotypeName + ".txt", false, false);
        writeStringToFile(ring_builder.toString(), output_path + "/ring/" + phenotypeName + ".txt", false, false);
    }

    private void printUsedColors(int longest_pheno, String phenotypeName, HashSet<String> phenotypeValues, int value_counter){
        int extra_spaces1 = longest_pheno - phenotypeName.length();
        StringBuilder spaces1 = new StringBuilder();
        for (int i = 0; i < extra_spaces1; i++) { // extra spaces for outlining text
            spaces1.append(" ");
        }

        String number_of_prop_str = String.valueOf(phenotypeValues.size());
        int extra_spaces2 = 16 - number_of_prop_str.length(); // 'phenotype values' is 16 characters long
        StringBuilder spaces2 = new StringBuilder();
        for(int i = 0; i < extra_spaces2; i++) { // extra spaces for outlining text
            spaces2.append(" ");
        }

        if (value_counter < 21) { // can only visualize up to 20 colors, allow counter of 20
            Pantools.logger.info("{}{} {}{} {}", phenotypeName, spaces1, phenotypeValues.size(), spaces2, value_counter);
        } else {
            Pantools.logger.warn("{}{} {}{} 20 but unable to visualize all {}phenotype values!", phenotypeName, spaces1, phenotypeValues.size(), spaces2, value_counter);
        }
    }


}
