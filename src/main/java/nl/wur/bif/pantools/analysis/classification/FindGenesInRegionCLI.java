package nl.wur.bif.pantools.analysis.classification;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Find genes in a given genomic region.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "find_genes_in_region", sortOptions = false)
public class FindGenesInRegionCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "regions-file", index = "0+")
    @InputFile(message = "{file.regions}")
    Path regionsFile;

    @Option(names = "--partial") boolean partial;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        classification.find_genes_in_region(true);
        return 0;
    }

    private void setGlobalParameters() {
        PATH_TO_THE_REGIONS_FILE = regionsFile.toString();
        if (partial) Mode = "PARTIAL";
    }

}
