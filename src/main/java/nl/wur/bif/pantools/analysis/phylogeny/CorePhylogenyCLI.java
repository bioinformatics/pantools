package nl.wur.bif.pantools.analysis.phylogeny;

import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.SelectHmGroups;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.cli.validation.Constraints.MatchInteger;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.GraphUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create a SNP tree from single-copy genes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "core_phylogeny", aliases = "core_snp_tree", sortOptions = false)
public class CorePhylogenyCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;
    @ArgGroup private SelectHmGroups selectHmGroups = new SelectHmGroups(); //needs to be initialized to prevent NullPointerException

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    private static boolean alignNucleotide;
    private static boolean alignProtein;

    @ArgGroup AlignmentMode alignmentMode;
    private static class AlignmentMode {

        @Option(names = "--align-nucleotide")
        void setAlignNucleotide(boolean value) {
            alignNucleotide = value;
        }

        @Option(names = "--align-protein")
        void setAlignProtein(boolean value) {
            alignProtein = value;
        }
    }

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Option(names = {"--phasing"})
    boolean phasing;

    // only allow --pavs when --variants is used
    @ArgGroup(exclusive = false)
    VariationOptions variationOptions;
    private static boolean alignVariants;
    private static boolean pavs;

    private static class VariationOptions {
        @Option(names = {"-v", "--variants"}, required = true)
        void setVariants(boolean value) {
            alignVariants = value;
        }

        @Option(names = "--pavs")
        void setPavs(boolean value) {
            pavs = value;
        }
    }

    @Option(names = {"-m", "--clustering-mode"})
    @Pattern(regexp = "ML|NJ", flags = CASE_INSENSITIVE, message = "{pattern.clustering-mode}")
    String mode;

    @Option(names = "--blosum")
    @MatchInteger(value = {45, 50, 62, 80, 90}, message = "match.blosum")
    int blosum;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes, selectHmGroups);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        setAlignmentMode();
        phylogeny.core_phylogeny(selectHmGroups.getHomologyGroups(), alignNucleotide, alignProtein, alignVariants, pavs);
        return 0;
    }

    private void setAlignmentMode() { //TODO: remove msa from this command
        // Set default alignment mode if none were provided
        if (!alignNucleotide && !alignProtein && !alignVariants) {
            if (PROTEOME) {
                Pantools.logger.info("No alignment mode was provided, defaulting to protein alignment.");
                alignProtein = true;
            } else if (GraphUtils.containsVariantInformation()) {
                Pantools.logger.info("No alignment mode was provided, defaulting to variant alignment.");
                alignVariants = true;
            } else {
                Pantools.logger.info("No alignment mode was provided, defaulting to nucleotide alignment.");
                alignNucleotide = true;
            }
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignNucleotide && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Nucleotide alignment is unavailable for panproteomes");
        }
        // Assert that nucleotide alignment is not performed on a panproteome
        if (alignVariants && PROTEOME) {
            throw new ParameterException(spec.commandLine(), "Variant alignment is unavailable for panproteomes");
        }
        // Assert that the database contains variants if the method is "variants"
        if (alignVariants) {
            GraphUtils.validateAccessions("VCF");
        }
        // Assert that the database contains trimmed protein alignments per homology-group if alignProtein is used
        if (alignProtein && !GraphUtils.containsAlignmentOutput("per-group", "prot", true)) {
            Pantools.logger.error("Missing trimmed protein alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-protein' first.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed protein alignments per homology group");
        }
        // Assert that the database contains trimmed nucleotide alignments per homology-group if alignNucleotide is used
        if (alignNucleotide && !GraphUtils.containsAlignmentOutput("per-group", "nuc", true)) {
            Pantools.logger.error("Missing trimmed nucleotide alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-nucleotide' first; preferably including --trim-using-proteins.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed nucleotide alignments per homology group");
        }
        // Assert that the database contains trimmed variant alignments per homology-group if alignVariants is used
        if (alignVariants && !GraphUtils.containsAlignmentOutput("per-group", "var", true)) {
            Pantools.logger.error("Missing trimmed variant alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-variants' first.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed variant alignments per homology group");
        }
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        BLOSUM = blosum;
        PHENOTYPE = phenotype;
        CLUSTERING_METHOD = mode;
        if (alignProtein) Mode = "PROTEIN";

        if (phasing) {
            PHASED_ANALYSIS = true;
        }
    }
}
