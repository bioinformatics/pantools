package nl.wur.bif.pantools.analysis.gene_retention;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.construction.synteny.Synteny;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.Utils.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import java.nio.file.Path;

public class GeneRetention {

    private BlockingQueue<String> stringQueue;
    private static AtomicLong atomicCounter;

    /**
     * gene_retention()
     *
     * --window-length is window length and number of sequences in plots  (default is 100,20)
     * --longest-transcripts automatically set
     * --input-file to select the sequences that should be used as reference. default is all sequences
     * --include OR --exclude OR --selection-file
     * --phasing OR --distinct-colors
     * --run automatically runs all Rscripts
     *
     * step 1. which sequence is reference, which sequences to compare it to
     * step 2. go over the genes/ mrna of reference, from start to stop. using the set window size, assign a window nr to a mrna
     * step 3. go over homology groups. check which are shared with the reference . calculate a % retention for a certain window nr
     * Step 4. write an rscript and input
     * @param coloringType string is either 'phasing' or 'distinct'
     */
    public void geneRetentionVisualization(int windowLength, int allowedSequences, int sequencesPerGenome, Path inputSequences,
                                           String coloringType) {

        PhasedFunctionalities pf = new PhasedFunctionalities();
        Synteny synteny = new Synteny();
        Pantools.logger.info("Calculating gene retention between sequences.");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            Node pangenomeNode = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
            stop_if_panproteome("gene_retention"); // sets PROTEOME boolean that controls functions, retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // sets grouping_version and longest_transcripts
            if (!longest_transcripts) {
                Pantools.logger.info("--longest-transcripts is automatically included for this functionality.");
                longest_transcripts = true;
            }
            proLayer.find_longest_transcript_per_gene(false, 0);
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the pangenome database.");
            System.exit(1);
        }

        if (sequencesPerGenome > 21 && coloringType.equals("distinct")) {
            Pantools.logger.info("--sequences-genome is set to {}, which is more than the 21 available distinct colors. ", sequencesPerGenome);
            Pantools.logger.info("The least similar sequences are colored in dark grey.");
        } else {
            Pantools.logger.info("--sequences-genome is set to " + sequencesPerGenome + ".");
        }

        if (allowedSequences == 999) { // 999 is default
            allowedSequences = adj_total_genomes * sequencesPerGenome;
            Pantools.logger.info("--sequences-plot is set to: {} * {} (genomes * allowed sequences per genome) = {}", adj_total_genomes, sequencesPerGenome, allowedSequences);
        } else {
            Pantools.logger.info("--sequences-plot is set to: " + allowedSequences);
        }
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(10000, false);
        pf.preparePhasedGenomeInformation(PHASED_ANALYSIS, selectedSequences);

        ArrayList<String> targetSequenceIds = readInputFileRetention(inputSequences); // fills selectedSequences
        skip.updateSkipArraysSequenceSelection(targetSequenceIds); // to only retrieve the gene order of the target sequences
        HashMap<String, String> renamedIdentifiersMap = synteny.retrieveRenamedSyntentyIdentifiers(false);
        HashMap<String, Node[]> mrnaNodesPerSequence = pf.getOrderedMrnaNodesPerSequence(false, true, null); // key is sequence identifier, value is array with 'gene' nodes
        skip.updateSkipArraysSequenceSelection(new ArrayList<>(selectedSequences)); // return skip_array to original state
        stringQueue = new LinkedBlockingQueue<>();

        String[] modes;
        if (!renamedIdentifiersMap.isEmpty() && !Mode.contains("SYNTELOGS")) {
            modes = new String[]{"homologs","syntelogs"};
        } else if (!renamedIdentifiersMap.isEmpty()) {
            modes = new String[]{"syntelogs"};
        } else {
            modes = new String[]{"homologs"};
        }

        StringBuilder missedLog = new StringBuilder("#Sequences excluded in the visualization because of --sequences-plot " +
                allowedSequences + " and --sequences-genome " +  sequencesPerGenome + " arguments. " +
                "Sequences are sorted in ascending order (starting with the highest number shared genes to the reference)\n" +
                "#sequence identifier,type,total included sequences,total excluded sequences, excluded sequences\n");

        int stepSize = 10;
        int sequenceCounter = 0;
        for (String sequenceId : targetSequenceIds) {
            sequenceCounter++;
            String[] seqIdArray = sequenceId.split("_");
            if (!mrnaNodesPerSequence.containsKey(sequenceId)) {
                continue;
            }
            Node[] mrnaNodesArray = mrnaNodesPerSequence.get(sequenceId);
            ArrayList<Node> mrnaNodes = new ArrayList<>(Arrays.asList(mrnaNodesArray));
            if (mrnaNodes.size() < windowLength) {
                System.out.println("\rWARNING: " + sequenceId + " was excluded because it has less than " + windowLength + " genes");
                continue;
            }
            createDirectory("retention/" + seqIdArray[0] + "/" + sequenceId + "/per_genome", true);
            ArrayList<Integer> geneAddressesForWindows = genePositionsToAddresses(mrnaNodes, windowLength, stepSize);
            System.out.print("\rPreparing retention files: Sequence " + sequenceCounter + "/" + targetSequenceIds.size());
            for (String homologsOrSyntelogs : modes) {
                HashMap<String, ArrayList<Node>> sharedGenesPerPosition = new HashMap<>(); // key is mrna position + "#" + sequence identifier, value is mRNA nodes
                int[][] geneCountsPerPosition;
                if (homologsOrSyntelogs.equals("syntelogs")) {
                    geneCountsPerPosition = findSyntenicRetention(mrnaNodes, sharedGenesPerPosition);
                } else { // homologs
                    geneCountsPerPosition = findHomologyRetention(mrnaNodes, sequenceId, sharedGenesPerPosition);
                }

                HashMap <String, int[]> sharedGenesPerWindow = convertPositionsIntoWindows(geneCountsPerPosition,
                        sequenceId, mrnaNodes, windowLength, stepSize);
                ArrayList<String>[] similarMissedSequences = findMostSimilarSequencesRetention(sharedGenesPerWindow, allowedSequences, sequencesPerGenome);
                ArrayList<String> mostSimilarSequences = similarMissedSequences[0];

                missedLog.append(sequenceId).append(",").append(homologsOrSyntelogs).append(",").append(mostSimilarSequences.size()).append(",").append(similarMissedSequences[1].size());
                if (!similarMissedSequences[1].isEmpty()) {
                    missedLog.append(",").append(similarMissedSequences[1].toString().replace("[", "").replace("]", "").replace(",", "")).append("\n");
                } else {
                    missedLog.append("\n");
                }

                createRetentionCsvOutput(sharedGenesPerPosition, mrnaNodes, mostSimilarSequences, homologsOrSyntelogs, sequenceId);
                createRetentionRScriptInput(sequenceId, windowLength, sharedGenesPerWindow, mostSimilarSequences,
                        homologsOrSyntelogs, geneAddressesForWindows);

                createRetentionRScript(sequenceId, mostSimilarSequences, homologsOrSyntelogs, coloringType);
            }
        }

        String missed = "";
        if (missedLog.length() > 95) {
            missed = "Not all sequences were included in the visualization. Check " + WORKING_DIRECTORY + "retention/excluded_sequences.csv";
        }
        writeStringToFile(missedLog.toString(),WORKING_DIRECTORY + "retention/excluded_sequences.csv",
                false, false);
        createOrRunRetentionRscripts();
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}retention/", WORKING_DIRECTORY);
        Pantools.logger.info(missed);
        Pantools.logger.info("Run Rscripts to generate the retention plots:");
        Pantools.logger.info(" sh {}retention/retention_rscripts.sh", WORKING_DIRECTORY);
    }

    /**
     *
     * @param sequenceId sequence identifier of the current reference sequence: 1_1 or 1_2
     * @param mostSimilarSequences a list of sequence identifiers
     * @param homologsOrSyntelogs string that can be "homologs", "syntelogs"
     * @param coloringType string that is either 'phasing' or 'distinct'
     */
    private void createRetentionRScript(String sequenceId, ArrayList<String> mostSimilarSequences, String homologsOrSyntelogs,
                                       String coloringType) {

        String defaultMultiplier = determineCombiPlotMultiplier();
        String[] seqIdArray = sequenceId.split("_");
        String outputDirectory = WD_full_path + "retention/" + seqIdArray[0] + "/" + sequenceId + "/";
        if (!checkIfFileExists(outputDirectory + homologsOrSyntelogs + "_retention.csv")) {
            return;
        }
        stringQueue.add("Rscript " + WD_full_path + "retention/" + seqIdArray[0] + "/" + sequenceId + "/" + homologsOrSyntelogs + "_retention.R");
        String R_LIB = check_r_libraries_environment();

        HashSet<Integer> genomeNumbers = genomesfromSequenceIds(mostSimilarSequences);
        HashMap<String, String> colorsPerGenome;
        if (coloringType.equals("phasing")) {
            colorsPerGenome = determineShadeColors(mostSimilarSequences, homologsOrSyntelogs, outputDirectory);
            if (colorsPerGenome.isEmpty()) {
                Pantools.logger.error("include --phasing");
                System.exit(1);
            }
        } else { // "distinct"
            colorsPerGenome = determineDistinctColors(mostSimilarSequences);
        }

        StringBuilder rscript = new StringBuilder();
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#Arguments to control the size of text and of the plot are given via the command line.\n")
                .append("# This was done to prevent this Rscript from becoming enormous as each plot requires some custom code\n")
                .append("\n")
                .append("#Assuming the R libraries were installed via Conda, the next line can be ignored. ")
                .append("If not, use it to manually install the required package\n")
                .append("#install.packages(\"cowplot\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(cowplot)\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("library(grid)\n")
                .append("library(gridExtra)\n\n");

        String referenceSequence = sequenceId;
        if (coloringType.equals("phasing") && phasingInfoMap.containsKey(sequenceId)) {
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            referenceSequence = sequenceId + " " + phasingInfo[3];
        }
        rscript.append("# Reference sequence: ").append(referenceSequence).append("\n")
                .append("\n")
                .append("args = commandArgs(trailingOnly=TRUE)\n")
                .append("show_titles = \"all\" # \"all\" is default, can also be set to \"none\"\n")
                .append("show_axis_titles = \"outer\" # \"outer\" is default, can also be set to \"all\" or \"none\"\n")
                .append("y_lower_limit <- 0\n")
                .append("y_higher_limit <- 100\n")
                .append("text_size <- 15\n")
                .append("individual_plot_height <- 5\n")
                .append("individual_plot_width <- 7.5\n")
                .append("size_multiplier <- ").append(defaultMultiplier).append("\n")
                .append("print_directory <- TRUE\n")
                .append("if (length(args) > 2) { # first argument\n")
                .append("   show_titles <- args[1]\n")
                .append("   if (show_titles != \"all\" & show_titles != \"none\") {\n")
                .append("       stop(\"show titles should be 'all' or 'none'\")\n")
                .append("   }\n")
                .append("}\n")
                .append("if (length(args) > 2) { # second argument\n")
                .append("    show_axis_titles <- args[2]\n")
                .append("    if (show_axis_titles != \"outer\" & show_axis_titles != \"all\" & show_axis_titles != \"none\") {\n")
                .append("        stop(\"show_axis_titles should be 'outer', 'all' or 'none'\")\n")
                .append("    }\n")
                .append("}\n")
                .append("if (length(args) > 2) { # third argument\n")
                .append("    y_lower_limit <- as.numeric(args[3])\n")
                .append("}\n")
                .append("if (length(args) > 3) { # fourth argument\n")
                .append("    y_higher_limit <- as.numeric(args[4])\n")
                .append("}\n")
                .append("if (length(args) > 4) { # fifth argument\n")
                .append("    text_size <- as.numeric(args[5])\n")
                .append("}\n")
                .append("if (length(args) > 5) { # sixth argument\n")
                .append("    individual_plot_height <- as.numeric(args[6])\n")
                .append("}\n")
                .append("if (length(args) > 6) { # seventh argument\n")
                .append("    individual_plot_width <- as.numeric(args[7])\n")
                .append("}\n")
                .append("if (length(args) > 7) { # eight argument\n")
                .append("    size_multiplier <- as.numeric(args[8])\n")
                .append("}\n")
                .append("if (length(args) > 8) { # ninth argument\n")
                .append("    print_directory <- as.logical(args[9])\n")
                .append("}\n")
                .append("if (length(args) > 9) { \n")
                .append("   stop(\"No more than nine arguments are allowed\")\n")
                .append("}\n")
                .append("\n");

        if (coloringType.equals("phasing")) {
            rscript.append("#Each color has 8 values for haplotype A-H and one additional for unphased sequences.\n" +
                            "#First color is assigned to haplotype A, second to haplotype B,...., ninth color is assigned to unphased sequences\n")
                    .append("red    <- c(\"#EE5E81\", \"#B8143C\", \"#EB476F\", \"#CF1744\", \"#F07593\", \"#E6194B\", \"#A11235\", \"#8A0F2D\", \"#F38CA5\")\n")
                    .append("green  <- c(\"#30903C\", \"#77CB81\", \"#3CB44B\", \"#246C2D\", \"#63C36F\", \"#36A244\", \"#2A7E35\", \"#50BC5D\", \"#9EDAA5\")\n")
                    .append("blue   <- c(\"#7B92E4\", \"#3C59C2\", \"#364FAD\", \"#4363D8\", \"#6982E0\", \"#8EA1E8\", \"#5673DC\", \"#2F4597\", \"#B4C1EF\")\n")
                    .append("purple <- c(\"#911EB4\", \"#B262CB\", \"#831BA2\", \"#BD78D2\", \"#9C35BC\", \"#66157E\", \"#C88FDA\", \"#A74BC3\", \"#D3A5E1\")\n")
                    .append("orange <- c(\"#F68F46\", \"#C46827\", \"#F8A86F\", \"#DD752C\", \"#F79B5A\", \"#AC5B22\", \"#F58231\", \"#AC5B22\", \"#FAC198\")\n")
                    .append("yellow <- c(\"#FFE119\", \"#B39E12\", \"#FFED75\", \"#99870F\", \"#E6CB17\", \"#665A0A\", \"#CCB414\", \"#80710D\", \"#FFF6BA\")\n\n");
        }

        rscript.append("df = read.csv(\"").append(outputDirectory).append(homologsOrSyntelogs).append("_retention.csv\", sep= \",\", header = TRUE)\n")
                .append("\n")
                .append("create_genome_retention <- function(df, plot_title){\n")
                .append("    plot = ggplot(data=df, aes(x=gene_address, y=retention, color=Sequence))  +\n")
                .append("    geom_line(aes(color=Sequence), size=1.1, na.rm=TRUE) +\n")
                .append("    scale_y_continuous(limits=c(y_lower_limit, y_higher_limit)) +\n")
                .append("    theme_classic(base_size=text_size) +\n")
                .append("    theme(legend.key.size = unit(1, \"line\")) +\n")  //  legend.text=element_text(size=10),
                .append("    scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6))\n")
                .append("    if (show_titles == \"all\"){\n")
                .append("        plot <- plot + ggtitle(plot_title) + theme(plot.title = element_text(hjust = 0.5))\n")
                .append("    }\n")
                .append("return (plot)\n")
                .append("}\n")
                .append("\n");

        if (coloringType.equals("phasing")) {
            String plotLegend = colorsPerGenome.get("legend");
            rscript.append(plotLegend);
        }

        int[] gridArray = calculateGridWith(genomeNumbers.size()); // [ columns, rows]
        ArrayList<Integer> yAxisGenomes = plotsWithYAxis(gridArray, genomeNumbers);
        ArrayList<Integer> xAxisGenomes = plotsWithXAxis(gridArray, genomeNumbers);
        ArrayList<Integer> whitexAxisGenomes = plotsWithWhiteXAxis(gridArray, genomeNumbers);

        int plotCounter = 0;
        StringBuilder cowPlotLineBuilder = new StringBuilder("combination_plot <- plot_grid(");
        StringBuilder cowPlotLegendBuilder = new StringBuilder("legends_combined <- plot_grid(");
        for (int genomeNr : genomeNumbers) {
            plotCounter++;
            String colorStr = colorsPerGenome.get(genomeNr +"");
            rscript.append("df_genome").append(genomeNr).append(" <- df[which(df$Genome == \"").append(genomeNr).append("\"),]\n")
                    .append("plot_").append(genomeNr).append(" <- create_genome_retention(df_genome").append(genomeNr).append(", \"Genome ").append(genomeNr).append(" to seq ").append(referenceSequence).append("\")\n")
                    .append("plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + ").append(colorStr).append("\n");

            rscript.append("plot_").append(genomeNr).append("_legend <- get_legend(plot_").append(genomeNr).append(")\n")
                    .append("grid.newpage()\n")
                    .append("\n")
                    .append("plot_").append(genomeNr).append("_individual <- plot_").append(genomeNr).append(" + ylab(\"% retention\") + xlab(\"Genomic position\") + theme(legend.position = \"none\")\n")
                    .append("ggsave(plot_").append(genomeNr).append("_individual, " +
                            "file=\"").append(outputDirectory).append("/per_genome/").append(homologsOrSyntelogs).append("_genome_").append(genomeNr).append(".png\", bg = \"white\"," +
                            " width = individual_plot_width, height = individual_plot_height)\n\n");

            rscript.append("#Changing titles/axes for the combination plot\n");
            rscript.append("if (show_axis_titles == \"all\") {\n" +
                    "    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + ylab(\"% retention\")\n" +
                    "} else if (show_axis_titles == \"none\") {\n" +
                    "    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + theme(axis.title=element_blank())\n" +
                    "} else { # show_axis_titles is \"outer\"\n");

            if (yAxisGenomes.contains(plotCounter)) {
                rscript.append("    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + ylab(\"% retention\")\n");
            } else {
                rscript.append("    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + theme(axis.title.y=element_blank())\n");
            }

            if (xAxisGenomes.contains(plotCounter)) {
                rscript.append("    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + xlab(\"Genomic position\")\n");
            } else if (whitexAxisGenomes.contains(plotCounter)) {
                rscript.append("    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + theme(axis.title.x=element_text(color = \"white\"))\n");
            } else {
                rscript.append("    plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + theme(axis.title.x=element_blank())\n");
            }
            rscript.append("}\n");

            rscript.append("plot_").append(genomeNr).append(" <- plot_").append(genomeNr).append(" + theme(legend.position = \"none\")\n\n\n");
            cowPlotLineBuilder.append("plot_").append(genomeNr).append(", ");
            cowPlotLegendBuilder.append("plot_").append(genomeNr).append("_legend, ");
        }

        rscript.append(cowPlotLineBuilder.toString().replaceFirst(".$","").replaceFirst(".$","")).append(")\n");
        rscript.append(cowPlotLegendBuilder.toString().replaceFirst(".$","").replaceFirst(".$","")).append(")\n");
        rscript.append("ggsave(combination_plot, file=\"").append(outputDirectory).append(homologsOrSyntelogs).append("_all_genomes.png\", ")
                .append("width = individual_plot_width * size_multiplier, height = individual_plot_height * size_multiplier, bg = \"white\")\n");
        rscript.append("ggsave(legends_combined, file=\"").append(outputDirectory).append(homologsOrSyntelogs).append("_all_legends.png\", bg = \"white\", width = 10, height = 10)\n");
        rscript.append("if (print_directory) {\n")
                .append("    cat('\\nPlots written to: ").append(outputDirectory).append("\\n\\n')\n")
                .append("}");

        writeStringToFile(rscript.toString(), outputDirectory + homologsOrSyntelogs + "_retention.R", false, false);
    }

    private HashSet<Integer> genomesfromSequenceIds(ArrayList<String> sequenceIdentifiers) {
        HashSet<Integer> genomeNumbers = new HashSet<>();
        for (String sequenceId : sequenceIdentifiers) {
            String[] seqIdArray = sequenceId.split("_");
            genomeNumbers.add(Integer.parseInt(seqIdArray[0]));
        }
        return genomeNumbers;
    }

    private ArrayList<Integer> plotsWithXAxis(int[] gridArray, HashSet<Integer> genomeNumbers) {
        ArrayList<Integer> plotsWithXaxis = new ArrayList<>();
        int remaining = genomeNumbers.size() % gridArray[0];
        int addAxis;
        int startLastrow;
        if (remaining == 0) {
            addAxis = 0;
            startLastrow = genomeNumbers.size()-gridArray[0];
        } else {
            addAxis = gridArray[0]-remaining;
            startLastrow = genomeNumbers.size()-remaining;
        }

        // add genomes that are on the bottom row
        for (int i = genomeNumbers.size(); i > startLastrow; i--) {
            plotsWithXaxis.add(i);
        }

        // add genomes that are second to last row
        for (int i = startLastrow; i > startLastrow- addAxis; i--) {
            plotsWithXaxis.add(i);
        }
        return plotsWithXaxis;
    }

    private ArrayList<Integer> plotsWithWhiteXAxis(int[] gridArray, HashSet<Integer> genomeNumbers) {
        ArrayList<Integer> plotsWithXaxis = new ArrayList<>();
        int remaining = genomeNumbers.size() %  gridArray[0];
        int addAxis = gridArray[0]-remaining;
        if (remaining == 0) {
            addAxis = 0;
        }
        int startLastrow = genomeNumbers.size()-remaining;
        for (int i = startLastrow- addAxis; i > startLastrow-gridArray[0]; i--) {
            plotsWithXaxis.add(i);
        }
        return plotsWithXaxis;
    }

    private ArrayList<Integer> plotsWithYAxis(int[] gridArray, HashSet<Integer> genomeNumbers){
        ArrayList<Integer> plotsWithYaXis = new ArrayList<>();
        for (int i = 1; i < 1000; i++) {
            if (i == 1) {
                plotsWithYaXis.add(1);
            } else {
                int position = ((i-1) * gridArray[0]) + 1;
                if (position >= genomeNumbers.size()){
                    break;
                }
                plotsWithYaXis.add(position);
            }
        }
        return plotsWithYaXis;
    }

    /**
     *
     * @param geneCountsPerPosition
     * @param referenceSeqId sequence identifier of the current reference sequence: 1_1 or 1_2
     * @param referenceMrnaNodes all mRNA nodes of current reference sequence
     * @param windowLength number of genes
     * @return
     */
    private HashMap <String, int[]> convertPositionsIntoWindows(int[][] geneCountsPerPosition,
                                                                      String referenceSeqId,
                                                                      ArrayList<Node> referenceMrnaNodes,
                                                                      int windowLength, int stepSize) {

        ArrayList<String> selectedSequencesList = new ArrayList<>(selectedSequences);
        int numberOfWindows = ((referenceMrnaNodes.size() - windowLength) / 10) +1;
        HashMap <String, int[]> sharedGenesPerWindow = new HashMap<>();
        for (int i = 0; i < geneCountsPerPosition.length ; i++) {
            String sequenceId = selectedSequencesList.get(i);
            if (referenceSeqId.equals(sequenceId)) {
                continue;
            }
            int[] countsPerWindow = new int[numberOfWindows];
            int windowNr = 0;
            int sharedGenes = 0; // is constantly updated in the loop below

            // go over all gene positions in steps of size set by 'stepSize'
            for (int j = 0; j < geneCountsPerPosition[i].length; j+= stepSize) {
                if (j + windowLength == geneCountsPerPosition[i].length) {
                    break;
                }
                if (windowNr == 0) {
                    for (int x = 0; x < windowLength; x++) {
                        sharedGenes += geneCountsPerPosition[i][x];
                    }
                    countsPerWindow[windowNr] = sharedGenes;
                } else {
                    int previousEndPosition = (j-1);
                    int previousStartPosition = (j-10);
                    int newStartPosition = j + (windowLength-10);
                    int newEndPosition = j + (windowLength-1);
                    if (newStartPosition > referenceMrnaNodes.size() || newEndPosition >= referenceMrnaNodes.size()) {
                        break; // window has reached the end
                    }

                    // remove the scores from previous window
                    for (int x = previousStartPosition; x <= previousEndPosition; x++) {
                        sharedGenes -= geneCountsPerPosition[i][x];
                    }

                    // include the scores from current window
                    for (int x = newStartPosition; x <= newEndPosition; x++) {
                        sharedGenes += geneCountsPerPosition[i][x];
                    }
                    countsPerWindow[windowNr] = sharedGenes;
                }
                windowNr ++;
            }
            sharedGenesPerWindow.put(sequenceId, countsPerWindow);
        }
        return sharedGenesPerWindow;
    }

    /**
     *
     * @param sharedGenesPerPosition
     * @param referenceMrnaNodes
     * @param mostSimilarSequences a list of sequence identifiers
     * @param homologsOrSyntelogs string that can be "homologs", "syntelogs"
     * @param referenceSeqId
     */
    private void createRetentionCsvOutput(HashMap<String, ArrayList<Node>> sharedGenesPerPosition,
                                                ArrayList<Node> referenceMrnaNodes, ArrayList<String> mostSimilarSequences,
                                                String homologsOrSyntelogs, String referenceSeqId) {

        StringBuilder header = new StringBuilder("reference gene position,gene start coordinate,");
        for (String sequenceId : mostSimilarSequences) {
            header.append(sequenceId).append(",");
        }
        String headerStr = header.toString().replaceFirst(".$","");

        StringBuilder sharedProteinIds = new StringBuilder();
        StringBuilder sharedNodeIds = new StringBuilder();

        sharedProteinIds.append(headerStr).append("\n");
        sharedNodeIds.append(headerStr).append("\n");

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 0; i < referenceMrnaNodes.size(); i++) {
                Node referenceMrnaNode = referenceMrnaNodes.get(i);
                int[] address = (int[]) referenceMrnaNode.getProperty("address");
                String proteinId = (String) referenceMrnaNode.getProperty("protein_ID");
                sharedProteinIds.append(i).append(",").append(address[2]).append(",").append(proteinId).append(",");
                sharedNodeIds.append(i).append(",").append(address[2]).append(",").append(referenceMrnaNode.getId()).append(",");
                for (String sequenceId : mostSimilarSequences) {
                    if (sharedGenesPerPosition.containsKey(i + "#" + sequenceId)) {
                        ArrayList<Node> sharedMrnaNodes = sharedGenesPerPosition.get(i + "#" + sequenceId);
                        StringBuilder proteinIdBuilder = new StringBuilder();
                        StringBuilder nodeIdBuilder = new StringBuilder();
                        for (Node mrnaNode : sharedMrnaNodes) {
                            String proteinId2 = (String) mrnaNode.getProperty("protein_ID");
                            proteinIdBuilder.append(proteinId2).append(" ");
                            nodeIdBuilder.append(mrnaNode.getId()).append(" ");
                        }
                        sharedProteinIds.append(proteinIdBuilder.toString().replaceFirst(".$", ""));
                        sharedNodeIds.append(nodeIdBuilder.toString().replaceFirst(".$", ""));
                    }
                    sharedProteinIds.append(",");
                    sharedNodeIds.append(",");
                }
                sharedProteinIds.append("\n");
                sharedNodeIds.append("\n");
            }
            tx.success();
        }
        String[] sequenceIdArray = referenceSeqId.split("_");
        writeStringToFile(sharedNodeIds.toString(), "retention/" + sequenceIdArray[0] + "/" + referenceSeqId + "/" + homologsOrSyntelogs + "_shared_gene_nodes.csv",
                true, false);
        writeStringToFile(sharedProteinIds.toString(), "retention/" + sequenceIdArray[0] + "/" + referenceSeqId + "/" + homologsOrSyntelogs + "_shared_gene_ids.csv",
                true, false);
    }

    /**
     * Select 20 sequences with the most shared genes to the reference (because there are only 20 colors available for the rscript).
     * @param sharedGenesPerWindow
     * @param allowedSequences sets the maximum number of sequences
     * @return
     */
    private ArrayList<String>[] findMostSimilarSequencesRetention(HashMap <String, int[]> sharedGenesPerWindow,
                                                                      int allowedSequences, int allowedSequencesPerGenome) {

        TreeSet<Integer> numberSharedGenes = new TreeSet<>();
        HashMap<Integer, ArrayList<String>> seq_per_shared_genes = new HashMap<>();

        // Sum the shared genes between sequences.
        for (String sequenceId : sharedGenesPerWindow.keySet()) {
            int[] genes_per_window = sharedGenesPerWindow.get(sequenceId);
            int sharedGenesCount = 0;
            for (int shared_genes : genes_per_window) {
                sharedGenesCount += shared_genes;
            }
            if (sharedGenesCount == 0) {
                continue;
            }

            numberSharedGenes.add(sharedGenesCount);
            seq_per_shared_genes.computeIfAbsent(sharedGenesCount, i -> new ArrayList<>()).add(sequenceId);
        }
        numberSharedGenes = (TreeSet<Integer>) numberSharedGenes.descendingSet(); // reverse the order of the treeset
        return mostSimilarRetentionSequences(numberSharedGenes, seq_per_shared_genes, allowedSequencesPerGenome, allowedSequences);
    }

    private ArrayList<String>[] mostSimilarRetentionSequences(TreeSet<Integer> numberSharedGenes,
                                                            HashMap<Integer, ArrayList<String>> seq_per_shared_genes,
                                                            int allowedSequencesPerGenome, int allowedSequences) {
        ArrayList<String> mostSimilarSequences = new ArrayList<>();
        ArrayList<String> missedSequences = new ArrayList<>();
        int addedSequences = 0;
        HashMap<String, ArrayList<String>> mostSimilarSequencesPerGenome = new HashMap<>();
        for (int sharedGenesCount : numberSharedGenes) {
            ArrayList<String> sequences = seq_per_shared_genes.get(sharedGenesCount);
            //System.out.println(sharedGenesCount + " " + sequences);
            for (String sequenceId : sequences) {
                if (addedSequences >= allowedSequences) {
                    missedSequences.add(sequenceId);
                    continue;
                }
                String[] sequenceArray = sequenceId.split("_"); // 1_1 becomes [1,1]
                ArrayList<String> sequenceIdentifiers = mostSimilarSequencesPerGenome.get(sequenceArray[0]);
                if (sequenceIdentifiers == null) {
                    sequenceIdentifiers = new ArrayList<>();
                }
                if (sequenceIdentifiers.size() < allowedSequencesPerGenome) {
                    sequenceIdentifiers.add(sequenceId);
                    addedSequences++;
                    mostSimilarSequences.add(sequenceId);
                } else {
                    missedSequences.add(sequenceId);
                }
                mostSimilarSequencesPerGenome.put(sequenceArray[0], sequenceIdentifiers);
            }
        }
        return new ArrayList[]{mostSimilarSequences, missedSequences};
    }

    /**
     * --input-sequences can be formatted in two ways.
     * 1. one sequence identifier per line
     * 1. all sequence identifier on 1 line, separated by a comma.
     */
    private ArrayList<String> readInputFileRetention(Path inputSequences) {
        ArrayList<String> correctInputSequences = new ArrayList<>();
        if (inputSequences == null) {
            System.out.print("\rNo file provided via --input-sequences, making plots for all " + selectedSequences.size() + " sequences.");
            if (selectedSequences.size() > 500) {
                System.out.println(" This is a lot, consider making a selection!");
            } else {
                System.out.println("");
            }
            return new ArrayList<>(selectedSequences);
        }

        // loop to validate if the included sequence identifiers are correct
        StringBuilder missingBuilder = new StringBuilder();

        int linesInFile = countLinesInFile(inputSequences);
        if (linesInFile == 1) {
            String[] lineArray = readOneLineFile(inputSequences).split(",");
            for (String sequenceId : lineArray) {
                verifySequenceId(sequenceId, missingBuilder, correctInputSequences);
            }
        } else {
            try {
                BufferedReader br = Files.newBufferedReader(inputSequences);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    String sequenceId = line.trim();
                    if (line.equals("")) { // skip empty lines
                        continue;
                    }
                    verifySequenceId(sequenceId, missingBuilder, correctInputSequences);
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read: {}", inputSequences);
                System.exit(1);
            }
        }

        if (correctInputSequences.isEmpty()) {
            Pantools.logger.error("None of the selected sequences were found in the pangenome.");
            System.exit(1);
        } else if (missingBuilder.length() > 0) {
            Pantools.logger.error("Sequences of --input-sequences not found in pangenome: {}", missingBuilder.toString().replaceFirst(".$", ""));
            System.exit(1);
        }
        System.out.println("\rA plot will be created for " + correctInputSequences.size() + " selected sequence(s).");
        return correctInputSequences;
    }

    private void verifySequenceId(String sequenceId, StringBuilder missingBuilder, ArrayList<String> correctInputSequences) {
        String[] sequenceIdArray = sequenceId.split("_");
        try {
            int genomeNr = Integer.parseInt(sequenceIdArray[0]);
            int sequenceNr = Integer.parseInt(sequenceIdArray[1]);
            int numberSequences = GENOME_DB.num_sequences[genomeNr];
            if (sequenceNr > numberSequences) {
                missingBuilder.append(sequenceId).append(",");
            } else {
                correctInputSequences.add(sequenceId);
            }
            if (!selectedSequences.contains(sequenceId)) {
                selectedSequences.add(sequenceId); // update the sequence selection
            }
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException(sequenceId + " is not a correct sequence identifier. Identifier should consist of: Genome number + \"_\" + Sequence number");
        }
    }



    /**
     * Retrieve gene addresses for the first and last gene of a window.
     *
     * First window: use first gene coordinate
     * last window: use last gene coordinate
     * all other windows: use genomic position precisely in the middle of the two genes
     */
     private ArrayList<Integer> genePositionsToAddresses(ArrayList<Node> referenceMrnaNodes, int windowLength, int stepSize) {
         int[] lastAddress = new int[0];
         ArrayList<Integer> geneAddresses = new ArrayList<>();
         try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
             for (int i = 0; i < referenceMrnaNodes.size(); i+= stepSize) {
                 Node mrnaNode = referenceMrnaNodes.get(i);
                 int[] address = (int[]) mrnaNode.getProperty("address");
                 int endPos = i + windowLength-1;
                 if (endPos > referenceMrnaNodes.size()-1){
                     break;
                 }
                 Node lastMrnaNode = referenceMrnaNodes.get(endPos);
                 lastAddress = (int[]) lastMrnaNode.getProperty("address");
                 if (i == 0) {
                     geneAddresses.add(address[3]);
                 } else {
                     geneAddresses.add((int) (address[3] + (((double)lastAddress[2]-address[3])/2)));
                 }
             }
             geneAddresses.set(geneAddresses.size()-1, lastAddress[3]);
             tx.success();
         }
         return geneAddresses;
     }

    /**
     *
     * @param referenceSequenceId a sequence identifier. Example: 1_1, 1_2, 1_3... 2_1,
     * @param windowLength
     * @param sharedGenesPerWindow
     * @param mostSimilarSequences list of sequence identifiers
     * @param homologsOrSyntelogs string that can be "homologs", "syntelogs"
     */
    private void createRetentionRScriptInput(String referenceSequenceId, int windowLength, HashMap <String, int[]> sharedGenesPerWindow,
                                            ArrayList<String> mostSimilarSequences, String homologsOrSyntelogs,
                                            ArrayList<Integer> geneAddressesForWindows) {

        String[] refSeqIdArray = referenceSequenceId.split("_");
        String phasingHeader = "";
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
            phasingHeader = ",Phasing";
        }
        StringBuilder input = new StringBuilder();
        for (String sequenceId : sharedGenesPerWindow.keySet()) {
            if (!mostSimilarSequences.contains(sequenceId)) {
                continue;
            }
            String[] seqIdArray = sequenceId.split("_");
            int[] geneCountsPerWindow = sharedGenesPerWindow.get(sequenceId);
            if (geneCountsPerWindow.length == 1) { // unable to plot a line with only 1 value
                continue;
            }

            String previousPercentage = "0.0";
            String previousLine = "";
            for (int i = 0; i < geneCountsPerWindow.length ; i++) {
                String percentage = get_percentage_str(geneCountsPerWindow[i], windowLength, 1);
                String phasingId = "";
                if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
                    String[] phasingInfo = phasingInfoMap.get(sequenceId); // example [1, B, 1B, 1_B]
                    if (phasingInfo == null) {
                        phasingId += ",unphased";
                    } else {
                        phasingId += ',' + phasingInfo[1];
                    }
                }

                int position;
                if (i == 0) {
                    position = 1;
                } else {
                    position = i * 10;
                }
                String line = position + "," + geneAddressesForWindows.get(i) + "," + percentage + "," + seqIdArray[0]
                        + "," + sequenceId + phasingId + "\n";
                if (previousPercentage.equals("0.0") && percentage.equals("0.0")) { // do not add more than two consecutive lines with zero percentage shared
                    previousLine = line;
                    previousPercentage = percentage;
                    continue;
                } else if (previousPercentage.equals("0.0")) { // and percentage is not 0.0
                    input.append(previousLine); // add the last line before that still was a zero
                }
                input.append(line);
                previousPercentage = percentage;
                previousLine = line;
            }
        }

        if (input.length() > 0) {
            writeStringToFile("gene_position,gene_address,retention,Genome,Sequence" + phasingHeader + "\n" + input,
                    "retention/" + refSeqIdArray[0] + "/" + referenceSequenceId + "/" + homologsOrSyntelogs + "_retention.csv",
                    true, false);
        }
    }

    private String determineCombiPlotMultiplier(){
        String defaultMultiplier = "2";
        if (selectedGenomes.size() > 30) {
            defaultMultiplier = "2.5";
        } if (selectedGenomes.size() > 49) {
            defaultMultiplier = "3";
        }
        return defaultMultiplier;
    }

    /**
     * All jobs to execute are placed in 'stringQueue'
     */
    private void createOrRunRetentionRscripts() {
        String defaultMultiplier = determineCombiPlotMultiplier();
        int initialSize = stringQueue.size();
        int counter = 0;
        StringBuilder shellScript = new StringBuilder("#!/bin/sh\n\n"); // Y_LOWER=0 // Y_HIGHER=100
        shellScript.append("#Arguments to control the size of text and of the plot are given via the command line.\n")
                .append("#This was done to prevent this Rscript from becoming enormous as each plot requires some custom code\n\n");
        shellScript.append("SHOW_TITLES=\"all\" # \"all\" is default, can also be set to \"none\"\n")
                .append("SHOW_AXIS_TITLES=\"outer\" # \"outer\" is default, can also be set to \"all\" or \"none\"\n")
                .append("Y_LOWER_LIMIT=0\n")
                .append("Y_HIGHER_LIMIT=100\n")
                .append("TEXT_SIZE=15\n")
                .append("INDIVIDUAL_PLOT_HEIGHT=5\n")
                .append("INDIVIDUAL_PLOT_WIDTH=7.5\n").append("COMBI_PLOT_SIZE_MULTIPLIER=").append(defaultMultiplier).append("\n")
                .append("PRINT_DIRECTORY=FALSE\n\n");

        while (stringQueue.size() > 0) {
            counter++;
            if (counter % 10 == 0 || counter == initialSize || counter == 1) {
                shellScript.append("printf \"\\rPlot ").append(counter).append("/").append(initialSize).append("\"\n");
            }
            try {
                shellScript.append(stringQueue.take()).append(" $SHOW_TITLES $SHOW_AXIS_TITLES $Y_LOWER_LIMIT " +
                        "$Y_HIGHER_LIMIT $TEXT_SIZE $INDIVIDUAL_PLOT_HEIGHT $INDIVIDUAL_PLOT_WIDTH $COMBI_PLOT_SIZE_MULTIPLIER $PRINT_DIRECTORY").append("\n");
            } catch (InterruptedException e) {
                break;
            }
        }
        shellScript.append("printf \"\\r\\n\"");
        writeStringToFile(shellScript.toString(), "retention/retention_rscripts.sh", true, false);
    }

    /**
     * Check which of the mRNA's of the reference sequence are syntenic to any other mRNA
     * @param referenceMrnaNodes all mRNA nodes of current reference sequence
     * @param sharedGenesPerPosition key is mrna position + "#" + sequence identifier, value is mRNA nodes
     * @return
     */
    private int[][] findSyntenicRetention(ArrayList<Node> referenceMrnaNodes,
                                                HashMap<String, ArrayList<Node>> sharedGenesPerPosition) {

        ArrayList<String> selectedSequencesList = new ArrayList<>(selectedSequences);
        int[][] geneCountsPerPosition = new int[selectedSequences.size()][referenceMrnaNodes.size()];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 0; i < referenceMrnaNodes.size(); i++) {
                Node mrnaNode = referenceMrnaNodes.get(i);
                Iterable<Relationship> syntenicRelationships = mrnaNode.getRelationships(RelTypes.is_syntenic_to);
                for (Relationship part_of_rel : syntenicRelationships) {
                    long mrnaNode2Id = part_of_rel.getEndNodeId();
                    Node mrnaNode2 = part_of_rel.getEndNode();
                    if (mrnaNode2Id == mrnaNode.getId()) {
                        mrnaNode2 = part_of_rel.getStartNode();
                    }
                    int[] address2 = (int[]) mrnaNode2.getProperty("address");
                    String sequenceId = address2[0] + "_" + address2[1];
                    int index = selectedSequencesList.indexOf(sequenceId);
                    if (index == -1) {
                        continue;
                    }
                    geneCountsPerPosition[index][i] = 1;
                    sharedGenesPerPosition.computeIfAbsent(i + "#" + sequenceId, s -> new ArrayList<>()).add(mrnaNode2);
                }
            }
            tx.success();
        }
        return geneCountsPerPosition;
    }

    /**
     * Check which of the mRNA's of the reference sequence are homologous to any other mRNA
     * @param referenceMrnaNodes all mRNA nodes of current reference sequence
     * @param referenceSeqId sequence identifier of the current reference sequence: 1_1 or 1_2
     * @param sharedGenesPerPosition key is mrna position + "#" + sequence identifier, value is mRNA nodes
     * @return
     */
    private int[][] findHomologyRetention(ArrayList<Node> referenceMrnaNodes, String referenceSeqId,
                                                HashMap <String, ArrayList<Node>> sharedGenesPerPosition) {

        ArrayList<String> selectedSequencesList = new ArrayList<>(selectedSequences);
        int[][] geneCountsPerPosition = new int[selectedSequences.size()][referenceMrnaNodes.size()];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> homologyNodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            while (homologyNodes.hasNext()) {
                Node homologyNode = homologyNodes.next();
                Iterable<Relationship> relations = homologyNode.getRelationships(RelTypes.has_homolog);
                HashSet<Node> sharedNodes = new HashSet<>();
                ArrayList<Integer> foundPositions = new ArrayList<>();
                for (Relationship rel : relations) {
                    Node mrnaNode = rel.getEndNode();
                    if (!mrnaNode.hasProperty("longest_transcript")) {
                        continue;
                    }
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    int genome_nr = address[0];
                    int sequence_nr = address[1];
                    if (skip_seq_array[genome_nr-1][sequence_nr-1]) {
                        continue;
                    }
                    String sequenceId = address[0] + "_" + address[1];
                    if (sequenceId.equals(referenceSeqId)) { // this mrna is from the selected reference sequence
                        if (!referenceMrnaNodes.contains(mrnaNode)) {
                            continue;
                        }
                        int position = referenceMrnaNodes.indexOf(mrnaNode);
                        foundPositions.add(position);
                    } else {
                        sharedNodes.add(mrnaNode);
                    }
                }

                // after going all the mRNAs of the homology_group
                for (int position : foundPositions) {
                    for (Node mrnaNode : sharedNodes) {
                        int[] address = (int[]) mrnaNode.getProperty("address");
                        int index = selectedSequencesList.indexOf(address[0] + "_" + address[1]);
                        if (index == -1) {
                            continue;
                        }
                        geneCountsPerPosition[index][position] = 1;
                        sharedGenesPerPosition
                                .computeIfAbsent(position + "#" + address[0] + "_" + address[1], s -> new ArrayList<>())
                                .add(mrnaNode);
                    }
                }
            }
            tx.success();
        }
        return geneCountsPerPosition;
    }

    private int[] calculateGridWith(int genomes) {
        int width = 1;
        int plots = 0;
        while (plots < genomes) {
            plots = (width*width);
            if (plots >= genomes){
                break;
            }
            width++;
        }
        double rows = Math.ceil((double) genomes /  width);
        return new int[]{width, (int) rows};
    }

    /**
     * max 21 colors allowed, afterwards all are grey
     * @param mostSimilarSequences list of sequence identifiers
     * @return
     */
    private HashMap<String, String> determineDistinctColors(ArrayList<String> mostSimilarSequences) {
        int[] countPerGenome = new int[total_genomes];
        HashMap<String, String> finalColorsPerGenome = new HashMap<>();
        HashMap<String, StringBuilder> colorsPerGenome = new HashMap<>();
        for (String sequenceId : mostSimilarSequences) {
            String[] seqIdArray = sequenceId.split("_");
            int genomeNr = Integer.parseInt(seqIdArray[0]);
            String genomeNrStr = seqIdArray[0];
            int occurrence = countPerGenome[genomeNr-1];
            StringBuilder colorBuilder, legendBuilder;
            if (!colorsPerGenome.containsKey(genomeNrStr)) {
                colorBuilder = new StringBuilder("scale_color_manual(values=c(");
                legendBuilder = new StringBuilder("labels = c(");
            } else {
                colorBuilder = colorsPerGenome.get(genomeNrStr);
                legendBuilder = colorsPerGenome.get(genomeNr + "_legend");
            }
            if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
                String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
                if (phasingInfo == null) {
                    legendBuilder.append("\"").append(sequenceId).append(" unphased\", ");
                } else {
                    legendBuilder.append("\"").append(sequenceId).append(" ").append(phasingInfo[3]).append("\", ");
                }
                colorsPerGenome.put(genomeNrStr + "_legend", legendBuilder);
            }
            String color = "";
            if (occurrence < 22) {
                color = COLOR_CODES2[occurrence];
            } else {
                color = "#737373";
            }
            colorBuilder.append("\"").append(sequenceId).append("\" = ").append("\"").append(color).append("\", ");
            colorsPerGenome.put(genomeNrStr, colorBuilder);
            countPerGenome[genomeNr-1]++;
        }

        for (String genomeKey : colorsPerGenome.keySet()) {
            if (genomeKey.endsWith("_legend")) {
                continue;
            }
            StringBuilder colorBuilder = colorsPerGenome.get(genomeKey);
            String colorsStr = colorBuilder.toString().replaceFirst(".$","").replaceFirst(".$","");
            if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
                StringBuilder legendBuilder = colorsPerGenome.get(genomeKey + "_legend");
                finalColorsPerGenome.put(genomeKey, colorsStr +
                        "), " + legendBuilder.toString().replaceFirst(".$","").replaceFirst(".$","")  +"))");
            } else {
                finalColorsPerGenome.put(genomeKey, colorsStr + "))");
            }
        }
        return finalColorsPerGenome;
    }

    /**
     * @param mostSimilarSequences a list of sequence identifiers
     * @return
     */
    private HashMap<String, String> determineShadeColors(ArrayList<String> mostSimilarSequences, String homologsOrSyntelogs, String outputDirectory) {
        HashMap<String, String> finalColorsPerGenome = new HashMap<>();
        if (phasingInfoMap.isEmpty() || !PHASED_ANALYSIS) {
            return finalColorsPerGenome;
        }

        HashMap<String, ArrayList<String>> sequencesPerChromosome  = new HashMap<>(); // key is genome number + "_" + chromosome number
        ArrayList<Integer> chromosomeNrs = new ArrayList<>(); // order of chromosomes is important
        for (String sequenceId : mostSimilarSequences) {
            String[] seqIdArray = sequenceId.split("_");
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            if (phasingInfo == null) {
                sequencesPerChromosome.computeIfAbsent(seqIdArray[0] + "_0", s -> new ArrayList<>()).add(sequenceId);
            } else {
                int chromosomeNr = 0;
                try {
                    chromosomeNr = Integer.parseInt(phasingInfo[0]);
                } catch (NumberFormatException nfe) {
                    // sequence does not belong to any chromosome, do nothing
                    continue;
                }
                if (chromosomeNr == 0) {
                    continue;
                }
                if (!chromosomeNrs.contains(chromosomeNr)) {
                    chromosomeNrs.add(chromosomeNr);
                }
                sequencesPerChromosome.computeIfAbsent(seqIdArray[0] + "_" + chromosomeNr, s -> new ArrayList<>())
                        .add(sequenceId);
            }
        }

        HashMap<String, StringBuilder> colorsPerGenome = new HashMap<>();
        HashMap<String, String> legendValues = new HashMap<>();
        for (String chromosomeKey : sequencesPerChromosome.keySet()) { // key is genome number + "_" + chromosome number
            ArrayList<String> sequences = sequencesPerChromosome.get(chromosomeKey);
            String[] chromosomeArray = chromosomeKey.split("_");
            int chromosomeNr = Integer.parseInt(chromosomeArray[1]);
            int index = chromosomeNrs.indexOf(chromosomeNr);
            String color = "", legend = "";
            String[] colorAndLegend = new String[2]; // will replace the color and legend strings
            if (index == -1) {
                // grey for unphased
                StringBuilder colorBuilder = new StringBuilder();
                StringBuilder legendBuilder = new StringBuilder();
                for (String sequenceId : sequences) {
                    colorBuilder.append("\"").append(sequenceId).append("\" = ").append("\"#999999\", ");
                    legendBuilder.append("\"").append(sequenceId).append(" Unphased\", ");
                }
                color = colorBuilder.toString();
                legend = legendBuilder.toString();
                legendValues.put("unphased","#999999");
            } else if  (index == 0) {
                colorAndLegend = determineShade(sequences, legendValues, "green");
                //colorAndLegend = determineGreenShade(sequences, legendValues);
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            } else if  (index == 1) {
                //colorAndLegend = determineRedShade(sequences, legendValues);
                colorAndLegend = determineShade(sequences, legendValues, "red");
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            } else if (index == 2) {
                colorAndLegend = determineShade(sequences, legendValues, "blue");
                //colorAndLegend = determineBlueShade(sequences, legendValues);
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            } else if (index == 3) {
                colorAndLegend = determineShade(sequences, legendValues, "purple");
                //colorAndLegend = determinePurpleShade(sequences, legendValues);
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            }  else if (index == 4) {
                colorAndLegend = determineShade(sequences, legendValues, "orange");
                //colorAndLegend = determineOrangeShade(sequences, legendValues);
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            } else if (index == 5) {
                colorAndLegend = determineShade(sequences, legendValues, "yellow");
                //colorAndLegend = determineYellowShade(sequences, legendValues);
                color = colorAndLegend[0];
                legend = colorAndLegend[1];
            } else {
                StringBuilder colorBuilder = new StringBuilder();
                StringBuilder legendBuilder = new StringBuilder();
                for (String sequenceId : sequences) {
                    colorBuilder.append("\"").append(sequenceId).append("\" = ").append("\"#737373\", ");
                    legendBuilder.append("\"").append(sequenceId).append(" Chr ").append(chromosomeArray[1]).append(" Too many colors\", ");
                }
                color = colorBuilder.toString();
                legend = legendBuilder.toString();
                legendValues.put("too_many_colors", "#737373");
            }

            StringBuilder legendBuilder;
            if (!colorsPerGenome.containsKey(chromosomeArray[0] + "_legend")) {
                legendBuilder = new StringBuilder("labels = c(");
            } else {
                legendBuilder = colorsPerGenome.get(chromosomeArray[0] + "_legend");
            }
            legendBuilder.append(legend);
            colorsPerGenome.put(chromosomeArray[0] + "_legend", legendBuilder);

            StringBuilder colorBuilder;
            if (!colorsPerGenome.containsKey(chromosomeArray[0])) {
                colorBuilder = new StringBuilder("scale_color_manual(values=c(");
            } else {
                colorBuilder = colorsPerGenome.get(chromosomeArray[0]);
            }
            colorBuilder.append(color);
            colorsPerGenome.put(chromosomeArray[0], colorBuilder);
        }

        // combine the legend with the colors
        for (String genomeKey : colorsPerGenome.keySet()) {
            if (genomeKey.endsWith("_legend")) {
                continue;
            }
            String colorStr = colorsPerGenome.get(genomeKey).toString().replaceFirst(".$","").replaceFirst(".$","");
            String legendStr = colorsPerGenome.get(genomeKey + "_legend").toString().replaceFirst(".$","").replaceFirst(".$","");
            finalColorsPerGenome.put(genomeKey, colorStr + "), " + legendStr + "))");
        }

        String plotlegendStr = createPhasingRetentionLegend(legendValues, homologsOrSyntelogs, outputDirectory);
        finalColorsPerGenome.put("legend", plotlegendStr);
        return finalColorsPerGenome;
    }

    /**
     * The legend for all plots combined
     *
     * @param legendValues
     * @return
     */
    private String createPhasingRetentionLegend(HashMap<String, String> legendValues, String homologsOrSyntelogs, String outputDirectory) {
        if (phasingInfoMap.isEmpty() || !PHASED_ANALYSIS) {
            return "";
        }
        StringBuilder legendPartOne = new StringBuilder("scale_color_manual(name= \"Chromosome coloring\", breaks=c(");
        StringBuilder legendPartTwo = new StringBuilder(",\n    values=c(");
        String[] haplotypes = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "unphased"};
        for (int i = 0; i < 100; i++) { // i represents a chromosome number
            for (int j = 0; j < 9; j++) { //  haplotype letter
                String chromosomeKey = i + "_" + haplotypes[j];
                if (legendValues.containsKey(chromosomeKey)) {
                    legendPartOne.append("\"").append(chromosomeKey).append("\", ");
                    legendPartTwo.append("\"").append(chromosomeKey).append("\"=").append(legendValues.get(chromosomeKey)).append(", ");
                }
            }
        }
        if (legendValues.containsKey("unphased")) {
            legendPartOne.append("\"").append("unphased").append("\", ");
            legendPartTwo.append("\"").append("unphased").append("\"= \"#").append("999999").append("\", ");
        }

        if (legendValues.containsKey("too_many_colors")) {
            legendPartOne.append("\"").append("too_many_colors").append("\", ");
            legendPartTwo.append("\"").append("too_many_colors").append("\"= \"#").append("737373").append("\", ");
        }

        return "create_chromosome_color_legend <- function() {\n" +
                "    plot_for_legend <- ggplot(data=df, aes(x=gene_address, y=retention, color=Sequence)) +\n" +
                "    geom_line(size=2) +\n" +
                "    theme_classic() +\n" +
                "    " + legendPartOne.toString().replaceFirst(".$", "").replaceFirst(".$", "") + ")" +
                legendPartTwo.toString().replaceFirst(".$", "").replaceFirst(".$", "") + "))\n" +
                "    legend <- get_legend(plot_for_legend)\n" +
                "    grid.newpage()\n" +
                "    save_legend <- grid.arrange(legend)\n" +
                "    ggsave(filename=\"" + outputDirectory + homologsOrSyntelogs + "_legend.png\", plot=save_legend, width=5, height=5)\n" +
                "}\n" +
                "create_chromosome_color_legend()\n\n";
    }

    private String[] determineShade(ArrayList<String> sequenceIdentifiers, HashMap<String, String> legendValues, String color) {
        String[] phases = new String[] {"unphased","A","B","C", "D", "E", "F", "G", "H"};
        StringBuilder colorBuilder = new StringBuilder();
        StringBuilder legendBuilder = new StringBuilder();
        for (String sequenceId : sequenceIdentifiers) {
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            int index = Arrays.asList(phases).indexOf(phasingInfo[1]);
            if (index == 0) { // unphased
                colorBuilder.append("\"").append(sequenceId).append("\" = ").append(color).append("[9], ");
                legendValues.put(phasingInfo[3], color + "[9]");
            } else {
                colorBuilder.append("\"").append(sequenceId).append("\" = ").append(color).append("[").append(index).append("], ");
                legendValues.put(phasingInfo[3], color + "[" + index + "]");
            }
            legendBuilder.append("\"").append(sequenceId).append(" Chr ").append(phasingInfo[2].replace("un"," un")).append("\", ");
        }
        return new String[]{colorBuilder.toString(), legendBuilder.toString()};
    }
}
