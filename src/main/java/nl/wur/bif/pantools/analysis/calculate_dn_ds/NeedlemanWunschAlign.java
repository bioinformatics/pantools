package nl.wur.bif.pantools.analysis.calculate_dn_ds;

// https://zhanglab.ccmb.med.umich.edu/NW-align/ 
//--------------------------------------------------------------------------------------------------
// 
// Description: Implementation of Needleman-Wunsch global alignment.
// This code is written by Ren-Xiang Yan in China Agricultural University and is originally based on 
// the fortran implementation from Dr. Yang Zhang (http://zhanglab.ccmb.med.umich.edu/NW-align/).
// Last update is in 2010/08/14. 
//
//  Usage:
//      java -jar NWAlign.jar F1.fasta F2.fasta  (align two sequences in fasta file)
//		java -jar NWAlign.jar F1.pdb F2.pdb    1 (align two sequences in PDB file)
//		java -jar NWAlign.jar F.fasta F.pdb  2 (align sequences 1 in fasta and 1 in pdb)
//		java -jar NWAlign.jar GKDGL EVADELVSE    3 (align two sequences in plain text)
//		java -jar NWAlign.jar GKDGL F.fasta  4 (align sequences 1 in text and 1 in fasta)
//		java -jar NWAlign.jar GKDGL F.pdb    5 (align sequences 1 in text and 1 in pdb)
//  
//   Note: You also could complied the code by yourself.
//         Decompress the NWAlign.jar file and you can get the source code in the NWAlign folder.
//   The program can be compiled by 
//              javac NWAlign.java
//   Then you could use the program by the following commands:
//		java NWAlign F1.fasta F2.fasta  (align two sequences in fasta file)
//		java NWAlign F1.pdb F2.pdb    1 (align two sequences in PDB file)
//		java NWAlign file1.fasta file2.pdb  2 (align sequences 1 in fasta and 1 in pdb)
//		java NWAlign GKDGL EVADELVSE    3 (align two sequences in plain text)
//		java NWAlign GKDGL F.fasta  4 (align sequences 1 in text and 1 in fasta)
//		java NWAlign GKDGL F.pdb    5 (align sequences 1 in text and 1 in pdb)            
//-----------------x-------------------x-------------------x--------------------------------------------

import nl.wur.bif.pantools.utils.FileUtils;

public class NeedlemanWunschAlign {
	private final String scoringMatrix;

	public NeedlemanWunschAlign(String scoringMatrix) {
		this.scoringMatrix = scoringMatrix;
	}

	public String[] NeedlemanWunsch2(String f1, String f2, int gap_open, int gap_extn) {
		int[][] imut = FileUtils.loadScoringMatrix(scoringMatrix);
		f1 = "*" + f1;                                     // Add a '*' character in the head of a sequence and this can make java code much more consistent with orginal fortran code.
		f2 = "*" + f2;                                     // Use 1 to represent the first position of the sequence in the original fortran code,and 1 stand for the second position in java code. Here, add a '*' character in the head of a sequence could make 1 standard for the first postion of thse sequence in java code.     
		char[] seq1 = f1.toCharArray();
		char[] seq2 = f2.toCharArray();
		int i,j;

	 	int[][] score = new int[f1.length()][f2.length()];		// score[i][j] start for the alignment score that align ith position of the first sequence to the jth position of the second sequence.
		for (i=1;i<f1.length();i++) {
			for (j=1;j<f2.length();j++) {
				score[i][j] = imut[seq1[i]][seq2[j]];
			}
		}

		int[] j2i = new int[f2.length()+1];
		for (j=1;j<f2.length();j++) {
			j2i[j] = -1; // !all are not aligned
		}		
		
		int[][] val = new int[f1.length()+1][f2.length()+1];  // val[][] was assigned as a global variable, and the value could be printed in the final.
		int[][] idir = new int[f1.length()+1][f2.length()+1];
		int[][] preV = new int[f1.length()+1][f2.length()+1];
		int[][] preH = new int[f1.length()+1][f2.length()+1];
		int D,V,H;

		// If you want to use alternative implementation of Needleman-Wunsch dynamic program , you can assign "false" value to the "standard" variable.

		////////////////////////////////////////////////////////////////////////////////
		//	     This is a standard Needleman-Wunsch dynamic program (by Y. Zhang 2005).
		//	     1. Count multiple-gap.
		//	     2. The gap penality W(k)=Go+Ge*k1+Go+Ge*k2 if gap open on both sequences
		//	     idir[i][j]=1,2,3, from diagonal, horizontal, vertical
		//	     val[i][j] is the cumulative score of (i,j)
		////////////////////////////////////////////////////////////////////////////////

		int[][] jpV = new int[f1.length()+1][f2.length()+1];
		int[][] jpH = new int[f1.length()+1][f2.length()+1];
		val[0][0]=0;
		val[1][0] =gap_open;
		for (i=2;i<f1.length();i++){
			val[i][0] = val[i-1][0]+gap_extn;
		}
		for (i=1;i<f1.length();i++) {
			preV[i][0] = val[i][0]; // not use preV at the beginning
			idir[i][0] = 0;         // useless
			jpV[i][0] = 1;          // useless
			jpH[i][0] = i;          // useless
		}
		val[0][1]=gap_open;
		for (j=2;j<f2.length();j++){
			val[0][j]=val[0][j-1]+gap_extn;
		}
		for (j=1;j<f2.length();j++)
		{
			 preH[0][j] = val[0][j];
			 idir[0][j] = 0;
			 jpV[0][j] = j;
			 jpH[0][j] = 1;
		}

		// DP ------------------------------>
		for (j=1;j<f2.length();j++) {
			for (i=1;i<f1.length();i++) {
				// D=VAL(i-1,j-1)+SCORE(i,j)--------------->
				D = val[i-1][j-1] + score[i][j];	// from diagonal, val(i,j) is val(i-1,j-1)

				//	H=H+gap_open ------->
				jpH[i][j] = 1;
				int val1 = val[i-1][j] + gap_open;  // gap_open from both D and V
				int val2 = preH[i-1][j] + gap_extn; // gap_extn from horizontal
				if(val1>val2) {   // last step from D or V
					H = val1;
				}
				else {            // last step from H
					H = val2;
					if(i > 1) {
						jpH[i][j] = jpH[i-1][j] + 1;  // record long-gap
					}
				}

				//	V=V+gap_open --------->
				jpV[i][j] = 1;
				val1 = val[i][j-1] + gap_open;
				val2 = preV[i][j-1] + gap_extn;
				if(val1>val2) {
					V = val1;
				}
				else {
					V = val2;
					if(j > 1) {
						jpV[i][j] = jpV[i][j-1] + 1;   // record long-gap
					}
				}

				preH[i][j] = H; // unaccepted H
				preV[i][j] = V;	// unaccepted V
				if((D>H)&&(D>V)) {
					idir[i][j]=1;
					val[i][j]=D;
				}
				else if(H > V) {
					idir[i][j] = 2;
					val[i][j] = H;
				}
				else {
					 idir[i][j] = 3;
					  val[i][j] = V;
				}
			}
		}

		//  tracing back the pathway
		i = f1.length()-1;
		j = f2.length()-1;
		while((i>0)&&(j>0)) {
			 if(idir[i][j]==1)  {       // from diagonal
				j2i[j] = i;
				i=i-1;
				j=j-1;
			 }
			 else if(idir[i][j]==2) {  // from horizonal
				 int temp1 = jpH[i][j];
				 for (int me=1;me<=temp1;me++) {    //  In the point view of a programer,
					if(i>0) {                       //  you should not use the  "for (int me=1;me<=jpH[i][j];me++)".
						i = i - 1;                  //  If you use up sentence,the value of jpH[i][j] is changed when variable i changes.
					}	                            //  So the value of jpH[i][j] was assigned to the value temp1 and use the sentence "for (int me=1;me<=temp1;me++)" here.
				 }
			 }
			 else {
				 int temp2 = jpV[i][j];
				 for (int me=1;me<=temp2;me++) {   //  In the point view of a programmer,
					 if(j>0) {                     //  you should not use the  "for (int me=1;me<=jpV[i][j];me++)".
						 j = j - 1;                //  Because when variable i change, the jpV[i][j] employed here is also change.
					 }	                           //  So the value of jpV[i][j] was assigned to the value temp2 and use the sentence "for (int me=1;me<=temp2;me++)" here.
				 }
			 }
		}

		// calculate sequence identity			
		int L_id=0;
	    int L_ali=0;
	    for (j=1;j<f2.length();j++) {
			if (j2i[j] > 0) {
				i = j2i[j];
				L_ali = L_ali + 1;
				if (seq1[i] == seq2[j]) {
					L_id = L_id + 1;
				}
			}
		}
	    
	    // output aligned sequences	    
	    char[] sequenceA = new char[f1.length()+f2.length()];
	    char[] sequenceB = new char[f1.length()+f2.length()];
	    char[] sequenceM = new char[f1.length()+f2.length()];	    
	    int k = 0;
	    i=1;
	    j=1;
		while ((i <= (f1.length() - 1)) || (j <= (f2.length() - 1))) {
			if ((i > (f1.length() - 1)) && (j < (f2.length() - 1))) {     // unaligned C on 1
				k = k + 1;
				sequenceA[k] = '-';
				sequenceB[k] = seq2[j];
				sequenceM[k] = ' ';
				j = j + 1;
			} else if ((i < (f1.length() - 1)) && (j > (f2.length() - 1))) { // unaligned C on 2
				k = k + 1;
				sequenceA[k] = seq1[i];
				sequenceB[k] = '-';
				sequenceM[k] = ' ';
				i = i + 1;
			} else if (i == j2i[j]) { // if align
				k = k + 1;
				sequenceA[k] = seq1[i];
				sequenceB[k] = seq2[j];
				if (seq1[i] == seq2[j]) { // identical
					sequenceM[k] = '*';
				} else {
					sequenceM[k] = ' ';
				}
				i = i + 1;
				j = j + 1;
			} else if (j2i[j] < 0) {  // gap on 1
				k = k + 1;
				sequenceA[k] = '-';
				sequenceB[k] = seq2[j];
				sequenceM[k] = ' ';
				j = j + 1;
			} else if (j2i[j] >= 0) { // gap on 2
				k = k + 1;
				sequenceA[k] = seq1[i];
				sequenceB[k] = '-';
				sequenceM[k] = ' ';
				i = i + 1;
			}
		}
		StringBuilder seq_a = new StringBuilder();
		StringBuilder seq_b = new StringBuilder();
		StringBuilder scores = new StringBuilder();
		
	    for (i=1;i<=k;i++) {
			seq_a.append(sequenceA[i]);
	    }
	    for (i=1;i<=k;i++) {
			scores.append(sequenceM[i]);
	    }
		for (i=1;i<=k;i++) {
			seq_b.append(sequenceB[i]);
	    }
		return new String[]{seq_a.toString(), seq_b.toString(), scores.toString() };
	}
}
