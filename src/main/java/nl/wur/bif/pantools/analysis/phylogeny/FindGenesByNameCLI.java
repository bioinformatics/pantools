package nl.wur.bif.pantools.analysis.phylogeny;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Find your genes of interest in the pangenome by using the gene name and extract the nucleotide and protein sequence.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "find_genes_by_name", sortOptions = false)
public class FindGenesByNameCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-g", "--genes"}, required = true)
    void setGenes(String value) {
        genes = Arrays.asList(value.split(","));
    }
    List<String> genes;

    @Option(names = "--extensive")
    boolean extensive;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, Arrays.asList(this, selectGenomes));

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead
        phylogeny.findGenesByName(genes, false, extensive);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
    }

}
