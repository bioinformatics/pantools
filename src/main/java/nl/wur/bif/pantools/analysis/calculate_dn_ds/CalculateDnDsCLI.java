package nl.wur.bif.pantools.analysis.calculate_dn_ds;

import nl.wur.bif.pantools.utils.GraphUtils;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Spec;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.nio.file.Path;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools calculate_dn_ds
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "calculate_dn_ds", sortOptions = false, abbreviateSynopsis = true)
public class CalculateDnDsCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"-H", "--homology-file"})
    @InputFile(message = "{file.homology}")
    private Path homologyFile;

    @Option(names = {"--syntelogs"})
    private boolean syntelogs;

    @Option(names = {"--phasing"})
    private boolean phasing;

    @Option(names = "--scoring-matrix")
    @ScoringMatrix(message = "{scoring_matrix}")
    String scoringMatrix;

    @Option(names = {"--allow-zeros"})
    private boolean allowZero;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);
        pantools.setPangenomeGraph();
        final String homologsOrSyntelogs = syntelogs ? "syntelogs" : "homologs";
        setGlobalParameters(); //TODO: use local parameters instead
        if (!syntelogs) setAlignmentMode();
        new CalculateDnDs().calculateDnDs(homologsOrSyntelogs, allowZero, scoringMatrix);
        return 0;
    }


    private void setAlignmentMode() {
        // Assert that the database contains trimmed protein alignments per homology-group
        if (!GraphUtils.containsAlignmentOutput("per-group", "prot", false)) {
            Pantools.logger.error("Missing trimmed protein alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-protein' first.");
            throw new ParameterException(spec.commandLine(), "Missing trimmed protein alignments per homology group");
        }
        // Assert that the database contains untrimmed nucleotide alignments per homology-group
        if (!GraphUtils.containsAlignmentOutput("per-group", "nuc", false)) {
            Pantools.logger.error("Missing nucleotide alignments per homology group.");
            Pantools.logger.error("Please run 'pantools msa --align-nucleotide' first.");
            throw new ParameterException(spec.commandLine(), "Missing nucleotide alignments per homology group");
        }
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        THREADS = threadNumber.getnThreads();
        PHASED_ANALYSIS = phasing;
        if (homologyFile != null) {
            SELECTED_HMGROUPS = homologyFile.toString();
        }
    }
}
