package nl.wur.bif.pantools.analysis.blast;

import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.Utils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 * holds three command line functions: blast, best_BLAST_hits, blast_cog
 */
public class Blast {


    /**
     */
    public void blast(Path fastaFile, int alignmentThreshold, int minimumIdentity, boolean rebuildDatabase, String blastProgram) {
        Pantools.logger.info("Run BLAST against the pangenome.");
        check_if_program_exists_stdout("blastn -h", 100, "blastn", true); // check if program is set to $PATH
        check_if_program_exists_stdout("makeblastdb -h", 100, "makeblastdb", true); // check if program is set to $PATH
        Pantools.logger.info("--alignment-threshold set to {}% of the original query length", alignmentThreshold);
        Pantools.logger.info("--minimum-identity set to {}%", minimumIdentity);

        create_directory_in_DB("BLAST");
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success(); // transaction successful, commit changes
        }
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(0, false);
        PhasedFunctionalities pf = new PhasedFunctionalities();
        pf.preparePhasedGenomeInformation(true, selectedSubgenomes);
        checkBuildBLASTDatabase(rebuildDatabase); // create BLAST database if it doesn't exists

        ArrayList<String> inputSequences = read_multifasta_for_BLAST(fastaFile);
        verifyBlastProgramCorrectness(blastProgram);
        if (blastProgram.equals("adaptive")) { // user did not include a BLAST program
            blastProgram = determineBlastProgram(inputSequences, blastProgram);
        }
        try {
            runBlast(blastProgram, inputSequences, alignmentThreshold, minimumIdentity);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong writing to: " + WORKING_DIRECTORY + "/BLAST/blast_results.tsv");
        }

        Pantools.logger.info("Finding best hits.");
        find_best_blast_hits("genome");
        find_best_blast_hits("sequence");
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}BLAST/presence.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}BLAST/blast_results.tsv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}BLAST/best_blast_result_genome.tsv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}BLAST/best_blast_result_sequence.tsv", WORKING_DIRECTORY);
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
            find_best_blast_hits("subgenome");
            Pantools.logger.info(" {}BLAST/best_blast_result_subgenome.tsv", WORKING_DIRECTORY);
        }
    }

    /**
     *
     * Example (pantools) BLAST output. tab separated.
     * #BLASTP
     * #Strictness for accepting hits was >= 65% sequence identity with >= 65% alignment length of input sequence length
     * #query  subject identity    length  mismatches  gaps    alignment start query   alignment end query alignment start subject alignment end subject   evalue  bitscore
     *      PASS minimum identity   PASS minimum alignment length   genome  sequence (number)   original sequence name  start coordinate    end coordinate  mRNA length (bp)
     *      protein length (aa) mRNA identifier mRNA name   mRNA node identifier    homology group node identifier  gene identifier gene name   gene node identifier
     *
     *
     * ##sequence 1,>Msy_05A020290-mRNA1, 1040 length
     * Msy_05A020290-mRNA1 Msy_05A020290-mRNA1_genome_1    100.000 1040    0   0   1   1040    1   1040    0.0 2128    PASS    PASS    1   25  chr5A   33629628    33644667    15040   1040    Msy_05A020290-mRNA1 -   104233655   0   Msy_05A020290   Msy_05A020290   104233654
     * Msy_05A020290-mRNA1 Mdg_scaffold382g000290-mRNA1_genome_2   82.009  428 11  5   619 1040    1   368 0.0 697 PASS    FAIL    2
     * Msy_05A020290-mRNA1 MABA000591_genome_5 65.444  518 55  6   610 1034    15  501 0.0 636 PASS    FAIL    5
     * Msy_05A020290-mRNA1 Msy_05B019610-mRNA1_genome_1    65.243  515 55  6   619 1040    1   484 0.0 629 PASS    FAIL    1
     * Msy_05A020290-mRNA1 Msy_05A020360-mRNA1_genome_1    65.815  509 53  6   619 1034    1   481 0.0 627 PASS    FAIL    1
     */
    private void find_best_blast_hits(String sequenceSubgenomeGenome) {
        // save only the best hit per sequence/subgenome/genome per query

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            HashMap<String, Double> highestScoreMap = new HashMap<>(); // keep track of the highest score per sequence/subgenome/genome
            HashMap<String, StringBuilder> highestValue = new HashMap<>(); // used to store the line (from BLAST) with the highest value
            try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "BLAST/blast_results.tsv"))) {
                BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "BLAST/best_blast_result_" + sequenceSubgenomeGenome + ".tsv"));

                String previousQueryId = "";
                boolean previousHighest = false;

                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    if (line.startsWith("#query")){
                        out.write("#highest in " + sequenceSubgenomeGenome + "\t" + line.replace("#query,", "query") + "\n");
                    } else if (line.startsWith("#") && !line.startsWith("##")) {
                        out.write(line + "\n");
                    }

                    if (line.startsWith("##sequence")) {
                        if (!previousQueryId.equals("")) {
                            out.write(previousQueryId + "\n");
                            for (String subjectKey : highestValue.keySet()) {
                                 out.write(subjectKey + "\t" + highestValue.get(subjectKey) + "\n");
                            }
                            out.write("\n");
                        }
                        previousQueryId = line;
                        previousHighest = false;
                    } else if (!line.startsWith("#") && !line.equals("")) {
                        String[] lineArray = line.split("\t");
                        if (lineArray.length < 16) { // alignment did not pass thresholds
                            continue;
                        }
                        String key = getSubjectKey(lineArray, sequenceSubgenomeGenome);
                        if (lineArray[0].equals(".")) {
                            // the first 14 columns are dots. this is another gene found in the earlier BLAST hit
                            // append it if the previous line was the highest
                            if (previousHighest) {
                                highestValue.computeIfAbsent(key, k -> new StringBuilder()).append("\n" + line);
                            }
                            continue;
                        }

                        double identity = Double.parseDouble(lineArray[2]);
                        double alignmentLength = Double.parseDouble(lineArray[3]);
                        double score = identity*alignmentLength;

                        if (highestScoreMap.containsKey(key)) {
                            double highestScore = highestScoreMap.get(key);
                            if (score > highestScore){  // new score is higher, replace old
                                highestValue.put(key, new StringBuilder(line));
                                previousHighest = true;
                            }  else if (score == highestScore) { // new score is equal, append the map
                                highestValue.computeIfAbsent(key, k -> new StringBuilder()).append("\n" + line);
                            } else {
                                previousHighest = false;
                            }
                        } else { // key doesn't exist yet, automatically highest
                            highestValue.put(key, new StringBuilder(line));
                            highestScoreMap.put(key, score);
                            previousHighest = true;
                        }
                    }
                }
                out.write(previousQueryId + "\n");
                for (String subjectKey : highestValue.keySet()) {
                    out.write(subjectKey + "\t" + highestValue.get(subjectKey) + "\n");
                }
                out.write("\n");
                out.close();
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read: {}BLAST/blast_results.tsv", WORKING_DIRECTORY);
                System.exit(1);
            }
            tx.success(); // transaction successful, commit changes
        }
    }

    private String getSubjectKey(String[] lineArray, String sequenceSubgenomeGenome) {
        String key;
        if (sequenceSubgenomeGenome.equals("genome")) {
            key = lineArray[14];
        } else if  (sequenceSubgenomeGenome.equals("sequence")) {
            key = lineArray[14] + "_" + lineArray[15];
        } else { // subgenome
            String[] phasingInfo = phasingInfoMap.get(lineArray[14] + "_" + lineArray[15]); // [1, B, 1B, 1_B]
            if (phasingInfo == null){
                key = lineArray[14] + "_unphased";
            } else {
                key = lineArray[14] + "_" + phasingInfo[1];
            }
        }
        return key;
    }

    /**
     *
     * @param mrnaNodes
     * @param genomeNr
     * @return
     */
    private void createProteinFilesForBLAST(ResourceIterator<Node> mrnaNodes, int genomeNr) {
        int counter = 0;
        StringBuilder proteins = new StringBuilder();
        while (mrnaNodes.hasNext()) {
            counter++;
            Node mrnaNode = mrnaNodes.next();
            if (counter % 10000 == 0 ||counter == 1) {
                System.out.print("\rGathering mRNA nodes: Genome " + genomeNr + ". " + counter + "            ");
            }
            if (mrnaNode.hasProperty("protein_ID")) {
                String proteinSequence = get_protein_sequence(mrnaNode);
                String id = (String) mrnaNode.getProperty("protein_ID");
                proteins.append(">").append(id).append("_genome_").append(genomeNr).append("\n")
                        .append(proteinSequence).append("\n");
            }
        }
        writeStringToFile(proteins.toString(), WORKING_DIRECTORY + "proteins/for_blast/proteins_" + genomeNr + ".fasta", false, false);
    }

    private Long from_mrna_node_to_hm_node_id(Node mrna_node) {
        Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
        long hm_id = 0;
        if (hm_rel != null) {
            Node hm_node = hm_rel.getStartNode();
            hm_id = hm_node.getId();
        }
        return hm_id;
    }

    /**
     *
     * @param blastProgram
     */
    private void verifyBlastProgramCorrectness(String blastProgram) { // TODO: move to CLI
        if (blastProgram.equals("adaptive")) {
            Pantools.logger.info("No --mode included. Using BLASTN or BLASTP depending on input sequences.");
            Pantools.logger.info("Available options are: BLASTN, BLASTP, BLASTX, TBLASTX, TBLASTN");
        } else if (blastProgram.equals("BLASTN")) { // nuc -> nuc
            Pantools.logger.info("Using BLASTN. Nucleotide -> Nucleotide.");
        } else if (blastProgram.equals("BLASTP")) { // protein -> protein
            Pantools.logger.info("Using BLASTP. Protein -> Protein.");
        } else if (blastProgram.equals("BLASTX")) { // six nuc frame -> protein
            Pantools.logger.info("Using BLASTX. Six nucleotide frames -> Protein.");
        } else if (blastProgram.equals("TBLASTX")) { // six nuc frame -> six nuc frame
            Pantools.logger.info("Using TBLASTX. Six nucleotide frames -> Six nucleotide frames.");
        } else if (blastProgram.equals("TBLASTN")) { // protein -> six nuc frame
            Pantools.logger.info("Using TBLASTN. Protein -> Six nucleotide frames.");
        } else {
            throw new RuntimeException("BLAST program provided via --mode not recognized. Use one of the following: BLASTN, BLASTP, BLASTX, TBLASTX, TBLASTN");
        }
    }

    private void createSelectedGenomesInfo() {
        java.util.Date date = new java.util.Date();
        if (selectedGenomes.size() != GENOME_DB.num_genomes) {
            int skipped_genomes = GENOME_DB.num_genomes-selectedGenomes.size();
            Pantools.logger.info("Excluding {} genomes from the BLAST DATABASE", skipped_genomes);
        }
        writeStringToFile("Created on " + date + " with " + total_genomes + " genomes\n" +
                        "Selected genomes: " + selectedGenomes.toString().replace("[","").replace("]","").replace(" ","")
                , WORKING_DIRECTORY + "BLAST/info", false, false);
    }

    /**
     *  Excludes genome from the database
     */
    private void makeblastdb() {
        create_directory_in_DB("BLAST/protein_db");
        if (!PROTEOME) {
            create_directory_in_DB("BLAST/genome_db");
            create_directory_in_DB("retrieval/genomes");
        }

        createSelectedGenomesInfo();
        String[] genomeProteinFileLocations = prepareGenomesProteins();
        if (!PROTEOME) { // build nucleotide database
            String genomeCommand = "cat " + genomeProteinFileLocations[0] + " | makeblastdb -out " + WORKING_DIRECTORY + "BLAST/genome_db/genome_db -title genome_db -parse_seqids -dbtype nucl";
            Pantools.logger.info("Building BLAST genome database.");
            Pantools.logger.debug(genomeCommand);
            runCommand(genomeCommand, 86399);
        }

        //build protein database
        String proteinCommand = "cat " + genomeProteinFileLocations[1] + " | makeblastdb -out " + WORKING_DIRECTORY + "BLAST/protein_db/protein_db -title protein_db -parse_seqids -dbtype prot";
        Pantools.logger.info("Building BLAST protein database.");
        Pantools.logger.debug(proteinCommand);
        runCommand(proteinCommand, 86400);

        if (!PROTEOME) {
            Pantools.logger.info("Created a BLAST nucleotide and protein database:");
            Pantools.logger.info(" {}BLAST/genome_db/", WORKING_DIRECTORY);
            Pantools.logger.info(" {}BLAST/protein_db/", WORKING_DIRECTORY);
        } else {
            Pantools.logger.info("Created a BLAST protein database:");
            Pantools.logger.info(" {}BLAST/protein_db/", WORKING_DIRECTORY);
        }
    }

    private String[] prepareGenomesProteins() {
        StringBuilder genomeFiles = new StringBuilder();
        StringBuilder proteinFiles = new StringBuilder();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int genomeNr : selectedGenomes) {
                if (!PROTEOME) {
                    if (!checkIfFileExists( WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genomeNr + ".fasta")) {
                        throw new RuntimeException("Unable to open the file for genome " + genomeNr);
                    }
                    genomeFiles.append(WORKING_DIRECTORY).append("/retrieval/genomes/genome_").append(genomeNr).append(".fasta  "); // spaces at end are intentional
                }

                if (!checkIfFileExists(WORKING_DIRECTORY + "/proteins/for_blast/proteins_" + genomeNr + ".fasta")) {
                    throw new RuntimeException("Unable to open the protein file for genome " + genomeNr);
                }
                proteinFiles.append(WORKING_DIRECTORY).append("/proteins/for_blast/proteins_").append(genomeNr).append(".fasta  "); // spaces at end are intentional
            }
            tx.success();
        }
        return new String[] { genomeFiles.toString() ,proteinFiles.toString() };
    }

    private boolean validatePreviousHeaders(int genomeNr) {
        Path file = Paths.get(WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genomeNr + ".fasta");
        boolean goodHeader = false;
        try {
            BufferedReader br = Files.newBufferedReader(file);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.equals(">" + genomeNr + "_1") ){
                    goodHeader = true;
                }
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return goodHeader;
    }

    /**
     * Use sequence identifiers instead of original header
     * @param genomeNr
     */
    private void write_genome_file_with_new_headers(int genomeNr) {
        if (checkIfFileExists(WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genomeNr + ".fasta")) { // check if file already exists. if yes, check if the header is ok
            boolean goodHeader = validatePreviousHeaders(genomeNr);
            if (goodHeader) {
                return;
            }
        }

        Paths.get(WORKING_DIRECTORY).resolve("retrieval").resolve("genomes").toFile().mkdirs();
        StringBuilder seq = new StringBuilder();
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/retrieval/genomes/genome_" + genomeNr + ".fasta"));
            for (int sequence = 1; sequence <= GENOME_DB.num_sequences[genomeNr]; sequence++) {
                if (!selectedSequences.contains(genomeNr + "_" + sequence)){
                    continue;
                }
                out.write(">" + genomeNr + "_" + sequence + "\n");
                int begin = 0;
                int end = (int) GENOME_DB.sequence_length[genomeNr][sequence] - 1;
                GENOME_SC.get_sub_sequence(seq, genomeNr, sequence, begin, end - begin + 1, true);
                write_fasta(out, seq.toString(), 80);
                seq.setLength(0);
            }
            out.close();
        } catch (IOException e) {
            throw new RuntimeException("Unable to write a genome to " + WORKING_DIRECTORY + "/retrieval/genome_" + genomeNr + ".fasta");
        }
    }

    private void increase_blast_hashmap(HashMap<Integer, StringBuilder> hashmap, int key, String value) {
        StringBuilder builder;
        if (hashmap.containsKey(key)) {
            builder = hashmap.get(key);
            builder.append(value);
        } else {
            if (key == 0) {
                builder = new StringBuilder("," + value);
            } else if (key > total_genomes) {
                builder = new StringBuilder("\n," + value);
            } else {
                builder = new StringBuilder("Genome " + key + "," + value);
            }
        }
        hashmap.put(key, builder);
    }

    /**
     * Read a fasta file
     * @return sequences
     */
    private ArrayList<String> read_multifasta_for_BLAST(Path fastaFile) {
        ArrayList<String> sequences = new ArrayList<>();
        int counter = 0;
        StringBuilder sequence = new StringBuilder();
        try {
            BufferedReader br = Files.newBufferedReader(fastaFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                 line = line.trim();
                 if (line.contains(">")) {
                    if (counter > 0) {
                        String sequenceStr = sequence.toString();
                        sequences.add(sequenceStr);
                        sequence = new StringBuilder();
                    }
                    sequence.append(line).append("\n");
                    counter++;
                    continue;
                }
                sequence.append(line);
            }
            String seq_str = sequence.toString();
            sequences.add(seq_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read: " + fastaFile);
        }
        return sequences;
    }

    /**
     * @return true when the string consists of nucleotides
     */
    private boolean check_if_nucleotide_or_protein(String sequence) {
        String[] sequenceArray = sequence.split("\n");
        String nucleotide = "ATCGatcg";
        String protein = "ARNDCEQGHILKMFPSTWYXarndceqghilkmfpstwyx";
        boolean nucleotide_seq = true;
        int nuc_count = 0, prot_count = 0;
        for (char character : sequenceArray[1].toCharArray()) {
            int index = nucleotide.indexOf(character);
            int index2 = protein.indexOf(character);
            if (index != -1) {
                nuc_count++;
            }
            if (index2 != -1) {
                prot_count++;
            }
            if ((nuc_count + prot_count) > 100) {
                break;
            }
        }
        if (prot_count > nuc_count) {
            nucleotide_seq = false;
        }
        return nucleotide_seq;
    }

    /**
     *
     * @param seq_counter
     * @param fasta_array [header, sequence]
     * @param BLAST_Mode
     */
    private String createBlastSequenceOutputHeader(int seq_counter, String[] fasta_array, String BLAST_Mode) {
        StringBuilder header = new StringBuilder();
        header.append("\n##sequence ").append(seq_counter).append(",").append(fasta_array[0]).append(", ")
                .append(fasta_array[1].length()).append(" length");

        if (BLAST_Mode.equals("BLASTX") || BLAST_Mode.equals("TBLASTX")) {
            header.append("/3 = ").append((fasta_array[1].length()/3));
        }
        header.append("\n");
        return header.toString();
    }

    private String determineBlastProgram(ArrayList<String> sequences, String blastProgram) {
        boolean nucleotide = check_if_nucleotide_or_protein(sequences.get(0));
        if (nucleotide && blastProgram.equals("adaptive")) {
            blastProgram = "BLASTN";
        } else if (!nucleotide && blastProgram.equals("adaptive")) {
            blastProgram = "BLASTP";
        }

        if (nucleotide && (blastProgram.equals("BLASTP") || blastProgram.equals("TBLASTN"))) {
            throw new RuntimeException(blastProgram + " requires protein sequences\n");
        } else if (!nucleotide && (blastProgram.equals("BLASTN") || blastProgram.equals("BLASTX") || blastProgram.equals("TBLASTX"))) {
            throw new RuntimeException(blastProgram + " requires nucleotide sequences\n");
        }
        return blastProgram;
    }

    private String createBlastOutputTSVheader(String blastProgram, int alignmentThreshold, int minimumIdentity) {
        StringBuilder output_builder = new StringBuilder("#" + blastProgram + "\n")
                .append("#Strictness for accepting hits was >= ").append(minimumIdentity).append("% sequence identity with ")
                .append(">= ").append(alignmentThreshold).append("% alignment length of input sequence length\n")
                .append("#query\tsubject\tidentity\tlength\tmismatches\tgaps\talignment start query\talignment end query\t" +
                        "alignment start subject\talignment end subject\tevalue\tbitscore\tPASS minimum identity\tPASS minimum alignment length\tgenome\t");

        if (PROTEOME) {
            output_builder.append("Protein node identifier\tprotein id\n");
        } else {
            output_builder.append("sequence (number)\toriginal sequence name\tstart coordinate\tend coordinate\tmRNA length (bp)\t" +
                    "protein length (aa)\tmRNA identifier\tmRNA name\tmRNA node identifier\t" +
                    "homology group node identifier\tgene identifier\tgene name\tgene node identifier\n");
        }
        return output_builder.toString();
    }

    private HashMap<String, String> createSequenceNumberIdHashmap() {
        HashMap<String, String> sequenceIdNameMap = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequenceNodes.hasNext()) {
                Node sequenceNode = sequenceNodes.next();
                String identifier = (String) sequenceNode.getProperty("identifier");
                String title = (String) sequenceNode.getProperty("title");
                String[] titleArray = title.split(" ");
                sequenceIdNameMap.put(identifier, titleArray[0]);
            }
            tx.success();
        }
        return sequenceIdNameMap;
    }

    private void runBlast(String blastProgram, ArrayList<String> sequences, int alignmentThreshold, int minimumIdentity) throws IOException {
        Mode = "PARTIAL"; // for find_genes_in_region(). genes that partially overlap with the region are also reported
        boolean againstNucleotide = false;
        HashMap<String, String> sequenceIdNameMap = createSequenceNumberIdHashmap();
        if (blastProgram.equals("BLASTN") || blastProgram.equals("TBLASTN") || blastProgram.equals("TBLASTX")){ // against nucleotide sequences
            againstNucleotide = true;
        }


        String blastHeader = createBlastOutputTSVheader(blastProgram, alignmentThreshold, minimumIdentity);
        //StringBuilder tsvOutput = new StringBuilder(blastHeader);
        BufferedWriter presence = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/BLAST/presence.txt"));
        BufferedWriter tsvOutput = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "/BLAST/blast_results.tsv"));
        tsvOutput.write(blastHeader);

        //write_log = true; // to ensure the BLAST output is written to LOG
        int sequenceCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (String sequence : sequences) {
                sequenceCounter++;
                ArrayList<Integer> present_genomes = new ArrayList<>();
                ArrayList<String> gene_names = new ArrayList<>();
                TreeSet<Long> hmgroups = new TreeSet<>();

                String[] fasta_array = sequence.split("\n"); // [ header, sequence]

                String header = createBlastSequenceOutputHeader(sequenceCounter, fasta_array, blastProgram);
                tsvOutput.write(header);

                Pantools.logger.info(blastProgram + "ing: sequence " + sequenceCounter + "/" + sequences.size() ); // So anyway, I started
                writeStringToFile(sequence, WORKING_DIRECTORY + "sequence.fasta", false, false);

                String blast_command2 = createBLASTCommandString(blastProgram);
                Pantools.logger.debug(blast_command2);
                runCommand(blast_command2 , 86400);

                int hits = countLinesInFile(Paths.get(WORKING_DIRECTORY + "BLAST_OUT"));
                Pantools.logger.info("Sequence " +  sequenceCounter + ". Analyzing " + hits + " hits.");
                try {
                    BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "BLAST_OUT")); // only read to count the lines
                    while (in.ready()) {
                        String line = in.readLine().trim();
                        tsvOutput.write(line);
                        String[] lineArray = line.split("\t");
                        int genomeNr = checkGenomeNrOfHit(blastProgram, lineArray);
                        if (!selectedGenomes.contains(genomeNr)) {
                            tsvOutput.write("\tSKIPPED\tSKIPPED\t" + genomeNr + "\t" + "\n");
                            continue;
                        }
                        boolean[] passTwoThresholds = checkIfPasssedThresholds(lineArray, fasta_array[1].length(), blastProgram, alignmentThreshold, minimumIdentity);
                        boolean pass = true;
                        if (!passTwoThresholds[0]) {
                            tsvOutput.write("\tFAIL\t");
                            pass = false;
                        } else {
                            tsvOutput.write("\tPASS\t");
                        }

                        if (!passTwoThresholds[1]) {
                            tsvOutput.write("FAIL\t");
                            pass = false;
                        } else {
                            tsvOutput.write("PASS\t");
                        }

                        if (!pass) {
                            tsvOutput.write(genomeNr + "\t" + "\n");
                            continue;
                        }

                        present_genomes.add(genomeNr);
                        if (againstNucleotide) { // against nucleotide sequences
                            ArrayList<Node> foundMrnaNodes = findGenesInHitRegion(lineArray);
                            if (foundMrnaNodes.isEmpty()) {
                                tsvOutput.write("# No gene was found in this region\n");
                            } else {
                                boolean firstMrna = true;
                                for (Node mrnaNode : foundMrnaNodes) {
                                    if (!firstMrna) {
                                        // multiple genes found in region. Multiple tabs to outline gene info to the right (skips the blast hit columns)
                                        tsvOutput.write(".\t.\t.\t.\t.\t.\t.\t.\t.\t.\t.\t.\t.\t.\t");
                                    }
                                    firstMrna = false;
                                    add_gene_info_to_blast_output(mrnaNode, tsvOutput, gene_names, hmgroups, sequenceIdNameMap);
                                }
                            }
                        } else { // against protein database
                            String[] identifierArray = lineArray[1].split("_genome_");
                            ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL, "protein_ID", identifierArray[0], "genome", genomeNr);
                            while (mrnaNodes.hasNext()) {
                                Node mrnaNode = mrnaNodes.next();
                                add_gene_info_to_blast_output(mrnaNode, tsvOutput, gene_names, hmgroups, sequenceIdNameMap);
                            }
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }

                TreeSet<Integer> absent = get_missing_genomes_in_arraylist(present_genomes);
                String names_freq = determine_frequency_list_str(gene_names);
                String genomes_freq = determine_frequency_list_int(present_genomes);
                TreeSet<Integer> found_genomes_freq = return_found_frequencies_in_arraylist(present_genomes);
                String freq_str = found_genomes_freq.toString().replace("[","").replace("]","");
                String copies_str = "Number of different copies found: " + freq_str;
                presence.write("#Sequence " + sequenceCounter + ", " + fasta_array[0]);
                if (present_genomes.size() > 0) {
                    presence.write("\nGene names: " + names_freq
                            + "\nPresent in genomes " + genomes_freq
                            + "\n" + copies_str
                            + "\nMissing in genomes " + absent);
                } else {
                    presence.write("\nNo hits found");
                }

                presence.write("\nGenes are part of " + hmgroups.size() + " homology groups: " +
                        hmgroups.toString().replace("[","").replace("[","") + "\n\n");
            }
            tx.success();
        }
        presence.close();
        tsvOutput.close();
        delete_file_in_DB("BLAST_OUT");
        delete_file_in_DB("sequence.fasta");
    }

    /**
     *
     * @param present_genomes
     * @return
     */
    private TreeSet<Integer> get_missing_genomes_in_arraylist(ArrayList<Integer> present_genomes) {
        TreeSet<Integer> absent = new TreeSet<>();
        for (int i = 1; i <= total_genomes; ++i) {
            if (skip_array[i-1]) {
                continue;
            }
            if (!present_genomes.contains(i)) {
                absent.add(i);
            }
        }
        return absent;
    }

    /**
     *
     * @param size_list
     * @return
     */
    private String determine_frequency_list_int(ArrayList<Integer> size_list) {
        ArrayList<Integer> distinct_size_list = remove_duplicates_from_AL_int(size_list);
        StringBuilder output = new StringBuilder();
        for (int size : distinct_size_list) {
            int occurrences = Collections.frequency(size_list, size);
            output.append(size).append("(").append(occurrences).append("x), ");
        }
        return output.toString().replaceFirst(".$", "").replaceFirst(".$", ""); // remove last character (2x)
    }

    private ArrayList<Node> findGenesInHitRegion(String[] line_array) {
        String[] identifierArray = line_array[1].split("_");
        int start_pos = Integer.parseInt(line_array[8]);
        int end_pos = Integer.parseInt(line_array[9]);
        if (start_pos > end_pos) {
            regions_to_search = new String[]{identifierArray[0] + " " + identifierArray[1] + " " + line_array[9] + " " + line_array[8]};
        } else {
            regions_to_search = new String[]{identifierArray[0] + " " + identifierArray[1] + " " + line_array[8] + " " + line_array[9]};
        }
        return classification.find_genes_in_region(false);
    }

    private int checkGenomeNrOfHit(String BLAST_Mode, String[] lineArray) {
        int genomeNr;
        if (BLAST_Mode.equals("BLASTN") || BLAST_Mode.equals("TBLASTN") || BLAST_Mode.equals("TBLASTX")) { // against nucleotide
            String[] id_array = lineArray[1].split("_");
            genomeNr = Integer.parseInt(id_array[0]);
        } else {//if (BLAST_Mode.equals("BLASTP") || BLAST_Mode.equals("BLASTX") ) { // against protein database
            String[] genome_array = lineArray[1].split("_genome_");
            genomeNr = Integer.parseInt(genome_array[1]);
        }
        return genomeNr;
    }

    /**
     * for some unknown reason sometimes pal2nal just gets stuck. The command works fine when run separately
     *
     * @param command the shell command to run
     * @param timeLimit the time limit in seconds
     */
    private void runCommand(String command, int timeLimit) {
        List<String> cmd = new ArrayList<>();
        cmd.add("sh");
        cmd.add("-c");
        cmd.add(command);
        ProcessBuilder pb = new ProcessBuilder(cmd);
        try {
            Process p = pb.start();
            p.waitFor(timeLimit, TimeUnit.SECONDS);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Something went wrong running: " + command);
        }
    }

    /**
     *
     * @param size_list
     * @return
     */
    private String determine_frequency_list_str(ArrayList<String> size_list) {
        ArrayList<String> distinct_size_list = remove_duplicates_from_AL_str(size_list);
        StringBuilder output = new StringBuilder();
        for (String size : distinct_size_list) {
            int occurrences = Collections.frequency(size_list, size);
            output.append(size).append("(").append(occurrences).append("x), ");
        }
        return output.toString().replaceFirst(".$", "").replaceFirst(".$", "");
    }

    private TreeSet<Integer> return_found_frequencies_in_arraylist(ArrayList<Integer> size_list) {
        TreeSet<Integer> freq_set = new TreeSet<>();
        ArrayList<Integer> distinct_size_list = Utils.remove_duplicates_from_AL_int(size_list);
        for (int size : distinct_size_list) {
            int occurrences = Collections.frequency(size_list, size);
            freq_set.add(occurrences);
        }
        return freq_set;
    }

    /**
     *
     * @param list
     * @return
     */
    private ArrayList<String> remove_duplicates_from_AL_str(ArrayList<String> list) {
        List<String> newList = list.stream()
                .distinct()
                .collect(Collectors.toList());
        Collections.sort(newList);
        ArrayList tester = new ArrayList<>(newList);
        return tester;
    }

    /**
     *
     * @param line_array blast output 6 line split on tab
     * @param sequence_length
     * @param BLAST_Mode
     * @return
     */
    private boolean[] checkIfPasssedThresholds(String[] line_array, int sequence_length,
                                                            String BLAST_Mode, int alignmentThreshold, int minimumIdentity) {
        double identity = Double.parseDouble(line_array[2]);
        double ali_length = Double.parseDouble(line_array[3]);
        if (BLAST_Mode.equals("BLASTX") || BLAST_Mode.equals("TBLASTX")) {
            sequence_length = sequence_length / 3;
        }

        double length_threshold = (double) alignmentThreshold;
        length_threshold = length_threshold / 100;


        boolean identityPass = false;
        if (identity >= minimumIdentity ) {
            identityPass = true;
        }
        boolean alignmentLengthPass = false;
        if (ali_length > (sequence_length * length_threshold)){
            alignmentLengthPass = true;
        }
        return new boolean[]{identityPass, alignmentLengthPass};
    }


    /**
     * Excluded -max_target_seqs " + max_lines +
     * @param BLAST_Mode
     * @return
     */
    private String createBLASTCommandString(String BLAST_Mode) {
        String command = "";
        if (BLAST_Mode.equals("BLASTN")) {
            command = "blastn -db " + WORKING_DIRECTORY + "BLAST/genome_db/genome_db -num_threads " + THREADS +
                    " -query " + WORKING_DIRECTORY + "/sequence.fasta -outfmt 6 1> " + WORKING_DIRECTORY + "BLAST_OUT";
        } else if (BLAST_Mode.equals("BLASTP")) {
            command = "blastp -db " + WORKING_DIRECTORY + "BLAST/protein_db/protein_db -num_threads " + THREADS +
                    " -query " + WORKING_DIRECTORY + "/sequence.fasta -outfmt 6 1> " + WORKING_DIRECTORY + "BLAST_OUT";
        } else if (BLAST_Mode.equals("BLASTX")) {
            command = "blastx -db " + WORKING_DIRECTORY + "BLAST/protein_db/protein_db -num_threads " + THREADS +
                    " -query " + WORKING_DIRECTORY + "/sequence.fasta -outfmt 6 1> " + WORKING_DIRECTORY + "BLAST_OUT";
        } else if (BLAST_Mode.equals("TBLASTX")) {
            command = "tblastx -db " + WORKING_DIRECTORY + "BLAST/genome_db/genome_db -num_threads " + THREADS +
                    " -query " + WORKING_DIRECTORY + "/sequence.fasta -outfmt 6 1> " + WORKING_DIRECTORY + "BLAST_OUT";
        } else if (BLAST_Mode.equals("TBLASTN")) {
            command = "tblastn -db " + WORKING_DIRECTORY + "BLAST/genome_db/genome_db -num_threads " + THREADS +
                    " -query " + WORKING_DIRECTORY + "/sequence.fasta -outfmt 6 1> " + WORKING_DIRECTORY + "BLAST_OUT";
        }
        return command;
    }

    /**
     * the name property can be String[] or String dependent on PanTools version
     * Works on gene and mRNA nodes
     * @return
     */
    private String getNameFromGeneMrnaNode(Node node) {
        String name;
        try {
            name = (String) node.getProperty("name");
        } catch (ClassCastException classCastException){
            String[] geneNames = (String[]) node.getProperty("name");
            name = Arrays.toString(geneNames).replace(", "," ").replace("[","").replace("]","");
        }
        if (name.equals("")) {
            name = "-";
        }
        return name;
    }

    private void add_gene_info_to_blast_output(Node mrnaNode, BufferedWriter tsvOutput, ArrayList<String> gene_names,
                                               TreeSet<Long> hmgroups, HashMap<String, String> sequenceIdNameMap) throws IOException {

        if (!mrnaNode.hasProperty("protein_ID")) return;
        String protein_id = (String) mrnaNode.getProperty("protein_ID");
        Relationship relation = mrnaNode.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
        long hm_node_id = from_mrna_node_to_hm_node_id(mrnaNode);
        hmgroups.add(hm_node_id);
        Node geneNode = relation.getStartNode();
        String geneId = (String) geneNode.getProperty("id");
        String geneName = getNameFromGeneMrnaNode(geneNode);
        gene_names.add(geneName);

        if (PROTEOME) {
            tsvOutput.write(mrnaNode.getId() + "\t" + protein_id + "\n");
        } else {
            int[] address = (int[]) mrnaNode.getProperty("address");
            String mrnaId = (String) mrnaNode.getProperty("id");
            String mrnaName = getNameFromGeneMrnaNode(mrnaNode);
            int protein_length = 0;
            if (mrnaNode.hasProperty("protein_length")) {
                protein_length = (int) mrnaNode.getProperty("protein_length");
            }

            tsvOutput.write(address[0] + "\t" + address[1] + "\t" // genome and sequence number
                    + sequenceIdNameMap.get(address[0] + "_" + address[1]) + "\t" // sequence name
                    + address[2] + "\t" + address[3] + "\t" // start and stop address
                    + ((address[3]-address[2])+1) + "\t" + protein_length + "\t" // nucleotide & protein lengths
                    + mrnaId + "\t" + mrnaName + "\t" + mrnaNode.getId() + "\t" // mrna info
                    + hm_node_id + "\t" // homology group
                    + geneId + "\t" + geneName + "\t" + geneNode.getId() + "\n"); // gene info
        }
    }

    /**
     * Create a protein/nucleotide BLAST database if it doesn't exists
     */
    private void checkBuildBLASTDatabase(boolean rebuildDatabase) {
        boolean exists1 = checkIfFileExists(WORKING_DIRECTORY + "BLAST/genome_db/genome_db.nsq");
        if (!exists1) {
            exists1 = checkIfFileExists(WORKING_DIRECTORY + "BLAST/genome_db/genome_db.00.nsq");
        }
        boolean exists2 = checkIfFileExists(WORKING_DIRECTORY + "BLAST/protein_db/protein_db.psq");
        if (!exists2) {
            exists2 = checkIfFileExists(WORKING_DIRECTORY + "BLAST/protein_db/protein_db.00.psq");
        }

        if (PROTEOME) {
            exists1 = true;
        }

        boolean buildDatabases = false;
        if (!exists1 || !exists2) {
            Pantools.logger.info("No (correct) BLAST databases available, building them now.");
            buildDatabases = true;
        } else {
            String infoFile = WORKING_DIRECTORY + "BLAST/info";
            if (!checkIfFileExists(infoFile)) {
                throw new RuntimeException(WORKING_DIRECTORY + "BLAST/info should exist. Remove this entire directory and try again");
            }
            boolean sameGenomes = check_if_blastdb_has_same_genomes();
            // there is a database but the genome selection is different
            if (!sameGenomes && rebuildDatabase) {
                Pantools.logger.info("\rThere is a BLAST database but with a different genome selection. Rebuilding the database!");
                buildDatabases = true;
            } else if (!sameGenomes) {
                Pantools.logger.info("\rWARNING! There is a BLAST database but with a different genome selection. See " + infoFile + ". If not OK, restart with --rebuild argument.");
            }
        }

        if (buildDatabases) {
            create_directory_in_DB("BLAST");
            prepare_proteins_for_BLAST();
            prepare_genomes_for_BLAST();
            makeblastdb();
        }
    }

    private void prepare_genomes_for_BLAST() {
        for (int genomeNr : selectedGenomes) {
            if (!PROTEOME) {
                System.out.print("\rPreparing genome input: " + genomeNr);
                write_genome_file_with_new_headers(genomeNr);
            }
        }
    }

    /**
     * @return the 'correct_genomes' variable true when database is built with same genomes
     */
    private boolean check_if_blastdb_has_same_genomes() {
        boolean correct_genomes = false;
        String selected_genomes_str = selectedGenomes.toString().replace("[","").replace("]","").replace(" ","");
        String line;
        boolean first_line = true;
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "BLAST/info"))) {
            while ((line = in.readLine()) != null) {
                if (first_line){
                    first_line = false;
                } else { // the second line
                    line = line.trim();
                    String[] line_array = line.split(": ");
                    if (line_array[1].equals(selected_genomes_str)) {
                        correct_genomes = true;
                    }
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}BLAST/info", WORKING_DIRECTORY);
            System.exit(1);
        }
        return correct_genomes;
    }

    /**
     *
     */
    public void prepare_proteins_for_BLAST() {
        create_directory_in_DB("proteins");
        create_directory_in_DB("proteins/for_blast/");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            if (!PROTEOME) {
                for (String annotationId : annotation_identifiers) { // go over annotation ids because genomes can have multiple annotations
                    if (annotationId.endsWith("_0")) {
                        continue;
                    }
                    String[] annotationIdArray = annotationId.split("_");
                    if (checkIfFileExists(WORKING_DIRECTORY + "proteins/for_blast/proteins_" + annotationIdArray[0] + ".fasta")) {
                        continue;
                    }
                    ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL, "annotation_id", annotationId);
                    createProteinFilesForBLAST(mrnaNodes, Integer.parseInt(annotationIdArray[0]));
                }
            } else { // panproteome
                for (int genomeNr : selectedGenomes) {
                    ResourceIterator<Node> mrnaNodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genomeNr);
                    createProteinFilesForBLAST(mrnaNodes, genomeNr);
                }
            }
            tx.success();
        }
    }


}
