package nl.wur.bif.pantools.analysis.map_reads.parallel;

import htsjdk.samtools.SAMException;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMTag;
import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.index.IndexPointer;
import nl.wur.bif.pantools.construction.index.IndexScanner;
import nl.wur.bif.pantools.construction.index.kmer;
import nl.wur.bif.pantools.construction.sequence.SequenceScanner;
import nl.wur.bif.pantools.construction.build_pangenome.GenomeLayer;
import nl.wur.bif.pantools.utils.Globals;
import nl.wur.bif.pantools.utils.Utils;
import nl.wur.bif.pantools.utils.local_sequence_alignment.BoundedLocalSequenceAlignment;
import nl.wur.bif.pantools.utils.local_sequence_alignment.LocalSequenceAlignment;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class MapReadsParallel implements Runnable {
    IndexScanner indexSc;
    SequenceScanner genomeSc;
    int thread_id;
    int K;
    FastqRecord[] fastq_record;
    ArrayList<int[]>[][] locations;
    IntComparator intcomp = new IntComparator();
    IndexPointer pointer;
    StringBuilder reference;
    BoundedLocalSequenceAlignment bounded_aligner;
    LocalSequenceAlignment aligner;
    LinkedList<int[]> node_results;
    PriorityQueue<SingleHit>[][] hits;
    LinkedList<SingleHit>[] alignments;
    PriorityQueue<SingleHit> single_hits;
    Queue<SingleHit> single_hits_2;
    PriorityQueue<PairedHit> paired_hits;
    Queue<PairedHit> paired_hits_2;
    boolean paired_end;
    IntPairComparator int_pair_comp;
    ArrayList<int[]> hit_counts;
    int num_neighbors = 0;
    int num_kmers = 0;
    int num_found = 0;
    int[] address = new int[3];
    ArrayList<Integer> genome_numbers;
    SAMFileWriter[] sam_writers;
    long[] genome_sizes;
    long total_genomes_size = 0;
    int[] shared;
    int[] unique;
    int[] unmapped;
    int total_unique = 1;
    int num_genomes;
    int[] num_sequences;
    long[][] sequence_length;
    String[][] sequence_titles;
    Random rand;
    double[] raw_abundance;
    PairedHitComparator phc = new PairedHitComparator();
    SingleHitComparator shc = new SingleHitComparator();
    //StringBuilder[] forward_read;
    StringBuilder[] reverse_read;
    //StringBuilder[] quality;
    kmer current_kmer;
    //int[] read_len;
    String[] read_name;
    int num_hits = 0;
    int num_alns = 0;
    SingleHit alignment_result;
    HashSet<Integer>[] unmapped_genomes;
    SingleHit[] best_hit;
    int num_segments;
    FastqReader[] reader;
    private final AtomicLong numberOfReads;
    private final AtomicLong numberOfAlignments;
    private final AtomicLong numberOfHits;
    private AtomicLong[] numSharedMapping;
    private AtomicLong[] numUniqueMapping;
    private AtomicLong[] numUnmapped;

    /**
     * Initialized the read mapping object
     *
     * @param id     ID of the working thread
     * @param gn     List of genomes to map reads against
     * @param paired Determines if reads are in pair
     * @param sams   Array of SAM file writer objects
     * @param r
     */
    public MapReadsParallel(int id, ArrayList<Integer> gn, boolean paired, SAMFileWriter[] sams, FastqReader[] r,
                            AtomicLong numberOfReads, AtomicLong numberOfAlignments, AtomicLong numberOfHits,
                            AtomicLong[] numSharedMapping, AtomicLong[] numUniqueMapping, AtomicLong[] numUnmapped) {
        this.numberOfReads = numberOfReads;
        this.numberOfAlignments = numberOfAlignments;
        this.numberOfHits = numberOfHits;
        this.numSharedMapping = numSharedMapping;
        this.numUniqueMapping = numUniqueMapping;
        this.numUnmapped = numUnmapped;
        int i, j, genome, abun;
        indexSc = new IndexScanner(Globals.INDEX_DB);
        K = indexSc.get_K();
        genomeSc = new SequenceScanner(Globals.GENOME_DB, 1, 1, K, indexSc.get_pre_len());
        current_kmer = new kmer(indexSc.get_K(), indexSc.get_pre_len());
        num_genomes = Globals.GENOME_DB.num_genomes;
        unmapped_genomes = new HashSet[2];
        num_sequences = new int[num_genomes + 1];
        sequence_length = new long[num_genomes + 1][];
        sequence_titles = new String[num_genomes + 1][];
        sam_writers = sams;
        thread_id = id;
        genome_numbers = gn;
        genome_numbers.sort(intcomp);
        paired_end = paired;
        num_segments = paired_end ? 2 : 1;
        alignment_result = new SingleHit();
        node_results = new LinkedList();
        hits = new PriorityQueue[2][];
        alignments = new LinkedList[2];
        locations = new ArrayList[2][];
        fastq_record = new FastqRecord[2];
        reverse_read = new StringBuilder[2];
        read_name = new String[2];
        best_hit = new SingleHit[2];
        reader = r;
        reverse_read[0] = new StringBuilder();
        hits[0] = new PriorityQueue[num_genomes + 1];
        alignments[0] = new LinkedList();
        single_hits = new PriorityQueue(shc);
        single_hits_2 = new LinkedList();
        paired_hits = new PriorityQueue(phc);
        paired_hits_2 = new LinkedList();
        locations[0] = new ArrayList[num_genomes + 1];
        unmapped_genomes[0] = new HashSet();
        genome_sizes = new long[num_genomes + 1];
        shared = new int[num_genomes + 1];
        unique = new int[num_genomes + 1];
        unmapped = new int[num_genomes + 2]; // is one larger for unmapped read count in case of -am < 0
        raw_abundance = new double[num_genomes + 1];
        for (i = 0; i <= num_genomes; ++i) {
            hits[0][i] = null;
            locations[0][i] = null;
            genome_sizes[i] = Globals.GENOME_DB.genome_length[i];
            total_genomes_size += genome_sizes[i];
            num_sequences[i] = Globals.GENOME_DB.num_sequences[i];
            sequence_length[i] = new long[num_sequences[i] + 1];
            sequence_titles[i] = new String[num_sequences[i] + 1];
            shared[i] = 0;
        }
        for (i = 0; i < genome_numbers.size(); ++i) {
            genome = genome_numbers.get(i);
            hits[0][genome] = new PriorityQueue(shc);
            locations[0][genome] = new ArrayList();
            for (j = 1; j <= num_sequences[genome]; ++j) {
                sequence_length[genome][j] = Globals.GENOME_DB.sequence_length[genome][j];
                sequence_titles[genome][j] = Globals.GENOME_DB.sequence_titles[genome][j];
            }
        }
        if (paired_end) {
            //quality[1] = new StringBuilder();
            //forward_read[1] = new StringBuilder();
            reverse_read[1] = new StringBuilder();
            hits[1] = new PriorityQueue[num_genomes + 1];
            alignments[1] = new LinkedList();
            locations[1] = new ArrayList[num_genomes + 1];
            unmapped_genomes[1] = new HashSet();
            for (i = 0; i <= num_genomes; ++i) {
                hits[1][i] = null;
                locations[1][i] = null;
            }
            for (i = 0; i < genome_numbers.size(); ++i) {
                genome = genome_numbers.get(i);
                hits[1][genome] = new PriorityQueue(shc);
                locations[1][genome] = new ArrayList();
            }
        }
        pointer = new IndexPointer();
        reference = new StringBuilder();
        bounded_aligner = new BoundedLocalSequenceAlignment(Globals.GAP_OPEN, Globals.GAP_EXT, Globals.MAX_ALIGNMENT_LENGTH, Globals.ALIGNMENT_BOUND, Globals.CLIPPING_STRINGENCY, "NUC.4.4");
        aligner = new LocalSequenceAlignment(Globals.GAP_OPEN, Globals.GAP_EXT, Globals.MAX_ALIGNMENT_LENGTH, Globals.CLIPPING_STRINGENCY, "NUC.4.4");
        int_pair_comp = new IntPairComparator();
        hit_counts = new ArrayList();
        rand = new Random(1);
        if (Globals.RAW_ABUNDANCE_FILE.equals("")) {
            for (i = 1; i <= num_genomes; ++i) {
                raw_abundance[i] = 1.0;
            }
        } else {
            try {
                String line;
                String[] fields;
                BufferedReader in = new BufferedReader(new FileReader(Globals.RAW_ABUNDANCE_FILE)); // file is provided via -raf
                while ((line = in.readLine()) != null) {
                    line = line.trim();
                    if (line.equals("") || line.startsWith("Genome") || line.startsWith("Unmapped")) {
                        continue;
                    }
                    fields = line.split("\\s+");
                    abun = Integer.parseInt(fields[2]);
                    if (abun <= 0)
                        abun = 1;
                    raw_abundance[Integer.parseInt(fields[0])] = abun;
                }
                in.close();
            } catch (IOException ex) {
                Pantools.logger.info(ex.getMessage());
            }
        }
    }

    /**
     * Takes reads from a blocking queue and maps it to the genomes
     */
    @Override
    public void run() {
        int i, mate, genome, counter = 0;
        Iterator<Integer> itr;
        String read_string;
        try (Transaction tx = Globals.GRAPH_DB.beginTx()) {
            while (get_read()) {
                for (mate = 0; mate < num_segments; ++mate) {
                    read_string = fastq_record[mate].getReadString();
                    Utils.reverse_complement(reverse_read[mate], read_string);
                    read_name[mate] = getBaseId(fastq_record[mate].getReadName());
                    find_locations(mate, read_string);
                    itr = genome_numbers.iterator();
                    while (itr.hasNext()) {
                        genome = itr.next();
                        cluster_and_examine_locations(mate, genome, 0, read_string);
                    }
                }
                report_hits();
                ++counter;
                if (counter % 1000 == 0) {
                    System.out.print("\rProcessed " + numberOfReads.getAndAdd(counter) + " reads  ");
                    counter = 0;
                }
            }
            tx.success();
        }

        numberOfReads.getAndAdd(counter);
        numberOfHits.getAndAdd(num_hits);
        numberOfAlignments.getAndAdd(num_alns);
        for (i = 0; i < genome_numbers.size(); ++i) {
            genome = genome_numbers.get(i);
            numSharedMapping[genome].getAndAdd(shared[genome]);
            numUniqueMapping[genome].getAndAdd(unique[genome]);
            numUnmapped[genome].getAndAdd(unmapped[genome]);
        }
        if (Globals.ALIGNMENT_MODE < 0) { // when competitive mapping
            numUnmapped[Globals.adj_total_genomes + 1].getAndAdd(unmapped[Globals.adj_total_genomes + 1]);
        }
    }

    /**
     * Kmerizes the read and collects all the candidate locations read may map in each genome
     *
     * @param mate        The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param read_string
     */
    public void find_locations(int mate, String read_string) {
        Node node = null;
        int i, step, pos, prev_pos = 0, prev_offset = 0, loc_diff, read_len = read_string.length();
        int[] result;
        ListIterator<int[]> itr;
        long cur_index, prev_node_id;
        prev_node_id = -1l;
        step = read_len / Globals.NUM_KMER_SAMPLES;
        step = (step == 0 ? 1 : step);
        initialize_kmer(read_string);
        for (pos = K; pos < read_len; ) {
            cur_index = indexSc.find(current_kmer);
            try {
                if (cur_index != -1l) {
                    indexSc.get_pointer(pointer, cur_index);
                    if (prev_node_id != pointer.node_id) { // to save a bit of time
                        node = Globals.GRAPH_DB.getNodeById(pointer.node_id);
                        prev_node_id = pointer.node_id;
                        prev_pos = pos - K;
                        prev_offset = pointer.offset;
                        node_results.clear();
                        explore_node(node, mate, pos - K, read_len);
                        for (itr = node_results.listIterator(); itr.hasNext(); ) {
                            result = itr.next();
                            locations[mate][result[0]].add(new int[]{result[1], result[2]});
                        }
                    } else {
                        for (itr = node_results.listIterator(); itr.hasNext(); ) {
                            result = itr.next();
                            loc_diff = 0;
                            if (result[2] < 0) {
                                loc_diff += (pos - K - prev_pos);
                            } else {
                                loc_diff -= (pos - K - prev_pos);
                            }
                            if (result[3] < 0) {
                                loc_diff -= (pointer.offset - prev_offset);
                            } else {
                                loc_diff += (pointer.offset - prev_offset);
                            }
                            locations[mate][result[0]].add(new int[]{result[1], result[2] + loc_diff});
                        }
                    }
                }
                if (pos + step >= read_len) {
                    break;
                }
                for (i = 0; i < step; ++i, ++pos) {
                    current_kmer.next_fwd_kmer(Globals.binary[read_string.charAt(pos)] & 3);
                }
            } catch (NotFoundException | ClassCastException ex) {
                //num_exceptions++;
                //Pantools.logger.info(ex.getMessage());
            }
            //Pantools.logger.info(current_kmer.toString());
        }
    }

    public boolean get_read() {
        synchronized (reader) {
            int file_nr = 0;
            try {
                if (reader[0].hasNext()) {
                    fastq_record[0] = reader[0].next();
                    if (paired_end) {
                        if (Globals.INTERLEAVED) {
                            fastq_record[1] = reader[0].next();
                        } else {
                            file_nr = 1;
                            fastq_record[1] = reader[1].next();

                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (SAMException SAMexc) { // error given by fastqreader
                Pantools.logger.info("Error with sample {}. Read starting on line {}. Stopping now.", (file_nr + 1), reader[file_nr].getLineNumber());
                return false;
            } catch (NoSuchElementException nse) { // file 1 is longer than file 2
                System.out.println("\rWARNING! File 1 has more reads as file 2!");
                return false;
            }
        }
    }

    /**
     * Removes the /1 or /2 or spaces from the end of the read ID
     *
     * @param Id
     * @return The base of the read ID
     */
    public String getBaseId(String Id) {
        int slashIdx = Id.indexOf("/");
        int spaceIdx = Id.indexOf(" ");

        if ((slashIdx == -1) && (spaceIdx == -1)) {
            return Id;
        }

        int idx = -1;
        if (slashIdx == -1) {
            idx = spaceIdx;
        } else if (spaceIdx == -1) {
            idx = slashIdx;
        } else {
            idx = spaceIdx < slashIdx ? spaceIdx : slashIdx;
        }

        return Id.substring(0, idx);
    }

    /**
     * Takes the first k-mer of the read
     *
     * @param read The read to be kmerized
     */
    public void initialize_kmer(String read) {
        int i;
        current_kmer.reset();
        for (i = 0; i < K; ++i) {
            current_kmer.next_fwd_kmer(Globals.binary[read.charAt(i)] & 3);
        }
    }

    /**
     * Explores all the incoming edges to a node and collects candidate genomic locations read may map in each genome
     *
     * @param node     A node of gDBG
     * @param mate     The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param position Offset of the sampled kmer in the read
     * @param read_len
     */

    public void explore_node(Node node, int mate, int position, int read_len) {
        int i, loc, offset, node_len;
        long seq_len;
        char side;
        int[] location_array;
        int genome, sequence;
        boolean is_canonical;
        long frequency;
        offset = pointer.offset;
        is_canonical = current_kmer.get_canonical();
        frequency = (long) node.getProperty("frequency");
        if (frequency <= (int) (total_genomes_size / 10000000.0 + num_genomes * 5 * Math.log(total_genomes_size))) {// Filter out high frequent nodes
            node_len = (int) node.getProperty("length");
            // for each incoming edge to the node of the anchor
            for (Relationship r : node.getRelationships(Direction.INCOMING, Globals.RelTypes.FF, Globals.RelTypes.FR, Globals.RelTypes.RF, Globals.RelTypes.RR)) {
                //num_neighbors++;
                side = r.getType().name().charAt(1);
                // for all seuences passing that node
                for (String seq_id : r.getPropertyKeys()) {
                    extract_address(address, seq_id);
                    genome = address[0];
                    if (locations[mate][genome] != null) {// should map against this genome
                        sequence = address[1];
                        // calculate the locations based on the offsets in the node
                        location_array = (int[]) r.getProperty(seq_id);
                        seq_len = sequence_length[genome][sequence];
                        if (side == 'F') {
                            for (i = 0; i < location_array.length; i += 1) {
                                if (pointer.canonical ^ is_canonical) {
                                    loc = location_array[i] + offset - read_len + position + K;
                                    if (loc >= 0 && loc <= seq_len - read_len) {
                                        node_results.add(new int[]{genome, sequence, -(1 + loc), 1});
                                    }
                                    Pantools.logger.trace("F-{}", loc);
                                } else {
                                    loc = location_array[i] + offset - position;
                                    if (loc >= 0 && loc <= seq_len - read_len) {
                                        node_results.add(new int[]{genome, sequence, loc, 1});
                                    }
                                    Pantools.logger.trace("F+{}", loc);
                                }
                            }
                        } else {
                            for (i = 0; i < location_array.length; i += 1) {
                                if (pointer.canonical ^ is_canonical) {
                                    loc = location_array[i] + node_len - K - offset - position;
                                    if (loc >= 0 && loc <= seq_len - read_len) {
                                        node_results.add(new int[]{genome, sequence, loc, -1});
                                    }
                                    Pantools.logger.trace("R+{}", loc);
                                } else {
                                    loc = location_array[i] + node_len - offset - read_len + position;
                                    if (loc >= 0 && loc <= seq_len - read_len) {
                                        node_results.add(new int[]{genome, sequence, -(1 + loc), -1});
                                    }
                                    Pantools.logger.trace("R-{}", loc);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Extracts a genomic address array from a property string. For example array {1, 3} from property G1S3
     *
     * @param address
     * @param property
     */
    public void extract_address(int[] address, String property) {
        int i;
        char ch;
        address[0] = 0;
        address[1] = 0;
        for (i = 1; i < property.length(); ++i)
            if ((ch = property.charAt(i)) != 'S')
                address[0] = address[0] * 10 + ch - 48;
            else
                break;
        for (++i; i < property.length(); ++i)
            address[1] = address[1] * 10 + property.charAt(i) - 48;
    }

    /**
     * Clusters all the candidate genomic locations based on their proximity and align the read to the candidate locations
     *
     * @param mate        The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param genome      The number of genome read is being mapped against
     * @param sholder     The maximum distance between two neighboring candidate locations in clusters
     * @param read_string
     */
    public void cluster_and_examine_locations(int mate, int genome, int sholder, String read_string) {
        int sequence, prev_sequence, prev_start;
        int[] intpair;
        int start, j, k, n, m, count;
        if (locations[mate][genome].size() > 0) {
            locations[mate][genome].sort(int_pair_comp);
            n = locations[mate][genome].size();
            for (j = 0; j < n; ) {
                intpair = locations[mate][genome].get(j);
                prev_sequence = sequence = intpair[0];
                prev_start = intpair[1];
                for (count = 0; j < n; ++j) {
                    intpair = locations[mate][genome].get(j);
                    sequence = intpair[0];
                    start = intpair[1];
                    if (sequence == prev_sequence) {
                        if (start - prev_start > sholder) {
                            hit_counts.add(new int[]{count, prev_start});
                            count = 1;
                            prev_start = start;
                        } else {
                            ++count;
                        }
                    } else {
                        break;
                    }
                    Pantools.logger.trace("{}_{}", sequence, start);
                }
                Pantools.logger.trace("count: {}", count);
                hit_counts.add(new int[]{count, prev_start});
                hit_counts.sort(int_pair_comp);
                m = Math.min(hit_counts.size(), Globals.MAX_NUM_LOCATIONS);
                for (k = 0; k < m; ++k) {
                    Pantools.logger.trace("{} {}", hit_counts.get(k)[0], hit_counts.get(k)[1]);
                    if (sholder == 0) {
                        examine_location(mate, genome, prev_sequence, hit_counts.get(k)[1], read_string);
                    } else {
                        exhaustive_examine_location(mate, genome, prev_sequence, hit_counts.get(k)[1], read_string);
                    }
                    if (hit_counts.get(k)[0] == 1 || (k + 1 < m && hit_counts.get(k)[0] - hit_counts.get(k + 1)[0] > hit_counts.get(k)[0] / 2.0)) {
                        break;
                    }
                }
                hit_counts.clear();
            }
            locations[mate][genome].clear();
        }
    }

    /**
     * Aligns the read to the candidate location of each cluster
     *
     * @param mate        The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param genome      The number of genome read is being mapped against
     * @param sequence    The number of sequence read is being mapped against
     * @param ref_start   The candidate location in the genome
     * @param read_string
     */
    public void examine_location(int mate, int genome, int sequence, int ref_start, String read_string) {
        boolean forward = ref_start >= 0;
        boolean banded_alignment;
        SingleHit h;
        int start, stop, read_len = read_string.length();
        if (!forward) {
            ref_start = -ref_start - 1;
        }
        start = ref_start - Globals.ALIGNMENT_BOUND;
        stop = start + read_len + 2 * Globals.ALIGNMENT_BOUND - 1;
        if (start >= 0 && stop <= sequence_length[genome][sequence] - 1) {
            banded_alignment = true;
        } else if (ref_start >= 0 && ref_start + read_len <= sequence_length[genome][sequence]) {
            start = ref_start;
            stop = start + read_len - 1;
            banded_alignment = false;
        } else {
            return;
        }
        num_hits++;
        reference.setLength(0);
        genomeSc.get_sub_sequence(reference, genome, sequence, start, stop - start + 1, true);
        if (alignments[mate].size() < 2 * Globals.ALIGNMENT_BOUND * read_len &&
                find_similar_subject(mate, genome, sequence, start, forward)) {
            if (valid_hit(read_len)) {
                hits[mate][genome].offer(new SingleHit(alignment_result));
            }
        } else {
            num_alns++;
            perform_alignment(banded_alignment, mate, genome, sequence, start, forward, read_string);
            h = new SingleHit(alignment_result);
            alignments[mate].add(h);
            if (valid_hit(read_len)) {
                hits[mate][genome].offer(h);
            }
        }
    }

    /**
     * Exhaustively aligns the read to a large region around the candidate location. Is used in very sensitive mode.
     *
     * @param mate        The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param genome      The number of genome read is being mapped against
     * @param sequence    The number of sequence read is being mapped against
     * @param ref_start   The candidate location in the genome
     * @param read_string
     */
    public void exhaustive_examine_location(int mate, int genome, int sequence, int ref_start, String read_string) {
        boolean forward = ref_start >= 0;
        SingleHit h;
        int start, stop, read_len = read_string.length();
        if (!forward) {
            ref_start = -ref_start - 1;
        }
        start = Math.max(ref_start - Globals.SHOULDER - read_len, 0);
        stop = Math.min(start + 2 * (read_len + Globals.SHOULDER), (int) sequence_length[genome][sequence] - 1);
        num_hits++;
        reference.setLength(0);
        genomeSc.get_sub_sequence(reference, genome, sequence, start, stop - start + 1, true);
        num_alns++;
        perform_alignment(false, mate, genome, sequence, start, forward, read_string);
        h = new SingleHit(alignment_result);
        if (valid_hit(read_len)) {
            hits[mate][genome].offer(h);
        }
    }

    /**
     * Calls the [banded] Smith-Waterman to align query (read) to the subject (genomic hit)
     *
     * @param banded_alignment Determines if alignment is banded
     * @param mate             The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param genome           The number of genome read is being mapped against
     * @param sequence         The number of sequence read is being mapped against
     * @param start            The candidate location in the genome
     * @param forward          Determines if read should be mapped in forward or in reverse direction
     * @param read_string
     */
    public void perform_alignment(boolean banded_alignment, int mate, int genome, int sequence, int start, boolean forward, String read_string) {
        if (banded_alignment) {
            bounded_aligner.align(forward ? read_string : reverse_read[mate].toString(), reference.toString());
            alignment_result.genome = genome;
            alignment_result.sequence = sequence;
            alignment_result.cigar = bounded_aligner.get_cigar().toString();
            alignment_result.identity = bounded_aligner.get_identity();
            alignment_result.score = bounded_aligner.get_similarity();
            alignment_result.start = start;
            alignment_result.offset = bounded_aligner.get_offset();
            alignment_result.length = bounded_aligner.get_range_length();
            alignment_result.deletions = bounded_aligner.get_deletions();
            alignment_result.forward = forward;
            alignment_result.reference = reference.toString();
        } else {
            aligner.align(forward ? read_string : reverse_read[mate].toString(), reference.toString());
            alignment_result.genome = genome;
            alignment_result.sequence = sequence;
            alignment_result.cigar = aligner.get_cigar().toString();
            alignment_result.identity = aligner.get_identity();
            alignment_result.score = aligner.get_similarity();
            alignment_result.start = start;
            alignment_result.offset = aligner.get_offset();
            alignment_result.length = aligner.get_range_length();
            alignment_result.deletions = aligner.get_deletions();
            alignment_result.forward = forward;
            alignment_result.reference = reference.toString();
        }
    }

    /**
     * Looks for a similar subject (reference) sequence in the list of previous alignments.
     *
     * @param mate      The number of segment in the read (can be 0 for single, 0/1 for paired-end)
     * @param genome    The number of genome read is being mapped against
     * @param sequence  The number of sequence read is being mapped against
     * @param ref_start The candidate location in the genome
     * @param fwd       The direction of the alignment
     * @return True if there is a similar subject in the list of previous alignments, or False.
     */
    public boolean find_similar_subject(int mate, int genome, int sequence, int ref_start, boolean fwd) {
        SingleHit similar_alignment;
        boolean found = false;
        Iterator<SingleHit> itr = alignments[mate].iterator();
        while (itr.hasNext() && !found) {
            similar_alignment = itr.next();
            if (are_equal(similar_alignment.reference, reference)) {
                alignment_result.genome = genome;
                alignment_result.sequence = sequence;
                alignment_result.identity = similar_alignment.identity;
                alignment_result.score = similar_alignment.score;
                alignment_result.start = ref_start;
                alignment_result.offset = similar_alignment.offset;
                alignment_result.length = similar_alignment.length;
                alignment_result.deletions = similar_alignment.deletions;
                alignment_result.forward = fwd;
                alignment_result.cigar = similar_alignment.cigar.toString();
                alignment_result.reference = reference.toString();
                found = true;
            }
        }
        return found;
    }

    /**
     * Determines if an alignment is a valid hit.
     *
     * @return True if the alignment is a valid hit, or False.
     */
    boolean valid_hit(int read_len) {
        return (alignment_result.identity > Globals.MIN_IDENTITY &&
                alignment_result.length >= Globals.MIN_HIT_LENGTH &&
                alignment_result.start + alignment_result.offset >= 0 &&
                alignment_result.start + alignment_result.offset +
                        alignment_result.deletions + read_len
                        <= sequence_length[alignment_result.genome][alignment_result.sequence]);
    }

    /**
     * Determines if two strings are equal.
     *
     * @param s1 The first string
     * @param s2 The second string
     * @return True if two strings are equal, or False
     */
    public boolean are_equal(String s1, StringBuilder s2) {
        boolean are_equal = s1.length() == s2.length();
        for (int i = 0; are_equal && i < s1.length(); ++i)
            if (s1.charAt(i) != s2.charAt(i))
                are_equal = false;
        return are_equal;
    }

    /**
     * Collects and reports all the hits of the read in the genomes.
     */
    public void report_hits() {
        int genome, i;
        if (Globals.ALIGNMENT_MODE < 0) { // pangenomic best
            for (i = 0; i < genome_numbers.size(); ++i) {
                genome = genome_numbers.get(i);
                collect_hits(genome);
            }
            call_mode();
            clear_hits_list();
        } else {
            best_hit[0] = null;
            best_hit[1] = null;
            for (i = 0; i < genome_numbers.size(); ++i) {
                genome = genome_numbers.get(i);
                collect_hits(genome);
                call_mode();
                clear_hits_list();
            }
            if (Globals.VERYSENSITIVE && unmapped_genomes[0].size() > 0)
                check_unmapped_genomes();
            unmapped_genomes[0].clear();
            if (paired_end)
                unmapped_genomes[1].clear();
        }
        alignments[0].clear();
        if (paired_end)
            alignments[1].clear();
    }

    /**
     * Tries to remap an unmapped read to remaining genomes, exhaustively.
     */
    public void check_unmapped_genomes() {
        int i, mate;
        int genome1, genome2;
        SingleHit s;
        Relationship rel;
        Node node, neighbor;
        IndexPointer pointer;
        String read_string;
        int[] loc2;
        int loc1, seq1, seq2, node_len, read_len;
        long high;
        char side1, side2;
        String origin1;
        Iterator<Integer> itr;
        for (mate = 0; mate < num_segments; ++mate) {
            read_string = fastq_record[mate].getReadString();
            read_len = read_string.length();
            s = best_hit[mate];
            if (s != null) {
                genome1 = s.genome;
                seq1 = s.sequence;
                try (Transaction tx = Globals.GRAPH_DB.beginTx()) {
                    loc1 = Math.max(s.start - Globals.SHOULDER, 0);
                    pointer = GenomeLayer.locate(Globals.GRAPH_DB, genomeSc, indexSc, genome1, seq1, loc1);
                    origin1 = "G" + genome1 + "S" + seq1;
                    node = Globals.GRAPH_DB.getNodeById(pointer.node_id);
                    side1 = pointer.canonical ? 'F' : 'R';
                    high = Math.min(s.start + read_len + Globals.SHOULDER - 1, sequence_length[genome1][seq1] - 1);
                    while (loc1 < high) {
                        Pantools.logger.trace(loc1);
                        node_len = (int) node.getProperty("length");
                        for (Relationship r : node.getRelationships(Direction.INCOMING, Globals.RelTypes.FF, Globals.RelTypes.FR, Globals.RelTypes.RF, Globals.RelTypes.RR)) {
                            side2 = r.getType().name().charAt(1);
                            for (String origin2 : r.getPropertyKeys()) {
                                loc2 = (int[]) r.getProperty(origin2);
                                genome2 = Integer.parseInt(origin2.split("S")[0].substring(1));
                                Pantools.logger.trace("{} {}", unmapped_genomes[0].size(), genome2);
                                if (unmapped_genomes[mate].contains(genome2)) {
                                    seq2 = Integer.parseInt(origin2.split("S")[1]);
                                    if (side1 == side2) {
                                        for (i = 0; i < loc2.length; ++i)
                                            locations[mate][genome2].add(new int[]{seq2, loc2[i]});
                                    } else {
                                        for (i = 0; i < loc2.length; ++i)
                                            locations[mate][genome2].add(new int[]{seq2, -loc2[i]});
                                    }
                                }
                            }
                        }
                        loc1 += node_len - K + 1;
                        rel = GenomeLayer.get_outgoing_edge(node, origin1, loc1);
                        if (rel == null)
                            break;
                        else
                            neighbor = rel.getEndNode();
                        node = neighbor;
                        side1 = rel.getType().name().charAt(1);
                    } // while
                    itr = unmapped_genomes[mate].iterator();
                    while (itr.hasNext())
                        cluster_and_examine_locations(mate, itr.next(), Globals.SHOULDER + read_len, read_string);
                    tx.success();
                }
            }
        }
    }

    /**
     * Calls the suitable method based on mapping mode.
     */
    public void call_mode() {
        switch (Math.abs(Globals.ALIGNMENT_MODE)) {
            case 0: // all hits
                report_all_hit(false);
                break;
            case 1: // unique best hits
                report_unique_hit();
                break;
            case 2: // one best hits
                report_one_hit();
                break;
            case 3: // all best hits
                report_all_hit(true);
        }
    }

    /**
     * Collect all the hits of the current read in a genome
     *
     * @param genome The genome for which hits are collected
     */
    public void collect_hits(int genome) {
        int read_len1 = fastq_record[0].getReadLength(), read_len2;
        if (!paired_end) {
            if (hits[0][genome].isEmpty()) {
                unmapped_genomes[0].add(genome);
                single_hits.offer(new SingleHit(genome, 0, 0, -1, -1, 0, 0, 0, true, null, null));
            } else {
                while (!hits[0][genome].isEmpty())
                    single_hits.offer(hits[0][genome].remove());
                SingleHit h = single_hits.peek();
                if (best_hit[0] == null || h.score > best_hit[0].score)
                    best_hit[0] = h;
            }
        } else {
            read_len2 = fastq_record[1].getReadLength();
            boolean reads_paired = false;
            SingleHit h1, h2;
            Iterator<SingleHit> itr1;
            Iterator<SingleHit> itr2;
            int frag_len, best_frag_len;
            if (hits[0][genome].isEmpty() && hits[1][genome].isEmpty()) {
                unmapped_genomes[0].add(genome);
                unmapped_genomes[1].add(genome);
                paired_hits.offer(new PairedHit(Integer.MAX_VALUE, new SingleHit(genome, 0, 0, -1, -1, 0, 0, 0, true, null, null), new SingleHit(genome, 0, 0, -1, -1, 0, 0, 0, true, null, null)));
            } else if (hits[0][genome].isEmpty() && !hits[1][genome].isEmpty()) {
                unmapped_genomes[0].add(genome);
                while (!hits[1][genome].isEmpty())
                    paired_hits.offer(new PairedHit(Integer.MAX_VALUE, new SingleHit(genome, 0, 0, -1, -1, 0, 0, 0, true, null, null), new SingleHit(hits[1][genome].remove())));
                h2 = paired_hits.peek().h2;
                if (best_hit[1] == null || h2.score > best_hit[1].score)
                    best_hit[1] = h2;
            } else if (!hits[0][genome].isEmpty() && hits[1][genome].isEmpty()) {
                unmapped_genomes[1].add(genome);
                while (!hits[0][genome].isEmpty())
                    paired_hits.offer(new PairedHit(Integer.MAX_VALUE, new SingleHit(hits[0][genome].remove()), new SingleHit(genome, 0, 0, -1, -1, 0, 0, 0, true, null, null)));
                h1 = paired_hits.peek().h1;
                if (best_hit[0] == null || h1.score > best_hit[0].score)
                    best_hit[0] = h1;
            } else {
                itr1 = hits[0][genome].iterator();
                while (itr1.hasNext()) {
                    frag_len = best_frag_len = Integer.MAX_VALUE;
                    h1 = itr1.next();
                    itr2 = hits[1][genome].iterator();
                    while (itr2.hasNext()) {
                        h2 = itr2.next();
                        if (h1.sequence != h2.sequence) {
                            continue;
                        }
                        frag_len = fragment_length(h1, h2, read_len1, read_len2);
                        if (frag_len < best_frag_len)
                            best_frag_len = frag_len;
                        else
                            continue;
                        paired_hits.offer(new PairedHit(frag_len, new SingleHit(h1), new SingleHit(h2)));
                        reads_paired = true;
                    }
                }
                if (!reads_paired)
                    paired_hits.offer(new PairedHit(Integer.MAX_VALUE, new SingleHit(hits[0][genome].peek()), new SingleHit(hits[1][genome].peek())));
                h1 = paired_hits.peek().h1;
                if (best_hit[0] == null || h1.score > best_hit[0].score)
                    best_hit[0] = h1;
                h2 = paired_hits.peek().h2;
                if (best_hit[1] == null || h2.score > best_hit[1].score)
                    best_hit[1] = h2;
                hits[0][genome].clear();
                hits[1][genome].clear();
            }
        }
    }

    /**
     * Reports hits which have only one best hit.
     */
    public void report_unique_hit() {
        if (!paired_end) { // single end reads
            SingleHit h, best_hit;
            h = single_hits.remove();
            best_hit = new SingleHit(h);
            if (h.start != -1) {
                if (!single_hits.isEmpty()) {
                    h = single_hits.remove();
                    if (h.score < best_hit.score) {
                        write_single_sam_record(best_hit, 0, 1);
                        unique[best_hit.genome]++;
                    } else {
                        shared[best_hit.genome]++;
                    }
                } else {
                    write_single_sam_record(best_hit, 0, 1);
                    unique[best_hit.genome]++;
                }
            } else {
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1]++;
                }
                write_single_sam_record(best_hit, 4, 0);
            }
        } else {
            PairedHit h, best_hit;
            h = paired_hits.remove();
            best_hit = new PairedHit(h.fragment_length, h.h1, h.h2);
            if (best_hit.get_max_start() != -1) {
                if (!paired_hits.isEmpty()) {
                    h = paired_hits.remove();
                    if (h.get_score() < best_hit.get_score()) {
                        if (best_hit.get_min_start() != -1) {
                            write_paired_sam_record(best_hit, 1, 1, 1, 1);
                            unique[best_hit.h1.genome]++;
                            unique[best_hit.h2.genome]++;
                        } else if (best_hit.h1.start != -1) {
                            write_paired_sam_record(best_hit, 1, 5, 1, 0);
                            unique[best_hit.h1.genome]++;
                            unmapped[best_hit.h2.genome]++;
                        } else if (best_hit.h2.start != -1) {
                            write_paired_sam_record(best_hit, 5, 1, 0, 1);
                            unique[best_hit.h2.genome]++;
                            unmapped[best_hit.h1.genome]++;
                        }
                    } else {
                        if (best_hit.get_min_start() != -1) {
                            write_paired_sam_record(best_hit, 1, 1, 1.0 / (2 + paired_hits.size()), 1.0 / (2 + paired_hits.size()));
                            shared[best_hit.h1.genome]++;
                            shared[best_hit.h2.genome]++;
                        } else if (best_hit.h1.start != -1) {
                            write_paired_sam_record(best_hit, 1, 5, 1.0 / (2 + paired_hits.size()), 0);
                            shared[best_hit.h1.genome]++;
                            unmapped[best_hit.h2.genome]++;
                        } else if (best_hit.h2.start != -1) {
                            write_paired_sam_record(best_hit, 5, 1, 0, 1.0 / (2 + paired_hits.size()));
                            shared[best_hit.h2.genome]++;
                            unmapped[best_hit.h1.genome]++;
                        }
                    }
                } else {
                    if (best_hit.get_min_start() != -1) {
                        write_paired_sam_record(best_hit, 1, 1, 1, 1);
                        unique[best_hit.h1.genome]++;
                        unique[best_hit.h2.genome]++;
                    } else if (best_hit.h1.start != -1) {
                        write_paired_sam_record(best_hit, 1, 5, 1, 0);
                        unique[best_hit.h1.genome]++;
                        unmapped[best_hit.h2.genome]++;
                    } else if (best_hit.h2.start != -1) {
                        write_paired_sam_record(best_hit, 5, 1, 0, 1);
                        unique[best_hit.h2.genome]++;
                        unmapped[best_hit.h1.genome]++;
                    }
                }
            } else { // unmapped
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.h1.genome]++;
                    unmapped[best_hit.h2.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1] += 2;
                }
                write_paired_sam_record(best_hit, 5, 5, 0, 0);
            }
        }
    }

    /**
     * Reports a random best-scored hit. Alignment mode -2 and 2
     */
    public void report_one_hit() {
        if (!paired_end) { // single end reads
            SingleHit h, best_hit;
            int count;
            double rnd, freq = 0, sum_freq = 0;
            best_hit = single_hits.peek();
            for (count = 0; !single_hits.isEmpty(); ++count) {
                h = single_hits.remove();
                if (h.start == -1 || h.score < best_hit.score) {
                    break;
                }
                sum_freq += raw_abundance[h.genome] / genome_sizes[h.genome];
                single_hits_2.add(h);
            }
            rnd = rand.nextDouble();
            while (!single_hits_2.isEmpty()) {
                best_hit = single_hits_2.remove();
                freq += raw_abundance[best_hit.genome] / genome_sizes[best_hit.genome];
                if (rnd < freq / sum_freq)
                    break;
            }
            if (best_hit.start != -1) {
                if (count == 1) {
                    unique[best_hit.genome]++;
                } else {
                    shared[best_hit.genome]++;
                }
                write_single_sam_record(best_hit, 0, 1.0 / count);
            } else {
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1]++;
                }
                write_single_sam_record(best_hit, 4, 0);
            }
        } else { // paired
            PairedHit h, best_hit;
            int count;
            double rnd, freq = 0, sum_freq = 0;
            best_hit = paired_hits.peek();
            for (count = 0; !paired_hits.isEmpty(); ++count) {
                h = paired_hits.remove();
                if (h.get_max_start() == -1 || h.get_score() < best_hit.get_score())
                    break;
                sum_freq += raw_abundance[h.h1.genome] / genome_sizes[h.h1.genome];
                paired_hits_2.add(h);
            }

            rnd = rand.nextDouble();
            while (!paired_hits_2.isEmpty()) {
                best_hit = paired_hits_2.remove();
                freq += raw_abundance[best_hit.h1.genome] / genome_sizes[best_hit.h1.genome];
                if (rnd < freq / sum_freq) {
                    break;
                }
            }
            if (best_hit.get_max_start() != -1) {
                if (best_hit.get_min_start() != -1) {
                    write_paired_sam_record(best_hit, 1, 1, 1.0 / count, 1.0 / count);
                    if (count == 1) {
                        unique[best_hit.h1.genome]++;
                        unique[best_hit.h2.genome]++;
                    } else {
                        shared[best_hit.h1.genome]++;
                        shared[best_hit.h2.genome]++;
                    }
                } else if (best_hit.h1.start != -1) {
                    write_paired_sam_record(best_hit, 1, 5, 1.0 / count, 0);
                    unmapped[best_hit.h2.genome]++;
                    if (count == 1) {
                        unique[best_hit.h1.genome]++;
                    } else {
                        shared[best_hit.h1.genome]++;
                    }
                } else if (best_hit.h2.start != -1) {
                    write_paired_sam_record(best_hit, 5, 1, 0, 1.0 / count);
                    unmapped[best_hit.h1.genome]++;
                    if (count == 1) {
                        unique[best_hit.h2.genome]++;
                    } else {
                        shared[best_hit.h2.genome]++;
                    }
                }
            } else {
                write_paired_sam_record(best_hit, 5, 5, 0, 0);
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.h1.genome]++;
                    unmapped[best_hit.h2.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1] += 2;
                }
            }
        }
    }

    /**
     * Reports all best-scored hits. Alignment mode -3, 0, 3
     *
     * @param best
     */
    public void report_all_hit(boolean best) {
        if (!paired_end) {
            SingleHit h, best_hit;
            int count, prev_genome;
            best_hit = single_hits.peek();
            for (count = 0; !single_hits.isEmpty(); ++count) {
                h = single_hits.remove();
                if (h.start == -1 || (best && h.score < best_hit.score)) {
                    break;
                }
                single_hits_2.add(h);
            }
            if (best_hit.start != -1) {
                prev_genome = -1;
                while (!single_hits_2.isEmpty()) {
                    best_hit = single_hits_2.remove();
                    if (count == 1) {
                        unique[best_hit.genome]++;
                    } else {
                        shared[best_hit.genome]++;
                    }
                    write_single_sam_record(best_hit, best_hit.genome == prev_genome ? 256 : 0, 1.0 / count);
                    prev_genome = best_hit.genome;
                }
            } else {
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1]++;
                }
                write_single_sam_record(best_hit, 4, 0);
            }
        } else { // paired
            PairedHit h, best_hit;
            int count, prev_genome;
            best_hit = paired_hits.peek();
            for (count = 0; !paired_hits.isEmpty(); ++count) {
                h = paired_hits.remove();
                if (h.get_max_start() == -1 || (best && h.get_score() < best_hit.get_score()))
                    break;
                paired_hits_2.add(h);
            }
            if (best_hit.get_max_start() != -1) {
                prev_genome = -1;
                while (!paired_hits_2.isEmpty()) {
                    best_hit = paired_hits_2.remove();
                    if (best_hit.get_min_start() != -1) {
                        write_paired_sam_record(best_hit, best_hit.h1.genome == prev_genome ? 257 : 1,
                                best_hit.h2.genome == prev_genome ? 257 : 1, 1.0 / count, 1.0 / count);
                        if (count == 1) {
                            unique[best_hit.h1.genome]++;
                            unique[best_hit.h2.genome]++;
                        } else {
                            shared[best_hit.h1.genome]++;
                            shared[best_hit.h2.genome]++;
                        }
                    } else if (best_hit.h1.start != -1) {
                        write_paired_sam_record(best_hit, best_hit.h1.genome == prev_genome ? 257 : 1, 5, 1.0 / count, 0);
                        unmapped[best_hit.h2.genome]++;
                        if (count == 1) {
                            unique[best_hit.h1.genome]++;
                        } else {
                            shared[best_hit.h1.genome]++;
                        }
                    } else if (best_hit.h2.start != -1) {
                        write_paired_sam_record(best_hit, 5, best_hit.h2.genome == prev_genome ? 257 : 1, 0, 1.0 / count);
                        unmapped[best_hit.h1.genome]++;
                        if (count == 1) {
                            unique[best_hit.h2.genome]++;
                        } else {
                            shared[best_hit.h2.genome]++;
                        }
                    }
                    prev_genome = best_hit.h1.genome;
                }
            } else {
                write_paired_sam_record(best_hit, 5, 5, 0, 0);
                if (Globals.ALIGNMENT_MODE >= 0) {
                    unmapped[best_hit.h1.genome]++;
                    unmapped[best_hit.h2.genome]++;
                } else { // when competitive mapping
                    unmapped[Globals.adj_total_genomes + 1] += 2;
                }
            }
        }
    }

    /**
     * Clears data structures of the collected hits
     */
    public void clear_hits_list() {
        if (!paired_end) {
            single_hits.clear();
            single_hits_2.clear();
        } else {
            paired_hits.clear();
            paired_hits_2.clear();
        }
    }

    /**
     * Writes the SAM record for single-end mapping
     *
     * @param h       The hit to be reported
     * @param flag    The SAM flag of the hit
     * @param p_value
     */
    public void write_single_sam_record(SingleHit h, int flag, double p_value) {
        if (sam_writers != null) {
            SAMRecord sam_record = new SAMRecord(null);
            if (h.start == -1) {
                sam_record.setReadName(read_name[0]);
                sam_record.setFlags(flag);
                sam_record.setReadString(fastq_record[0].getReadString());
            } else {
                flag |= h.forward ? 0 : 16; // direction
                sam_record.setReferenceName(sequence_titles[h.genome][h.sequence].split("\\s")[0]);
                sam_record.setFlags(flag);
                sam_record.setReadName(read_name[0]);
                sam_record.setAlignmentStart(h.start + h.offset + 1);
                sam_record.setMappingQuality((int) Math.ceil(p_value * -10 * Math.log10(1 - (h.identity - 0.000001))));
                sam_record.setCigarString(h.cigar);
                sam_record.setReadString(h.forward ? fastq_record[0].getReadString() : reverse_read[0].toString());
                sam_record.setBaseQualityString(fastq_record[0].getBaseQualityString());
            }
            if (Globals.READ_GROUP != null) {
                sam_record.setAttribute(SAMTag.RG, Globals.READ_GROUP.getReadGroupId());
            }
            synchronized (sam_writers[h.genome]) {
                if (flag == 4 && Globals.ALIGNMENT_MODE < 0) {
                    sam_writers[Globals.adj_total_genomes + 1].addAlignment(sam_record);
                } else {
                    sam_writers[h.genome].addAlignment(sam_record);
                }
            }
        }
    }

    /**
     * Writes the SAM record of a paired-end hit
     *
     * @param h        The hit to be reported
     * @param flag1    the initial SAM flag of read 1
     * @param flag2    the initial SAM flag of read 2
     * @param p_value1
     * @param p_value2
     */
    public void write_paired_sam_record(PairedHit h, int flag1, int flag2, double p_value1, double p_value2) {

        if (sam_writers == null) { // when --out-format is 0
            return;
        }
        // the first segment
        SAMRecord sam_record1, sam_record2;
        sam_record1 = new SAMRecord(null);
        sam_record2 = new SAMRecord(null);
        int position1 = h.h1.start + h.h1.offset + 1;
        int position2 = h.h2.start + h.h2.offset + 1;

        String chr_name_1 = "";
        String chr_name_2 = "";

        if (h.h1.start != -1) {
            chr_name_1 = sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0];
        }
        if (h.h2.start != -1) {
            chr_name_2 = sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0];
        }
        flag1 |= 64;
        if (h.h2.start == -1) {
            flag1 |= 8;
        } else {
            flag1 |= !h.h2.forward ? 32 : 0; // SEQ being reverse complemented
        }

        if (h.h1.start == -1) {
            sam_record1.setReadName(read_name[0]);
            sam_record1.setFlags(flag1);
            sam_record1.setReadString(fastq_record[0].getReadString());
            if (h.h2.start != -1) {
                sam_record1.setReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                sam_record1.setAlignmentStart(position2);
                sam_record1.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
                sam_record1.setMateAlignmentStart(position2);
            }
        } else {
            if (h.h1.start != -1 && h.h2.start != -1) {
                if (chr_name_1.equals(chr_name_2)) {
                    if (h.h1.forward != h.h2.forward) {
                        if (h.fragment_length < Integer.MAX_VALUE) {
                            flag1 |= 2;
                        }
                    }
                }
            }

            flag1 |= h.h1.forward ? 0 : 16; // SEQ being reverse complemented

            //qname
            sam_record1.setReadName(read_name[0]);
            //flag
            sam_record1.setFlags(flag1);
            //rname
            sam_record1.setReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
            //pos
            sam_record1.setAlignmentStart(position1);
            //mapq
            sam_record1.setMappingQuality((int) Math.ceil(p_value1 * -10 * Math.log10(1 - (h.h1.identity - 0.000001))));
            //sam_record.setMappingQuality((int)Math.round(h.identity1 * 100));
            //cigar
            sam_record1.setCigarString(h.h1.cigar);
            //rnext
            if (h.h1.start != -1 && h.h2.start != -1 && !chr_name_1.equals(chr_name_2)) {
                sam_record1.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
            } else {
                sam_record1.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
            }
            //pnext
            if (h.h2.start == -1) {
                sam_record1.setMateAlignmentStart(position1);
            } else {
                sam_record1.setMateAlignmentStart(position2);
            }
            //tlen
            if (h.h1.start != -1 && h.h2.start != -1 && !chr_name_1.equals(chr_name_2)) {
                sam_record1.setInferredInsertSize(0);
            } else if (h.h2.start == -1) {
                sam_record1.setInferredInsertSize(0);
            } else if (position2 > position1) {
                sam_record1.setInferredInsertSize(position2 - position1 + fastq_record[0].getReadLength());
            } else {
                sam_record1.setInferredInsertSize(-(position1 - position2 + fastq_record[1].getReadLength()));
            }
            //seq
            sam_record1.setReadString((h.h1.forward ? fastq_record[0].getReadString() : reverse_read[0]).toString());
            //qual
            sam_record1.setBaseQualityString(fastq_record[0].getBaseQualityString());
        }
        if (Globals.READ_GROUP != null) {
            sam_record1.setAttribute(SAMTag.RG, Globals.READ_GROUP.getReadGroupId());
        }

        // the second segment
        flag2 |= 128;

        if (h.h1.start == -1) {
            flag2 |= 8;
        } else {
            flag2 |= !h.h1.forward ? 32 : 0; // SEQ being reverse complemented
        }

        if (h.h2.start == -1) { // not mapped
            sam_record2.setReadName(read_name[1]);
            sam_record2.setFlags(flag2);
            sam_record2.setReadString(fastq_record[1].getReadString());
            if (h.h1.start != -1) {
                sam_record2.setReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                sam_record2.setAlignmentStart(position1);
                sam_record2.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
                sam_record2.setMateAlignmentStart(position1);
            }

        } else {
            if (h.h1.start != -1 && h.h2.start != -1 && chr_name_1.equals(chr_name_2)) {
                if (h.h1.forward != h.h2.forward) {
                    if (h.fragment_length < Integer.MAX_VALUE) {
                        flag2 |= 2;
                    }
                }
            }

            flag2 |= h.h2.forward ? 0 : 16; // SEQ being reverse complemented
            //qname
            sam_record2.setReadName(read_name[1]);
            //flag
            sam_record2.setFlags(flag2);
            //rname
            sam_record2.setReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
            //pos
            sam_record2.setAlignmentStart(position2);
            //mapq
            sam_record2.setMappingQuality((int) Math.ceil(p_value2 * -10 * Math.log10(1 - (h.h2.identity - 0.000001))));
            //sam_record.setMappingQuality((int)Math.round(h.identity2 * 100));
            //cigar
            sam_record2.setCigarString(h.h2.cigar);
            //rnext
            if (h.h1.start != -1 && h.h2.start != -1 && !chr_name_1.equals(chr_name_2)) {
                sam_record2.setMateReferenceName(sequence_titles[h.h1.genome][h.h1.sequence].split("\\s")[0]);
            } else {
                sam_record2.setMateReferenceName(sequence_titles[h.h2.genome][h.h2.sequence].split("\\s")[0]);
            }
            //pnext
            if (h.h1.start == -1) {
                sam_record2.setMateAlignmentStart(position2);
            } else {
                sam_record2.setMateAlignmentStart(position1);
            }
            //tlen
            if (h.h1.start != -1 && h.h2.start != -1 && !chr_name_1.equals(chr_name_2)) {
                sam_record2.setInferredInsertSize(0);
            } else if (h.h1.start == -1) {
                sam_record2.setInferredInsertSize(0);
            } else if (position2 > position1) {
                sam_record2.setInferredInsertSize(-(position2 - position1 + fastq_record[0].getReadLength()));
            } else {
                sam_record2.setInferredInsertSize(position1 - position2 + fastq_record[1].getReadLength());
            }
            //seq
            sam_record2.setReadString((h.h2.forward ? fastq_record[1].getReadString() : reverse_read[1]).toString());
            //qual
            sam_record2.setBaseQualityString(fastq_record[1].getBaseQualityString());
        }
        if (Globals.READ_GROUP != null) {
            sam_record2.setAttribute(SAMTag.RG, Globals.READ_GROUP.getReadGroupId());
        }
        synchronized (sam_writers[h.h1.genome]) {
            if (Globals.ALIGNMENT_MODE < 0 && flag1 == 77 && flag2 == 141) { // both reads are unmapped & competitive mapping
                sam_writers[Globals.adj_total_genomes + 1].addAlignment(sam_record1);
                sam_writers[Globals.adj_total_genomes + 1].addAlignment(sam_record2);
            } else {
                sam_writers[h.h1.genome].addAlignment(sam_record1);
                sam_writers[h.h2.genome].addAlignment(sam_record2);
            }
        }
    }

    /**
     * Calculates length of the fragment of a paired-end hit
     *
     * @param h1        Hit of the first segment
     * @param h2        Hit of the second segment
     * @param read_len1 Length of the first read
     * @param read_len2 Length of the first read
     * @return The proper length of the fragment, or Infinity if it is invalid.
     */
    public int fragment_length(SingleHit h1, SingleHit h2, int read_len1, int read_len2) {
        int frag_len;
        int position1, position2;
        position1 = h1.start + h1.offset;
        position2 = h2.start + h2.offset;
        if (h1.forward) {
            frag_len = position2 + fastq_record[1].getReadLength() - position1;
        } else {
            frag_len = position1 + fastq_record[0].getReadLength() - position2;
        }

        if (frag_len < Math.max(read_len1, read_len2) || frag_len > Globals.MAX_FRAGMENT_LENGTH) {
            frag_len = Integer.MAX_VALUE;
        }
        return frag_len;
    }
}