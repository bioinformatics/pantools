package nl.wur.bif.pantools.analysis.sequence_visualization;

import nl.wur.bif.pantools.analysis.gene_classification.GeneClassification;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.repeats.Repeats;
import nl.wur.bif.pantools.utils.Utils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.determine_sequence_overlap_in_AL;
import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Utils.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class SequenceVisualization {

    PhasedFunctionalities pf;

    /**
     * Still uses globals:
     * compare_sequences (will give warning in preparePhasedGenomeInfo)
     *
     * sequence_visualization()
     *
     * what is required?
     * 1. gene_classification: core, accessory, unique
     * 1. gene_classification: haplotype copies
     *
     */
    public void sequenceVisualization(int windowSize, Path rulesFile) {
        Pantools.logger.info("Preparing sequence/chromosome plots.");
        pf = new PhasedFunctionalities();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            stop_if_panproteome("sequence_visualization"); // retrieves K_size & total_genomes
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_current_grouping_version(); // check which version of homology grouping is active
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            return;
        }

        //TODO move to CLI class
        if (windowSize == 500000) {
            Pantools.logger.info("--window-size is set to 500.000 bp (default)");
        } else {
            Pantools.logger.info("--window-size is set to " + windowSize + " bp");
        }

        ArrayList<String> sequenceSpecificPlotRules = readSeqVisualizationInputFile(rulesFile);
        ArrayList<String> plotRules = checkIfSequenceSpecificRules(sequenceSpecificPlotRules);
        if (plotRules == null) {
            plotRules = sequenceSpecificPlotRules;
            sequenceSpecificPlotRules = null;
            Pantools.logger.info("Generating new annotation bars for all sequences in selection.");
        } else {
            Pantools.logger.info("Found sequence specific annotation bars.");
            Pantools.logger.info(plotRules);
            Pantools.logger.info(sequenceSpecificPlotRules);

        }
        ArrayList<String> sequenceSelection = findSequencesInPlotRules(rulesFile); // is null when visualizing sequences from chromosome
        //System.out.println(sequenceSelection);
        // sequenceSelection is different from selectedSequences.
        // - sequenceSelection are the only sequences that are visualized
        // - selectedSequences are all sequences included for calculations, e.g. checking if a gene is present on another sequence

        createDirectory("sequence_visualization/dnds/", true);
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(0, false);
        int nrPhasedGenomes = pf.preparePhasedGenomeInformation(true, selectedSequences);

        HashMap<String, ArrayList<String>> sequencesPerChromosomeMap = createSequencesPerChromosomeMapString();
        if (sequenceSelection == null) {
            if (nrPhasedGenomes == 0) {
                Pantools.logger.error("Sequences need to be specified in the --rules file for unphased pangenomes.");
                throw new RuntimeException("Missing sequences parameter in rules file.");
            }
            removeUnphasedSequences(sequencesPerChromosomeMap);
        }

        if (sequenceSelection == null) {
            System.out.print("\r" + selectedSequences.size() + " sequences selected.");
            System.out.println("\r Phasing information found for " + nrPhasedGenomes + " genome(s). Plots are created per chromosome      ");
        } else {
            System.out.println("\r" + sequenceSelection.size() + " sequences selected: " + sequenceSelection.toString().replace("[","").replace("]","") + "       ");
        }

        createSeqVisualizationDirectorties();

        HashMap<String, Integer> sequenceLengthMap = getSequenceLengths(selectedSequences); // holds all keys for selectedsequences
        //prepareVariantDensityPlots(windowSize, sequenceLengthMap);

        Pantools.logger.info("1/7 Preparing synteny coordinates.");
        createSyntenyInput(sequenceSelection);
        HashMap<String, Node[]> mrnaNodesPerSequence = pf.getOrderedMrnaNodesPerSequence(false,
                true, sequenceSelection); // key is sequence identifier, value is array with 'gene' nodes
        updateSequenceSelectionBasedOnNoGenes(mrnaNodesPerSequence, sequenceSelection == null);

        if (sequenceSelection != null) {
            createSharedBetweenForSeqVisualization(mrnaNodesPerSequence, sequenceLengthMap, plotRules);
        }

        if (plotRules.contains("gene_coverage")) {
            Pantools.logger.info("2/7 Preparing gene coverage per window.");
            geneDensityForSeqVisualization(sequenceLengthMap, mrnaNodesPerSequence, windowSize);
        }

        if (plotRules.contains("repeat_coverage")) {
            Pantools.logger.info("3/7 Preparing repeat coverage per window.");
            repeatDensityForSeqVisualization(windowSize);
        }
        if (plotRules.contains("other_chromosomes")) {
            Pantools.logger.info("4/7 Checking number of gene copies within chromosome.");
            checkIfGeneCopiesWithinGenome(mrnaNodesPerSequence, sequencesPerChromosomeMap);
            convertBarPlotCoordinates(mrnaNodesPerSequence, sequenceLengthMap, "single_chromosome");
        }

        int genomesInGClassification = 0;
        if (plotRules.contains("gene_classification")) {
            genomesInGClassification = checkNrGenomesGeneClassification();
            Pantools.logger.info("5/7 Checking gene classification output.");
            Pantools.logger.info(" Bar is based on earlier gene_classification analysis of {} genomes", genomesInGClassification);
            createCAUInput(mrnaNodesPerSequence, genomesInGClassification, sequenceLengthMap);
        }

        if (plotRules.contains("haplotype_presence")) {
            Pantools.logger.info("6/7 Counting haplotype copies.");
            createHaplotypeCopiesInput(mrnaNodesPerSequence, sequenceLengthMap);
        }
        createCAUHaplotypeRscripts(mrnaNodesPerSequence, genomesInGClassification);

        Pantools.logger.info("7/7. Creating Rscripts.");
        longest_transcripts = true;
        writeStringToFile(selectedSequences.toString().replace("[", "").replace("]", "").replace(" ", ""),
                "sequence_visualization/selected_sequences.info", true, false);

        if (sequenceSelection == null) {
            createPhasedChromosomeVisualization(mrnaNodesPerSequence, sequenceLengthMap, genomesInGClassification, plotRules);
            Pantools.logger.info("Generate the output files using:");
            Pantools.logger.info(" sh {}sequence_visualization/run_visualization_scripts.sh", WORKING_DIRECTORY);
        } else {
            createSeqVisualization(sequenceLengthMap, plotRules, sequenceSelection, sequenceSpecificPlotRules);
            Pantools.logger.info("Generate the visualization:");
            Pantools.logger.info(" Rscript {}sequence_visualization/scripts/plot_sequences.R", WORKING_DIRECTORY);
        }
    }

    private void createSeqVisualizationDirectorties(){
        createDirectory("sequence_visualization/variants/SNP/", true);
        createDirectory("sequence_visualization/gene/coverage/", true);
        createDirectory("sequence_visualization/gene/density/", true);
        createDirectory("sequence_visualization/repeat/density", true);
        createDirectory("sequence_visualization/repeat/coverage", true);
        createDirectory("sequence_visualization/gene_classification", true);
        createDirectory("sequence_visualization/between_selection/", true);
        createDirectory("sequence_visualization/haplotype_presence", true);
        createDirectory("sequence_visualization/scripts/", true);
        createDirectory("sequence_visualization/output_figures/", true);
        createDirectory("sequence_visualization/single_chromosome", true);
        createDirectory("sequence_visualization/synteny/", true);
    }

    /**
     * if a sequence selection was made by the user. All sequences must have genes
     * if no selection was made. only stop the program if none of the sequences have genes
     * @param sequenceSelection true when a specific sequence selection was made
     */
    private void updateSequenceSelectionBasedOnNoGenes(HashMap<String, Node[]> mrnaNodesPerSequence, boolean sequenceSelection) {
        ArrayList<String> sequencesWithoutGenes = new ArrayList<>();
        for (String sequenceId : selectedSequences) {
            if (!mrnaNodesPerSequence.containsKey(sequenceId)) {
                sequencesWithoutGenes.add(sequenceId);
            }
        }
        if (sequenceSelection && !sequencesWithoutGenes.isEmpty()) {
            System.out.println("\r" + sequencesWithoutGenes.size() + "/" + selectedSequences.size() + " of the input sequences do not have gene annotations");
        }
        for (String sequenceId : sequencesWithoutGenes) {
            selectedSequences.remove(sequenceId);
        }
        if (selectedSequences.isEmpty()) {
            throw new RuntimeException("None of the selected sequences have gene annotations");
        }
    }

    /**
     *
     * @param plotRules
     * @return return null when not all rules are specific for a sequence
     */
    private ArrayList<String> checkIfSequenceSpecificRules(ArrayList<String> plotRules) {
        ArrayList<String> updatedPlotRules = new ArrayList<>();
        boolean changesMade = false, allRulesBetween = true;
        for (String rule : plotRules) {
            if (rule.startsWith("sequence")) { // the sequence selection rule.
                return null;
            }
            if (!rule.contains("between")) { // is always sequence specific
                allRulesBetween = false;
            }
            if (rule.contains(",") && !rule.contains("between")) {
                String[] ruleArray = rule.split(",");
                if (!updatedPlotRules.contains(ruleArray[0])) {
                    changesMade = true;
                    updatedPlotRules.add(ruleArray[0]);
                }
            } else if (!updatedPlotRules.contains(rule)) {
                updatedPlotRules.add(rule);
            }
        }
        if (!changesMade && !allRulesBetween) {
            return null;
        }
        return updatedPlotRules;
    }

    private void prepareVariantDensityPlots(int windowSize, HashMap<String, Integer> sequenceLengthMap) {
        Path directoryPath = Paths.get(WORKING_DIRECTORY + "alignments/sequence/output/");
        File directory = directoryPath.toFile();
        String[] allFiles = directory.list();
        int counter = 0;
        for (String fileName : allFiles) {
            counter++;
            if (counter % 100 == 0) {
                System.out.print("\rReading VCF files: " + counter + "/" + allFiles.length);
            }
                if (fileName.contains(".")) {
                    continue;
                }
                //System.out.println(fileName);
                //String outputName = WORKING_DIRECTORY + "sequence_visualization/variants/SNP/" + fileName.replace("vcf","csv");
                String outputName = WORKING_DIRECTORY + "sequence_visualization/variants/SNP/" + fileName + ".vcf";
                if (checkIfFileExists(outputName)) {
                    continue;
                }

                String[] fileNameArray = fileName.split("_"); // [1S7, 1S8.vcf]
                String sequenceId = fileNameArray[0].replace("S","_");
                int sequenceLength = sequenceLengthMap.get(sequenceId);
                int totalWindows = (int) ((double) sequenceLength / windowSize) + 1; // number of windows is rounded down, therefore +1;

                int [][] SNPCounts = new int [totalWindows][2];
                Path syriVCF = Paths.get(WORKING_DIRECTORY + "alignments/sequence/output/" + fileName + "/syri.vcf");
                countVariantsInVCF(syriVCF, windowSize, SNPCounts);

                StringBuilder rInput = new StringBuilder("position,type,density\n");
                for (int i = 0; i < SNPCounts.length; i++) { // i is a genome number
                    int position = (i * windowSize) +1;
                    rInput.append(position + ",ts," + SNPCounts[i][0] + "\n");
                    rInput.append(position + ",tv," + SNPCounts[i][1] + "\n");
                }
                //writeStringToFile(rInput.toString(), outputName, false, false );
            //}
        }

        boolean test = true;
        if (test){
            System.exit(0);
        }
    }


    /**
     * Generate the summary output as paftools would
     * @param vcfFile
     * @return
     */
    private int[] countVariantsInVCF(Path vcfFile, int windowSize, int [][] SNPCounts) {
        int[] variantCounts = new int[13]; // [ substitutions, ts, tv, del_1, ins_1, del_2, ins_2, del_3_50, ins_3_50, del_50_1000, ins_50_1000, del_1000plus, ins_1000plus]
        try {
            BufferedReader br = Files.newBufferedReader(vcfFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.contains("#")) {
                    continue;
                }
                String[] line_array = line.split("\t");
                int position = Integer.parseInt(line_array[1]);
                int windowNr = position/windowSize;

                int ref_length = line_array[3].length();
                int alt_length = line_array[4].length();
                int difference = ref_length - alt_length;
                if (ref_length == 1 && alt_length == 1) {
                    variantCounts[0]++;
                    if (line_array[3].equals("A") || line_array[3].equals("G")) {
                        if (line_array[4].equals("A") || line_array[4].equals("G")) {
                            variantCounts[1]++;
                            SNPCounts[windowNr][0] ++;
                        } else {
                            variantCounts[2]++;
                            SNPCounts[windowNr][1] ++;
                        }
                    } else { // is a C or T
                        if (line_array[4].equals("C") || line_array[4].equals("T")) {
                            variantCounts[1]++;
                            SNPCounts[windowNr][0] ++;
                        } else {
                            variantCounts[2]++;
                            SNPCounts[windowNr][1] ++;
                        }
                    }
                } else if (difference == -1) {
                    variantCounts[4]++;
                } else if (difference == 1) {
                    variantCounts[3]++;
                } else if (difference == -2) {
                    variantCounts[6]++;
                } else if (difference == 2) {
                    variantCounts[5]++;
                } else if (difference < -2 && difference > -50) {
                    variantCounts[8]++;
                } else if (difference > 2 && difference < 50) {
                    variantCounts[7]++;
                } else if (difference > 49 && difference < 1000) {
                    variantCounts[9]++;
                } else if (difference < -49 && difference > -1000) {
                    variantCounts[10]++;
                } else if (difference > 1000) {
                    variantCounts[11]++;
                } else if (difference < -1000) {
                    variantCounts[12]++;
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("\nSomething went wrong while reading: " + vcfFile + "\n");
        }
        return variantCounts;
    }

    private void addOverlappingSyntenyBlocks(TreeSet<Integer> block_sizes,
                                                   HashMap<Integer, ArrayList<int[]>> addresses_per_blocksize,
                                                   HashMap<Integer, ArrayList<int[]>> target_addresses_per_blocksize,
                                                   StringBuilder r_input, AtomicInteger block_nr, String forward) {

        for (int block_size : block_sizes) {
            ArrayList<int[]> addresses = addresses_per_blocksize.get(block_size);
            ArrayList<int[]> target_addresses = target_addresses_per_blocksize.get(block_size);

            for (int i = 0; i < addresses.size(); i++) { // i is a genome number
                int[] address = addresses.get(i);
                int[] target_address = target_addresses.get(i);
                //System.out.print(Arrays.toString(address) + " " + Arrays.toString(target_address));
                r_input.append(target_address[0]).append(",0,").append(block_nr.get()).append(",minor").append(forward).append("\n")
                        .append(target_address[1]).append(",0,").append(block_nr.get()).append(",minor").append(forward).append("\n")
                        .append(address[1]).append(",1,").append(block_nr.get()).append(",minor").append(forward).append("\n")
                        .append(address[0]).append(",1,").append(block_nr.get()).append(",minor").append(forward).append("\n");
                block_nr.incrementAndGet();
            }
        }
    }

    /**
     *
     * @param sequenceSelection the sequences to be visualized
     */
    public void createSyntenyInput(ArrayList<String> sequenceSelection) {
        HashSet<String> sequenceCombinations = new HashSet<>();
        HashMap<String, ArrayList<int[]>> map = new HashMap<>();
        HashMap<String, ArrayList<int[]>> inverted_map = new HashMap<>();
        System.out.print("\rRetrieving synteny blocks: ");
        int counter = 0;
        boolean syntenyFound = false;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> syntenyNodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
            while (syntenyNodes.hasNext()) {
                Node syntenyNode = syntenyNodes.next();
                counter++;
                if (counter % 100 == 0) {
                    System.out.print("\rRetrieving synteny: " + counter + " blocks ");
                }
                syntenyFound = true;
                String seq_ids = (String) syntenyNode.getProperty("sequence_identifiers"); // example 2_3,2_22
                String[] seq_ids_array = seq_ids.split(","); // 2_3,2_22 becomes [2_3, 2_22]
                if (sequenceSelection != null && !sequenceSelection.isEmpty() &&
                        (!sequenceSelection.contains(seq_ids_array[0]) || !sequenceSelection.contains(seq_ids_array[1]))) {
                    continue;
                }

                if (!selectedSequences.contains(seq_ids_array[0]) || !selectedSequences.contains(seq_ids_array[1])){
                    continue;
                }

                sequenceCombinations.add(seq_ids);
                boolean inverted = (boolean) syntenyNode.getProperty("block_inverted");
                int block_size = (int) syntenyNode.getProperty("block_size");
                Iterable<Relationship> relations = syntenyNode.getRelationships();
                int seq1_start = 0;
                int seq2_start = 0;
                int seq1_end = 0;
                int seq2_end = 0;
                for (Relationship relation : relations) {
                    int position = (int) relation.getProperty("position");
                    if (position == 0) {
                        Node mrna_node = relation.getStartNode();
                        int[] address = (int[]) mrna_node.getProperty("address");
                        if ((address[0] + "_" + address[1]).equals(seq_ids_array[0])) {
                            seq1_start = address[2];
                        } else {
                            seq2_start = address[2];
                        }
                    } else if (position == block_size - 1) {
                        Node mrna_node = relation.getStartNode();
                        int[] address = (int[]) mrna_node.getProperty("address");
                        if ((address[0] + "_" + address[1]).equals(seq_ids_array[0])) {
                            seq1_end = address[3];
                        } else {
                            seq2_end = address[3];
                        }
                    }
                }
                //System.out.println(inverted + " " + seq1_start + "-" + seq1_end + " " + seq2_start + "-" + seq2_end);
                int[] seq1_start_stop = new int[]{seq1_start, seq1_end};
                int[] seq2_start_stop;
                if (inverted) {
                    seq2_start_stop = new int[]{seq2_end, seq2_start};
                    inverted_map.computeIfAbsent(seq_ids, k -> new ArrayList<>()).add(seq1_start_stop);
                    inverted_map.computeIfAbsent(seq_ids_array[1] + "," + seq_ids_array[0], k -> new ArrayList<>()).add(seq2_start_stop);
                } else {
                    seq2_start_stop = new int[]{seq2_start, seq2_end};
                    map.computeIfAbsent(seq_ids, k -> new ArrayList<>()).add(seq1_start_stop);
                    map.computeIfAbsent(seq_ids_array[1] + "," + seq_ids_array[0], k -> new ArrayList<>()).add(seq2_start_stop);
                }
            }
            tx.success();
        }

        if (!syntenyFound) {
            throw new RuntimeException("No synteny was added to the pangenome yet");
        }

        for (String sequenceCombination : sequenceCombinations) {
            String renamedSeqCombi = sequenceCombination.replace("_","S").replace(",","_");
            String majorFile = "sequence_visualization/synteny/" + renamedSeqCombi + "_major_blocks.csv";
            String allFile = "sequence_visualization/synteny/" + renamedSeqCombi + "_all_blocks.csv";
            if (checkIfFileExists(WORKING_DIRECTORY + majorFile) && checkIfFileExists(WORKING_DIRECTORY + allFile)) {
                continue;
            }
            AtomicInteger block_nr = new AtomicInteger();
            StringBuilder r_input = new StringBuilder("x,y,block_nr,type\n");
            StringBuilder r_minor_input = new StringBuilder();
            if (map.containsKey(sequenceCombination)) {
                HashMap<Integer, ArrayList<int[]>> addresses_per_blocksize = new HashMap<>();
                HashMap<Integer, ArrayList<int[]>> target_addresses_per_blocksize = new HashMap<>();
                TreeSet<Integer> block_sizes = order_blocks_by_size(sequenceCombination, map,
                        addresses_per_blocksize, target_addresses_per_blocksize);
                getNonOverlappingSyntenyBlocks(block_sizes, addresses_per_blocksize, target_addresses_per_blocksize, sequenceCombination,
                        false, "", r_input, block_nr);
                addOverlappingSyntenyBlocks(block_sizes, addresses_per_blocksize, target_addresses_per_blocksize,
                        r_minor_input, block_nr, "");
            }

            if (inverted_map.containsKey(sequenceCombination)) {
                HashMap<Integer, ArrayList<int[]>> addresses_per_blocksize = new HashMap<>();
                HashMap<Integer, ArrayList<int[]>> target_addresses_per_blocksize = new HashMap<>();
                TreeSet<Integer> block_sizes = order_blocks_by_size(sequenceCombination, inverted_map, addresses_per_blocksize,
                        target_addresses_per_blocksize);
                getNonOverlappingSyntenyBlocks(block_sizes, addresses_per_blocksize, target_addresses_per_blocksize, sequenceCombination,
                        true, "RV", r_input, block_nr);
                addOverlappingSyntenyBlocks(block_sizes, addresses_per_blocksize, target_addresses_per_blocksize,
                        r_minor_input, block_nr, "RV");
            }

            writeStringToFile(r_input.toString(), majorFile, true, false);
            writeStringToFile(r_input.toString() + r_minor_input.toString(),
                    "sequence_visualization/synteny/" +  renamedSeqCombi + "_all_blocks.csv", true, false);
            swapCoordinatesSyntenyGgplot(WORKING_DIRECTORY + majorFile);
            swapCoordinatesSyntenyGgplot(WORKING_DIRECTORY + allFile);
        }
    }

    private void getNonOverlappingSyntenyBlocks(TreeSet<Integer> block_sizes,
                                                      HashMap<Integer, ArrayList<int[]>> addresses_per_blocksize,
                                                      HashMap<Integer, ArrayList<int[]>> target_addresses_per_blocksize,
                                                      String seq_combination, boolean append, String forward,
                                                      StringBuilder r_input, AtomicInteger block_nr) {

        String[] seq_combi_array = seq_combination.split(",");
        String[] seq_id1_array = seq_combi_array[0].split("_");
        int genome1_nr = Integer.parseInt(seq_id1_array[0]);
        int sequence1_nr = Integer.parseInt(seq_id1_array[1]);
        int sequence_end = (int) GENOME_DB.sequence_length[genome1_nr][sequence1_nr] - 1;
        r_input.append(sequence_end).append(",0,").append(block_nr.getAndIncrement()).append(",major\n");

        TreeSet<Integer> block_sizes_reverted = (TreeSet<Integer>) block_sizes.descendingSet();
        ArrayList<Integer> remove_block_sizes = new ArrayList<>();
        //System.out.println(sequence_end + " end. " + block_sizes_reverted);
        ArrayList<int[]> nonOverlappingAddresses = new ArrayList<>();
        ArrayList<int[]> target_nonOverlappingAddresses = new ArrayList<>();

        for (int block_size : block_sizes_reverted) {
            ArrayList<int[]> addresses = addresses_per_blocksize.get(block_size);
            ArrayList<int[]> target_addresses = target_addresses_per_blocksize.get(block_size);
            boolean all_added = true;
            for (int i = 0; i < addresses.size(); i++) {
                int[] address = addresses.get(i);
                int[] target_address = target_addresses.get(i);
                int start_pos1 = address[0];
                int end_pos1 = address[1];
                boolean overlap_found = false;
                for (int[] address2 : nonOverlappingAddresses) {
                    int start_pos2 = address2[0];
                    int end_pos2 = address2[1];
                    if (start_pos2 <= start_pos1 && end_pos2 >= end_pos1) { // complete overlap
                        overlap_found = true;
                        //System.out.println(" 1");
                    } else if (start_pos2 >= start_pos1 && end_pos2 <= end_pos1) { // complete overlap
                        overlap_found = true;
                        //System.out.println(" 2");
                    } else if (start_pos2 >= start_pos1 && end_pos2 >= end_pos1 && start_pos2 <= end_pos1) { // last part overlaps
                        overlap_found = true;
                        //System.out.println(" 3");
                    } else if (start_pos2 <= start_pos1 && end_pos2 <= end_pos1 && end_pos2 >= start_pos1 ) { // first part overlaps
                        overlap_found = true;
                        //System.out.println(" 4");
                    }
                }
                if (!overlap_found) {
                    nonOverlappingAddresses.add(address);
                    target_nonOverlappingAddresses.add(target_address);
                } else {
                    all_added = false;
                }
            }
            if (all_added) {
                remove_block_sizes.add(block_size);
            }
        }
        for (int block_size : remove_block_sizes) {
            block_sizes.remove(block_size);
        }

        for (int i = 0; i < nonOverlappingAddresses.size(); i++) { // i is a genome number
            int[] address = nonOverlappingAddresses.get(i);
            int[] target_address = target_nonOverlappingAddresses.get(i);
            //System.out.print(Arrays.toString(address) + " " + Arrays.toString(target_address));
            r_input.append(target_address[0]).append(",0,").append(block_nr.get()).append(",major").append(forward).append("\n")
                    .append(target_address[1]).append(",0,").append(block_nr.get()).append(",major").append(forward).append("\n")
                    .append(address[1]).append(",1,").append(block_nr.get()).append(",major").append(forward).append("\n")
                    .append(address[0]).append(",1,").append(block_nr.get()).append(",major").append(forward).append("\n");
            block_nr.incrementAndGet();
        }
        //System.out.println("");
        //String renamed_seq_combi = seq_combination.replace("_","S").replace(",","_");
    }

    private TreeSet<Integer> order_blocks_by_size(String seq_combi, HashMap<String, ArrayList<int[]>> map,
                                                        HashMap<Integer, ArrayList<int[]>> addresses_per_blocksize,
                                                        HashMap<Integer, ArrayList<int[]>> target_addresses_per_blocksize) {

        String[] seq_combi_array = seq_combi.split(",");
        ArrayList<int[]> addresses1 = map.get(seq_combi);
        ArrayList<int[]> addresses2 = map.get(seq_combi_array[1] + "," + seq_combi_array[0]);
        //System.out.println(seq_combi + " " + addresses1.size() + " " + addresses2.size());
        TreeSet<Integer> block_sizes = new TreeSet<>();
        for (int i = 0; i < addresses1.size(); i++) { // i is a genome number
            int[] address1 = addresses1.get(i);
            int[] address2 = addresses2.get(i);
            //System.out.println(" " + Arrays.toString(address1)  + " " + (address1[1]-address1[0]) + " " + Arrays.toString(address2));
            int block_size = (address1[1]-address1[0]) +1;
            block_sizes.add(block_size);
            addresses_per_blocksize.computeIfAbsent(block_size, k -> new ArrayList<>()).add(address1);
            target_addresses_per_blocksize.computeIfAbsent(block_size, k -> new ArrayList<>()).add(address2);
        }
        return block_sizes;
    }

    /**
     * Each block consists of 4 lines
     *
     * x,y,block_nr,type
     * 41244705,0,0,major
     * 16379,0,1,major
     * 25979961,0,1,major
     * 26285264,1,1,major
     * 11156677,0,39,majorRV
     * 11649724,0,39,majorRV
     * 11119750,1,39,majorRV
     * 10619868,1,39,majorRV
     * @param inputFile String, ends with / sequence combination / syn_ggplot(_major).csv
     */
    private void swapCoordinatesSyntenyGgplot(String inputFile) {
        String[] inputArray = inputFile.split("/");
        StringBuilder outputFileBuilder = new StringBuilder();
        for (int i = 0; i < inputArray.length; i++) {
            if (i == inputArray.length-1) { // last
                String[] seqCombiArray = inputArray[i].split("_");
                outputFileBuilder.append(seqCombiArray[1]).append("_").append(seqCombiArray[0]).append("_")
                        .append(seqCombiArray[2]).append("_").append(seqCombiArray[3]);
            } else {
                outputFileBuilder.append(inputArray[i]);
            }
            outputFileBuilder.append("/");
        }
        String outputFile = outputFileBuilder.toString().replaceFirst(".$",""); // remove last /
        String line;
        boolean firstLine = true;
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile))) {
            BufferedReader in = new BufferedReader(new FileReader(inputFile));
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                line = line.trim();
                if (firstLine) {
                    out.write(line + "\n");
                    firstLine = false;
                } else { // change the second value
                    String[] line_array = line.split(",");
                    String oneZero = "1";
                    if (line_array[1].equals("1")) {
                        oneZero = "0";
                    }
                    out.write(line_array[0] + "," + oneZero + "," + line_array[2] + "," + line_array[3] + "\n");
                }
            }
        }  catch (IOException ioe) {
            throw new RuntimeException("Failed to read and write:\n"
                    + inputFile + "\n"
                    + outputFile + "\n");

        }
    }

    /**
     * When no sequence selection is made, only create plots of homologous chromosomes (with phasing information).
     * Remove unphased sequences from selection
     * @param sequencesPerChromosomeMap
     */
    private void removeUnphasedSequences(HashMap<String, ArrayList<String>> sequencesPerChromosomeMap) {
        ArrayList<String> sequencesWithPhasing = new ArrayList<>();
        for (String chromosomeKey : sequencesPerChromosomeMap.keySet()) {
            if (chromosomeKey.endsWith("_0")) {
                continue;
            }
            ArrayList<String> sequenceIds = sequencesPerChromosomeMap.get(chromosomeKey);
            sequencesWithPhasing.addAll(sequenceIds);
        }

        ArrayList<String> seqIdsToBeRemoved = new ArrayList<>();
        for (String sequenceId : selectedSequences) {
            if (!sequencesWithPhasing.contains(sequenceId)){
                seqIdsToBeRemoved.add(sequenceId);
            }
        }

        for (String sequenceId : seqIdsToBeRemoved) {
            selectedSequences.remove(sequenceId);
            String[] seqIdArray = sequenceId.split("_"); // 2_22 becomes [2, 22]
            int genomeNr = Integer.parseInt(seqIdArray[0]);
            int sequenceNr = Integer.parseInt(seqIdArray[1]);
            skip_seq_array[genomeNr-1][sequenceNr-1] = true;
        }
        System.out.println("\rRemoved " + seqIdsToBeRemoved.size() + " sequences from current sequence selection                          ");
    }

    /**
     *
     * @return  key is a chromosome key: genome number + "_" + chromosome number. value is list of sequence identifiers
     */
    private HashMap<String, ArrayList<String>> createSequencesPerChromosomeMapString() {
        HashMap<String, ArrayList<String>> sequencesPerChromosomeMap = new HashMap<>();
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] seqIdArray = sequenceId.split("_");
            String[] phasingInfo = phasingInfoMap.get(sequenceId);
            sequencesPerChromosomeMap.computeIfAbsent(seqIdArray[0] + "_" + phasingInfo[0], k -> new ArrayList<>()).add(sequenceId);
        }
        return sequencesPerChromosomeMap;
    }


    /**
     * Example file 1:
     *
     * sequence 4_1,3_3,3_4,4_1
     * haplotype_presence
     *
     * Example file 2:
     *
     * between,4_1,3_3
     * haplotype_presence,3_3
     * haplotype_presence,3_4
     * between,4_1,3_4
     *
     *
     * @param rulesFile
     * @return
     */
    private ArrayList<String> findSequencesInPlotRules(Path rulesFile) {
        if (rulesFile == null) {
            return null;
        }
        ArrayList<String> sequencesTovisualize = new ArrayList<>();
        try {
            BufferedReader br = Files.newBufferedReader(rulesFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.startsWith("sequence")) {
                    String[] array = line.split(" ");
                    String[] array2 = array[1].split(",");
                    sequencesTovisualize.addAll(Arrays.asList(array2));
                }  else if (line.contains("between")) { // // example line "between,4_1,3_4". first sequence ID is the reference
                    String[] lineArray = line.split(",");
                    sequencesTovisualize.add(lineArray[1]);
                } else if (line.contains(",")) { //
                    String[] lineArray = line.split(","); // sequence is added after the rule
                    sequencesTovisualize.add(lineArray[1]);
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to read the file included with --rules: " + rulesFile);
        }

        if (sequencesTovisualize.isEmpty()) {
            return null;
        }
        return sequencesTovisualize;
    }

    /**
     * Example input file:
     *
     * gene_classification
     * gene_coverage
     * repeat_coverage
     * haplotype_presene
     * other_chromosomes
     *
     * @return list with rules
     */
    private ArrayList<String> readSeqVisualizationInputFile(Path rulesFile) {
        ArrayList<String> rules = new ArrayList<>();
        if (rulesFile == null) {
            Pantools.logger.info("No --rules were defined. Trying to include all bar types: gene_coverage, repeat_coverage, other_chromosomes, gene_classification, haplotype_presence");
            rules.add("all");
            rules.add("gene_coverage");
            rules.add("repeat_coverage");
            rules.add("other_chromosomes");
            rules.add("gene_classification");
            rules.add("haplotype_presence");
        } else {
            try {
                BufferedReader br = Files.newBufferedReader(rulesFile);
                for (String line = br.readLine(); line != null; line = br.readLine()) {
                    if (line.startsWith("gene_classification")) {
                        rules.add(line);
                    } else if (line.startsWith("other_chromosomes")) {
                        rules.add(line);
                    } else if (line.startsWith("gene_coverage")) {
                        rules.add(line);
                    } else if (line.startsWith("repeat_coverage")) {
                        rules.add(line);
                    } else if (line.startsWith("haplotype_presence")) {
                        rules.add(line);
                    } else if (line.startsWith("sequence")) {
                        rules.add(line);
                    } else if (line.startsWith("all")) {
                        rules.add("gene_coverage");
                        rules.add("repeat_coverage");
                        rules.add("other_chromosomes");
                        rules.add("gene_classification");
                        rules.add("haplotype_presence");
                    } else if (line.startsWith("between")) { // example line between,4_1,3_4
                        String[] lineArray = line.split(",");
                        if (lineArray.length < 3) {
                            throw new RuntimeException("Expected at least two sequences (or genomes), separated by a comma.");
                        }
                        rules.add(line);
                    }
                }
            } catch (IOException ioe) {
                throw new RuntimeException("Unable to read the file included with --rules: " + rulesFile);
            }
           Pantools.logger.info("--rules was included. Trying to include: " + rules.toString().replace("[","").replace("]",""));
        }

        if (rules.size() == 0) {
            throw new RuntimeException("No (correct) rules were included in the --rules file");
        }
        if (rules.size() == 1 && rules.get(0).startsWith("sequence") ) {
            rules.add("gene_coverage");
            rules.add("repeat_coverage");
            rules.add("other_chromosomes");
            rules.add("gene_classification");
            rules.add("haplotype_presence");
        }
        return rules;
    }

    /**
     * Calculate the gene density per Mb for windows of 500Kb
     * @param sequenceLengthMap
     * @param mrnaNodesPerSeq
     */
    private void geneDensityForSeqVisualization(HashMap<String, Integer> sequenceLengthMap,
                                               HashMap<String, Node[]> mrnaNodesPerSeq, int windowSize) {

        HashMap<String, ArrayList<int[]>> addressesPerSequence = null;
        boolean first = true;
        for (String sequenceId : selectedSequences) {
            String outputPath = WD_full_path + "sequence_visualization/gene/";
            if (!checkIfFileExists(outputPath + "coverage/" + sequenceId + ".csv")) {
                if (first) {
                    first = false;
                    addressesPerSequence = get_gene_addresses_per_seq(mrnaNodesPerSeq);
                }
                long sequenceLength = sequenceLengthMap.get(sequenceId);
                ArrayList<int[]> mrnaAddresses = addressesPerSequence.get(sequenceId);
                calculateDensityCoveragePerWindow(windowSize, sequenceLength, mrnaAddresses, outputPath, "gene", sequenceId);
            }
        }
    }

    /**
     * A simpler version for genes as there are not different gene types
     * @param windowLength
     * @param sequenceLength length
     * @param addresses
     * @param type
     * @param sequenceId
     */
    private void calculateDensityCoveragePerWindow(int windowLength, long sequenceLength,
                                                  ArrayList<int[]> addresses, String outputPath, String type, String sequenceId) {

        StringBuilder density_builder = new StringBuilder("position,type,density\n");
        StringBuilder coverage_builder = new StringBuilder("position,type,coverage\n");
        int windowNr = 0, last_position = 0;
        double part_of_MB = 1000000 / (double) windowLength;
        int total_windows = (int) ((double) sequenceLength / windowLength) + 1; // number of windows is rounded down, therefore +1;

        while (windowNr < total_windows) {
            int window_start_position = (windowNr * windowLength) +1;
            int window_end_position = ((windowNr+1) * windowLength);
            windowNr++;
            int counts_in_window = 0, coverage_of_window = 0;
            ArrayList<Integer> overlappingGenePositions = new ArrayList<>();
            for (int j = last_position; j < addresses.size(); j++) {
                int[] address = addresses.get(j);
                int[] window_ovl = check_if_overlap(window_start_position, window_end_position, address[0], address[1]);
                if (window_ovl[0] > 0) { // repeat overlaps with the window
                    counts_in_window++;
                    overlappingGenePositions.add(window_ovl[0]);
                    overlappingGenePositions.add(window_ovl[1]);
                }
                last_position = j;
                if (address[0] > window_end_position) { // start of gene is behind the end of the window
                    determine_sequence_overlap_in_AL(overlappingGenePositions);
                    for (int i = 0; i < overlappingGenePositions.size()-1; i+=2) {
                        coverage_of_window += (overlappingGenePositions.get(i+1)-overlappingGenePositions.get(i)+1);
                    }
                    overlappingGenePositions.clear();
                    if (last_position > 10) {
                        last_position = j - 10; // go back 10 position to get genes/repeats that start in earlier window but end in the next
                    } else {
                        last_position = 0;
                    }
                    break;
                }
            }
            int density_per_MB = (int) (counts_in_window * part_of_MB); // round the number
            String percentageWindowCovered = get_percentage_str(coverage_of_window, windowLength, 2);
            density_builder.append(window_start_position).append(",").append(type).append(",").append(density_per_MB).append("\n");
            coverage_builder.append(window_start_position).append(",").append(type).append(",").append(percentageWindowCovered).append("\n");
        }
        write_SB_to_file_full_path(density_builder, outputPath + "density/" + sequenceId + ".csv");
        write_SB_to_file_full_path(coverage_builder, outputPath + "coverage/" + sequenceId + ".csv");
    }

    /**
     *
     * @param ref_start_position
     * @param ref_end_position
     * @param target_start_position
     * @param target_end_position
     * @return
     */
    private int[] check_if_overlap(int ref_start_position, int ref_end_position, int target_start_position, int target_end_position) {
        int[] overlapped_positions = new int[2];
        String overlap = "nothing";
        if (target_start_position <= ref_start_position && target_end_position >= ref_end_position) { // complete overlap
            overlap = "complete1 " + ref_start_position + " " + ref_end_position;
            overlapped_positions[0] = ref_start_position;
            overlapped_positions[1] = ref_end_position;
        } else if (target_start_position >= ref_start_position && target_end_position <= ref_end_position) { // complete overlap
            overlap = "complete2 " + target_start_position + " " + target_end_position;
            overlapped_positions[0] = target_start_position;
            overlapped_positions[1] = target_end_position;
        } else if (target_start_position >= ref_start_position && target_end_position >= ref_end_position && target_start_position < ref_end_position) { // last part overlaps
            overlap = "last part " + target_start_position + " " + ref_end_position;
            overlapped_positions[0] = target_start_position;
            overlapped_positions[1] = ref_end_position;
        } else if (target_start_position <= ref_start_position &&  target_end_position <= ref_end_position && target_end_position > ref_start_position) { // first part overlaps
            overlap = "first part " + ref_start_position + " " + target_end_position;
            overlapped_positions[0] = ref_start_position;
            overlapped_positions[1] = target_end_position;
        }
        if (!overlap.equals("nothing")) {
            //System.out.println("   " + type + " " + overlap);
        }
        return overlapped_positions;
    }

    private void repeatDensityForSeqVisualization(int windowSize) {
        Repeats repeatClass = new Repeats();
        ArrayList<String> sequencesWithoutOutput = new ArrayList<>();
        for (String sequenceId : selectedSequences) {
            if (!checkIfFileExists(Paths.get(WORKING_DIRECTORY + "sequence_visualization/repeat/coverage/" + sequenceId + ".csv"))) {
                sequencesWithoutOutput.add(sequenceId);
            }
        }
        if (sequencesWithoutOutput.isEmpty()) {
            Pantools.logger.warn(" Repeat files were already prepared.");
            return;
        }

        skip.updateSkipArraysSequenceSelection(sequencesWithoutOutput); // to only retrieve the gene order of the target sequences
        HashSet<String> repeat_types = repeatClass.retrieveRepeatTypes(selectedSequences);
        ArrayList<String> selected_repeat_types = repeatClass.updateRepeatTypesForAnalysis(repeat_types, null);
        boolean check_types = false;
        if (repeat_types.size() != selected_repeat_types.size()) {
            check_types = true;
        }
        HashMap<String, Node[]> repeatNodesPerSequence = repeatClass.getRepeatNodesPerSequenceAscending(check_types,
                selected_repeat_types, selectedGenomes, selectedSequences); // get repeat nodes
        if (!repeatNodesPerSequence.isEmpty()) {
            //createDirectory("repeats/windows", true);
            HashMap<String, ArrayList<int[]>> repeatAddressesPerSequence = repeatClass.getRepeatAddressesPerSequence(repeatNodesPerSequence); // get the address per node
            HashMap<String, ArrayList<String>> repeat_types_per_seq = repeatClass.get_repeat_types_per_seq(repeatNodesPerSequence); // get the repeat type
            repeatClass.calcateRepeatDensityCoverageForWindows(windowSize, selected_repeat_types, repeatAddressesPerSequence,
                    repeat_types_per_seq, WORKING_DIRECTORY + "sequence_visualization/repeat/", selectedSequences);
        } else if (selectedSequences.size() == sequencesWithoutOutput.size()) {
            System.out.println("\r No repeats found (for sequence selection)");
        }
        skip.updateSkipArraysSequenceSelection(new ArrayList<>(selectedSequences)); // return skip_array to original state
    }

    private int checkNrGenomesGeneClassification() {
        Path geneClassificationOverview = Paths.get(WORKING_DIRECTORY + "gene_classification/gene_classification_overview.txt");
        if (!geneClassificationOverview.toFile().exists()) {
            Pantools.logger.info("No gene_classification output available. Unable to create gene classification bar!");
            return 0;
        }
        int numberOfGenomes = 0;
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(geneClassificationOverview.toFile()))) {
            while ((line = in.readLine()) != null) {
                if (line.startsWith("Genomes in analysis:") || line.startsWith("Total genomes:") || line.startsWith("Genomes:") ) { // it differs between pantools versions
                    String[] lineArray = line.split(": ");
                    String[] lineArray2 = lineArray[1].split("");
                    numberOfGenomes = Integer.parseInt(lineArray2[0]);
                    break;
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Something went wrong while reading: " + geneClassificationOverview);
        }
        if (numberOfGenomes == 0) { // file was read but no genomess? something is wrong
            throw new RuntimeException("Error for developer. Number of genomes should be at least 1");
        }
        return numberOfGenomes;
    }

    /**
     * for every mrna, go to the homology node and check if the sequence
     * @param mrnaNodesPerSequence
     */
    private void checkIfGeneCopiesWithinGenome(HashMap<String, Node[]> mrnaNodesPerSequence,
                                              HashMap<String, ArrayList<String>> sequencesPerChromosomeMap) {

        if (phasingInfoMap == null || sequencesPerChromosomeMap.isEmpty()) { // no phasing
            System.out.println("\r No phasing information found (for the sequence selection)");
            return;
        }
        String selectedStr = PhasedFunctionalities.readOneLineFile(WORKING_DIRECTORY + "sequence_visualization/selected_sequences.info", false);
        String currentselectedStr = selectedSequences.toString().replace("[","").replace("]","").replace(" ","");
        boolean redoCalculation = true;
        if (selectedStr.equals(currentselectedStr)) {
            redoCalculation = false;
        }

        int seqCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String sequenceId : mrnaNodesPerSequence.keySet()) {
                seqCounter++;
                System.out.print("\r Checking chromosome presence: " + seqCounter + "/" + mrnaNodesPerSequence.size() + "  ");
                String[] phasingInfo = phasingInfoMap.get(sequenceId); // example [1, D, 1D, 1_D]
                if (phasingInfo == null) {
                    continue;
                }
                if (checkIfFileExists(WORKING_DIRECTORY + "sequence_visualization/single_chromosome/" + sequenceId + ".csv") && !redoCalculation) {
                    continue; // skip if done earlier
                }
                Node[] mrnaNodes = mrnaNodesPerSequence.get(sequenceId);
                String[] seqIdArray = sequenceId.split("_");
                int genomeNr = Integer.parseInt(seqIdArray[0]);
                StringBuilder singleCopyBuilder = new StringBuilder("x,y,block_nr,type\n");
                int blockNr = 0;
                for (Node mrnaNode : mrnaNodes) {
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    Node hmNode = mrnaNode.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING).getStartNode();
                    int[] copyNumberArray = (int[]) hmNode.getProperty("copy_number");
                    String inOtherChromosome = "Other chromosome";
                    if (copyNumberArray[genomeNr] == 1) { // only found once in genome
                        inOtherChromosome = "Single copy";
                    } else { // found multiple times, check if it occurs in same chromosome
                        if (!hmNode.hasProperty("copy_number_genome_" + genomeNr)) {
                            Pantools.logger.warn("This plot requires you to run gene_classification2 with the --sequence argument.");
                            return;
                        }
                        int[] sequenceCopyNumberArray = (int[]) hmNode.getProperty("copy_number_genome_" + genomeNr);
                        inOtherChromosome = "Single copy"; // this is new
                        ArrayList<String> chromosomeSeqIdentifiers = sequencesPerChromosomeMap.get(genomeNr + "_" + phasingInfo[0]);
                        for (int i = 0; i < sequenceCopyNumberArray.length; i++) {
                            if (!selectedSequences.contains(genomeNr + "_" + (i+1))){
                                continue;
                            }
                            if (sequenceCopyNumberArray[i] > 0 && !chromosomeSeqIdentifiers.contains(genomeNr + "_" + (i+1) )) {
                                inOtherChromosome = "Other chromosome";
                                break;
                            }
                        }
                    }

                    singleCopyBuilder.append(address[2]).append(",0,").append(blockNr).append(",").append(inOtherChromosome).append("\n");
                    singleCopyBuilder.append(address[2]).append(",5,").append(blockNr).append(",").append(inOtherChromosome).append("\n");
                    singleCopyBuilder.append(address[3]).append(",5,").append(blockNr).append(",").append(inOtherChromosome).append("\n");
                    singleCopyBuilder.append(address[3]).append(",0,").append(blockNr).append(",").append(inOtherChromosome).append("\n");
                    blockNr++;
                }
                writeStringToFile(singleCopyBuilder.toString(), "sequence_visualization/single_chromosome/" + sequenceId + ".csv", true, false);
            }
            tx.success();
        }
        System.out.println("");
    }

    /**
     * Plots (and scripts) for the entire genome are only generated if phasing info is available
     * @param mrnaNodesPerSequence
     * @param genomesInGClassification
     */
    private void createCAUHaplotypeRscripts(HashMap<String, Node[]> mrnaNodesPerSequence, int genomesInGClassification) {
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return;
        }
        for (int genomeNr : selectedGenomes) { // i is a genome number
            ArrayList<String> sequenceIdentifiers = new ArrayList<>();
            for (String sequenceId : selectedSequences) {
                if (!sequenceId.startsWith(genomeNr + "_") || !mrnaNodesPerSequence.containsKey(sequenceId)) {
                    continue;
                }
                sequenceIdentifiers.add(sequenceId);
            }

            sequenceIdentifiers = pf.sequencesInChromosomePhasingOrder(sequenceIdentifiers);
            Collections.reverse(sequenceIdentifiers);
            String[] axis_breaks_labels = create_glassification_ggplot_axis_break(sequenceIdentifiers);
            if (genomesInGClassification > 1) {
                create_CAU_bar_rscript(genomeNr, axis_breaks_labels);
            }

            // check if the genome is phased. if not, don't create rscript
            if (!checkIfFileExists(WORKING_DIRECTORY + "sequence_visualization/haplotype_presence/genome_" + genomeNr + ".csv")){
                continue;
            }
            HashSet<String> phases = new HashSet<>();
            for (String sequenceId : sequenceIdentifiers) {
                if (phasingInfoMap.containsKey(sequenceId)) {
                    String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
                    if (phasingInfo[1].equals("unphased")) {
                        continue;
                    }
                    phases.add(phasingInfo[1]);
                }
            }
            if (phases.size() >= 2) {
                if (phases.size() > 8) {
                    throw new RuntimeException("Currently only able to visualize 8 phases, not " + phases.size() + ":" + phases);
                }
                create_haplotype_copies_rscript(genomeNr, axis_breaks_labels, phases.size());
            }
        }
    }

    private void create_CAU_bar_rscript(int genomeNr, String[] axisBreaksLabels) {
        StringBuilder rscript = new StringBuilder();
        String R_LIB = check_r_libraries_environment();
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#The next line can be ignored assuming the R libraries were installed via Conda. "
                        + "If not, use it to manually install the required R package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"scales\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("\n")
                .append("#df = read.csv(\"").append(WD_full_path).append("sequence_visualization/gene_classification/genome_").append(genomeNr).append("_no_white.csv\" , header= TRUE)\n")
                .append("df = read.csv(\"").append(WD_full_path).append("sequence_visualization/gene_classification/genome_").append(genomeNr).append("_extended.csv\" , header= TRUE)\n")
                .append("#df = read.csv(\"").append(WD_full_path).append("sequence_visualization/gene_classification/genome_").append(genomeNr).append(".csv\" , header= TRUE)\n")
                .append("\n")
                .append("sequence_bars <- ggplot() +\n")
                .append("    geom_polygon(data=df, mapping=aes(x=x, y=y, group=block_nr, fill=type)) +\n")
                .append("    theme_classic(base_size=14) +\n")
                .append("    labs(x = \"Genomic position\", y = \"Sequence\") +\n")
                .append("    scale_fill_manual(values=c(\"Accessory\" = \"#4363d8\", \"Core\" = \"#3cb44b\", \"Unique\" = \"#e6194B\")) +\n")
                .append("    # scale_fill_manual(values=c(\"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\")) + # colourblind-friendly palette\n")
                .append("    scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6)) +\n")
                .append("    # theme(legend.position = \"none\") + # uncomment to exclude the legend\n")
                .append("\n")
                .append("# Two different y-axes possible\n")
                .append("# 1. Show a tick for for every sequence. Uncomment the first 'scale_y_continuous' line (default). \n")
                .append("# 2. Show only one tick for sequences belonging to the same chromosome. Block the first 'scale_y_continuous' by commenting it out and uncomment the second line.\n\n")
                .append("    #").append(axisBreaksLabels[0]).append("\n")
                .append("     ").append(axisBreaksLabels[1]).append("\n")
                .append("ggsave(sequence_bars, file=\"").append(WD_full_path).append("sequence_visualization/output_figures/genome_").append(genomeNr).append("_class.png\", width = 25, height = 15, units = \"cm\")\n");
        writeStringToFile(rscript.toString(), "sequence_visualization/scripts/genome_" + genomeNr + "_class.R", true, false);
    }

    /**
     *
     * @param genomeNr a genome number
     * @param axis_breaks_labels length of 2. breaks & labels without and with chromosome number
     * @param numberOfPhases cannot exceed 8
     */
    private void create_haplotype_copies_rscript(int genomeNr, String[] axis_breaks_labels, int numberOfPhases) {
        StringBuilder rscript = new StringBuilder();
        String R_LIB = check_r_libraries_environment();

        StringBuilder colorManual = new StringBuilder("scale_fill_manual(values=c(");
        String[] haplotypeColors = new String[]{"\"1 haplotype\" = \"#363636\"" , "\"2 haplotypes\" = \"#56B4E9\"", "\"3 haplotypes\" = \"#E69F00\"",
            "\"4 haplotypes\" = \"#009E73\"", "\"5 haplotype\" = \"#F0E442\"", "\"6 haplotypes\" = \"#0072B2\"", "\"7 haplotypes\" = \"#D55E00\"", "\"8 haplotypes\" = \"#CC79A7\""};

        for (int i = 1; i <= numberOfPhases; i++) {
            colorManual.append(haplotypeColors[i - 1]).append(", ");
        }
        String colorManualStr = colorManual.toString().replaceFirst(".$","").replaceFirst(".$","") + "))";
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#The next line can be ignored assuming the R libraries were installed via Conda. "
                        + "If not, use it to manually install the required R package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"scales\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("\n")
                .append("#df = read.csv(\"").append(WD_full_path).append("sequence_visualization/haplotype_presence/genome_").append(genomeNr).append("_no_white.csv\", header= TRUE)\n")
                .append("df = read.csv(\"").append(WD_full_path).append("sequence_visualization/haplotype_presence/genome_").append(genomeNr).append("_extended.csv\", header= TRUE)\n")
                .append("#df = read.csv(\"").append(WD_full_path).append("sequence_visualization/haplotype_presence/genome_").append(genomeNr).append(".csv\", header= TRUE)\n")
                .append("\n")
                .append("sequence_bars <- ggplot() +\n")
                .append("    geom_polygon(data=df, mapping=aes(x=x, y=y, group=block_nr, fill=type)) +\n")
                .append("    theme_classic(base_size=14) +\n")
                .append("    labs(x = \"Genomic position\", y = \"Sequence\") +\n")
                .append("    scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6)) +\n")
                .append("    ").append(colorManualStr).append(" +\n")
                .append("    # theme(legend.position = \"none\") + # uncomment this line to exclude the legend\n")
                .append("\n")
                .append("# Two different y-axes possible\n")
                .append("# 1. Show a tick for for every sequence. Uncomment the first 'scale_y_continuous' line (default).\n")
                .append("# 2. Show only one tick for sequences belonging to the same chromosome. Block the first 'scale_y_continuous' by commenting it out and uncomment the second line.\n\n")
                .append("    #").append(axis_breaks_labels[0]).append("\n")
                .append("     ").append(axis_breaks_labels[1]).append("\n")
                .append("\n")
                .append("ggsave(sequence_bars, file=\"").append(WD_full_path).append("sequence_visualization/output_figures/genome_").append(genomeNr).append("_phasing.png\", " +
                        "width = 25, height = 15, units = \"cm\")\n");
        writeStringToFile(rscript.toString(), "sequence_visualization/scripts/genome_" + genomeNr + "_phasing.R", true, false);
    }

    private void addSeqVisualizationRscriptHeader(StringBuilder rscript) {
        String R_LIB = check_r_libraries_environment();
        rscript.append("#!/usr/bin/env Rscript\n\n")
                .append("#The next line can be ignored assuming the R libraries were installed via Conda. "
                        + "If not, use it to manually install the required R package\n")
                .append("#install.packages(\"ggplot2\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"scales\", lib=\"").append(R_LIB).append("\")\n")
                .append("#install.packages(\"cowplot\", lib=\"").append(R_LIB).append("\")\n")
                .append("library(ggplot2)\n")
                .append("library(scales)\n")
                .append("library(cowplot)\n\n")
                .append("# Sequences in plots:\n");
    }

    private int determineLongestSequence(ArrayList<String> sequenceIdentifiers,
                                               HashMap<String, Integer> sequence_lengths) {
        int longest_sequence = 0;
        for (String sequenceId : sequenceIdentifiers) {
            longest_sequence = Math.max(longest_sequence, sequence_lengths.get(sequenceId)); // take the highest value
        }
        return longest_sequence;
    }

    /**
     * for an unkwnown reason some homology_group nodes are not connected to the mRNA's. Use this function for now
     */
    public HashMap<String, Long> createMrnaHomologyMap(int genomeNr) {
        HashMap<String, Long> mrnaId_hmgroup_map = new HashMap<>();
        String hmGroupingOutput = WORKING_DIRECTORY + "pantools_homology_groups.txt";
        String line;
        try (BufferedReader in = new BufferedReader(new FileReader(hmGroupingOutput))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                if (line.startsWith("#")) {
                    continue;
                }
                String[] line_array = line.trim().split(" ");
                String hmgroup_id_str = line_array[0].replace(":","");
                long hmgroup_id = Long.parseLong(hmgroup_id_str);
                for (int i = 1; i < line_array.length; i++) { // skip the first value (hmgroup id)
                    if (!line_array[i].endsWith("#" + genomeNr)) {
                        continue;
                    }
                    //long mrna_id = Long.parseLong(line_array[i]);
                    mrnaId_hmgroup_map.put(line_array[i], hmgroup_id);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read: {}", hmGroupingOutput);
            System.exit(1);
        }
        return mrnaId_hmgroup_map;
    }

    /**
     * the repeat line plot is dependant on the highest gene density of the chromosome
     * @param rscript
     * @param geneInputFile
     * @param repeatInputFile
     * @param longest_sequence
     * @param number
     * @param firstLastNames String[]
     */
    private void appendGeneRepeatCoverageGgplot(StringBuilder rscript, String geneInputFile, String repeatInputFile,
                                                int longest_sequence, int number, String[] firstLastNames) {

        StringBuilder input = new StringBuilder();
        StringBuilder geomLine = new StringBuilder();
        boolean genesPresent = checkIfFileExists(geneInputFile);
        boolean repeatsPresent = checkIfFileExists(repeatInputFile);
        if (genesPresent) { // should always be present
            input.append("df_gene = read.csv(\"").append(geneInputFile).append("\", header = TRUE)\n");
            geomLine.append("    geom_line(data=df_gene, aes(x=position, y=coverage)) +\n");
        }
        if (repeatsPresent) {
            input.append("df_repeat = read.csv(\"").append(repeatInputFile).append("\", header = TRUE)\n");
            geomLine.append("    geom_line(data=df_repeat, aes(x=position, y=coverage, color='red')) +\n");
        }

        boolean top_plot = false;
        boolean bottom_plot = false;
        if (("density_" + number).equals(firstLastNames[0])) { // first
            top_plot = true;
        } else if (("density_" + number).equals(firstLastNames[1])) { // last
            bottom_plot = true;
        }
        // margin; first is top, second is left, third is bottom, fourth is left
        double margin_top = 0.01;
        if (top_plot) {
            margin_top = 0.15;
        }

        double margin_bottom = 0.01;
        String xAxistitle = "";
        String xAxisBlank = "    theme(axis.title.x = element_blank(), axis.text.x=element_blank()) +\n";
        if (bottom_plot) {
            margin_bottom = 0.25;
            xAxistitle = "    labs(x = \"Genomic position\") +\n";
            xAxisBlank = "";
        }

        // text on left axis is disabled because else the plot becomes smaller
        rscript.append(input)
                .append("density_").append(number).append(" <- ggplot() +\n")
                .append(geomLine)
                .append("    theme_classic(base_size=text_size) +\n")
                .append("    theme(legend.position = \"none\") +\n")
                .append(xAxisBlank)
                .append("    theme(axis.title.y=element_text(size = 10)) + \n")
                .append("    expand_limits(x = c(0, ").append(longest_sequence).append(")) +\n")
                .append("    expand_limits(y = c(0, 100)) +\n")
                .append("    scale_x_continuous(labels = unit_format(unit = \"Mb\", scale = 1e-6), expand = c(0.005, 0.01)) +\n")
                .append("    scale_y_continuous(labels = unit_format(unit = \"%\")) +\n")
                .append("    theme(plot.margin = unit(c(").append(margin_top).append(", 0.01, ").append(margin_bottom).append(", 0.01), \"cm\")) +\n") // the last 0.01 was 0.015
                .append(xAxistitle)
                .append("    labs(y = \"Coverage\\nper Mb\")\n\n");
    }

    /**
     * requires exactly specified which sequence should share genes with genomes or sequences
     * shared_between 4_1 3_3,3_4
      */
    private void createSharedBetweenForSeqVisualization(HashMap<String, Node[]> mrnaNodesPerSequence,
                                                        HashMap<String, Integer> sequenceLengthMap, ArrayList<String> plotRules) {

        ArrayList<String> queries = new ArrayList<>();
        for (String rule : plotRules) {
            if (rule.contains("between")) {
                queries.add(rule.replace("between,","")); //between,4_1,3_3 becomes 4_1,3_3
            }
        }

        compare_sequences = true;
        GeneClassification classification2 = new GeneClassification();
        int longestSequence = determineLongestSequence(new ArrayList<>(selectedSequences), sequenceLengthMap);
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String query : queries) {
                long blockNr = 0;
                String[] queryArray = query.split(","); /// [reference sequences, possible target sequences or genomes]
                String sequenceId = queryArray[0];
                String genomesOrSequences = query.replace(queryArray[0] + ",","");

                String fileName = query.replace(",","#").replace("_","S").replace("#","_");
                Path filePath = Paths.get(WORKING_DIRECTORY + "sequence_visualization/between_selection/" + fileName + ".csv");
                String[] genomesOrSequencesArray = genomesOrSequences.split(",");

                ArrayList<Integer> genomes = new ArrayList<>();
                ArrayList<String> sequences = new ArrayList<>();
                for (String genomesOrSequence : genomesOrSequencesArray) {
                    try {
                        genomes.add(Integer.parseInt(genomesOrSequence));
                    } catch (NumberFormatException numberFormatException){
                        sequences.add(genomesOrSequence);
                    }
                }

                Node[] mrnaNodes = mrnaNodesPerSequence.get(sequenceId);
                StringBuilder sequenceBuilder = new StringBuilder();
                for (Node mrnaNode : mrnaNodes) {
                    boolean foundInAll = checkIfMrnaIsShared(mrnaNode, genomes, sequences, classification2);
                    int[] address = (int[]) mrnaNode.getProperty("address");
                    write_block_to_ggplot_input(sequenceBuilder, address, 0, blockNr, String.valueOf(foundInAll));
                    blockNr++;
                }

                writeStringToFile("x,y,block_nr,type\n" + sequenceBuilder, filePath.toFile(), false);
                Path sequenceExtendedFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/between_selection/" + fileName + "_extended.csv");
                Path newSequenceFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/between_selection/" + fileName + "_no_white.csv");
                convertWhiteTemplateToFilled(filePath, newSequenceFile);
                convertWhiteTemplateToWide(filePath, sequenceExtendedFile, longestSequence);
            }
            tx.success();
        }
    }

    private boolean checkIfMrnaIsShared(Node mrnaNode, ArrayList<Integer> genomes, ArrayList<String> sequences,
                                     GeneClassification classification2) {

        Relationship homologyRelation = mrnaNode.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
        Node homologyNode = homologyRelation.getStartNode();
        boolean foundInAll = true;
        ///System.out.println(mrnaNode + " "+ sequences);
        int[] copy_number_array = (int[]) homologyNode.getProperty("copy_number");
        if (!sequences.isEmpty()) {
            int[][] geneCopiesPerSequenceArray = classification2.getGeneCopiesPerSequence(homologyNode);
            //System.out.println(geneCopiesPerSequenceArray.length + " " + geneCopiesPerSequenceArray[3].length  + " " + Arrays.toString(copy_number_array));
            for (String targetSequenceId : sequences) {
                String[] seqIdArray = targetSequenceId.split("_");
                int genomerNr = Integer.parseInt(seqIdArray[0]);
                int sequenceNr = Integer.parseInt(seqIdArray[1]);
                if (geneCopiesPerSequenceArray[genomerNr-1][sequenceNr-1] == 0) {
                    foundInAll = false;
                }
            }
        }

        if (!genomes.isEmpty()) {
            for (int genomeNr : genomes) {
                if (copy_number_array[genomeNr] == 0) {
                    foundInAll = false;
                }
            }
        }
        return foundInAll;
    }

    /**
     *
     */
    private void createCAUInput(HashMap<String, Node[]> mrnaNodesPerSequence, int genomesInGClassification, HashMap<String, Integer> sequenceLengthMap) {
        HashMap<String, ArrayList<String>> sequences_per_genome = new HashMap<>(); // key is a genome number (but as string)
        HashMap<String, ArrayList<String>> sequences_per_chromosome = new HashMap<>(); // 1_1 means genome 1 chromosome 1
        String skippedStr = PhasedFunctionalities.readOneLineFile(WORKING_DIRECTORY + "gene_classification/skipped_genomes.info", false);
        if (skippedStr.equals("")) {
            throw new RuntimeException("Please run gene_classifcation first");
        }
        String skippedStr2 = PhasedFunctionalities.readOneLineFile(WORKING_DIRECTORY + "sequence_visualization/gene_classification/gene_classification_skipped_genomes.info", false);
        boolean redoCalculation = true;
        if (skippedStr.equals(skippedStr2)) {
            redoCalculation = false;
        }
        writeStringToFile(skippedStr, "sequence_visualization/gene_classification/gene_classification_skipped_genomes.info",
                true, false);

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            HashMap<Node, String> hmgroup_class_map = pf.read_core_accessory_unique_groups(false);
            for (int genomeNr : selectedGenomes) {
                System.out.print("\r Retrieving classification results: Genome " + genomeNr);
                int yPosition = 0;
                long blockNr = 0;
                HashMap<String, Long> mrnaId_hmgroup_map = createMrnaHomologyMap(genomeNr);
                StringBuilder genomeBuilder = new StringBuilder();
                ArrayList<String> sequenceIdentifiers = retrieveSequencesInOrder(genomeNr, mrnaNodesPerSequence);
                if (sequenceIdentifiers.isEmpty()) {
                    continue;
                }
                String chromosome = "";
                for (String sequenceId : sequenceIdentifiers) {
                    if (sequenceId == null) {
                        continue;
                    }
                    boolean pass = checkIfFileExists("sequence_visualization/gene_classification/" + sequenceId + ".csv");
                    if (pass && !redoCalculation) { // output already exists with the same genome selection
                        continue;
                    }

                    Node[] mrnaNodes = mrnaNodesPerSequence.get(sequenceId);
                    String[] sequenceIdArray = sequenceId.split("_");
                    sequences_per_genome.computeIfAbsent(sequenceIdArray[0], k -> new ArrayList<>()).add(sequenceId);
                    if (phasingInfoMap != null && phasingInfoMap.containsKey(sequenceId)) {
                        String[] phasing_info = phasingInfoMap.get(sequenceId);  // [1, B, 1B, 1_B]
                        if (!phasing_info[0].equals(chromosome) && !chromosome.equals("")) { // sequence is on a different chromosome as the previous
                            yPosition += 2;
                        }
                        chromosome = phasing_info[0];
                        sequences_per_chromosome.computeIfAbsent(genomeNr + "_" + chromosome, k -> new ArrayList<>()).add(sequenceId);
                    }

                    int[] address = null;
                    if (genomesInGClassification < 1) {
                        continue;
                    }
                    String assigned_class = "";
                    StringBuilder sequenceBuilder = new StringBuilder();
                    for (Node mrnaNode : mrnaNodes) {
                        Relationship hm_relation = mrnaNode.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                        Node homologyNode;
                        if (hm_relation == null) { // for petunia pangenome, did not resolve issue yet
                            String mrna_id = (String) mrnaNode.getProperty("protein_ID");
                            long hmid = mrnaId_hmgroup_map.get( mrna_id + "#" + genomeNr);
                            homologyNode = GRAPH_DB.getNodeById(hmid);
                        } else {
                            homologyNode = hm_relation.getStartNode();
                        }
                        address = (int[]) mrnaNode.getProperty("address");
                        assigned_class = hmgroup_class_map.get(homologyNode);
                        if (assigned_class == null) {  // this mrna (and sequence) was not part of the previous gene_classification run
                            throw new RuntimeException("The previous gene_classification analysis does not match the current sequence selection. " +
                                    " Please rerun gene_classification (with the --phasing argument if you also selected 'haplotype_presence') " +
                                    " Homology node " +  homologyNode.getId()  + " was not found in: " +
                                    " " + WORKING_DIRECTORY + "gene_classification/group_identifiers/core_groups.csv" +
                                    " " + WORKING_DIRECTORY + "gene_classification/group_identifiers/accessory_groups.csv" +
                                    " " + WORKING_DIRECTORY + "gene_classification/group_identifiers/unique_groups.csv");
                        }

                        write_block_to_ggplot_input(genomeBuilder, address, yPosition, blockNr, capitalize_first_letter(assigned_class));
                        write_block_to_ggplot_input(sequenceBuilder, address, 0, blockNr, capitalize_first_letter(assigned_class));
                        blockNr++;
                    }
                    yPosition += 6;
                    writeStringToFile("x,y,block_nr,type\n" + sequenceBuilder,
                            "sequence_visualization/gene_classification/" + sequenceId + ".csv", true, false);
                }
                writeStringToFile("x,y,block_nr,type\n" + genomeBuilder,
                        "sequence_visualization/gene_classification/genome_" + genomeNr + ".csv", true, false);
            }
            tx.success();
        }
        System.out.println("");
        convertBarPlotCoordinates(mrnaNodesPerSequence, sequenceLengthMap, "gene_classification");
    }

    /**
     *  Used for both core,accessory unique plot and the haplotype copies plot
     * @param builder
     * @param geneAddress
     * @param yPosition
     * @param block_nr
     * @param previousMrnaClass
     */
    private void write_block_to_ggplot_input(StringBuilder builder, int[] geneAddress, int yPosition, long block_nr,
                                            String previousMrnaClass) {

        builder.append(geneAddress[2]).append(",").append(yPosition).append(",").append(block_nr).append(",").append(previousMrnaClass).append("\n")
                .append(geneAddress[2]).append(",").append(yPosition + 5).append(",").append(block_nr).append(",").append(previousMrnaClass).append("\n")
                .append(geneAddress[3]).append(",").append(yPosition + 5).append(",").append(block_nr).append(",").append(previousMrnaClass).append("\n")
                .append(geneAddress[3]).append(",").append(yPosition).append(",").append(block_nr).append(",").append(previousMrnaClass).append("\n");
    }

    private void convertWhiteTemplateToFilled(Path file, Path newFile) {
        String[][] blockValues = convertTemplateToArray(file);
        for (int i = 0; i < blockValues.length; i++) {
            if (i == 0){
                //System.out.println("first TO DO ");
            } else if (i == blockValues.length-1){
                //System.out.println("last TO DO ");
            } else {
                String[] previousValues = blockValues[i-1][3].split(",");
                String[] currentStartValues = blockValues[i][0].split(",");
                String end = previousValues[0];
                int endInt = Integer.parseInt(end);
                String start = currentStartValues[0];
                int startInt = Integer.parseInt(start);
                int addCoordinate = (int) (((double) Integer.parseInt(start) - endInt) / 2);
                if (addCoordinate > 1) {
                    int newEnd = endInt + addCoordinate-1;
                    int newStart = startInt -addCoordinate;
                    blockValues[i-1][2] = blockValues[i-1][2].replace(end, newEnd+"");
                    blockValues[i-1][3] = blockValues[i-1][3].replace(end, newEnd+"");
                    blockValues[i][0] = blockValues[i][0].replace(start, newStart +"");
                    blockValues[i][1] = blockValues[i][1].replace(start, newStart +"");
                }
            }
        }

        try (BufferedWriter out = new BufferedWriter(new FileWriter(newFile.toFile()))) {
            out.write( "x,y,block_nr,type\n");
            for (int i = 0; i < blockValues.length; i++) {
                out.write(blockValues[i][0] + "\n" +
                        blockValues[i][1] + "\n" +
                        blockValues[i][2] + "\n" +
                        blockValues[i][3] + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to write to: " + newFile);
        }
    }

    private String[][] convertTemplateToArray(Path file) {
        boolean skip_first_lines = true;
        int lineCounter = 0;
        int lines = countLinesInFile(file);

        int blocks = (int) ((lines-1)/4);
        //System.out.println("Lines " + lines + " " + blocks);
        String[][] currentBlockValues = new String[blocks][4];
        int blockNr = 0;
        try {
            BufferedReader br = Files.newBufferedReader(file);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (skip_first_lines) {
                    skip_first_lines = false;
                    continue;
                }
                if (lineCounter % 4 == 0 && lineCounter != 0) {
                    blockNr ++;
                }
                currentBlockValues[blockNr][(lineCounter % 4)] = line;
                lineCounter++;
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read: " + file);
        }
        return currentBlockValues;
    }

    private void convertWhiteTemplateToWide(Path file, Path extendedFile, int longestSequence) {
        String[][] blockValues = convertTemplateToArray(file);
        int maxGeneLength = (int) (longestSequence * 0.002);
        for (int i = 0; i < blockValues.length; i++) {
            int previousEndInt;
            if (i != 0) {
                String[] previousValues = blockValues[i - 1][3].split(",");
                String previousEnd = previousValues[0];
                previousEndInt = Integer.parseInt(previousEnd);
            } else {
                previousEndInt = 1;
            }

            String[] currentStartValues = blockValues[i][0].split(",");
            String[] currentEndValues = blockValues[i][3].split(",");
            String start = currentStartValues[0];
            int startInt = Integer.parseInt(start);
            String end = currentEndValues[0];
            int endInt = Integer.parseInt(end);


            int nextStartInt;
            if (i != blockValues.length-1 ) {
                String[] nextStartValues = blockValues[i + 1][0].split(",");
                String nextStart = nextStartValues[0];
                nextStartInt = Integer.parseInt(nextStart);
            } else {
                nextStartInt = endInt + maxGeneLength; // for the last position, allow full extension
            }

            int distanceEarlierGene = (int) (((double) startInt - previousEndInt));
            int distanceNextGene = (int) (((double) nextStartInt - endInt));
            int geneLength = ((endInt-startInt)+1);
            int required = maxGeneLength-geneLength;
            if (geneLength >= maxGeneLength) {
                continue;
            }
            //System.out.println(" gene " + geneLength + " requires 2x " + (required/2) + " distances " + distanceEarlierGene + " " + distanceNextGene);

            if ((distanceEarlierGene > required/2 && distanceNextGene/2 > required/2) ){
                int add = required/2;
                blockValues[i][0] = blockValues[i][0].replace(start, (startInt - add) +"");
                blockValues[i][1] = blockValues[i][1].replace(start, (startInt - add) +"");

                blockValues[i][2] = blockValues[i][2].replace(end, (endInt + add) +"");
                blockValues[i][3] =blockValues[i][3].replace(end, (endInt + add) +"");
            } else if (distanceEarlierGene > (required/2) ){
                int add = required/2;
                blockValues[i][0] = blockValues[i][0].replace(start, (startInt - add) +"");
                blockValues[i][1] = blockValues[i][1].replace(start, (startInt - add) +"");
                if (distanceNextGene > 1) {
                    blockValues[i][2] = blockValues[i][2].replace(end, (endInt + (distanceNextGene/2) - 1) + "");
                    blockValues[i][3] = blockValues[i][3].replace(end, (endInt + (distanceNextGene/2) - 1) + "");
                }
            } else if (distanceNextGene/2 > required/2){
                int add = required/2;
                if (distanceEarlierGene > 1) {
                    blockValues[i][0] = blockValues[i][0].replace(start, (startInt - distanceEarlierGene + 1) + "");
                    blockValues[i][1] = blockValues[i][1].replace(start, (startInt - distanceEarlierGene + 1) + "");
                }

                blockValues[i][2] = blockValues[i][2].replace(end, (endInt + add) +"");
                blockValues[i][3] = blockValues[i][3].replace(end, (endInt + add) +"");
                //System.out.println("\t\t" + Arrays.toString(blockValues[i]));
            } else { // none are
                if (distanceEarlierGene > 1) {
                    blockValues[i][0] = blockValues[i][0].replace(start, (startInt - (distanceNextGene/2) + 1) + "");
                    blockValues[i][1] = blockValues[i][1].replace(start, (startInt - (distanceNextGene/2) + 1) + "");
                }

                if (distanceNextGene > 1) {
                    blockValues[i][2] = blockValues[i][2].replace(end, (endInt + distanceNextGene - 1) + "");
                    blockValues[i][3] = blockValues[i][3].replace(end, (endInt + distanceNextGene - 1) + "");
                }
            }
        }

        try (BufferedWriter out = new BufferedWriter(new FileWriter(extendedFile.toFile()))) {
            out.write( "x,y,block_nr,type\n");
            for (int i = 0; i < blockValues.length; i++) {
                out.write(blockValues[i][0] + "\n" +
                        blockValues[i][1] + "\n" +
                        blockValues[i][2] + "\n" +
                        blockValues[i][3] + "\n");
            }
        } catch (IOException e) {
            Pantools.logger.warn("Unable to write to: {}", extendedFile);
        }
    }

    /**
     * prepare haplotype copies sequence visualization input
     */
    private void createHaplotypeCopiesInput(HashMap<String, Node[]> mrnaNodesPerSequence, HashMap<String, Integer> sequenceLengthMap) {
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            System.out.println("\r No phasing information found (for the sequence selection)");
            return;
        }

        if (!checkIfFileExists(WORKING_DIRECTORY + "gene_classification/selected_sequences_phased_analysis.info")) {
            System.out.println("\r No phasing information found from earlier gene_classification --phasing run\n");
            return;
        }

        HashSet<String> genomes = new HashSet<>();
        String[] selectedSequencesClassification = PhasedFunctionalities.readOneLineFile(WORKING_DIRECTORY + "gene_classification/selected_sequences_phased_analysis.info", false).split(",");
        for (String sequenceId : selectedSequencesClassification) {
            String[] seqIdArray = sequenceId.split("_");
            genomes.add(seqIdArray[0]);
        }

        int noInputCounter = 0;
        String selectedStr = PhasedFunctionalities.readOneLineFile(WORKING_DIRECTORY + "sequence_visualization/selected_sequences.info", false);
        String currentselectedStr = selectedSequences.toString().replace("[","").replace("]","").replace(" ","");
        String[] selectedSequencesArray = currentselectedStr.split(",");
        Pantools.logger.info(" Using counts from earlier gene_classification analysis of {} genomes and {} sequences.", genomes.size(), selectedSequencesClassification.length);
        Pantools.logger.info(" Current sequences in analysis: {}", selectedSequencesArray.length);
        boolean redoCalculation = true;
        if (selectedStr.equals(currentselectedStr)) {
            redoCalculation = false;
        }
        boolean firstWarning = true;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int genomeNr : selectedGenomes) { // i is a genome number
                int yPosition = 0;
                long blockNr = 0;
                StringBuilder genomeBuilder = new StringBuilder();
                HashMap<String, Long> mrnaId_hmgroup_map = createMrnaHomologyMap(genomeNr);
                ArrayList<String> sequenceIdentifiers = retrieveSequencesInOrder(genomeNr, mrnaNodesPerSequence);
                if (sequenceIdentifiers.isEmpty()) {
                    continue;
                }

                String chromosome = "";
                int sequenceCounter = 0;
                for (String sequenceId : sequenceIdentifiers) {
                    if (sequenceId == null) {
                        continue;
                    }
                    boolean pass = checkIfFileExists( WORKING_DIRECTORY + "sequence_visualization/haplotype_presence/" + sequenceId + ".csv");
                    if (pass && !redoCalculation) {
                        continue;
                    }
                    sequenceCounter++;
                    HashMap<String,String> count_per_group = null;
                    if (phasingInfoMap.containsKey(sequenceId)) {
                        String[] phasing_info = phasingInfoMap.get(sequenceId);  // [1, B, 1B, 1_B]
                        if (!phasing_info[0].equals(chromosome) && !chromosome.equals("")) { // sequence is on a different chromosome as the previous
                            yPosition += 2;
                        }
                        chromosome = phasing_info[0];
                        count_per_group = read_phasing_copies_for_chromosome(genomeNr + "_" + chromosome);
                        if (count_per_group.isEmpty() && firstWarning) {
                            Pantools.logger.warn(" Genome/sequence selection does not match. ");
                            Pantools.logger.warn(" Please run 'gene_classification' together with the --phasing argument!");
                            firstWarning = false;
                        }
                    }

                    if (count_per_group == null || count_per_group.isEmpty()) {
                        noInputCounter++;
                        continue;
                    }
                    System.out.print("\r Counting genes in haplotypes: Genome " + genomeNr + ", sequence " + sequenceCounter + "/" + sequenceIdentifiers.size());
                    String haplotypeCounts = "";
                    StringBuilder sequenceBuilder = new StringBuilder();
                    Node[] mrnaNodes = mrnaNodesPerSequence.get(sequenceId);
                    for (Node mrnaNode : mrnaNodes) {
                        Relationship homologyRelation = mrnaNode.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                        Node homologyNode;
                        if (homologyRelation == null) { // for petunia pangenome, did not resolve issue yet
                            String mrna_id = (String) mrnaNode.getProperty("protein_ID");
                            long homologyId = mrnaId_hmgroup_map.get(mrna_id + "#" + genomeNr);
                            homologyNode = GRAPH_DB.getNodeById(homologyId);
                        } else {
                            homologyNode = homologyRelation.getStartNode();
                        }
                        haplotypeCounts = count_per_group.get(homologyNode.getId() +"");
                        if (haplotypeCounts == null) { // this mrna (from a certain genome or sequence) was not part of the previous gene classification run
                            noInputCounter++;
                            break;
                        }

                        if (haplotypeCounts.equals("1")) {
                            haplotypeCounts += " haplotype";
                        } else {
                            haplotypeCounts += " haplotypes";
                        }

                        int[] address = (int[]) mrnaNode.getProperty("address");
                        write_block_to_ggplot_input(genomeBuilder, address, yPosition, blockNr, haplotypeCounts);
                        write_block_to_ggplot_input(sequenceBuilder, address, 0, blockNr, haplotypeCounts);
                        blockNr++;
                    }
                    yPosition += 6;
                    writeStringToFile("x,y,block_nr,type\n" + sequenceBuilder,
                            "sequence_visualization/haplotype_presence/" + sequenceId + ".csv", true, false);
                }
                String counts = genomeBuilder.toString();
                if (counts.equals("")) {
                    continue;
                }
                writeStringToFile("x,y,block_nr,type\n" + counts,
                        "sequence_visualization/haplotype_presence/genome_" + genomeNr + ".csv", true, false);
            }
            tx.success();
        }
        if (noInputCounter > 0) {
            throw new RuntimeException("There was no input for " + noInputCounter + " sequences");
        }
        System.out.println("");
        convertBarPlotCoordinates(mrnaNodesPerSequence, sequenceLengthMap, "haplotype_presence");
    }

    private HashMap<String,String> read_phasing_copies_for_chromosome(String chromosome_key) {
        HashMap<String,String> count_per_group = new HashMap<>(); // key is a homology_group node id, value is counts in haplotypes
        String line;
        String file = WORKING_DIRECTORY + "gene_classification/haplotype_presence/" + chromosome_key + ".txt";
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            while ((line = in.readLine()) != null) { // go over the lines of the input file
                String[] line_array = line.trim().split(",");
                count_per_group.put(line_array[0], line_array[1]);
            }
        } catch (IOException ioe) {
            // do nothing
            // this means gene_classification must be run together with the --phased argument
        }
        return count_per_group;
    }

    /**
     *
     * @param mrnaNodesPerSequence
     * @param sequenceLengthMap
     * @param type can be 'single_chromosome', 'haplotype_presence' or 'gene_classification'
     */
    private void convertBarPlotCoordinates(HashMap<String, Node[]> mrnaNodesPerSequence, HashMap<String, Integer> sequenceLengthMap, String type) {
        // loop to go over genome and sequence files. fill in the white spaces
        for (int genomeNr : selectedGenomes) {
            ArrayList<String> selectionForLongest = new ArrayList<>();
            for (String sequenceId : selectedSequences) {
                if (sequenceId.startsWith(genomeNr +"_")) {
                    selectionForLongest.add(sequenceId);
                }
            }
            int longestSequence = determineLongestSequence(selectionForLongest, sequenceLengthMap);
            if (type.equals("haplotype_presence") || type.equals("gene_classification")) {
                Path genomeFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/genome_" + genomeNr + ".csv");
                if (!checkIfFileExists(genomeFile)) {
                    continue;
                }
                Path genomeExtendedFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/genome_" + genomeNr + "_extended.csv");
                Path newGenomeFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/genome_" + genomeNr + "_no_white.csv");
                convertWhiteTemplateToFilled(genomeFile, newGenomeFile);
                convertWhiteTemplateToWide(genomeFile, genomeExtendedFile, longestSequence);
            }

            ArrayList<String> sequenceIdentifiers = retrieveSequencesInOrder(genomeNr, mrnaNodesPerSequence);
            for (String sequenceId : sequenceIdentifiers) {
                Path sequenceFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/" + sequenceId + ".csv");
                Path sequenceExtendedFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/" + sequenceId + "_extended.csv");
                Path newSequenceFile = Paths.get(WORKING_DIRECTORY + "sequence_visualization/" + type + "/" + sequenceId + "_no_white.csv");
                convertWhiteTemplateToFilled(sequenceFile, newSequenceFile);
                convertWhiteTemplateToWide(sequenceFile, sequenceExtendedFile, longestSequence);
            }
        }
    }

    /**
     *
     * @param genomeNr a genome number
     * @param mrnaNodesPerSequence
     * @return
     */
    private ArrayList<String> retrieveSequencesInOrder(int genomeNr, HashMap<String, Node[]> mrnaNodesPerSequence) {
        ArrayList<String> sequenceIdentifiers = new ArrayList<>();
        for (String sequenceId : selectedSequences) {
            if (!mrnaNodesPerSequence.containsKey(sequenceId) || !sequenceId.startsWith(genomeNr + "_")) {
                continue;
            }
            sequenceIdentifiers.add(sequenceId);
        }
        if (phasingInfoMap != null && !phasingInfoMap.isEmpty()) {
            sequenceIdentifiers = pf.sequencesInChromosomePhasingOrder(sequenceIdentifiers);
        }
        Collections.reverse(sequenceIdentifiers);
        return sequenceIdentifiers;
    }

    /**
     *
     * @param sequenceIdentifiers
     * @return
     */
    private String[] create_glassification_ggplot_axis_break(ArrayList<String> sequenceIdentifiers) {
        StringBuilder breaks_builder = new StringBuilder("scale_y_continuous(breaks=c(");
        StringBuilder labels_builder = new StringBuilder("), labels=c(");
        StringBuilder chrom_breaks_builder = new StringBuilder("scale_y_continuous(breaks=c(");
        StringBuilder chrom_labels_builder = new StringBuilder("), labels=c(");
        int axis_position = 0;
        String chromosome = "";
        int start_chromosome = 0;
        HashMap<String, ArrayList<Double>> positions_per_chromosome = new HashMap<>();
        for (int j = 0; j < sequenceIdentifiers.size(); j++) {
            String seq_id = sequenceIdentifiers.get(j);
            String phasing = "";
            if (phasingInfoMap != null && phasingInfoMap.containsKey(seq_id)) {
                String[] phasing_info = phasingInfoMap.get(seq_id);
                phasing = " " + phasing_info[1];
                if (!phasing_info[0].equals(chromosome) && !chromosome.equals("")) {
                    axis_position += 2;
                    //System.out.println("change " +   axis_position + " old " + start_chromosome + "=" +  ( start_chromosome) + " " + phasing_info[0] + "->" + chromosome);
                    start_chromosome = axis_position;
                }
                chromosome = phasing_info[0];
            }
            double position = ((j*5)+ 2.5 + axis_position);
            breaks_builder.append(position).append(",");
            if (!chromosome.equals("")) {
                positions_per_chromosome.computeIfAbsent(chromosome, k -> new ArrayList<>()).add(position);
            }
            labels_builder.append("\"").append(seq_id).append(phasing).append("\",");
            axis_position++;
        }
        String breaks = breaks_builder.toString().replaceFirst(".$","");
        String labels = labels_builder.toString().replaceFirst(".$","");
        String sequence_labels = breaks + labels + "))";

        for (String chromosome_nr : positions_per_chromosome.keySet()) {
            double median = Utils.median(positions_per_chromosome.get(chromosome_nr));
            chrom_breaks_builder.append(median).append(",");
            chrom_labels_builder.append("\"Chr ").append(chromosome_nr).append("\",");
        }
        String chrom_breaks = chrom_breaks_builder.toString().replaceFirst(".$","");
        String chrom_labels = chrom_labels_builder.toString().replaceFirst(".$","");
        String chromosome_labels = chrom_breaks + chrom_labels + "))";
        return new String[]{sequence_labels, chromosome_labels};
    }

    /**
     *
     * @param includedPlots
     * @param plotRules
     * @param totalAnnotationBars
     * @return
     */
    private String determineOriginalBarHeights(HashSet<String> includedPlots, ArrayList<String> plotRules,
                                                     int totalAnnotationBars) {

        boolean coveragePlotAdded = false;
        boolean betweenPlotAdded = false;
        StringBuilder sizesBuilder = new StringBuilder("#Bar heights:\n" +
                "increase_last_bar <- 0.7 " +
                "# the last bar is smaller because of the 'Genomic position' title. Increase number to make bar higher, decrease to make smaller\n");
        boolean covPlotPresent = false; // change the heights of other bars when this plot is present
        for (String rule : includedPlots) {
            if (rule.contains("coverage") && !covPlotPresent) {
                covPlotPresent = true;
                sizesBuilder.append("coverage <- 1.0\n");
            }
        }
        for (String rule : plotRules) { //[all, gene_classification, other_chromosomes, gene_coverage, repeat_coverage, haplotype_presence]
            if (rule.contains("coverage") && !coveragePlotAdded) { // can only be added once
                coveragePlotAdded = true;
            } else {
                if (rule.contains("between") && !betweenPlotAdded) { // between are always
                    betweenPlotAdded = true;
                    if (covPlotPresent) {
                        sizesBuilder.append("between <- 0.5\n");
                    } else {
                        sizesBuilder.append("between <- 0.8\n");
                    }
                }

                if (includedPlots.contains(rule)) {
                    if (rule.equals("gene_classification")) {
                        if (covPlotPresent) {
                            sizesBuilder.append("gene_classification <- 0.5\n");
                        } else {
                            sizesBuilder.append("gene_classification <- 0.8\n");
                        }
                    }
                    if (rule.equals("other_chromosomes")) {
                        if (covPlotPresent) {
                            sizesBuilder.append("other_chromosomes <- 0.3\n");
                        } else {
                            sizesBuilder.append("other_chromosomes <- 0.8\n");
                        }
                    }
                    if (rule.equals("haplotype_presence")) {
                        if (covPlotPresent) {
                            sizesBuilder.append("haplotype_presence <- 0.5\n");
                        } else {
                            sizesBuilder.append("haplotype_presence <- 0.8\n");
                        }
                    }
                }
            }
        }
        if (covPlotPresent) {
            sizesBuilder.append("synteny <- 0.9\n\n");
        } else {
            sizesBuilder.append("synteny <- 1.0\n\n");
        }
        return "#Plot sizes:\nplot_height <- " + (totalAnnotationBars*2) + "\nplot_width <- 25\n\n" + sizesBuilder;
    }

    private String[] createSpecificBarOrder(ArrayList<String> sequenceSpecificPlotRules) {
        StringBuilder names = new StringBuilder();
        StringBuilder sizes = new StringBuilder();
        for (int i = 0; i < sequenceSpecificPlotRules.size(); i++) {
            String rule = sequenceSpecificPlotRules.get(i);
            if (rule.contains("coverage") ) {
                names.append("density_" + i + ", ");
                names.append("coverage, ");
                throw new RuntimeException("still in development verify this... sorry");
            } else if (rule.contains("gene_classification")) {
                names.append("classification_").append(i).append(", ");
                sizes.append("gene_classification, ");
            } else if (rule.contains("other_chromosomes")) {
                names.append("other_chromosomes_").append(i).append(", ");
                sizes.append("other_chromosomes, ");
            } else if (rule.contains("haplotype_presence")) {
                names.append("haplotype_presence_").append(i).append(", ");
                sizes.append("haplotype_presence, ");
            } else if (rule.contains("between")) {
                names.append("between_").append(i).append(", ");
                sizes.append("between, ");
            } else {
                throw new RuntimeException("incomplete... sorry " + rule);
            }

            if (i != sequenceSpecificPlotRules.size()-1) {
                names.append("synteny_").append(i).append(", ");
                sizes.append("synteny, ");
            }
        }

        String[] names_heights = new String[2];
        names_heights[0] = names.toString().replaceFirst(".$","").replaceFirst(".$",""); // remove last 2 characters
        names_heights[1] = sizes.toString().replaceFirst(".$","").replaceFirst(".$","")
                + " + increase_last_bar"; // remove last 2 characters

        return names_heights;
    }

    /**
     *
     * @param sequenceIdentifiers
     * @param includedPlots can contain: [all, gene_classification, other_chromosomes, gene_coverage, repeat_coverage, haplotype_presence]
     */
    private String[] createBarOrder(ArrayList<String> sequenceIdentifiers, HashSet<String> includedPlots,
                                   ArrayList<String> plotRules) {

        String[] names_heights = new String[2]; // [plot names, plot height variables]
        ArrayList<String> sizesOrder = new ArrayList<>();
        if (sequenceIdentifiers.size() == 2) { // if only two sequences, mirror the second set
            ArrayList<String> namesOrder = new ArrayList<>();
            boolean covPlotAdded = false;
            for (String rule : plotRules) { // see if any % coverage plot was added
                if (rule.contains("coverage") && !covPlotAdded) {
                    covPlotAdded = true;
                    namesOrder.add("density_0");
                    sizesOrder.add("coverage");
                } else if (includedPlots.contains(rule)) {
                    if (rule.equals("gene_classification")) {
                        namesOrder.add("classification_0");
                        sizesOrder.add("gene_classification");
                    }
                    if (rule.equals("other_chromosomes")) {
                        namesOrder.add("other_chromosomes_0");
                        sizesOrder.add("other_chromosomes");
                    }
                    if (rule.equals("haplotype_presence")) {
                        namesOrder.add("haplotype_presence_0");
                        sizesOrder.add("haplotype_presence");
                    }
                }
            }
            String namesPartOne = namesOrder.toString().replace("[","").replace("]","");
            Collections.reverse(namesOrder);
            String namesPartTwo = namesOrder.toString().replace("[","").replace("]","").replace("_0","_1");
            String sizesPartOne = sizesOrder.toString().replace("[","").replace("]","");
            Collections.reverse(sizesOrder);
            String sizesPartTwo = sizesOrder.toString().replace("[","").replace("]","");
            names_heights[0] = namesPartOne +  ", synteny_0, " + namesPartTwo;
            names_heights[1] = sizesPartOne + ", synteny, " + sizesPartTwo + " + increase_last_bar";

        } else { // more than 2
            StringBuilder namesBuilder = new StringBuilder();
            StringBuilder sizesBuilder = new StringBuilder();
            for (int i = 0; i < sequenceIdentifiers.size(); i++) { // skip the first value (hmgroup id)
                boolean coveragePlotAdded = false;
                for (String rule : plotRules) { //[all, gene_classification, other_chromosomes, gene_coverage, repeat_coverage, haplotype_presence]
                    if (rule.contains("coverage") && !coveragePlotAdded) { // can only be added once per sequence
                        namesBuilder.append("density_").append(i).append(", ");
                        coveragePlotAdded = true;
                        sizesBuilder.append("coverage, ");
                    } else if (includedPlots.contains(rule)) {
                        if (rule.equals("gene_classification")) {
                            namesBuilder.append("classification_").append(i).append(", ");
                            sizesBuilder.append("gene_classification, ");
                        }
                        if (rule.equals("other_chromosomes")) {
                            namesBuilder.append("other_chromosomes_").append(i).append(", ");
                            sizesBuilder.append("other_chromosomes, ");
                        }
                        if (rule.equals("haplotype_presence")) {
                            namesBuilder.append("haplotype_presence_").append(i).append(", ");
                            sizesBuilder.append("haplotype_presence, ");
                        }
                    }
                }
                if (i != sequenceIdentifiers.size()-1) {
                    namesBuilder.append("synteny_").append(i).append(", ");
                    sizesBuilder.append("synteny, ");
                }
            }
            names_heights[0] = namesBuilder.toString().replaceFirst(".$","").replaceFirst(".$",""); // remove last 2 characters
            names_heights[1] = sizesBuilder.toString().replaceFirst(".$","").replaceFirst(".$","")
                    + " + increase_last_bar"; // remove last 2 characters
        }
        return names_heights;
    }


    /**
     *
     * @param sequenceLengthMap
     * @param plotRules possible rules
     *            "other_chromosomes"
     *            "gene_classification"
     *            "gene_coverage"
     *            "repeat_coverage"
     *            "haplotype_presence"
     *            "between....."
     *
     */
    private void createSeqVisualization(HashMap<String, Integer> sequenceLengthMap, ArrayList<String> plotRules,
                                       ArrayList<String> sequenceSelection, ArrayList<String> sequenceSpecificPlotRules) {

        StringBuilder log = new StringBuilder();
        int longestSequence = determineLongestSequence(sequenceSelection, sequenceLengthMap);
        StringBuilder rscript = new StringBuilder();
        addSeqVisualizationRscriptHeader(rscript);
        rscript.append("# ").append(sequenceSelection.toString().replace("[", "").replace("]", "")).append("\n\n");

        HashSet<String> includedPlots = checkSeqVisualizationInputFiles(sequenceSelection, plotRules, null, log);

        Pantools.logger.debug("hoi{}", sequenceSelection);
        Pantools.logger.debug("{} {} {}", sequenceSpecificPlotRules, plotRules, includedPlots);

        // kijk hier naar met nieuwe originalPlotRules

        String[] names_heights;
        if (sequenceSpecificPlotRules == null) {
            names_heights = createBarOrder(sequenceSelection, includedPlots, plotRules);
        } else {
            names_heights = createSpecificBarOrder(sequenceSpecificPlotRules);
        }
        String[] namesArray = names_heights[0].split(", ");
        String sizes = determineOriginalBarHeights(includedPlots, plotRules, namesArray.length);
        rscript.append(sizes);

        rscript.append("#change axes\n" +
                        "text_size <- 15\n")
                .append("use_minor_ticks <- FALSE # can be set to TRUE\n")
                .append("label_at <- function(n) function(x) ifelse(x %% n == 0, x, \"\")\n\n");

        loopToAppendSyntenyInput(sequenceSelection, rscript, longestSequence);

        String[] firstLastNames = new String[]{namesArray[0], namesArray[namesArray.length-1]}; // used to determine first and last bar
        if (sequenceSpecificPlotRules != null) {
            loopToAppendBarInput(rscript,sequenceSpecificPlotRules, firstLastNames, longestSequence);
        }  else {
            loopToAppendBarInput(rscript, sequenceSelection, includedPlots, firstLastNames, longestSequence);
        }

        rscript.append("\nplots <- plot_grid(").append(names_heights[0]).append(",\n")
                .append("                   rel_heights = c(").append(names_heights[1]).append("),\n")
                .append("                   ncol = 1, align = 'v', axis = 'lr')\n\n")
                .append("ggsave(plots, file=\"").append(WD_full_path).append("sequence_visualization/output_figures/sequences").append(".png\", width = plot_width, height = plot_height, units = \"cm\")\n")
                .append("cat ('\\nOutput written to ").append(WD_full_path).append("sequence_visualization/output_figures/sequences.png").append("\\n\\n')\n");

        if (log.length() > 0) {
            Pantools.logger.warn("Not all annotations could be created, please check: {}log/sequence_visualization.log", WORKING_DIRECTORY);
        }
        writeStringToFile(log.toString(), WORKING_DIRECTORY + "log/sequence_visualization.log", false, false);
        writeStringToFile(rscript.toString(), "sequence_visualization/scripts/plot_sequences.R", true, false);
    }

    /**
     * loop to add synteny plots to rscript,
     * works for any ploidy and for unphased sequences
     * @param sequenceIdentifiers
     * @param Rscript
     * @param longest_sequence
     */
    private void loopToAppendSyntenyInput(ArrayList<String> sequenceIdentifiers, StringBuilder Rscript,
                                                int longest_sequence) {

        for (int i = 0; i < sequenceIdentifiers.size()-1; i++) {
            String seqIdA = sequenceIdentifiers.get(i);
            if (i < sequenceIdentifiers.size()-1) {
                String seqIdB = sequenceIdentifiers.get(i + 1);
                String renamed_seq_combi = seqIdA.replace("_", "S") + "_" + seqIdB.replace("_", "S");
                appendSyntenyGgplot(Rscript, renamed_seq_combi, longest_sequence, i);
                if (!checkIfFileExists( WD_full_path + "sequence_visualization/synteny/" + renamed_seq_combi + "_major_blocks.csv")) {
                    writeStringToFile("x,y,block_nr,type\n" + longest_sequence + ",0,0,major\n",
                            "sequence_visualization/synteny/" + renamed_seq_combi + "_major_blocks.csv", true, false);
                }
            }
        }
    }

    private void appendSyntenyGgplot(StringBuilder rscript, String seq_combi, int longest_sequence, int number) {
        rscript.append("df_synteny = read.csv(\"").append(WD_full_path).append("sequence_visualization/synteny/").append(seq_combi).append("_major_blocks.csv\", header= TRUE)\n")
                .append("synteny_").append(number).append(" <- ggplot() +\n")
                .append("    geom_polygon(data=df_synteny, mapping=aes(x=x, y=y, group=block_nr, fill=type), alpha = 0.5) +\n")
                .append("    theme_classic() +\n")
                .append("    theme(legend.position = \"none\") +\n")
                .append("    theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.line.x=element_blank(), " +
                        "axis.ticks=element_blank()) +\n")
                .append("    theme(axis.title.y=element_blank(), axis.text.y=element_blank(), axis.line.y=element_blank() ) +\n")
                .append("    expand_limits(x = c(0, ").append(longest_sequence).append(")) +\n")
                .append("    theme(plot.margin=unit(c(0.01, 0.01, 0.01, 0.01), \"cm\")) +\n")
                .append("    scale_x_continuous(expand = c(0.005, 0.01)) +\n")
                .append("    scale_fill_manual(values=c(\"major\" = \"#61b2ff\", \"majorRV\" = \"#ff7373\", " +
                        "\"minor\" = \"#46a5ff\", \"minorRV\" = \"#ff5757\"))\n")
                .append("\n");
    }

    /**
     *
     * @param mrnaNodesPerSequence
     * @param sequenceLengthMap
     * @param genomesInGClassification
     * @param plotRules possible rules
     *            "other_chromosomes"
     *            "gene_classification"
     *            "gene_coverage"
     *            "repeat_coverage"
     *            "haplotype_presence"
     */
    private void createPhasedChromosomeVisualization(HashMap<String, Node[]> mrnaNodesPerSequence,
                                                    HashMap<String, Integer> sequenceLengthMap,
                                                    int genomesInGClassification, ArrayList<String> plotRules) {

        HashMap<String, ArrayList<String>> sequences_per_chromosome = new HashMap<>(); // 1_1 means genome 1 chromosome 1
        int createdPlots = 0;
        StringBuilder log = new StringBuilder();
        ArrayList<String> chromosome_keys = new ArrayList<>();
        StringBuilder shellscript = new StringBuilder("#!/bin/bash\necho \"Creating genome and chromosome visualizations\"\n");
        for (int genomeNr : selectedGenomes) {
            ArrayList<String> sequenceIdentifiers = new ArrayList<>();
            for (String sequenceId : selectedSequences) {
                if (!mrnaNodesPerSequence.containsKey(sequenceId) || !sequenceId.startsWith(genomeNr + "_")) {
                    continue;
                }
                sequenceIdentifiers.add(sequenceId);
            }
            sequenceIdentifiers = pf.sequencesInChromosomePhasingOrder(sequenceIdentifiers);
            if (sequenceIdentifiers.isEmpty()) {
                continue;
            }

            for (String sequenceId : sequenceIdentifiers) {
                if (phasingInfoMap.containsKey(sequenceId)) {
                    String[] phasing_info = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
                    sequences_per_chromosome.computeIfAbsent(genomeNr + "_" + phasing_info[0], k -> new ArrayList<>()).add(sequenceId);
                    if (!chromosome_keys.contains(genomeNr + "_" + phasing_info[0])) {
                        chromosome_keys.add(genomeNr + "_" + phasing_info[0]);
                    }
                }
            }
            if (genomesInGClassification > 1 && plotRules.contains("gene_classification") &&
                    check_if_file_exists(WD_full_path + "sequence_visualization/scripts/genome_" + genomeNr + "_class.R")) {
                shellscript.append("Rscript ").append(WD_full_path).append("sequence_visualization/scripts/genome_").append(genomeNr).append("_class.R\n");
                createdPlots++;
            }
            if (plotRules.contains("haplotype_presence") && check_if_file_exists(WD_full_path + "sequence_visualization/scripts/genome_" + genomeNr+ "_phasing.R")) {
                shellscript.append("Rscript ").append(WD_full_path).append("sequence_visualization/scripts/genome_").append(genomeNr).append("_phasing.R\n")
                        .append("printf \"\r Created genome ").append(genomeNr).append(" \"\n");
                createdPlots++;
            }
        }

        //System.out.println("keys " + chromosome_keys);
        for (String chromosome_key : chromosome_keys) {
            //System.out.println(chromosome_key);
            ArrayList<String> sequenceIdentifiers = sequences_per_chromosome.get(chromosome_key);
            sequenceIdentifiers = pf.sequencesInChromosomePhasingOrder(sequenceIdentifiers);
            //System.out.println(chromosome_key  + " " + sequenceIdentifiers);
            if (sequenceIdentifiers.size() == 1) { // only a single sequence in this chromosome
                continue;
            }
            int longestSequence = determineLongestSequence(sequenceIdentifiers, sequenceLengthMap);
            StringBuilder chromosomeRscript = new StringBuilder();
            addSeqVisualizationRscriptHeader(chromosomeRscript);
            for (String sequenceId : sequenceIdentifiers) {
                String[] phasingInfo = phasingInfoMap.get(sequenceId);  // example [1, B, 1B, 1_B]
                chromosomeRscript.append("# ").append(sequenceId).append(" ").append(phasingInfo[3]).append("\n");
            }
            chromosomeRscript.append("\n");


            HashSet<String> includedPlots = checkSeqVisualizationInputFiles(sequenceIdentifiers, plotRules, chromosome_key, log);
            if (includedPlots.isEmpty()) {
                continue;
            }

            createdPlots++;
            String[] names_heights = createBarOrder(sequenceIdentifiers, includedPlots, plotRules);
            String[] namesArray = names_heights[0].split(", ");
            String[] firstLastNames = new String[]{namesArray[0], namesArray[namesArray.length-1]}; // used to determine first and last bar
            String sizes = determineOriginalBarHeights(includedPlots, plotRules, namesArray.length);
            chromosomeRscript.append(sizes);

            chromosomeRscript.append("#change axes\n" +
                            "text_size <- 15\n" +
                            "use_minor_ticks <- FALSE # can be set to TRUE\n")
                    .append("\n")
                    .append("label_at <- function(n) function(x) ifelse(x %% n == 0, x, \"\")\n");
            loopToAppendSyntenyInput(sequenceIdentifiers, chromosomeRscript, longestSequence);

            //System.out.println("included " + includedPlots);


            loopToAppendBarInput(chromosomeRscript, sequenceIdentifiers, includedPlots, firstLastNames, longestSequence);
            chromosomeRscript.append("plots <- plot_grid(").append(names_heights[0]).append(", ncol = 1, ")
                    .append("align = 'v', axis = 'lr', rel_heights = c(").append(names_heights[1]).append("))\n\n")
                    .append("ggsave(plots, file=\"").append(WD_full_path).append("sequence_visualization/output_figures/chromosome_").append(chromosome_key).append(".png\", width = plot_width, height = plot_height, units = \"cm\")");
            shellscript.append("Rscript ").append(WD_full_path).append("sequence_visualization/scripts/chromosome_").append(chromosome_key).append(".R\n")
                    .append("printf \"\r Created chromosome ").append(chromosome_key).append(" \"\n");
            writeStringToFile(chromosomeRscript.toString(), "sequence_visualization/scripts/chromosome_" + chromosome_key + ".R", true, false);
        }
        if (createdPlots == 0) {
            throw new RuntimeException("No visualizations could be created with your current settings.");
        }

        shellscript.append("printf \"\rDone! Output written to ").append(WD_full_path).append("sequence_visualization/output_figures/\\n\\n\"");
        if (log.length() > 0) {
            Pantools.logger.warn(" Not all annotations could be created, please check: {}log/sequence_visualization.log", WORKING_DIRECTORY);
        }
        writeStringToFile(log.toString(), WORKING_DIRECTORY + "log/sequence_visualization.log", false, false);
        writeStringToFile(shellscript.toString(), "sequence_visualization/run_visualization_scripts.sh", true, false);
    }

    /**
     *
     * @param rscript
     * @param sequenceSpecificPlotRules
     * @param firstLastNames
     * @param longestSequence
     */
    private void loopToAppendBarInput(StringBuilder rscript, ArrayList<String> sequenceSpecificPlotRules, String[] firstLastNames, int longestSequence) {
        int number = 0;
        for (String rule : sequenceSpecificPlotRules) {

            String[] ruleArray = rule.split(",");
            String type = ruleArray[0];
            String sequenceId = ruleArray[1];
            if (type.contains("gene_coverage") || type.contains("repeat_coverage")) {
                String geneDensityInput = WD_full_path + "sequence_visualization/gene/coverage/" + sequenceId + ".csv";
                String repeatDensityInput = WD_full_path + "sequence_visualization/repeat/coverage/" + sequenceId + ".csv";
                appendGeneRepeatCoverageGgplot(rscript, geneDensityInput, repeatDensityInput, longestSequence, number,
                        firstLastNames);
            } else if (type.contains("between")) {
                String file = rule.replace("between,","").replace(",","#").replace("_","S").replace("#","_");
                append_single_bar_ggplot(rscript, WD_full_path + "sequence_visualization/between_selection/" + file + ".csv", longestSequence, number, type, sequenceId, firstLastNames);
            } else {
                String file = WD_full_path + "sequence_visualization/" + ruleArray[0] + "/" +ruleArray[1] + ".csv";
                if (!checkIfFileExists(file)) {
                    throw new RuntimeException("Unable to create plot, following file does not exist: " +
                            WD_full_path + "sequence_visualization/" + ruleArray[0] + "/" + ruleArray[1] + ".csv");
                }
                append_single_bar_ggplot(rscript, file, longestSequence, number, type, sequenceId, firstLastNames);
            }
            number++;
        }
    }

    private void loopToAppendBarInput(StringBuilder rscript, ArrayList<String> sequenceIdentifiers,
                                     HashSet<String> includedPlots, String[] firstLastNames, int longestSequence) {

        for (int i = 0; i < sequenceIdentifiers.size(); i++) {
            String seqId = sequenceIdentifiers.get(i);
            String geneDensityInput = WD_full_path + "sequence_visualization/gene/coverage/" + seqId + ".csv";
            String repeatDensityInput = WD_full_path + "sequence_visualization/repeat/coverage/" + seqId + ".csv";
            String classificationInput = WD_full_path + "sequence_visualization/gene_classification/" + seqId + ".csv";
            String phasingInput = WD_full_path + "sequence_visualization/haplotype_presence/" + seqId + ".csv";
            String otherChromosomesInput = WD_full_path + "sequence_visualization/single_chromosome/" + seqId + ".csv";

            if (includedPlots.contains("gene_classification")) {
                append_single_bar_ggplot(rscript, classificationInput, longestSequence, i, "classification", seqId, firstLastNames);
            }

            if (includedPlots.contains("haplotype_presence")) {
                append_single_bar_ggplot(rscript, phasingInput, longestSequence, i, "haplotype_presence", seqId, firstLastNames);
            }

            if (includedPlots.contains("other_chromosomes")) {
                append_single_bar_ggplot(rscript, otherChromosomesInput, longestSequence, i, "other_chromosomes", seqId, firstLastNames);
            }

            if (includedPlots.contains("gene_coverage") || includedPlots.contains("repeat_coverage")) {
                appendGeneRepeatCoverageGgplot(rscript, geneDensityInput, repeatDensityInput, longestSequence, i,
                        firstLastNames);
            }
        }
    }

    /**
     *
     * @param rscript
     * @param input_file
     * @param longest_sequence
     * @param number
     * @param type
     */
    private void append_single_bar_ggplot(StringBuilder rscript, String input_file, int longest_sequence,
                                                int number, String type, String seq_id, String[] firstLastNames) {

        String xAxistitle = "";
        String xAxisBlank = "   theme(axis.title.x = element_blank(), axis.text.x=element_blank()) +\n";
        double margin_bottom = 0.01;
        double margin_top = 0.01;

        if ((type + "_" + number).equals(firstLastNames[0])) { // first
            margin_top = 0.15;
        } else if ((type + "_" + number).equals(firstLastNames[1])) { // last
            margin_bottom = 0.25;
            xAxistitle =  "    labs(x = \"Genomic position\") +\n";
            xAxisBlank = "";
        }

        rscript.append("df_bar <- read.csv(\"").append(input_file.replace(".csv","_extended.csv")).append("\", header = TRUE)\n");
        rscript.append("#df_bar <- read.csv(\"").append(input_file.replace(".csv","_no_white.csv")).append("\", header = TRUE)\n");
        rscript.append("#df_bar <- read.csv(\"").append(input_file).append("\", header = TRUE)\n");
        //.append("   theme(" + xAxisBlank + ", axis.text.x=element_blank()) +\n")
        rscript.append(type).append("_").append(number)
                .append(" <- ggplot() + geom_polygon(data=df_bar, mapping=aes(x=x, y=y, group=block_nr, fill=type)) +\n")
                .append("   theme_classic(base_size=text_size) +\n")
                .append("   theme(legend.position = \"none\") +\n")
                //.append("   theme(" + xAxisBlank + ", axis.text.x=element_blank()) +\n")
                .append(xAxisBlank)
                .append("   theme(axis.title.y = element_blank()) +\n")
                .append(xAxistitle)
                .append("   expand_limits(x = c(0, ").append(longest_sequence).append(")) +\n")
                .append("   {if(use_minor_ticks)scale_x_continuous(breaks = seq(0, ").append(+longest_sequence).append(", by = 1000000), labels = label_at(10000000), expand = c(0.005, 0.01))\n")
                .append("    else scale_x_continuous(labels = unit_format(unit = 'Mb', scale = 1e-6), expand = c(0.005, 0.01))} +\n")
                .append("\n")
                .append("   theme(plot.margin=unit(c(").append(margin_top).append(", 0.01, ").append(margin_bottom).append(", 0.01), \"cm\")) +\n")
                .append("   scale_y_continuous(breaks=c(2.5), labels=c(\"").append(seq_id).append("\"))");

        if (type.equals("classification")) { // core, accessory unique plot
            rscript.append(" +\n")
                    .append("   scale_fill_manual(values=c(\"Accessory\" = \"#4363d8\", \"Core\" = \"#3cb44b\", \"Unique\" = \"#e6194B\"))\n")
                    .append("   #scale_fill_manual(values=c(\"Accessory\" = \"#0072B2\", \"Core\" = \"#009E73\", \"Unique\" = \"#D55E00\")) # colourblind-friendly palette\n");
        } else if (type.equals("other_chromosomes")) {
            rscript.append(" +\n")
                    .append("   scale_fill_manual(values=c(\"Single copy\" = \"#ffffff\", \"Other chromosome\" = \"#929292\"))\n");
        } else if (type.equals("between")) {
            rscript.append(" +\n")
                    .append("   scale_fill_manual(values=c(\"true\" = \"#0072B2\", \"false\" = \"#D55E00\"))\n");
        } else { // phasing
            rscript.append("+ \n")
                    .append ("  scale_fill_manual(values=c(\"1 haplotype\" = \"#000000\", \"2 haplotypes\" = \"#56B4E9\", " +
                            "\"3 haplotypes\" = \"#E69F00\", \"4 haplotypes\" = \"#009E73\", \"5 haplotype\" = \"#F0E442\", \"6 haplotypes\" = \"#0072B2\", " +
                            "\"7 haplotypes\" = \"#D55E00\", \"8 haplotypes\" = \"#CC79A7\"))\n");
        }
        rscript.append("\n");
    }

    private HashSet<String> checkSeqVisualizationInputFiles(ArrayList<String> sequenceIdentifiers,
                                                           ArrayList<String> plotRules, String chromosome_key, StringBuilder log) {

        HashSet<String> includedPlots = new HashSet<>();
        HashSet<String> missingInput = new HashSet<>();

        for (int i = 0; i < sequenceIdentifiers.size(); i++) {
            boolean addAll = false;
            if (plotRules.contains("all")) {
                addAll = true;
            }
            String seqId = sequenceIdentifiers.get(i);
            if (addAll || plotRules.contains("gene_classification")) {
                if (checkIfFileExists(WD_full_path + "sequence_visualization/gene_classification/" + seqId + ".csv")) {
                    includedPlots.add("gene_classification");
                } else {
                    missingInput.add("gene_classification");
                }
            }

            if (addAll || plotRules.contains("haplotype_presence")) {
                if (checkIfFileExists(WD_full_path + "sequence_visualization/haplotype_presence/" + seqId + ".csv")) {
                    includedPlots.add("haplotype_presence");
                } else {
                    missingInput.add("haplotype_presence");
                }
            }

            if (addAll || plotRules.contains("other_chromosomes")) {
                if (checkIfFileExists(WD_full_path + "sequence_visualization/single_chromosome/" + seqId + ".csv")) {
                    includedPlots.add("other_chromosomes");
                } else {
                    missingInput.add("other_chromosomes");
                }
            }

            if (addAll || plotRules.contains("gene_coverage")) {
                if (checkIfFileExists(WD_full_path + "sequence_visualization/gene/coverage/" + seqId + ".csv")) {
                    includedPlots.add("gene_coverage");
                } else {
                    missingInput.add("gene_coverage");
                }
            }
            if (addAll || plotRules.contains("repeat_coverage")) {
                if (checkIfFileExists(WD_full_path + "sequence_visualization/repeat/coverage/" + seqId + ".csv")) {
                    includedPlots.add("repeat_coverage");
                } else {
                    missingInput.add("repeat_coverage");
                }
            }
        }

        // if one of the sequences misses the input, do not plot
        if (chromosome_key == null) {
            if (!missingInput.isEmpty()) {
                log.append("Warning! Not all plots can be generated: ");
                for (String rule : missingInput) {
                    log.append(rule).append(", ");
                }
                log.append("\n");
            }
        } else {
            if (!missingInput.isEmpty()) {
                log.append("Warning! Not all plots are generated for chromosome ").append(chromosome_key).append(": ");
                for (String rule : missingInput) {
                    log.append(rule).append(", ");
                }
                log.append("\n");
            }
        }
        return includedPlots;
    }
}
