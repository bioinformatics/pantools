package nl.wur.bif.pantools.analysis.phylogeny.rename_phylogeny;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Update or alter the terminal nodes (leaves) of a phylogenic tree.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "rename_phylogeny")
public class RenamePhylogenyCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "tree-file", index = "0+")
    @InputFile(message = "{file.tree}")
    Path treeFile;

    @Option(names = "--no-numbers")
    boolean noNumbers;

    @Option(names = "--phasing")
    boolean phasing;
    // TODO --phasing can only be used in combination with --sequence

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @ArgGroup(multiplicity = "1") GenomeOrSequence genomeSequenceTree;
    private static class GenomeOrSequence {
        @Option(names = {"--sequence"})
        boolean sequence;

        @Option(names = {"--genome"})
        boolean genome;

        @Option(names = {"--gene-tree"})
        boolean geneTree;
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);
        pantools.setPangenomeGraph();
        String treeType = determineTemplateType();
        setGlobalParameters(treeType); //TODO: use local parameters instead
        RenamePhylogeny RenamePhylogeny = new RenamePhylogeny();
        RenamePhylogeny.renamePhylogeny(noNumbers, treeType, treeFile);
        return 0;
    }

    private String determineTemplateType() {
        if (genomeSequenceTree.genome) {
            compare_genomes = true;
            return "genome";
        } else if (genomeSequenceTree.sequence) {
            compare_sequences = true;
            return "sequence";
        } // else, a gene tree file was included
        return "gene_tree";
    }

    private void setGlobalParameters(String treeType) {
        INPUT_FILE = treeFile.toString();
        PHENOTYPE = phenotype;
        if (noNumbers) Mode = "NO-NUMBERS";
        if (phasing) {
            if (treeType.equals("genome")) {
                Pantools.logger.error("can only be used with sequences or gene trees.");
                System.exit(1);
            }
            PHASED_ANALYSIS = true;
        }
    }

}
