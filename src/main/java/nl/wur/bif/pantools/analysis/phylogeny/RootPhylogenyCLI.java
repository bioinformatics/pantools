package nl.wur.bif.pantools.analysis.phylogeny;

import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * (Re)root a phylogenetic tree.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "root_phylogeny", sortOptions = false)
public class RootPhylogenyCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "tree-file", index = "0+")
    @InputFile(message = "{file.tree}")
    Path treeFile;

    @Option(names = {"-n", "--node"}, required = true)
    String terminalNode;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        phylogeny.reroot_phylogeny();
        return 0;
    }

    private void setGlobalParameters() {
        INPUT_FILE = treeFile.toString();
        NODE_VALUE = terminalNode;
    }

}
