package nl.wur.bif.pantools.analysis.map_reads.parallel;

import java.util.Comparator;

/**
 * Implements a comparator for integer arrays of size 2
 */
public class IntPairComparator implements Comparator<int[]> {
    @Override
    public int compare(int[] x, int[] y) {
        if (x[0] > y[0])
            return -1;
        else if (x[0] < y[0])
            return 1;
        else if (x[1] < y[1])
            return -1;
        else if (x[1] > y[1])
            return 1;
        else
            return 0;
    }
}
