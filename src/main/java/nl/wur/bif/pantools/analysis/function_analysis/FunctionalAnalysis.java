/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.analysis.function_analysis;

import cern.jet.stat.Gamma;
import nl.wur.bif.pantools.analysis.classification.Classification;
import nl.wur.bif.pantools.utils.ExecCommand;
import nl.wur.bif.pantools.Pantools;
import org.apache.commons.lang.ArrayUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 *
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class FunctionalAnalysis {

    private final static String[] cog_category_array = {"A: RNA processing and modification", "B: Chromatin structure and dynamics", "C: Energy production and conversion",
            "D: Cell cycle control, cell division, chromosome partitioning", "E: Amino acid transport and metabolism", "F: Nucleotide transport and metabolism",
            "G: Carbohydrate transport and metabolism","H: Coenzyme transport and metabolism", "I: Lipid transport and metabolism",
            "J: Translation, ribosomal structure and biogenesis", "K: Transcription", "L: Replication, recombination and repair", "M: Cell wall/membrane/envelope biogenesis",
            "N: Cell motility", "O: Posttranslational modification, protein turnover, chaperones", "P: Inorganic ion transport and metabolism",
            "Q: Secondary metabolites biosynthesis, transport and catabolism","R: General function prediction only", "S: Function unknown", "T: Signal transduction mechanisms",
            "U: Intracellular trafficking, secretion, and vesicular transport", "V: Defense mechanisms", "W: Extracellular structures", "X: Mobilome: prophages, transposons",
            "Y: Nuclear structure", "Z: Cytoskeleton"};

    /**
     *
     * @param target_node
     * @param reltype
     * @param overview_builder
     * @param function_count_map
     * @return
     */
    public static int print_connected_function_node_info(Node target_node, RelationshipType reltype, StringBuilder overview_builder,
            HashMap<String, Integer> function_count_map) {

        Iterable<Relationship> relationships = target_node.getRelationships(reltype, Direction.OUTGOING);
        int found_counter = 0;
        for (Relationship rel : relationships) {
            found_counter ++;
            Node function_node = rel.getEndNode();
            String function_id = (String) function_node.getProperty("id");
            String function_name = (String) function_node.getProperty("name");
            overview_builder.append(" ").append(function_id).append(", ").append(function_name).append("\n");
            function_count_map.merge(function_id + ", " + function_name, 1, Integer::sum);
            if (function_node.hasLabel(GO_LABEL)) {
                String sub_cats = (String) function_node.getProperty("sub category");
                String[] sub_cat_array = sub_cats.split(" & ");
                for (String sub_cat : sub_cat_array) {
                    String cat = (String) function_node.getProperty("category");
                    function_count_map.merge(sub_cat + cat, 1, Integer::sum);
                }
            }
        }
        return found_counter;
    }

    /**
     *
     * @param mrnas_per_genome
     * @param path
     */
    public static void create_all_functions_overview(HashMap<Integer, Node[]> mrnas_per_genome, String path) {
        ArrayList<Integer> genomes_without_anno = new ArrayList<>();
        String mrna_or_prot = "mRNA's", mrna_or_prot_file = "mrna";
        if (PROTEOME) {
            mrna_or_prot = "proteins";
            mrna_or_prot_file = "protein";
        }
        StringBuilder overview_builder = new StringBuilder();
        StringBuilder genome_builder = new StringBuilder();
        for (int i=1; i <= total_genomes; i++) {
            int mrna_with_anno = 0;
            HashMap<String, Integer> function_count_map = new HashMap<>();
            if (skip_array[i-1]) {
                continue;
            } else if (!mrnas_per_genome.containsKey(i)) {
                continue;
            }
            System.out.print("\rGenerating function overview: Genome " + i );
            Node[] mrnaNodes = mrnas_per_genome.get(i);
            overview_builder.append("#Genome ").append(i).append("\n");
            genome_builder.append("#Genome ").append(i).append("\n");
            StringBuilder info_per_mrna = new StringBuilder();
            int[] mrnas_with_function = new int[9]; //go, pfam, interpro, tigrfam, cog, bgc, phobius signal peptide, phobius transmem domains, signalp signalpep
            for (Node mrna_node : mrnaNodes) {
                boolean function_found = false;
                Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                long hm_node_id = hm_rel.getStartNodeId();
                long mrna_node_id = mrna_node.getId();
                if (PROTEOME) {
                    String mrna_id = (String) mrna_node.getProperty("protein_ID");
                    info_per_mrna.append("Protein id: ").append(mrna_id)
                            .append("\nProtein node id: ").append(mrna_node_id)
                            .append("\nHomology group node id: ").append(hm_node_id)
                            .append("\nFunction:\n");

                } else { // pangenome
                    String mrna_id = (String) mrna_node.getProperty("id");
                    Relationship gene_rel = mrna_node.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
                    Node gene_node = gene_rel.getStartNode();
                    long gene_node_id = gene_node.getId();
                    String gene_id = (String) gene_node.getProperty("id");
                    String name = retrieveNamePropertyAsString(gene_node);
                    if (name.equals("")) {
                        name = "-";
                    }

                    info_per_mrna.append("\nGene name: ").append(name)
                            .append("\nGene GFF id: ").append(gene_id)
                            .append("\nGene node id: ").append(gene_node_id)
                            .append("\nmRNA GFF id: ").append(mrna_id)
                            .append("\nmRNA node id: ").append(mrna_node_id)
                            .append("\nHomology group node id: ").append(hm_node_id)
                            .append("\nFunction:\n");
                    int bgc_counter = get_bgc_info(gene_node, info_per_mrna, function_count_map);
                    if (bgc_counter > 0) {
                        mrnas_with_function[5] ++;
                        function_found = true;
                    }
                }

                int go_counter = print_connected_function_node_info(mrna_node, RelTypes.has_go, info_per_mrna, function_count_map);
                if (go_counter > 0) {
                    mrnas_with_function[0] ++;
                    function_found = true;
                }
                int pfam_counter = print_connected_function_node_info(mrna_node, RelTypes.has_pfam, info_per_mrna, function_count_map);
                if (pfam_counter > 0) {
                    mrnas_with_function[1] ++;
                    function_found = true;
                }
                int interpro_counter = print_connected_function_node_info(mrna_node, RelTypes.has_interpro, info_per_mrna, function_count_map);
                if (interpro_counter > 0) {
                    mrnas_with_function[2] ++;
                    function_found = true;
                }

                int tigrfam_counter = print_connected_function_node_info(mrna_node, RelTypes.has_tigrfam, info_per_mrna, function_count_map);
                if (tigrfam_counter > 0) {
                    mrnas_with_function[3] ++;
                    function_found = true;
                }

                if (mrna_node.hasLabel(PHOBIUS_LABEL)) {
                    function_found = true;
                    if (mrna_node.hasProperty("phobius_signal_peptide")) {
                        String phobius_signalpep = (String) mrna_node.getProperty("phobius_signal_peptide");
                        if (phobius_signalpep.equals("yes")) {
                            function_count_map.merge("Phobius signal peptide", 1, Integer::sum);
                            info_per_mrna.append(" Phobius signal peptide").append("\n");
                            mrnas_with_function[6] ++;
                        }
                    }

                    if (mrna_node.hasProperty("phobius_transmembrane")) {
                        int transmem_domains = (int) mrna_node.getProperty("phobius_transmembrane");
                        if (transmem_domains > 0) {
                            function_count_map.merge("Phobius transmembrane domains", 1, Integer::sum);
                            info_per_mrna.append(transmem_domains).append(" Phobius transmembrane domains").append("\n");
                            mrnas_with_function[7] ++;
                        }
                    }
                }

                if (mrna_node.hasLabel(SIGNALP_LABEL)) {
                    function_found = true;
                    if (mrna_node.hasProperty("signalp_signal_peptide")) { // type can be 'yes', 'SP(Sec/SPI)', 'LIPO(Sec/SPII)' or 'TAT(Tat/SPI)'
                        String signalp_signalpep = (String) mrna_node.getProperty("signalp_signal_peptide");
                        if (signalp_signalpep.equals("yes")){
                           signalp_signalpep = "(no type)";
                        }
                        function_count_map.merge("SignalP signal peptide", 1, Integer::sum);
                        info_per_mrna.append(" SignalP ").append(signalp_signalpep).append(" signal peptide").append("\n");
                        mrnas_with_function[8] ++;
                    }
                }

                if (mrna_node.hasLabel(COG_LABEL)) {
                    Map<String, Object> props = mrna_node.getProperties();
                    function_found = true;
                    mrnas_with_function[4] ++;
                    String cog_cat = (String) mrna_node.getProperty("COG_category");
                    String cogId = (String) mrna_node.getProperty("COG_id");
                    String cogDescription = "";
                    if (mrna_node.hasProperty("COG_description")) {
                         cogDescription = "," + (String) mrna_node.getProperty("COG_description");
                    }
                    info_per_mrna.append(" COG category ").append(cog_cat).append(",") .append(cogId).append(cogDescription).append("\n");
                    function_count_map.merge("COG category " + cog_cat + "," + cogId, 1, Integer::sum);
                }
                info_per_mrna.append("\n");
                if (function_found) {
                    mrna_with_anno ++;
                }
            }

            overview_builder.append(mrna_with_anno).append("/").append(mrnaNodes.length).append(" ").append(mrna_or_prot).append(" have a functional annotation\n")
                    .append(info_per_mrna);
            genome_builder.append(mrna_with_anno).append("/").append(mrnaNodes.length).append(" ").append(mrna_or_prot).append(" have a functional annotation\n\n");
            create_function_per_genome_overview(function_count_map, genome_builder, mrnas_with_function);
            if (mrna_with_anno == 0) {
                genomes_without_anno.add(i);
            }
        }
        for (int genome_nr : genomes_without_anno) {
            mrnas_per_genome.remove(genome_nr);
        }

        write_SB_to_file_in_DB(overview_builder, path + "function_overview_per_" + mrna_or_prot_file + ".txt");
        write_SB_to_file_in_DB(genome_builder, path + "function_overview_per_genome.txt");
    }

    /**
     *
     * @param gene_node a 'gene' node
     * @param overview_builder
     * @param function_count_map
     * @return
     */
    public static int get_bgc_info(Node gene_node, StringBuilder overview_builder, HashMap<String, Integer> function_count_map) {
        Iterable<Relationship> rels = gene_node.getRelationships(RelTypes.part_of); // relation to bgc node
        int found_counter = 0;
        for (Relationship rel: rels) {
            int position = (int) rel.getProperty("position");
            Node bgc_node = rel.getEndNode();
            String type = (String) bgc_node.getProperty("type");
            int length = (int) bgc_node.getProperty("length");
            overview_builder.append(" gene ").append(position).append("/").append(length).append(" of '").append(type).append(" bgc");
            function_count_map.merge("gene of " + type + ", length " + length + "bgc", 1, Integer::sum);
            found_counter ++;
        }
        return found_counter;
    }

    /**
     *
     * @param function_count_map
     * @param genome_builder
     * @param mrnas_with_function
     */
    public static void create_function_per_genome_overview(HashMap<String, Integer> function_count_map,
                                                           StringBuilder genome_builder, int[] mrnas_with_function) {
        String mrna_or_prot = "mRNA's";
        if (PROTEOME) {
            mrna_or_prot = "proteins";
        }
        int[] function_count_array = new int[8]; //go, pfam, interpro, tigrfam, cog, bgc, effector, receptor, transmembrane
        StringBuilder pfam = new StringBuilder();
        StringBuilder signalp = new StringBuilder("with SignalP signal peptides: ");
        StringBuilder phobius_signalpep = new StringBuilder("with Phobius signal peptides: ");
        StringBuilder p_transmembrane = new StringBuilder("with Phobius transmembrane: ");
        StringBuilder go_bp = new StringBuilder("GO sub categories\n\nBiological process\n");
        StringBuilder go_mf = new StringBuilder("Molecular function\n");
        StringBuilder go_cl = new StringBuilder("Cellular component\n");
        StringBuilder go = new StringBuilder();
        StringBuilder interpro = new StringBuilder();
        StringBuilder tigrfam = new StringBuilder();
        StringBuilder cog = new StringBuilder();
        StringBuilder bgc = new StringBuilder();
        for (String key : function_count_map.keySet()) {
            int value = function_count_map.get(key);
            if (key.startsWith("PF")) {
                pfam.append(key).append(": ").append(value).append("\n");
                function_count_array[1] ++;
            } else if (key.startsWith("SignalP")) {
                signalp.append(value);
            } else if (key.startsWith("Phobius signal")) {
                phobius_signalpep.append(value);
            } else if (key.startsWith("Phobius transmembrane")) { // Phobius transmembrane domains
                p_transmembrane.append(value);
            } else if (key.startsWith("GO")) {
                function_count_array[0] ++;
                go.append(key).append(": ").append(value).append("\n");
            } else if (key.endsWith("molecular_function")) {
                key = key.replace("molecular_function","");
                if (key.equals("")) {
                    continue;
                }
                go_mf.append(" ").append(key).append(": ").append(value).append("\n");
            } else if (key.endsWith("biological_process")) {
                key = key.replace("biological_process","");
                if (key.equals("")) {
                    continue;
                }
                go_bp.append(" ").append(key).append(": ").append(value).append("\n");
            } else if (key.endsWith("cellular_component")) {
                key = key.replace("cellular_component","");
                if (key.equals("")) {
                    continue;
                }
                go_cl.append(" ").append(key).append(": ").append(value).append("\n");
            } else if (key.startsWith("IPR")) {
                interpro.append(key).append(": ").append(value).append("\n");
                function_count_array[2] ++;
            } else if (key.endsWith("bgc")) {
                function_count_array[5] ++;
                key = key.replace("bgc","");
                bgc.append(key).append(": ").append(value).append("\n");
            } else if (key.startsWith("TIGR")) { // tigr
                function_count_array[3] ++;
                tigrfam.append(key).append(": ").append(value).append("\n");
            } else if (key.startsWith("COG")) {
                cog.append(key).append(": ").append(value).append("\n");
                function_count_array[4] ++;
            } else {
                Pantools.logger.error("{} not recognized.", key);
                System.exit(1);
            }
        }

        genome_builder.append("Number of ").append(mrna_or_prot).append(" with at least one of the following functions, number of distinct functions found\n");
        if (mrnas_with_function[0] != 0) {
            genome_builder.append("GO: ").append(mrnas_with_function[0]).append(",").append(function_count_array[0]).append("\n");
        }
        if (mrnas_with_function[1] != 0) {
            genome_builder.append("PFAM: ").append(mrnas_with_function[1]).append(",").append(function_count_array[1]).append("\n");
        }
        if (mrnas_with_function[2] != 0) {
            genome_builder.append("InterPro: ").append(mrnas_with_function[2]).append(",").append(function_count_array[2]).append("\n");
        }
        if (mrnas_with_function[3] != 0) {
            genome_builder.append("TIGRFAM: ").append(mrnas_with_function[3]).append(",").append(function_count_array[3]).append("\n");
        }
        if (mrnas_with_function[4] != 0) {
            genome_builder.append("COG: ").append(mrnas_with_function[4]).append(",").append(function_count_array[4]).append("\n");
        }
        if (mrnas_with_function[5] != 0) {
            genome_builder.append("BGC: ").append(mrnas_with_function[5]).append(",").append(function_count_array[5]).append("\n");
        }
        if (mrnas_with_function[6] != 0) {
            genome_builder.append("Secreted proteins: ").append(mrnas_with_function[6]).append("\n");
        }
        if (mrnas_with_function[7] != 0) {
            genome_builder.append("Receptor: ").append(mrnas_with_function[7]).append("\n");
        }
        if (mrnas_with_function[8] != 0) {
            genome_builder.append("Transmembrane: ").append(mrnas_with_function[8]).append("\n");
        }
        genome_builder.append("\nFound functions\n")
                .append("GO: ").append("\n")
                .append(go).append("\n")
                .append(go_bp).append("\n")
                .append(go_mf).append("\n")
                .append(go_cl).append("\n")
                .append("PFAM domains: ").append("\n").append(pfam).append("\n")
                .append("InterPro domains: ").append("\n").append(interpro).append("\n")
                .append("TIGRFAMs: ").append("\n").append(tigrfam).append("\n")
                .append("Biosynthetic gene clusters: ").append("\n").append(bgc).append("\n")
                .append("COG proteins: ").append("\n").append(cog).append("\n");

        String signalp_str = signalp.toString();
        if (signalp_str.endsWith(": ")) {
            signalp_str += 0;
        }
        genome_builder.append(signalp_str).append("\n");

        String phobius_signalpep_str = phobius_signalpep.toString();
        if (phobius_signalpep_str.endsWith(": ")) {
            phobius_signalpep_str += 0;
        }
        genome_builder.append(phobius_signalpep_str).append("\n");

        String transmembrane_str = p_transmembrane.toString();
        if (transmembrane_str.endsWith(": ")) {
            transmembrane_str += 0;
        }
        genome_builder.append(transmembrane_str).append("\n\n");
    }

    /**
     *
     * @return
     */
    public static HashMap<Integer, Node[]> get_mrna_nodes_by_hm_or_ids() {
        String mrna_or_prot = "mRNA's";
        if (PROTEOME) {
            mrna_or_prot = "proteins";
        }
        HashMap<Integer, ArrayList<Node>> temp_genes_per_genome = new HashMap<>();
        HashMap<Integer, Node[]> genes_per_genome = new HashMap<>();
        if (SELECTED_HMGROUPS == null && NODE_ID == null) {
            Pantools.logger.error("No genes were provided via --homology-groups/-hm or --node.");
            System.exit(1);
        } else if (SELECTED_HMGROUPS != null && NODE_ID != null) {
            Pantools.logger.error("Use either the --homology-groups/-hm or --node command.");
            System.exit(1);
        }

        if (SELECTED_HMGROUPS != null) {
            ArrayList<Node> hmNodeList;
            try {
                hmNodeList = findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1);
            } catch (IOException ioe) {
                Pantools.logger.error("Could not read homology group file: {}", SELECTED_HMGROUPS);
                throw new RuntimeException(ioe);
            }
            if (hmNodeList.isEmpty()) {
                HashMap<Integer, String[]> hms_per_genome = Classification.read_unique_hmgroups();
                for (Map.Entry<Integer, String[]> entry : hms_per_genome.entrySet()) {
                    int genome_nr = entry.getKey();
                    if (skip_array[genome_nr-1]) {
                        continue;
                    }
                    String[] values = entry.getValue();
                    for (String hm_str : values) {
                        long id = Long.parseLong(hm_str);
                        Node hm_node = GRAPH_DB.getNodeById(id);
                        test_if_correct_label(hm_node, HOMOLOGY_GROUP_LABEL, true);
                        Iterable<Relationship> relations = hm_node.getRelationships();
                        for (Relationship rel: relations) {
                            Node mrna_node = rel.getEndNode();
                            temp_genes_per_genome.computeIfAbsent(genome_nr, i -> new ArrayList<>()).add(mrna_node);
                        }
                    }
                }
            } else {
                int counter = 0;
                for (Node hm_node : hmNodeList) {
                    counter ++;
                    if (counter % 10 == 0 || counter == hmNodeList.size()) {
                        System.out.print("\rRetrieving homology groups and connected " + mrna_or_prot + ": " + counter + "/" + hmNodeList.size() + " groups");
                    }

                    test_if_correct_label(hm_node, HOMOLOGY_GROUP_LABEL, true);
                    Iterable<Relationship> relations = hm_node.getRelationships();
                    for (Relationship rel : relations) {
                        Node mrna_node = rel.getEndNode();
                        int genome_nr = (int) mrna_node.getProperty("genome");
                        if (skip_array[genome_nr-1]) {
                            continue;
                        }
                        temp_genes_per_genome.computeIfAbsent(genome_nr, i -> new ArrayList<>()).add(mrna_node);
                    }
                }
                System.out.println();
            }
        } else if (NODE_ID != null) {
            if (NODE_ID_long != null) {
                Pantools.logger.error("finish this code.");
                System.exit(1);
            }
            String[] gene_array;
            if (NODE_ID.contains(",")) { // string file provided on command line
                if (NODE_ID.contains(", ")) {
                    gene_array = NODE_ID.split(", ");
                } else {
                    gene_array = NODE_ID.split(",");
                }
                Pantools.logger.info("Selected {} nodes.", gene_array.length);
                for (String gene_str: gene_array) {
                    long id = Long.parseLong(gene_str);
                    Node mrna_node = GRAPH_DB.getNodeById(id);
                    test_if_correct_label(mrna_node, MRNA_LABEL, true);
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    temp_genes_per_genome.computeIfAbsent(genome_nr, i -> new ArrayList<>()).add(mrna_node);
                }
             } else { // read an input file
                String node_str = "";
                try (BufferedReader in = new BufferedReader(new FileReader(NODE_ID))) {
                    int line_count = 0;
                    for (int c = 0; in.ready();) {
                        node_str = in.readLine().trim();
                        line_count++;
                    }
                    if (line_count != 1) {
                        Pantools.logger.warn("File has more than 1 line.");
                    }
                } catch (IOException e) {
                    Pantools.logger.error(e.getMessage());
                    System.exit(1);
                }

                if (node_str.contains(", ")) {
                    gene_array = node_str.split(", ");
                } else {
                    gene_array = node_str.split(", ");
                }

                for (String gene_str: gene_array) {
                    long id = Long.parseLong(gene_str);
                    Node mrna_node = GRAPH_DB.getNodeById(id);
                    test_if_correct_label(mrna_node, MRNA_LABEL, true);
                    int genome = (int) mrna_node.getProperty("genome");
                    temp_genes_per_genome.computeIfAbsent(genome, i -> new ArrayList<>()).add(mrna_node);
                }
            }
        }

        int counter = 0;
        for (Map.Entry<Integer, ArrayList<Node>> entry : temp_genes_per_genome.entrySet()) {
            int key = entry.getKey();
            ArrayList<Node> value_list = entry.getValue();
            counter ++;
            Node[] node_array = value_list.toArray(new Node[value_list.size()]);
            genes_per_genome.put(key, node_array);
        }
        return genes_per_genome;
    }

    /**
     *
     */
    public void function_of_mRNAs() {
        Pantools.logger.info("Retrieving functional annotations from a set of mRNAs or homology groups.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            K_SIZE = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("k_mer_size");
            total_genomes = (int) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("num_genomes");
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success();
        } catch (NotFoundException | ClassCastException nee) {
            Pantools.logger.error("The function 'function_of_mRNAs' does not work on a panproteome.");
            System.exit(1);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) {
            HashMap<Integer, Node[]> mrnas_per_genome = get_mrna_nodes_by_hm_or_ids();
            create_all_functions_overview(mrnas_per_genome, "function/");
            tx.success();
        }
        Pantools.logger.info("Ouput written to:");
        Pantools.logger.info(" {}function/function_overview_per_mrna.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}function/function_overview_per_genome.txt", WORKING_DIRECTORY);
    }

    /**
     * Prepares a csv overview with one function (node) per line
     * Can be used on go, pfam, tigrfam and interpro nodes
     * @param function_label
     * @param reltype
     * @return
     */
    public static StringBuilder count_function_node_connections_fa_overview(Label function_label, RelationshipType reltype) {
        if (hmgroup_class_map == null) {
            hmgroup_class_map = new HashMap<>();
        }
        HashMap<String, HashSet<Node>> hmgroups_per_class = new HashMap<>();
        long counted_nodes = 0, total_rels = 0;
        String label_str = function_label.toString().replace("_label", "");
        long[] total_array = new long[3]; // total term, total relations, total unconnected
        long[] rel_per_genome_array = new long[total_genomes+1];
        long[] term_per_genome_array = new long[total_genomes+1];
        StringBuilder missing = new StringBuilder();
        StringBuilder output_builder = new StringBuilder();
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(function_label);
        int total_nodes = (int) count_nodes(function_label); // count nodes with a specific label
        while (all_nodes.hasNext()) {
            StringBuilder line_builder = new StringBuilder();
            StringBuilder mrna_info_builder = new StringBuilder();
            StringBuilder all_mrnas = new StringBuilder();
            HashSet<Long> hmgroups_set = new HashSet<>();
            Node function_node = all_nodes.next();
            long funtion_node_id = function_node.getId();
            String function_name = (String) function_node.getProperty("name");
            String function_id = (String) function_node.getProperty("id");
            Iterable<Relationship> all_relations = function_node.getRelationships(reltype);
            int[] frequency_array;
            if ( function_node.hasProperty("frequency")) {
                frequency_array = (int[]) function_node.getProperty("frequency");
            } else {
                frequency_array = new int[0]; // never used because the node has no connections
            }

            boolean first_rel = true;
            counted_nodes ++;
            if (counted_nodes % 100 == 0 || counted_nodes < 100 || counted_nodes == total_nodes) {
                System.out.print("\rChecking for mRNAs connected to '" + label_str + "' nodes: " + counted_nodes + "/" + total_nodes + "                 ");
            }

            for (Relationship rel : all_relations) {
                total_rels ++;
                Node mrna_node = rel.getStartNode();
                long mrna_node_id = mrna_node.getId();
                int genome_nr = (int) mrna_node.getProperty("genome");
                if (PROTEOME && skip_array[genome_nr-1]) {
                    continue;
                } else if (!PROTEOME) {
                    String annotation_id = (String) mrna_node.getProperty("annotation_id");
                    if (!annotation_identifiers.contains(annotation_id)) {
                        continue;
                    }
                }
                all_mrnas.append(mrna_node_id).append(" ");
                Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                String hm_id_str = "not grouped";
                if (hm_rel == null) {
                   // do nothing
                } else {
                    Node hm_node = hm_rel.getStartNode();
                    long hm_id = hm_node.getId();
                    hmgroups_set.add(hm_id);
                    hm_id_str = hm_id +"";
                    String hmgroup_class = hmgroup_class_map.get(hm_node);
                    if (hmgroup_class == null) {
                        hmgroup_class = "no group category";
                    }
                    hmgroups_per_class.computeIfAbsent(hmgroup_class, s -> new HashSet<>()).add(hm_node);
                }
                if (first_rel) {
                    line_builder.append(function_id).append(";").append(function_name).append(";").append(funtion_node_id).append(";");
                    first_rel = false;
                }

                if (!PROTEOME) { // pangenome
                    String mrna_id = (String) mrna_node.getProperty("id");
                    String mrna_name = retrieveNamePropertyAsString(mrna_node);
                    if (mrna_name.equals("")) {
                        mrna_name = "-";
                    }
                    mrna_info_builder.append(genome_nr).append(",").append(mrna_name).append(",").append(mrna_id).append(",")
                            .append(mrna_node_id).append(",").append(hm_id_str).append(";");
                } else { // panproteome
                    String mrna_id = (String) mrna_node.getProperty("protein_ID");
                    mrna_info_builder.append(genome_nr).append(" ").append(mrna_id).append(",").append(mrna_node_id).append(",").append(hm_id_str).append(";");
                }

                if (frequency_array[genome_nr-1] == 1) {
                    term_per_genome_array[genome_nr] += 1;
                }
                rel_per_genome_array[genome_nr] += 1;
            }

            if (first_rel) { // true means the function node is not connected to any node
                missing.append(function_id).append(",").append(function_name).append(",").append(funtion_node_id).append("\n");
                total_array[2] += 1;
            } else {
                line_builder.append(Arrays.toString(frequency_array).replace("[","").replace("]","").replace(",",";").replace(" ",""));
                String groupsStr = hmgroups_set.toString().replace("[","").replace("]","").replace(", "," ").replaceFirst(".$","");
                output_builder.append(line_builder).append(";").append(all_mrnas).append(";").append(groupsStr).append(";")
                        .append(mrna_info_builder).append("\n");
            }
        }
        total_array[0] = counted_nodes;
        total_array[1] = total_rels;
        StringBuilder per_genome_str = new StringBuilder();
        for (int i = 1; i < rel_per_genome_array.length; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            per_genome_str.append("Genome ").append(i).append(",").append(term_per_genome_array[i]).append(",").append(rel_per_genome_array[i]).append("\n");
        }
        per_genome_str.append("\n");

        StringBuilder results = new StringBuilder();
        results.append("Number of ").append(label_str).append(" terms in pangenome: ").append(total_array[0])
                .append("\nTotal: ").append(total_array[0] - total_array[2]).append(" connected to a gene, ").append(total_array[1]).append(" total connections\n\n");

        HashMap<String, Integer> class_counter = count_hmgroups_per_class(hmgroups_per_class);
        add_annotated_to_stringbuilder(class_counter, hmgroups_per_class, results, label_str) ;
        results.append(per_genome_str).append("\n")
                .append("#Connected functions. Split on semicolon\n")
                .append("Function identifier;function name;function node id;");

        for (int i=1; i <= total_genomes; i++) {
            results.append("occurrence in genome ").append(i).append(";");
        }

        results.append("mRNA identifiers (separated by spaces);homology group identifiers (separated by spaces);"
                + "Remaining columns contain information of connected mRNA's (separated by commas): genome number, "
                + "mRNA name, mRNA id, mRNA node id, homology group node id\n");

        results.append(output_builder)
                .append("\n#Not connected functions\n")
                .append("function id, function name, node id\n")
                .append(missing).append("\n");
        add_missing_groups_to_stringbuilder(hmgroups_per_class, results, label_str);
        System.out.print("\r                                                                       "); // spaces are intentional
        return results;
    }

    /**
     *
     * @param hmgroups_per_class
     * @return
     */
    public static HashMap<String, Integer> count_hmgroups_per_class(HashMap<String, HashSet<Node>> hmgroups_per_class) {
        HashMap<String, Integer> class_counter = new HashMap<>();
        for (Node hm_node : hmgroup_class_map.keySet()) {
            String class1 = hmgroup_class_map.get(hm_node);
            class_counter.merge(class1, 1, Integer::sum);
            if (hmgroups_per_class.containsKey(class1)) {
                HashSet<Node> hm_set = hmgroups_per_class.get(class1);
                if (!hm_set.contains(hm_node)) {
                    hmgroups_per_class.computeIfAbsent(class1 + "_missing", s -> new HashSet<>()).add(hm_node);
                }
            }
        }
        return class_counter;
    }

    /**
     *
     * @param class_counter
     * @param hmgroups_per_class
     * @param results
     * @param node_label
     */
    public static void add_annotated_to_stringbuilder(HashMap<String, Integer> class_counter, HashMap<String, HashSet<Node>> hmgroups_per_class,
            StringBuilder results, String node_label) {

        int total_core_groups = 0, total_accessory_groups = 0, total_unique_groups = 0;
        if (class_counter.containsKey("core")) {
            total_core_groups = class_counter.get("core");
        }
        if (class_counter.containsKey("accessory")) {
            total_accessory_groups = class_counter.get("accessory");
        }
        if (class_counter.containsKey("unique")) {
            total_unique_groups = class_counter.get("unique");
        }
        int total_hmgroups = total_core_groups + total_accessory_groups + total_unique_groups;

        int core_groups = 0, accessory_groups = 0, unique_groups = 0;
        if (hmgroups_per_class.containsKey("core")) {
            HashSet<Node> core_set = hmgroups_per_class.get("core");
            core_groups = core_set.size();
        }
        if (hmgroups_per_class.containsKey("accessory")) {
            HashSet<Node> accessory_set = hmgroups_per_class.get("accessory");
            accessory_groups = accessory_set.size();
        }

        if (hmgroups_per_class.containsKey("unique")) {
            HashSet<Node> unique_set = hmgroups_per_class.get("unique");
            unique_groups = unique_set.size();
        }

        int annotated_hmgroups = core_groups + accessory_groups + unique_groups;
        results.append("Homology groups with ").append(node_label).append(": ").append(annotated_hmgroups).append("/").append(total_hmgroups).append("\n");
        if (total_core_groups > 0) {
            String core_percentage = get_percentage_str(core_groups, total_core_groups, 2);
            results.append(" Core: ").append(core_groups).append("/").append(total_core_groups).append(" (").append(core_percentage).append("%)\n");
        }
        if (total_accessory_groups > 0) {
            String accessory_percentage = get_percentage_str(accessory_groups, total_accessory_groups, 2);
            results.append(" Accessory: ").append(accessory_groups).append("/").append(total_accessory_groups).append(" (").append(accessory_percentage).append("%)\n");
        }
        if (total_unique_groups > 0) {
            String unique_percentage = get_percentage_str(unique_groups, total_unique_groups, 2);
            results.append(" Unique: ").append(unique_groups).append("/").append(total_unique_groups).append(" (").append(unique_percentage).append("%)\n");
        }
        results.append("\n");
    }

    /**
     *
     * @param hmgroups_per_class
     * @param results
     * @param node_label
     */
    public static void add_missing_groups_to_stringbuilder(HashMap<String, HashSet<Node>> hmgroups_per_class, StringBuilder results, String node_label) {
        HashSet<Node>  missing_core_set = retrieve_hashet_return_empty_when_null(hmgroups_per_class, "core_missing");
        HashSet<Node> missing_accessory_set = retrieve_hashet_return_empty_when_null( hmgroups_per_class, "accessory_missing");
        HashSet<Node> missing_unique_set = retrieve_hashet_return_empty_when_null(hmgroups_per_class, "unique_missing");

        results.append("\n#no ").append(node_label).append(", ").append(missing_core_set.size()).append(" core groups\n");
        for (Node noanno : missing_core_set ) {
            long node_id = noanno.getId();
            results.append(node_id).append(",");
        }
        results.append("\n#no ").append(node_label).append(", ").append(missing_accessory_set.size()).append(" accessory groups\n");
        for (Node noanno : missing_accessory_set) {
            long node_id = noanno.getId();
            results.append(node_id).append(",");
        }
        results.append("\n#no ").append(node_label).append(", ").append(missing_unique_set.size()).append(" unique groups\n");
        for (Node noanno : missing_unique_set) {
            long node_id = noanno.getId();
            results.append(node_id).append(",");
        }
    }

    /**
     * Retrieve phobius and signalp nodes and create an overview
     */
    public static void signal_peptides_overview() {
        long total_signalp_nodes = count_nodes(SIGNALP_LABEL); // count nodes with a specific label
        long total_phobius_nodes = count_nodes(PHOBIUS_LABEL); // count nodes with a specific label
        if (total_signalp_nodes == 0 && total_phobius_nodes == 0) {
            return;
        }
        if (hmgroup_class_map == null) {
            hmgroup_class_map = new HashMap<>();
        }
        HashMap<String, HashSet<Node>> hmgroups_per_class = new HashMap<>();
        HashMap<String, StringBuilder> info_per_genome_map = new HashMap<>();
        HashMap<Integer, int[]> function_count_map = new HashMap<>();
        for (int i=1; i <= total_genomes; i++) {
            int[] counts = new int[3]; // [phobius transmembrane, phobius signal peptide, signalp signal peptide]
            function_count_map.put(i, counts);
            StringBuilder mrna_builder = new StringBuilder();
            info_per_genome_map.put(i + "#signalp_signalpep", mrna_builder);
            mrna_builder = new StringBuilder();
            info_per_genome_map.put(i + "#phobius_signalpep", mrna_builder);
            mrna_builder = new StringBuilder();
            info_per_genome_map.put(i + "#phobius_transmembrane", mrna_builder);
        }
        count_signalp_nodes_for_function_overview(function_count_map, hmgroups_per_class, info_per_genome_map);
        count_phobius_nodes_for_function_overview(function_count_map, hmgroups_per_class, info_per_genome_map);

        StringBuilder output_builder = new StringBuilder();
        HashMap<String, Integer> class_counter = count_hmgroups_per_class(hmgroups_per_class);
        add_annotated_to_stringbuilder(class_counter, hmgroups_per_class, output_builder, "phobius");
        output_builder.append("# File consists of 4 parts.\n"
                + "# Part 1. Total number of mRNA's/proteins with signal peptides & transmembrane domains found per genome\n"
                + "# Part 2. SignalP signal peptides\n"
                + "# Part 3. Phobius signal peptides\n"
                + "# Part 4. Phobius transmembrane domains\n\n");

        for (int i=1; i<= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            int[] counts = function_count_map.get(i); // [phobius transmembrane, phobius signal peptide, signalp signal peptide]
            output_builder.append("Genome ").append(i).append("\n ")
                    .append("SignalP signal peptides: ").append(counts[2]).append("\n ")
                    .append("Phobius signal peptides: ").append(counts[1]).append("\n ")
                    .append("Phobius transmembrane domains: ").append(counts[0]).append("\n\n");
        }

        StringBuilder signalp_signalpep_builder = new StringBuilder("#Part 2. SignalP signal peptides\n# Signal peptide type (only v5.0 has types), mRNA/gene name, mRNA id, mRNA node id, homology group node id\n\n");
        StringBuilder phobius_signalpep_builder = new StringBuilder("#Part 3. Phobius signal peptides\n# mRNA/gene name, mRNA id, mRNA node id, homology group node id\n\n");
        StringBuilder transmembrane_builder = new StringBuilder("#Part 4. Phobius transmembrane domains\n# mRNA/gene name, mRNA id, mRNA node id, homology group node id\n\n");
        if (PROTEOME) {
            signalp_signalpep_builder = new StringBuilder("#Part 2. SignalP signal peptides\n# Protein id, protein node id, homology group node id\n\n");
            phobius_signalpep_builder = new StringBuilder("#Part 3. Phobius signal peptides\n# Protein id, protein id, homology group node id\n\n");
            transmembrane_builder = new StringBuilder("#Part 4. Phobius transmembrane domains\n# Protein id, protein node id, homology group node id\n\n");
        }

        for (int i=1; i<= total_genomes; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            StringBuilder value = info_per_genome_map.get(i + "#signalp_signalpep");
            signalp_signalpep_builder.append("Genome ").append(i).append("\n").append(value.toString()).append("\n\n");

            value = info_per_genome_map.get(i + "#phobius_signalpep");
            phobius_signalpep_builder.append("Genome ").append(i).append("\n").append(value.toString()).append("\n\n");

            value = info_per_genome_map.get(i + "#phobius_transmembrane");
            transmembrane_builder.append("Genome ").append(i).append("\n").append(value.toString()).append("\n\n");
        }
        output_builder.append(signalp_signalpep_builder)
                .append("\n").append(phobius_signalpep_builder)
                .append("\n").append(transmembrane_builder);
        add_missing_groups_to_stringbuilder(hmgroups_per_class, output_builder, "phobius");

        if (output_builder.length() > 0) {
            write_SB_to_file_in_DB(output_builder, "function/phobius_signalp_overview.csv");
        }
        System.out.println("\r                                                               ");
    }

    /**
     *
     * @param function_count_map
     * @param hmgroups_per_class
     * @param info_per_genome_map
     */
    public static void count_phobius_nodes_for_function_overview(HashMap<Integer, int[]> function_count_map, HashMap<String, HashSet<Node>> hmgroups_per_class,
                                                                 HashMap<String, StringBuilder> info_per_genome_map) {

        int node_counter = 0;
        int total_nodes = (int) count_nodes(PHOBIUS_LABEL); // count nodes with a specific label
        if (total_nodes == 0) {
            return;
        }
        ResourceIterator<Node> phobius_nodes = GRAPH_DB.findNodes(PHOBIUS_LABEL);
        while (phobius_nodes.hasNext()) { // phobius nodes are mrna nodes
            node_counter ++;
            Node mrna_node = phobius_nodes.next();
            int genome_nr = (int) mrna_node.getProperty("genome");
            if (PROTEOME && skip_array[genome_nr-1]) {
                continue;
            } else if (!PROTEOME) {
                String annotation_id = (String) mrna_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }

            if (node_counter % 100 == 0 || node_counter == total_nodes) {
                System.out.print("\rExtracting 'phobius' nodes: " + node_counter + "/" + total_nodes + "                 ");
            }
            String mrna_name = "", mrna_id;
            if (!PROTEOME) {
                mrna_name = retrieveNamePropertyAsString(mrna_node);
                mrna_id = (String) mrna_node.getProperty("id");
                if (mrna_name.equals("")) {
                    mrna_name = "-";
                }
            } else {
                mrna_id = (String) mrna_node.getProperty("protein_ID");
            }
            long mrna_node_id = mrna_node.getId();
            if (!mrna_node.hasProperty("phobius_signal_peptide")) {
                continue;
            }

            String sigal_peptide = (String) mrna_node.getProperty("phobius_signal_peptide");
            int transmembrane = (int) mrna_node.getProperty("phobius_transmembrane");
            Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog,Direction.INCOMING);
            Node hm_node = hm_rel.getStartNode();
            long hm_id = hm_node.getId();
            String hmgroup_class = hmgroup_class_map.get(hm_node);
            hmgroups_per_class.computeIfAbsent(hmgroup_class, s -> new HashSet<>()).add(hm_node);
            String mrna_info = mrna_name + "," + mrna_id + "," + mrna_node_id + "," + hm_id + "\n";
            if (PROTEOME) {
                mrna_info = mrna_id + "," + mrna_node_id + "," + hm_id + "\n";
            }

            int[] function_counts = function_count_map.get(genome_nr); // [phobius transmembrane, phobius signal peptide, signalp signal peptide]
            if (sigal_peptide.equals("yes")) {
                function_counts[1] ++;
                info_per_genome_map.computeIfAbsent(genome_nr + "#phobius_signalpep", s -> new StringBuilder())
                        .append(mrna_info);
            }
            if (transmembrane > 0) {
                function_counts[0] ++;
                info_per_genome_map.computeIfAbsent(genome_nr + "#phobius_signalpep", s -> new StringBuilder())
                        .append(mrna_info);
            }
            function_count_map.put(genome_nr, function_counts);
        }
    }

    /**
     *
     * @param function_count_map
     * @param hmgroups_per_class
     * @param info_per_genome_map
     */
    public static void count_signalp_nodes_for_function_overview(HashMap<Integer, int[]> function_count_map, HashMap<String, HashSet<Node>> hmgroups_per_class,
            HashMap<String, StringBuilder> info_per_genome_map) {

        long total_signalp_nodes = count_nodes(SIGNALP_LABEL); // count nodes with a specific label
        if (total_signalp_nodes == 0) {
            return;
        }
        ResourceIterator<Node> signalp_nodes = GRAPH_DB.findNodes(SIGNALP_LABEL);
        int node_counter = 0;
        while (signalp_nodes.hasNext()) { // signalp nodes are mrna nodes
            node_counter ++;
            Node mrna_node = signalp_nodes.next();
            long mrna_node_id = mrna_node.getId();
            int genome_nr = (int) mrna_node.getProperty("genome");
            if (PROTEOME && skip_array[genome_nr-1]) {
                continue;
            } else if (!PROTEOME) {
                String annotation_id = (String) mrna_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }

            if (node_counter % 100 == 0 || node_counter == total_signalp_nodes) {
                System.out.print("\rExtracting 'signalp' nodes: " + node_counter + "/" + total_signalp_nodes + "                 ");
            }
            String mrna_name = "", mrna_id;
            if (!PROTEOME) {
                mrna_name = retrieveNamePropertyAsString(mrna_node);
                mrna_id = (String) mrna_node.getProperty("id");
                if (mrna_name.equals("")) {
                    mrna_name = "-";
                }
            } else {
                mrna_id = (String) mrna_node.getProperty("protein_ID");
            }

            Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
            long hm_id  = 0;
            if (hm_rel == null) {

            } else {
                Node hm_node = hm_rel.getStartNode();
                hm_id = hm_node.getId();

                String hmgroup_class = hmgroup_class_map.get(hm_node);
                if (hmgroup_class == null) {
                    hmgroup_class = "no group category";
                }

                hmgroups_per_class.computeIfAbsent(hmgroup_class, s -> new HashSet<>()).add(hm_node);
            }
            String mrna_info = mrna_name + "," + mrna_id + "," + mrna_node_id + "," + hm_id + "\n";
            if (PROTEOME) {
                mrna_info = mrna_id + "," + mrna_node_id + "," + hm_id + "\n";
            }
            if (!mrna_node.hasProperty("signalp_signal_peptide")) {
                continue;
            }
            String peptide_type = (String) mrna_node.getProperty("signalp_signal_peptide"); // type can be 'yes', 'SP(Sec/SPI)', 'LIPO(Sec/SPII)' or 'TAT(Tat/SPI)'
            if (peptide_type.equals("yes")) {
                peptide_type = "no type";
            }
            mrna_info = peptide_type + "," + mrna_info;
            info_per_genome_map.computeIfAbsent(genome_nr + "#signalp_signalpep", s -> new StringBuilder())
                    .append(mrna_info);
            int[] function_counts = function_count_map.get(genome_nr); // [phobius transmembrane, phobius signal peptide, signalp signal peptide]
            function_counts[2] ++;
            function_count_map.put(genome_nr, function_counts);
        }
    }


    /**
     *
     * @return
     */
    public static String get_go_sub_category_overview() {
        String[] biop_array = {"biological phase","immune system process","sulfur utilization","cell killing","multi-organism process","locomotion","growth",
                "carbon utilization","response to stimulus","metabolic process","biological regulation","phosphorus utilization","signaling","developmental process",
                "reproduction","detoxification","cellular component organization or biogenesis","reproductive process","nitrogen utilization","behavior",
                "cell population proliferation","carbohydrate utilization","biological adhesion","cell aggregation","pigmentation","localization","cellular process",
                "rhythmic process","multicellular organismal process","negative regulation of biological process","positive regulation of biological process",
                "regulation of biological process"};
        String[] molf_array = {"cargo receptor activity","protein tag","catalytic activity","binding","antioxidant activity","transporter activity",
                "nutrient reservoir activity","transcription regulator activity","translation regulator activity","molecular sequestering activity","molecular carrier activity",
                "toxin activity","molecular transducer activity","molecular function regulator","small molecule sensor activity","cargo adaptor activity",
                "structural molecule activity","protein folding chaperone"};
        String[] celc_array = {"symplast","other organism part","membrane-enclosed lumen","membrane","extracellular region part","cell junction",
                "mitochondrion-associated adherens complex","organelle part","membrane part","virion part","nucleoid","virion","synapse part","organelle","host",
                "cell","other organism","synapse","cell part","protein-containing complex","supramolecular complex","extracellular region"};

        StringBuilder output_builder = new StringBuilder("#The occurrence per genome of the child nodes from: biological process, molecular functions, and cellular component.\n")
                .append("GO term,");
        for (int i=1; i <= total_genomes; i++) {
            if (i == total_genomes) {
                output_builder.append("frequency in genome ").append(i);
            } else {
                output_builder.append("frequency in genome ").append(i).append(",");
            }
        }
        output_builder.append("\n");
        ResourceIterator<Node> go_nodes = GRAPH_DB.findNodes(GO_LABEL);
        HashMap<String, int[]> sub_category_freq_map = new HashMap<>();
        while (go_nodes.hasNext()) {
            Node go_node = go_nodes.next();
            String sub_category = (String) go_node.getProperty("sub category");
            if (sub_category.equals("")) {
                continue;
            }
            String[] category_array = sub_category.split(" & ");
            Iterable<Relationship> has_go_rels = go_node.getRelationships(RelTypes.has_go);
            for (Relationship rel: has_go_rels) {
                Node mrna_node = rel.getStartNode();
                int genome = (int) mrna_node.getProperty("genome");
                for (String sub_cat: category_array) {
                    int[] rel_per_genome_array = sub_category_freq_map.get(sub_cat);
                    if (rel_per_genome_array == null) {
                        rel_per_genome_array = new int[total_genomes];
                    }
                    rel_per_genome_array[genome-1] += 1;
                    sub_category_freq_map.put(sub_cat, rel_per_genome_array);
                }
            }
        }

        output_builder.append("biological_process\n\n");
        for (String biop_str: biop_array) {
            int[] value1 = sub_category_freq_map.get(biop_str);
            if (value1 != null) {
                output_builder.append(biop_str).append(",").append(Arrays.toString(value1).replace("[","").replace("]","").replace(" ","")).append("\n");
            }
        }

        output_builder.append("\nmolecular_function\n\n");
        for (String molf_str: molf_array) {
            int[] value1 = sub_category_freq_map.get(molf_str);
            if (value1 != null) {
                output_builder.append(molf_str).append(",").append(Arrays.toString(value1).replace("[","").replace("]","").replace(" ","")).append("\n");
            }
        }

        output_builder.append("\ncellular_component\n\n");
        for (String celc_str: celc_array) {
            int[] value1 = sub_category_freq_map.get(celc_str);
            if (value1 != null) {
                output_builder.append(celc_str).append(",").append(Arrays.toString(value1).replace("[","").replace("]","").replace(" ","")).append("\n");
            }
        }
        return output_builder.toString();
    }

    /**
     *
     * @param relation
     * @param target_node
     * @param output_builder
     * @param function_type
     */
    public static void check_connected_mrna_nodes(RelationshipType relation, Node target_node, StringBuilder output_builder, String function_type) {
        boolean present_genes = false;
        Iterable<Relationship> mrna_rels = target_node.getRelationships(relation);
        for (Relationship rel : mrna_rels) {
            if (!present_genes) {
                if (PROTEOME) {
                    output_builder.append("Proteins connected to the selected '").append(function_type)
                            .append("' node. On each line: Protein id, protein node id, homology group node id\n");
                } else {
                    output_builder.append("mRNA's connected to the selected '").append(function_type)
                            .append("' node. On each line: mRNA name, mRNA id, mRNA node id, address, homology group node id\n");
                }
                present_genes = true;
            }
            Node mrna_node = rel.getStartNode();
            Relationship hm_rel = mrna_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
            Node hm_node = hm_rel.getStartNode();
            long hm_node_id = hm_node.getId();
            long mrna_node_id = mrna_node.getId();

            if (PROTEOME) {
                String mrna_id = (String) mrna_node.getProperty("protein_ID");
                output_builder.append(" ").append(mrna_id).append(",").append(mrna_node_id).append(",").append(mrna_node_id).append(",").append(hm_node_id).append("\n");
            } else {
                String mrna_name = retrieveNamePropertyAsString(mrna_node);
                if (mrna_name.equals("")) {
                    mrna_name = "-";
                }
                String mrna_id = (String) mrna_node.getProperty("id");
                int[] address = (int[]) mrna_node.getProperty("address");
                output_builder.append(" ").append(mrna_name).append(",").append(mrna_id).append(",").append(mrna_node_id).append(",").append(address[0]).append("_")
                        .append(address[1]).append("_").append(address[2]).append("_").append(address[3]).append(",").append(hm_node_id).append("\n");
            }
        }
        if (present_genes == false) {
            if (PROTEOME) {
                output_builder.append("No proteins were connected to this GO term!\n");
            } else {
                output_builder.append("No mRNA's were connected to this GO term!\n");
            }
        } else {
            output_builder.append("\n");
        }
    }

    /**
     *
     * @param genes_per_genome
     * @return
     */
    public static HashMap<String, StringBuilder> create_go_SB_hashmap(HashMap<Integer, Node[]> genes_per_genome) {
        HashMap<String, StringBuilder> go_tree_per_genome = new HashMap<>();
        for (int genome_nr : genes_per_genome.keySet()) {
            StringBuilder empty1 = new StringBuilder("digraph unix {\n  \"biological_process\" [shape=diamond, style=filled];\n  node [shape=box];\n\n");
            StringBuilder empty2 = new StringBuilder("digraph unix {\n  \"molecular_function\" [shape=diamond, style=filled];\n  node [shape=box];\n\n");
            StringBuilder empty3 = new StringBuilder("digraph unix {\n  \"cellular_component\" [shape=diamond, style=filled];\n  node [shape=box];\n\n");
            go_tree_per_genome.put(genome_nr + "_biological_process", empty1);
            go_tree_per_genome.put(genome_nr + "_molecular_function", empty2);
            go_tree_per_genome.put(genome_nr + "_cellular_component", empty3);
            create_directory_in_DB("/function/go_enrichment/results_per_genome/" + genome_nr);
        }
        return go_tree_per_genome;
    }

    /**
     * Create csv overview file for biosynthetic gene clusters
     */
    public void bgc_overview() {
        if (PROTEOME) { // geneclusters cannot be added to panproteomes
            return;
        }
        HashMap<String, int[]> counts_per_type = new HashMap<>(); // key is type, value are the counts per genome
        int longest_cluster = 0;
        int[] counter_array = new int[total_genomes+1]; // first position is total count for all genomes
        ResourceIterator<Node> bgc_nodes = GRAPH_DB.findNodes(BGC_LABEL);
        while (bgc_nodes.hasNext()) {
            Node bgc_node = bgc_nodes.next();
            int genome_nr = (int) bgc_node.getProperty("genome");
            if (PROTEOME && skip_array[genome_nr-1]) {
                continue;
            } else {
                String annotation_id  = (String) bgc_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }
            String type = (String) bgc_node.getProperty("type");
            int length = (int) bgc_node.getProperty("length");
            if (length > longest_cluster) {
                longest_cluster = length;
            }
            counter_array[0] += 1; // total for all genomes
            counter_array[genome_nr] += 1;
            String[] type_array = type.split(","); // type can consists of multiple types. example: NRPS,NRPS-like
            for (String cluster_type : type_array) {
                try_incr_array_hashmap(counts_per_type, cluster_type,1, (genome_nr-1), total_genomes);
            }
            if (type_array.length > 1) { // also add the cluster combination
                try_incr_array_hashmap(counts_per_type, type.replace(",", " "), 1, (genome_nr-1), total_genomes);
            }
        }

        //create the header for 2nd part of overview
        StringBuilder info_per_cluster= new StringBuilder("type,number of genes,address,bgc node (identifier),");
        StringBuilder header_id = new StringBuilder();
        StringBuilder header_node = new StringBuilder();
        for (int i=1; i <= longest_cluster; i++) {
            header_id.append("position ").append(i).append(". gene id,");
            header_node.append("position ").append(i).append(" gene node,");
        }
        info_per_cluster.append(header_id).append(header_node).append("\n");

        bgc_nodes = GRAPH_DB.findNodes(BGC_LABEL); // go over nodes again, now we know the length of the longest cluster
        while (bgc_nodes.hasNext()) {
            Node bgc_node = bgc_nodes.next();
            int[] address = (int[]) bgc_node.getProperty("address");
            String type = (String) bgc_node.getProperty("type");
            int length = (int) bgc_node.getProperty("length");
            Iterable<Relationship> rels = bgc_node.getRelationships();
            info_per_cluster.append(type.replace(","," ")).append(",").append(length).append(",")
                    .append(address[0]).append(" ").append(address[1]).append(" ").append(address[2]).append(" ")
                    .append(address[3]).append(",").append(bgc_node.getId()).append(",");
            StringBuilder gene_node_per_cluster = new StringBuilder();
            StringBuilder gene_id_per_cluster = new StringBuilder();
            for (Relationship rel : rels) {
                Node gene_node = rel.getStartNode();
                String id = (String) gene_node.getProperty("id");
                gene_node_per_cluster.append(gene_node.getId()).append(",");
                gene_id_per_cluster.append(id).append(",");
            }
            int extra_commas = longest_cluster-length;
            StringBuilder commas = new StringBuilder();
            for (int i=0; i < extra_commas; i++) {
                commas.append(",");
            }
            info_per_cluster.append(gene_id_per_cluster).append(commas).append(gene_node_per_cluster).append("\n");
        }

        StringBuilder occ_per_genome = new StringBuilder("Cluster type (product),");
        for (int i=1; i <= total_genomes; i++) {
            occ_per_genome.append("occurrence in genome ").append(i).append(",");
        }
        occ_per_genome.append("\ntotal clusters").append(",");
        for (int i=1; i < counter_array.length; i++) { // skip first position
            occ_per_genome.append(counter_array[i]).append(",");
        }
        occ_per_genome.append("\n");
        for (String type : counts_per_type.keySet()){
            int[] counts_per_genome = counts_per_type.get(type);
            String occ_per_genome_str = Arrays.toString(counts_per_genome).replace("[","")
                    .replace("]","").replace(", ",",");occ_per_genome.append(type)
                    .append(",").append(occ_per_genome_str).append("\n");
        }

        if (counter_array[0] > 0) {
            write_string_to_file_in_DB("Total BGCs: " + counter_array[0] + "\n\n" + occ_per_genome + "\n" +
                    info_per_cluster, "function/bgc_overview.csv");
        }
    }

    /**
     * Check for Phobius properties on 'mrna' node
     * @param mrna_node a 'mrna' node
     * @param function_counter_map key is functional annotation (id or description). value is array with counts per genome
     * @param genome_nr a genome number
     * @param output_builder write directly to builder for 'functions_per_group_and_mrna'
     */
    public static void incr_function_counter_map_phobius(Node mrna_node, HashMap<String, int[]> function_counter_map,
                                                         int genome_nr, StringBuilder output_builder) {

        if (!mrna_node.hasLabel(PHOBIUS_LABEL) || !mrna_node.hasProperty("phobius_signal_peptide")
                || !mrna_node.hasProperty("phobius_signal_peptide")) {
            output_builder.append("-;");
            return;
        }
        String output = "";
        String signal_peptide = (String) mrna_node.getProperty("phobius_signal_peptide"); // can be "yes" or "no"
        int transmembrane_domains = (int) mrna_node.getProperty("phobius_transmembrane");
        if (signal_peptide.equals("yes")) {
            output += "Phobius signal peptide,";
            try_incr_array_hashmap(function_counter_map, "Phobius signal peptide,",1, (genome_nr-1), total_genomes);
        }

        if (transmembrane_domains > 0) {
            output += transmembrane_domains + " Phobius transmembrane domains";
            try_incr_array_hashmap(function_counter_map, "Phobius transmembrane domains,",1, (genome_nr-1), total_genomes);
        }
        if (output.endsWith(",")) {
            output = output.replaceFirst(".$",""); // removes last character
        }
        output_builder.append(output).append(";");
    }

    /**
     * Check for signalP properties on 'mrna' node
     * @param mrna_node a 'mrna' node
     * @param function_counter_map key is functional annotation (id or description). value is array with counts per genome
     * @param genome_nr a genome number
     * @param output_builder write directly to builder for 'functions_per_group_and_mrna'
     */
    public static void incr_function_counter_map_signalp(Node mrna_node, HashMap<String, int[]> function_counter_map,
                                                         int genome_nr, StringBuilder output_builder) {

        if (!mrna_node.hasLabel(SIGNALP_LABEL) || !mrna_node.hasProperty("signalp_signal_peptide")) {
            output_builder.append("-;");
            return;
        }

        String signal_peptide = (String) mrna_node.getProperty("signalp_signal_peptide"); // can be "yes", "SP(Sec/SPI)", "LIPO(Sec/SPII)" or "TAT(Tat/SPI)"
        if (signal_peptide.equals("yes")) {
            output_builder.append("SignalP signal peptide,");
            try_incr_array_hashmap(function_counter_map, "SignalP signal peptide,",1, (genome_nr-1), total_genomes);
        } else {
            output_builder.append("SignalP ").append(signal_peptide).append(" signal peptide,");
            try_incr_array_hashmap(function_counter_map, "SignalP " + signal_peptide + " signal peptide,",1,
                    (genome_nr-1), total_genomes);
        }
        output_builder.append(";");
    }

    /**
     * Check for COG properties on 'mrna' node
     * @param mrna_node a 'mrna' node
     * @param function_counter_map key is functional annotation (id or description). value is array with counts per genome
     * @param genome_nr a genome number
     * @param output_builder write directly to builder for 'functions_per_group_and_mrna'
     */
    public static void incr_function_counter_map_cog(Node mrna_node, HashMap<String, int[]> function_counter_map,
                                                     int genome_nr, StringBuilder output_builder) {

        if (!mrna_node.hasLabel(COG_LABEL)) {
            output_builder.append("-;");
            return;
        }

        String cogId = (String) mrna_node.getProperty("COG_id");
        if (cogId.contains("COG")) { // some identifiers are jibberish ortholog identifiers that only match very specific databases. example: 2EN8X & 2CMGX
            try_incr_array_hashmap(function_counter_map, cogId + ",", 1, (genome_nr - 1), total_genomes);
        }

        String cogCategories = (String) mrna_node.getProperty("COG_category");
        String[] cog_cat_array = cogCategories.split(""); // the category can be multiple letters, e.g. AB, DE
        for (String cog_category1 : cog_cat_array) {
            try_incr_array_hashmap(function_counter_map, "COG category " + cogCategories + ",",1, (genome_nr-1), total_genomes);
            output_builder.append("COG category ").append(cog_category1).append(",");
        }
        output_builder.append(";");




    }

    /**
     * function is currently disabled, do not remove
     * @param gene_node a 'gene' node
     * @param function_counter_map key is functional annotation (id or description). value is array with counts per genome
     * @param genome_nr a genome number
     * @param output_builder write directly to builder for 'functions_per_group_and_mrna'
     */
    public static void incr_function_counter_map_bgc(Node gene_node, HashMap<String, int[] > function_counter_map,
                                                     int genome_nr, StringBuilder output_builder) {
        if (PROTEOME) { // no geneclusters present in panproteome
            return;
        }
        Iterable<Relationship> bgc_rels = gene_node.getRelationships(RelTypes.part_of); // relation to bgc node
        boolean bgc_present = false;
        for (Relationship rel : bgc_rels) {
            bgc_present = true;
            int position = (int) rel.getProperty("position");
            Node bgc_node = rel.getEndNode();
            String type = (String) bgc_node.getProperty("type");
            type = type.replace(",", " ");
            int length = (int) bgc_node.getProperty("length");
            output_builder.append("gene ").append(position).append("/").append(length).append(" of ").append(type)
                    .append(" bgc ").append(bgc_node.getId()).append(",");
            try_incr_array_hashmap(function_counter_map, type + " bgc,",1, (genome_nr-1), total_genomes);
        }
        if (!bgc_present) {
            output_builder.append("-");
        }
    }

    /**
     * Function node can be GO, InterPro, Pfam, Tigrfam
     * @param mrna_node a 'mrna' node
     * @param function_counter_map key is functional annotation (id or description). value is array with counts per genome
     * @param genome_nr a genome number
     * @param function_reltype a relationship type
     * @param output_builder write directly to builder for 'functions_per_group_and_mrna'
     */
    public static void incr_function_counter_map(Node mrna_node, HashMap<String, int[]> function_counter_map, int genome_nr,
                                                 RelTypes function_reltype, StringBuilder output_builder) {

        Iterable<Relationship> function_rels = mrna_node.getRelationships(function_reltype);
        StringBuilder function_builder = new StringBuilder();
        for (Relationship function_rel : function_rels) {
            Node function_node = function_rel.getEndNode();
            String function_id = (String) function_node.getProperty("id"); // GO:0001, PFAM12, etc
            String function_name = (String) function_node.getProperty("name");
            function_name = function_name.replace(",",";");
            function_builder.append(function_id).append(",");
            try_incr_array_hashmap(function_counter_map, function_id + "," + function_name,1, (genome_nr-1), total_genomes);
        }

        if (function_builder.length() == 0) {
            output_builder.append("-");
        }
        output_builder.append(function_builder.toString().replaceFirst(".$","")).append(";");
    }

    /**
     * @param frequency_array frequency array from 'homology_group' node
     * @return 'core', 'accessory' or 'unique'
     */
    public static String determine_if_core_accessory_unique(int[] frequency_array) {
        String hm_class = "skipped";
        int present_counter = 0;
        for (int i = 0; i < frequency_array.length; i++) {
            if (skip_array[i]) {
                continue;
            }
            if (frequency_array[i] != 0) {
                present_counter++;
            }
        }

        if (present_counter >= adj_total_genomes) {
            hm_class = "core";
        } else if (present_counter == 1) {
            hm_class = "unique";
        } else if (present_counter != 0) {
            hm_class = "accessory";
        } // else, present_counter is zero. All genomes in homology groups were skipped in current analysis
        return hm_class;
    }

    /**
     *
     */
    public static void read_core_accessory_unique_groups() {
        hmgroup_class_map = new HashMap<>();
        // with gene_classification2 the files are placed in group_identifiers directory
        //boolean exists1 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/group_identifiers/core_groups.csv");
        //boolean exists2 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/group_identifiers/accessory_groups.csv");
        //boolean exists3 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/group_identifiers/unique_groups.csv");
        boolean exists1 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/core_groups.csv");
        boolean exists2 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/accessory_groups.csv");
        boolean exists3 = check_if_file_exists(WORKING_DIRECTORY + "gene_classification/unique_groups.csv");
        if (!exists1 || !exists2 || !exists3) {
            Pantools.logger.error("This analysis requires the gene content to be classified with gene_classification2.");
            System.exit(1);
        }

        try {
            SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/core_groups.csv";
            for (Node hm_node : findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1)) {
                hmgroup_class_map.put(hm_node, "core");
            }
            SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/accessory_groups.csv";
            for (Node hm_node : findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1)) {
                hmgroup_class_map.put(hm_node, "accessory");
            }
            SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/unique_groups.csv";
            for (Node hm_node : findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1)) {
                hmgroup_class_map.put(hm_node, "unique");
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Could not read homology groups file: {}", SELECTED_HMGROUPS);
            throw new RuntimeException(ioe);
        }
    }

    /**
     *
     * @param hm_nodes_list list with 'homology_group' node identifiers
     * @param output_path Output directory
     */
    public static void function_overview_per_group(ArrayList<Node> hm_nodes_list, String output_path) {
        if (Mode.contains("CLASSIFICATION")) {
            read_core_accessory_unique_groups();
        } else {
            hmgroup_class_map = new HashMap<>();
        }
        StringBuilder output_builder = new StringBuilder("#split this file on a semicolon\n");
        if (!PROTEOME) {
            output_builder.append("#homology group node id;genome;mRNA identifier;mRNA node id;gene name;gene identifier;gene node id;");
        } else {
            output_builder.append("#homology group node id;genome;protein identifier;protein node id;");
        }
        output_builder.append("GO terms;Pfam domains;Interpro domains;TIGRFAM annotations;COG annotation;" +
                "Phobius annotation;SignalP annotation");
        if (!PROTEOME) {
            output_builder.append(";Biosynthetic gene clusters\n");
        } else {
            output_builder.append("\n");
        }
        StringBuilder counts_per_genome = new StringBuilder("#split this file on a comma\nhomology group or function," +
                "description,");
        for (int i=1; i <= total_genomes; i++) {
            counts_per_genome.append("genome ").append(i).append(",");
        }
        counts_per_genome.append("\n");
        if (hm_nodes_list == null) {
            hm_nodes_list = new ArrayList<>();
            ResourceIterator<Node> hm_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            while (hm_nodes.hasNext()) {
                Node hm_node = hm_nodes.next();
                hm_nodes_list.add(hm_node);
            }
        }
        int total_hmgroups = hm_nodes_list.size();
        int hm_counter = 0, no_annotation = 0;
        HashMap<String, HashSet<Node>> annotated_hmgroup_count_map = new HashMap<>();
        for (Node hm_node : hm_nodes_list) {
            hm_counter ++;
            if (hm_counter % 100 == 0 || hm_counter == total_hmgroups || hm_counter < 100) {
                Pantools.logger.info("Retrieving function information: {}/{} homology groups.", hm_counter, total_hmgroups);
            }
            HashMap<String, int[]> function_counter_map = new HashMap<>();
            // key is functional annotation (id or description). value is array with counts per genome

            int[] temp_copy_number = (int[]) hm_node.getProperty("copy_number");
            int[] copy_number = remove_first_position_array(temp_copy_number);
            long hm_node_id = hm_node.getId();
            Iterable<Relationship> hm_rels = hm_node.getRelationships();
            int num_members = (int) hm_node.getProperty("num_members");
            boolean present = false;
            for (Relationship hm_rel : hm_rels) {
                Node mrna_node = hm_rel.getEndNode();
                int genome_nr = (int) mrna_node.getProperty("genome");
                if (skip_array[genome_nr-1]) {
                    continue;
                }
                Relationship mrna_rel = mrna_node.getSingleRelationship(RelTypes.codes_for, Direction.INCOMING);
                long mrna_node_id = mrna_node.getId();
                Node gene_node = GRAPH_DB.getNodeById(0);
                if (!PROTEOME) { // pangenome
                    String mrna_id = (String) mrna_node.getProperty("id");
                    gene_node = mrna_rel.getStartNode();
                    long gene_node_id = gene_node.getId();
                    String gene_id = (String) gene_node.getProperty("id");
                    String gene_name = retrieveNamePropertyAsString(mrna_node);
                    if (gene_name.equals("")) {
                        gene_name = "-";
                    }
                    output_builder.append(hm_node_id).append(";").append(genome_nr).append(";").append(mrna_id)
                            .append(";").append(mrna_node_id).append(";").append(gene_name).append(";").append(gene_id)
                            .append(";").append(gene_node_id).append(";");
                } else { // proteome only has an mrna node
                    String mrna_id = (String) mrna_node.getProperty("protein_ID");
                    output_builder.append(hm_node_id).append(";").append(genome_nr).append(";").append(mrna_id)
                            .append(";").append(mrna_node_id).append(";");
                }
                incr_function_counter_map(mrna_node, function_counter_map, genome_nr, RelTypes.has_go, output_builder);
                incr_function_counter_map(mrna_node, function_counter_map, genome_nr, RelTypes.has_pfam, output_builder);
                incr_function_counter_map(mrna_node, function_counter_map, genome_nr, RelTypes.has_interpro, output_builder);
                incr_function_counter_map(mrna_node, function_counter_map, genome_nr, RelTypes.has_tigrfam, output_builder);
                incr_function_counter_map_cog(mrna_node, function_counter_map, genome_nr, output_builder);
                incr_function_counter_map_phobius(mrna_node, function_counter_map, genome_nr, output_builder);
                incr_function_counter_map_signalp(mrna_node, function_counter_map, genome_nr, output_builder);
                incr_function_counter_map_bgc(gene_node, function_counter_map, genome_nr, output_builder);
                present = true;
                output_builder.append("\n");
            }

            if (!present) {
                continue;
            }

            String hmgroup_class;
            if (Mode.contains("CLASSIFICATION")) {
                hmgroup_class = hmgroup_class_map.get(hm_node);
                if (hmgroup_class == null) {
                    Pantools.logger.error("The 'homology_group' node {} was not part of the previous 'gene_classification' run.", hm_node);
                    Pantools.logger.error("Make sure you have the same genome/sequence selection.");
                    System.exit(1);
                }
            } else {
                // loop over copy_number array, can be 'core', 'accessory' or 'unique'
                hmgroup_class = determine_if_core_accessory_unique(copy_number);
                hmgroup_class_map.put(hm_node, hmgroup_class);
            }


            if (function_counter_map.isEmpty()) { // no function found for this homology group
                no_annotation ++;
                annotated_hmgroup_count_map.computeIfAbsent(hmgroup_class + "_missing", s -> new HashSet<>()).add(hm_node);
            } else {
                String copy_nr_str = Arrays.toString(copy_number).replace("[","")
                        .replace("]","").replace(" ","");
                annotated_hmgroup_count_map.computeIfAbsent(hmgroup_class + "_anno", s -> new HashSet<>()).add(hm_node);
                counts_per_genome.append("Homology group ").append(hm_node_id).append(",").append(num_members)
                        .append(" genes,").append(copy_nr_str).append("\n");

                for (String function : function_counter_map.keySet()) {
                    int[] value_array = function_counter_map.get(function);
                    copy_nr_str = Arrays.toString(value_array).replace("[","")
                            .replace("]","")
                            .replace(" ","");
                    counts_per_genome.append(function).append(",").append(copy_nr_str).append("\n");
                }
                counts_per_genome.append("\n");
            }
        }

        int annotated_groups = total_hmgroups - no_annotation;
        if (annotated_groups == 0) { // no functional annotations in the pangenome
            write_string_to_file_full_path("No functions found for this homology group selection\n", output_path + "functions_per_group_and_mrna.csv");
            write_string_to_file_full_path("No functions found for this homology group selection\n", output_path + "function_counts_per_group.csv");
            return;
        }
        int core_anno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "core_anno");
        int core_noanno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "core_missing");
        int accessory_anno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "accessory_anno");
        int accessory_noanno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "accessory_missing");
        int unique_anno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "unique_anno");
        int unique_noanno = return_size_of_hashset_in_hashmap(annotated_hmgroup_count_map, "unique_missing");

        String core_percentage = get_percentage_str(core_anno, core_anno + core_noanno,2);
        String accessory_percentage = get_percentage_str(accessory_anno, accessory_anno + accessory_noanno, 2);
        String unique_percentage = get_percentage_str(unique_anno, unique_anno + unique_noanno, 2);
        String total_percentage = get_percentage_str((total_hmgroups-no_annotation), total_hmgroups, 2);
        String start_str = "Homology groups with any assigned function: " + (total_hmgroups-no_annotation) + " out of "
                + total_hmgroups + " (" + total_percentage + "%)"
                + "\n Core     : " + core_anno + "/" + (core_anno + core_noanno) + " (" + core_percentage + "%)"
                + "\n Accessory: " + accessory_anno + "/" + (accessory_anno + accessory_noanno) + " (" + accessory_percentage + "%)"
                + "\n Unique   : " + unique_anno + "/" + (unique_anno + unique_noanno) + " (" + unique_percentage + "%)\n\n";

        add_missing_groups_to_stringbuilder(annotated_hmgroup_count_map, output_builder, "annotation");
        write_string_to_file_full_path(start_str + output_builder + "\n", output_path + "functions_per_group_and_mrna.csv");
        write_string_to_file_full_path(counts_per_genome.toString(),  output_path + "function_counts_per_group.csv");
    }

    /**
     * returns the size of hashset in a hashmap. 0 when empty
     * @param map hashmap
     * @param key hashmap key
     * @return size of hashmap
     */
    public static int return_size_of_hashset_in_hashmap(HashMap<String, HashSet<Node>> map, String key) {
        if (map.containsKey(key)) {
            HashSet<Node> hashset = map.get(key);
            return hashset.size();
        }
        return 0;
    }

    /**
     * return a new hashset when the key doesn't exist yet
     * @param map hashmap
     * @param key hashmap key
     * @return hashmap value or new hashset
     */
    public static HashSet<Node> retrieve_hashet_return_empty_when_null(HashMap<String, HashSet<Node>> map, String key) {
        HashSet<Node> hashset = new HashSet<>();
        if (map.containsKey(key)) {
            hashset = map.get(key);
        }
        return hashset;
    }

    /**
     * also include some of the statistics from missing annotations in the output here?
     */
    public static void create_COG_rscript() {
        StringBuilder rscript = new StringBuilder();
        rscript.append("#! /usr/bin/env RScript\n")
                .append("#install.packages(\"ggplot2\", \"").append("~/local/R_libs/").append("\", \"https://cran.us.r-project.org\")\n")
                .append("library(ggplot2)\n\n")
                .append("#input = read.csv(\"").append(WD_full_path).append("function/cog_per_class_hmgroups.csv\", sep=\";\",header = TRUE)\n")
                .append("input = read.csv(\"").append(WD_full_path).append("function/cog_per_class_percentage.csv\", sep=\";\",header = TRUE)\n\n")
                .append("plot1 = ggplot(input, aes(x=Category, y=Value, fill=Class)) + \n")
                .append("    ggtitle(\"Number of COGs found per class\") + \n")
                .append("    scale_fill_manual(values=c(\" core\" = \"#009900\", \"accessory\" = \"#0066CC\", \"unique\" = \"#990000\")) +\n")
                .append("    geom_bar(position=\"dodge\", stat=\"identity\", show.legend = TRUE) + \n")
                .append("    theme(axis.text.x = element_text(angle = 45, size = 10, color = \"black\", face = \"plain\", vjust = 1, hjust = 1),\n")
                .append("        plot.margin = margin(10, 10, 10, 100)) +\n")
                .append("    labs(y = \"Percentage abundance\", size = 15) + \n")
                .append("    #labs(y = \"Number of homology groups\", size = 15) + \n")
                .append("    theme(axis.title.x = element_blank())\n\nratio <- 2\n");

        rscript.append("ggsave(\"").append(WD_full_path).append("function/cog_per_class.png\", plot= plot1, height = 6, width = 10 * ratio)\n")
                .append("print(\"COG plot written to: ").append(WD_full_path).append("function/cog_per_class.png\")");
        write_SB_to_file_in_DB(rscript, "function/cog_per_class.R");
    }

    /**
     *
     * @param cog_counter_map
     * @param cog_cat_counter_map
     * @return
     */
    public static HashMap<String, String> create_COG_rscript_input(HashMap<String, Integer> cog_counter_map,
                                                                   HashMap<String, int[]> cog_cat_counter_map) {

        DecimalFormat df = new DecimalFormat("0.00");
        String[] class_array = {"core", "accessory", "unique"};
        StringBuilder output_builder = new StringBuilder("Category;Class;Value\n");
        StringBuilder output_builder2 = new StringBuilder("Category;Class;Value\n");
        HashMap<String, String> cog_log2_map = new HashMap<>();
        for (String classStr : class_array) {
            int total_groups_class = 0;
            for (String category : cog_category_array) {
                String[] category_array = category.split(": ");
                int value = 0;
                if (cog_counter_map.containsKey(category_array[0]+ "#" + classStr)) {
                    value = cog_counter_map.get(category_array[0] + "#" + classStr);
                }
                total_groups_class += value;
                if (classStr.equals("core")) {
                    output_builder.append(category).append("; core;").append(value).append("\n");
                } else {
                    output_builder.append(category).append(";").append(classStr).append(";").append(value).append("\n");
                }
            }
            for (String category : cog_category_array) {
                String[] category_array = category.split(": ");
                int value = 0;
                if (cog_counter_map.containsKey(category_array[0] + "#" + classStr)) {
                    value = cog_counter_map.get(category_array[0] + "#" + classStr);
                }

                double percentage = percentage(value, total_groups_class);
                int log2_pc = 0;
                if (percentage > 0) {
                    log2_pc = log2((int) percentage);
                }
                if (value == 0) {
                    cog_log2_map.put(category + "#" + classStr, "0%, " + log2_pc);
                } else {
                    cog_log2_map.put(category + "#" + classStr, df.format(percentage) + "%, " + log2_pc);
                }
                if (classStr.equals("core")) {
                    output_builder2.append(category).append("; core;").append(percentage).append("\n");
                } else {
                    output_builder2.append(category).append(";").append(classStr).append(";").append(percentage).append("\n");
                }
            }
        }
        write_SB_to_file_in_DB(output_builder, "function/cog_per_class_hmgroups.csv");
        write_SB_to_file_in_DB(output_builder2, "function/cog_per_class_percentage.csv");
        return cog_log2_map;
    }

    /**
     *
     * @param x
     * @return
     */
    public static int log2(int x) {
        return (int) (Math.log(x) / Math.log(2));
    }

    /**
     *
     * @param genes_per_genome
     * @param go_hits_per_genome
     * @param go_hits_in_gene_set
     * @return
     */
    public static HashMap<String, HashSet<Node>> prepare_go_set_background(HashMap<Integer, Node[]> genes_per_genome,
                                                                           HashMap<Node, int[]> go_hits_per_genome,
                                                                           HashMap<Node, int[]> go_hits_in_gene_set) {

        HashMap<String, HashSet<Node>> mrna_with_go_per_genome = new HashMap<>();
        String prot_or_mrna = " mRNA's";
        if (PROTEOME) {
            prot_or_mrna = " proteins";
        }
        ArrayList<Integer> present_genomes = new ArrayList<>();
        for (Integer genome_nr : genes_per_genome.keySet()) {
            present_genomes.add(genome_nr);
        }
        Collections.sort(present_genomes);
        StringBuilder background_logbuilder = new StringBuilder("#mRNA node id, part of subset, GO terms\n");
        StringBuilder background_logbuilder2 = new StringBuilder("#part of subset, mRNA node id,GO node ids\n"
                + "match (n) where id(n) in [PUT MRNA & GO NODE IDS HERE] return n\n");
        for (int genome_nr : present_genomes) {
            Node[] mrna_array = genes_per_genome.get(genome_nr); // genes from homology groups
            ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", genome_nr);
            int node_counter = 0;
            background_logbuilder.append("#Genome ").append(genome_nr).append("\n");
            background_logbuilder2.append("#Genome ").append(genome_nr).append("\n");
            while (mrna_nodes.hasNext()) {
                Node mrna_node = mrna_nodes.next();
                long mrna_id = mrna_node.getId();
                Iterable<Relationship> go_rels = mrna_node.getRelationships(RelTypes.has_go);
                node_counter ++;
                boolean has_go = false, in_subset = false;
                HashSet<Node> mrna_go_set = new HashSet<>();
                HashSet<Node> mrna_subet_go_set = new HashSet<>();
                for (Relationship go_rel : go_rels) {
                    has_go = true;
                    if (node_counter % 100 == 0) {
                        System.out.print("\rRetrieving 'GO' nodes, preparing background. Genome " + genome_nr + ", " + node_counter + prot_or_mrna);
                    }
                    Node go_node = go_rel.getEndNode();
                    //String go_id = (String) go_node.getProperty("id");
                    String all_go = go_node.toString();
                    HashSet<Node> go_set = new HashSet<>();
                    go_set.add(go_node);
                    boolean stop = false;
                    while (stop == false) {
                        String new_all_go = get_go_hierarchy_nodes(all_go, go_set);
                        if (new_all_go.length() < 2) {
                            stop = true;
                        }
                        all_go = new_all_go;
                    }
                    for (Node go_node1 : go_set) {
                        mrna_go_set.add(go_node1);
                    }

                    if (ArrayUtils.contains(mrna_array, mrna_node)) {
                        in_subset = true;
                        for (Node go_node1 : go_set) {
                            mrna_subet_go_set.add(go_node1);
                        }
                    }
                }

                //Pantools.logger.info(mrna_go_set);
                for (Node go_node1 : mrna_go_set) {
                    int[] freq_array = go_hits_per_genome.get(go_node1);
                    if (freq_array == null) {
                        freq_array = new int[total_genomes];
                    }
                    freq_array[genome_nr-1] += 1;
                    go_hits_per_genome.put(go_node1, freq_array);
                }

                if (in_subset) {
                    for (Node go_node1 : mrna_subet_go_set) {
                        int[] freq_array = go_hits_in_gene_set.get(go_node1);
                        if (freq_array == null) {
                            freq_array = new int[total_genomes];
                        }
                        freq_array[genome_nr-1] += 1;
                        go_hits_in_gene_set.put(go_node1, freq_array);
                    }
                }
                //System.out.println();
                if (in_subset) {
                    mrna_with_go_per_genome.computeIfAbsent(genome_nr + "_subset", s -> new HashSet<>()).add(mrna_node);
                    if (mrna_go_set.isEmpty()) {
                        background_logbuilder.append(mrna_id).append(",yes\n");
                        background_logbuilder2.append("yes,").append(mrna_id).append("\n");
                    } else {
                        background_logbuilder.append(mrna_id).append(",yes,,");
                        background_logbuilder2.append("yes,").append(mrna_id).append(",");
                        for (Node go_node : mrna_go_set) {
                            String go_id = (String) go_node.getProperty("id");
                            long node_id = go_node.getId();
                            background_logbuilder.append(go_id).append(",");
                            background_logbuilder2.append(node_id).append(",");
                        }
                        background_logbuilder.append("\n");
                        background_logbuilder2.append("\n");
                    }
                } else {
                    if (mrna_go_set.isEmpty()) {
                        background_logbuilder.append(mrna_id).append(",no\n");
                        background_logbuilder2.append("no,").append(mrna_id).append("\n");
                    } else {
                        background_logbuilder.append(mrna_id).append(",no,,");
                        background_logbuilder2.append("no,").append(mrna_id).append(",");
                        for (Node go_node : mrna_go_set) {
                            String go_id = (String) go_node.getProperty("id");
                            long node_id = go_node.getId();
                            background_logbuilder.append(go_id).append(",");
                            background_logbuilder2.append(node_id).append(",");
                        }
                        background_logbuilder.append("\n");
                        background_logbuilder2.append("\n");
                    }
                }

                if (has_go) {
                    mrna_with_go_per_genome.computeIfAbsent(genome_nr + "_genome", s -> new HashSet<>()).add(mrna_node);
                }
            }
        }
        //write_SB_to_file_in_DB(background_logbuilder, "function/go_enrichment/background_go_ids.log");
        //write_SB_to_file_in_DB(background_logbuilder2, "function/go_enrichment/background_node_ids.log");
        return mrna_with_go_per_genome;
    }

    /**
     * Do not stop but only give a warning
     * @return
     */
    public boolean check_if_dot_is_installed() {
        boolean installed = true;
        StringBuilder exe_output = new StringBuilder();
        try {
            Process p = Runtime.getRuntime().exec("dot -h");
            p.waitFor();
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String s = null;
            while ((s = stdError.readLine()) != null) {
                exe_output.append(s);
            }
        } catch (Exception e) {
            installed  = false;
        }
        if (exe_output.toString().length() > 50) { // dot dit not generate the output manual
            installed  = false;
        }

        if (!installed) {
            Pantools.logger.info("IMPORTANT! dot is not correctly installed. Unable to create plots with colored GO hierarchy.");
        }
        return installed;
    }

    /**
     * Requires
     * -dp
     * -hm OR --node
     *
     * Optional
     * --skip
     * --reference
     * --value  The false discovery rate (percentage), default is 5%.
     */
    public void go_enrichment() {
        Pantools.logger.info("Performing GO enrichment analysis using a hypergeometric distribution.");
        check_database(); // starts up the graph database if needed
        HashMap<Node, int[]> go_hits_per_genome = new HashMap<>(); //
        HashMap<Node, int[]> go_hits_in_gene_set = new HashMap<>(); // node is GOterm node, int[] is gene counter per genome
        create_directory_in_DB("/function/go_enrichment/");
        boolean dot_installed = check_if_dot_is_installed();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }
        if (NODE_VALUE == null) {
            System.out.println("\rNo --fdr was given. False discovery rate is set to 5%");
        } else {
            int fdr = Integer.parseInt(NODE_VALUE);
            if (fdr < 1 || fdr > 99) {
                Pantools.logger.error("The FDR --value must be between 1 and 100.");
                System.exit(1);
            }
            System.out.println("\rFalse discovery rate set to " + NODE_VALUE + "% (via --fdr)");
        }

        try (Transaction tx = GRAPH_DB.beginTx()) {
            HashMap<Integer, Node[]> genes_per_genome = get_mrna_nodes_by_hm_or_ids();
            HashMap<String, StringBuilder> go_tree_per_genome = create_go_SB_hashmap(genes_per_genome); // for each genome are 3 keys, bio_proc, mol_func cellular component
            create_all_functions_overview(genes_per_genome, "function/go_enrichment/");
            HashMap<String, HashSet<Node>> mrna_with_go_per_genome = prepare_go_set_background(genes_per_genome, go_hits_per_genome, go_hits_in_gene_set);
            if (go_hits_in_gene_set.isEmpty()) {
                Pantools.logger.error("No GO terms were connected to the provided homology groups.");
                System.exit(0);
            }
            build_go_tree_per_genome(genes_per_genome, go_tree_per_genome);
            perform_hyper_geometric_test(genes_per_genome, go_hits_per_genome, go_hits_in_gene_set, go_tree_per_genome, mrna_with_go_per_genome, dot_installed);
            tx.success();
        }
        String mrna_or_prot = "mrna";
        if (PROTEOME) {
            mrna_or_prot = "protein";
        }
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}function/go_enrichemnt/results_per_genome/", WORKING_DIRECTORY);
        Pantools.logger.info(" {}function/go_enrichemnt/go_enrichment.csv", WORKING_DIRECTORY);
        Pantools.logger.info(" {}function/go_enrichemnt/go_enrichment_overview_per_go.txt", WORKING_DIRECTORY);
        Pantools.logger.info(" {}function/go_enrichemnt/function_overview_per_{}.txt", WORKING_DIRECTORY, mrna_or_prot);
        Pantools.logger.info(" {}function/go_enrichemnt/function_overview_per_genome.txt", WORKING_DIRECTORY);
        //Pantools.logger.info("Log files written to:");
        //Pantools.logger.info(" {}function/go_enrichment/go_enrichment.log", WORKING_DIRECTORY);
        //Pantools.logger.info(" {}function/go_enrichment/background_go_ids.log", WORKING_DIRECTORY);
        //Pantools.logger.info(" {}function/go_enrichment/background_node_ids.log", WORKING_DIRECTORY);
    }

    /**
     * Calculate the p-Value of the Hypergeometric Distribution
     * from: http://en.wikipedia.org/wiki/Hypergeometric_distribution
     *
     * P(X=k) = {m over k} * { (N-m) over (n-k) } / {N over n}
     *
     * @param N size of the population (Universe of genes)
     * @param n size of the sample (signature geneset)
     * @param m successes in population (enrichment geneset)
     * @param k successes in sample (intersection of both genesets)
     *
     * @return the p-Value of the Hypergeometric Distribution for P(X=k)

     Suppose we randomly select 5 cards without replacement from an ordinary deck of playing cards. What is the probability of getting exactly 2 red cards (i.e., hearts or diamonds)?
         N = 52; since there are 52 cards in a deck.
         k = 26; since there are 26 red cards in a deck
         n = 5; since we randomly select 5 cards from the deck
         x = 2; since 2 of the cards we select are red.

      double p_value = hyperGeomPvalue(52, 26, 5, 2);

         Validated with https://keisan.casio.com/exec/system/1180573201
     */
    public static double hyperGeomPvalue(final int N, final int n, final int m, final int k)
            throws ArithmeticException {
        //calculating in logarithmic scale as we are dealing with large numbers.
        double log_p = binomialLog(m, k) + binomialLog(N - m, n - k) - binomialLog(N, n);
        return Math.exp(log_p);
    }

    public static double binomialLog(final int n, final int k) throws ArithmeticException {
        return Gamma.logGamma(n + 1.0) - Gamma.logGamma(k + 1.0) - Gamma.logGamma(n - k + 1.0);
    }

    /**
     *
     * @param genes_per_genome
     * @param go_hits_per_genome
     * @param go_hits_in_gene_set
     * @param go_tree_per_genome
     * @param mrna_with_go_per_genome
     * @param dot_installed
     */
    public static void perform_hyper_geometric_test(HashMap<Integer, Node[]> genes_per_genome, HashMap<Node, int[]> go_hits_per_genome, HashMap<Node, int[]> go_hits_in_gene_set,
                                                    HashMap<String, StringBuilder> go_tree_per_genome, HashMap<String, HashSet<Node>> mrna_with_go_per_genome, boolean dot_installed) {

        DecimalFormat formatter = new DecimalFormat("0.0");
        TreeSet<Integer> present_genomes = new TreeSet<>(genes_per_genome.keySet());
        HashMap<Integer, StringBuilder> output_per_genome = new HashMap<>();
        HashMap<Integer, TreeSet<Double>> genome_pvals = new HashMap<>();
        HashMap<String, ArrayList<String>> pval_goterm = new HashMap<>();
        HashMap<String, String> input_values = new HashMap<>();
        int[] mrna_count = new int[total_genomes];
        int[] subset_mrna_count = new int[total_genomes];
        for (int i : present_genomes) {
            HashSet<Node> mrna_set = mrna_with_go_per_genome.get(i + "_genome");
            int total_mrnas = mrna_set.size();
            mrna_count[i-1] = total_mrnas;
            HashSet<Node> mrna_subset = mrna_with_go_per_genome.get(i + "_subset");
            if (mrna_subset == null) {
                continue;
            }
            subset_mrna_count[i-1] = mrna_subset.size();
        }

        HashSet<String> all_go = new HashSet<>();
        for (Node go_node : go_hits_in_gene_set.keySet()) {
            long node_id = go_node.getId();
            String go_id = (String) go_node.getProperty("id");
            all_go.add(go_id);
            String go_name = (String) go_node.getProperty("name");
            String go_category = (String) go_node.getProperty("category");
            int[] value = go_hits_in_gene_set.get(go_node);
            int[] value2 = go_hits_per_genome.get(go_node);
            input_values.put(go_id, "," + go_name + "," + go_category + "," + node_id + "\n");
            for (int genomeNr: present_genomes) {
                int pos = genomeNr -1;
                if (mrna_count[pos] == 0) {
                    continue;
                }
                double p_value = hyperGeomPvalue(mrna_count[pos], subset_mrna_count[pos], value2[pos], value[pos]);
                double percentage_genes = (double) subset_mrna_count[pos] / (double) mrna_count[pos];
                String color = determine_color(p_value);
                String format = String.format("%10.9f", p_value);
                input_values.put(genomeNr + "#" + go_id + "p_val", format);
                if (value[pos] == 0) {
                    genome_pvals.computeIfAbsent(genomeNr, i -> new TreeSet<>()).add(1.0);
                    pval_goterm.computeIfAbsent(genomeNr + "#" + 1.0, s -> new ArrayList<>()).add(go_id);
                } else {
                    double percentage_terms = (double) value[pos] / (double)value2[pos];
                    double expected = (double) subset_mrna_count[pos]* ((double)value2[pos]/(double)mrna_count[pos]);
                    String hyper_parameters = mrna_count[pos] + "," + subset_mrna_count[pos] + "," + value2[pos] + "," + value[pos] +"," + formatter.format(expected);
                    if (percentage_genes > percentage_terms) {
                        input_values.put(genomeNr + "#" + go_id,  hyper_parameters + ",under");
                    } else if (percentage_genes == percentage_terms) {
                        input_values.put(genomeNr + "#" + go_id, hyper_parameters + ",-");
                    } else {
                        input_values.put(genomeNr + "#" + go_id, hyper_parameters + ",over");
                    }
                    genome_pvals.computeIfAbsent(genomeNr, i -> new TreeSet<>()).add(p_value);
                    pval_goterm.computeIfAbsent(genomeNr + "#" + p_value, s -> new ArrayList<>()).add(go_id);
                    if (color.length() > 1) {
                        add_color_to_hierarchy(go_category, go_name, genomeNr, go_tree_per_genome, color);
                    }
                    output_per_genome.computeIfAbsent(genomeNr, i -> new StringBuilder())
                            .append(go_id).append(" ").append(p_value).append("\n");
                }
            }
        }

        HashMap<String, String> goterm_significance = new HashMap<>();
        multiple_testing_corr(genome_pvals, pval_goterm, input_values, goterm_significance);
        create_go_enrichment_csv_overview(all_go, present_genomes, goterm_significance, input_values);
        create_go_enrichment_overview_per_go(all_go, present_genomes, goterm_significance, input_values);
        for (Map.Entry<Integer, StringBuilder> entry : output_per_genome.entrySet()) {
            int key = entry.getKey();
            StringBuilder builder = entry.getValue();
            write_SB_to_file_in_DB(builder, "/function/go_enrichment/results_per_genome/" + key + "/revigo.txt");
        }
        if (dot_installed) {
            create_colored_go_hierarchy(output_per_genome, go_tree_per_genome);
        }
    }

    /**
     *
     * @param all_go
     * @param present_genomes
     * @param goterm_significance
     * @param input_values
     */
    public static void create_go_enrichment_csv_overview(HashSet<String> all_go, TreeSet<Integer> present_genomes, HashMap<String, String> goterm_significance,
                                                         HashMap<String, String> input_values) {

        String header = "#GOterm,";
        for (int genome_nr : present_genomes) {
            header += "p-value genome" + genome_nr + ",pass Benjamini Hochberg genome " + genome_nr + ",pass Bonferroni genome " + genome_nr + ",";
        }
        StringBuilder output_builder = new StringBuilder(header + "\n");
        for (String go_id : all_go) {
            output_builder.append(go_id).append(",");
            for (int genome_nr : present_genomes) {
                String p_val = input_values.get(genome_nr + "#" + go_id + "p_val");
                if (p_val.equals("1.000000000")) {
                    p_val = "No test";
                }
                if (goterm_significance.containsKey(genome_nr + "#" + go_id)) {
                    String test = goterm_significance.get(genome_nr + "#" + go_id);
                    String[] split_array = test.split(",");
                    output_builder.append(p_val).append(",").append(split_array[2]).append(",").append(split_array[3]).append(",");
                }else {
                    output_builder.append(p_val).append(",,,");
                }
            }
            output_builder.append("\n");
        }
        write_SB_to_file_in_DB(output_builder, "/function/go_enrichment/go_enrichment.csv");
    }

    /**
     *
     * @param genome_pvals
     * @param pval_goterm
     * @param input_values
     * @param goterm_significance
     */
    public static void multiple_testing_corr(HashMap<Integer, TreeSet<Double>> genome_pvals, HashMap<String, ArrayList<String>> pval_goterm,
                                             HashMap<String, String> input_values, HashMap<String, String> goterm_significance) {
        DecimalFormat formatter2 = new DecimalFormat("0.0000000000"); // 10 zeros
        //DecimalFormat formatter2 = new DecimalFormat("0.00000");
        double q = 0.05; // false discovery rate default is 5%
        if (NODE_VALUE != null) {
            double fdr = Double.parseDouble(NODE_VALUE);
            q = (fdr/100);
        }

        for (Map.Entry<Integer, TreeSet<Double>> entry : genome_pvals.entrySet()) {
            int genome_nr = entry.getKey();
            TreeSet<Double> p_value_set = entry.getValue();
            int total_tests = 0;
            //Pantools.logger.info(p_value_set);
            for (double pval : p_value_set) {
                if (pval == 1.0) {
                    continue;
                }
                ArrayList<String> value2 = pval_goterm.get(genome_nr + "#" + pval);
                total_tests += value2.size();
            }
            if (p_value_set.isEmpty()) {
                continue;
            }
            double benjamini_rank_p = (1.0/total_tests) * q;
            double critical_p_bonferroni = q/total_tests;
            int last_correct_rank = find_last_correct_rank(p_value_set, benjamini_rank_p);
            StringBuilder output_builder = new StringBuilder();
            double rank = 1;
            for (double pval : p_value_set) {
                if (pval == 1.0) {
                    continue;
                }
                ArrayList<String> go_terms = pval_goterm.get(genome_nr + "#" + pval);
                boolean benjamini_pass = false, bonferroni_pass = false;
                double critical_p_benjamini = benjamini_rank_p*rank;
                if (rank <= last_correct_rank) {
                    benjamini_pass = true;
                }
                if (pval < critical_p_bonferroni) {
                    bonferroni_pass = true;
                }
                for (String go_term : go_terms) {
                    String input_stats = input_values.get(genome_nr + "#" + go_term);
                    output_builder.append((int) rank).append(",").append(go_term).append(",").append(formatter2.format(pval)).append(",")
                            .append(formatter2.format(critical_p_benjamini)).append(",").append(formatter2.format(critical_p_bonferroni)).append(",")
                            .append(benjamini_pass).append(",").append(bonferroni_pass).append(",").append(input_stats).append("\n");

                    goterm_significance.put(genome_nr + "#" + go_term, formatter2.format(critical_p_benjamini) + "," + formatter2.format(critical_p_bonferroni) + ","
                            + benjamini_pass + "," + bonferroni_pass);
                }
                rank ++;
            }
            String header = "# There were " + total_tests + " hypergeometric tests performed for this genome\n"
                    + "# Total number of ranks: " + rank
                    + "\n# False discovery rate is " + q + "\n\n"
                    + "# Bonferroni multiple testing correction. Each p-value needs to be below " + (q/total_tests) + ".\n"
                    + "# Benjamini-Hochberg (BH) multiple testing correction. The first rank needs to be (1/" + total_tests + ")*" + q + " = " + benjamini_rank_p +
                    ". For each rank the p-value is increased with this number. "
                    + "All GO terms above the term with the highest p-value that is still below the critical p-value are considered significant\n\n"
                    + "Rank, GO identifier, p value, Critical p value Benjamini-Hochberg, Critical p value Bonferroni, pass Benjamini-Hochberg, "
                    + "pass Bonferroni, Total genes (N), Subset genes (n), GOterm occurace (K), "
                    + "GOterm in subset (k), Expected occurrence, Over- or underrepresented\n";
            write_string_to_file_in_DB(header + output_builder, "/function/go_enrichment/results_per_genome/" + genome_nr + "/go_enrichment.csv");
        }
    }

    /**
     *
     * @param go_category
     * @param go_name
     * @param genome_nr a genome number
     * @param go_tree_per_genome
     * @param color
     */
    public static void add_color_to_hierarchy(String go_category, String go_name, int genome_nr, HashMap<String, StringBuilder> go_tree_per_genome, String color) {
        StringBuilder tree = go_tree_per_genome.get(genome_nr + "_" + go_category);
        String add = "  \"" + go_name + "\" [style=filled, fillcolor=" + color + "];";
        tree.append(add).append("\n");
        go_tree_per_genome.put(genome_nr + "_" + go_category, tree);
    }

    /**
     *
     * @param p_value
     * @return
     */
    public static String determine_color(double p_value) {
        String add = "";
        double firered1 = 0.0000000001; // E-10
        double darkorange = 0.00000001;
        double gold = 0.000001;
        double yellow = 0.0001;
        double lyellow = 0.01;
        if (p_value < firered1) {
            add = "firebrick2";
        } else if (p_value < darkorange) {
            add = "darkorange";
        } else if (p_value < gold) {
            add = "gold1";
        } else if (p_value < yellow) {
            add = "yellow1";
        } else if (p_value < lyellow) {
            add = "lightgoldenrodyellow";
        }
        return add;
    }

    /**
     *
     * @param genes_per_genome
     * @param go_tree_per_genome
     */
    public static void build_go_tree_per_genome(HashMap<Integer, Node[]> genes_per_genome, HashMap<String, StringBuilder> go_tree_per_genome) {
        for (Map.Entry<Integer, Node[]> entry : genes_per_genome.entrySet()) {
            int genome_nr = entry.getKey();
            System.out.print("\rConstructing graphviz input " + genome_nr + "   ");
            Node[] node_val = entry.getValue();
            for (Node mrna_node: node_val) {
                Iterable<Relationship> go_rels = mrna_node.getRelationships(RelTypes.has_go);
                for (Relationship go_rel: go_rels) {
                    Node go_node = go_rel.getEndNode();
                    String go_category = (String) go_node.getProperty("category");
                    StringBuilder go_tree = go_tree_per_genome.get(genome_nr + "_" + go_category);
                    String all_go = go_node.toString();
                    StringBuilder all_go_builder = new StringBuilder();
                    int counter = 0;
                    boolean stop = false;
                    while (stop == false) {
                        String new_all_go = get_go_hierarchy(all_go, counter, all_go_builder, go_tree);
                        if (new_all_go.length() < 2) {
                            stop = true;
                        }
                        counter ++;
                        all_go = new_all_go;
                    }
                    go_tree_per_genome.put(genome_nr + "_" + go_category, go_tree);
                }
            }
        }
        System.out.println();
    }

    /**
     *
     * @param all_go
     * @param present_genomes
     * @param goterm_significance
     * @param input_values
     */
    public static void create_go_enrichment_overview_per_go(HashSet<String> all_go, TreeSet<Integer> present_genomes,
                                                            HashMap<String, String> goterm_significance, HashMap<String, String> input_values) {

        StringBuilder go_overview = new StringBuilder("# On the first line for each GO term: the GO id, GO name, GO category and node id\n"
                + "# Remaining lines: Genome number, p value, critical p value Benjamini Hochberg, critical p value Bonferroni, pass Benjamini Hochberg, pass Bonferroni,"
                + " total genes (N), subset genes (n), GOterm occurrence (K), GOterm in subset (k), Expected occurrence, Over- or underrepresented\n\n");

        HashMap<String, StringBuilder> overview_per_genome = new HashMap();
        for (String go_id : all_go) {
            String header = input_values.get(go_id);
            go_overview.append(go_id).append(header.replace(","," "));
            for (int genome_nr : present_genomes) {
                String input_stats = input_values.get(genome_nr + "#" + go_id);
                String p_val = input_values.get(genome_nr + "#" + go_id + "p_val");
                if (!p_val.equals("1.000000000") && goterm_significance.containsKey(genome_nr + "#" + go_id)) {
                    String test = goterm_significance.get(genome_nr + "#" + go_id);
                    go_overview.append("Genome ").append(genome_nr).append(",").append(p_val).append(",").append(test).append(",").append(input_stats).append("\n");
                    final String testShort = test.contains("true,true") ? "TT" : test.contains("true,false") ? "TF" : "FF";
                    final String line = String.format("%s,%s,%s,%s\n", go_id, p_val, test, input_stats);
                    overview_per_genome.computeIfAbsent(genome_nr + testShort, s -> new StringBuilder()).append(line);
                }
            }
            go_overview.append("\n");
        }
        write_SB_to_file_in_DB(go_overview, "/function/go_enrichment/go_enrichment_overview_per_go.txt");
    }

    /**
     *
     * @param output_per_genome
     * @param go_tree_per_genome
     */
    public static void create_colored_go_hierarchy(HashMap<Integer, StringBuilder> output_per_genome, HashMap<String, StringBuilder> go_tree_per_genome) {
        for (int genome_nr : output_per_genome.keySet()) {
            System.out.print("\rGenerating output files: Genome " + genome_nr + "  ");
            String dir = WORKING_DIRECTORY + "/function/go_enrichment/results_per_genome/" + genome_nr + "/";
            StringBuilder output_builder = go_tree_per_genome.get(genome_nr + "_biological_process");
            add_legend_to_dot_tree(output_builder);
            write_SB_to_file_in_DB(output_builder, "/function/go_enrichment/results_per_genome/" + genome_nr + "/bio_process.txt");
            String[] dot_command = {"dot","-Tps", dir + "bio_process.txt", "-T", "pdf", "-O"};
            ExecCommand.ExecCommand(dot_command);
            copyFile(dir + "bio_process.txt.pdf", dir + "bio_process.pdf");
            delete_file_full_path(dir + "bio_process.txt.pdf");
            delete_file_full_path(dir + "bio_process.txt.ps");

            output_builder = go_tree_per_genome.get(genome_nr + "_molecular_function");
            add_legend_to_dot_tree(output_builder);
            write_SB_to_file_in_DB(output_builder, "/function/go_enrichment/results_per_genome/" + genome_nr + "/mol_function.txt");
            dot_command = new String[]{"dot", "-Tps", dir + "mol_function.txt","-T","pdf","-O"};
            ExecCommand.ExecCommand(dot_command);
            copyFile(dir + "mol_function.txt.pdf", dir + "mol_function.pdf");
            delete_file_full_path(dir + "mol_function.txt.pdf");
            delete_file_full_path(dir + "mol_function.txt.ps");

            output_builder = go_tree_per_genome.get(genome_nr + "_cellular_component");
            add_legend_to_dot_tree(output_builder);
            write_SB_to_file_in_DB(output_builder, "/function/go_enrichment/results_per_genome/" + genome_nr + "/cell_comp.txt");
            dot_command = new String[]{"dot","-Tps", dir + "cell_comp.txt","-T","pdf","-O"};
            ExecCommand.ExecCommand(dot_command);
            copyFile(dir + "cell_comp.txt.pdf", dir + "cell_comp.pdf");
            delete_file_full_path(dir + "cell_comp.txt.pdf");
            delete_file_full_path(dir + "cell_comp.txt.ps");
        }
    }

    /**
     *
     * @param output_builder
     */
    public static void add_legend_to_dot_tree(StringBuilder output_builder) {
        output_builder.append("\n\n  node [shape=plaintext]\n")
                .append("  some_node [\n")
                .append("  label=<\n")
                .append("  <table border=\"0\" cellborder=\"1\" cellspacing=\"0\">\n")
                .append("    <tr><td bgcolor=\"white\">Legend</td></tr>\n")
                .append("    <tr><td bgcolor=\"lightgoldenrodyellow\">p below 0.01</td></tr>\n")
                .append("    <tr><td bgcolor=\"yellow1\">p below 0.0001</td></tr>\n")
                .append("    <tr><td bgcolor=\"gold1\">p below 0.000001</td></tr>\n")
                .append("    <tr><td bgcolor=\"darkorange\">p below 0.00000001</td></tr>\n")
                .append("    <tr><td bgcolor=\"firebrick2\">p below 0.0000000001</td></tr>")
                .append("  </table>>\n")
                .append("  ];\n")
                .append("}");
    }

    /**
     * Check if there is any type of functional annotation present in the pangenome
     * @param extensive
     * @return
     */
    public static boolean check_if_functional_annotations_present(boolean extensive) {
        int go_nodes = (int) count_nodes(GO_LABEL);
        if (!extensive) {
            return go_nodes != 0;
        }
        int bgc_nodes = (int) count_nodes(BGC_LABEL); // count nodes with a specific label
        int phobius_nodes = (int) count_nodes(PHOBIUS_LABEL);
        int cog_nodes = (int) count_nodes(COG_LABEL);
        return bgc_nodes != 0 || go_nodes != 0 || phobius_nodes != 0 || cog_nodes != 0; // no functions are present
    }

    /**
     * functional_annotation_overview() fa_overview()
     *
     * Proteins must be clustered
     */
    public void functional_annotation_overview() {
        Pantools.logger.info("Functional annotation overview.");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            stop_if_no_active_grouping_present(); // stop if there is active homology grouping
            skip.create_skip_arrays(false, true);
            annotation_identifiers = get_annotation_identifiers(false, false, PATH_TO_THE_ANNOTATIONS_FILE); // do not print, USE the input via -af
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        create_directory_in_DB("function");
        try (Transaction tx = GRAPH_DB.beginTx()) {
            check_if_functional_annotations_present(true);
            function_overview_per_group(null, WORKING_DIRECTORY +"function/");
            StringBuilder go_results = count_function_node_connections_fa_overview(GO_LABEL, RelTypes.has_go);
            String go_sub_results = get_go_sub_category_overview();
            String go_results_str = go_results.toString();
            go_results_str = go_results_str.replace("#Connected", go_sub_results + "\n\n#Connected");
            write_string_to_file_in_DB(go_results_str, "function/go_overview.csv");

            StringBuilder ipro_results = count_function_node_connections_fa_overview(INTERPRO_LABEL, RelTypes.has_interpro);
            write_SB_to_file_in_DB(ipro_results, "function/interpro_overview.csv");

            StringBuilder pfam_results = count_function_node_connections_fa_overview(PFAM_LABEL, RelTypes.has_pfam);
            write_SB_to_file_in_DB(pfam_results, "function/pfam_overview.csv");

            StringBuilder tigrfam_results = count_function_node_connections_fa_overview(TIGRFAM_LABEL, RelTypes.has_tigrfam);
            write_SB_to_file_in_DB(tigrfam_results, "function/tigrfam_overview.csv");

            signal_peptides_overview();
            countCOGsFunctionOverview();
            bgc_overview();
            print_functional_annotation_overview_files();
            tx.success();
        }
    }

    /**
     * hmgroup_class_map is was created and filled in 'function_overview_per_group'
     */
    public static void countCOGsFunctionOverview() {
        HashMap<String, HashSet<Node>> hmgroups_per_class = new HashMap<>();
        HashMap<String, Integer> cog_counter_map = new HashMap<>();
        StringBuilder missing_builder = new StringBuilder("\n#Part 4. Homology groups without COG category\n");
        int[] counter_array = new int[total_genomes+1]; // total COG nodes per genome
        int total_cog_nodes = (int) count_nodes(COG_LABEL); // count nodes with a specific label
        int node_counter = 0;
        HashMap<String, int[]> cog_cat_counter_map = new HashMap<>();
        HashMap<String, StringBuilder> all_mrnas_per_cog = new HashMap<>();
        for (String cog_category : cog_category_array) {
            String[] cog_category_array2 = cog_category.split(": ");
            int[] cog_cat_counter_array = new int[total_genomes];
            cog_cat_counter_map.put(cog_category_array2[0], cog_cat_counter_array);
        }
        HashSet<String> known_hmgroups_with_cog = new HashSet<>(); // prevents that a category is counted multiple times within a homology group
        ResourceIterator<Node> cog_nodes = GRAPH_DB.findNodes(COG_LABEL); // these are mRNA nodes with a COG label
        while (cog_nodes.hasNext()) {
            node_counter ++;
            if (node_counter % 100 == 0 || node_counter == total_cog_nodes) {
                System.out.print("\rExtracting 'COG' nodes: " + node_counter + "/" + total_cog_nodes +
                        "                                        "); // spaces are intentional
            }
            Node cog_node = cog_nodes.next();
            long node_id = cog_node.getId();
            int genome_nr = (int) cog_node.getProperty("genome");
            if (PROTEOME && skip_array[genome_nr-1]) {
                continue;
            }

            String mrna_name = "", mrna_id = "";
            if (!PROTEOME) {
                String annotation_id = (String) cog_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
                mrna_name = retrieveNamePropertyAsString(cog_node);
                if (mrna_name.equals("")) {
                    mrna_name = "-";
                }
                mrna_id = (String) cog_node.getProperty("id");
            } else {
                mrna_id = (String) cog_node.getProperty("protein_ID");
            }
            String COG_category = (String) cog_node.getProperty("COG_category");
            String[] category_array = COG_category.split(""); // can have multiple categories
            Relationship homologyRelationship = cog_node.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
            long homologyNodeId = -1;
            String hmgroup_class = "unknown";
            Node hm_node = null;
            if (homologyRelationship == null) { // even though there is a grouping, the mRNA is not part of an homology group
                Pantools.logger.trace("{} {}", cog_node, homologyRelationship);
            } else {
                hm_node = homologyRelationship.getStartNode();
                homologyNodeId = hm_node.getId();
                hmgroup_class = hmgroup_class_map.get(hm_node);
            }
            boolean cog_found = false;
            for (String cog_cat : category_array) {
                if (cog_cat.equals("-")) {
                    continue;
                }
                cog_found = true;
                incr_array_hashmap(cog_cat_counter_map, cog_cat, 1, genome_nr-1);
                if (!PROTEOME) {
                    all_mrnas_per_cog.computeIfAbsent(cog_cat, s -> new StringBuilder())
                            .append(genome_nr).append(",").append(mrna_name).append(",").append(mrna_id).append(",")
                            .append(node_id).append(",").append(homologyNodeId).append("\n");
                } else {
                    all_mrnas_per_cog.computeIfAbsent(cog_cat, s -> new StringBuilder())
                            .append(genome_nr).append(",").append(mrna_id).append(",").append(node_id).append(",")
                            .append(homologyNodeId).append("\n");
                }
                if (hm_node != null && !known_hmgroups_with_cog.contains(homologyNodeId + cog_cat)) {
                    cog_counter_map.merge(cog_cat + "#" + hmgroup_class, 1, Integer::sum);
                    known_hmgroups_with_cog.add(homologyNodeId + cog_cat);
                }
            }
            if (!cog_found || hm_node == null) {
                continue;
            }
            counter_array[0] ++;
            counter_array[genome_nr] ++;
            hmgroups_per_class.computeIfAbsent(hmgroup_class, s -> new HashSet<>()).add(hm_node);
        }
        if (node_counter == 0) {
            return;
        }
        StringBuilder output_builder2 = new StringBuilder("\nTotal COGs: " + counter_array[0] + "\n");
        for (int i = 1; i < counter_array.length; i++) {
            if (skip_array[i-1]) {
                continue;
            }
            output_builder2.append("Genome ").append(i).append(": ").append(counter_array[i]).append("\n");
        }
        StringBuilder header_builder = new StringBuilder();
        HashMap<String, Integer> class_counter = count_hmgroups_per_class(hmgroups_per_class);
        add_annotated_to_stringbuilder(class_counter, hmgroups_per_class, header_builder, "COG") ;
        add_missing_groups_to_stringbuilder(hmgroups_per_class, missing_builder, "COG");

        StringBuilder all_mrnas_with_cog = new StringBuilder();
        if (!PROTEOME) {
            all_mrnas_with_cog.append("\n#Part 3. mRNAs per COG category. ")
                    .append("Information on each line: genome number, mRNA/gene name, mRNA id, mRNA node id, homology group id\n");
        } else {
            all_mrnas_with_cog.append("\n#Part 3. Proteins per COG category. ")
                    .append("Information on each line: genome number, protein id, protein node id, homology group node id\n");
        }

        for (String cog_category : cog_category_array) {
            String[] cog_cat_array = cog_category.split(": ");
            all_mrnas_with_cog.append("#").append(cog_category).append("\n");
            StringBuilder mrna_per_category = all_mrnas_per_cog.get(cog_cat_array[0]);
            if (mrna_per_category == null) {
                all_mrnas_with_cog.append("\n");
            } else {
                all_mrnas_with_cog.append(mrna_per_category);
            }
        }

        HashMap<String, String> cog_log2_map = create_COG_rscript_input(cog_counter_map, cog_cat_counter_map);
        String freq_abundance_output = create_COG_abundance_frequency_tables(cog_log2_map, cog_cat_counter_map);
        write_string_to_file_in_DB(header_builder.toString() + output_builder2 +
                        freq_abundance_output + all_mrnas_with_cog + missing_builder,
                "function/cog_overview.csv");
        create_COG_rscript();
        System.out.print("\r                                                                       ");
    }

    /**
     *
     * @param cog_log2_map
     * @param cog_cat_counter_map
     * @return
     */
    public static String create_COG_abundance_frequency_tables(HashMap<String, String> cog_log2_map, HashMap<String, int[]> cog_cat_counter_map) {
        String[] class_array = {"core", "accessory", "unique"};
        StringBuilder frequency_builder = new StringBuilder("#Part 2. COG category frequency per genome\nCOG category,");
        for (int i = 1; i <= total_genomes; i++) {
            frequency_builder.append("Genome ").append(i);
            if (i != total_genomes) {
                frequency_builder.append(",");
            }
        }
        frequency_builder.append("\n");

        StringBuilder abundance_builder = new StringBuilder();
        abundance_builder.append("\n#Part 1. Abundance of COGs within a category (core,accessory unique). Two numbers after each category. "
                + "The first value is the percentage abundances of COG functional categories within a category. "
                + "The second value is the log2(relative abundance)\n\n");

        for (String category : cog_category_array) {
            String[] category_array = category.split(":");
            int[] occurrence = cog_cat_counter_map.get(category_array[0]);
            String occurrenceStr = Arrays.toString(occurrence).replace("[","").replace("]","").replace(" ","").replace(",",";");
            frequency_builder.append(category).append(";").append(occurrenceStr).append("\n");
            abundance_builder.append(category).append("\n");
            for (String class1 : class_array) {
                String log2_pc = cog_log2_map.get(category + "#" + class1);
                String spaces = " ";
                if (class1.equals("core")) {
                    spaces = "      ";
                } else if (class1.equals("unique")) {
                    spaces = "    ";
                }
                abundance_builder.append(class1).append(spaces).append(log2_pc).append("\n");
            }
            abundance_builder.append("\n");
        }
        return abundance_builder.toString() + frequency_builder;
    }

    /**
     * Print output files nmaes if they were created
     */
    public static void print_functional_annotation_overview_files() {
        boolean go_present = is_node_present(GO_LABEL);
        boolean cog_present = is_node_present(COG_LABEL);
        boolean phobius_signalp_present = is_node_present(PHOBIUS_LABEL);
        if (!phobius_signalp_present) {
            phobius_signalp_present = is_node_present(SIGNALP_LABEL);
        }
        boolean bgc_present = is_node_present(BGC_LABEL);
        boolean tigrfam_present = is_node_present(TIGRFAM_LABEL);
        boolean pfam_present = is_node_present(PFAM_LABEL);
        boolean interpro_present = is_node_present(INTERPRO_LABEL);

        String dir = WORKING_DIRECTORY + "function/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}functions_per_group_and_mrna.csv", dir);
        Pantools.logger.info(" {}function_counts_per_group.csv", dir);
        if (go_present) {
            Pantools.logger.info(" {}go_overview.csv", dir);
        }
        if (pfam_present) {
            Pantools.logger.info(" {}pfam_overview.csv", dir);
        }
        if (tigrfam_present) {
            Pantools.logger.info(" {}tigrfam_overview.csv", dir);
        }
        if (interpro_present) {
            Pantools.logger.info(" {}interpro_overview.csv", dir);
        }
        if (bgc_present) {
            Pantools.logger.info(" {}bgc_overview.csv", dir);
        }
        if (phobius_signalp_present) {
            Pantools.logger.info(" {}phobius_signalp_overview.csv", dir);
        }
        if (cog_present) {
            Pantools.logger.info(" {}cog_overview.csv", dir);
            Pantools.logger.info(" {}cog_per_class.R", dir);
        }
    }

    /**
     * Classify a BGC only based on the absence presence of the name. Length or shared genes is not important.
     * @param core_access_uni_map
     * @param phenotype_shared_specific
     * @param overview_builder
     */
    public void classify_bgc(HashMap<String, StringBuilder> core_access_uni_map, HashMap<String, StringBuilder> phenotype_shared_specific, StringBuilder overview_builder) {
        overview_builder.append("#Biosynthetic gene clusters\n");
        int total_nodes = (int) count_nodes(BGC_LABEL); // count nodes with a specific label
        if (total_nodes == 0) { // no antiSMASH annotations in pangenome
            overview_builder.append(" No antiSMASH clusters were added to the pangenome yet\n\n");
            return;
        }
        boolean first_core = true;
        HashMap<String, int[]> bgc_map = gather_bgc_for_classify_bgc(total_nodes);
        for (String type : bgc_map.keySet()) {
            String bgc_type = "'" + type + "' bgc";
            int[] value = bgc_map.get(type);
            int genome_counter = 0;
            HashMap<String, Integer> phenotype_counter = new HashMap<>();
            ArrayList<Integer> present_genomes = new ArrayList<>();
            for (int i = 0; i < value.length; i++) {
                if (skip_array[i]) {
                    continue;
                }
                if (value[i] != 0) {
                    genome_counter ++;
                    present_genomes.add(i+1);
                } else {
                    continue;
                }

                if (PHENOTYPE == null) {
                    continue;
                }
                String current_pheno = geno_pheno_map.get(i+1);
                if ("?".equals(current_pheno) || current_pheno == null) {
                    continue;
                }
                phenotype_counter.merge(current_pheno, 1, Integer::sum);
            }
            String class1 = "core";
            if (genome_counter >= core_threshold) { // core
                if (first_core) {
                    core_access_uni_map.computeIfAbsent("core", s -> new StringBuilder()).append("#BGC\n");
                    first_core = false;
                }
                core_access_uni_map.computeIfAbsent("core", s -> new StringBuilder()).append(bgc_type).append("\n");
            } else if (genome_counter <= unique_threshold) { // unique
                for (int genome_nr : present_genomes) {
                    core_access_uni_map.computeIfAbsent(genome_nr + "_uni", s -> new StringBuilder())
                            .append(bgc_type).append("\n");
                }
                class1 = "unique";
            } else { //accessory
                core_access_uni_map.computeIfAbsent(genome_counter + "_acc", s -> new StringBuilder())
                        .append(bgc_type).append("\n");
                class1 = "accessory";
            }
            overview_builder.append(bgc_type).append(" - ").append(class1).append("\n");
            for (String phenotype : phenotype_counter.keySet()) { // empty when no --phenotype was given by the user
                int pheno_count = phenotype_counter.get(phenotype);
                int threshold = phenotype_threshold_map.get(phenotype);
                boolean specific = true, sufficient = false;
                if (pheno_count < threshold) {
                    sufficient = true;
                }
                for (String other_phenotype : phenotype_counter.keySet()) {
                    if (other_phenotype.equals ("?") || other_phenotype.equals(phenotype)) {
                        continue;
                    }
                    specific = false;
                }
                phenotype_shared_specific.computeIfAbsent(phenotype + "_sh", s -> new StringBuilder())
                        .append(bgc_type).append("\n"); // shared
                if (specific && sufficient) {
                    phenotype_shared_specific.computeIfAbsent(phenotype + "_sp", s -> new StringBuilder())
                            .append(bgc_type).append("\n"); // specific
                }
                if (specific) {
                    phenotype_shared_specific.computeIfAbsent(phenotype + "_ex", s -> new StringBuilder())
                            .append(bgc_type).append("\n"); // exclusive
                }
            }
        }
        overview_builder.append("\n");
    }

    /**
     *
     * @param total_nodes
     * @return
     */
    public HashMap<String, int[]> gather_bgc_for_classify_bgc(int total_nodes) {
        HashMap<String, int[]> bgc_map = new HashMap<>(); // key is bgc type, value is int[] with frequency per genome
        ResourceIterator<Node> bgc_nodes = GRAPH_DB.findNodes(BGC_LABEL);
        int node_counter = 0;
        while (bgc_nodes.hasNext()) {
            Node bgc_node = bgc_nodes.next();
            node_counter ++;
            if (node_counter % 10 == 0 || node_counter == total_nodes) {
                System.out.print("\r BGC nodes: " + node_counter + "/" + total_nodes);
            }
            int genome_nr = (int) bgc_node.getProperty("genome");
            if (!PROTEOME) {
                String annotation_id = (String) bgc_node.getProperty("annotation_id");
                if (!annotation_identifiers.contains(annotation_id)) {
                    continue;
                }
            }
            String type = (String) bgc_node.getProperty("type");
            int[] value_array = bgc_map.get(type);
            if (value_array == null) {
                value_array = new int[total_genomes];
            }
            value_array[genome_nr-1] ++;
            bgc_map.put(type, value_array);
        }
        return bgc_map;
    }

    /**
     * Classify GO, PFAM, InterPro or TIGRFAM nodes
     * @param function_label GO, PFAM, InterPro or TIGRFAM label
     * @param reltype has_go, has_pfam etc
     * @param core_access_uni_map
     * @param phenotype_shared_specific
     * @param type_str
     * @param overview_builder
     */
    public static void classify_function(Label function_label, RelTypes reltype, HashMap<String, StringBuilder> core_access_uni_map,
                                         HashMap<String, StringBuilder> phenotype_shared_specific, String type_str, StringBuilder overview_builder) {

        boolean first_core = true;
        ResourceIterator<Node> function_nodes = GRAPH_DB.findNodes(function_label);
        int total_nodes = (int) count_nodes(function_label); // count nodes with a specific label
        int node_counter = 0;
        int[] class_count_array = new int[4]; // [total nodes, core, accessory, unique]
        while (function_nodes.hasNext()) {
            node_counter ++;
            if (node_counter % 100 == 0 || node_counter == total_nodes) {
                System.out.print("\r " + type_str + " nodes: " + node_counter + "/" + total_nodes);
            }
            Node function_node = function_nodes.next();
            long function_node_id = function_node.getId();
            String function_id = (String) function_node.getProperty("id");
            int total_frequency = (int) function_node.getProperty("total_frequency");
            if (total_frequency == 0) {
                continue;
            }
            class_count_array[0] ++;
            int[] freq_array = (int[]) function_node.getProperty("frequency");
            int genome_counter = 0;
            HashMap<String, Integer> phenotype_counter = new HashMap<>();
            ArrayList<Integer> present_genomes = new ArrayList<>();
            for (int i = 0; i < freq_array.length; i++) {
                if (skip_array[i]) {
                    continue;
                }
                if (freq_array[i] != 0) {
                    genome_counter ++;
                    present_genomes.add(i+1);
                } else {
                    continue;
                }
                if (PHENOTYPE == null) {
                    continue;
                }
                String current_pheno = geno_pheno_map.get(i+1);
                if ("?".equals(current_pheno) || current_pheno == null ) {
                    continue;
                }
                phenotype_counter.merge(current_pheno, 1, Integer::sum);
            }

            final String line = String.format("%s,%d\n", function_id, function_node_id);
            if (genome_counter >= core_threshold) { // core
                if (first_core) {
                    core_access_uni_map.computeIfAbsent("core", s -> new StringBuilder())
                            .append("\n#").append(type_str).append("\n");
                    first_core = false;
                }
                core_access_uni_map.computeIfAbsent("core", s -> new StringBuilder()).append(line);
                class_count_array[1] ++;
            } else if (genome_counter <= unique_threshold) { // unique
                for (int genome_nr : present_genomes) {
                    core_access_uni_map.computeIfAbsent(genome_nr + "_uni", s -> new StringBuilder()).append(line);
                }
                class_count_array[3] ++;
            } else { // accessory
                core_access_uni_map.computeIfAbsent(genome_counter + "_acc", s -> new StringBuilder()).append(line);
                class_count_array[2] ++;
            }

            for (String pheno1 : phenotype_counter.keySet()) {
                int pheno_count = phenotype_counter.get(pheno1);
                int threshold = phenotype_threshold_map.get(pheno1);
                boolean specific = true, sufficient = true;
                if (pheno_count < threshold) {
                    sufficient = false;
                }
                phenotype_shared_specific.computeIfAbsent(pheno1 + "_sh", s -> new StringBuilder()).append(line);
                for (String pheno2 : phenotype_counter.keySet()) {
                    if (pheno2.equals ("?") || pheno2.equals(pheno1) || pheno2.equals("Unknown") || pheno2.equals("unknown")) {
                        continue;
                    }
                    specific = false;
                }
                if (specific && sufficient) { // specific
                    phenotype_shared_specific.computeIfAbsent(pheno1 + "_sp", s -> new StringBuilder()).append(line);
                }
                if (specific) { // exclusive
                    phenotype_shared_specific.computeIfAbsent(pheno1 + "_ex", s -> new StringBuilder()).append(line);
                }
            }
        }

        String percentage_core = get_percentage_str(class_count_array[1], class_count_array[0], 2);
        String percentage_accessory = get_percentage_str(class_count_array[2], class_count_array[0], 2);
        String percentage_unique = get_percentage_str(class_count_array[3], class_count_array[0], 2);
        System.out.println();
        overview_builder.append("#").append(type_str).append("\n")
                .append("Total ").append(type_str).append(" nodes: ").append(total_nodes)
                .append("\nNodes with connection: ").append(class_count_array[0])
                .append("\nCore ").append(class_count_array[1]).append(" ").append(percentage_core).append("%")
                .append("\nAccessory ").append(class_count_array[2]).append(" ").append(percentage_accessory).append("%")
                .append("\nUnique ").append(class_count_array[3]).append(" ").append(percentage_unique).append("%\n\n");
    }

    /**
     * Find core, accessory, unique, phenotype shared/specific/exclusive functions
     * Only works on biosyntetic geneclusters, GO, PFAM, TIGRFAM, InterPro
     *
     * Requires
     * -dp
     */
    public void functional_classification() {
        create_directory_in_DB("function/functional_classification");
        Pantools.logger.info("Calculating CORE, ACCESSORY, and UNIQUE functional annotations.");
        check_database(); // starts up the graph database if needed
        StringBuilder overview_builder = new StringBuilder();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user
            check_if_genomes_have_functions();
            annotation_identifiers = get_annotation_identifiers(false, false, PATH_TO_THE_ANNOTATIONS_FILE); // do not print, USE the input via -af
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) {
            ArrayList<String> genome_list = classification.create_genome_list(false); // needed for compatibility with --variants

            boolean annotations_present = check_if_functional_annotations_present(false);
            if (!annotations_present) {
                Pantools.logger.error("There are no functional annotation nodes in the pangenome.");
                System.exit(1);
            }
            if (PHENOTYPE != null) {
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            } else {
                System.out.println("\rNo --phenotype was given. Unable to find phenotype specific functions");
            }
            classification.core_unique_thresholds_for_classification(genome_list, "functional annotations"); //sets core_threshold and unique_threshold
            HashMap<String, StringBuilder> core_access_uni_map = new HashMap<>();
            HashMap<String, StringBuilder> phenotype_shared_specific = new HashMap<>(); // key is genome number with '_sp', '_sh' or '_ex'
            classify_bgc(core_access_uni_map, phenotype_shared_specific, overview_builder);
            classify_function(GO_LABEL, RelTypes.has_go, core_access_uni_map, phenotype_shared_specific, "GO", overview_builder);
            classify_function(PFAM_LABEL, RelTypes.has_pfam, core_access_uni_map, phenotype_shared_specific, "PFAM", overview_builder );
            classify_function(INTERPRO_LABEL, RelTypes.has_interpro, core_access_uni_map, phenotype_shared_specific, "InterPro", overview_builder);
            classify_function(TIGRFAM_LABEL, RelTypes.has_tigrfam, core_access_uni_map, phenotype_shared_specific, "TIGRFAM", overview_builder);
            write_accessory_unique_function_results(core_access_uni_map);
            write_function_phenotype_results(phenotype_shared_specific, overview_builder);
            tx.success();
        }

        write_SB_to_file_in_DB(overview_builder, "function/functional_classification/functional_classification_overview.txt");
        String dir = WORKING_DIRECTORY + "function/functional_classification/";

        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}functional_classification_overview.txt", dir);
        Pantools.logger.info(" {}core_functions.txt", dir);
        Pantools.logger.info(" {}accessory_functions.txt", dir);
        Pantools.logger.info(" {}unique_functions.txt", dir);
        if (PHENOTYPE != null) {
            Pantools.logger.info(" {}phenotype_shared_functions.txt", dir);
            Pantools.logger.info(" {}phenotype_specific_functions.txt", dir);
            Pantools.logger.info(" {}phenotype_exclusive_functions.txt", dir);
        }
    }

    /**
     * Create the phenotype related output files for 'functional_classification'
     * @param phenotype_shared_specific
     * @param overview_builder
     */
    public static void write_function_phenotype_results(HashMap<String, StringBuilder> phenotype_shared_specific, StringBuilder overview_builder) {
        if (PHENOTYPE == null) { // no --phenotype given by user
            return;
        }
        String header = "#Each line holds an annotation identifier and the database node identifier\n";
        StringBuilder shared_builder = new StringBuilder("#Phenotype shared functions\n#Selected phenotype: " + PHENOTYPE );
        StringBuilder specific_builder = new StringBuilder("#Phenotype specific functions\n#Selected phenotype: " + PHENOTYPE);
        StringBuilder exclusive_builder = new StringBuilder("#Phenotype exclusive functions\n#Selected phenotype: " + PHENOTYPE + "\n\n");
        StringBuilder shared_nr_builder = new StringBuilder("Phenotype shared, ");
        StringBuilder specific_nr_builder = new StringBuilder("Phenotype specific, ");
        StringBuilder exclusive_nr_builder = new StringBuilder("Phenotype exclusive\n");

        shared_builder.append(", a threshold of ").append(phenotype_threshold).append("% of the genome was required to be SHARED\n\n");
        specific_builder.append(", a threshold of ").append(phenotype_threshold).append("% of the genomes was required to be SPECIFIC\n\n");

        shared_builder.append(header);
        specific_builder.append(header);
        exclusive_builder.append(header);
        String[] key_additions = new String[]{"_sp","_sh", "_ex"};
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.startsWith("?") || phenotype.startsWith("Unknown")) {
                continue;
            }
            for (String key_addition : key_additions) {
                String pheno_key =  phenotype +  key_addition;
                if (!phenotype_shared_specific.containsKey(pheno_key)) {
                    continue;
                }
                String value_str = phenotype_shared_specific.get(pheno_key).toString();
                String[] value_array = value_str.split("\n");
                int total_functions = value_array.length;
                if (value_str.equals("")) {
                    total_functions = 0;
                }
                int[] genomes = phenotype_map.get(phenotype);
                int threshold = phenotype_threshold_map.get(phenotype);
                int genome_counter = 0;
                for(int genome_nr : genomes) {
                    if (skip_array[genome_nr-1]) {
                        continue;
                    }
                    genome_counter++;
                }
                String info_str = "#Phenotype: "  + phenotype + ", " + genome_counter + " genomes. "
                        + total_functions + " functions found\n\n" + value_str + "\n";
                String info_str1 = "#Phenotype: "  + phenotype + ", " + genome_counter + " genomes, threshold of " + threshold + " genomes\n"
                        + total_functions + " functions found\n\n" + value_str + "\n";

                String info_str2 = phenotype + ": " + total_functions + "\n";
                if (pheno_key.endsWith("_sh")) { // shared
                    shared_builder.append(info_str1);
                    shared_nr_builder.append(info_str2);
                } else if (pheno_key.endsWith("_sp")) { // specific
                    specific_builder.append(info_str1);
                    specific_nr_builder.append(info_str2);
                } else if (pheno_key.endsWith("_ex")) { // exclusive
                    exclusive_builder.append(info_str);
                    exclusive_nr_builder.append(info_str2);
                }
            }
        }
        write_SB_to_file_in_DB(specific_builder, "function/functional_classification/phenotype_specific_functions.txt");
        write_SB_to_file_in_DB(shared_builder, "function/functional_classification/phenotype_shared_functions.txt");
        write_SB_to_file_in_DB(exclusive_builder, "function/functional_classification/phenotype_exclusive_functions.txt");
        overview_builder.append("#Number of phenotype SHARED, SPECIFIC and EXCLUSIVE functions\n")
                .append(shared_nr_builder).append("\n")
                .append(specific_nr_builder).append("\n")
                .append(exclusive_nr_builder);
    }
    /**
     * Accessory functions are the number of genomes between the core and unique thresholds
     * @param core_access_uni_map
     */
    public static void write_accessory_unique_function_results(HashMap<String, StringBuilder> core_access_uni_map) {
        String core_groups;
        StringBuilder accessory_builder = new StringBuilder();
        StringBuilder unique_builder = new StringBuilder();

        if (core_access_uni_map.containsKey("core")) {
            core_groups = core_access_uni_map.get("core").toString();
        } else {
            core_groups = "";
        }
        write_string_to_file_in_DB(core_groups, "function/functional_classification/core_functions.txt");

        for (int i = adj_total_genomes-1; i > unique_threshold; i--) {
            String percentage = get_percentage_str(i, adj_total_genomes, 2);
            if (core_access_uni_map.containsKey(i + "_acc")) {
                StringBuilder value = core_access_uni_map.get(i + "_acc");
                accessory_builder.append("#Functions found in number of genomes: ").append(i).append(", ").append(percentage).append("%\n")
                        .append(value.toString()).append("\n");
            } else {
                accessory_builder.append("#Functions found in number of genomes: ").append(i).append(", ").append(percentage).append("%\n\n");
            }
        }
        write_SB_to_file_in_DB(accessory_builder, "function/functional_classification/accessory_functions.txt");

        for (int i = 1; i <= total_genomes; i++) {
            if (core_access_uni_map.containsKey(i + "_uni")) {
                StringBuilder value = core_access_uni_map.get(i + "_uni");
                unique_builder.append("#Genome ").append(i).append("\n").append(value.toString()).append("\n");
            } else {
                unique_builder.append("#Genome ").append(i).append("\n\n");
            }
        }
        write_SB_to_file_in_DB(unique_builder, "function/functional_classification/unique_functions.txt");
    }

    /**
     * Static method that populates GO, IPR, TIGRFAM, PFAM, Phobius and SignalP maps with information from an InterProScan GFF file
     *
     * @param goterm_node_map GO map: key is GO term, value is a map of protein names to start and end positions within that protein
     * @param interpro_node_map InterPro map: key is InterPro term, value is a map of protein names to start and end positions within that protein
     * @param tigrfam_node_map TIGRFAM map: key is TIGRFAM term, value is a map of protein names to start and end positions within that protein
     * @param pfam_node_map PFAM map: key is PFAM term, value is a map of protein names to start and end positions within that protein
     * @param phobius_node_map
     * @param signalp_map
     * @param path_to_gff path to the InterProScan GFF file
     * @param all_added_ids map to keep track of all added IDs: key is the database name (e.g. InterPro), value is a set of all added IDs
     */
    public static void read_interpro_gff(HashMap<String, HashMap<String, Integer[]>> goterm_node_map,
                                         HashMap<String, HashMap<String, Integer[]>> interpro_node_map,
                                         HashMap<String, HashMap<String, Integer[]>> tigrfam_node_map,
                                         HashMap<String, HashMap<String, Integer[]>> pfam_node_map,
                                         HashMap<String, int[]> phobius_node_map,
                                         HashMap<String, String> signalp_map,
                                         String path_to_gff, HashMap<String, HashSet<String>> all_added_ids) throws IOException {

        boolean add_interpro = false;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            int total_ipro = (int) count_nodes(INTERPRO_LABEL);
            if (total_ipro > 0) {
                add_interpro = true;
            }
            tx.success();
        }

        try (BufferedReader in = new BufferedReader(new FileReader(path_to_gff))) { // read the GFF file
            String prev_p_name = ""; // used for Phobius since these are on multiple lines
            int transmembrane_counter = 0, signal_peptide_counter = 0;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith("##FASTA")) {
                    break;
                } else if (line.equals("") || line.startsWith("#")) {
                    continue;
                }
                String[] fields = line.trim().split("\\t");
                if (fields.length != 9) {
                    throw new RuntimeException("The provided GFF file failed to split into 9 columns: " + path_to_gff +
                            " The following line is not correct:" + line);
                }
                String prot_name = fields[0].replaceAll("[|:]", "_"); // replace | and : with _ to match the Classification.create_mrna_node_map_for_genome method for obtaining the protein names
                String source_field = fields[1];
                String attribute_field = fields[fields.length-1];
                int proteinStartPosition = Integer.parseInt(fields[3]);
                int proteinEndPosition = Integer.parseInt(fields[4]);
                if (source_field.equals("Phobius")) { // hit
                    if (!prot_name.equals(prev_p_name)) {
                        int[] phobius_array = new int []{transmembrane_counter, signal_peptide_counter};
                        if (!prev_p_name.equals("")) {
                            phobius_node_map.put(prev_p_name, phobius_array);
                        }
                        transmembrane_counter = 0;
                        signal_peptide_counter = 0;
                    }

                    if (fields[8].contains("Name=SIGNAL_PEPTIDE")) {
                        signal_peptide_counter ++;
                    }
                    if (fields[8].contains("Name=TRANSMEMBRANE")) {
                        transmembrane_counter ++;
                    }
                    prev_p_name = prot_name;
                }

                if (source_field.contains("SignalP")) {
                    signalp_map.put(prot_name, "yes");
                }

                if (add_interpro && attribute_field.contains("IPR")) { // Interpro domain can be found on multiple lines: PFAM, TIGRFAM, etc.
                    String[] attribute_array = attribute_field.split("IPR");
                    for (int i=1; i<= attribute_array.length-1; i += 2) {
                        String line_str = attribute_array[i];
                        if (line_str.length() <= 5) {
                            continue;
                        }
                        String substring = line_str.substring(0, 6); // a interpro ID must have 6 numbers
                        boolean pass1 = check_if_string_can_be_converted_to_int(substring);
                        boolean pass2 = false;
                        if (line_str.length() >= 7) {
                            String substr2 = line_str.substring(0, 7); // and should not have 8 numbers
                            pass2 = check_if_string_can_be_converted_to_int(substr2);
                        }

                        if (pass1 && !pass2) {
                            String ipro_term = "IPR" + substring;
                            all_added_ids.computeIfAbsent("InterPro", s -> new HashSet<>()).add(ipro_term);
                            interpro_node_map.computeIfAbsent(ipro_term, s -> new HashMap<>()).put(prot_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                        }
                    }
                }

                if (attribute_field.contains("GO:")) { // GO term can be found on multiple lines: PFAM, TIGRFAM, etc.
                    String word = "GO:";
                    for (int i = -1; (i = attribute_field.indexOf(word, i + 1)) != -1; i++) {
                        String goterm = attribute_field.substring(i, i+10);
                        all_added_ids.computeIfAbsent("GO", s -> new HashSet<>()).add(goterm);
                        goterm_node_map.computeIfAbsent(goterm, s -> new HashMap<>()).put(prot_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                    }
                }


                if (source_field.equals("TIGRFAM")) {  // tigrfam on line found
                    String[] attribute_array = attribute_field.split("Name=TIGR");
                    String tigrfam_id = attribute_array[1].substring(0, 5);
                    if (attribute_array.length == 2) {
                        all_added_ids.computeIfAbsent("TIGRFAM", s -> new HashSet<>()).add(tigrfam_id);
                        tigrfam_node_map.computeIfAbsent("TIGR" + tigrfam_id, s -> new HashMap<>()).put(prot_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                    }
                }

                if (source_field.equals("Pfam")) { // pfam on line found
                    String[] attribute_array = attribute_field.split("PF");
                    for (int i=1; i < attribute_array.length; i ++) {
                        String line_str = attribute_array[i];
                        if (line_str.length() < 6) {
                            continue;
                        }
                        String substr1 = line_str.substring(0, 5); // a PFAM ID must have 5 numbers
                        String substr2 = line_str.substring(0, 6); // should not have 6 numbers
                        boolean pass1 = check_if_string_can_be_converted_to_int(substr1);
                        boolean pass2 = check_if_string_can_be_converted_to_int(substr2);
                        if (pass1 && !pass2) { //pfam identifier has 5 letters
                            String pfam_id = "PF" + substr1;
                            all_added_ids.computeIfAbsent("PFAM", s -> new HashSet<>()).add(pfam_id);
                            pfam_node_map.computeIfAbsent(pfam_id, s -> new HashMap<>()).put(prot_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                        }
                    }
                }
            }
            if (!prev_p_name.equals("")) {
                int[] phobius_array = new int []{transmembrane_counter, signal_peptide_counter};
                phobius_node_map.put(prev_p_name, phobius_array);
            }

        } catch (IOException ioe) {
            throw new IOException("Error reading file: " + path_to_gff, ioe);
        }
    }

    /**
     *
     * @param string
     * @return
     */
    public static boolean check_if_string_can_be_converted_to_int(String string) {
        try {
            int result1 = Integer.parseInt(string);
        } catch (NumberFormatException nee) {
            //do nothing
            return false;
        }
        return true;
    }

    /**
     * The first column contains the sequence name, the second an annotation identifier. Optionally, a third and fourth column indicate the start and end position of the annotation within the sequence.
     * Identifier is allowed to be from GO, pfam, interpro or tigrfam
     * @param goterm_node_map
     * @param interpro_node_map
     * @param tigrfam_node_map
     * @param pfam_node_map
     * @param all_added_ids
     * @param path_to_file
     * @param log_builder
     */
    public static void read_custom_annot_input(HashMap<String, HashMap<String, Integer[]>> goterm_node_map,
                                               HashMap<String, HashMap<String, Integer[]>> interpro_node_map,
                                               HashMap<String, HashMap<String, Integer[]>> tigrfam_node_map,
                                               HashMap<String, HashMap<String, Integer[]>> pfam_node_map,
                                               HashMap<String, HashSet<String>> all_added_ids, String path_to_file, StringBuilder log_builder) throws IOException {

        long line_counter = 1;
        String separator = ","; // default separator is comma
        ArrayList<String> unrecognized = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(path_to_file))) { // read the GFF file
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.isEmpty()) {
                    continue;
                }

                // determine if the file is tab or comma separated from first line
                if (line_counter == 1) {
                    separator = line.contains("\t") ? "\t" : ",";
                    Pantools.logger.debug("Found separator for {} to be '{}'.", path_to_file, separator);
                }

                // split the line based on the determined separator
                String[] fields = line.trim().split(separator);
                if (fields.length != 2 && fields.length != 4) {
                    Pantools.logger.error("Line {} is {} columns wide; must be two (or four).", line_counter, fields.length);
                    throw new RuntimeException("Invalid custom functional annotation file format: " + path_to_file);
                }

                String mrna_name = fields[0]; // protein/mrna name
                String function_identifier = fields[1];
                int proteinStartPosition = -1;
                int proteinEndPosition = -1;
                if (fields.length == 4) {
                    proteinStartPosition = Integer.parseInt(fields[2]);
                    proteinEndPosition = Integer.parseInt(fields[3]);
                }
                if (function_identifier.startsWith("GO")) {
                    all_added_ids.computeIfAbsent("GO", s -> new HashSet<>()).add(function_identifier);
                    goterm_node_map.computeIfAbsent(function_identifier, s -> new HashMap<>()).put(mrna_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                } else if (function_identifier.startsWith("PF")) {
                    all_added_ids.computeIfAbsent("PFAM", s -> new HashSet<>()).add(function_identifier);
                    pfam_node_map.computeIfAbsent(function_identifier, s -> new HashMap<>()).put(mrna_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                } else if (function_identifier.startsWith("IPR")) {
                    all_added_ids.computeIfAbsent("InterPro", s -> new HashSet<>()).add(function_identifier);
                    interpro_node_map.computeIfAbsent(function_identifier, s -> new HashMap<>()).put(mrna_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                } else if (function_identifier.startsWith("TIGR")) {
                    all_added_ids.computeIfAbsent("TIGRFAM", s -> new HashSet<>()).add(function_identifier);
                    tigrfam_node_map.computeIfAbsent(function_identifier, s -> new HashMap<>()).put(mrna_name, new Integer[]{proteinStartPosition, proteinEndPosition});
                } else {
                    // not recognized
                    unrecognized.add(function_identifier);
                }
                line_counter ++;
            }
        } catch (IOException ioe) {
            throw new IOException("Error reading file: " + path_to_file, ioe);
        }

        if (!unrecognized.isEmpty()) {
            log_builder.append(unrecognized.size()).append(" function identifiers in the file were not recognized\n")
                    .append(unrecognized.toString().replace("[","").replace("]","")).append("\n\n");
        }
    }

    /**
     This function can read the official format of eggNOG-mapper v2. 22 columns.
     * @param input_file
     * @param mrna_node_map
     * @param goterm_node_map key is GO term, value is a map of protein names to start and end positions within that protein (hardcoded as -1 because eggNOG-mapper does not provide this information)
     * @param all_added_ids
     * @param log_builder
     * @param not_found_ids_mrnas
     */
    public static void read_eggnogmapper_input(String input_file, HashMap<String, Node> mrna_node_map, HashMap<String, HashMap<String, Integer[]>> goterm_node_map,
                                               HashMap<String, HashSet<String>> all_added_ids, StringBuilder log_builder, HashMap<String, HashSet<String>> not_found_ids_mrnas,
                                               ArrayList<String> selectedLabels) throws IOException {

        String[] line_part_array;
        int known_cog = 0, unknown_cog = 0;
        HashSet<Node> added_mrnas = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            try (BufferedReader in = new BufferedReader(new FileReader(input_file))) { // read the GFF file
                String[] header_array = new String[] {""};
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    if (line.startsWith("#query")) { // temp
                        header_array = line.split("\t");
                    }
                    if (line.startsWith("#")) {
                        continue;
                    }

                    String[] line_array = line.split("\t");
                    Node mrna_node = mrna_node_map.get(line_array[0]);
                    if (mrna_node == null) {
                        not_found_ids_mrnas.computeIfAbsent("protein_id", s -> new HashSet<>()).add(line_array[0]);
                        continue;
                    }
                    boolean new_cog = false;
                    boolean cog_info_added = false;
                    for (int i = 0; i < line_array.length; i++) {
                        if (line_array[i].equals("")) {
                            continue;
                        }
                        switch (header_array[i]) { // not all 22 are informative
                            case "GOs": // all GO terms are added, not only the specific ones
                                if (!selectedLabels.contains("GO")){
                                    continue;
                                }
                                line_part_array = line_array[i].split(",");
                                for (String goterm : line_part_array) {
                                    all_added_ids.computeIfAbsent("GO", s -> new HashSet<>()).add(goterm);
                                    goterm_node_map.computeIfAbsent(goterm, s -> new HashMap<>()).put(line_array[0], new Integer[]{-1, -1});
                                }
                                break;
                            case "EC":
                                mrna_node.removeProperty("EC");
                                mrna_node.setProperty("EC", line_array[i]);
                                break;
                            case "KEGG_ko":
                                break;
                            case "KEGG_Pathway":
                                break;
                            case "KEGG_Module":
                                break;
                            case "KEGG_Reaction":
                                break;
                            case "KEGG_rclass":
                                break;
                            case "BRITE":
                                break;
                            case "KEGG_TC":
                                break;
                            case "CAZy":
                                break;
                            case "BiGG_Reaction":
                                break;
                            case "COG Functional cat.":
                                if (!selectedLabels.contains("COG")){
                                    continue;
                                }
                                if (mrna_node.hasLabel(COG_LABEL)) {
                                    mrna_node.removeProperty("COG_category"); // old, can be removed later
                                    mrna_node.removeProperty("COG_id");  // old, can be removed later
                                    mrna_node.removeProperty("COG_name"); // old, can be removed later
                                    mrna_node.removeProperty("COG_description"); // old, can be removed later
                                } else {
                                    mrna_node.addLabel(COG_LABEL);
                                    new_cog = true;
                                }
                                mrna_node.setProperty("COG_category", line_array[i]);
                                cog_info_added = true;
                                break;
                            case "eggNOG free text desc.": // old eggnog mapper version
                                mrna_node.setProperty("COG_description", line_array[i]);

                                //for new eggnog mapper versions
                                break;
                            case "eggNOG_OGs":
                                if (!selectedLabels.contains("COG")){
                                    continue;
                                }
                                line_part_array = line_array[i].split("\\|");
                                String cog_id;
                                if (line_part_array[0].contains("@")) {
                                    String[] line_part_array2 = line_part_array[0].split("@");
                                    cog_id = line_part_array2[0];
                                } else {
                                    cog_id = line_part_array[0];
                                }
                                if (mrna_node.hasLabel(COG_LABEL)) {
                                    mrna_node.removeProperty("COG_id");
                                } else {
                                    mrna_node.addLabel(COG_LABEL);
                                    new_cog = true;
                                }
                                mrna_node.setProperty("COG_id", cog_id);
                                cog_info_added = true;
                                break;
                            case "COG_category": // old eggnog mapper version
                                if (!selectedLabels.contains("COG")){
                                    continue;
                                }
                                mrna_node.removeProperty("COG_category");
                                mrna_node.setProperty("COG_category", line_array[i]);
                                break;
                            default:
                                break;
                        }
                    }
                    if (new_cog) {
                        unknown_cog ++;
                    } else if (cog_info_added && !added_mrnas.contains(mrna_node)) {
                        known_cog ++;
                    }
                    added_mrnas.add(mrna_node);
                }
            } catch (IOException ioe) {
                throw new IOException("Failed to read file " + input_file, ioe);
            }
            log_builder.append(unknown_cog).append(" new mRNAs have a COG category, ").append(known_cog).append(" were already known\n");
            tx.success();
        }
    }

    /**
     *
     * @param node_label
     * @param annotation_node_map
     */
    public static void put_nodes_and_ids_in_map(Label node_label, HashMap<String, Node> annotation_node_map) {
        ResourceIterator<Node> nodes = GRAPH_DB.findNodes(node_label);
        while (nodes.hasNext()) { // retrieve genome locations
            Node node = nodes.next();
            String id_str = (String) node.getProperty("id");
            annotation_node_map.put(id_str,node);
        }
    }

    /**
     *
     * @return
     */
    public static HashMap<String, Node> find_all_F_annotation_nodes() {
        HashMap<String, Node> annotation_node_map = new HashMap<>();
        Pantools.logger.debug("Gathering functional annotation nodes: GO.");
        put_nodes_and_ids_in_map(GO_LABEL, annotation_node_map);
        Pantools.logger.debug("Gathering functional annotation nodes: TIGRFAM.");
        put_nodes_and_ids_in_map(TIGRFAM_LABEL, annotation_node_map);
        Pantools.logger.debug("Gathering functional annotation nodes: PFAM.");
        put_nodes_and_ids_in_map(PFAM_LABEL, annotation_node_map);
        Pantools.logger.debug("Gathering functional annotation nodes: InterPro.");
        put_nodes_and_ids_in_map(INTERPRO_LABEL, annotation_node_map);
        return annotation_node_map;
    }

    /**
     * Example phobius normal output
     *
     *   ID   mRNA-ndhB-2_1
     *   FT   SIGNAL        1     21
     *   FT   DOMAIN        1      4       N-REGION.
     *   FT   DOMAIN        5     16       H-REGION.
     *   FT   DOMAIN       17     21       C-REGION.
     *   FT   DOMAIN       22     36       NON CYTOPLASMIC.
     *   FT   TRANSMEM     37     57
     *   FT   DOMAIN       58     63       CYTOPLASMIC.
     *   FT   TRANSMEM     64     83
     *   FT   DOMAIN       84     88       NON CYTOPLASMIC.
     *   FT   TRANSMEM     89    113
     *   FT   DOMAIN      114    133       CYTOPLASMIC.
     *   FT   TRANSMEM    134    156
     *   FT   DOMAIN      157    167       NON CYTOPLASMIC.
     *   FT   TRANSMEM    168    189
     *   FT   DOMAIN      190    222       CYTOPLASMIC.
     *   FT   TRANSMEM    223    246
     *   FT   DOMAIN      247    253       NON CYTOPLASMIC.
     *   //
     *
     *  example phobius short output
     *   SEQENCE ID                     TM SP PREDICTION
     *   mRNA-ndhB-2_1                   6  Y n5-16c21/22o37-57i64-83o89-113i134-156o168-189i223-246o
     *
     * @param path_to_annotations
     * @param phobius_node_map
     *
     */
    public static void read_phobius_output(String path_to_annotations, HashMap<String, int[]> phobius_node_map) throws IOException {
        try (BufferedReader in = new BufferedReader(new FileReader(path_to_annotations))) {
            String mrna_id = "";
            int speptide_count = 0, trans_counter = 0 ;
            boolean first_line = true;
            boolean short_format = true;
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (first_line) {
                    if (line.startsWith("ID")) {
                        short_format = false;
                    }

                }
                if (short_format) {
                    if (line.startsWith("SEQENCE")) {
                        continue; // the header
                    }
                    String[] line_array = line.split("\\s+"); // split on one or multiple spaces
                    Pantools.logger.trace("{} {} {}", line_array[0], line_array[1], line_array[2]);
                    if (!line_array[1].equals("0") || line_array[2].equals("Y")) {
                        int trans_count2 = Integer.parseInt(line_array[1]);
                        int speptide_count2 = 0;
                        if (line_array[2].equals("Y")) {
                            speptide_count2 = 1;
                        }
                        int[] phobius_counts = new int[]{trans_count2, speptide_count2};
                        phobius_node_map.put(line_array[0], phobius_counts);
                    }
                } else {
                    if (line.startsWith("ID")) {
                        mrna_id = line.replace("ID   ","");
                    }
                    if (line.startsWith("FT   SIGNAL")) {
                        speptide_count ++;
                    }

                    if (line.startsWith("FT   TRANS")) {
                        trans_counter ++;
                    }

                    if (line.startsWith("//")) {
                        Pantools.logger.trace("{} {} {}", mrna_id, trans_counter, speptide_count);
                        int[] phobius_counts = new int[]{trans_counter, speptide_count};
                        if (trans_counter != 0 || speptide_count != 0) {
                            phobius_node_map.put(mrna_id, phobius_counts);
                        }
                        speptide_count = 0;
                        trans_counter = 0;
                    }
                }
            }
        } catch (IOException ioe) {
            throw new IOException("Failed to read: " + path_to_annotations, ioe);
        }
    }

    /**
     * Can read 'short' output files from signal 4.1
     example output of 4.1. always 12 columns
     # name                     Cmax  pos  Ymax  pos  Smax  pos  Smean   D     ?  Dmaxcut    Networks-used
     mRNA-rpl2-3                0.148  20  0.136  20  0.146   3  0.126   0.131 N  0.450      SignalP-noTM
     mRNA-cox2                  0.107  25  0.132  12  0.270   4  0.162   0.148 N  0.450      SignalP-noTM
     mRNA-cox2_1                0.850  17  0.776  17  0.785   2  0.717   0.753 Y  0.500      SignalP-TM

     output of 5.0,  OTHER means there is no signal peptide

     # SignalP-5.0	Organism: Eukarya	Timestamp: 20211122233246
     # ID	Prediction	SP(Sec/SPI)	OTHER	CS Position
     AT3G26880.1	SP(Sec/SPI)	0.998803	0.001197	CS pos: 21-22. VYG-KK. Pr: 0.9807

     # SignalP-5.0	Organism: Eukarya	Timestamp: 20211122233019
     # ID	Prediction	SP(Sec/SPI)	OTHER	CS Position
     mRNA-rpl2-3	OTHER	0.001227	0.998773

     @param path_to_annotations
     @param signalp_map
     */

    public static void read_signalp_output(String path_to_annotations, HashMap<String, String> signalp_map) throws IOException {
        try (BufferedReader in = new BufferedReader(new FileReader(path_to_annotations))) { // read the GFF file
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (line.startsWith("#") || line.equals("")) {
                    // do nothing
                } else {
                    String[] line_array = line.split("\\s+"); // split on one or multiple spaces
                    if (line_array.length == 12) { //output is from signalp 4,
                        if (line_array[9].equals("Y")) {
                            Pantools.logger.trace("{} yes.", line_array[0]);
                            signalp_map.put(line_array[0], "yes");
                        }
                    } else { // signalp 5
                        if (!line_array[1].equals("OTHER")) {
                            Pantools.logger.trace("{} {}", line_array[0], line_array[1]);
                            signalp_map.put(line_array[0], line_array[1]);
                        }
                    }
                }
            }
        } catch (IOException ioe) {
            throw new IOException("Failed to read: " + path_to_annotations, ioe);
        }
    }

    /**
     * Update the skip_array when a genome does not have a functional annotation
     */
    public void check_if_genomes_have_functions() {
        int additional_skipped = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                Node genome_node = genome_nodes.next();
                if (!genome_node.hasProperty("has_functional_annotations")) {
                    int genome_nr = (int) genome_node.getProperty("number");
                    skip_array[genome_nr-1] = true;
                    additional_skipped ++;
                    adj_total_genomes --;
                }
            }
            tx.success();
        }
        if (additional_skipped > 0) {
            System.out.println("\r" + additional_skipped + " additional genomes are skipped because these don't have functional annotations");
        }
    }

    /**
     *
     * @param not_found_ids_mrnas, has only two keys 'function_id' and 'protein_id'
     * @param log_builder
     * @param genome_nr_str a genome number (formatted as string)
     */
    public static void increase_add_fa_log_with_missing_info(HashMap<String, HashSet<String>> not_found_ids_mrnas, StringBuilder log_builder,
                                                             String genome_nr_str) {

        if (not_found_ids_mrnas.containsKey("function_id")) {
            HashSet<String> missing_fa_nodes = not_found_ids_mrnas.get("function_id");
            log_builder.append("\nTerms that were unable to connect because the function node does not exist:\n")
                    .append( missing_fa_nodes.toString().replace("[","").replace("]","").replace(", ",",")).append("\n");
        }

        if (not_found_ids_mrnas.containsKey("protein_id")) {
            HashSet<String> missing_protein_identifiers = not_found_ids_mrnas.get("protein_id");
            log_builder.append("\nmRNA/protein identifiers that were found in the input files but do not exists in genome ")
                    .append(genome_nr_str).append(" of the pangenome:\n")
                    .append(missing_protein_identifiers.toString().replace("[","").replace("]","").replace(", ",",")).append("\n");
        }
        log_builder.append("\n\n");
    }

    /**
     * Add the found phobius properties from the InterProScan output
     * @param phobius_node_map
     * @param mrna_node_map
     * @param not_found_ids_mrnas
     * @param log_builder
     */
    public static void add_phobius_information(HashMap<String, int[]> phobius_node_map, HashMap<String, Node> mrna_node_map,
                                               HashMap<String, HashSet<String>> not_found_ids_mrnas, StringBuilder log_builder) {

        if (phobius_node_map.isEmpty()) {
            return;
        }

        Pantools.logger.info("Starting to add Phobius information to the pangenome.");

        int count = 0, known_count = 0, unknown_count = 0, transmembrane_count = 0, signalpep_count = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (String key : phobius_node_map.keySet()) {
                int[] phobius_values = phobius_node_map.get(key); // [transmembrane counter, second is signal_peptide counter]
                Node mrna_node = mrna_node_map.get(key);

                count += 1;
                if (count % 100 == 0) {
                    System.out.print("\rAdding Phobius information to the pangenome: " + count + " of " + phobius_node_map.size() + " done."); // TODO: replace by actual progress bar
                }

                if (mrna_node == null) {
                    not_found_ids_mrnas.computeIfAbsent("protein_id", s -> new HashSet<>()).add(key);
                    continue;
                }
                if (mrna_node.hasLabel(PHOBIUS_LABEL)) {
                    known_count ++;
                    mrna_node.removeProperty("phobius_effector"); // old versions
                    mrna_node.removeProperty("phobius_secreted_protein"); // old versions
                    mrna_node.removeProperty("phobius_receptor"); // old versions
                    mrna_node.removeProperty("phobius_transmembrane");
                    mrna_node.removeProperty("phobius_signal_peptide");
                } else {
                    mrna_node.addLabel(PHOBIUS_LABEL);
                    unknown_count ++;
                }

                if (phobius_values[1] > 0) {
                    mrna_node.setProperty("phobius_signal_peptide", "yes");
                    signalpep_count ++;
                } else {
                    mrna_node.setProperty("phobius_signal_peptide", "no");
                }
                if (phobius_values[0] > 0) {
                    transmembrane_count ++;
                }
                mrna_node.setProperty("phobius_transmembrane", phobius_values[0]);
            }
            tx.success();
        }
        System.out.println(); // TODO: replace by actual progress bar

        log_builder.append("Phobius information was found for ").append(( known_count + unknown_count)).append(" genes, ").append(known_count).append(" were already known. ")
                .append(signalpep_count).append(" genes have a signal peptide, ").append(transmembrane_count).append(" genes with at least one transmembrane\n");

        Pantools.logger.info("Added {} Phobius to mRNA nodes.", count);
    }

    /**
     * Add the found phobius properties from the InterProScan output
     *
     * Signal peptide value is 'yes' when using interproscan & signalp4 output
     * signalp 5 can have different values: SP(Sec/SPI), LIPO(Sec/SPII) or TAT(Tat/SPI)
     * @param signalp_map
     * @param mrna_node_map
     * @param not_found_ids_mrnas
     * @param log_builder
     */
    public static void add_signalp_information(HashMap<String, String> signalp_map, HashMap<String, Node> mrna_node_map,
                                               HashMap<String, HashSet<String>> not_found_ids_mrnas, StringBuilder log_builder) {
        if (signalp_map.isEmpty()) {
            return;
        }

        Pantools.logger.info("Starting to add SignalP information to the pangenome.");

        int count = 0, known_count = 0, unknown_count = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (String mrna_id : signalp_map.keySet()) {
                Node mrna_node = mrna_node_map.get(mrna_id);
                String value = signalp_map.get(mrna_id);

                count += 1;
                if ((count % 100) == 0) {
                    System.out.print("\rAdding SignalP information to the pangenome: " + count + " of " + signalp_map.size() + " done."); // TODO: replace by actual progress bar
                }

                if (mrna_node == null) {
                    not_found_ids_mrnas.computeIfAbsent("protein_id", s -> new HashSet<>()).add(mrna_id);
                    continue;
                }
                if (mrna_node.hasLabel(SIGNALP_LABEL)) {
                    known_count ++;
                } else {
                    mrna_node.addLabel(SIGNALP_LABEL);
                    unknown_count ++;
                    mrna_node.setProperty("signalp_signal_peptide", value);
                }
            }
            tx.success();
        }
        System.out.println(); // TODO: replace by actual progress bar

        log_builder.append("SignalP information was found for ").append(( known_count + unknown_count )).append(" genes, ").append(known_count).append(" were already known\n");

        Pantools.logger.info("Added {} SignalP to mRNA nodes.", count);
    }

    /*
     Updates the "frequency" (array) and "total_frequency" (integer) properties in all functional annotation nodes.
     The frequency is influenced by --skip/--reference and --annotations-file.
    */
    public static void update_all_FA_nodes_frequency() {
        update_fa_nodes_frequency(GO_LABEL, RelTypes.has_go);
        update_fa_nodes_frequency(INTERPRO_LABEL, RelTypes.has_interpro);
        update_fa_nodes_frequency(PFAM_LABEL, RelTypes.has_pfam);
        update_fa_nodes_frequency(TIGRFAM_LABEL, RelTypes.has_tigrfam);
    }

    /**
     Updates the "frequency" (array) and "total_frequency" (integer) properties of the nodes of one type of functional annotation (go, interpro, pfam or tigrfam).
     * @param node_label
     * @param reltype
     */
    public static void update_fa_nodes_frequency(Label node_label, RelationshipType reltype) {

        try (Transaction tx = GRAPH_DB.beginTx()) { // start transaction
            ResourceIterator<Node> function_nodes = GRAPH_DB.findNodes(node_label);
            int total_nodes = (int) count_nodes(node_label); // count nodes with a specific label
            int node_counter = 0;
            while (function_nodes.hasNext()) {
                int total_frequency = 0;
                Node function_node = function_nodes.next();
                node_counter ++;
                Iterable<Relationship> all_relations = function_node.getRelationships(reltype);
                int[] frequency_array = new int[total_genomes];
                if (node_counter % 100 == 0 || node_counter == total_nodes || node_counter < 100) {
                    System.out.print("\rUpdating 'frequency' property in '" + node_label + "' nodes: " + node_counter + "/" + total_nodes); // TODO: replace by actual progress bar
                }
                for (Relationship rel : all_relations) {
                    Node mrna_node = rel.getStartNode();
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    if (!PROTEOME) { // skip mRNAs from annotations that are not currently selected.
                        String annotation_id = (String) mrna_node.getProperty("annotation_id");
                        if (!annotation_identifiers.contains(annotation_id)) {
                            continue;
                        }
                    } else if (skip_array[genome_nr-1]) { // proteome
                        continue;
                    }
                    frequency_array[genome_nr-1] += 1;
                    total_frequency ++;
                }
                function_node.removeProperty("frequency");
                function_node.removeProperty("total_frequency");
                function_node.setProperty("frequency", frequency_array);
                function_node.setProperty("total_frequency", total_frequency);
            }
            tx.success();
        }
        System.out.println(); // TODO: replace by actual progress bar

    }

    /**
     *
     * @return
     */
    public static ArrayList<Node> retrieve_go_nodes_via_name_or_node() {
        Pantools.logger.debug("SELECTED_NAME: {}", SELECTED_NAME);
        Pantools.logger.debug("NODE_ID: {}", NODE_ID);

        ArrayList<Node> go_nodes = new ArrayList<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            if (NODE_ID != null && SELECTED_NAME != null) { // both --node and --name were given by user
                Pantools.logger.error("Use either the '--name' or '--node' command.");
                System.exit(1);
            } else if (NODE_ID == null && SELECTED_NAME == null) {
                Pantools.logger.error("Provide GO nodes via --node or --name.");
                System.exit(1);
            } else if (NODE_ID != null) { // --node was given by user
                NODE_ID = NODE_ID.replace(" ","").replace(",,",",");
                String[] node_array = NODE_ID.split(",");
                for (String node_id_str : node_array) {
                    long node_id = 0L;
                    try {
                        node_id = Long.parseLong(node_id_str);
                    } catch (NumberFormatException nfe) {
                        Pantools.logger.info("'{}' is not a number.", node_id_str);
                        continue;
                    }
                    Node go_node = GRAPH_DB.getNodeById(node_id);
                    go_nodes.add(go_node);
                }
            } else if (SELECTED_NAME != null) { // --name was given by user
                SELECTED_NAME = SELECTED_NAME.replace(" ","").replace(",,",",");

                HashMap<String, Node> goNodeMap = new HashMap<>();
                try (ResourceIterator<Node> allGoNodes = GRAPH_DB.findNodes(GO_LABEL)) {
                    while (allGoNodes.hasNext()) {
                        Node goNode = allGoNodes.next();
                        String id = (String) goNode.getProperty("id");
                        goNodeMap.put(id, goNode);
                    }
                }

                for (String goId : SELECTED_NAME.split(",")) {
                    Node goNode = goNodeMap.get(goId);
                    Pantools.logger.debug("Found node '{}' for id '{}'", goNode, goId);
                    if (goNode == null) {
                        Pantools.logger.warn("'{}' does not exist within the database", goId);
                        continue;
                    }
                    go_nodes.add(goNode);
                }
            }
            boolean stop = false;
            if (go_nodes.isEmpty()) {
                Pantools.logger.warn("No GO nodes were found.");
                stop = true;
            }
            for (Node go_node : go_nodes) {
                boolean correct = test_if_correct_label(go_node, GO_LABEL, false);
                if (!correct) {
                    Pantools.logger.warn("{} is not GO node.", go_node.getId());
                    stop = true;
                }
            }
            if (stop) {
                throw new RuntimeException("Failed to retrieve GO nodes.");
            }
            tx.success();
        }
        return go_nodes;
    }

    /**
     *
     * @param annotation_node_map
     * @param node_label
     * @param reltype has_tigrfam, has_pfam, etc
     * @param mrna_node_map
     * @param annotation_node_id_map
     * @param log_builder
     * @param type
     * @param genomeNrStr a genome number (formatted as string)
     * @param not_found_ids_mrnas
     */
    public static void connect_mrnas_to_function(HashMap<String, HashMap<String, Integer[]>> annotation_node_map, Label node_label,
                                                 RelationshipType reltype, HashMap<String, Node> mrna_node_map,
                                                 HashMap<String, Node> annotation_node_id_map, StringBuilder log_builder,
                                                 String type, String genomeNrStr, HashMap<String, HashSet<String>> not_found_ids_mrnas,
                                                 ArrayList<String> selectedLabels) {

        if (annotation_node_map.isEmpty() || !selectedLabels.contains(type)) {
            return;
        }

        Pantools.logger.info("Starting to connect {} to mRNAs for genome {}", type, genomeNrStr);

        Transaction tx = GRAPH_DB.beginTx();
        try {
            int counter = 0, skipped_counter = 0;
            String label_str = node_label.toString();
            label_str = label_str.replace("_label","");
            HashSet<Node> all_nodes = new HashSet<>();
            int total_node_counter = 0, transactions = 0;
            for (String function_id : annotation_node_map.keySet()) {
                HashMap<String, Integer[]> domainProteinPositions = annotation_node_map.get(function_id);
                Node annotation_node = annotation_node_id_map.get(function_id);
                if (annotation_node == null) { // a functional annotation node with this id does not exist
                    not_found_ids_mrnas.computeIfAbsent("function_id", s -> new HashSet<>()).add(function_id);
                    continue;
                }
                long anno_node_id = annotation_node.getId();
                total_node_counter ++;
                if (total_node_counter < 10 || total_node_counter % 100 == 0 || total_node_counter == annotation_node_map.size()) {
                    System.out.print("\rAdding functional annotations. Genome " + genomeNrStr + ", connecting to " +
                            type + " nodes: " + total_node_counter + " / " + annotation_node_map.size()); // TODO: replace by actual progress bar
                }

                for (Map.Entry<String, Integer[]> domainProteinPosition : domainProteinPositions.entrySet()) {
                    String gene_name = domainProteinPosition.getKey();
                    Integer[] proteinPositions = domainProteinPosition.getValue();
                    int proteinStartPosition = proteinPositions[0];
                    int proteinEndPosition = proteinPositions[1];
                    Node mrna_node = mrna_node_map.get(gene_name);
                    if (mrna_node == null) {
                        mrna_node = mrna_node_map.get("gene_" + gene_name);
                    }
                    if (mrna_node == null) {
                        not_found_ids_mrnas.computeIfAbsent("protein_id", s -> new HashSet<>()).add(gene_name);
                        continue;
                    }
                    boolean skip = false;
                    Iterable<Relationship> current_rels = mrna_node.getRelationships(reltype);
                    for (Relationship rel : current_rels) {
                        Node end_node = rel.getEndNode();
                        Pantools.logger.trace("{} {}", rel, annotation_node);
                        long new_anno_node_id = end_node.getId();
                        if (new_anno_node_id == anno_node_id) {
                            skip = true;
                            if (all_nodes.contains(end_node)) { // was already counted as skip earlier
                                skipped_counter --;
                            } else {
                                all_nodes.add(end_node);
                            }
                        }
                    }
                    if (!skip) {
                        Relationship functionRel = mrna_node.createRelationshipTo(annotation_node, reltype);
                        functionRel.setProperty("aa_start", proteinStartPosition);
                        functionRel.setProperty("aa_end", proteinEndPosition);
                        counter ++;
                        transactions ++;
                    } else { // mrna already has the function connected
                        skipped_counter ++;
                    }
                    if (transactions > 10000) { // commit changes every 10k transactions
                        tx.success();
                        tx.close();
                        tx = GRAPH_DB.beginTx();
                        transactions = 0;
                    }
                }
            }
            System.out.println(); // TODO: replace by actual progress bar

            log_builder.append(total_node_counter).append(" distinct ").append(label_str).append(" found. ").append(counter)
                    .append(" ").append(label_str).append(" nodes connected to genes, ").append(skipped_counter).append(" were already connected\n");
            tx.success();
        } finally {
            tx.close();
        }

        Pantools.logger.info("Connected {} {} to mRNAs.", annotation_node_map.size(), type);
    }

    /**
     *
     */
    public void compare_go() {
        Pantools.logger.info("Checking how similar the selected GO terms are.");
        check_database(); // starts up the graph database if needed
        StringBuilder final_output = new StringBuilder();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            int total_go_nodes = (int) count_nodes(GO_LABEL); // count nodes with a specific label
            if (total_go_nodes == 0) {
                Pantools.logger.error("No 'GO' nodes are present in the pangenome.");
                System.exit(1);
            }
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        ArrayList<Node> go_nodes = retrieve_go_nodes_via_name_or_node();
        if (go_nodes.size() != 2) {
            Pantools.logger.error("compare_go can only compare two GO nodes (not {} per run).", go_nodes.size());
            Pantools.logger.error("We found these GO nodes: {}", go_nodes.toString());
            return;
        }
        boolean found = false;
        try (Transaction tx = GRAPH_DB.beginTx()) {
            Node node1 = go_nodes.get(0);
            Node node2 = go_nodes.get(1);
            String go_id1 = append_go_node_information(node1, final_output);
            String go_id2 = append_go_node_information(node2, final_output);
            String all_go = node1.toString();
            int counter = 1;
            StringBuilder all_go_builder = new StringBuilder();
            StringBuilder output_builder = new StringBuilder("#Hierarchy of " + go_id1 + " (1)\n");
            boolean stop = false;
            while (stop == false) {
                String new_all_go = print_go_layer(all_go, counter, all_go_builder, output_builder);
                if (new_all_go.length() < 2) {
                    stop = true;
                }
                counter ++;
                all_go = new_all_go;
            }
            String output_str = output_builder.toString();
            String[] output_array = output_str.split("Layer ");
            int line_count = 0;
            for (String line : output_array) {
                if (line.contains(go_id2)) {
                    final_output.append("FOUND! ").append(go_id2).append(" (2) was found in the hierarchy of ").append(go_id1)
                            .append(" (1) on layer ").append(line_count).append("!\n\n");
                    found = true;
                }
                line_count ++;
            }
            String output1 = output_builder.toString();
            String go_str1 = all_go_builder.toString();
            go_str1 = go_str1.replaceFirst(".$","");
            String[] go_array1 = go_str1.split(",");

            output_builder = new StringBuilder("\n#Hierarchy of " + go_id2 + " (2)\n");
            all_go_builder = new StringBuilder();
            all_go = node2.toString();
            counter = 1;

            stop = false;
            while (stop == false) {
                String new_all_go = print_go_layer(all_go, counter, all_go_builder, output_builder);
                if (new_all_go.length() < 2) {
                    stop = true;
                }
                counter ++;
                all_go = new_all_go;
            }
            output_str = output_builder.toString();
            output_array = output_str.split("Layer ");
            String go_str2 = all_go_builder.toString();
            go_str2 = go_str2.replaceFirst(".$","");
            String[] go_array2 = go_str2.split(",");
            ArrayList<String> go_list2 = new ArrayList<>(Arrays.asList(go_array2));
            line_count = 0;
            for (String line: output_array) {
                if (line.contains(go_id1)) {
                    final_output.append("FOUND! ").append(go_id1).append(" (1) was found in the hierarchy of ").append(go_id2)
                            .append(" (2) on layer ").append(line_count).append("!\n\n");
                    found = true;
                }
                line_count ++;
            }

            StringBuilder shared_terms = new StringBuilder("Shared GO terms:\n ");
            for (String go_term1 : go_array1) {
                if (go_list2.contains(go_term1)) {
                    shared_terms.append(go_term1).append(",");
                    found = true;
                }
            }

            if (!found) {
                final_output.append("The GO terms do NOT share functionality\n\n");
            }

            final_output.append(output1).append(" ").append(output_str);
            if (found) {
                String shared_terms_str = shared_terms.toString().replaceFirst(".$","");
                final_output.append(shared_terms_str);
            }

            final_output.append("\n\nTerms found in hierarchy above ").append(go_id1).append(":\n ").append(go_str1)
                    .append("\n\nTerms found in hierarchy above ").append(go_id2).append(":\n ").append(go_str2);
            tx.success();
        }
        write_SB_to_file_in_DB(final_output, "function/compare_go.txt");
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}function/compare_go.txt", WORKING_DIRECTORY);
    }

    /**
     *
     * @param go_node
     * @param output
     * @return
     */
    public static String append_go_node_information(Node go_node, StringBuilder output) {
        test_if_correct_label(go_node, GO_LABEL, true);
        long id_long = go_node.getId();
        String id = (String) go_node.getProperty("id");
        String name = (String) go_node.getProperty("name");
        String category = (String) go_node.getProperty("category");
        String description = (String) go_node.getProperty("description");
        description = description.replace("def:","");
        output.append(id)
                .append("\nName: ").append(name)
                .append("\nCategory: ¨").append(category)
                .append("\nDescription:").append(description)
                .append("\nPanTools node identifier: ").append(id_long).append("\n\n");
        return id;
    }

    /**
     *
     * @param all_go
     * @param layer
     * @param all_go_builder
     * @param go_tree
     * @return
     */
    public static String get_go_hierarchy(String all_go, int layer, StringBuilder all_go_builder, StringBuilder go_tree) {
        String[] node_array = all_go.split(",");
        Set<String> numbersSet = new HashSet<>(Arrays.asList(node_array));
        String[] uniqueNumbers = numbersSet.toArray(new String[0]);
        String new_all_go = "";
        int counter = 0;
        int last_node_counter = 0;
        for (String node_str: uniqueNumbers) {
            node_str = node_str.replaceAll("[^\\d.]","");
            long node_id_long = Long.parseLong(node_str);
            Node new_target_node = GRAPH_DB.getNodeById(node_id_long);
            Iterable<Relationship> all_relations2 = new_target_node.getRelationships(Direction.OUTGOING, RelTypes.is_a);
            boolean printed = false;
            for (Relationship rel2: all_relations2) {
                Node parent_node2 = rel2.getEndNode();
                Node child_node = rel2.getStartNode();
                String go_id = (String) parent_node2.getProperty("id");
                String parent_name = (String) parent_node2.getProperty("name");
                String child_name = (String) child_node.getProperty("name");
                String rel_str = "\"" + parent_name + "\" -> \"" + child_name + "\";";
                String go_tree_str = go_tree.toString();
                if (!go_tree_str.contains(rel_str)) {
                    go_tree.append("  ").append(rel_str).append("\n");
                }

                String temp_str = all_go_builder.toString();
                if (!temp_str.contains(go_id)) {
                    all_go_builder.append(go_id).append(", ");
                }
                new_all_go += parent_node2 + ",";
                printed = true;
                counter ++;
                if (parent_name.equals("biological_process") || parent_name.equals("molecular_function") || parent_name.equals("cellular_component")) {
                    last_node_counter ++;
                }
            }
            if (printed == true) {

            }
        }
        if (counter == last_node_counter) {
            new_all_go = "";
        }
        return new_all_go;
    }

    /**
     *
     * @param all_go
     * @param all_go_list
     * @return
     */
    public static String get_go_hierarchy_nodes(String all_go, HashSet<Node> all_go_list) {
        String[] node_array = all_go.split(",");
        Set<String> numbersSet = new HashSet<>(Arrays.asList(node_array));
        String[] uniqueNumbers = numbersSet.toArray(new String[0]);
        String new_all_go = "";
        int counter = 0;
        int last_node_counter = 0;
        for (String node_str: uniqueNumbers) {
            node_str = node_str.replaceAll("[^\\d.]","");
            long node_id_long = Long.parseLong(node_str);
            Node new_target_node = GRAPH_DB.getNodeById(node_id_long);
            Iterable<Relationship> all_relations2 = new_target_node.getRelationships(Direction.OUTGOING, RelTypes.is_a);
            boolean printed = false;
            for (Relationship rel2: all_relations2) {
                Node parent_node2 = rel2.getEndNode();
                Node child_node = rel2.getStartNode();
                String parent_name = (String) parent_node2.getProperty("name");
                String child_name = (String) child_node.getProperty("name");
                all_go_list.add(parent_node2);
                new_all_go += parent_node2 + ",";
                printed = true;
                counter ++;
                if (parent_name.equals("biological_process") || parent_name.equals("molecular_function") || parent_name.equals("cellular_component")) {
                    last_node_counter ++;
                }
            }
            if (printed == true) {

            }
        }
        if (counter == last_node_counter) {
            new_all_go = "";
        }
        return new_all_go;
    }

    /**
     *
     */
    public void show_go() {
        Pantools.logger.info("Retrieving the GO hierarchy of provided 'GO' node(s).");
        check_database(); // starts up the graph database if needed
        try (Transaction tx = GRAPH_DB.beginTx()) {
            int total_go_nodes = (int) count_nodes(GO_LABEL); // count nodes with a specific label
            if (total_go_nodes == 0) {
                Pantools.logger.error("No 'GO' nodes are present in the pangenome.");
                System.exit(1);
            }
            tx.success();
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Unable to start the database.");
            System.exit(1);
        }

        ArrayList<Node> go_nodes = retrieve_go_nodes_via_name_or_node();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            StringBuilder all_go_builder = new StringBuilder();
            StringBuilder output_builder = new StringBuilder();
            int node_counter = 0;
            for (Node go_node : go_nodes) {
                int layer_counter = 1;
                node_counter ++;
                System.out.print("\rRetrieving GO node information: " + node_counter + "/" + go_nodes.size());
                String go_description = (String) go_node.getProperty("description");
                String go_name = (String) go_node.getProperty("name");
                String go_id = (String) go_node.getProperty("id");
                long go_id_l = go_node.getId();
                output_builder.append("#").append(go_id).append("\nPanTools node identifier: ").append(go_id_l).append(" ")
                        .append("\nName: ").append(go_name)
                        .append("\nDescription: ").append(go_description).append("\n\n");
                check_connected_mrna_nodes(RelTypes.has_go, go_node, output_builder, "GO");
                check_go_layer_below(go_node, output_builder);
                String all_go = go_node.toString();
                if (go_name.equals("biological_process") || go_name.equals("molecular_function") || go_name.equals("cellular_component")) {
                    output_builder.append("The selected node is on top of the GO hierarchy\n\n");
                } else {
                    boolean stop = false;
                    while (stop == false) {
                        String new_all_go = print_go_layer(all_go, layer_counter, all_go_builder, output_builder);
                        if (new_all_go.length() < 2) {
                            stop = true;
                        }
                        layer_counter ++;
                        all_go = new_all_go;
                    }
                    output_builder.append("All GO's found higher in the hierarchy:\n")
                            .append(all_go_builder.toString().replaceFirst(".$","")).append("\n");
                }
                output_builder.append("\n");
            }
            write_SB_to_file_in_DB(output_builder, "/function/show_go.txt");
            tx.success();
        }
        Pantools.logger.info("Output written to:");
        Pantools.logger.info(" {}function/show_go.txt", WORKING_DIRECTORY);
    }

    /**
     *
     * @param target_node
     * @param output_builder
     */
    public static void check_go_layer_below(Node target_node, StringBuilder output_builder) {
        output_builder.append("Layer below\n");
        Iterable<Relationship> all_relations = target_node.getRelationships(Direction.INCOMING);
        for (Relationship rel: all_relations) {
            Node node1 = rel.getStartNode();
            Iterable<Label> labels = node1.getLabels();
            for (Label label1 : labels) {
                if (label1.equals(GO_LABEL)) {
                    Node go_node = rel.getStartNode();
                    String go_id = (String) go_node.getProperty("id");
                    String go_name = (String) go_node.getProperty("name");
                    output_builder.append(rel).append(" ").append(go_id).append(", ").append(go_name).append("\n");
                }
            }
        }
        output_builder.append("\n");
    }

    /**
     *
     * @param all_go
     * @param layer
     * @param all_go_builder
     * @param output_builder
     * @return
     */
    public static String print_go_layer(String all_go, int layer, StringBuilder all_go_builder, StringBuilder output_builder) {
        output_builder.append("Layer ").append(layer).append("\n");
        String[] node_array = all_go.split(",");
        Set<String> numbersSet = new HashSet<>(Arrays.asList(node_array));
        String[] uniqueNumbers = numbersSet.toArray(new String[0]);
        String new_all_go = "";
        int counter = 0;
        int last_node_counter = 0;
        for (String node_str: uniqueNumbers) {
            node_str = node_str.replaceAll("[^\\d.]","");
            long node_id_long = Long.parseLong(node_str);
            Node new_target_node = GRAPH_DB.getNodeById(node_id_long);
            Iterable<Relationship> all_relations2 = new_target_node.getRelationships(Direction.OUTGOING);
            boolean printed = false;
            for (Relationship rel2: all_relations2) {
                Node parent_node2 = rel2.getEndNode();
                String go_id = (String) parent_node2.getProperty("id");
                String go_name = (String) parent_node2.getProperty("name");
                Pantools.logger.trace("{} {}, {}", rel2, go_id, go_name);
                output_builder.append(" ").append(rel2).append(" ").append(go_id).append(", ").append(go_name).append("\n");
                String temp_str = all_go_builder.toString();
                if (!temp_str.contains(go_id)) {
                    all_go_builder.append(go_id).append(",");
                }
                new_all_go += parent_node2 + ",";
                printed = true;
                counter ++;
                if (go_name.equals("biological_process") || go_name.equals("molecular_function") || go_name.equals("cellular_component")) {
                    last_node_counter ++;
                }
            }
            if (printed == true) {
                output_builder.append("\n");
            }
        }
        if (counter == last_node_counter) {
            new_all_go = "";
        }
        return new_all_go;
    }

    /**
     * This function is required in case a different id was give to a gene by antiSMASH
     * Important, This only works on antismash 4 output directory
     * @param original_input_file path to a antismash geneclusters input file
     * @return
     */
    public static HashMap<String, String> retrieve_gene_ids_for_clusters(String original_input_file) {
        HashMap<String, String> gene_ids = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            String[] input_array = original_input_file.split("/");
            StringBuilder input_directory = new StringBuilder();
            String gbk_final_file = "";
            for (int i = 0; i <= input_array.length - 2; i++) {
                input_directory.append(input_array[i]).append("/");
            }
            File folder = new File(input_directory.toString());
            String[] files = folder.list();
            assert files != null;
            for (String file : files) {
                if (file.endsWith(".final.gbk")) {
                    gbk_final_file = file;
                }
            }
            if (gbk_final_file.equals("")) { // no file present
                return null;
            }

            try (BufferedReader in = new BufferedReader(new FileReader(input_directory + gbk_final_file))) {
                String locus = "";
                while (in.ready()) {
                    String line = in.readLine().trim();
                    if (line.contains("locus_tag")) {
                        String[] line_array = line.split("locus_tag=");
                        locus = line_array[1].replace("\"", "");
                    }
                    if (line.contains("/ID=")) {
                        String[] line_array = line.split("/ID=");
                        String id = line_array[1].replace("\"", "");
                        gene_ids.put(locus, id);
                    }
                }
            } catch (IOException ioe) {
                Pantools.logger.info("Failed to read: {}", gbk_final_file);
            }
            tx.success();
        }
        return gene_ids;
    }

    /**
     * @param function_label
     * @param function_reltype
     * @return a long array where the first position holds all nodes, second all connections, the rest are the connections per genome
     */
    public static long[] count_function_nodes_per_genomes(Label function_label, RelTypes function_reltype) {
        long[] total_array = new long[total_genomes];
        ResourceIterator<Node> all_function_nodes = GRAPH_DB.findNodes(function_label);
        while (all_function_nodes.hasNext()) {
            Node function_node = all_function_nodes.next();
            long[] frequency_array = new long[total_genomes];
            Iterable<Relationship> all_relations = function_node.getRelationships(function_reltype);
            for (Relationship rel : all_relations) {
                Node mrna_node = rel.getStartNode();
                int genome_nr = (int) mrna_node.getProperty("genome");
                if (skip_array[genome_nr-1]) {
                    continue;
                }
                if (!PROTEOME) { // skip the mRNAs from annotations that are not currently selected.
                    String annotation_id = (String) mrna_node.getProperty("annotation_id");
                    if (!annotation_identifiers.contains(annotation_id)) {
                        continue;
                    }
                }
                if (frequency_array[genome_nr-1] == 0) {
                    total_array[genome_nr-1] += 1;
                }
                frequency_array[genome_nr-1] += 1;
            }
        }
        Pantools.logger.trace("{} {}", total_array.length, Arrays.toString(total_array));
        return total_array;
    }

    /*
     Finds the shared functions between two mrna's and put them on the 'is_similar' relationship if this exists
     GO: (and pfam,interpro and tigrfam): a property called 'shared_go'. contains an array with [shared terms, total terms].
     COG: 'shared_cog' contains a boolean
     Phobius: 3 different properties. "shared_phobius_receptor", "shared_phobius_secreted", "shared_phobius_transm_domains"
        "shared_phobius_receptor" has the states for both mrna's separated by a dash: no_receptor or receptor.  For example, no_receptor/receptor, receptor/receptor
        "shared_phobius_secreted" has the states for both mrna's separated by a dash: not_secreted or secreted.  For example, not_secreted/secreted, secreted/secreted
        "shared_phobius_transm_domains" contains an array with [ lowest number of transmembrane domains, highest number].

     */
    public static void find_shared_functions_between_mrnas() {
        Pantools.logger.info("Starting to find shared functions between mRNAs.");

        Transaction tx = GRAPH_DB.beginTx();
        try {
            ArrayList<Node> group_list = new ArrayList<>();
            long[] pfam_per_genome = count_function_nodes_per_genomes(PFAM_LABEL, RelTypes.has_pfam);
            long[] tigrfam_per_genome = count_function_nodes_per_genomes(TIGRFAM_LABEL, RelTypes.has_tigrfam);
            long[] go_per_genome = count_function_nodes_per_genomes(GO_LABEL, RelTypes.has_go);
            long[] interpro_per_genome = count_function_nodes_per_genomes(INTERPRO_LABEL, RelTypes.has_interpro);
            ResourceIterator<Node> hmgroup_nodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            while (hmgroup_nodes.hasNext()) {
                Node hmgroup_node = hmgroup_nodes.next();
                group_list.add(hmgroup_node);
            }
            hmgroup_nodes.close();
            long total_hm = count_nodes(HOMOLOGY_GROUP_LABEL); // count nodes with a specific label
            int hm_counter = 0;
            long gene_count = 0;
            for (Node hmgroup_node : group_list) {
                hm_counter ++;
                if (hm_counter % 100 == 0 || hm_counter == total_hm) {
                    System.out.print("\rFinding shared functions between similar sequences: Homology group " + hm_counter + "/" + total_hm); // TODO: replace by actual progress bar
                }
                int num_members = (int) hmgroup_node.getProperty("num_members");
                gene_count += num_members;
                if (num_members == 1) {
                    continue;
                }
                Iterable<Relationship> relations = hmgroup_node.getRelationships();
                HashMap<String, ArrayList<Node>> functions_per_mrna = new HashMap<>();
                for (Relationship rel : relations) { // get function properties of all mrnas in homology group
                    Node mrna_node = rel.getEndNode();
                    find_functions_of_mrna(mrna_node, RelTypes.has_pfam, functions_per_mrna, "pfam");
                    find_functions_of_mrna(mrna_node, RelTypes.has_go, functions_per_mrna, "go");
                    find_functions_of_mrna(mrna_node, RelTypes.has_interpro, functions_per_mrna, "interpro");
                    find_functions_of_mrna(mrna_node, RelTypes.has_tigrfam, functions_per_mrna, "tigrfam");
                }
                for (Relationship rel : relations) {
                    Node mrna_node = rel.getEndNode();
                    Iterable<Relationship> is_similar_rels = mrna_node.getRelationships(RelTypes.is_similar_to); //TODO: USE A DIFFERENT RELATIONSHIP
                    find_shared_functions(mrna_node, is_similar_rels, functions_per_mrna, "pfam", pfam_per_genome);
                    find_shared_functions(mrna_node, is_similar_rels, functions_per_mrna, "go", go_per_genome);
                    find_shared_functions(mrna_node, is_similar_rels, functions_per_mrna, "interpro", interpro_per_genome);
                    find_shared_functions(mrna_node, is_similar_rels, functions_per_mrna, "tigrfam", tigrfam_per_genome);
                    find_shared_cog(mrna_node, is_similar_rels);
                    find_shared_phobius(mrna_node, is_similar_rels);
                }
                if (gene_count > 25000) { // commit changes after every 25k genes
                    tx.success(); // transaction successful, commit changes
                    tx.close();
                    tx = GRAPH_DB.beginTx(); // start a new database transaction
                    gene_count = 0;
                }
            }
            System.out.println(); // TODO: replace by actual progress bar

            tx.success(); // transaction successful, commit changes
        } finally {
            tx.close();
        }

        Pantools.logger.info("Finished finding shared functions between mRNAs.");
    }

    /**
     *
     * @param mrna_node1
     * @param function_rel
     * @param functions_per_mrna
     * @param function_type
     */
    public static void find_functions_of_mrna(Node mrna_node1, RelTypes function_rel, HashMap<String, ArrayList<Node>> functions_per_mrna, String function_type) {
        Iterable<Relationship> function_rels = mrna_node1.getRelationships(function_rel);
        for (Relationship rel : function_rels) {
            Node function_node = rel.getEndNode();
            functions_per_mrna.computeIfAbsent(mrna_node1 + "" + function_type, s -> new ArrayList<>()).add(function_node);
        }
    }

    /**
     *
     * @param mrna_node
     * @param is_similar_rels
     */
    public static void find_shared_phobius(Node mrna_node, Iterable<Relationship> is_similar_rels) {
        String[] properties1_array = get_phobius_properties_in_string(mrna_node);
        long mrna1_id = mrna_node.getId();
        boolean has_label1 = mrna_node.hasLabel(PHOBIUS_LABEL);
        boolean forward = true;
        for (Relationship rel : is_similar_rels) {
            long mrna2_id = rel.getStartNodeId();
            Node mrna_node2;
            if (mrna2_id == mrna1_id) {
                mrna_node2 = rel.getEndNode();
                //mrna2_id = mrna_node2.getId();
            } else {
                forward = false;
                mrna_node2 = rel.getStartNode();
            }

            boolean has_label2 = mrna_node.hasLabel(PHOBIUS_LABEL);
            if (!has_label1 && !has_label2) {
                continue;
            }
            String[] properties2_array = get_phobius_properties_in_string(mrna_node2);
            rel.removeProperty("phobius_shared"); // old, can be removed in stable version
            rel.removeProperty("shared_phobius_receptor");
            rel.removeProperty("shared_phobius_secreted");
            rel.removeProperty("shared_phobius_transm_domains");
            int[] shared_transmembrane = new int[]{Integer.parseInt(properties1_array[2]), Integer.parseInt(properties2_array[2])};
            if (forward) {
                rel.setProperty("shared_phobius_secreted", properties1_array[0] + "/" + properties2_array[0]);
                rel.setProperty("shared_phobius_receptor", properties1_array[1] + "/" + properties2_array[1]);
                rel.setProperty("shared_phobius_transm_domains", shared_transmembrane);
            } else {
                shared_transmembrane[0] = Integer.parseInt(properties2_array[2]);
                shared_transmembrane[1] = Integer.parseInt(properties1_array[2]);
                rel.setProperty("shared_phobius_secreted", properties2_array[0] + "/" + properties1_array[0]);
                rel.setProperty("shared_phobius_receptor", properties2_array[1] + "/" + properties1_array[1]);
                rel.setProperty("shared_phobius_transm_domains", shared_transmembrane);
            }
        }
    }

    /**
     *
     * @param mrna_node
     * @return
     */
    public static String[] get_phobius_properties_in_string(Node mrna_node) {
        boolean has_label = mrna_node.hasLabel(PHOBIUS_LABEL);
        if (!has_label) {
            return new String[]{"not_secreted","no_receptor","0"};
        }
        String phobius_properties = "";
        boolean secreted = (boolean) mrna_node.getProperty("phobius_secreted_protein");
        if (secreted) {
            phobius_properties += "secreted,";
        } else {
            phobius_properties += "not_secreted,";
        }
        boolean receptor = (boolean) mrna_node.getProperty("phobius_receptor");
        if (receptor) {
            phobius_properties += "receptor,";
        } else {
            phobius_properties += "no_receptor,";
        }
        int transmembrane = (int) mrna_node.getProperty("phobius_transmembrane");
        phobius_properties += transmembrane;
        return phobius_properties.split(",");
    }

    /**
     * If there are no COG functions present, do not add the 'shared_cog' property
     * @param mrna_node1
     * @param is_similar_rels
     */
    public static void find_shared_cog(Node mrna_node1, Iterable<Relationship> is_similar_rels) {
        if (!mrna_node1.hasLabel(COG_LABEL)) {
            return;
        }
        long mrna1_id = mrna_node1.getId();
        String cog_category1 = (String) mrna_node1.getProperty("COG_category");
        for (Relationship rel: is_similar_rels) {
            long mrna2_id = rel.getStartNodeId();
            Node mrna_node2;
            if (mrna2_id == mrna1_id) {
                mrna_node2 = rel.getEndNode();
            } else {
                mrna_node2 = rel.getStartNode();
            }
            if (!mrna_node2.hasLabel(COG_LABEL)) {
                return;
            }

            String cog_category2 = (String) mrna_node2.getProperty("COG_category");
            if (cog_category1.equals(cog_category2)) {
                rel.removeProperty("shared_cog");
                rel.setProperty("shared_cog", true);
                Pantools.logger.trace("{} shared cog.", rel);
            } else if (!cog_category1.equals(cog_category2)) {
                rel.removeProperty("shared_cog");
                rel.setProperty("shared_cog", false);
            }
        }
    }

    /**
     *
     * @param mrna_node1
     * @param is_similar_rels
     * @param functions_per_mrna
     * @param function_type
     * @param total_per_genome
     */
    public static void find_shared_functions(Node mrna_node1, Iterable<Relationship> is_similar_rels, HashMap<String, ArrayList<Node>> functions_per_mrna,
                                             String function_type, long[] total_per_genome) {
        int genome_nr1 = (int) mrna_node1.getProperty("genome");
        long mrna1_id = mrna_node1.getId();
        ArrayList<Node> functions1 = functions_per_mrna.get(mrna_node1 + "" + function_type);
        if (total_per_genome[genome_nr1-1] == 0) {
            return;
        }
        for (Relationship rel : is_similar_rels) {
            long mrna2_id = rel.getStartNodeId();
            Node mrna_node2;
            if (mrna2_id == mrna1_id) {
                mrna_node2 = rel.getEndNode();
            } else {
                mrna_node2 = rel.getStartNode();
            }
            int genome_nr2 = (int) mrna_node2.getProperty("genome");
            if (genome_nr1 == genome_nr2) {
                continue;
            }
            if (total_per_genome[genome_nr2-1] == 0) {
                return;
            }
            ArrayList<Node> functions2 = functions_per_mrna.get(mrna_node2 + "" + function_type);
            int total = 0;
            int shared = 0;
            if (functions1 != null && functions2 != null) {
                for (Node func1 : functions1) {
                    if (functions2.contains(func1)) {
                        shared ++;
                    }
                    total ++;
                }
                for (Node func2 : functions2) {
                    if (!functions1.contains(func2)) {
                        total ++;
                    }
                }
            } else if (functions1 != null && functions2 == null) {
                total = functions1.size();
            } else if (functions1 == null && functions2 != null) {
                total = functions2.size();
            }

            int[] shared_array = {shared, total};
            rel.removeProperty("shared_" + function_type);
            rel.setProperty("shared_" + function_type, shared_array);
            if (total != shared) {
                Pantools.logger.trace("  {} {} vs {} -> {}/{} {} {}", mrna_node2, genome_nr1, genome_nr2, shared, total, functions1, functions2);
            } else {
                Pantools.logger.trace("  {} {} vs {} -> {}/{}", mrna_node2, genome_nr1, genome_nr2, shared, total);
            }
        }
    }
}
