package nl.wur.bif.pantools.analysis.classification;

import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Order the values of a matrix file created by PanTools.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "order_matrix", sortOptions = false)
public class OrderMatrixCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "matrix-file", index = "0+")
    @InputFile(message = "{file.matrix}")
    Path matrixFile;

    @Option(names = "--descending")
    boolean descending;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        classification.order_matrix(true);
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        INPUT_FILE = matrixFile.toString();
        if (descending) Mode = "DESC";
    }

}
