package nl.wur.bif.pantools.analysis.calculate_dn_ds;

import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.analysis.msa.MultipleSequenceAlignment;
import nl.wur.bif.pantools.utils.PhasedFunctionalities;
import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.*;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;

import static nl.wur.bif.pantools.analysis.classification.Classification.*;
import static nl.wur.bif.pantools.construction.synteny.Synteny.retrieveSyntenySequenceCombinations;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

public class CalculateDnDs {

    //Uses globals
    // compare_sequences
    // skip_array
    // skip_seq_array

    private BlockingQueue<String> alignmentQueue;
    private BlockingQueue<String> pal2nalQueue;
    private BlockingQueue<String> codemlQueue;
    private boolean convertCodemlFiles = false;
    private final PhasedFunctionalities pf = new PhasedFunctionalities();
    private final String[] phases = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "unphased"};
    private boolean allowZeros;

    /**
     * calculate_DnDs()  case calculate_dn_ds()
     * Requires executeable software: pal2nal.pl, codeml
     * for msa, requires mafft
     *
     * Optional
     * --homology-groups
     * --synteny
     * --homologs
     *
     * Follows the protocol of https://www.protocols.io/view/introduction-to-calculating-dn-ds-ratios-with-code-3byl4o4rgo5d/v2
     */
    public void calculateDnDs(String homologsOrSyntelogs, boolean allowZero, String scoringMatrix) {
        Pantools.logger.info("Calculate Dn, Ds and Dn/DFs.");
        check_if_program_exists_stderr("pal2nal.pl", 100, "PAL2NAL", true); // check if program is set to $PAT
        check_if_program_exists_stdout("which codeml", 3, "CODEML", true); // check if program is set to $PATH
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            check_current_grouping_version(); // sets `grouping_version` and `longest_transcripts`
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nfe) {
           throw new RuntimeException("Unable to start the pangenome database");
        }

        allowZeros = allowZero;
        compare_sequences = true;
        codemlQueue = new LinkedBlockingQueue<>();
        alignmentQueue = new LinkedBlockingQueue<>();
        pal2nalQueue = new LinkedBlockingQueue<>();

        HashSet<String> sequenceCombinations;
        Path outputPath = Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs);

        Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve("scripts").toFile().mkdirs();
        outputPath.resolve("between_sequences").toFile().mkdirs();
        outputPath.resolve("between_genomes").toFile().mkdirs();
        if (PHASED_ANALYSIS) {
            outputPath.resolve("between_subgenomes").toFile().mkdirs();
        }

        HashMap<Node, String> homologyNodeClassMap = pf.read_core_accessory_unique_groups(false);
        if (homologsOrSyntelogs.equals("syntelogs")) {
            try (Transaction ignored = GRAPH_DB.beginTx()) { // start database transaction
                skip.create_skip_arrays(false, true);
                skip.retrieveSelectedGenomes();
                skip.retrieveSelectedSequences(10000, false);
            } catch (NotFoundException nfe) {
                throw new RuntimeException("Unable to start the pangenome database");
            }
            pf.preparePhasedGenomeInformation(PHASED_ANALYSIS, selectedSequences);
            sequenceCombinations = retrieveSyntenySequenceCombinations(homologsOrSyntelogs, PHASED_ANALYSIS, selectedSequences, selectedGenomes);
            if (sequenceCombinations.isEmpty()) {
                throw new RuntimeException("No synteny information is available in the pangenome");
            }
            Pantools.logger.info("Selected " + selectedGenomes.size() + " genome(s) with " + selectedSequences.size() + " sequences. "
                    + sequenceCombinations.size() + " pairwise comparisons between sequences");
            pairwiseAligmentsForDnDs(sequenceCombinations, scoringMatrix);
            pairwisePal2nalForDnDs(sequenceCombinations);
            pairwiseCodeMLForDnDs(sequenceCombinations);
            pairwiseExtractDnDsFromCodeML(sequenceCombinations);
            combinePairwiseFilesPerSequenceCombi(sequenceCombinations, homologyNodeClassMap, homologsOrSyntelogs);
        } else { // homologs
            //TODO verify if there are homology groups
            String[] skipArguments = setSkipArraysToFalse();
            ArrayList<Node> homologyNodes = msaForDnds();
            setOriginalSkipSettings(skipArguments);
            ArrayList<Node> groupsToBeAnalyzed = checkPal2nalCodemlStatus(homologyNodes);
            msaPal2nalforDnDs();
            msaCodeMLforDnDs();
            msaExtractDnDsFromCodeML(groupsToBeAnalyzed);
            combineMsaFilesPerSequence(homologyNodes, homologyNodeClassMap, selectedSequences);
            sequenceCombinations = retrieveSyntenySequenceCombinations(homologsOrSyntelogs, PHASED_ANALYSIS, selectedSequences, selectedGenomes);
        }

        HashSet<String> genomeCombinations = findGenomeCombinations(sequenceCombinations);
        HashSet<String> subgenomeCombinations = findSubGenomeCombinations(sequenceCombinations);
        if (subgenomeCombinations != null) {
            combineDnDsSequenceFilesToSubgenomes(sequenceCombinations, subgenomeCombinations, homologsOrSyntelogs);
            createDnDsRscripts(subgenomeCombinations, homologsOrSyntelogs, "subgenomes");
            createStatisticsFile(subgenomeCombinations, homologsOrSyntelogs, "subgenomes");
        }
        combineDnDsSequenceFilesToGenomes(sequenceCombinations, genomeCombinations, homologsOrSyntelogs);
        createDnDsRscripts(genomeCombinations, homologsOrSyntelogs, "genomes");

        createStatisticsFile(genomeCombinations, homologsOrSyntelogs, "genomes");

        Pantools.logger.info("Selected {} genome(s) with {} sequences.", selectedGenomes.size(), selectedSequences.size());
        Pantools.logger.info("{} pairwise comparisons between (and within) genomes, {} between the sequences.", genomeCombinations.size(), +sequenceCombinations.size());

        Pantools.logger.info("How to interpret Dn/Ds ratios:");
        Pantools.logger.info(" Dn/Ds = 1: Neutral selection.");
        Pantools.logger.info(" Dn/Ds > 1: Positive (Darwinian) selection. More amino acid changing mutations compared to non-changing.");
        Pantools.logger.info(" Dn/Ds < 1: Negative (purifying) selection. Less amino acid changing mutations compared to non-changing. The protein is under constraint.");

        // Alternative explanation
        // A Dn/Ds value of approximately 0 suggests that any deleterious non-synonomous substitutions are rapidly removed from the population.
        // A Dn/Ds value of approximately 1 suggests that non-synonymous substitions are occuring at about the same rate as synonymous substituions.
        // A Dn/Ds value of greater than 1 suggests that some of the mutations are advantageous and the frequency rises due to natural selection.

        Pantools.logger.info("Output files written to:");
        Pantools.logger.info(" {}dn_ds/", WORKING_DIRECTORY);
        Pantools.logger.info("Plot the distribution of mutation rates:");
        Pantools.logger.info(" Rscript {}dn_ds/scripts/{}_plot_dnds.R", WORKING_DIRECTORY, homologsOrSyntelogs);
        Pantools.logger.info(" Rscript {}dn_ds/scripts/{}_plot_log10_dnds.R", WORKING_DIRECTORY, homologsOrSyntelogs);
    }

    /**
     *
     * @param skipArguments 3 string values [SELECTION_FILE, target_genome, skip_genomes]
     */
    private void setOriginalSkipSettings(String[] skipArguments) {
        if (skipArguments[0] != null) {
            SELECTION_FILE = Paths.get(skipArguments[0]);
        }
        target_genome = skipArguments[1];
        skip_genomes = skipArguments[2];
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // update skip array to  provided by user
            tx.success();
        }

        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(10000, false);
        pf.preparePhasedGenomeInformation(PHASED_ANALYSIS, selectedSequences);
    }

    /**
     * Don't skip any genome or sequence because MSA must be done on all sequences.
     * Why? MSA always reruns with new genome selections. Make sure its always the same
     * later on subselection is made
     */
    private String[] setSkipArraysToFalse() {
        skip_array = new boolean[GENOME_DB.num_genomes];
        skip_seq_array = new boolean[GENOME_DB.num_genomes][0];
        for (int i=0; i < GENOME_DB.num_genomes; i++) {
            skip_array[i] = false;
            skip_seq_array[i] = new boolean[GENOME_DB.num_sequences[i+1]];
            for (int j=0; j < GENOME_DB.num_sequences[i+1]; j++) {
                skip_seq_array[i][j] = false;
            }
        }

        String temp = null;
        if (SELECTION_FILE != null) {
            temp = SELECTION_FILE.toString(); // to prevent MSA function from skipping genomes/sequences
        }
        String temp2 = target_genome;
        String temp3 = skip_genomes;
        SELECTION_FILE = null;
        target_genome = null;
        skip_genomes = null;

        // selectedGenomes and selectedSequences are not used by the MSA class
//        skip.retrieveSelectedGenomes();
//        skip.retrieveSelectedSequences(10000);
        return new String[]{temp, temp2, temp3};
    }

    private BigInteger binomial(final int N, final int K) {
        BigInteger ret = BigInteger.ONE;
        for (int k = 0; k < K; k++) {
            ret = ret.multiply(BigInteger.valueOf(N-k))
                    .divide(BigInteger.valueOf(k+1));
        }
        return ret;
    }

    /**
     * Find which genomes are compared based on the sequence combinations
     * @param sequenceIdCombinations
     * @return
     */
    private HashSet<String> findGenomeCombinations(HashSet<String> sequenceIdCombinations) {
        BigInteger totalCombinations = binomial(selectedGenomes.size(), 2);
        HashSet<String> genomeCombinations = new HashSet<>();
        for (String sequenceIdCombi : sequenceIdCombinations) {
            String[] sequenceIdCombiArray = sequenceIdCombi.split("#"); // 1_1#1_2 becomes [1_1, 1_2]
            String[] sequenceId1Array = sequenceIdCombiArray[0].split("_"); // 1_1 becomes [1,1]
            String[] sequenceId2Array = sequenceIdCombiArray[1].split("_");
            int genomeNr1 = Integer.parseInt(sequenceId1Array[0]);
            int genomeNr2 = Integer.parseInt(sequenceId2Array[0]);
            if (genomeNr1 < genomeNr2) {
                genomeCombinations.add(sequenceId1Array[0] + "_" + sequenceId2Array[0]);
            } else {
                genomeCombinations.add(sequenceId2Array[0] + "_" + sequenceId1Array[0]);
            }
            if (genomeCombinations.size() == (totalCombinations.doubleValue() + selectedGenomes.size())) {
                break;
            }
        }
        return genomeCombinations;
    }

    /**
     * Find which subgenomes are compared based on the sequence combinations
     * @param sequenceIdCombinations
     * @return
     */
    private HashSet<String> findSubGenomeCombinations(HashSet<String> sequenceIdCombinations) {
        if (!PHASED_ANALYSIS) {
            return null;
        }

        HashSet<String> subgenomeCombinations = new HashSet<>();
        for (String sequenceIdCombi : sequenceIdCombinations) {
            String[] sequenceIdCombiArray = sequenceIdCombi.split("#"); // 1_1#1_2 becomes [1_1, 1_2]
            String subgenomeA = pf.getSubgenomeIdentifier(sequenceIdCombiArray[0]);
            String subgenomeB = pf.getSubgenomeIdentifier(sequenceIdCombiArray[1]);
            String[] orderedSubgenomes = orderTwoSubgenomeIdentifiers(subgenomeA, subgenomeB, phases);
            subgenomeCombinations.add(orderedSubgenomes[0] + "#" + orderedSubgenomes[1]);
        }
        return subgenomeCombinations;
    }

    private String createFileNameForGenomeCombination(String genomeCombination) {
        String[] genomeCombiArray = genomeCombination.split("_"); // example 1#1 or 1#2
        String genome1 = genomeCombiArray[0];
        String genome2 = genomeCombiArray[1];
        String fileName = genomeCombination;
        if (genome1.equals(genome2)) {
            fileName = genome1; // call file 1 instead of 1_1
        }
        return fileName;
    }

    /**
     * Example input file
     * type,value,class
     * Dn,0.0000,core
     * Ds,0.0000,core
     * Dn/Ds,0.0010,core
     * Dn,0.0000,core
     * Ds,0.0000,core
     * Dn/Ds,0.2585,core
     *
     *
     * @param sequenceCombinations
     * @param genomeCombinations
     * @param homologsOrSyntelogs
     */
    private void combineDnDsSequenceFilesToGenomes(HashSet<String> sequenceCombinations,
                                                   HashSet<String> genomeCombinations, String homologsOrSyntelogs) {

        int counter = 0;
        for (String genomeCombination : genomeCombinations) {
            counter++;
            System.out.print("\r Combining files per genome combination: " + counter + "/" + genomeCombinations.size() + "     ");
            String fileName = createFileNameForGenomeCombination(genomeCombination);
            String[] genomeCombiArray = genomeCombination.split("_"); // example 1#1 or 1#2, becomes [1,1]
            String genomeNr1 = genomeCombiArray[0];
            String genomeNr2 = genomeCombiArray[1];
            StringBuilder combinedFileBuilder = new StringBuilder("type,value,log10_value,class,mRNA_id1,mRNA_id2\n");

            // go over all sequence combinations
            for (String sequenceCombination : sequenceCombinations) {
                if (!sequenceCombination.startsWith(genomeNr1) || !sequenceCombination.contains("#" + genomeNr2)) {
                    continue;
                }
                // but only continue with combinations of the current genomes
                String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes G1S1_G1S2

                Path dndsFile = Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_sequences").resolve(renamedSequenceCombi + "_" + "dnds_values.csv");
                if (!dndsFile.toFile().exists()) {
                    continue;
                }
                String lines = retrieveLinesExceptFirst(dndsFile);
                combinedFileBuilder.append(lines);
            }
            writeStringToFile(combinedFileBuilder.toString(),
                    Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_genomes").resolve(fileName + "_dnds_input.csv").toFile(), false);
        }
        System.out.println("");
    }

    private String createFileNameForSubgenomeCombination(String subgenomeCombination) {
        String[] subgenomeCombiArray = subgenomeCombination.split("#"); // example 1#1 or 1#2
        String subgenomeA = subgenomeCombiArray[0].replace("_","").replace("unphased","U");
        String subgenomeB = subgenomeCombiArray[1].replace("_","").replace("unphased","U");
        String fileName = subgenomeA + "_" + subgenomeB;
        if (subgenomeA.equals(subgenomeB)) {
            fileName = subgenomeA; // call file 1A instead of 1A_1A
        }
        return fileName.replace("#","_");
    }

    private void createStatisticsFile(HashSet<String> genomeCombinations, String homologsOrSyntelogs, String genomeOrSubgenome) {
        StringBuilder csv = new StringBuilder("Between " + genomeOrSubgenome + ",Type,Median,Average,Standard deviation,Lowest value,Highest value,Sum of all values,Value count (number of values)\n");
        for (String genomeCombination : genomeCombinations) {
            String fileName;
            if (genomeOrSubgenome.equals("genomes")) {
                fileName = createFileNameForGenomeCombination(genomeCombination);
            } else {
                fileName = createFileNameForSubgenomeCombination(genomeCombination);
            }
            Path dndsFile = Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_" + genomeOrSubgenome).resolve(fileName + "_dnds_input.csv");
            double[][] statistics = calculateDnDsStatistics(dndsFile); // [dn, ds, dn/ds] [median, average, standard deviation, shortest, longest, sum of all values, count]
            String genomeCombiWithSpaces = fileName.replace("_", " ");
            csv.append(genomeCombiWithSpaces).append(",Dn,").append(arrayToCSVString(statistics[0])).append("\n")
                    .append(genomeCombiWithSpaces).append(",Ds,").append(arrayToCSVString((statistics[1]))).append("\n")
                    .append(genomeCombiWithSpaces).append(",Dn/Ds,").append(arrayToCSVString((statistics[2]))).append("\n");
        }
        writeStringToFile(csv.toString(), Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_" + genomeOrSubgenome + "_statistics.csv").toFile(), false);
    }

    /**
     * Example input file
     *  type,value,log10_value,class
     *  Dn,0.0862,-1.0644927341752872,core
     *  Ds,0.1072,-0.9698052146432488,core
     *  Dn/Ds,0.8047,-0.09436599867304526,core
     *  Ds,0.0205,-1.6882461389442456,core
     *  Dn/Ds,0.0010,-3.0,core
     *  Dn,0.0974,-1.0114410431213845,core
     *
     *
     * @param sequenceCombinations
     * @param subgenomeCombinations
     * @param homologsOrSyntelogs
     */
    private void combineDnDsSequenceFilesToSubgenomes(HashSet<String> sequenceCombinations,
                                                   HashSet<String> subgenomeCombinations, String homologsOrSyntelogs) {

        int counter = 0;
        for (String subgenomeCombination : subgenomeCombinations) {
            counter++;
            String fileName = createFileNameForSubgenomeCombination(subgenomeCombination);
            String[] subgenomeCombiArray = subgenomeCombination.split("#"); // example 1B#2B becomes [1B, 2B]
            //TODO replace with progress bar
            System.out.print("\r Combining files per subgenome combination: " + counter + "/" +  subgenomeCombinations.size());
            StringBuilder combinedFileBuilder = new StringBuilder("type,value,log10_value,class,mRNA_id1,mRNA_id2\n");
            for (String sequenceCombination : sequenceCombinations) {
                String[] sequenceIdCombiArray = sequenceCombination.split("#");
                String subgenomeA = pf.getSubgenomeIdentifier(sequenceIdCombiArray[0]);
                String subgenomeB = pf.getSubgenomeIdentifier(sequenceIdCombiArray[1]);
                String[] orderedSubgenomes = orderTwoSubgenomeIdentifiers(subgenomeA, subgenomeB, phases);
                if (!subgenomeCombiArray[0].equals(orderedSubgenomes[0]) || !subgenomeCombiArray[1].equals(orderedSubgenomes[1]) ) {
                    continue;
                }
                String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes G1S1_G1S2

                Path dndsFile = Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_sequences").resolve(renamedSequenceCombi + "_dnds_values.csv");
                if (!dndsFile.toFile().exists()) {
                    continue;
                }
                String lines = retrieveLinesExceptFirst(dndsFile);
                combinedFileBuilder.append(lines);
            }
            writeStringToFile(combinedFileBuilder.toString(),
                    Paths.get(WORKING_DIRECTORY).resolve("dn_ds").resolve(homologsOrSyntelogs).resolve("between_subgenomes").resolve(fileName + "_dnds_input.csv").toFile(),
                    false);
            System.out.println("");
        }
    }

    /**
     * Put all lines of a file in a String, except for the first
     * Lines are trimmed
     */
    private String retrieveLinesExceptFirst(Path file) {
        StringBuilder lines = new StringBuilder();
        boolean firstLine = true;
        try (BufferedReader in = Files.newBufferedReader(file)) { // read original fasta files
            for (int c = 0; in.ready();) {
                String line = in.readLine().trim();
                if (firstLine) { // skip the header
                    firstLine = false;
                    continue;
                }
                lines.append(line).append("\n");
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to open: " + file);
        }
        return lines.toString();
    }

    /**
     * Example input file
     *  type,value,log10_value,class
     *  Dn,0.0862,-1.0644927341752872,core
     *  Ds,0.1072,-0.9698052146432488,core
     *  Dn/Ds,0.8047,-0.09436599867304526,core
     *  Ds,0.0205,-1.6882461389442456,core
     *  Dn/Ds,0.0010,-3.0,core
     *  Dn,0.0974,-1.0114410431213845,core
     *
     */
    private double[][] calculateDnDsStatistics(Path dndsFile) {
        ArrayList<Double> dnValues = new ArrayList<>();
        ArrayList<Double> dsValues = new ArrayList<>();
        ArrayList<Double> dndsValues = new ArrayList<>();
        boolean firstLine = true;
        try {
            BufferedReader br = Files.newBufferedReader(dndsFile);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (firstLine) { // skip the header
                    firstLine = false;
                    continue;
                }
                String[] lineArray = line.trim().split(","); //   Dn,0.0862,-1.0644927341752872,core,mrna1,mrna2 becomes [Dn, 0.0862, -1.0644927341752872, core,mrna1,mrna2]
                double value = Double.parseDouble(lineArray[1]);
                if (lineArray[0].equals("Dn")) {
                    dnValues.add(value);
                } else if (lineArray[0].equals("Ds")) {
                    dsValues.add(value);
                } else { // Dn/Ds
                    dndsValues.add(value);
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Something went wrong while reading: " + dndsFile);
        }

        double[][] statistics = new double[3][0];
        statistics[0] = median_average_stdev_from_AL_double(dnValues);
        statistics[1] = median_average_stdev_from_AL_double(dsValues);
        statistics[2] = median_average_stdev_from_AL_double(dndsValues);
        return statistics;
    }

    /**
     *
     * @param genome_combinations
     * @param homologsOrSyntelogs
     * @param genomeOrSubgenome
     */
    private void createDnDsRscripts(HashSet<String> genome_combinations, String homologsOrSyntelogs, String genomeOrSubgenome) {
        createDirectory("dn_ds/plots/" + homologsOrSyntelogs + "_log10", true);
        createDirectory("dn_ds/plots/" + homologsOrSyntelogs, true);
        if (genomeOrSubgenome.equals("subgenomes")) {
            createDirectory("dn_ds/plots/" + homologsOrSyntelogs + "_log10_subgenomes", true);
            createDirectory("dn_ds/plots/" + homologsOrSyntelogs + "_subgenomes", true);
        }

        boolean outputPresent = false;
        StringBuilder genomeCombiBuilder = new StringBuilder("genome_combinations <- c(");
        for (String genomeCombination : genome_combinations) {
            String fileName;
            if (genomeOrSubgenome.equals("genomes")) {
                fileName = createFileNameForGenomeCombination(genomeCombination);
            } else {
                fileName = createFileNameForSubgenomeCombination(genomeCombination);
            }
            String dndsInput = WD_full_path + "dn_ds/" + homologsOrSyntelogs + "/between_" + genomeOrSubgenome + "/" + fileName + "_dnds_input.csv";
            if (checkIfFileExists(dndsInput)) {
                genomeCombiBuilder.append("\"").append(fileName).append("\", ");
                outputPresent = true;
            }
        }

        if (!outputPresent) {
            throw new RuntimeException("The program should have stopped earlier... Either MAFFT, PAL2NAL or CODEML failed.");
        }
        String genomeCombiStr = genomeCombiBuilder.toString().replaceFirst(".$","").replaceFirst(".$","")  + ")\n"; // remove two last character
        createRegularValueRscript(genomeCombiStr, homologsOrSyntelogs, genomeOrSubgenome);
        createLog10ValueRscript(genomeCombiStr,  homologsOrSyntelogs, genomeOrSubgenome);
    }

    /**
     *
     * @param genomeCombiStr
     * @param homologsOrSyntelogs
     * @param genomeOrSubgenome
     */
    private void createLog10ValueRscript(String genomeCombiStr, String homologsOrSyntelogs, String genomeOrSubgenome) {
        String addSubgenomestoDirectory = "";
        if (genomeOrSubgenome.equals("genomes") && PHASED_ANALYSIS) {
            String rscript = "\n" + genomeCombiStr +
                    "main_function(genome_combinations, \"" + genomeOrSubgenome + "\")\n" +
                    "cat('\\n\\nPlots written to: " + WD_full_path + "dn_ds/plots/" + homologsOrSyntelogs + "_log10/\\n\\n')\n";
            writeStringToFile(rscript, WORKING_DIRECTORY + "/dn_ds/scripts/" + homologsOrSyntelogs + "_plot_log10_dnds.R", false,true);
            return;
        } else if (PHASED_ANALYSIS) {
            addSubgenomestoDirectory = "_subgenomes";
        }

        String codeBinLocations = binLocationsRscript();
        String scriptPart = createSharedPartRscripts(homologsOrSyntelogs, true, addSubgenomestoDirectory);
        String rscript = "library(ggplot2)\n" +
                "library(cowplot)\n" +
                "\n" +
                "dn_xaxis_lower_limit = -4\n" +
                "ds_xaxis_lower_limit = -4\n" +
                "dnds_xaxis_lower_limit = -4\n" +
                "\n" +
                "dn_xaxis_upper_limit = 2\n" +
                "ds_xaxis_upper_limit = 2\n" +
                "dnds_xaxis_upper_limit = 2\n" +
                "\n" +
                "# Number of bins\n" +
                "dn_bins = 150\n" +
                "ds_bins = 150\n" +
                "dnds_bins = ds_bins * 0.8 # 20% less bins required to get the same binwidth as the other plots because dn/ds values only go to 0.001\n" +
                "\n" +
                "# Take this peak to determine the Y-axis cutoff\n" +
                "dn_peak_nr_yaxis = 1\n" +
                "ds_peak_nr_yaxis = 1\n" +
                "dnds_peak_nr_yaxis = 1\n" +
                "\n" +
                scriptPart +
                codeBinLocations +
                genomeCombiStr + "main_function(genome_combinations, \"" + genomeOrSubgenome + "\")\n" +
                "cat('\\n\\nPlots written to: " + WD_full_path + "dn_ds/plots/" + homologsOrSyntelogs + "_log10" + addSubgenomestoDirectory + "/\\n\\n')\n";

        writeStringToFile(rscript, WORKING_DIRECTORY + "/dn_ds/scripts/" + homologsOrSyntelogs + "_plot_log10_dnds.R", false,false);
    }

    /**
     * code is for log10 values
     */
    private String binLocationsRscript() {
        return "calculate_bin_locations <- function(nr_bins, xaxis_lower_limit, xaxis_upper_limit) {\n" +
                "plot_width = xaxis_upper_limit - xaxis_lower_limit\n" +
                "    sink(\"" + WORKING_DIRECTORY + "dn_ds/bin_positions.csv\")\n" +
                "    cat(\"#based on\", nr_bins, \"bins and the lower and upper axis boundaries set for Dn.\\n\")\n" +
                "    cat(\"bin position,lower bound,upper bound\\n\")\n" +
                "    for (i in 1:nr_bins) {\n" +
                "        value = (i * (plot_width / nr_bins)) + xaxis_lower_limit\n" +
                "        lower_bound = value - (6 / nr_bins)\n" +
                "        lower_bound_log10 = 10 ^ lower_bound\n" +
                "        value_log10 = 10 ^ value\n" +
                "        cat(i, lower_bound_log10, value_log10, \"\\n\", sep = \",\")\n" +
                "    }\n" +
                "    sink()\n" +
                "}\n" +
                "\n" +
                "calculate_bin_locations(dn_bins, dnds_xaxis_lower_limit, dnds_xaxis_upper_limit)\n\n";
    }

    private String createSharedPartRscripts(String homologsOrSyntelogs, boolean log10, String addSubgenomestoDirectory) {
        String alternativeYLabel = "        #labs(y = \"No. homologous gene pairs\") +\n";
        if (homologsOrSyntelogs.equals("syntelogs")) {
            alternativeYLabel = "        #labs(y = \"No. syntenic gene pairs\") +\n";
        }
        StringBuilder rscript = new StringBuilder();
        String addLogA = "",  addLogB = "";
        if (log10) {
            addLogA = "_log10";
            addLogB = "log10_";
        }
        rscript.append("out_path <- \"").append(WD_full_path).append("dn_ds/plots/").append(homologsOrSyntelogs).append(addLogA).append(addSubgenomestoDirectory).append("/\"\n")
                .append("\n")
                .append("create_save_histogram <- function(genome_combination, file_extension, df, type, lower_xlimit, upper_xlimit, nr_bins, peak_nr_yaxis) {\n")
                .append("    #df <-subset(df, value!=0.0000) # remove zeros\n")
                .append("    plot <- ggplot(df, aes(x=").append(addLogB).append("value)) +\n")
                .append("        geom_histogram(bins = nr_bins, fill=\"#336699\", color=\"#cbd3da\") +\n")
                .append("        theme_bw(base_size = 20) + # alternative themes: theme_classic(), theme_gray()\n")
                .append("        labs(y = \"Frequency\") + labs(x = type) #comment out to remove axis labels\n")
                //.append(alternativeYLabel)
                .append("        #theme(axis.title = element_blank()) #uncomment to remove axis labels\n")
                .append("    ylimit = highest_in_ggplot_histo(plot, peak_nr_yaxis)\n")
                .append("\n")
                .append("    histo_plot <- plot +\n")
                .append("        coord_cartesian(ylim=c(0, (ylimit*1.05)), xlim=c(lower_xlimit, upper_xlimit)) +\n")
                .append("        scale_x_continuous(breaks=c(-4,-3,-2,-1,0,1,2), labels=c(\"0.0001\",\"0.001\",\"0.01\",\"0.1\",\"1\",\"10\",\"100\"))\n")
                .append("    output_file <- paste(out_path, genome_combination, file_extension, sep=\"\")\n")
                .append("    ggsave(histo_plot, file=output_file, device = \"png\", width = 25, height = 15, units = \"cm\")\n")
                .append("    return (histo_plot)\n")
                .append("}\n")
                .append("\n")
                .append("highest_in_ggplot_histo <- function(plot, peak_nr_yaxis) {\n")
                .append("    build <-ggplot_build(plot)\n")
                .append("    plot_statistics <- build[[1]][[1]]\n")
                .append("    ordered_y <-sort(plot_statistics$y, decreasing = TRUE)\n")
                .append("    return(ordered_y[[peak_nr_yaxis]])\n")
                .append("}\n")
                .append("\n")
                .append("main_function <- function(genome_combinations, genome_or_subgenome) {\n")
                .append("    total_files <- toString(length(genome_combinations))\n")
                .append("    for (i in 1:length(genome_combinations)) {\n")
                .append("       genome_combination <-  genome_combinations[i]\n")
                .append("       file <- paste(\"").append(WD_full_path).append("dn_ds/").append(homologsOrSyntelogs)
                    .append("/between_\", genome_or_subgenome, \"/\", genome_combination, \"_dnds_input.csv\", sep=\"\")\n")

                .append("       cat(\"\\rComparison\", i, \"/\", total_files, \"\")\n")
                .append("       df = read.csv(file)\n")
                .append("       df_DN <- df[which(df$type == \"Dn\"),]\n")
                .append("       df_DS <- df[which(df$type == \"Ds\"),]\n")
                .append("       df_DNDS <- df[which(df$type == \"Dn/Ds\"),]\n")
                .append("\n")
                .append("       # filter values above the axis limit. required for correct number of bins/binwidth\n")
                .append("       df_DN = df_DN[df_DN$").append(addLogB).append("value < dn_xaxis_upper_limit, ]\n")
                .append("       df_DS = df_DS[df_DS$").append(addLogB).append("value < ds_xaxis_upper_limit, ]\n")
                .append("       df_DNDS = df_DNDS[df_DNDS$").append(addLogB).append("value < dnds_xaxis_upper_limit, ]\n")
                .append("\n")
                .append("       dn_histo = create_save_histogram(genome_combination, \"_dn_log10.png\", df_DN, \"Dn\", dn_xaxis_lower_limit, dn_xaxis_upper_limit, dn_bins, dn_peak_nr_yaxis)\n")
                .append("       ds_histo = create_save_histogram(genome_combination, \"_ds_log10.png\", df_DS, \"Ds\", ds_xaxis_lower_limit, ds_xaxis_upper_limit, ds_bins, ds_peak_nr_yaxis)\n")
                .append("       dnds_histo = create_save_histogram(genome_combination, \"_dnds_log10.png\", df_DNDS, \"Dn/Ds\", dnds_xaxis_lower_limit, dnds_xaxis_upper_limit, dnds_bins, dnds_peak_nr_yaxis)\n")
                .append("       plots <- plot_grid(dn_histo, ds_histo, dnds_histo, ncol = 1, align = 'v')\n")
                .append("       output_file <- paste(out_path, genome_combination, \"_log10.png\", sep=\"\")\n")
                .append("       ggsave(plots, file=output_file, width = 20, height = 20, units = \"cm\")\n")
                .append("   }\n")
                .append("}\n")
                .append("\n");
        return rscript.toString();
    }

    private void createRegularValueRscript(String genomeCombiStr, String homologsOrSyntelogs, String genomeOrSubgenome) {
        String addSubgenomestoDirectory = "";
        if (genomeOrSubgenome.equals("genomes") && PHASED_ANALYSIS) {
            String rscript = "\n" + genomeCombiStr +
                    "main_function(genome_combinations, \"" + genomeOrSubgenome + "\")\n" +
                    "cat('\\rPlots written to: " + WD_full_path + "dn_ds/plots/homologs/\\n\\n')\n";
            writeStringToFile(rscript, WORKING_DIRECTORY + "/dn_ds/scripts/" + homologsOrSyntelogs + "_plot_dnds.R", false,true);
            return;
        } else if (PHASED_ANALYSIS) {
            addSubgenomestoDirectory = "_subgenomes";
        }
        String scriptPart = createSharedPartRscripts(homologsOrSyntelogs, false, addSubgenomestoDirectory);
        String rscript = "library(ggplot2)\n" +
                "library(cowplot)\n" +
                "\n" +
                "# Set to \"dynamic\" or \"static\"\n" +
                "\n" +
                "axis <- \"dynamic\"\n" +
                "\n" +
                "# When using \"dynamic\", cutoff of X-axis is determined by 95% of the data\n" +
                "dn_procentile_xaxis = 0.95\n" +
                "ds_procentile_xaxis = 0.95\n" +
                "dnds_procentile_xaxis = 0.95\n" +
                "\n" +
                "# When using \"static\", X-axis limit is set here\n" +
                "dn_xaxis_upper_limit = 0.1\n" +
                "ds_xaxis_upper_limit = 0.3\n" +
                "dnds_xaxis_upper_limit = 2\n" +
                "\n" +
                "#Recommended to not change the lower limit of the X-axis.\n" +
                "dn_xaxis_lower_limit = 0\n" +
                "ds_xaxis_lower_limit = 0\n" +
                "dnds_xaxis_lower_limit = 0\n" +
                "\n" +
                "# Number of bins (applicable to both \"dynamic\" or \"static\")\n" +
                "dn_bins = 150\n" +
                "ds_bins = 150\n" +
                "dnds_bins = 150\n" +
                "\n" +
                "# Take this peak to determine the Y-axis cutoff (applicable to both \"dynamic\" or \"static\")\n" +
                "dn_peak_nr_yaxis = 1\n" +
                "ds_peak_nr_yaxis = 1\n" +
                "dnds_peak_nr_yaxis = 1\n" +
                "\n" +
                scriptPart +
                genomeCombiStr + "main_function(genome_combinations, \"" + genomeOrSubgenome + "\")\n" +
                "cat('\\rPlots written to: " + WD_full_path + "dn_ds/plots/homologs" + addSubgenomestoDirectory + "/\\n\\n')\n";

        boolean append = genomeOrSubgenome.equals("genomes") && PHASED_ANALYSIS;
        writeStringToFile(rscript, WORKING_DIRECTORY + "/dn_ds/scripts/" + homologsOrSyntelogs + "_plot_dnds.R", false, append);
    }

    /**
     * check prepare and run pairwise alignments
     * alignments are done in batches per sequence pair.
     * Once all alignments of the sequence pair are done a file is generated that is recognized on the next run.
     */
    private void pairwiseAligmentsForDnDs(HashSet<String> sequenceCombinations, String scoringMatrix) {
        int sequenceCombiCounter = 0;
        for (String sequenceCombination : sequenceCombinations) {
            sequenceCombiCounter++;
            String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
            createDirectory("alignments/pairwise/" + renamedSequenceCombi, true);
            if (!checkIfFileExists(WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/completed_alignment")) {
                find_prepare_sequences_for_pal2_nal3(sequenceCombination, "protein_alignment", sequenceCombiCounter);
                try {
                    ExecutorService es = Executors.newFixedThreadPool(THREADS);
                    for (int i = 1; i <= THREADS; i++) {
                        alignmentQueue.add("stop");
                        es.execute(new run_alignment_parallel(sequenceCombinations.size(), sequenceCombination,
                                sequenceCombiCounter, scoringMatrix));
                    }
                    es.shutdown();
                    es.awaitTermination(10, TimeUnit.DAYS);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Something went wrong during the pairwise alignments");
                }
                writeStringToFile("done", "alignments/pairwise/" + renamedSequenceCombi + "/completed_alignment", true, false);
            } else if (sequenceCombiCounter == 1 || sequenceCombiCounter % 10 == 0) {
                System.out.print("\r Aligning syntenic genes: sequence pair " + sequenceCombiCounter + "/" + sequenceCombinations.size());
            }
        }
    }

    /**
     * Create .paml file
     * @param sequenceCombinations
     */
    private void pairwisePal2nalForDnDs(HashSet<String> sequenceCombinations) {
        int sequenceCombiCounter = 0;
        for (String sequenceCombination : sequenceCombinations) {
            sequenceCombiCounter++;
            if (sequenceCombiCounter == 1 || sequenceCombiCounter % 10 == 0) {
                System.out.print("\r Running pal2nal " + sequenceCombiCounter + "/" + sequenceCombinations.size());
            }

            String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
            if (!checkIfFileExists(WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/completed_codon_alignment")) {
                find_prepare_sequences_for_pal2_nal3(sequenceCombination, "codon_alignment", sequenceCombiCounter);
                try {
                    ExecutorService es = Executors.newFixedThreadPool(THREADS);
                    for (int i = 1; i <= THREADS; i++) {
                        pal2nalQueue.add("stop");
                        es.execute(new run_pal2nal_parallel(sequenceCombinations.size(), sequenceCombination, sequenceCombiCounter));
                    }
                    es.shutdown();
                    es.awaitTermination(10, TimeUnit.DAYS);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Something went wrong during the PAL2NAL");
                }
                writeStringToFile("done", "alignments/pairwise/" + renamedSequenceCombi + "/completed_codon_alignment", true, false);
            }
        }
    }

    private void pairwiseExtractDnDsFromCodeML(HashSet<String> sequenceCombinations) {
        int sequenceCombiCounter = 0;
        for (String sequenceCombination : sequenceCombinations) {
            sequenceCombiCounter++;
            String seq_combi_space = sequenceCombination.replace("#", " to ");
            String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
            String completedFile = WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/completed_dnds_extraction";
            if (!checkIfFileExists(completedFile)) {
                HashSet<String> geneNodeCombinations = find_prepare_sequences_for_pal2_nal3(sequenceCombination, "codeml", sequenceCombiCounter);
                int geneCombiCounter = 0;
                for (String geneNodeCombi : geneNodeCombinations) {
                    geneCombiCounter++;
                    Path codemlOutput = Paths.get(WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/" + geneNodeCombi + "/codeml_output.txt");
                    if (!checkIfFileExists(codemlOutput)) {
                        Pantools.logger.error(codemlOutput + " is missing.");
                        throw new RuntimeException("Unable to continue without " + codemlOutput);
                    }

                    if (geneCombiCounter % 100 == 0 || geneCombiCounter < 11) {
                        System.out.print("\r " + sequenceCombiCounter + "/" + sequenceCombinations.size() + ". Sequence " + seq_combi_space + ". Extracting mutation rates: "+ geneCombiCounter);
                    }

                    extractDnDsFromCodeML(codemlOutput, Paths.get(WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/" + geneNodeCombi + "/dn_ds.txt"));
                }
            } else if (sequenceCombiCounter == 1 || sequenceCombiCounter % 10 == 0) {
                System.out.print("\r " + sequenceCombiCounter + "/" + sequenceCombinations.size() + ". Sequence " + seq_combi_space + ". Extracting mutation rates: ");
            }

            writeStringToFile("done", completedFile, false, false);
        }
    }

    /**
     * Generates codeml_output.txt
     * @param sequenceCombinations
     */
    private void pairwiseCodeMLForDnDs(HashSet<String> sequenceCombinations) {
        int sequenceCombiCounter = 0;
        for (String sequenceCombination : sequenceCombinations) {
            sequenceCombiCounter++;
            if (sequenceCombiCounter == 1 || sequenceCombiCounter % 10 == 0) {
                System.out.print("\r Running codeML " + sequenceCombiCounter + "/" + sequenceCombinations.size());
            }

            String renamedSequenceCombi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
            String completedFile = WORKING_DIRECTORY + "alignments/pairwise/" + renamedSequenceCombi + "/completed_codeML";
            if (!checkIfFileExists(completedFile)) {
                find_prepare_sequences_for_pal2_nal3(sequenceCombination, "codeml", sequenceCombiCounter);
                try {
                    ExecutorService es = Executors.newFixedThreadPool(THREADS);
                    for (int i = 1; i <= THREADS; i++) {
                        codemlQueue.add("stop");
                        es.execute(new runCodeMlParralel(sequenceCombinations.size(), sequenceCombination, sequenceCombiCounter));
                    }
                    es.shutdown();
                    es.awaitTermination(10, TimeUnit.DAYS);
                } catch (InterruptedException e) {
                   throw new RuntimeException("Something went wrong during CODEML");
                }
                writeStringToFile("done", completedFile, false, false);
            }
        }
    }

    /**
     * Perform pairwise alignment with NWAlign
     */
    private class run_alignment_parallel implements Runnable {
        int numberOfComparisons;
        String seq_combi;
        int seq_combi_counter;
        String scoringMatrix;

        private run_alignment_parallel(int numberOfComparisons, String seq_combi, int seq_combi_counter,
                                       String scoringMatrix) {
            this.numberOfComparisons = numberOfComparisons;
            this.seq_combi = seq_combi;
            this.seq_combi_counter = seq_combi_counter;
            this.scoringMatrix = scoringMatrix;
        }

        public void run() {
            seq_combi = seq_combi.replace("#", " to ");
            String input_file = "";
            NeedlemanWunschAlign seqAligner = new NeedlemanWunschAlign(scoringMatrix);
            while (!input_file.equals("stop")) {
                try {
                    input_file = alignmentQueue.take();
                } catch(InterruptedException e) {
                    continue;
                }
                if (input_file.equals("stop")) {
                    continue;
                }
                if (alignmentQueue.size() % 100 == 0) {
                    System.out.print("\r " + seq_combi_counter + "/" + numberOfComparisons + ". " +
                            "Sequence " + seq_combi + ". Alignments remaining " + alignmentQueue.size() + "   ");
                }
                String[] input_array = input_file.split("#");
                // length of 6 [seq1, seq2, gene1 node id with gene2 node id, gene1 id, gene2 id, seq1_id "_" seq2_id ]

                String[] alignment_output = seqAligner.NeedlemanWunsch2(input_array[0], input_array[1], -11, -1);
                String aligned_prot1 = splitSeqOnLength(alignment_output[0], 80);
                String aligned_prot2 = splitSeqOnLength(alignment_output[1], 80);
                write_string_to_file_full_path(">" + input_array[3] + "\n" + aligned_prot1 + "\n"
                                + ">" + input_array[4] + "\n" + aligned_prot2 + "\n",
                        WORKING_DIRECTORY + "alignments/pairwise/" + input_array[5] + "/" + input_array[2] + "/prot.fasta");
            }
        }
    }

    private void combinePairwiseFilesPerSequenceCombi(HashSet<String> sequenceCombinations, HashMap<Node, String> homologyNodeClassMap,
                                                      String homologsOrSyntelogs) {

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int sequenceCombiCounter = 0;
            for (String sequenceCombination : sequenceCombinations) {
                sequenceCombiCounter++;
                String renamed_seq_combi = sequenceCombination.replace("_", "S").replace("#", "_"); // 1_1#1_2 becomes 1S1_1S2
                if (checkIfFileExists(WORKING_DIRECTORY + "dn_ds/" + homologsOrSyntelogs + "/between_sequences/" + renamed_seq_combi + "_dnds_values.csv")) {
                    continue;
                }

                HashSet<String> gene_node_combinations = find_prepare_sequences_for_pal2_nal3(sequenceCombination, "read_output", sequenceCombiCounter);
                String seq_combi_space = sequenceCombination.replace("#", " to ");
                StringBuilder missing_gene_combi = new StringBuilder();

                //StringBuilder dnDsBuilder = new StringBuilder("type,value,log10_value,class\n");
                BufferedWriter writer = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "dn_ds/" + homologsOrSyntelogs + "/between_sequences/" + renamed_seq_combi + "_dnds_values.csv"));
                writer.write("type,value,log10_value,class\n");

                //StringBuilder csv_builder = new StringBuilder("gene 1 id,gene 2 id,gene 1 node id,gene 2 node id,Dn,Ds,Dn/Ds\n");
                int counter = 0;
                for (String gene_id_combination : gene_node_combinations) {
                    String[] gene_id_combi_array = gene_id_combination.split("_");
                    long mrnaNodeIdA = Long.parseLong(gene_id_combi_array[0]);
                    long mrnaNodeIdB = Long.parseLong(gene_id_combi_array[1]);
                    Node mrnaNodeA = GRAPH_DB.getNodeById(mrnaNodeIdA);
                    Node mrnaNodeB = GRAPH_DB.getNodeById(mrnaNodeIdB);
                    Relationship hm_relation1 = mrnaNodeA.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
                    Node homologyNode = hm_relation1.getStartNode();
                    String classStr = homologyNodeClassMap.get(homologyNode); // genes were originally from same homology group
                    String proteinIdA = (String) mrnaNodeA.getProperty("protein_ID");
                    String proteinIdB = (String) mrnaNodeB.getProperty("protein_ID");
                    counter++;
                    if (counter % 100 == 0 || counter == 1) {
                        System.out.print("\r " + sequenceCombiCounter + "/" + sequenceCombinations.size() + ". Sequence " + seq_combi_space + ". Reading output files: " + counter + "      ");
                    }

                    Path dndsfile = Paths.get(WORKING_DIRECTORY + "alignments/pairwise/" + renamed_seq_combi + "/" + gene_id_combination + "/dn_ds.txt");
                    boolean skipFirstLine = true;
                    try {
                        BufferedReader br = Files.newBufferedReader(dndsfile);
                        for (String line = br.readLine(); line != null; line = br.readLine()) {
                            line = line.trim();
                            if (skipFirstLine) {
                                skipFirstLine = false;
                                continue;
                            }
                            String[] lineArray = line.split(",");
                            writeDnDsLog10ToWriter(writer, lineArray, classStr, proteinIdA, proteinIdB);
                        }
                    } catch (IOException ioe) {
                        // file does not exist because the nucleotide and protein sequences do not match
                        missing_gene_combi.append(gene_id_combination).append(",");
                    }
                }
                writer.close();
                if (missing_gene_combi.length() > 2) {
                    writeStringToFile(missing_gene_combi.toString(),
                            WORKING_DIRECTORY + "dn_ds/" + homologsOrSyntelogs + "/between_sequences/" + renamed_seq_combi + "_missing_pairs.csv", false, false);
                }
            }
            tx.success();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private class run_pal2nal_parallel implements Runnable {
        int totalCombinations;
        String sequenceCombination;
        int seq_combi_counter;

        private run_pal2nal_parallel(int totalCombinations, String seq_combi, int seq_combi_counter) {
            this.totalCombinations = totalCombinations;
            this.sequenceCombination = seq_combi;
            this.seq_combi_counter = seq_combi_counter;
        }

        public void run() {
            sequenceCombination = sequenceCombination.replace("#", " to ");
            String proteinAlignment = ""; // path to protein alignment ouput. can be in two different types of directories
            while (!proteinAlignment.equals("stop")) {
                try {
                    proteinAlignment = pal2nalQueue.take();
                } catch (InterruptedException e) {
                    continue;
                }
                if (proteinAlignment.equals("stop")) {
                    continue;
                }

                if (pal2nalQueue.size() % 100 == 0) {
                    if (sequenceCombination.equals("")) { // MSA
                        System.out.print("\r Running pal2nal: " + pal2nalQueue.size() + " pairs remaining   ");
                    } else { // pairwise alignments
                        System.out.print("\r " + seq_combi_counter + "/" + totalCombinations + ". " +
                                "Sequence " + sequenceCombination + ". pal2nal: " + pal2nalQueue.size() + " pairs remaining   ");
                    }
                }

                String nucleotideInput = proteinAlignment.replace("output/prot", "input/nuc").replace("prot", "nuc_input"); // unaligned nucleotide sequences
                String pal2nalOutput = proteinAlignment.replace(".fasta", ".paml");
                String pal2nalCommand = "pal2nal.pl " + proteinAlignment + " " + nucleotideInput + " -output paml -nogap 1> " + pal2nalOutput;
                Pantools.logger.debug("PAL2NAL command: " +  pal2nalCommand);

                runCommand(pal2nalCommand);
                if (!checkIfFileExists(pal2nalOutput)) { // second try pal2nal
                    Pantools.logger.debug("PAL2NAL output was empty.");
                    Pantools.logger.debug("PAL2NAL second try: " +  pal2nalCommand);
                    pal2nalCommand = "pal2nal.pl " + proteinAlignment + " " + nucleotideInput + " -output paml -nogap 2> " + pal2nalOutput;
                    runCommand(pal2nalCommand);
                    continue;
                }
                if (checkIfFileExists(pal2nalOutput)) {
                    codemlQueue.add(pal2nalOutput);
                }
            }
        }

        /**
         * run pal2nal
         * for some unknown reason sometimes pal2nal just gets stuck. The command works fine when run separately
         */
        private void runCommand(String command) {
            List<String> cmd = new ArrayList<>();
            cmd.add("sh");
            cmd.add("-c");
            cmd.add(command);
            ProcessBuilder pb = new ProcessBuilder(cmd);
            try {
                Process p = pb.start();
                p.waitFor(3600, TimeUnit.SECONDS);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException("crashed: " + command);
            }
        }
    }

    /**
     *
     */
    private class runCodeMlParralel implements Runnable {
        int totalComparisons;
        String sequenceCombination;
        int seq_combi_counter;

        private runCodeMlParralel(int totalComparisons, String seq_combi, int seq_combi_counter) {
            this.totalComparisons = totalComparisons;
            this.sequenceCombination = seq_combi;
            this.seq_combi_counter = seq_combi_counter;
        }

        public void run() {
            sequenceCombination = sequenceCombination.replace("#", " to ");
            String pal2nalOutput = "";
            while (!pal2nalOutput.equals("stop")) {
                try {
                    pal2nalOutput = codemlQueue.take();
                } catch (InterruptedException e) {
                    continue;
                }
                if (pal2nalOutput.equals("stop")) {
                    continue;
                }

                if (codemlQueue.size() % 100 == 0) {
                    if (sequenceCombination.equals("")) { // MSA
                        System.out.print("\r Running codeml: " + codemlQueue.size() + " pairs remaining   ");
                    } else { // pairwise alignments
                        System.out.print("\r " + seq_combi_counter + "/" + totalComparisons + ". " +
                                "Sequence " + sequenceCombination + ". codeml: " + codemlQueue.size() + " pairs remaining   ");
                    }
                }
                String codeMlFilePath = createCodemlInput(pal2nalOutput, sequenceCombination);
                String codemlCommand = "codeml " + codeMlFilePath;
                Pantools.logger.debug("CODEML command: " + codemlCommand);
                runCommand(codemlCommand);
                String codeMloutput = pal2nalOutput.replace("prot.paml","codeml_output.txt");
                if (!checkIfFileExists(codeMloutput)) { // codeml needs to be run
                    Pantools.logger.debug(codeMloutput + " was not created by CODEML. Creating mock file (same name) to skip this alignment in a future run.");
                    writeStringToFile("#No output could be generated", codeMloutput, false, false);
                } else {
                    Pantools.logger.debug(codeMloutput + " was generated by CODEML.");
                    delete_file_full_path(codeMlFilePath);
                }
            }
        }

        /**
         * run codeml
         */
        private void runCommand(String command) {
            List<String> cmd = new ArrayList<>();
            cmd.add("sh");
            cmd.add("-c");
            cmd.add(command);
            ProcessBuilder pb = new ProcessBuilder(cmd);

            try {
                Process p = pb.start();
                p.waitFor(3600, TimeUnit.SECONDS);
                if (p.exitValue() > 0) {
                    final BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line;
                    while((line = input.readLine()) != null){
                        Pantools.logger.warn(line);
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException("Crashed: " + command);
            }
        }

        /**
         * Example from https://github.com/faylward/dnds/blob/master/codeml.ctl
         *
         * seqfile = cluster_1.pal2nal   * sequence data filename
         * outfile = codeml.txt   * main result file name
         * *treefile = cluster_1_tree2.nw
         * noisy = 0      * 0,1,2,3,9: how much rubbish on the screen
         * verbose = 0      * 1:detailed output
         * runmode = -2     * -2:pairwise
         * seqtype = 1      * 1:codons
         * CodonFreq = 2      * 0:equal, 1:F1X4, 2:F3X4, 3:F61
         * model = 1      *
         * NSsites = 0      *
         * icode = 0      * 0:universal code
         * fix_kappa = 1      * 1:kappa fixed, 0:kappa to be estimated
         * kappa = 1      * initial or fixed kappa
         * fix_omega = 0      * 1:omega fixed, 0:omega to be estimated
         * omega = 0.5    * initial omega value
         * *ndata = 1
         *
         * Explanation of settings
         * https://bioinformaticsworkbook.org/dataAnalysis/ComparativeGenomics/Finding_Positive_Selection_With_Codeml.html#gsc.tab=0
         *
         *
         runmode = 0 means I am specifying user trees
         seqtype = 1 means I am using codon sequences
         CodonFreq = 0 means I am assuming equal codon frequencies (codon substitution model)
         model = 0 whether ω should be allowed to vary among branches in the tree
         NSsites = 0 1 2 7 8 whether ω should be allowed to vary among sites , several of these can be specified at once.
         icode = 0 is specifies that I want to use the universal genetic code
         fix_kappa = 0 – initial transition/transversion ratio value set to 0
         kappa = 0 – assumption starting value for expectation of transitions/transversions
         fix_omega = 1 – fixes omega to test for significance between current and foreground branch
         omega = 0.001 – not 100% sure here, but likely the initial value set for omega in current branch
         RateAncestor = 0 – ancestral sequence not constructed
         clock = 0 – assumption of a molecular clock is not allowed, rates are free to vary from branch to branch
         Small_Diff = 5e-08 – supposed to use multiple values to make sure your results are not sensitive to this number uses 1e-8 to 1e-5.
         Malpha = 0 – when substitution rates are assumed to vary from site to site, Malpha specifies whether one gamma distribution will be applied across all sites
         method = 0 – nucleotide substitution model
         ndata = 1 – number of separate datasets

         *
         * @param inputFile path to pal2nal output file
         */
        private String createCodemlInput(String inputFile, String sequenceCombination) {
            String outputPath = inputFile.replace("prot.paml","");
            String codeMlFile = inputFile.replace("prot.paml","codeml_settings.txt");
            String treeFile = "prot_trimmed.newick";
            if (!sequenceCombination.equals("")) { // pairwise
                treeFile = "dummy.tree";
                try {
                    FileUtils.touch(new File(outputPath + treeFile));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            String codemlInput =
                    "      seqfile = " + inputFile + "\n" +
                            "     treefile = " + outputPath + treeFile + "\n" +
                            "      outfile = " + outputPath + "codeml_output.txt\n\n" +
                            "        noisy = 0   * 0,1,2,3,9: how much rubbish on the screen\n" +
                            "      verbose = 0   * 1: detailed output, 0: concise output\n" +
                            "      runmode = -2  * 0: user tree;  1: semi-automatic;  2: automatic\n" +
                            "                    * 3: StepwiseAddition; (4,5):PerturbationNNI; -2: pairwise\n\n" +
                            "    cleandata = 1   * \"I added on 07/07/2004\" Mikita Suyama\n\n" +
                            "      seqtype = 1   * 1:codons; 2:AAs; 3:codons-->AAs\n" +
                            "    CodonFreq = 2   * 0:1/61 each, 1:F1X4, 2:F3X4, 3:codon table\n" +
                            "        model = 2\n" +
                            "                    * models for codons:\n" +
                            "                        * 0:one, 1:b, 2:2 or more dN/dS ratios for branches\n\n" +
                            "      NSsites = 0   * dN/dS among sites. 0:no variation, 1:neutral, 2:positive\n" +
                            "        icode = 0   * 0:standard genetic code; 1:mammalian mt; 2-10:see below\n" +
                            "        Mgene = 0   * 0:rates, 1:separate; 2:pi, 3:kappa, 4:all\n\n" +
                            "    fix_kappa = 0   * 1: kappa fixed, 0: kappa to be estimated\n" +
                            "        kappa = 2   * initial or fixed kappa\n" +
                            "    fix_omega = 0   * 1: omega or omega_1 fixed, 0: estimate\n" +
                            "        omega = 1   * initial or fixed omega, for codons or codon-transltd AAs\n\n" +
                            "    fix_alpha = 1   * 0: estimate gamma shape parameter; 1: fix it at alpha\n" +
                            "        alpha = .0  * initial or fixed alpha, 0:infinity (constant rate)\n" +
                            "       Malpha = 0   * different alphas for genes\n" +
                            "        ncatG = 4   * # of categories in the dG or AdG models of rates\n\n" +
                            "        clock = 0   * 0: no clock, unrooted tree, 1: clock, rooted tree\n" +
                            "        getSE = 0   * 0: don't want them, 1: want S.E.s of estimates\n" +
                            " RateAncestor = 0   * (1/0): rates (alpha>0) or ancestral states (alpha=0)\n" +
                            "       method = 0   * 0: simultaneous; 1: one branch at a time";
            writeStringToFile(codemlInput, codeMlFile, false, false);
            return codeMlFile;
        }
    }

    private HashSet<String> find_prepare_sequences_for_pal2_nal3(String seq_combi, String step_name, int seq_combi_counter) {
        seq_combi = seq_combi.replace("#", ",");
        HashSet<String> gene_id_combinations = new HashSet<>();
        //int synteny_node_counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> synteny_nodes = GRAPH_DB.findNodes(SYNTENY_LABEL);
            while (synteny_nodes.hasNext()) {
                Node synteny_node = synteny_nodes.next();
                String sequence_identifiers = (String) synteny_node.getProperty("sequence_identifiers");
                if (!seq_combi.equals(sequence_identifiers)) {
                    continue;
                }
                String[] seq_combi_array = sequence_identifiers.split(",");
                String[] seq_1_array = seq_combi_array[0].split("_");
                String[] seq_2_array = seq_combi_array[1].split("_");
                int genome1 = Integer.parseInt(seq_1_array[0]);
                int seq1 = Integer.parseInt(seq_1_array[1]);
                int genome2 = Integer.parseInt(seq_2_array[0]);
                int seq2 = Integer.parseInt(seq_2_array[1]);
                if (!selectedSequences.contains(genome1 + "_" + seq1) || !selectedSequences.contains(genome2 + "_" + seq2)){
                    continue;
                }
                //synteny_node_counter++;
                //if (synteny_node_counter == 1 || synteny_node_counter % 10 == 0) {
               //     System.out.print("\r" + seq_combi_counter + ". Sequence " + seq_combi.replace(",", " to ") +
                //            ". Retrieving synteny block " + synteny_node_counter + "             ");
                //}
                //System.out.println(sequence_identifiers);
                String renamed_seq_combi = seq_combi_array[0].replace("_","S") +"_" +
                        seq_combi_array[1].replace("_","S") ;

                Iterable<Relationship> relations = synteny_node.getRelationships(); // 'part_of' relationships
                HashMap<String, Node> mrna_per_position = new HashMap<>();
                for (Relationship rel : relations) {
                    int position = (int) rel.getProperty("position"); // position starts at 0
                    Node mrna_node = rel.getStartNode();
                    int genome_nr = (int) mrna_node.getProperty("genome");
                    int sequence_nr = (int) mrna_node.getProperty("sequence");
                    mrna_per_position.put(position + "#" + genome_nr + "_" + sequence_nr, mrna_node);
                }

                int block_size = (int) synteny_node.getProperty("block_size");
                for (int i = 0; i < block_size; i++) {
                    Node mrna_node1 = mrna_per_position.get(i + "#" + seq_combi_array[0]);
                    Node mrna_node2 = mrna_per_position.get(i + "#" + seq_combi_array[1]);
                    String gene_id_combi = mrna_node1.getId() + "_" + mrna_node2.getId();
                    gene_id_combinations.add(gene_id_combi);
                    switch (step_name) {
                        case "codon_alignment":
                            pal2nalQueue.add(WORKING_DIRECTORY + "alignments/pairwise/" + renamed_seq_combi + "/" + gene_id_combi + "/prot.fasta");
                            break;
                        case "codeml":
                            codemlQueue.add(WORKING_DIRECTORY + "alignments/pairwise/" + renamed_seq_combi + "/" + gene_id_combi + "/prot.paml");
                            break;
                        case "protein_alignment":
                            String id1 = (String) mrna_node1.getProperty("protein_ID");
                            String id2 = (String) mrna_node2.getProperty("protein_ID");
                            if (id1.equals(id2)) {
                                int[] address1 = (int[]) mrna_node1.getProperty("address");
                                int[] address2 = (int[]) mrna_node2.getProperty("address");
                                id1 = id1 + "_" + address1[0] + "_" + address1[1];
                                id2 = id2 + "_" + address2[0] + "_" + address2[1];
                            }
                            String prot_sequence1 = get_protein_sequence(mrna_node1);
                            String prot_sequence2 = get_protein_sequence(mrna_node2);
                            String nuc_sequence1 = get_nucleotide_sequence(mrna_node1);
                            String nuc_sequence2 = get_nucleotide_sequence(mrna_node2);
                            alignmentQueue.add(prot_sequence1 + "#" + prot_sequence2 + "#" + gene_id_combi
                                    + "#" + id1 + "#" + id2 + "#" + renamed_seq_combi);
                            createDirectory("alignments/pairwise/" + renamed_seq_combi + "/" + gene_id_combi, true);
                            write_string_to_file_in_DB(">" + id1 + "\n" + nuc_sequence1 + "\n>" + id2 + "\n" + nuc_sequence2,
                                    "alignments/pairwise/" + renamed_seq_combi + "/" + gene_id_combi + "/nuc_input.fasta");
                            break;
                    }
                }
            }
            tx.success();
        }
        return gene_id_combinations;
    }

    /**
     * Align (selected) homology groups. Groups must always be trimmed
     * @return
     */
    private ArrayList<Node> msaForDnds() {
        check_if_program_exists_stderr("mafft -h", 100, "MAFFT", true); // check if program is set to the $PATH
        check_if_program_exists_stderr("FastTree -h", 100, "Fasttree", true); // check if program is set to the $PATH
        FAST = false; // generate gene trees

        List<Long> hmNodeIds = new ArrayList<>();
        if (SELECTED_HMGROUPS != null) {
           try {
               hmNodeIds = parseHmFile(Paths.get(SELECTED_HMGROUPS));
           } catch (IOException e) {
               e.printStackTrace();
           }
        } else {
           try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
               ArrayList<Node> hmNodes = findHmNodes(null, 2);
               hmNodeIds = convertHmNodestoIds(hmNodes);
               tx.success();
           }
        }

        // only align protein sequences
        Mode += "#PROTEIN";
        MultipleSequenceAlignment msaProtein = new MultipleSequenceAlignment(
                "per_group",
                false,
                true,
                false,
                false,
                hmNodeIds
        );

        ArrayList<Node> hmNodeList = msaProtein.getHmNodeList(); //get hmNodeList, this list is not updated by msa
        hmNodeList = filterGroupsWithoutTrimmedOutput(hmNodeList);
        return filterGroupsWithOneProtein(hmNodeList);
    }

    /**
     * Remove homology groups from the analysis for which the alignment could not be trimmed
     * @param hmNodeList
     * @return
     */
    private ArrayList<Node> filterGroupsWithoutTrimmedOutput(ArrayList<Node> hmNodeList) {
        ArrayList<Node> newHmNodeList = new ArrayList<>();
        for (Node homologyNode : hmNodeList) {
            Path notTrimmedFile = Paths.get(WORKING_DIRECTORY).resolve("alignments").resolve("msa_per_group")
                    .resolve("grouping_v" + GROUPING_VERSION).resolve(homologyNode.getId() +"").resolve("input/not_trimmed");
            if (!notTrimmedFile.toFile().exists()) { //
                newHmNodeList.add(homologyNode);
            }
        }
        return newHmNodeList;
    }

    /**
     * Remove homology groups with one protein sequence
     * @param hmNodeList
     * @return
     */
    private ArrayList<Node> filterGroupsWithOneProtein(ArrayList<Node> hmNodeList) {
        ArrayList<Node> newHmNodeList = new ArrayList<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node homologyNode : hmNodeList) {
                int members = (int) homologyNode.getProperty("num_members");
                if (members == 1) {
                    continue;
                }
                newHmNodeList.add(homologyNode);
            }
            tx.success();
        }
       return newHmNodeList;
    }

    /**
     *
     * @param homologyNodes 'homology_group' node identifiers
     */
    private ArrayList<Node> checkPal2nalCodemlStatus(ArrayList<Node> homologyNodes) {
        int notDone = 0, done = 0;
        ArrayList<Node> groupsToBeDone = new ArrayList<>();
        for (Node homologyNode : homologyNodes) {
            String msaPath = WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/" +
                    homologyNode.getId() + "/output/";

            if ((done+notDone) % 500 == 0) {
                System.out.print("\r Checking codeml output: " + done + "/" + notDone);
            }

            if (!checkIfFileExists(msaPath + "prot.paml")) { // both pal2nal & codeml needs to be run
                groupsToBeDone.add(homologyNode);
                pal2nalQueue.add(msaPath + "prot.fasta");
            } else if (!checkIfFileExists(msaPath + "codeml_output.txt")) { // codeml needs to be run
                groupsToBeDone.add(homologyNode);
                codemlQueue.add(msaPath + "prot.paml");
                notDone++;
            } else if (!checkIfFileExists(msaPath + "dn_ds.txt")) {
                groupsToBeDone.add(homologyNode);
                convertCodemlFiles = true;
            } else {
                done++;
            }
        }
        System.out.println("");
        return groupsToBeDone;
    }

    private int adjustThreadsToQueueSize(BlockingQueue<String> queue) {
        int threadsToUse = THREADS;
        if (queue.size() < THREADS) {
            threadsToUse = queue.size();
        }
        return threadsToUse;
    }

    /**
     * Create .paml file
     */
    private void msaPal2nalforDnDs() {
        if (pal2nalQueue.size() > 0) {
            Pantools.logger.info("Converting multiple sequence alignments into a codon alignments with PAL2NAL.");
            try {
                int threadsToUse = adjustThreadsToQueueSize(pal2nalQueue);

                ExecutorService es = Executors.newFixedThreadPool(threadsToUse);
                for (int i = 1; i <= threadsToUse; i++) {
                    pal2nalQueue.add("stop");
                    es.execute(new run_pal2nal_parallel(0, "", 0));
                }
                System.out.println("");
                es.shutdown();
                es.awaitTermination(10, TimeUnit.DAYS);
            } catch (InterruptedException e) {
               throw new RuntimeException("Something went wrong during PAL2NAL");
            }
        }
    }

    /**
     * generates codeml_output.txt
     */
    private void msaCodeMLforDnDs() {
        if (codemlQueue.size() > 0) { // queue was filled by 'run_pal2nal_parallel'
            Pantools.logger.info("Calculating mutation rates with codeML.");
            try {
                int threadsToUse = adjustThreadsToQueueSize(codemlQueue);
                ExecutorService es = Executors.newFixedThreadPool(threadsToUse);
                for (int i = 1; i <= threadsToUse; i++) {
                    codemlQueue.add("stop");
                    es.execute(new runCodeMlParralel(codemlQueue.size(), "", 0));
                }
                System.out.println("");
                es.shutdown();
                es.awaitTermination(10, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                throw new RuntimeException("Something went wrong during CODEML");
            }
            convertCodemlFiles = true;
        }
    }

    private void msaExtractDnDsFromCodeML(ArrayList<Node> homologyNodes) {
        if (!convertCodemlFiles) {
            return;
        }
        int counter = 0;
        for (Node homologyNode : homologyNodes) {
            counter++;
            String msaPath = WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/" + homologyNode.getId() + "/output/";
            Path codemlOutput = Paths.get(msaPath + "codeml_output.txt");
            if (!checkIfFileExists(codemlOutput)) {
                Pantools.logger.error("{} is missing", codemlOutput);
                throw new RuntimeException("Unable to continue without " + codemlOutput);
            }

            if (counter % 100 == 0 || counter < 11 || counter == homologyNodes.size()) {
                System.out.print("\r Reading codeml output: group " + counter);
            }
            extractDnDsFromCodeML(codemlOutput, Paths.get(msaPath + "dn_ds.txt"));
        }
    }

    /**
     * Convert the CODEML output to a CSV file with only mrna identifiers and dn, ds, dn/ds values
     * @param codemlOutput output from CODEML
     * @param dndsOutput path to the output file to write to
     *
     * Example CODDEML file, first three comparisons of codeml.txt
     *
     * pairwise comparison, codon frequencies: F3x4.
     *
     *
     * 2 (5_2_C88_C09H2G050840.1) ... 1 (5_1_C88_C09H2G050880.1)
     * lnL = -127.521839
     *   0.70370  4.06233  0.69261
     *
     * t= 0.7037  S=    20.4  N=    48.6  dN/dS=  0.6926  dN = 0.2073  dS = 0.2994
     *
     *
     * 3 (5_3_C88_C09H1G020850.1) ... 1 (5_1_C88_C09H2G050880.1)
     * lnL = -128.962401
     *   0.68345  2.88654  0.63121
     *
     * t= 0.6835  S=    19.3  N=    49.7  dN/dS=  0.6312  dN = 0.1958  dS = 0.3101
     *
     *
     * 3 (5_3_C88_C09H1G020850.1) ... 2 (5_2_C88_C09H2G050840.1)
     * lnL =  -93.883727
     *   0.19450  9.51520  0.29865
     *
     * t= 0.1945  S=    16.5  N=    52.5  dN/dS=  0.2986  dN = 0.0415  dS = 0.1389
     *
     */
    private void extractDnDsFromCodeML(Path codemlOutput, Path dndsOutput) {
        boolean startReading = false;
        int lineCounter = 0;
        String[] line_array;
        StringBuilder output_builder = new StringBuilder("mRNA 1 id,mRNA 2 id,t,s,n,dn/ds,dn,ds\n");
        try {
            BufferedReader br = Files.newBufferedReader(codemlOutput);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.startsWith("pairwise comparison")) {
                    startReading = true;
                    continue;
                }
                if (startReading) {
                    // on every third line are the mrna ids
                    // on every seventh line are the mrna ids
                    lineCounter ++;
                    if (lineCounter == 3) {
                        line_array = line.split(" ");
                        String mrna1_id = line_array[1] = line_array[1].substring(1, line_array[1].length() - 1);
                        String mrna2_id = line_array[4] = line_array[4].substring(1, line_array[4].length() - 1);
                        output_builder.append(mrna1_id).append(",").append(mrna2_id).append(",");
                    } else if (lineCounter == 7) {
                        line_array = line.split("=");
                        String[] part1_array = line_array[1].split("S");
                        String[] part2_array = line_array[2].split("N");
                        String[] part3_array = line_array[3].split("dN/d");
                        String[] part4_array = line_array[4].split("dN");
                        String[] part5_array = line_array[5].split("dS");
                        String t = part1_array[0].replace(" ","");
                        String s = part2_array[0].replace(" ","");
                        String n = part3_array[0].replace(" ","");
                        String dnds = part4_array[0].replace(" ","");
                        String dn = part5_array[0].replace(" ","");
                        String ds = line_array[6].replace(" ","");
                        output_builder.append(t).append(",").append(s).append(",").append(n)
                                .append(",").append(dnds).append(",").append(dn).append(",").append(ds).append("\n");
                        lineCounter = 0;
                    }
                }
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to read: " + codemlOutput);
        }
        writeStringToFile(output_builder.toString(), String.valueOf(dndsOutput),  false, false);
    }

    /**
     *
     * @param homologyNodes
     * @param homologyNodeClassMap
     * @param selectedSequences sequence (identifiers) that are included in the current analysis
     * @return
     */
    private void combineMsaFilesPerSequence(ArrayList<Node> homologyNodes, HashMap<Node, String> homologyNodeClassMap,
                                            LinkedHashSet<String> selectedSequences) {

        ArrayList<Long> mismatchHomologyGroups = new ArrayList<>();
        HashMap<String, BufferedWriter> writerPerSequenceCombi = new HashMap<>();
        int counter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (Node homologyNode : homologyNodes) {
                counter ++;
                if (counter % 100 == 0 || counter < 11) {
                    System.out.print("\r Converting MSA to pairwise files: " + counter + "/" + homologyNodes.size());
                }
                HashMap<String, String> seqIdOfMrna = createProteinIDSeqIdMap(homologyNode);
                boolean firstLine = true;
                Path dndsFile = Paths.get(WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/" +
                        homologyNode.getId() + "/output/dn_ds.txt");
                if (!checkIfFileExists(dndsFile)) {
                    mismatchHomologyGroups.add(homologyNode.getId());
                    continue;
                }
                String classStr = homologyNodeClassMap.get(homologyNode);
                try {
                    BufferedReader br = Files.newBufferedReader(dndsFile);
                    for (String line = br.readLine(); line != null; line = br.readLine()) {
                        line = line.trim();
                        if (firstLine) { // skip the header
                            firstLine = false;
                            continue;
                        }

                        String[] lineArray = line.split(",");
                        String[] mrna1IdArray = lineArray[0].split("_");
                        String[] mrna2IdArray = lineArray[1].split("_");
                        int genomeNr1 = Integer.parseInt(mrna1IdArray[0]);
                        int genomeNr2 = Integer.parseInt(mrna2IdArray[0]);

                        String sequence1Id = seqIdOfMrna.get(lineArray[0]);
                        String sequence2Id = seqIdOfMrna.get(lineArray[1]);
                        if (sequence1Id == null || sequence2Id == null || sequence1Id.equals(sequence2Id)) {
                            continue;
                        }
                        String[] sequence1IdArray = sequence1Id.split("_");
                        String[] sequence2IdArray = sequence2Id.split("_");
                        int sequenceNr1 = Integer.parseInt(sequence1IdArray[1]);
                        int sequenceNr2 = Integer.parseInt(sequence2IdArray[1]);
                        if (!selectedSequences.contains(genomeNr1 + "_" + sequenceNr1) ||
                                !selectedSequences.contains(genomeNr2 + "_" + sequenceNr2) ) {
                            continue;
                        }

                        String[] orderedSeqArray = orderTwoSequenceIdentifiers(sequence1Id, sequence2Id);
                        boolean switched = false;
                        if (sequence1Id.equals(orderedSeqArray[1])) { // sequences were switched
                            switched = true;
                        }
                        sequence1Id = orderedSeqArray[0];
                        sequence2Id = orderedSeqArray[1];
                        //sequenceCombinations.add(sequence1Id + "#" + sequence2Id);
                        String renamedSeqId1 = sequence1Id.replace("_", "S"); // 1_1 becomes G1S1
                        String renamedSeqId2 = sequence2Id.replace("_", "S"); // 1_2 becomes G1S2

                        BufferedWriter writer;
                        if (writerPerSequenceCombi.containsKey(sequence1Id + " " + sequence2Id)) {
                            writer = writerPerSequenceCombi.get(sequence1Id + " " + sequence2Id);
                        } else {
                            writer = new BufferedWriter(new FileWriter( WORKING_DIRECTORY + "dn_ds/homologs/between_sequences/" +
                                    renamedSeqId1 + "_" + renamedSeqId2 + "_dnds_values.csv"));
                            writer.write("type,value,log10_value,class,mrnaId1,mrnaId2\n");
                        }

                        if (switched) {
                            writeDnDsLog10ToWriter(writer, lineArray, classStr, lineArray[1], lineArray[0]);
                        } else {
                            writeDnDsLog10ToWriter(writer, lineArray, classStr, lineArray[0], lineArray[1]);
                        }
                        writerPerSequenceCombi.put(sequence1Id + " " + sequence2Id, writer);
                    }
                } catch (IOException ioe) {
                   throw new RuntimeException("Something went wrong while reading: " + dndsFile);
                }
            }
            tx.success();
        }
        closePerSequenceCombinationWriters(writerPerSequenceCombi);
        if (!mismatchHomologyGroups.isEmpty()) {
            Pantools.logger.warn("The calculation failed for " + mismatchHomologyGroups.size() + " homology groups. " +
                    "See " + WORKING_DIRECTORY + "dn_ds/failed_groups.txt");
            writeStringToFile("Homology groups missing file: " + WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/" +
                   "HOMOLOGY_GROUP_ID/output/dn_ds.txt\n" + mismatchHomologyGroups.toString().replace("[","").replace("]","").replace(" ",""),
                    "dn_ds/failed_groups.txt", true, false);
        }

        if (homologyNodes.size() == mismatchHomologyGroups.size()) {
           throw new RuntimeException("The calculation failed for all homology groups.");
        }
    }

    /**
     * Expects line from dn_ds.txt
     *
     * Example file:
     * mRNA 1 id,mRNA 2 id,t,s,n,dn/ds,dn,ds
     * 4_1_gene-GU280_gp02,1_1_gene-GU280_gp02,0.0000,632.1,3186.9,0.0010,0.0000,0.0000
     * 3_1_gene-GU280_gp02,1_1_gene-GU280_gp02,0.0000,709.6,3109.4,0.0263,0.0000,0.0000
     * 3_1_gene-GU280_gp02,4_1_gene-GU280_gp02,0.0000,632.1,3186.9,0.1188,0.0000,0.0000
     * 2_1_gene-GU280_gp02,1_1_gene-GU280_gp02,0.0008,972.1,2846.9,0.0010,0.0000,0.0010
     * 2_1_gene-GU280_gp02,4_1_gene-GU280_gp02,0.0008,972.1,2846.9,0.0010,0.0000,0.0010
     * 2_1_gene-GU280_gp02,3_1_gene-GU280_gp02,0.0008,972.1,2846.9,0.0010,0.0000,0.0010
     *
     * @param writer
     * @param lineArray
     * @param classStr
     * @param mrnaIdA
     * @param mrnaIdB
     */
    private void writeDnDsLog10ToWriter(BufferedWriter writer, String[] lineArray, String classStr, String mrnaIdA, String mrnaIdB) {
        if (mrnaIdA.contains(",") || mrnaIdB.contains(",")) { // not allowed, will break csv file
            throw new RuntimeException(mrnaIdA + " or " + mrnaIdB + "contains a comma");
        }

        try {
            if (!lineArray[6].equals("0.0000")) {
                writer.write("Dn," + lineArray[6] + "," + Math.log10(Double.parseDouble(lineArray[6])) + "," + classStr + "," + mrnaIdA + "," + mrnaIdB + "\n");
            } else if (allowZeros) {
                writer.write("Dn," + lineArray[6] + ",-4," + classStr + "," + mrnaIdA + "," + mrnaIdB + "\n");
            }
        } catch (NumberFormatException nfe) {
            // missing dn value
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (!lineArray[7].equals("0.0000")) {
                writer.write("Ds," + lineArray[7] + "," + Math.log10(Double.parseDouble(lineArray[7])) + "," + classStr + "," + mrnaIdA + "," + mrnaIdB + "\n");
            } else if (allowZeros) {
                writer.write("Ds," + lineArray[7] + ",-4," + classStr + "," + mrnaIdA + "," + mrnaIdB + "\n");
            }

        } catch (NumberFormatException nfe) {
            // missing ds value
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer.write("Dn/Ds," + lineArray[5] + "," + Math.log10(Double.parseDouble(lineArray[5])) + "," + classStr + "," + mrnaIdA + "," + mrnaIdB + "\n");
        } catch (NumberFormatException nfe) {
            // missing dn/ds value
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close the files
     */
    private void closePerSequenceCombinationWriters(HashMap<String, BufferedWriter> writerPerSequenceCombi) {
        int counter = 0;
        try {
            for (String sequenceCombination : writerPerSequenceCombi.keySet()) {
                counter++;
                if (counter % 100 == 0 || counter < 11) {
                    System.out.print("\r Writing pairwise files: " + counter + "/" + writerPerSequenceCombi.size());
                }
                BufferedWriter writer = writerPerSequenceCombi.get(sequenceCombination);
                writer.close();
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Unable correctly close files in: " + WORKING_DIRECTORY + "dn_ds/homologs/between_sequences/");
        }
    }

    /**
     * Read sequences.info to obtain the sequence identifiers.
     * File is in input directory of an homology group msa
     *
     * example lines
     * #genome, gene/mRNA name, mRNA identifier, node identifier, address, strand
     * 13, psbA, 13_1_mRNA-LasaCp001, 21877, 13 1 24363 25424, -
     * 10, psbA, 10_1_mRNA-LasaCp001, 20650, 10 1 25607 26668, -
     *
     * @param hmNode
     */
    private HashMap<String, String> createProteinIDSeqIdMap(Node hmNode) {
        boolean readLines = false;
        HashMap<String, String> seqIdOfMrna = new HashMap<>();
        Path msaSequenceInfo = Paths.get(WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/"
                + hmNode.getId() + "/input/sequences.info");
        try {
            BufferedReader br = Files.newBufferedReader(msaSequenceInfo);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = line.trim();
                if (line.startsWith("#genome")) {
                    readLines = true;
                } else if (readLines) {
                    String[] lineArray = line.split(", ");
                    String[] lineArray2 = lineArray[4].split(" ");
                    seqIdOfMrna.put(lineArray[2] + "#id", lineArray[3]); // store the  mrna node identifier
                    int genomeNr = Integer.parseInt(lineArray2[0]);
                    if (!selectedGenomes.contains(genomeNr)) {
                        continue;
                    }
                    seqIdOfMrna.put(lineArray[2], lineArray2[0] + "_" + lineArray2[1]);
                }
            }
        } catch (IOException ioe) {
           throw new RuntimeException("Something went wrong while reading: " + msaSequenceInfo);
        }
        return seqIdOfMrna;
    }
}
