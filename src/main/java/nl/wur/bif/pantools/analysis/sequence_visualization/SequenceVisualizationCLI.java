package nl.wur.bif.pantools.analysis.sequence_visualization;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.setGenomeSelectionOptions;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Command line interface class for pantools sequence_visualisation
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "sequence_visualization", sortOptions = false, abbreviateSynopsis = true)
public class SequenceVisualizationCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = {"--rules"})
    private Path rulesFile;

    @Option(names = {"--window-size"})
    @Min(value = 10000)
    @Max(value = 100000000)
    private int windowSize;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes);
        pantools.setPangenomeGraph();
        setGenomeSelectionOptions(selectGenomes);
        new SequenceVisualization().sequenceVisualization(windowSize, rulesFile);
        return 0;
    }
}
