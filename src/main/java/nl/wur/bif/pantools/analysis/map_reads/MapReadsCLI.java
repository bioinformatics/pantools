package nl.wur.bif.pantools.analysis.map_reads;

import jakarta.validation.constraints.*;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.*;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Map single or paired-end short reads to one or multiple genomes in the pangenome.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "map", sortOptions = false, abbreviateSynopsis = true)
@OutputDirectory(directory = "outDirectory")
public class MapReadsCLI implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "short-read-file", index = "1+", arity = "1..2")
    @InputFiles(message = "{file.short-read}")
    Path[] shortReadFiles;

    @Option(names = {"-o", "--output"})
    Path outDirectory;

    @Pattern(regexp = "none|random|all", flags = CASE_INSENSITIVE, message = "{pattern.best-hits}")
    static String bestHits = "random";
    static boolean allHits;
    static boolean competitive;

    @ArgGroup
    HitsOptions hitsOptions;
    static class HitsOptions {
        @Option(names = "--all-hits")
        void isAllHits(boolean value) {
            allHits = value;
        }

        @ArgGroup(exclusive = false)
        BestHitsOptions bestHitsOptions;
        static class BestHitsOptions {
            @Option(names = "--best-hits", paramLabel = "none|random|all")
            void setBestHits(String value) {
                bestHits = value;
            }

            @Option(names = "--competitive")
            void isCompetitive(boolean value) {
                competitive = value;
            }
        }
    }

    @Option(names = "--previous-run")
    @InputFile(message = "{file.previous-run}")
    Path previousRunFile;

    @Option(names = "--out-format", paramLabel = "BAM|SAM|none")
    @Pattern(regexp = "SAM|BAM|none", flags = CASE_INSENSITIVE, message = "{pattern.out-format}")
    String outFormat;

    @Option(names = "--gap-open")
    @Min(value = -50, message = "{min.gap-open}")
    @Max(value = -1, message = "{max.gap-open}")
    int gapOpen;

    @Option(names = "--gap-extension")
    @Min(value = -5, message = "{min.gap-ext}")
    @Max(value = -1, message = "{max.gap-ext}")
    int gapExtension;

    @Option(names = "--interleaved")
    boolean interleaved;

    @Option(names = "--unmapped")
    boolean checkUnmapped;


    @Option(names = "--read-group")

    void rgstringToMap(String value){
        String rg = value.replace("\\t", "\t");
        String[] tokens = rg.split("\t");
        readGroup = new HashMap<>();
        for (String element : tokens) {
            readGroup.put(element.split(":")[0], element.split(":")[1]);
        }
    }
    
    @Patterns(regexp = "BC|CN|DS|DT|FO|KS|LB|PG|PI|PL|PM|PU|SM|ID", message = "{pattern.read-group}")
    HashMap<String, String> readGroup;

    @DecimalMin(value = "0", message = "{min.min-identity}")
    @DecimalMax(value = "1", message = "{max.min-identity}")
    static double minIdentity = 0.5;

    @Min(value = 0, message = "{min.kmers}")
    static int numKmerSamples = 15;

    @Min(value = 10, message = "{min.min-hit-length}")
    @Max(value = 100, message = "{max.min-hit-length}")
    static int minHitLength = 13;

    @Min(value = 50, message = "{min.max-alignment-length}")
    @Max(value = 5000, message = "{max.max-alignment-length}")
    static int maxAlignmentLength = 2000;

    @Min(value = 50, message = "{min.max-fragment-length}")
    @Max(value = 5000, message = "{max.max-fragment-length}")
    static int maxFragmentLength = 4998;

    @Min(value = 1, message = "{min.max-num-locations}")
    @Max(value = 100, message = "{max.max-num-locations}")
    static int maxNumLocations = 15;

    @Min(value = 1, message = "{min.alignment-band}")
    @Max(value = 100, message = "{max.alignment-band}")
    static int alignmentBand = 5;

    @Min(value = 0, message = "{range.clipping-stringency}")
    @Max(value = 3, message = "{range.clipping-stringency}")
    static int clippingStringency = 1;

    @ArgGroup Sensitivity sensitivity;
    static class Sensitivity {
        @Option(names = {"-s", "--sensitivity"}, paramLabel = "very-fast|fast|sensitive|very-sensitive")
        public void setPreset(String value) {
            int preset;
            switch (value) {
                case "very-fast": preset = 0; break;
                case "fast": preset = 1; break;
                case "sensitive": preset = 2; break;
                case "very-sensitive": preset = 3; break;
                default:
                    throw new ParameterException(spec.commandLine(), "Invalid value for option '--preset'. " +
                            "Select between: [very-fast|fast|sensitive|very-sensitive]");
            }
            minIdentity = new double[]{0.8, 0.7, 0.6, 0.5}[preset];
            numKmerSamples = new int[]{7, 15, 23, 30}[preset];
            maxNumLocations = new int[]{7, 15, 23, 30}[preset];
            alignmentBand = new int[]{3, 6, 9, 12}[preset];
            clippingStringency = new int[]{0, 1, 2, 3}[preset];
        }

        @ArgGroup(exclusive = false) SensitivityOptions sensitivityOptions;
    }

    static class SensitivityOptions {
        @Option(names = "--min-identity")
        void setMinIdentity(double value) {minIdentity = value;}

        @Option(names = "--num-kmer-samples")
        void setNumKmerSamples(int value) {numKmerSamples = value;}

        @Option(names = "--min-hit-length")
        void setMinHitLength(int value) {minHitLength = value;}

        @Option(names = "--max-alignment-length")
        void setMaxAlignmentLength(int value) {maxAlignmentLength = value;}

        @Option(names = "--max-fragment-length")
        void setMaxFragmentLength(int value) {maxFragmentLength = value;}

        @Option(names = "--max-num-locations")
        void setMaxNumLocations(int value) {maxNumLocations = value;}

        @Option(names = "--alignment-band")
        void setAlignmentBand(int value) {alignmentBand = value;}

        @Option(names = "--clipping-stringency")
        void setClippingStringency(int value) {clippingStringency = value;}
    }

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, hitsOptions);

        pantools.setPangenomeGraph("pangenome");
        setGlobalParameters(); //TODO: use local parameters instead

        new MapReads();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        PATH_TO_THE_FIRST_SRA = shortReadFiles[0].toString();
        if (shortReadFiles.length == 2) PATH_TO_THE_SECOND_SRA = shortReadFiles[1].toString();
        if (previousRunFile != null) RAW_ABUNDANCE_FILE = previousRunFile.toString();
        if (outDirectory != null) OUTPUT_PATH = outDirectory + "/";
        THREADS = threadNumber.getnThreads();
        OUTFORMAT = outFormat;
        setReadGroup(readGroup);
        if (allHits) {
            ALIGNMENT_MODE = 0;
        } else {
            switch (bestHits) {
                case "none":
                    ALIGNMENT_MODE = 1;
                    break;
                case "random":
                    ALIGNMENT_MODE = 2;
                    break;
                case "all":
                    ALIGNMENT_MODE = 3;
                    break;
                default:
                    throw new IllegalArgumentException(
                            String.format("Value for bestHits not recognized (%s).", bestHits));
            }
            if (competitive) ALIGNMENT_MODE *= -1;
        }
        MIN_IDENTITY = minIdentity;
        NUM_KMER_SAMPLES = numKmerSamples;
        MAX_NUM_LOCATIONS = maxNumLocations;
        ALIGNMENT_BOUND = alignmentBand;
        CLIPPING_STRINGENCY = clippingStringency;
        MIN_HIT_LENGTH = minHitLength;
        MAX_ALIGNMENT_LENGTH = maxAlignmentLength;
        MAX_FRAGMENT_LENGTH = maxFragmentLength;
        INTERLEAVED = interleaved;
        if (checkUnmapped) VERYSENSITIVE = true;
    }

    public Pantools getPantools() {
        return pantools;
    }
    public Path getOutDirectory() {
        return  outDirectory;
    }
}


