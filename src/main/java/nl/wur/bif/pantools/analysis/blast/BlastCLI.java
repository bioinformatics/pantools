package nl.wur.bif.pantools.analysis.blast;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.cli.mixins.ThreadNumber;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static jakarta.validation.constraints.Pattern.Flag.CASE_INSENSITIVE;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.THREADS;
import static nl.wur.bif.pantools.utils.Globals.setGenomeSelectionOptions;
import static picocli.CommandLine.*;

/**
 * Command line interface class for pantools blast
 *
 * @author Eef Jonkheer, Bioinformatics Group, Wageningen University, the Netherlands.
 */
@Command(name = "blast", sortOptions = false, abbreviateSynopsis = true)
public class BlastCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @Mixin private ThreadNumber threadNumber;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Parameters(descriptionKey = "fasta", index = "0+")
    @InputFile(message = "{file.fasta}")
    private Path fastaFile;

    @Option(names = {"--alignment-threshold"})
    @Min(value = 1)
    @Max(value = 100)
    private int alignmentThreshold;

    @Option(names = {"--minimum-identity"})
    @Min(value = 1)
    @Max(value = 100)
    private int minimumIdentity;

    @Option(names = {"--rebuild"})
    private boolean rebuildDatabase;

    @Option(names = {"--mode"})
    @Pattern(regexp = "blastn|blastp|blastx|tblastx|tblastn|adaptive", flags = CASE_INSENSITIVE,
            message = "{pattern.mode-blast}")
    private String blastProgram;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, threadNumber, selectGenomes);
        pantools.setPangenomeGraph();
        setGlobalParameters();
        new Blast().blast(fastaFile, alignmentThreshold, minimumIdentity, rebuildDatabase, blastProgram);
        return 0;
    }

    private void setGlobalParameters() {
        THREADS = threadNumber.getnThreads();
        setGenomeSelectionOptions(selectGenomes);
    }
}
