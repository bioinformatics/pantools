package nl.wur.bif.pantools.analysis.function_analysis;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerList;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Identify over or underrepresented GO terms in a set of genes.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "go_enrichment", sortOptions = false)
public class GoEnrichmentCLI implements Callable<Integer> {

    @Spec static CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @ArgGroup(multiplicity = "1") Identifiers identifiers;
    private static class Identifiers {
        @Option(names = {"-H", "--homology-file"})
        @InputFile(message = "{file.homology}")
        Path homologyFile;

        @Option(names = {"-n", "--nodes"}, required = true)
        void setNodes(String value) {
            nodes = stringToIntegerList(value);
        }
        @Size(min = 1, message = "{size.empty.node}")
        List<Integer> nodes;
    }

    @Option(names = "--fdr")
    @Min(value = 1, message = "{min.fdr}")
    @Max(value = 100, message = "{max.fdr}")
    int falseDiscoveryRate;

    @Override
    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, this, selectGenomes, identifiers);

        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead

        functionalAnnotations.go_enrichment();
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        if (identifiers.homologyFile != null) SELECTED_HMGROUPS = identifiers.homologyFile.toString();
        if (identifiers.nodes != null) {
            NODE_ID = identifiers.nodes.toString().replaceAll("[\\[\\]]", "");
            if (NODE_ID.matches("^[0-9]*$") && NODE_ID.length() > 0) NODE_ID_long = Long.parseLong(NODE_ID);
        }
        NODE_VALUE = Integer.toString(falseDiscoveryRate);
    }

}
