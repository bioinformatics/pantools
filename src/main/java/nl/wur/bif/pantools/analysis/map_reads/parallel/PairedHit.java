package nl.wur.bif.pantools.analysis.map_reads.parallel;

/**
 * Implements a class for genomic hit of a paired_end-layout read
 */
public class PairedHit {
    public int fragment_length;
    public SingleHit h1;
    public SingleHit h2;
    public PairedHit(int flen, int gn1, int sq1, double idn1, int sc1, int st1, int off1, int len1, int del1, boolean fw1, String cg1, String r1,
                      int gn2, int sq2, double idn2, int sc2, int st2, int off2, int len2, int del2, boolean fw2, String cg2, String r2) {
        fragment_length = flen;
        h1.genome = gn1;
        h1.sequence = sq1;
        h1.identity = idn1;
        h1.score = sc1;
        h1.start = st1;
        h1.offset = off1;
        h1.length = len1;
        h1.deletions = del1;
        h1.forward = fw1;
        h1.cigar = cg1;
        h1.reference = r1;

        h2.genome = gn2;
        h2.sequence = sq2;
        h2.identity = idn2;
        h2.score = sc2;
        h2.start = st2;
        h2.offset = off2;
        h2.length = len2;
        h2.deletions = del2;
        h2.forward = fw2;
        h2.cigar = cg2;
        h2.reference = r2;
    }

    public PairedHit(int f_len, SingleHit hit1, SingleHit hit2) {
        fragment_length = f_len;
        h1 = hit1;
        h2 = hit2;
    }

    public int get_score() {
        return h1.score + h2.score;
    }

    public int get_min_start() {
        return Math.min(h1.start, h2.start);
    }

    public int get_max_start() {
        return Math.max(h1.start, h2.start);
    }

    @Override
    public String toString() {
        return "(genome1:" + h1.genome +
                ",sequence1:" + h1.sequence +
                ",identity1:" + h1.identity +
                ",score1:" + h1.score +
                ",start1:" + h1.start +
                ",offset1:" + h1.offset +
                ",length1:" + h1.length +
                ",deletions1:" + h1.deletions +
                ",forward1:" + h1.forward +
                ",reference1:" + h1.reference +
                ",cigar1:" + h1.cigar +")" +
                "\n" +
                "(genome2:" + h2.genome +
                ",sequence2:" + h2.sequence +
                ",identity2:" + h2.identity +
                ",score2:" + h2.score +
                ",start2:" + h2.start +
                ",offset2:" + h2.offset +
                ",length2:" + h2.length +
                ",deletions2:" + h2.deletions +
                ",forward2:" + h2.forward +
                ",reference2:" + h2.reference +
                ",cigar2:" + h2.cigar +")";
    }
}
