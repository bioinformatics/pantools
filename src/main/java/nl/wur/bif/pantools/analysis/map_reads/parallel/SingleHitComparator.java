package nl.wur.bif.pantools.analysis.map_reads.parallel;

import java.util.Comparator;

/**
 * Implements a comparator for single genomic hits
 */
public class SingleHitComparator implements Comparator<SingleHit> {
    @Override
    public int compare(SingleHit x, SingleHit y) {
        if (x.score > y.score)
            return -1;
        else if (x.score < y.score)
            return 1;
        else if (x.sequence > y.sequence)
            return -1;
        else if (x.sequence < y.sequence)
            return 1;
        else return Integer.compare(y.start, x.start);
    }
}
