package nl.wur.bif.pantools.analysis.gene_classification;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import nl.wur.bif.pantools.utils.GraphUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;
import static picocli.CommandLine.Model.CommandSpec;

/**
 * Classify the gene repertoire as core, accessory or unique.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "gene_classification", sortOptions = false, abbreviateSynopsis = true)
public class GeneClassificationCLI implements Callable<Integer> {

    @Spec CommandSpec spec;
    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    GeneClassification newClassification = new GeneClassification();

    @Option(names = {"--phasing"})
    boolean phasing;

    @Option(names = {"--sequence"})
    boolean sequence;

    @ArgGroup
    VariationOptions variationOptions;
    static boolean pavs;
    static String phenotype;
    static boolean mlsa;

    static class VariationOptions {
        @Option(names = {"--pavs"})
        void isVariation(boolean value) {
            pavs = value;
        }

        @ArgGroup(exclusive = false)
        NoVariationOptions noVariationOptions;
        static class NoVariationOptions {
            @Option(names = {"-p", "--phenotype"})
            void setPhenotype(String value) {
                phenotype = value;
            }

            @Option(names = {"--mlsa"})
            void isMlsa(boolean value) {
                mlsa = value;
            }
        }
    }

    @Option(names = "--core-threshold")
    @Min(value = 0, message = "{min.ct}")
    @Max(value = 100, message = "{max.ct}")
    int coreThreshold;

    @Option(names = "--unique-threshold")
    @Min(value = 0, message = "{min.ut}")
    @Max(value = 100, message = "{max.ut}")
    int uniqueThreshold;

    @Option(names = "--phenotype-threshold")
    @Min(value = 0, message = "{min.pt}")
    @Max(value = 100, message = "{max.pt}")
    int phenotypeThreshold;

    @Override
    public Integer call() throws IOException {
        // initialize logging
        pantools.createLogger(spec);

        // validate command line arguments
        BeanUtils.argValidation(spec, this, selectGenomes);

        // set global parameters
        setGlobalParameters(); //TODO: use local parameters instead

        // initialize the neo4j graph database
        final Path databaseDirectory = pantools.getDatabaseDirectory();
        GraphUtils.createGraphDatabaseService(databaseDirectory);
        GraphUtils.setDatabaseParameters();

        // validate the neo4j graph database
        GraphUtils.validateHomologyGroups();
        if (pavs) GraphUtils.validateAccessions("PAV");

        // run the main code
        try {
            newClassification.geneClassification(pavs);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }

        // return exit code 0 if successful
        return 0;
    }

    private void setGlobalParameters() {
        setGenomeSelectionOptions(selectGenomes);
        core_threshold = coreThreshold;
        unique_threshold = uniqueThreshold;
        phenotype_threshold = phenotypeThreshold;
        PHENOTYPE = phenotype;
        if (mlsa) Mode = "MLSA";
        PHASED_ANALYSIS = phasing;
        compare_sequences = sequence;
    }

}
