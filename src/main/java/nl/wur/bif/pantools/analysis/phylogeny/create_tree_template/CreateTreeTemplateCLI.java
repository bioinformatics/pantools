package nl.wur.bif.pantools.analysis.phylogeny.create_tree_template;


import jakarta.validation.constraints.Positive;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.utils.BeanUtils;
import picocli.CommandLine.Model.CommandSpec;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.Callable;

import static nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import static nl.wur.bif.pantools.utils.Globals.*;
import static picocli.CommandLine.*;

/**
 * Create templates for coloring phylogenetic trees in iTOL.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
@Command(name = "create_tree_template", sortOptions = false)
public class CreateTreeTemplateCLI implements Callable<Integer> {

    @Spec CommandSpec spec;

    @ArgGroup private SelectGenomes selectGenomes;

    @ParentCommand
    @GraphDatabase
    private Pantools pantools;

    @Option(names = "--color")
    @Positive(message = "{positive.color")
    int color;

    @Option(names = {"-p", "--phenotype"})
    String phenotype;

    @Option(names = {"--no-numbers"})
    boolean excludeGenomeNumbers;

    @Option(names = {"--phasing"})
    boolean phasing;

    @ArgGroup(multiplicity = "1") GenomeOrSequence genomeOrSequence;
    private static class GenomeOrSequence {
        @Option(names = {"--sequence"})
        boolean sequence;

        @Option(names = {"--genome"})
        boolean genome;

        @Option(names = {"--gene-tree"})
        Path geneTree;
    }

    @Override

    public Integer call() throws IOException {
        pantools.createLogger(spec);
        BeanUtils.argValidation(spec, genomeOrSequence, this);
        pantools.setPangenomeGraph();
        setGlobalParameters(); //TODO: use local parameters instead
        //phylogeny.create_tree_templates();
        setGenomeSelectionOptions(selectGenomes);
        CreateTreeTemplate template = new CreateTreeTemplate();
        String templateType = determineTemplateType();
        template.createTreeTemplates(templateType, genomeOrSequence.geneTree, excludeGenomeNumbers, color);
        return 0;
    }

    private String determineTemplateType() {
        if (genomeOrSequence.genome) {
            compare_genomes = true;
            return "genome";
        } else if (genomeOrSequence.sequence) {
            compare_sequences = true;
            return "sequence";
        } // else, a gene tree file was included
        return "gene_tree";
    }

    private void setGlobalParameters() {
        PHENOTYPE = phenotype;
        //NODE_VALUE = String.valueOf(color);
        if (phasing) {
            PHASED_ANALYSIS = true;
        }
    }

}
