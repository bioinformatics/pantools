package nl.wur.bif.pantools.utils.cli.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import nl.wur.bif.pantools.utils.cli.validation.validators.*;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Class containing custom Bean validation constraint interfaces.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class Constraints {

    /**
     * Validate the graph database directory and database path.
     * Usage: @GraphDatabase Pantools pantools
     */
    @Documented
    @Target({FIELD, TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = PantoolsDatabaseValidator.class)
    public @interface GraphDatabase {

        String message() default "Directory not found (${validatedValue})";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validate an input directory.
     * Usage: @InputDirectory Path directory
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE, TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = InputDirectoryValidator.class)
    public @interface InputDirectory {

        String message() default "${validatedValue} is not a valid directory";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validate an input file.
     * Usage: @InputFile Path file
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = InputFileValidator.class)
    public @interface InputFile {

        String message() default "File not found (${validatedValue})";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validate multiple input files.
     * Usage: @InputFiles Path[] files
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = InputFilesValidator.class)
    public @interface InputFiles {

        String message() default "File does not exist (${validatedValue})";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validate whether the scoring matrix exist in resources/scoring-matrices.
     * Usage: @ScoringMatrix String matrixName
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = ScoringMatrixValidator.class)
    public @interface ScoringMatrix {

        String message() default "Matrix does not exist: (${validatedValue})";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    /**
     * Validate an optional integer that has a minimum, bus is 0 when unassigned.
     * Usage: @MinOrZero(value=x) int integer
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = MinOrZeroValidator.class)
    public @interface MinOrZero {

        String message() default "{jakarta.validation.constraints.Min.message}";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        long value();
    }

    /**
     * Match a list of strings with a regular expression
     * Usage: @Patterns(regexp="regexp") List<String> strings;
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Repeatable(Patterns.List.class)
    @Documented
    @Constraint(validatedBy = {PatternListValidator.class, PatternMapValidator.class})
    public @interface Patterns {

        String regexp();

        Patterns.Flag[] flags() default {};

        String message() default "{jakarta.validation.constraints.Pattern.message}";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @interface List {
            Patterns[] value();
        }

        enum Flag {
            UNIX_LINES(1),
            CASE_INSENSITIVE(2),
            COMMENTS(4),
            MULTILINE(8),
            DOTALL(32),
            UNICODE_CASE(64),
            CANON_EQ(128);

            private final int value;

            Flag(int value) {
                this.value = value;
            }

            public int getValue() {
                return this.value;
            }
        }
    }

    /**
     * Match an integer with an allowed selection of integers
     * Usage: @MatchInteger(value={x,y,z}) int integer;
     */
    @Documented
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = MatchIntegerValidator.class)
    public @interface MatchInteger {

        String message() default "${validatedValue} is not a valid value";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        int[] value();
    }

    /**
     * Validates and output directory
     * Usage: <code>@OutputDirectory Path directory</code>
     */
    @Documented
    @Target({FIELD, TYPE})
    @Retention(RUNTIME)
    @Constraint(validatedBy = OutputDirectoryValidator.class)
    public @interface OutputDirectory {

        String message() default "The output directory already exists and contains files";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        String directory();

        /**
         * Defines several <code>@OutputDirectory</code> annotations on the same element
         *
         * @see OutputDirectory
         */
        @Target({TYPE, ANNOTATION_TYPE})
        @Retention(RUNTIME)
        @Documented
        @interface List {
            OutputDirectory[] value();
        }
    }

    /**
     * Negative regex pattern validator for "forbidden" strings.
     * Usage: @ExcludePatterns(regexp="forbidden,words",field="fieldName") class Class
     */
    @Target(TYPE)
    @Retention(RUNTIME)
    @Repeatable(ExcludePatterns.List.class)
    @Documented
    @Constraint(validatedBy = ExcludePatternsValidator.class)
    public @interface ExcludePatterns {

        // forbidden word(s) separated by comma
        String regexp();

        // field to validate; this constraint is only class-level to access Pantools.isForce() and Pantools.isInput()
        String field();

        // if set the user can ignore this constraint by confirming the query
        // the query is always followed by ", are you sure you want to continue?"
        String query() default "";

        ExcludePatterns.Flag[] flags() default {};

        String message() default "${validatedValue} is not a valid value";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        @Target(TYPE)
        @Retention(RetentionPolicy.RUNTIME)
        @Documented
        @interface List {
            ExcludePatterns[] value();
        }

        enum Flag {
            UNIX_LINES(1),
            CASE_INSENSITIVE(2),
            COMMENTS(4),
            MULTILINE(8),
            DOTALL(32),
            UNICODE_CASE(64),
            CANON_EQ(128);

            private final int value;

            Flag(int value) {
                this.value = value;
            }

            public int getValue() {
                return this.value;
            }
        }
    }
}
