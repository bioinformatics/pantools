package nl.wur.bif.pantools.utils.cli.validation.validators;

import nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFile;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.nio.file.Files;
import java.nio.file.Path;

import static nl.wur.bif.pantools.utils.BeanUtils.setViolationMessage;

/**
 * Custom ConstraintValidator that verifies that an input file exists.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class InputFileValidator implements ConstraintValidator<InputFile, Path> {

    @Override
    public void initialize(InputFile constraintAnnotation) {
    }

    /**
     * Verifies whether a path is an existing directory.
     * @param file root path to input file
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true when the file exists or is unassigned, false otherwise
     */
    @Override
    public boolean isValid(Path file, ConstraintValidatorContext context) {
        if (file == null) return true;
        if (Files.isDirectory(file)) {
            setViolationMessage(context, String.format("Input file is a directory, not a file (%s).", file));
            return false;
        }
        return Files.exists(file);
    }
}
