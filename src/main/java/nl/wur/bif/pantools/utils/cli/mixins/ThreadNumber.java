package nl.wur.bif.pantools.utils.cli.mixins;

import jakarta.validation.constraints.Positive;

import static picocli.CommandLine.Option;

/**
 * Mixin class for '--threads' option.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class ThreadNumber {

    @Option(names = {"-t", "--threads"})
    void setnThreads(int value) {
        if (value == 0) {
            int cores = Runtime.getRuntime().availableProcessors();
            value = Math.min(cores, 8);
        }
        nThreads = value;
    }

    @Positive(message = "{positive.threads}")
    private int nThreads;

    public int getnThreads() {
        return  nThreads;
    }
}
