package nl.wur.bif.pantools.utils.cli.validation.validators;

import nl.wur.bif.pantools.utils.cli.validation.Constraints.InputFiles;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.nio.file.Files;
import java.nio.file.Path;

import static nl.wur.bif.pantools.utils.BeanUtils.setViolationMessage;

/**
 * Custom ConstraintValidator that verifies that all files in an array of input file exist.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class InputFilesValidator implements ConstraintValidator<InputFiles, Path[]> {

    @Override
    public void initialize(InputFiles constraintAnnotation) {
    }

    /**
     * Verifies whether a path is an existing directory.
     * @param files array of root paths to input files
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true when all files exist or no files are assigned, false otherwise
     */
    @Override
    public boolean isValid(Path[] files, ConstraintValidatorContext context) {
        if (files == null) return true;
        for (Path file : files) {
            if (!Files.exists(file)) return false;

            if (Files.isDirectory(file)) {
                setViolationMessage(context, String.format("Input file is a directory, not a file (%s).", file));
                return false;
            }
        }
        return true;
    }
}
