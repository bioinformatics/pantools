package nl.wur.bif.pantools.utils.cli.validation.validators;

import nl.wur.bif.pantools.utils.cli.validation.Constraints.MinOrZero;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Custom version of the @Min constraint for optional integers that allows 0 as a valid option.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class MinOrZeroValidator implements ConstraintValidator<MinOrZero, Integer> {

    private long value;

    @Override
    public void initialize(MinOrZero constraintAnnotation) {
        this.value = constraintAnnotation.value();
    }

    /**
     * Verify whether an integer is either 0 or lower than the value given in the constraint
     * @param integer input integer
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true when the integer is 0 or >= value, false otherwise
     */
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext context) {
        if (integer == 0) return true;
        return (integer >= value);
    }
}
