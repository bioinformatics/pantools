/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static nl.wur.bif.pantools.utils.PhasedFunctionalities.phasingInfoMap;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.GraphUtils.getVcfAccessions;
import static nl.wur.bif.pantools.utils.Utils.orderTwoSubgenomeIdentifiers;

/**
 *
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands
 */
public class CreateSkipArrays {
    
    /**
     * --reference/-ref, --skip. allowed input is by using commas and hyphens. Example 1,2,3,4-10.
     * needs a test to see if it is over 1 and below the total nr of genomes +1
     * @param ignore_ref_argument
     * @param print
     */
    public void create_skip_arrays(boolean ignore_ref_argument, boolean print) {
        skip_list = new ArrayList<>();
        skip_seq_list = new ArrayList<>();
        skip_array = new boolean[total_genomes];
        skip_seq_array = new boolean[total_genomes][0];

        setAnnotationIdentifiers(true, false);
        for (int i = 1; i <= total_genomes; i++) {
            skip_array[i-1] = false;
        }

        HashMap<Integer, Integer> sequencesPerGenome = retrieveSequencesPerGenome();
        if (SELECTION_FILE != null) { // a file was included with the --selection-file argument
            readSelectionFile();
        } else if (target_genome != null && !ignore_ref_argument) { // --reference was provided
            target_genome = target_genome.replace(" ","");
            if (target_genome.endsWith(",")) {
                target_genome = target_genome.replaceFirst(".$",""); // remove last character
            }
            ArrayList<Integer> targetList = convert_genome_selection_to_list(target_genome);
            if (targetList.size() < 101 && print) {
                System.out.print("\rSelected " + targetList.size() + " genome(s) " + target_genome + "\n");
            } else if (print) {
                System.out.print("\rSelected " + targetList.size() + " genome(s)\n");
            }
            for (int i=1; i<= total_genomes; i++) {
                if (targetList.contains(i)) {
                    continue;
                }
                skip_list.add(i);
                skip_array[i-1] = true;
            }
            if (!PROTEOME) {
                for (int i = 1; i <= total_genomes; i++) { // i is a genome number
                    int numberSequences = sequencesPerGenome.get(i);
                    for (int j = 0; j < numberSequences; j++) {
                        if (skip_array[i - 1]) {
                            skip_seq_array[i - 1][j] = true;
                        }
                    }
                }
            }
        } else if (skip_genomes != null) { // --skip argument was provided
            skip_genomes = skip_genomes.replace(",,", ",");
            if (skip_genomes.endsWith(",")) {
                skip_genomes = skip_genomes.replaceFirst(".$",""); // remove last character
            }
            String[] skipGenomesarray = skip_genomes.split(",");
            for (String genomeStr : skipGenomesarray) {
                if (genomeStr.contains("-")) {
                    String[] genomeArray = genomeStr.split("-");
                    int start = Integer.parseInt(genomeArray[0]);
                    int end = Integer.parseInt(genomeArray[1]);
                    for (int genome_nr = start; genome_nr <= end; genome_nr ++) {
                        if (genome_nr > 0 && genome_nr <= total_genomes) {
                            skip_list.add(genome_nr);
                            skip_array[genome_nr-1] = true;
                        }
                    }
                } else {
                    int genomeNr = 0;
                    try {
                        genomeNr = Integer.parseInt(genomeStr);
                    } catch (NumberFormatException nfe) {
                        Pantools.logger.error("Unable to correctly parse the --skip argument.");
                        System.exit(1);
                    }
                    if (genomeNr > 0 && genomeNr <= total_genomes) {
                        skip_list.add(genomeNr);
                        skip_array[genomeNr-1] = true;
                    }
                }
            }
            
            for (int i = 1; i <= total_genomes; i++) { // i is a genome number
                int numberSequences = sequencesPerGenome.get(i);
                for (int j = 0; j < numberSequences; j++) {
                    if (skip_array[i-1]) {
                         skip_seq_array[i-1][j] = true;   
                    }     
                }
            }   
        }

        ArrayList<Integer> skipGenomesList = new ArrayList<>();
        for (int i = 1; i <= total_genomes; i++) { // i is a genome number
            if (skip_array[i-1]) {
                skipGenomesList.add(i);
            }
        }
        adj_total_genomes = total_genomes - skipGenomesList.size();
        if (skipGenomesList.size() > 0) {
            Pantools.logger.info("Selected {} genomes, skipping {} genomes", adj_total_genomes, skipGenomesList.size());
        }
        if (adj_total_genomes == 0) {
            Pantools.logger.error("No genomes are in the current selection.");
            System.exit(1);
        }
        updateAnnotationIdentifiers();
    }

    private HashMap<Integer, Integer> retrieveSequencesPerGenome() {
        HashMap<Integer, Integer> sequencesPerGenome = new HashMap<>();
        if (PROTEOME) {
            return null;
        }
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genomeNodes.hasNext()) {
                Node genomeNode = genomeNodes.next();
                int genomeNr = (int) genomeNode.getProperty("number");
                int num_sequences = (int) genomeNode.getProperty("num_sequences");
                skip_seq_array[genomeNr-1] = new boolean[num_sequences];
                for (int j = 1; j <= num_sequences; j++) {
                    skip_seq_array[genomeNr-1][j-1] = false;
                }
                sequencesPerGenome.put(genomeNr, num_sequences);
            }
            tx.success();
        }
        return sequencesPerGenome;
    }

    /**
     * annotation_identifiers list is initialized in 'setAnnotationIdentifiers' but the genome selection might be different now.
     * Updates annotation_identifiers by changing the identifiers of skipped genomes to end with '_0'
     */
    private void updateAnnotationIdentifiers() {
        if (PROTEOME) { // panproteome does not have annotations
            return;
        }
        for (int i = 1; i <= total_genomes; i++) { // i is a genome number
            if (skip_array[i-1]) {
                String annotationId = annotation_identifiers.get(i-1);
                if (!annotationId.endsWith("_0")) { // the annotation was not labeled as skipped yet
                    String[] annotationIdArray = annotationId.split("_");
                    annotation_identifiers.set(i-1, annotationIdArray[0] +"_0");
                }
            }
        }
    }

    /**
     * if initialized without a file, take the higest.
     * when using a --annotations-file, take the ones from the file
     *
     * If a genome is skipped, the identifier ends with '_0'
     *
     * @param print
     * @param ignore_file
     * @return arraylist of identifiers to use
     */
    public static void setAnnotationIdentifiers(boolean print, boolean ignore_file) {
        annotation_identifiers = new ArrayList<>();
        if (PROTEOME) { // panproteomes don't have annotation identifiers, only genome numbers
            return ;
        }

        // go over the annotation nodes and take the highest
        ResourceIterator<Node> annotation_nodes = GRAPH_DB.findNodes(ANNOTATION_LABEL);
        int[] highest_ids = new int[total_genomes];
        String[] highest_ids_str = new String[total_genomes];
        while (annotation_nodes.hasNext()) {
            Node annotation_node = annotation_nodes.next();
            String identifier = (String) annotation_node.getProperty("identifier");
            if (!annotation_node.hasProperty("total_genes")) {
                continue;
            }
            String[] id_array = identifier.split("_");
            int genome_nr = Integer.parseInt(id_array[0]);
            if (skip_array != null && skip_array[genome_nr-1]) {
                continue;
            }
            int anotation_number = Integer.parseInt(id_array[1]);
            if (anotation_number > highest_ids[genome_nr-1]) {
                highest_ids[genome_nr-1] = anotation_number;
            }
        }

        for (int i=0; i < highest_ids.length; i++) {
            highest_ids_str[i] = (i+1) + "_" + highest_ids[i];
        }

        // --anotations-file was included. replace the highest identifiers
        if (PATH_TO_THE_ANNOTATIONS_FILE != null && !ignore_file) {
            ArrayList<String> selected_ids = new ArrayList<>();
            try (BufferedReader in = new BufferedReader(new FileReader(PATH_TO_THE_ANNOTATIONS_FILE))) { // asume one identifier per line
                for (int c = 0; in.ready();) {
                    String line = in.readLine().trim();
                    if (line.contains(",") || line.contains(".") || !line.contains("_")) {
                        Pantools.logger.error("Not all identifiers in {} are correctly formatted: {}", PATH_TO_THE_ANNOTATIONS_FILE, line);
                        System.exit(1);
                    }
                    selected_ids.add(line);
                }
            } catch (IOException ioe) {
                Pantools.logger.error("Failed to read file given by --annotations-file: {}", PATH_TO_THE_ANNOTATIONS_FILE);
                System.exit(1);
            }
            if (print) {
                Pantools.logger.info("Using {} selected annotations", selected_ids.size());
            }
            for (String identifier : selected_ids) {
                String[] id_array = identifier.split("_");
                int genomeNr = 0;
                try {
                    genomeNr = Integer.parseInt(id_array[0]);
                } catch (NumberFormatException nfe) {
                    Pantools.logger.error("Unable to correctly parse the identifiers in the provided --annotations-file.");
                    System.exit(1);
                }
                highest_ids_str[genomeNr-1] = identifier;
            }
        }
        annotation_identifiers.addAll(Arrays.asList(highest_ids_str));
    }

    /**
     * 1,3-5 means 1,3,4,5
     * @param genome_selection_str string with genome numbers
     * @return list with genome numbers
     */
    public static ArrayList<Integer> convert_genome_selection_to_list(String genome_selection_str){
        String[] temp_target_array = genome_selection_str.split(",");
        ArrayList<Integer> selected_genomes_list = new ArrayList<>();
        for (String genome_str : temp_target_array) {
            if (genome_str.contains("-")) {
                String[] genome_array = genome_str.split("-");
                int start = Integer.parseInt(genome_array[0]);
                int end = Integer.parseInt(genome_array[1]);
                for (int genome_nr = start; genome_nr <= end; genome_nr++) {
                    if (genome_nr > 0 && genome_nr <= total_genomes) {
                        selected_genomes_list.add(genome_nr);
                    }
                }
            } else {
                int genome_nr = Integer.parseInt(genome_str);
                if (genome_nr > 0 && genome_nr <= total_genomes) {
                    selected_genomes_list.add(genome_nr);
                }
            }
        }
        return selected_genomes_list;
    }
    
    /**
     * Returns the size of a file
     * @param path
     * @return
     */
    public static boolean check_if_file_exists(String path) {
        File file = new File(path);
        boolean exists = file.exists();
        if (exists) { // if the file exists, check if anything is in there
            if (file.length() < 1) {
                exists = false;
            }
        }
        return exists;
    }

    /**
     * When entering this function every genome and sequence in skip_seq_array are set to false
     */
    private void readSelectionFile() {
        try {
            BufferedReader br = Files.newBufferedReader(SELECTION_FILE);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                boolean correctLine = false;
                if (line.startsWith("GENOME_MINIMUM_NUMBER_GENES =") && !line.equals("GENOME_MINIMUM_NUMBER_GENES =")) {
                    skipGenomesGeneNumber(line.split("="));
                    correctLine = true;
                } else if (line.startsWith("SEQUENCE_MINIMUM_NUMBER_GENES =") && !line.equals("SEQUENCE_MINIMUM_NUMBER_GENES =")) {
                    skipSequencesGeneNumber(line.split("="));
                    correctLine = true;
                } else if (line.startsWith("GENOMES_WITH_MCSCANX_ID =") && !line.equals("GENOMES_WITH_MCSCANX_ID =")) {
                    skip_genomes_based_on_having_mcscanx_id();
                    correctLine = true;
                } else if (line.startsWith("#SEQUENCES_WITH_MCSCANX_ID = true") || line.startsWith("#SEQUENCES_WITH_MCSCANX_ID = TRUE")) {
                    skip_sequences_based_on_having_mcscanx_id();
                    correctLine = true;
                } else if (line.startsWith("SEQUENCES_WITH_PHASING = true") || line.startsWith("SEQUENCES_WITH_PHASING = TRUE")) {
                    selectSequencesChromosomePhasing();
                    correctLine = true;
                } else if (line.startsWith("FIRST_SEQUENCES =") && !line.equals("FIRST_SEQUENCES =")) {
                    selectFirstSequences(line);
                    correctLine = true;
                } else if (line.startsWith("SELECT_CHROMOSOME =") && !line.equals("SELECT_CHROMOSOME =")) {
                    selectSequencesBelongingToChromosome(line);
                    correctLine = true;
                } else if (line.startsWith("#LARGEST_X_SEQUENCES =") && !line.equals("#LARGEST_X_SEQUENCES =")) {
                    Pantools.logger.error("working on this 20872:") ;
                    correctLine = true;
                    System.exit(1);
                }

                if (line.startsWith("SELECT_SEQUENCE =") && !line.equals("SELECT_SEQUENCE =")) {
                    skip_genomes_sequences_based_on_identifiers(line, false);
                    correctLine = true;
                }

                if (line.startsWith("SKIP_SEQUENCE =") && !line.equals("#SKIP_SEQUENCE =")) {
                    skip_genomes_sequences_based_on_identifiers(line, true);
                    correctLine = true;
                }
                
                if (line.startsWith("SELECT_GENOME =") && !line.equals("SELECT_GENOME =")) {
                    skip_genomes_based_on_selection(line, false);
                    correctLine = true;
                }

                if (line.startsWith("SKIP_GENOME =") && !line.equals("SKIP_GENOME =")) {
                    skip_genomes_based_on_selection(line, true);
                    correctLine = true;
                }
                if (!correctLine) {
                    Pantools.logger.error("Rule '{}' not recognized!", line);
                    System.exit(1);
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", SELECTION_FILE);
            System.exit(1);
        }

        updateSkipGenomesBasedOnSequences();
    }

    /**
     * Only allow sequences that have a specific chromosome number
     * @param input
     */
    private void selectSequencesBelongingToChromosome(String input) {
        ArrayList<Integer> chromosomeNumbers = retrieveNumbersFromSelectionInput(input);
        int skipCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            PhasedFunctionalities pf = new PhasedFunctionalities();
            pf.preparePhasedGenomeInformation(false, null); // retrieve phasing information of all sequences
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequenceNodes.hasNext()) {
                Node sequence_node = sequenceNodes.next();
                int genomeNr = (int) sequence_node.getProperty("genome");
                int sequenceNr = (int) sequence_node.getProperty("number");
                String[] phasingInfo = phasingInfoMap.get(genomeNr + "_" + sequenceNr); // example [1, D, 1D, 1_D]
                int chromosomeNumber = -1; // default -1 will automaticcally skip sequence
                if (phasingInfo != null) {
                    chromosomeNumber = Integer.parseInt(phasingInfo[0]);
                }
                if (!chromosomeNumbers.contains(chromosomeNumber)) {
                    skipCounter++;
                    skip_seq_array[genomeNr - 1][sequenceNr - 1] = true;
                    skip_seq_list.add(genomeNr + "_" + sequenceNr);
                }
            }
            tx.success();
        }
        Pantools.logger.info(skipCounter + " sequences were skipped because they didn't belong to the selected chromosome numbers.");
    }

    /**
     * update 'seq_skip_array' and 'selectedSequences' to skip sequences that do not have repeats
     * @param sequences_with_repeats
     */
    public void update_seq_skip_based_on_repeat_presence(ArrayList<String> sequences_with_repeats) {
        ArrayList<String> seqs_to_be_removed = new ArrayList<>();
        int sequence_before = selectedSequences.size();
        for (String seq_id : selectedSequences) {
            if (sequences_with_repeats.contains(seq_id)) {
                continue;
            }
            seqs_to_be_removed.add(seq_id);
            String[] seq_array = seq_id.split("_");
            int genome_nr = Integer.parseInt(seq_array[0]);
            int sequence_nr = Integer.parseInt(seq_array[1]);
            skip_seq_array[genome_nr-1][sequence_nr-1] = true;
        }

        for (String seq_id : seqs_to_be_removed) {
            selectedSequences.remove(seq_id);
        }
        int sequence_after = selectedSequences.size();
        if (sequence_before != sequence_after) {
            Pantools.logger.info("Not every sequence in the analysis has repeats.");
            Pantools.logger.info("Lowered the number of sequences from {} to {}", sequence_before, selectedSequences.size());
        }
        if (sequence_after == 0) {
            Pantools.logger.error("No repeats are present in the pangenome.");
            Pantools.logger.error(" Please run 'add_repeats'");
            System.exit(1);
        }
    }

    /**
     * Second part of the input line, after =, is only allowed to have numbers
     * @param input
     * @return
     */
    private ArrayList<Integer> retrieveNumbersFromSelectionInput(String input){
        String[] inputArray = input.split("=");
        String[] numberArray = inputArray[1].replace(" ","").split(",");
        ArrayList<Integer> numbers = new ArrayList<>();
        for (String numberString : numberArray) { // retrieve chromosome numbers from input
            try {
                int number = Integer.parseInt(numberString);
               numbers.add(number);
            } catch (NumberFormatException nfe) {
                Pantools.logger.error("{} is not a number", numberString);
                System.exit(1);
            }
        }
        return numbers;
    }

    private void selectSequencesChromosomePhasing() {
        int skipCounter = 0;
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            while (sequenceNodes.hasNext()) {
                Node sequence_node = sequenceNodes.next();
                int genomeNr = (int) sequence_node.getProperty("genome");
                int sequenceNr = (int) sequence_node.getProperty("number");
                if (!sequence_node.hasProperty("phasing_assigned")) {
                    skipCounter++;
                    skip_seq_array[genomeNr - 1][sequenceNr - 1] = true;
                    skip_seq_list.add(genomeNr + "_" + sequenceNr);
                }
            }
            tx.success();
        }
        Pantools.logger.info("{} sequences were skipped because they didn't have phasing information.", skipCounter);
    }

    /**
     *
     * @param line
     */
    private void selectFirstSequences(String line) {
        String[] lineArray = line.split("=");
        int allowed_sequences = Integer.parseInt(lineArray[1].replace(" ",""));
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genomeNodes.hasNext()) {
                Node genomeNode = genomeNodes.next();
                int genomeNr = (int) genomeNode.getProperty("number");
                int numSequences = (int) genomeNode.getProperty("num_sequences");
                for (int j = 1; j <= numSequences; j++) {
                    if (j > allowed_sequences) {
                        skip_seq_array[genomeNr - 1][j - 1] = true;
                    }
                }
            }
            tx.success();
        }
    }

    /**
     * When skip = true, selected genomes are skipped in analysis
     * When skip = false, only selected genomes are used in analysis
     * @param line string with genome numbers
     * @param skip boolean skip or select
     */
    public static void skip_genomes_based_on_selection(String line, boolean skip) {
        String[] line_array = line.split("=");
        ArrayList<Integer> selected_genome_list = convert_genome_selection_to_list(line_array[1].replace(" ",""));
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genome_nodes.hasNext()) {
                Node genome_node = genome_nodes.next();     
                int genome_nr = (int) genome_node.getProperty("number");
                if (selected_genome_list.contains(genome_nr) && !skip) {
                    continue;
                } else if (!selected_genome_list.contains(genome_nr) && skip) {
                    continue;
                }
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                skip_array[genome_nr-1] = true;
                for (int j = 1; j <= num_sequences; j++) {
                    skip_seq_array[genome_nr-1][j-1] = true;         
                }                       
            } 
            tx.success();
        }
    }

    /**
     * When skip = true, selected genomes are skipped in analysis
     * When skip = false, only selected genomes are used in analysis
     * @param line string with genome numbers
     * @param skip boolean skip or select
     */
    public static void skip_genomes_sequences_based_on_identifiers(String line, boolean skip) {
        String[] line_array = line.split("=");
        List<String> selected_sequences = Arrays.asList(line_array[1].replace(" ","").split(","));
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (int i = 1; i <= total_genomes; i++) {
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                for (int j = 1; j <= num_sequences; j++) {
                    if (selected_sequences.contains(i + "_" + j) && skip) {
                        skip_seq_array[i-1][j-1] = true; 
                        skip_seq_list.add(i + "_" + j);
                    } else if (!selected_sequences.contains(i + "_" + j) && !skip) {
                        skip_seq_array[i-1][j-1] = true;
                        skip_seq_list.add(i + "_" + j);
                    }              
                }
            }
            tx.success();
        }
    }
    
    /**
     * If all sequences of a genome are skipped, skip the genome as well
     */
    public static void updateSkipGenomesBasedOnSequences() {
        for (int i = 0; i < skip_seq_array.length; i++) {  // go over genomes
            boolean any_seq_not_skipped = false; 
            for (int j = 0; j < skip_seq_array[i].length; j++) { // go over the sequences of a genome
                if (!skip_seq_array[i][j]) {
                    any_seq_not_skipped = true;
                }
            }
            if (!any_seq_not_skipped && !skip_array[i]) {
                skip_array[i] = true;
            }
        }
    }

    /**
     * Genome nodes are not always retrieved in the correct order (1,2,3)
     * @param thresholdForWarning
     * @param accessions whether to include accessions in the analysis
     */
    public void retrieveSelectedSequences(int thresholdForWarning, boolean accessions) {
        selectedSequences = new LinkedHashSet<>();
        if (PROTEOME) {
            Pantools.logger.warn("Not supported for panproteome analysis.");
            return;
        }

        for (int genomeNr : selectedGenomes) {
            for (int j = 1; j <= GENOME_DB.num_sequences[genomeNr]; ++j) {
                if (skip_seq_array[genomeNr-1][j-1]) {
                    continue;
                }
                selectedSequences.add(genomeNr + "_" + j);

                if (accessions) {
                    for (String accessionNr : getVcfAccessions(genomeNr)) {
                        selectedSequences.add(accessionNr + "_" + j);
                    }
                }
            }
        }

        if (compare_sequences) {
            if (selectedSequences.size() > thresholdForWarning && thresholdForWarning > 0) {
                Pantools.logger.warn("Sequences in analysis: " + selectedSequences.size() + ". " +
                        "This is a lot and has major impact on the analysis runtime! Please consider making a selection through --include, --exclude or --selection-file.");
            } else {
                Pantools.logger.info("Sequences in analysis: " + selectedSequences.size() + ".");
            }
        }
    }

    /**
     * Creates set with correctly ordered subgenome identifiers.
     * order is determined by genome number and phasing letter. 'unphased' subgenomes come after all letters
     */
    public void retrieveSelectedSubgenomes() {
        if (selectedSequences == null) {
            Pantools.logger.error("hi developer, retrieveSelectedSequences() must first be initialized.");
            System.exit(1);
        }
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) { // no phasing information is found in the pangenome
            selectedSubgenomes = new LinkedHashSet<>();
            return;
        }

        ArrayList<String> subgenomes = new ArrayList<>();
        int counter = 0;

        // step 1/2 collect all subgenome identifiers
        for (String sequenceId : selectedSequences) {
            counter++;
            String[] sequenceIdArray = sequenceId.split("_"); // 1_1 becomes [1,1]
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            String genomeOnePhase = "";
            if (phasingInfo == null) {
                genomeOnePhase = sequenceIdArray[0] + "_unphased";
            } else {
                genomeOnePhase = sequenceIdArray[0] + "_" + phasingInfo[1];
            }
            // TODO: implement progress bar
            if (counter % 100 == 0 || counter < 11 || counter == selectedSequences.size()) {
                System.out.print("\rInput file preparation: " + counter + "/" + selectedSequences.size() + "  ");
            }
            if (!subgenomes.contains(genomeOnePhase)){
                subgenomes.add(genomeOnePhase);
            }
        }

        // step 2/2 order the identifiers
        boolean noChangeMade = true;
        while (noChangeMade) {
            noChangeMade = false;
            for (int i = 0; i < subgenomes.size()-1; i++) {
                String subgenomeA = subgenomes.get(i);
                String subgenomeB = subgenomes.get(i+1);
                String[] orderedIds = orderTwoSubgenomeIdentifiers(subgenomes.get(i),  subgenomes.get(i+1),null);
                if (!orderedIds[0].equals(subgenomes.get(i))) { // order is incorrect, swap the ids
                    subgenomes.set(i, subgenomeB); // replace A with B
                    subgenomes.set(i+1, subgenomeA);
                    noChangeMade = true;
                }
            }
        }
        selectedSubgenomes = new LinkedHashSet<>(subgenomes);
    }

    /**
     Genome nodes are not always retrieved in the correct order (1,2,3)
     */
    public void retrieveSelectedGenomes() {
        selectedGenomes = new LinkedHashSet<>();
        for (int i = 1; i <= total_genomes; i++) { // i is genome number
           if (skip_array[i-1]){
               continue;
           }
           selectedGenomes.add(i);
        }
    }

    public static void skip_genomes_based_on_having_mcscanx_id() {
        ResourceIterator<Node> genome_nodes = GRAPH_DB.findNodes(GENOME_LABEL);
        int skip_counter = 0;
        while (genome_nodes.hasNext()) {
            Node genome_node = genome_nodes.next();
            int genome = (int) genome_node.getProperty("number");
            if (genome % 100 == 0) {
                System.out.print("\rGathering genomes: genome " + genome + "  ");
            }
            if (!genome_node.hasProperty("MCScanX_ID")) {
                skip_counter ++;
                skip_array[genome-1] = true;
            }  
        }
        Pantools.logger.info("{} genomes were skipped because they didn't have a MCScanX identifier", skip_counter);
    }

    public static void skip_sequences_based_on_having_mcscanx_id() {
        ResourceIterator<Node> sequence_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
        int skip_counter = 0;
        // TODO: implement progress bar
        while (sequence_nodes.hasNext()) {
            Node sequence_node = sequence_nodes.next();
            int genome_nr = (int) sequence_node.getProperty("genome");
            int seq_nr = (int) sequence_node.getProperty("number");
            System.out.print("\rGathering genomes: genome " + genome_nr + "  ");
            if (!sequence_node.hasProperty("MCScanX_ID")) {
                skip_counter ++;
                skip_seq_array[genome_nr-1][seq_nr-1] = true;
                skip_seq_list.add(genome_nr + "_" + seq_nr);
            }  
        }
        Pantools.logger.info("{} sequences were skipped because they didn't have a MCScanX identifier", skip_counter);
    }
    
    public static void skipGenomesGeneNumber(String[] line_array) {
        HashSet<Integer> presentAnnotations = new HashSet<>();
        int minimumNumber = Integer.parseInt(line_array[1].replace(" ",""));
        ResourceIterator<Node> annotationNodes = GRAPH_DB.findNodes(ANNOTATION_LABEL); // currently assumes there is 1 annotation per genome
        int skipCounter = 0, nodeCounter = 0;
        while (annotationNodes.hasNext()) {
            Node annotationNode = annotationNodes.next();
            int genome = (int) annotationNode.getProperty("genome");
            if (nodeCounter < 1 || nodeCounter % 50 == 0) {
                System.out.print("\rCounting genes: Genome " + genome + "  ");
            }
            int numberGenes = (int) annotationNode.getProperty("total_genes");
            presentAnnotations.add(genome);
            if (numberGenes < minimumNumber) {
                skipCounter ++;
                skip_array[genome-1] = true;
            }
            nodeCounter ++;
        }
        
        if (nodeCounter == 0) {
            Pantools.logger.error("This skip option is not available as no genes are annotated yet in the pangenome.");
            System.exit(1);
        }
        
        // genomes that don't have an annotation node are not skipped yet
        for (int i=1; i <= total_genomes; i++) {
            if (!presentAnnotations.contains(i)) {
                skipCounter ++;
                skip_array[i-1] = true;
            }
        }
        Pantools.logger.info("{} genomes were skipped based on a minimum number of {} genes per genome", skipCounter, minimumNumber);
    }

    public static void retrieveGenomesWithAnnotation(String[] lineArray) {

    }
            
    /**
     * 
     * @param lineArray
     */
    public static void skipSequencesGeneNumber(String[] lineArray) {
        HashSet<Integer> presentAnnotations = new HashSet<>();
        int minimumNumber = Integer.parseInt(lineArray[1].replace(" ",""));
        ResourceIterator<Node> annotationNodes = GRAPH_DB.findNodes(ANNOTATION_LABEL); // currently assumes there is 1 annotation per genome
        int skipCounter = 0, nodeCounter = 0;
        while (annotationNodes.hasNext()) {
            Node annotationNode = annotationNodes.next();
            String id = (String) annotationNode.getProperty("identifier");
            if (!annotation_identifiers.contains(id)) {
                continue;
            }
            int genome = (int) annotationNode.getProperty("genome");
            if (nodeCounter < 1 || nodeCounter % 50 == 0) {
                System.out.print("\rCounting genes: Genome " + genome + "  ");

            }
            presentAnnotations.add(genome);
            nodeCounter ++;
        }
        if (nodeCounter == 0) {
            Pantools.logger.error("This skip option is not available as no genes are annotated yet in the pangenome.");
            System.exit(1);
        }
        // genomes that don't have an annotation node are not skipped yet
        for (int i=1; i <= total_genomes; i++) { // i is a genome number
            if (!presentAnnotations.contains(i)) {
                skipCounter ++;
                skip_array[i-1] = true;
            }
        }

        nodeCounter = 0;
        HashMap<String, Integer> geneCountPerSequence = new HashMap<>();
        ResourceIterator<Node> geneNodes = GRAPH_DB.findNodes(GENE_LABEL);
        while (geneNodes.hasNext()) { // loop to count number of genes per sequence
            Node geneNode = geneNodes.next();
            int[] address = (int[]) geneNode.getProperty("address");
            if (skip_array[address[0]-1]){ // if genome was already skipped by other rule
               continue;
            }
            geneCountPerSequence.merge(address[0] + "_" + address[1], 1, Integer::sum);
            // TODO: implement progress bar
            if (nodeCounter < 1 || nodeCounter % 1000 == 0) {
                System.out.print("\rCounting genes: Genome " + address[0] + ", total count " + nodeCounter + "   ");

            }
            nodeCounter++;
        }

        for (int i = 1; i <= total_genomes; i++) {
            Node genomeNode = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
            int numberSequences = (int) genomeNode.getProperty("num_sequences");
            for (int j = 1; j <= numberSequences; j++) {
                int geneCount = 0;
                if (geneCountPerSequence.containsKey(i + "_" + j)) {
                    geneCount = geneCountPerSequence.get(i + "_" + j);
                }
                if (geneCount < minimumNumber) {
                    skip_seq_array[i-1][j-1] = true;
                    skipCounter ++;
                    skip_seq_list.add(i + "_" + j);
                }
            }
        }
        Pantools.logger.info("{} sequences were skipped based on a minimum number of {} genes per sequence", skipCounter, minimumNumber);
    }

    /**
     * First resets skip_array and skip_seq_array by setting all booleans to true.
     * Sequences in 'sequenceIdentifiers' are set to false.
     * @param sequenceIdentifiers
     */
    public void updateSkipArraysSequenceSelection(ArrayList<String> sequenceIdentifiers) {
        skip_array = new boolean[total_genomes];
        skip_seq_array = new boolean[total_genomes][0];
        HashMap<Integer, Integer> sequencesPerGenome = retrieveSequencesPerGenome();
        for (int i = 1; i <= total_genomes; i++) { // i is a genome number
            skip_array[i-1] = false; // are set to true when all sequences are skipped
            int numberSequences = sequencesPerGenome.get(i);
            for (int j = 0; j < numberSequences; j++) {
                skip_seq_array[i-1][j] = true;
            }
        }

        for (String sequenceId : sequenceIdentifiers) {
            String[] seqIdArray = sequenceId.split("_");
            int genomeNr = Integer.parseInt(seqIdArray[0]);
            int sequenceNr = Integer.parseInt(seqIdArray[1]);
            skip_seq_array[genomeNr-1][sequenceNr-1] = false;
        }
        updateSkipGenomesBasedOnSequences();
    }
}
