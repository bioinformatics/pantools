package nl.wur.bif.pantools.utils;
import nl.wur.bif.pantools.construction.index.IndexScanner;
import nl.wur.bif.pantools.construction.sequence.SequenceScanner;
import org.apache.commons.io.FileUtils;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.MemoryMXBean;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;

import nl.wur.bif.pantools.construction.index.IndexDatabase;
import nl.wur.bif.pantools.construction.index.IndexPointer;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.sequence.SequenceDatabase;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.neo4j.graphdb.*;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.nio.file.Files;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.construction.build_pangenome.GenomeLayer.*;
import static nl.wur.bif.pantools.utils.Globals.*;

/**
 * A bunch of random utilities.
 */
public final class Utils {

    private Utils() {}

    public static Properties readPropertiesFile(Path path) throws IOException {
        // TODO: this method should also allow for loading from properties files outside jars
        final Properties properties = new Properties();
        properties.load(Utils.class.getResourceAsStream(path.toString()));
        return properties;
    }

    /**
     * The relationship should start with the lower genome & sequence number
     * @param seq_id1 sequence identifier, example 1_1
     * @param seq_id2 sequence identifier, example 1_2
     * @param node1_id mRNA node identifier of mRNA 1
     * @param node2_id mRNA node identifier of mRNA 2
     * @return true is forward, false is inward
     */
    public static boolean orientation_of_relation(String seq_id1, String seq_id2, Long node1_id, Long node2_id) {
        String[] seq1_array = seq_id1.split("_");
        int genome1_nr = Integer.parseInt(seq1_array[0]);
        int genome2_nr = Integer.parseInt(seq1_array[1]);
        String[] seq2_array = seq_id2.split("_");
        int seq1_nr = Integer.parseInt(seq2_array[0]);
        int seq2_nr = Integer.parseInt(seq2_array[1]);
        if (genome1_nr < genome2_nr) {
            return true;
        } else if (genome1_nr > genome2_nr) {
            return false;
        }

        // genome numbers are equal
        if (seq1_nr < seq2_nr) {
            return true;
        } else if (seq1_nr > seq2_nr) {
            return false;
        }
        // sequence numbers are also equal
        if (node1_id < node2_id) {
            return true;
        }
        return false;
    }

    /**
     * order two sequence identifiers where the lower genome & sequence number should come first
     * @param sequence1Id sequence identifier, example 1_1
     * @param sequence2Id sequence identifier, example 1_2
     * @return strings in correct order
     */
    public static String[] orderTwoSequenceIdentifiers(String sequence1Id, String sequence2Id) {
        String[] sequence1First = new String[]{sequence1Id, sequence2Id};
        String[] sequence2First = new String[]{sequence2Id, sequence1Id};
        String[] seq1_array = sequence1Id.split("_");
        String[] seq2_array = sequence2Id.split("_");
        int genome1Nr = Integer.parseInt(seq1_array[0]);
        int genome2Nr = Integer.parseInt(seq2_array[0]);

        int sequence1Nr = Integer.parseInt(seq1_array[1]);
        int sequence2Nr = Integer.parseInt(seq2_array[1]);
        if (genome1Nr < genome2Nr) {
            return sequence1First;
        } else if (genome1Nr > genome2Nr) {
            return sequence2First;
        }

        // genome numbers are equal
        if (sequence1Nr < sequence2Nr) {
            return sequence1First;
        } else if (sequence1Nr > sequence2Nr) {
            return sequence2First;
        }
        // its the same sequence
        return sequence1First;
    }

    /**
     *
     * @param subgenomeA genome number combined with haplotype letter, seperated by a underscore.
     * @param subgenomeB see subgenomeA
     * @return
     */
    public static String[] orderTwoSubgenomeIdentifiers(String subgenomeA, String subgenomeB, String[] letters) {
        String[] subgenomeAFirst = new String[]{subgenomeA, subgenomeB};
        String[] subgenomeBFirst = new String[]{subgenomeB, subgenomeA};
        String[] subgenomeAArray = subgenomeA.split("_");
        String[] subgenomeBArray = subgenomeB.split("_");
        int genomeNrA = Integer.parseInt(subgenomeAArray[0]);
        int genomeNrB = Integer.parseInt(subgenomeBArray[0]);
        if (genomeNrA < genomeNrB) {
            return subgenomeAFirst;
        } else if (genomeNrA > genomeNrB) {
            return subgenomeBFirst;
        }
        // genome numbers are equal

        String phaseA = subgenomeAArray[1].toLowerCase();
        String phaseB = subgenomeBArray[1].toLowerCase();

        if (phaseA.equals("unphased")) {
            return subgenomeBFirst;
        } else if (phaseB.equals("unphased")) {
            return subgenomeAFirst;
        }
        // both are phased, phasing letter determines order

        //int indexA = Arrays.binarySearch(letters, phaseA);
        //int indexB = Arrays.binarySearch(letters, phaseB);
        if (phaseA.charAt(0) < phaseB.charAt(0)) {
            return subgenomeAFirst;
        } else if (phaseA.charAt(0) > phaseB.charAt(0)) {
            return subgenomeBFirst;
        }

        // its the same subgenome
        return subgenomeAFirst;
    }

    public static String arrayToCSVString(String[] array) {
        return Arrays.toString(array).replace("[", "").replace("]", "").replace(" ", "");
    }

    public static String arrayToCSVString(double[] array) {
        return Arrays.toString(array).replace("[", "").replace("]", "").replace(" ", "");
    }

    public static String arrayToCSVString(int[] array) {
        return Arrays.toString(array).replace("[", "").replace("]", "").replace(" ", "");
    }

    public static String arrayListToCSVString(ArrayList<Integer> arrayList) {
        return arrayList.toString().replace("[", "").replace("]", "").replace(" ", "");
    }

    public static String ALStrToCSVString(ArrayList<String> arrayList) {
        return arrayList.toString().replace("[", "").replace("]", "").replace(" ", "");
    }


    /**
     * Return build version of PanTools and abbreviated commit ID, also indicating if the current state of the code base
     * is dirty (containing changed files not staged). Build version is the latest tag.
     *
     * Will throw an {{@link IllegalArgumentException}} in case the git.properties file is found, but does not contain
     * a needed item.
     * @return string containing the PanTools version and abbreviated commit ID, with a comma in in-between.
     * Connects to genome, index and graph databases of the pangenome.
     */
    public static void connect_pangenome() {
        INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, "sorted");
        if (Files.exists(Paths.get(WORKING_DIRECTORY + GENOME_DATABASE_PATH))) {
            GENOME_DB = new SequenceDatabase(WORKING_DIRECTORY + GENOME_DATABASE_PATH);
        } else {
            Pantools.logger.warn("No genome database found at {}", WORKING_DIRECTORY);
            if (ConsoleUtils.askYesOrNo("Do you want to reconstruct it from the graph database?")) {
                rebuild_genome_database();
            } else {
                throw new RuntimeException("Genome database not found");
            }
        }
        INDEX_SC = new IndexScanner(INDEX_DB);
        GENOME_SC = new SequenceScanner(GENOME_DB, 1, 1, K_SIZE, INDEX_DB.get_pre_len());
    }

    /**
     * Disconnects genome, index and graph databases of the pangenome.
     */
    public static void disconnectPangenome() {
        delete_file_in_DB("tmp.log");
        if (WORKING_DIRECTORY == null || WORKING_DIRECTORY.equals("")) { // database was not used
            return;
        }
        if (GRAPH_DB != null && GRAPH_DB.isAvailable(0)) {
            GRAPH_DB.shutdown();
        }
        if (GENOME_DB != null) {
            GENOME_DB.close();
        }
        if (INDEX_DB != null) {
            INDEX_DB.close();
        }

        if (Files.isDirectory(Paths.get(WORKING_DIRECTORY + GRAPH_DATABASE_PATH))) {
            File directory = new File(WORKING_DIRECTORY + GRAPH_DATABASE_PATH);
            for (File f : directory.listFiles()) {
                if (f.getName().startsWith("neostore.transaction.db.")) {
                    f.delete();
                }
            }

        }
    }

    /**
     * Rebuilds the genome database from the graph database.
     * Read genomes information from the graph and rebuild the genomes database
     */
    public static void rebuild_genome_database() {
        int genome, seqience, begin, end, j, len;
        long byte_number = 0;
        GENOME_DB = new SequenceDatabase(WORKING_DIRECTORY + GENOME_DATABASE_PATH, GRAPH_DB);
        StringBuilder seq = new StringBuilder();
        for (genome = 1; genome <= GENOME_DB.num_genomes; ++genome) {
            for (seqience = 1; seqience <= GENOME_DB.num_sequences[genome]; ++seqience) {
                begin = 1;
                end = (int) GENOME_DB.sequence_length[genome][seqience];
                extract_sequence_from_graph(seq, genome, seqience, begin, end);
                len = seq.length();
                if (len % 2 == 1) {
                    --len;
                }
                for (j = 0; j < len; j += 2, ++byte_number) {
                    GENOME_DB.genomes_buff[(int) (byte_number / GENOME_DB.MAX_BYTE_COUNT)].put((byte) ((binary[seq.charAt(j)] << 4) | binary[seq.charAt(j + 1)]));
                }
                if (len == seq.length() - 1) {
                    GENOME_DB.genomes_buff[(int) (byte_number / GENOME_DB.MAX_BYTE_COUNT)].put((byte) (binary[seq.charAt(len)] << 4));
                    ++byte_number;
                }
            }
        }
    }

    /**
     * Capitalize the first letter of a string
     * @param str input string
     * @return capitalized string
     */
    public static String capitalize_first_letter(String str) {
        if(str == null) return str;
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * Calculates median value of arraylist
     * @param inputList input arraylist
     * @return median
     *
     * Does NOT modify order of input ArrayList
     */
    public static double getMedian(ArrayList<Double> inputList) {
        ArrayList<Double> deepCopyInputList = new ArrayList<>(inputList);
        Collections.sort(deepCopyInputList);

        int length = deepCopyInputList.size();
        if (length%2 == 1) { //uneven length
            return deepCopyInputList.get((length + 1) / 2 - 1);
        } else { //even length
            return (deepCopyInputList.get(length / 2 - 1) + deepCopyInputList.get(length / 2)) / 2;
        }
    }

    /**
     * Creates the directory named by the pathname, including necessary and non-existent parent directories.
     * @param pathname
     */
    public static void create_directory_in_DB(String pathname) {
        if (pathname == null) {
            Pantools.logger.error("Directory is not defined.");
            System.exit(1);
        }
        new File(WORKING_DIRECTORY + pathname).mkdirs(); // create directory
    }

    public static void createDirectory(String pathName, boolean inDatabase) {
        if (pathName == null) {
            Pantools.logger.error("Directory is not defined.");
            System.exit(1);
        }
        if (inDatabase) {
            new File(WORKING_DIRECTORY + pathName).mkdirs(); // create directory
        } else {
            new File(pathName).mkdirs(); // create directory
        }
    }

    /**
     * Creates the directory named by the pathname (full path), including necessary and non-existent parent directories.
     * @param pathname path to wanted directory
     */
    public static void create_directory_full_path(String pathname) {
        if (pathname == null) {
            Pantools.logger.error("Directory is not defined.");
            System.exit(1);
        }
        boolean wasSuccessful = new File(pathname).mkdirs(); // create directory
        if (!wasSuccessful) {
            Pantools.logger.info("Could not create directory: {}", pathname);
        }
    }

    /**
     * Creates the directory named by the pathname (full path), including necessary and non-existent parent directories.
     * @param pathname path to wanted directory
     */
    public static void create_directory_full_path(Path pathname) {
        if (pathname == null) {
            Pantools.logger.error("Directory is not defined.");
            System.exit(1);
        }
        boolean wasSuccessful = pathname.toFile().mkdirs(); // create directory
        if (!wasSuccessful) {
            Pantools.logger.info("Could not create directory: {}", pathname);
        }
    }


    /**
     * Extracts a genomic region form the graph database.
     *
     * @param seq The StringBuilder to write the sequence in.
     * @param genome The query genome
     * @param sequence The query sequence
     * @param begin The start of the region
     * @param end The end of the region
     */
    public static void extract_sequence_from_graph(StringBuilder seq, int genome, int sequence, int begin, int end) {
        Relationship rel;
        Node neighbor, node;
        IndexPointer start_ptr;
        int loc, len = 0, node_len, neighbor_len, seq_len, position;
        String rel_name, origin;
        --begin;
        --end;
        origin = "G" + genome + "S" + sequence;
        seq_len = end - begin + 1;
        seq.setLength(0);
        start_ptr = locate(GRAPH_DB, GENOME_SC, INDEX_SC, genome, sequence, begin);
        position = start_ptr.offset;
        node = GRAPH_DB.getNodeById(start_ptr.node_id);
        node_len = (int) node.getProperty("length");
        // Takes the part of the region lies in the first node of the path that region takes in the graph
        if (start_ptr.canonical) {
            if (position + seq_len - 1 <= node_len - 1) { // The whole sequence lies in this node
                len += append_fwd(seq, (String) node.getProperty("sequence"), position, position + seq_len - 1);
            } else {
                len += append_fwd(seq, (String) node.getProperty("sequence"), position, node_len - 1);
            }
        } else {
            if (position - (seq_len - 1) >= 0) { // The whole sequence lies in this node
                len += append_rev(seq, (String) node.getProperty("sequence"), position - (seq_len - 1), position);
            } else {
                len += append_rev(seq, (String) node.getProperty("sequence"), 0, position);
            }
        }
        //  traverse the path of the region
        while (len < seq_len) {
            Pantools.logger.trace("{} {} {}", node.getId(), len, seq_len);
            loc = (begin + len) - K_SIZE + 1;
            rel = get_outgoing_edge(node, origin, loc);
            neighbor = rel.getEndNode();
            rel_name = rel.getType().name();
            neighbor_len = (int) neighbor.getProperty("length");
            if (rel_name.charAt(1) == 'F') {// Enterring forward side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) // neighbor is the last node of the path
                    len += append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, seq_len - len + K_SIZE - 2);
                else
                    len += append_fwd(seq, (String) neighbor.getProperty("sequence"), K_SIZE - 1, neighbor_len - 1);
            }else{ // Enterring reverse side
                if (len + neighbor_len - K_SIZE + 1 > seq_len) // neighbor is the last node of the pat
                    len += append_rev(seq, (String) neighbor.getProperty("sequence"), neighbor_len - K_SIZE - (seq_len - len) + 1, neighbor_len - K_SIZE);
                else
                    len += append_rev(seq, (String) neighbor.getProperty("sequence"), 0, neighbor_len - K_SIZE);
            }
            node = neighbor;
        } // while
    }

    /**
     * Estimates and prints the peak memory used during the execution of the program.
     */
    public static void print_peak_memory() {
        long memoryUsage = 0;
        try {
            for (MemoryPoolMXBean pool : ManagementFactory.getMemoryPoolMXBeans()) {
                MemoryUsage peak = pool.getPeakUsage();
                memoryUsage += peak.getUsed();
            }
            Pantools.logger.info("Peak memory : {} MB.", memoryUsage / 1024 / 1024);
        } catch (Throwable t) {
            Pantools.logger.error("Exception in agent: {}", t);
        }
    }

    /**
     * Prints the number of threads to screen and returns number as string
     * @return number of threads as string
     */
    public static String report_number_of_threads() {
        Pantools.logger.info("Number of threads: {}", THREADS);
        return String.valueOf(THREADS);
    }

    /**
     * The 'last_grouping_successful' property is created at the end of the 'group' function
     */
    public static void stop_if_no_active_grouping_present() {
        Node pangenome_node = GRAPH_DB.findNodes(PANGENOME_LABEL).next();
        boolean last_grouping_successful = false;
        if (pangenome_node.hasProperty("last_grouping_successful")) {
            last_grouping_successful = (boolean) pangenome_node.getProperty("last_grouping_successful");
        }
        if (!last_grouping_successful) {
            Pantools.logger.error("No (correct) grouping is present. Please run 'group'.");
            System.exit(1);
        }
    }

    /**
     * Writes a sequence in a FASTA file with specified length for lines.
     *
     * @param fasta_file The FASTA file object
     * @param seq The sequence to be written in the file.
     * @param length Length of the lines.
     */
    public static void write_fasta(BufferedWriter fasta_file, String seq, int length) {
        int i;
        try {
            for (i = 1; i <= seq.length(); ++i) {
                fasta_file.write(seq.charAt(i - 1));
                if (i % length == 0) {
                    fasta_file.write("\n");
                }
            }
            fasta_file.write("\n");
        } catch (IOException ioe) {

        }
    }

    /**
     * Calsulates the reverse complement of a given string.
     *
     * @param s The input string
     * @param rev builds the reverse complement of the input string in the StringBuilder rev
     */
    public static void reverse_complement(StringBuilder rev, String s) {
        int j;
        rev.setLength(0);
        for (j = s.length() - 1; j >= 0; --j)
            rev.append(complement(s.charAt(j)));
    }

    /**
     * Calculates the complement of an IUPAC symbol
     *
     * @param ch The input symbol
     * @return The complement symbol
     */
    public static char complement(char ch) {
        switch (ch) {
            case 'A':
                return 'T';
            case 'C':
                return 'G';
            case 'G':
                return 'C';
            case 'T':
                return 'A';
            case 'R':
                return 'Y';
            case 'Y':
                return 'R';
            case 'K':
                return 'M';
            case 'M':
                return 'K';
            case 'B':
                return 'V';
            case 'V':
                return 'B';
            case 'D':
                return 'H';
            case 'H':
                return 'D';
            default:
                return ch;
        }
    }

    public static void printProgressBar(long total, long current, String addition, boolean every_hundred) {
        StringBuilder string = new StringBuilder(140);
        int percent = (int) (current * 100 / total);
        string
                .append('\r')
                .append(String.join("", Collections.nCopies(percent == 0 ? 2 : 2 - (int) (Math.log10(percent)), " ")))
                .append(String.format(" %d%% [", percent))
                .append(String.join("", Collections.nCopies(percent, "=")))
                .append('>')
                .append(String.join("", Collections.nCopies(100 - percent, " ")))
                .append(']')
                .append(String.join("", Collections.nCopies((int) (Math.log10(total)) - (int) (Math.log10(current)), " ")))
                .append(String.format(" %d/%d " + addition, current, total));
        if (!every_hundred || (current % 100 == 0 || current == total)) {
            Pantools.logger.info(string.toString());
        }
    }

    /**
     * Executes a shell command.
     * @param command the shell command
     * @return the output (both stdout and stderr) of the command
     */
    public static String runCommandWithOutput(String command) {
        List<String> cmd = new ArrayList<>();
        cmd.add("sh");
        cmd.add("-c");
        cmd.add(command);
        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectErrorStream(true);
        try {
            Process p = pb.start();
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder output = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }
            return output.toString();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Executes a shell command in a limited number of seconds.
     *
     * @param command The command
     * @param timeout The number of seconds before the command is killed
     * @return The output of the bash command
     */
    public static boolean executeCommand_for(String command, int timeout) {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(command);
            StreamGobbler outputGobbler = new StreamGobbler(process.getInputStream()); //to make sure the process does not hang
            StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream()); //to make sure the process does not hang

            outputGobbler.start();
            errorGobbler.start();

            boolean finished = process.waitFor(timeout, TimeUnit.SECONDS);
            outputGobbler.join();
            errorGobbler.join();

            return finished && process.exitValue() == 0;
        } catch (IOException | InterruptedException e) {
            Pantools.logger.error("Error executing command: {}", command, e);
            return false;
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }

    private static class StreamGobbler extends Thread {
        private final InputStream inputStream;

        public StreamGobbler(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    Pantools.logger.trace("{}", line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Determines if a file is in FASTA format.
     *
     * @param file_name Path to the file
     * @return True if file is in FASTA format, or False
     */
    public static boolean is_fasta(String file_name) {
        try {
            BufferedReader in = open_file(file_name);
            String line;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.equals("")) {
                    // do nothing
                } else {
                    in.close();
                    return line.charAt(0) == '>';
                }
            }
        } catch (IOException ex) {
            Pantools.logger.warn("Error while reading file {}", file_name);
        }
        return false;
    }

    /**
     * Determines if a file is in FASTQ format.
     *
     * @param file_name Path to the file
     * @return True if file is in FASTQ format, or False
     */
    public static boolean is_fastq(String file_name) {
        try {
            BufferedReader in = open_file(file_name);
            String line;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.equals(""))
                    continue;
                else {
                    in.close();
                    return line.charAt(0) == '@';
                }
            }
        } catch (IOException ex) {
            Pantools.logger.warn("Error while reading file {}", file_name);
        }
        return false;
    }

    /**
     * Opens a possibly compressed file.
     *
     * @param filename Path to the file
     * @return The buffered reader to the input file
     */
    public static BufferedReader open_file(String filename) {
        try {
            String[] fields = filename.split("\\.");
            String file_type = fields[fields.length - 1].toLowerCase();
            if (file_type.equals("gz") || file_type.equals("gzip") || file_type.equals("bz2") || file_type.equals("bzip2"))
                return getBufferedReaderForCompressedFile(filename);
            else
                return new BufferedReader(new BufferedReader(new FileReader(filename)));
        } catch (IOException ex) {
            Pantools.logger.error("Error while reading file {}", filename);
            return null;
        }
    }

    /**
     * Counts the number of lines in a file.
     *
     * @param file_name Path to the file
     * @param skip_empty determines if empty lines should be skipped
     * @return The count of the lines
     */
    public static int get_lines_count(String file_name, boolean skip_empty) {
        int count = 0;
        try {
            BufferedReader in = open_file(file_name);
            String line;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (skip_empty && line.equals(""))
                    continue;
                ++count;
            }
            in.close();
        } catch (IOException ex) {
            Pantools.logger.warn("Error while reading file {}", file_name);
        }
        return count;
    }

    /**
     * Gives the buffered reader object for a compressed file.
     *
     * @param fileIn Path of the input file
     * @return
     */
    public static BufferedReader getBufferedReaderForCompressedFile(String fileIn) {
        try {
            FileInputStream fin = new FileInputStream(fileIn);
            BufferedInputStream bis = new BufferedInputStream(fin);
            CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
            BufferedReader br2 = new BufferedReader(new InputStreamReader(input));
            return br2;
        } catch (Exception ex) {
            Pantools.logger.warn("Failed to open the compressed file {}", fileIn);
            return null;
        }
    }

    /**
     * Shuts down the graph database if the program halts unexpectedly.
     *
     * @param graphDb The graph database object
     */
    public static void registerShutdownHook(final GraphDatabaseService graphDb) {
        Runtime.getRuntime().addShutdownHook(new Thread(graphDb::shutdown));
    }

    /**
     * Throws exception if file does not exist
     * @param path
     */
    public static void checkInvalidPath(String path) throws FileNotFoundException {
        if (!check_if_file_exists(path)) {
            throw new FileNotFoundException(
                    path + " does not exist!"
            );
        }
    }

    /**
     * Returns whether the file exists or not
     * @param path Path to the file (String)
     * @return True if file exists, False otherwise
     */
    public static boolean check_if_file_exists(String path) {
        Pantools.logger.debug("Checking existence of: {}", path);
        File file = new File(path);
        boolean exists = file.exists();
        if (exists) { // if the file exists, check if anything is in there
            if (file.length() < 1) {
                exists = false;
            }
        }
        return exists;
    }

    /**
     * Returns whether the file exists or not
     * @param path Path to the file (File)
     * @return True if file exists, False otherwise
     */
    public static boolean check_if_file_exists(File path) {
        return check_if_file_exists(path.getAbsolutePath());
    }

    /**
     *
     * @param target_node
     * @param required_label1
     * @param stop
     * @return boolean
     */
    public static boolean test_if_correct_label(Node target_node, Label required_label1, boolean stop) throws RuntimeException {
        String required_label = required_label1.toString();
        boolean correct_label = false;
        Iterable<Label> all_labels = target_node.getLabels();
        for (Label label1 : all_labels) {
            String label_str = label1.toString();
            if (label_str.equals(required_label)) {
                correct_label = true;
            }
        }
        if (!correct_label && stop) {
            Pantools.logger.error("{} is not a '{}' node.", target_node, required_label);
            throw new RuntimeException("Node " + target_node + " is not a '" + required_label + "' node");
        }
        return correct_label;
    }

    /**
     * Almost all arrays holding counts for genome numbers placed in nodes have a zero at the beginning. I don't know why?!
     * @param original_array array
     * @return new array where the first value is removed.
     */
    public static int[] remove_first_position_array(int[] original_array) {
        int[] new_array = new int[original_array.length-1];
        for (int i=1; i < original_array.length; i++) { // skip the first position in the original array;
            new_array[i-1] = original_array[i];
        }
        return new_array;
    }

    /**
     * Almost all arrays holding counts for genome numbers placed in nodes have a zero at the beginning. I don't know why?!
     * @param original_array array
     * @return new array where the first value is removed.
     */
    public static long[] remove_first_position_array(long[] original_array) {
        long[] new_array = new long[original_array.length-1];
        for (int i=1; i < original_array.length; i++) { // skip the first position in the original array;
            new_array[i-1] = original_array[i];
        }
        return new_array;
    }

    /**
     * Write a StringBuilder to a file. File is written inside database directory
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void write_SB_to_file_in_DB(StringBuilder outputBuilder, String outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + outputFile))) {
            out.write(outputBuilder.toString());
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create: " + WORKING_DIRECTORY + outputFile, ioe);
        }
    }

    /**
     * Write a string to a file. File is written inside database directory
     * @param output_str string
     * @param outputFile (path to) file name
     */
    public static void write_string_to_file_in_DB(String output_str, String outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + outputFile))) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create: " + WORKING_DIRECTORY + outputFile, ioe);
        }
    }

    /**
     * Write a string to a file. File is written inside database directory
     * @param output_str string
     * @param outputFile (path to) file name in the database
     */
    public static void write_string_to_file_in_DB(String output_str, Path outputFile) {
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(WORKING_DIRECTORY).resolve(outputFile))) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create: " + WORKING_DIRECTORY + outputFile, ioe);
        }
    }

    /**
     * Write a StringBuilder to an already existing file. File is written inside database directory
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void append_SB_to_file_in_DB(StringBuilder outputBuilder, String outputFile) {
        String output_str = outputBuilder.toString();
        try (FileWriter fw = new FileWriter(WORKING_DIRECTORY + outputFile, true)) { // true allows to append the original instead of overwriting
            fw.write(output_str);
        } catch(IOException ioe) {
            throw new RuntimeException("Unable to create: " + WORKING_DIRECTORY + outputFile, ioe);
        }
    }

    /**
     * Append the content of one file to another (existing) file.
     * @param input_file input comes from this file
     * @param outputFile output is written to this file
     */
    public static void append_file_to_other_file(String input_file, String outputFile) {
        try (FileWriter fw = new FileWriter(outputFile, true)) { //the true will append the new data
            BufferedReader in = new BufferedReader(new FileReader(input_file));
            while (in.ready()) {
                String line = in.readLine();
                fw.write(line + "\n");
            }
            fw.close();
            in.close();
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read and write : {} {}", input_file, outputFile);
            System.exit(1);
        }
    }

    /**
     * Write a StringBuilder to a file
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void write_SB_to_file_full_path(StringBuilder outputBuilder, String outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile))) {
            out.write(outputBuilder.toString());
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}", outputFile);
            System.exit(1);
        }
    }

    /**
     * Write a StringBuilder to a file
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void write_SB_to_file_full_path(StringBuilder outputBuilder, Path outputFile) {
        try (BufferedWriter out = Files.newBufferedWriter(outputFile)) {
            out.write(outputBuilder.toString());
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to create: {}", outputFile);
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a String to a file
     * @param output_str string
     * @param outputFile (path to) file name
     */
    public static void write_string_to_file_full_path(String output_str, String outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile))) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a String to a file
     * @param output_str string
     * @param outputFile (path to) file name
     */
    public static void write_string_to_file_full_path(String output_str, Path outputFile) {
        try (BufferedWriter out = Files.newBufferedWriter(outputFile)) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a String to an already existing file
     * @param output_str string
     * @param outputFile (path to) file name
     */
    public static void appendStringToFileFullPath(String output_str, String outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile, true))) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a String to an already existing file
     * @param output_str string
     * @param outputFile (path to) file name
     */
    public static void appendStringToFileFullPath(String output_str, Path outputFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile.toFile(), true))) {
            out.write(output_str);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a StringBuilder to an already existing file
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void append_SB_to_file_full_path(StringBuilder outputBuilder, String outputFile) {
        String output_str = outputBuilder.toString();
        try (FileWriter fw = new FileWriter(outputFile, true)) { //the true will append the new data
            fw.write(output_str);
        } catch(IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    /**
     * Write a StringBuilder to an already existing file
     * @param outputBuilder StringBuilder
     * @param outputFile (path to) file name
     */
    public static void append_SB_to_file_full_path(StringBuilder outputBuilder, Path outputFile) {
        String output_str = outputBuilder.toString();
        try (FileWriter fw = new FileWriter(outputFile.toFile(), true)) { //the true will append the new data
            fw.write(output_str);
        } catch(IOException ioe) {
            throw new RuntimeException("Unable to create " + outputFile, ioe);
        }
    }

    public static void copyFile(String string1, String string2) { // original and new position
        File file1 = new File(string1);
        File file2 = new File(string2);
        try {
            FileUtils.copyFile(file1, file2);
        } catch (IOException ioe) {

        }
    }

    public static void copy_directory(String string1, String string2) {
        File file1 = new File(string1);
        File file2 = new File(string2);
        try {
            FileUtils.copyDirectory(file1, file2);
        } catch (IOException ioe) {

        }
    }

    public static void delete_file_full_path(String file_name) {
        new File(file_name).delete();
    }

    public static void delete_file_full_path(Path file_name) {
        file_name.toFile().delete();
    }

    public static void delete_file_in_DB(String file_name) {
        File file = new File(WORKING_DIRECTORY + file_name);
        file.delete();
    }

    /**
     * Read the standard out of the executed command
     * @param command
     * @param min_length
     * @param toolName
     * @param stop stop pantools if program cannot be found
     * @return
     */
    public static boolean check_if_program_exists_stdout(String command, int min_length, String toolName, boolean stop) {
        StringBuilder exe_output = new StringBuilder();
        String line = "";
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = reader.readLine()) != null) {
                exe_output.append(line).append("\n");
            }
        } catch (Exception e) {
            if (stop) {
                e.printStackTrace();
            }
        }

        String log_str = exe_output.toString();
        if (log_str.length() < min_length) {
            if (stop) {
                Pantools.logger.error("{} is not installed ({}).", toolName, log_str.length());
                throw new RuntimeException("Requires correct installion of " + toolName);
            } else {
                Pantools.logger.warn("{} is not installed ({}).", toolName, log_str.length());
            }
            return false;
        }
        return true;
    }

    /**
     * Read the standard out of the executed command
     * @param command
     * @param min_length
     * @param toolName
     * @param stop stop pantools if program cannot be found
     * @return
     */
    public static boolean checkIfProgramExistsUsingStdout(String command, int min_length, String toolName, boolean stop, boolean print) {
        StringBuilder exe_output = new StringBuilder();
        String line = "";
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = reader.readLine()) != null) {
                exe_output.append(line).append("\n");
            }
        } catch (Exception e) {
            if (stop) {
                e.printStackTrace();
            }
        }

        String log_str = exe_output.toString();
        if (log_str.length() < min_length) {
            Level severity = stop ? Level.ERROR : Level.WARN;
            if (print) {
                Pantools.logger.log(severity, "{} is not installed.", toolName);
            }
            if (stop) {
                System.exit(1);
            }
            return false;
        }
        return true;
    }

    public static String getSubgenomeId(String sequenceId, boolean alwaysUnderscore) {
        String[] sequenceIdArray = sequenceId.split("_"); // 1_1 becomes [1,1]
        String[] phasing_info = PhasedFunctionalities.phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
        String subgenome;
        String underscore = "";
        if (alwaysUnderscore){
            underscore = "_";
        }
        if (phasing_info == null) {
            subgenome = sequenceIdArray[0] + "_unphased";
        } else {
            subgenome = sequenceIdArray[0] + underscore + phasing_info[1];
        }
        return subgenome;
    }

    /**
     * Read the standard error of the executed command
     * @param command
     * @param min_length
     * @param toolname
     * @param stop
     * @return
     */
    public static boolean check_if_program_exists_stderr(String command, int min_length, String toolname, boolean stop) {
        StringBuilder exe_output = new StringBuilder();
        try {
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String s = null;
            while ((s = stdError.readLine()) != null) {
                exe_output.append(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String log_str = exe_output.toString();
        boolean exists = true;
        if (log_str.length() < min_length && stop) {
            Pantools.logger.error("{} is not installed ({}).", toolname, log_str.length());
            throw new RuntimeException("Program not installed");
        } else if (log_str.length() < min_length) {
            exists = false;
        }
        return exists;
    }

    /**
     *  Verify if PATH_TO_THE_PANGENOME_DATABASE actually contains a graph database
     */
    public static void check_database() {
        if (PATH_TO_THE_PANGENOME_DATABASE == null) {
            Pantools.logger.error("No database directory was provided.");
            System.exit(1);
        }
        File file = new File(PATH_TO_THE_PANGENOME_DATABASE);
        if (!file.exists()) {
            Pantools.logger.error("The provided database was not found: {}", PATH_TO_THE_PANGENOME_DATABASE);
            System.exit(1);
        } else if (!new File(PATH_TO_THE_PANGENOME_DATABASE + GRAPH_DATABASE_PATH).exists()) {
            Pantools.logger.error("Unable to open the database provided: {}", PATH_TO_THE_PANGENOME_DATABASE);
            System.exit(1);
        }
    }

    /**
     * Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
     */
    public static void retrieve_phenotypes() {
        geno_pheno_map = new HashMap<>();
        phenotype_map = new HashMap<>();
        phenotype_threshold_map = new HashMap<>();
        if (PHENOTYPE == null) {
            Pantools.logger.info("No phenotype was provided (via --phenotype).");
            return;
        }
        int pheno_node_count = (int) count_nodes(PHENOTYPE_LABEL);
        if (pheno_node_count == 0) {
            Pantools.logger.error("No phenotype nodes are present yet. Please run 'add_phenotypes'.");
            System.exit(1);
        }
        ResourceIterator<Node> pheno_nodes = GRAPH_DB.findNodes(PHENOTYPE_LABEL);
        HashMap<String, ArrayList<Integer>> temp_phenotype_map = new HashMap<>();
        Object value;
        while (pheno_nodes.hasNext()) {
            Node pheno_node = pheno_nodes.next();
            int current_genome = (int) pheno_node.getProperty("genome");
            if (pheno_node.hasProperty(PHENOTYPE)) {
                value = pheno_node.getProperty(PHENOTYPE);
            } else {
                Pantools.logger.error("The provided phenotype '{}' was not found in the phenotype nodes.", PHENOTYPE);
                System.exit(1);
                continue;
            }


            if (value instanceof String) { // It's a String
                String value_str = value.toString();
                if (value_str.equals("?")) {
                    value_str = "Unknown";
                }
                geno_pheno_map.put(current_genome, value_str);
                temp_phenotype_map.computeIfAbsent(value_str, s -> new ArrayList<>()).add(current_genome);
            } else if (value instanceof Integer) { // It's an Integer
                String value_str = value.toString();
                geno_pheno_map.put(current_genome, value_str);
                temp_phenotype_map.computeIfAbsent(value_str, s -> new ArrayList<>()).add(current_genome);
            } else if (value instanceof Boolean) { // It's a Boolean
                String value_str = value.toString();
                geno_pheno_map.put(current_genome, value_str);
                temp_phenotype_map.computeIfAbsent(value_str, s -> new ArrayList<>()).add(current_genome);
            } else {
                Pantools.logger.error("something else.. finish function 486324."); // TODO: what does this even mean?
                System.exit(1);
            }
        }

        for (String phenotype : temp_phenotype_map.keySet()) {
            ArrayList<Integer> genome_list = temp_phenotype_map.get(phenotype);
            int[] genome_array = genome_list.stream().mapToInt(i -> i).toArray();
            Arrays.sort(genome_array);
            phenotype_map.put(phenotype, genome_array);
        }
        prepare_phenotype_threshold();
    }

    /**
     * Prepare the threshold for phenotype SHARED or SPECIFIC units (genes, kmers functions).
     * --phenotype-threshold must be between 1 and 100
     */
    public static void prepare_phenotype_threshold() {
        for (String phenotype : phenotype_map.keySet()) {
            if (phenotype.equals("?")) {
                continue;
            }
            int[] value = phenotype_map.get(phenotype);
            int skip_counter = 0;
            for (int genome : value) {
                if (skip_array[genome-1]) {
                    skip_counter ++;
                }
            }
            if (skip_counter == value.length) {
                phenotype_threshold_map.put(phenotype, 0);
                continue;
            }
            int allowed = value.length-skip_counter;
            if (phenotype_threshold < 100) { // set by --phyenotype-threshold argument
                float allowed_float = (float) (value.length-skip_counter) * Math.round(phenotype_threshold) / 100;
                allowed = Math.round(allowed_float);
            }
            if (allowed == 0) {
                allowed ++;
            }
            phenotype_threshold_map.put(phenotype, allowed);
        }
    }

    /*
     * Retrieve location of R libraries from user profile, $PATH
     */
    public static String check_r_libraries_environment() {
        String R_LIBS = System.getenv("R_LIBS");
        if (R_LIBS == null) { // No R environment for libraries is set, using \"~/local/R_libs/\". "
            R_LIBS = "~/local/R_libs/";
        }
        if (!R_LIBS.endsWith("/")) {
            R_LIBS += "/";
        }
        return R_LIBS;
    }

    /**
     *
     * @param path full path to a directory
     */
    public static void delete_directory(String path) {
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException nee) {
            Pantools.logger.error("Unable to delete this directory: {}", path);
        }
    }

    /**
     * Delete directory
     * @param path
     */
    public static void delete_directory(Path path) {
        try {
            FileUtils.deleteDirectory(path.toFile());
        } catch (IOException nee) {
            Pantools.logger.info("Unable to delete this directory: {}", path);
        }
    }

    public static void delete_directory_in_DB(String path) {
        try {
            FileUtils.deleteDirectory(new File(WORKING_DIRECTORY + path));
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to delete this directory: {}", path);
        }
    }

    /**
     * Count the number nodes matching a specific label
     * @param label1 the node label
     * @return
     */
    public static long count_nodes(Label label1) {
        long total_nodes = 0;
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(label1);
        while (all_nodes.hasNext()) {
            all_nodes.next();
            total_nodes ++;
        }
        return total_nodes;
    }

    /**
     * Count the number nodes matching a specific label AND a specific value of a property
     * @param node_label the node label
     * @param property
     * @param property_value the property value to match
     * @return
     */
    public static long count_nodes(Label node_label, String property, String property_value) {
        long total_nodes = 0;
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(node_label, property, property_value);
        while (all_nodes.hasNext()) {
            all_nodes.next();
            total_nodes ++;
        }
        return total_nodes;
    }

    /*
      Same as function above but with integer as property value
    */
    public static long count_nodes(Label node_label, String property, int property_value) {
        long total_nodes = 0;
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(node_label, property, property_value);
        while (all_nodes.hasNext()) {
            all_nodes.next();
            total_nodes ++;
        }
        return total_nodes;
    }

    /**
     * Check if any node with a specific label exists
     * @param node_label
     * @return
     */
    public static boolean is_node_present(Label node_label) {
        boolean present = false;
        ResourceIterator<Node> all_nodes = GRAPH_DB.findNodes(node_label);
        while (all_nodes.hasNext()) {
            all_nodes.next();
            present = true;
            if (present) {
                break;
            }
        }
        return present;
    }

    /**
     * Fills the variable WD_full_path, the full path of the directory instead of only the directory name
     */
    public static void create_full_path_working_directory() {
        int count = StringUtils.countMatches(WORKING_DIRECTORY, "../");
        String[] path_array = current_path.split("/");
        String temp = WORKING_DIRECTORY.replace ("../","");
        WD_full_path = "";
        for (int j=0; j< path_array.length-count; j++) {
            WD_full_path += path_array[j] + "/";
        }
        WD_full_path += temp;
        if (!new File(WD_full_path).exists()) {
            WD_full_path = WORKING_DIRECTORY;
        }
    }

    /**
     * Check if a program's location can be found using the linux command 'which PROGRAM_NAME'
     * @param command_str string to be executed
     * @return output of command
     */
    public static String find_program_location(String command_str) {
        write_log = true;
        delete_file_in_DB("tmp.log");
        String[] command = {"which", command_str};
        ExecCommand.ExecCommand(command);
        String line = "";
        try (BufferedReader in = new BufferedReader(new FileReader(WORKING_DIRECTORY + "tmp.log"))) {
            while (in.ready()) {
                line = in.readLine().trim();
            }
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to read: tmp.log");
        }
        if (line.length() == 0) {
            Pantools.logger.error("The program '{}' cannot be found.", command_str);
            System.exit(1);
        }
        delete_file_in_DB("tmp.log");
        write_log = false;
        return line;
    }

    /**
     * Check which grouping version is currently active. If no grouping is active, set 'grouping_version' to -1
     */
    public static void check_current_grouping_version() {
        try {
            grouping_version = (int) GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL).next().getProperty("group_version");
            String grouping_info = (String) GRAPH_DB.findNodes(PANGENOME_LABEL).next().getProperty("grouping_v" + grouping_version );
            if (grouping_info.contains("longest gene transcript")) {
                longest_transcripts = true;
            }
        } catch (NotFoundException | NoSuchElementException nee) {
            grouping_version = -1;
        }
    }

    /**
     * Check if database is pangenome or panproteome.
     * Sets the 'K_SIZE' and 'total_genomes' variables
     * @param function_name the function that was initialized by the user
     */
    public static void stop_if_panproteome(String function_name) {
        if (PROTEOME) {
            Pantools.logger.error("The function '{}' does not work on a panproteome.", function_name);
            System.exit(1);
        }
    }

    /**
     *
     * @param pathName full path to a directory
     * @param inDatabase
     */
    public static void deleteDirectory(String pathName, boolean inDatabase) {
        try {
            if (inDatabase) {
                FileUtils.deleteDirectory(new File(WORKING_DIRECTORY + pathName));
            } else {
                FileUtils.deleteDirectory(new File(pathName));
            }
        } catch (IOException nee) {
            Pantools.logger.error("Unable to delete this directory: {}", pathName);
        }
    }

    /**
     * Write a String to a file.
     * @param output String to be stored
     * @param filePath path to file name
     * @param inDatabase when true, write the file in to a location in the database
     * @param appendFile when false, overwrite the file if already present.
     */
    public static void writeStringToFile(String output, String filePath, boolean inDatabase, boolean appendFile) {
        File file;
        if (inDatabase) {
            file = new File(WORKING_DIRECTORY + filePath);
        } else {
            file = new File(filePath);
        }
        printMemoryUsage("Before writing to file");
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file, appendFile))) {
            out.write(output);
        } catch (IOException ioe) {
            Pantools.logger.error(ioe.getMessage());
            Pantools.logger.error("Unable to create: {}", file);
            throw new RuntimeException(ioe);
        }
        printMemoryUsage("After writing to file");
    }

    /**
     * Write a String to a file.
     * @param output String to be stored
     * @param file
     * @param appendFile when false, overwrite the file if already present.
     */
    public static void writeStringToFile(String output, File file, boolean appendFile) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(file, appendFile))) {
            out.write(output);
        } catch (IOException ioe) {
            Pantools.logger.error(ioe.getMessage());
            throw new RuntimeException("Unable to create: " + file);
        }
    }

    /*
     * Creates the directory named by the pathname, including necessary and non-existent parent directories.
     * @param pathName
     * @param inDatabase
     */
    public static void createDirectory(Path pathName, boolean inDatabase) {
        if (pathName == null) {
            Pantools.logger.error("Directory is not defined.");
            System.exit(1);
        }
        if (inDatabase) {
            new File(WORKING_DIRECTORY + pathName).mkdirs(); // create directory
        } else {
            pathName.toFile().mkdirs(); // create directory
        }
    }

    /**
     * Returns the size of a file
     * @param path
     * @return
     */
    public static boolean checkIfFileExists(String path) {
        //if (LOG) {
        //    System.out.println("Checking existence of: " + path);
        //}
        File file = new File(path);
        boolean exists = file.exists();
        if (exists) { // if the file exists, check if anything is in there
            if (file.length() < 1) {
                exists = false;
            }
        }
        return exists;
    }

    /**
     * Returns the size of a file
     * @param file
     * @return
     */
    public static boolean checkIfFileExists(File file) {
        //if (LOG) {
        //    System.out.println("Checking existence of: " + path);
        //}
        boolean exists = file.exists();
        if (exists) { // if the file exists, check if anything is in there
            if (file.length() < 1) {
                exists = false; // file exists but its empty
            }
        }
        return exists;
    }

    /**
     * Returns the size of a file
     * @param path
     * @return
     */
    public static boolean checkIfFileExists(Path path) {
        //if (LOG) {
        //    System.out.println("Checking existence of: " + path);
        //}
        boolean exists = false;
        if (Files.exists(path)) { // if the file exists, check if anything is in there
            File file = path.toFile();
            if (file.length() > 1) {
                exists = true; // file exists but its empty
            }
        }
        return exists;
    }

    public static HashMap<Integer, Node> retrieveGenomeNodes() {
        HashMap<Integer, Node> genomeNodesMap = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL);
            while (genomeNodes.hasNext()) {
                Node genomeNode = genomeNodes.next();
                int genomeNr = (int) genomeNode.getProperty("number");
                genomeNodesMap.put(genomeNr, genomeNode);
            }
            tx.success();
        }
        return genomeNodesMap;
    }

    /**
     *
     * @param size_list
     * @return
     */
    public static String determine_frequency_list_int(ArrayList<Integer> size_list) {
        ArrayList<Integer> distinct_size_list = remove_duplicates_from_AL_int(size_list);
        StringBuilder output = new StringBuilder();
        for (int size : distinct_size_list) {
            int occurrences = Collections.frequency(size_list, size);
            output.append(size).append("(").append(occurrences).append("x), ");
        }
        return output.toString().replaceFirst(".$", "").replaceFirst(".$", ""); // remove last character (2x)
    }

    /**
     *
     * @param list
     * @return
     */
    public static ArrayList<Integer> remove_duplicates_from_AL_int(ArrayList<Integer> list) {
        List<Integer> newList = list.stream()
                .distinct()
                .collect(Collectors.toList());
        Collections.sort(newList);
        ArrayList tester = new ArrayList<>(newList);
        return tester;
    }

    public static int[] sequenceIdToIntegers(String sequenceId) {
        String[] seq_array = sequenceId.split("_"); // 1_1 becomes [1,1]
        int genomeNr = Integer.parseInt(seq_array[0]);
        int sequenceNr = Integer.parseInt(seq_array[1]);
        return new int[]{genomeNr, sequenceNr};
    }

    public static int countLinesInFile(Path file) {
        int lines = 0;
        try {
            BufferedReader br = Files.newBufferedReader(file);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                lines++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    /**
     *
     * @param value1
     * @param value2
     * @param decimals number of decimals the percentage should be rounded to
     * @return
     */
    public static String percentageAsString(long value1, long value2, int decimals) {
        double percentage = ((double) value1 / (double) value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }

    /**
     * @param value1
     * @param value2
     * @param decimals  number of decimals the percentage should be rounded to
     * @return
     */
    public static String percentageAsString(int value1, int value2, int decimals) {
        double percentage = ((double) value1 / (double) value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }

    /**
     * @param value1
     * @param value2
     * @param decimals number of decimals the percentage should be rounded to
     * @return
     */
    public static String percentageAsString(double value1, double value2, int decimals) {
        double percentage = (value1 / value2) * 100;
        if (Double.isNaN(percentage)) {
            return "0";
        }
        return String.format("%." + decimals + "f", percentage);
    }

    /**
     *
     * @param value1
     * @param value2
     * @return
     */
    public static double divide(int value1, int value2) {
        double division = (double) value1 / (double) value2;
        if (Double.isNaN(division)) {
            division = 0;
        }
        return division;
    }

    /**
     *
     * @param value1
     * @param value2
     * @return
     */
    public static double divide(long value1, long value2) {
        double division = (double) value1 / (double) value2;
        if (Double.isNaN(division)) {
            division = 0;
        }
        return division;
    }

    /**
     * Splits sequences based on length
     * @param sequence sequence
     * @param length length to split sequence on
     * @return splitted sequence
     */
    public static String splitSeqOnLength(String sequence, int length) {
        StringBuilder sequenceSB = new StringBuilder();
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            sequenceSB.append(c);
            if ((i+1) % length == 0) {
                sequenceSB.append("\n");
            }
        }
        String sequenceString = sequenceSB.toString();
        if (sequenceString.endsWith("*")) {
            sequenceString = sequenceString.replaceAll("\\*$", "");
        }
        return sequenceString;
    }

    /**
     * Splits sequences based on length
     * @param sequence sequence
     * @param length length to split sequence on
     * @return splitted sequence
     */
    public static String splitSeqOnLength(StringBuilder sequence, int length) {
        return splitSeqOnLength(sequence.toString(), length);
    }

    /**
     * Parse a file with homology groups (comma separated)
     * @param hmFile a file with homology groups (comma separated)
     * @return a list with homology groups
     * @throws IOException if file cannot be read
     */
    public static List<Long> parseHmFile(Path hmFile) throws IOException {
        ArrayList<Long> hmGroups = new ArrayList<>();
        try (BufferedReader in = Files.newBufferedReader(hmFile)) {
            while (in.ready()) {
                String line = in.readLine().trim();

                if (line.length() == 0) continue;
                if (line.startsWith("#")) continue;
                if (line.startsWith("Genome")) continue;
                if (line.contains(" genomes")) continue;
                if (line.contains("Phenotype")) continue;

                for (String s : line.replaceAll(",$", "").split(",")) {
                    hmGroups.add(Long.parseLong(s));
                }
            }
        } catch (IOException e) {
            Pantools.logger.error("Could not read file: {}", hmFile);
            throw new IOException(e.getMessage());
        } catch (NumberFormatException e) {
            Pantools.logger.error("Could not parse file: {}", hmFile);
            throw new NumberFormatException(e.getMessage());
        }
        return hmGroups;
    }

    /**
     * Get all currently active homology groups (if hmGroups is null, otherwise get the homology groups in hmGroups)
     * @param hmGroups a list of homology group ids (in case of null, all homology groups are retrieved)
     * @param minimumMembers the minimum number of members a homology group should have
     * @return arraylist of homology group nodes that have more than minimumMembers members
     * NB: has to be called within a transaction
     */
    public static ArrayList<Node> findHmNodes(List<Long> hmGroups, int minimumMembers) throws RuntimeException {
        final ArrayList<Node> hmList = new ArrayList<>();
        if (hmGroups == null) {
            Pantools.logger.info("No homology groups were provided, using all.");
            try (ResourceIterator<Node> hmNodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL)) {
                while (hmNodes.hasNext()) {
                    Node hmNode = hmNodes.next();
                    int numMembers = (int) hmNode.getProperty("num_members");
                    if (numMembers > minimumMembers) {
                        hmList.add(hmNode);
                    }
                }
            } catch (Exception e) {
                Pantools.logger.error("Error while retrieving homology groups.");
                throw new RuntimeException(e);
            }
        } else {
            Pantools.logger.info("Using homology groups provided by user.");
            for (Long hmGroup : hmGroups) {
                try {
                    Node hmNode = GRAPH_DB.getNodeById(hmGroup);
                    test_if_correct_label(hmNode, HOMOLOGY_GROUP_LABEL, true);
                    hmList.add(hmNode);
                } catch (NotFoundException nfe) {
                    Pantools.logger.error("Homology group {} was not found.", hmGroup);
                    throw new RuntimeException(nfe);
                }
            }
        }
        return hmList;
    }

    public static ArrayList<Long> convertHmNodestoIds(List<Node> hmNodes) {
        final ArrayList<Long> hmIdList = new ArrayList<>();
        for (Node hmNode : hmNodes) {
            hmIdList.add(hmNode.getId());
        }
        return hmIdList;
    }

    public static HashMap<String, ArrayList<int[]>> get_gene_addresses_per_seq(HashMap<String, Node[]> gene_nodes_per_seq) {
        HashMap<String, ArrayList<int[]>> gene_addresses_per_seq = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            for (String key : gene_nodes_per_seq.keySet()) {
                Node[] gene_nodes = gene_nodes_per_seq.get(key);
                for (Node gene_node : gene_nodes) {
                    int[] address = (int[]) gene_node.getProperty("address");
                    ArrayList<int[]> list = gene_addresses_per_seq.get(key);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(address);
                    gene_addresses_per_seq.put(key, list);
                }
            }
            tx.success();
        }
        return gene_addresses_per_seq;
    }

    /**
     * Safe to transform long into int. Maximum integer is 2.147.483.647 which is longer than any (expected) chromosome length
     * @return
     */
    public static HashMap<String, Integer> getSequenceLengths(LinkedHashSet<String> selectedSequences) {
        HashMap<String, Integer> seq_length_map = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            int total_sequences = (int) count_nodes(SEQUENCE_LABEL);
            ResourceIterator<Node> seq_nodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            int seq_counter = 0;
            while (seq_nodes.hasNext()) {
                seq_counter++;
                if (seq_counter % 10000 == 0 || seq_counter == total_sequences) {
                    System.out.print("\rRetrieving sequence lengths: Sequence " + seq_counter + "/" + total_sequences + "  ");
                }
                Node seq_node = seq_nodes.next();
                String sequenceId = (String) seq_node.getProperty("identifier");
                String[] seq_id_array = sequenceId.split("_"); // 1_1 -> [1,1]
                int genome_nr = Integer.parseInt(seq_id_array[0]);
                int sequence_nr = Integer.parseInt(seq_id_array[1]);
                if (!selectedSequences.contains(genome_nr + "_" + sequence_nr)){
                    continue;
                }
                long seq_length = (long) seq_node.getProperty("length");
                seq_length_map.put(sequenceId, (int) seq_length);
            }
            System.out.println(""); // TODO: part of progress bar
            tx.success();
        }
        return seq_length_map;
    }

    /**
     * Calculate the median of a list of numbers. Sorts the array.
     *
     * @param numbersList list of numbers
     * @param <T> type of number (Integer, Long, Double etc.)
     * @return median of the given list
     */
    public static <T extends Number & Comparable<T>> double median(List<T> numbersList) {
        if (numbersList.isEmpty()) {
            throw new IllegalArgumentException("Cannot compute median on an empty list of numbers");
        }
        Collections.sort(numbersList);
        final int middle = numbersList.size()/2;
        if (numbersList.size() % 2 == 0) {
            return 0.5 * (numbersList.get(middle).doubleValue() + numbersList.get(middle-1).doubleValue());
        } else {
            return numbersList.get(middle).doubleValue();
        }
    }

    /**
     * Calculate the average of a list of numbers.
     *
     * @param numbersList list of numbers
     * @param <T> type of number (Integer, Long, Double etc.)
     * @return average of the given list
     */
    public static <T extends Number & Comparable<T>> double average(List<T> numbersList) {
        if (numbersList.isEmpty()) {
            throw new IllegalArgumentException("Cannot compute average on an empty list of numbers");
        }
        double sum = 0;
        for (Number number : numbersList) {
            sum += number.doubleValue();
        }
        return sum / numbersList.size();
    }

    /**
     * Calculate the average of an array of integers.
     *
     * @param numbersArray array of integers
     * @return average of the given list
     */
    public static double average(int[] numbersArray) {
        if (numbersArray.length == 0) {
            throw new IllegalArgumentException("Cannot compute average on an empty array of numbers");
        }
        return Arrays.stream(numbersArray).sum() / (double) numbersArray.length;
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     * @param position
     */
    public static void incr_array_hashmap(HashMap<String, int[]> hashmap, String key, int value, int position) {
        int[] value_array = hashmap.get(key);
        value_array[position] += value;
        hashmap.put(key, value_array);
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     * @param position
     */
    public static void incr_array_hashmap(HashMap<Integer, int[] > hashmap, int key, int value, int position) {
        int[] value_array = hashmap.get(key);
        value_array[position] += value;
        hashmap.put(key, value_array);
    }

    /**
     *
     * @param hashmap
     * @param key
     * @param value
     * @param position
     * @param array_size
     */
    public static void try_incr_array_hashmap(HashMap<String, int[] > hashmap, String key, int value, int position,
                                              int array_size) {
        int[] value_array = hashmap.get(key);
        if (value_array == null) {
            value_array = new int[array_size];
        }
        value_array[position] += value;
        hashmap.put(key, value_array);
    }

    /**
     * Print memory usage to debug log
     * @param checkpoint checkpoint message
     */
    public static void printMemoryUsage(String checkpoint) {
        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
        MemoryUsage nonHeapUsage = memoryBean.getNonHeapMemoryUsage();
        long mb = 1024 * 1024;
        Pantools.logger.debug(String.format("  %s: Heap: %d MB / %d MB, Non-Heap: %d MB / %d MB",
                checkpoint,
                heapUsage.getUsed() / mb, heapUsage.getMax() / mb,
                nonHeapUsage.getUsed() / mb, nonHeapUsage.getMax() / mb));
    }
}
