package nl.wur.bif.pantools.utils.cli.validation.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import nl.wur.bif.pantools.utils.cli.validation.Constraints;

import java.io.InputStream;
import java.nio.file.Paths;

import static org.apache.logging.log4j.core.util.Loader.getClassLoader;


public class ScoringMatrixValidator implements ConstraintValidator<Constraints.ScoringMatrix, String> {

    @Override
    public void initialize(Constraints.ScoringMatrix constraintAnnotation) {
    }

    @Override
    public boolean isValid(String matrixName, ConstraintValidatorContext context) {
        if (matrixName == null) return true;

        // get matrix files from the resources/scoring-matrices directory
        final String resourceLocation = Paths.get("scoring-matrices").resolve(matrixName).toString();
        final InputStream inputStream = getClassLoader().getResourceAsStream(resourceLocation);
        return inputStream != null;
    }
}
