package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.construction.index.IndexDatabase;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.construction.sequence.SequenceDatabase;
import org.codehaus.plexus.util.StringUtils;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.unsafe.impl.batchimport.input.DataException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.check_current_grouping_version;
import static nl.wur.bif.pantools.utils.Utils.connect_pangenome;

/**
 * Create, load and validate neo4j graph databases
 *
 * @author Robin van Esch
 */
public final class GraphUtils {

    private GraphUtils() {}

    /**
     * Create a new graph database service for the Neo4j database.
     * A shutdown hook is configured to make sure the database is always shut down correctly.
     *
     * @param databaseDirectory path to the pangenome database directory
     */
    public static void createGraphDatabaseService(Path databaseDirectory) {
        final Path graphDatabasePath = databaseDirectory.resolve("databases").resolve("graph.db");
        GRAPH_DB = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(graphDatabasePath.toFile())
                .setConfig(GraphDatabaseSettings.keep_logical_logs, "4 files")
                .newGraphDatabase();

        Runtime.getRuntime().addShutdownHook(new Thread(GRAPH_DB::shutdown));
    }

    /**
     * Verify if the database contains a pangenome or a panproteome and set type-specific parameters.
     */
    public static void setDatabaseParameters() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> pangenomeNodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) {
            final Node pangenomeNode = pangenomeNodes.next();
            total_genomes = (int) pangenomeNode.getProperty("num_genomes");
            if (pangenomeNode.hasProperty("k_mer_size")) {
                K_SIZE = (int) pangenomeNode.getProperty("k_mer_size");
                connect_pangenome();
            } else {
                PROTEOME = true;
            }
            if (pangenomeNodes.hasNext()) {
                throw new DataException("Database contains more than one pangenome and can only contain one");
            }
        } catch (NotFoundException nfe) {
            throw new NotFoundException("Unable to find the pangenome node in the graph database");
        }
    }

    /**
     * Creates and connects to genome, index and graph databases of the pangenome.
     *
     * @param databaseDirectory path to the pangenome database directory
     * @throws IOException when directories cannot be created in the database directory
     */
    public static void createPangenomeDatabase(Path databaseDirectory) throws IOException {
        createGraphDatabaseService(databaseDirectory);
        GENOME_DB = new SequenceDatabase(WORKING_DIRECTORY + GENOME_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE);
        INDEX_DB = new IndexDatabase(WORKING_DIRECTORY + INDEX_DATABASE_PATH, PATH_TO_THE_GENOMES_FILE, GENOME_DB, K_SIZE);
        Files.createDirectories(databaseDirectory.resolve("databases").resolve("genome.db").resolve("genomes"));
        Files.createDirectory(databaseDirectory.resolve("log")); //TODO: remove
    }

    /**
     * Creates and connects to graph databases of the panproteome.
     *
     * @param databaseDirectory path to the pangenome database directory
     * @throws IOException when directories cannot be created in the database directory
     */
    public static void createPanproteomeDatabase(Path databaseDirectory) throws IOException {
        createGraphDatabaseService(databaseDirectory);
        Files.createDirectory(databaseDirectory.resolve("proteins"));
        Files.createDirectory(databaseDirectory.resolve("log")); //TODO: remove
    }

    /**
     * Validate that the database is a pangenome or a panproteome if one is required.
     *
     * @param type database type; panproteome or pangenome
     */
    public static void validateGraphType(String type) {
        final String databaseType;
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> pangenomeNodes = GRAPH_DB.findNodes(PANGENOME_LABEL)) {
            final Node pangenomeNode = pangenomeNodes.next();
            if (pangenomeNode.hasProperty("k_mer_size")) {
                databaseType = "pangenome";
            } else {
                databaseType = "panproteome";
            }
        }
        if (!databaseType.equals(type)) {
            throw new DataException(
                    String.format("%s database detected; this function is for %ss only",
                            StringUtils.capitalise(databaseType), type));
        }
    }

    /**
     * Check if the pangenome has genome nodes
     */
    public static void validateGenomes() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL)) {
            if (!genomeNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain genomes.");
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the genome nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains annotation and if not throw an exception
     */
    public static void validateAnnotations() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> annotationNodes = GRAPH_DB.findNodes(ANNOTATION_LABEL)) {
            if (!annotationNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain annotations. Please run add_annotations first.");
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the annotation nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome/panproteome contains homology groups and if not throw an exception
     */
    public static void validateHomologyGroups() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> homologyGroupNodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL)) {
            if (!homologyGroupNodes.hasNext()) {
                Pantools.logger.error("The pangenome/panproteome does not contain homology groups. Please run group first.");
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the homology group nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains accession information and if not throw an exception
     */
    public static void validateAccessions() {
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL)) {
            if (!accessionNodes.hasNext()) {
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find the accession nodes in the graph database.");
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * check if pangenome contains PAV/VCF accession information and if not throw an exception
     *
     * @param type type of accession information (PAV/VCF)
     */
    public static void validateAccessions(String type) {
        assert (type.matches("PAV|VCF"));
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, type, true)) {
            if (!accessionNodes.hasNext()) {
                Pantools.logger.error("The pangenome does not contain {} information.", type);
                throw new RuntimeException("Invalid graph structure");
            }
        } catch (NotFoundException nfe) {
            Pantools.logger.error("Could not find {} nodes in the graph database.", type);
            throw new DataException("Invalid graph structure");
        }
    }

    /**
     * Remove a node and ints relationships from the graph database.
     *
     * @param node node to remove
     */
    public static void removeNode(Node node) { // TODO: make general neo4j functions
        for (Relationship relationship : node.getRelationships()) {
            relationship.delete();
        }
        node.delete();
    }

    /**
     * Checks if this pangenome contains accessions (variants in VCF format) that were added with add_variants.
     *
     * @return true if the pangenome contains accessions, false otherwise
     */
    public static boolean containsVariantInformation() {
        final boolean containsVcf;
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> accessionNodes = GRAPH_DB.findNodes(ACCESSION_LABEL, "VCF", true)) {
            containsVcf = accessionNodes.hasNext();
        }
        return containsVcf;
    }

    /**
     * Checks if the database contains alignment output for the given alignment mode.
     * @param alignmentMethod alignment method to check (per-group, multiple-groups, regions, functions)
     * @param alignmentModeShort alignment mode to check (nuc, prot, var)
     * @param trimming whether the alignment has to be trimmed
     * @return true if the database contains alignment output, false otherwise
     */
    public static boolean containsAlignmentOutput(String alignmentMethod, String alignmentModeShort, boolean trimming) {
        if (!alignmentMethod.matches("per-group|multiple-groups|regions|functions")) {
            throw new IllegalArgumentException("Invalid alignment method");
        }
        if (!alignmentModeShort.matches("nuc|prot|var")) {
            throw new IllegalArgumentException("Invalid alignment mode");
        }

        final String fastaFileName = alignmentModeShort + (trimming ? "_trimmed" : "") + ".fasta";
        Path alignmentDirectory;
        boolean containsAlignmentOutput = false;
        long numberOfAlignments = 0;

        // check grouping version
        try (Transaction ignored = GRAPH_DB.beginTx()) {
            check_current_grouping_version();
        }

        // set the alignment directory based on the alignment method
        switch (alignmentMethod) {
            case "per-group":
            case "multiple-groups":
                alignmentDirectory = Paths.get(WORKING_DIRECTORY)
                        .resolve("alignments")
                        .resolve(alignmentModeShort.equals("var") ?
                                "msa_" + alignmentMethod.replace("-", "_") + "_var" :
                                "msa_" + alignmentMethod.replace("-", "_"))
                        .resolve("grouping_v" + grouping_version);
                break;
            case "regions":
                alignmentDirectory = Paths.get(WORKING_DIRECTORY)
                        .resolve("alignments")
                        .resolve("msa_regions");
                break;
            case "functions":
                alignmentDirectory = Paths.get(WORKING_DIRECTORY)
                        .resolve("alignments")
                        .resolve("msa_functions");
                break;
            default:
                throw new NoSuchElementException("Invalid alignment method");
        }

        // check if the alignment directory exists and count the number of alignments
        if (Files.exists(alignmentDirectory)) {
            Pantools.logger.debug("Found alignment directory {}", alignmentDirectory);
            containsAlignmentOutput = true;
            try (Stream<Path> paths = Files.list(alignmentDirectory)) {
                numberOfAlignments = paths
                        .filter(Files::isDirectory)
                        .filter(path -> {
                            final File file = path.resolve("output").resolve(fastaFileName).toFile();
                            return file.exists() && file.length() > 0;
                        })
                        .count();
            } catch (IOException e) {
                Pantools.logger.error("Unable to list directories in {}", alignmentDirectory);
                throw new RuntimeException("Unable to list directories");
            }
        } else {
            Pantools.logger.debug("Alignment directory {} does not exist", alignmentDirectory);
        }

        // if no alignments were found, the database does not contain correct alignment output
        if (numberOfAlignments == 0) {
            containsAlignmentOutput = false;
        }

        // log the number of alignments that were found
        Pantools.logger.debug("Found {} alignments for alignment method {} and alignment mode '{}'",
                numberOfAlignments, alignmentMethod, alignmentModeShort);

        // return whether the database contains alignment output
        return containsAlignmentOutput;
    }

    /**
     * Gets the name of the mrna node, if it has one
     * @param mrnaNode node to get name from
     * @return name of mrna node
     * NB: has to be run within a transaction
     * NB: name used to be stored as a string, nowadays as a string array (keeping old style for backwards compatibility)
     */
    public static String getMrnaNodeName(Node mrnaNode) {
        assert mrnaNode.hasLabel(MRNA_LABEL);

        String name = "-";
        if (mrnaNode.hasProperty("name")) {
            if (mrnaNode.getProperty("name") instanceof String) { // old style
                name = (String) mrnaNode.getProperty("name");
            } else if (mrnaNode.getProperty("name") instanceof String[]) { // new style
                String[] nameArray = (String[])(mrnaNode.getProperty("name"));
                name = Arrays.toString(nameArray).replace("[","").replace("]","").replace(",","/").replace(" ","");
            } else {
                throw new RuntimeException("Unexpected type for property 'name' of node " + mrnaNode.getId());
            }
        }

        return name;
    }

    /**
     * Gets all accession identifiers connected to a genome node, if it has any
     * @param genomeNr genome number to get accessions from
     * @return set of accession identifiers
     */
    public static Set<String> getVcfAccessions(int genomeNr) {
        Set<String> accessions = new HashSet<>();
        try (Transaction ignored = GRAPH_DB.beginTx();
             ResourceIterator<Node> genomeNodes = GRAPH_DB.findNodes(GENOME_LABEL, "number", genomeNr)) {
            if (genomeNodes.hasNext()) {
                Node genomeNode = genomeNodes.next();
                for (Relationship accessionRelationship : genomeNode.getRelationships(RelTypes.has)) {
                    Node accessionNode = accessionRelationship.getEndNode();
                    if (accessionNode.hasLabel(ACCESSION_LABEL) && accessionNode.hasProperty("VCF")) {
                        String sample = (String) accessionNode.getProperty("sample");
                        if ((boolean) accessionNode.getProperty("VCF")) {
                            accessions.add(String.format("%s|%s", genomeNr, sample));
                        } else {
                            Pantools.logger.trace("Skipping non-VCF accession {}", sample);
                        }
                    }
                }
            }
        }
        return accessions;
    }
}
