/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.Pantools;
import org.neo4j.graphdb.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static nl.wur.bif.pantools.analysis.phylogeny.Phylogeny.addStringBeforeFinalDot;
import static nl.wur.bif.pantools.utils.Globals.*;
import static nl.wur.bif.pantools.utils.Utils.*;

/**
 *
 * @author Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class PhasedFunctionalities {

    public static HashMap<String, String[]> phasingInfoMap;


    public static final String[] letters = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    final public static String[] COLOR_CODES2 = new String[]{"#e6194B", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4",
            "#42d4f4", "#f032e6", "#bfef45", "#fabed4", "#469990", "#dcbeff", "#9A6324", "#fffac8", "#800000", "#aaffc3",
            "#808000", "#ffd8b1", "#000075", "#a9a9a9", "#000000", "#ffffff"}; // length is 22, last one is white.

    public void call_experimental_code(String function_name) {
        switch (function_name) {

            case "rename_matrix2":
                rename_matrix2();
                break;

            default:
                Pantools.logger.info("experimental function not found.");
        }
    }

    /**
     * Return a phenotype value of a specific genome as a string when a --phenotype is included
     * @param sequenceId a sequence identifier: genome number '_' sequence number
     * @return empty when no phased analysis
     */
    public String createPhasingRenamingPhylogeny(String sequenceId, boolean requiresPhasingAnalysisBoolean) {
        String phasingId = "";
        if (PHASED_ANALYSIS || !requiresPhasingAnalysisBoolean) {
            String[] phasingInfo = phasingInfoMap.get(sequenceId);
            if (phasingInfo == null) {
                phasingId = "_unphased";
            } else {
                phasingId = "_" + phasingInfo[2].replace("unphased", "_unphased").replace("__","_"); // example [1, B, 1B, 1_B]
            }
        }
        return phasingId;
    }

    /**
     *
     * @param sequenceId
     * @return genome number with phasing letter
     */
    public String getSubgenomeIdentifier(String sequenceId) {
        String[] phasingInfo = phasingInfoMap.get(sequenceId); // example [1, D, 1D, 1_D]
        String[] sequenceIdArray = sequenceId.split("_");
        String genomePhase = sequenceIdArray[0] + "_unphased";
        if (phasingInfo != null) {
            genomePhase = sequenceIdArray[0] + "_" + phasingInfo[1];
        }
        return genomePhase;
    }

    /**
     *
     * @param sequenceId
     * @return chromosome number with phasing letter
     */
    public String getPhasingIdentifier(String sequenceId, boolean withUnderscore) {
        String phasingIdentifier = "unphased";
        String[] phasingInfo = phasingInfoMap.get(sequenceId); // example [1, D, 1D, 1_D]
        if (phasingInfo != null && withUnderscore) {
            phasingIdentifier = phasingInfo[3];
        } else if (phasingInfo != null) {
            phasingIdentifier = phasingInfo[2];
        }
        return phasingIdentifier;
    }
    /**
     * Determine the ploidy of genomes based on the distinct set of haplotype letters
     * @return array with the ploidy of all genomes in pangenome.
     */
    public HashMap<String, Integer> determinePloidyPerGenome(LinkedHashSet<String> selectedSequences, ArrayList<String> genomes) {
        HashMap<String, Integer> ploidyPerGenome = new HashMap<>(); // key is genome number (as string), value is ploidy
        if (phasingInfoMap == null || phasingInfoMap.isEmpty()) {
            return ploidyPerGenome;
        }

        // collect all haplotype letters from sequences
        HashMap<String, HashSet<String>> lettersPerGenome = new HashMap<>(); // key is genome number (as string), value is set with haplotype letters
        for (String sequenceId : phasingInfoMap.keySet()) {
            if (!selectedSequences.contains(sequenceId)) {
                continue;
            }
            if (!sequenceId.contains("_")) { // its a genome number
                continue;
            }
            String[] seqIdArray = sequenceId.split("_"); // 1_1 becomes [1,1]
            String[] phasingInfo = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            if (phasingInfo == null) { // no phasing information available for this sequence
                continue;
            }
            lettersPerGenome.computeIfAbsent(seqIdArray[0], s -> new HashSet<>()).add(phasingInfo[1]);
        }

        //count how many letters each genome has
        for (String genome : genomes) {
            int ploidy = 1;
            if (lettersPerGenome.containsKey(genome)) {
                ploidy = lettersPerGenome.get(genome).size();
            }
            ploidyPerGenome.put(genome, ploidy);
        }
        return ploidyPerGenome;
    }

    public static ArrayList<Integer> determine_sequence_overlap_in_AL(ArrayList<Integer> positions) {
        if (positions.size() == 2) { // only 1 sequence included
            return positions;
        }
        boolean finished = false;
        while (!finished) {
            boolean found = false;
            int new_pos1 = 0, new_pos2 = 0;
            TreeSet<Integer> remove = new TreeSet<>();
            for (int i = 0; i < positions.size()-1; i += 2) {
                for (int j = i+2; j < positions.size()-1; j += 2) {
                    int start_pos1 = positions.get(i);
                    int start_pos2 = positions.get(j);
                    int end_pos1 = positions.get(i+1);
                    int end_pos2 = positions.get(j+1);
                    //System.out.println(positions.get(i) + " " + positions.get(i+1) + " -> " + positions.get(j) + " " + positions.get(j+1));
                    if (start_pos2 <= start_pos1 && end_pos2 >= end_pos1) { // complete overlap
                        new_pos1 = start_pos2;
                        new_pos2 =  end_pos2;
                        found = true;
                    } else if (start_pos2 >= start_pos1 && end_pos2 <= end_pos1) { // complete overlap
                        new_pos1 = start_pos1;
                        new_pos2 =  end_pos1;
                        found = true;
                    } else if (start_pos2 >= start_pos1 && end_pos2 >= end_pos1 && start_pos2 <= end_pos1) { // last part overlaps
                        found = true;
                        new_pos1 = start_pos1;
                        new_pos2 =  end_pos2;
                    } else if (start_pos2 <= start_pos1 && end_pos2 <= end_pos1 && end_pos2 >= start_pos1 ) { // first part overlaps
                        new_pos1 = start_pos2;
                        new_pos2 =  end_pos1;
                        found = true;
                    }

                    if (found) {
                        //System.out.println(positions.get(i) + " " + positions.get(i+1) + " -> " + positions.get(j) + " " + positions.get(j+1) + " new " + new_pos1 + " " + new_pos2 );
                        remove.add(i);
                        remove.add(i+1);
                        remove.add(j);
                        remove.add(j +1);
                        remove = (TreeSet<Integer>) remove.descendingSet();
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
            if (found) { // two blocks overla
                for (int index : remove) {
                    positions.remove(index);
                }
                positions.add(new_pos1);
                positions.add(new_pos2);
            } else { // no more overlaps are found
                finished = true;
            }
        }
        return positions;
    }

     /**
         *  types of keys
         *  1. genome number
         *  2. genome 1 number + "#" genome 2 number + "#" + number of genome 1 or 2
         *  3. sequence identifier
         *  4. sequence 1 identifier + "#" sequence 2 identifier + "#" + identifier of sequence 1 or 2
         */
    public static HashMap<String, Integer> calculate_syntenic_genes_per_sequence_genome(HashMap<String, HashSet<Node>> collinear_genes_map) {
        HashMap<String, Integer> syntelog_gene_count_map = new HashMap<>();
        HashMap<String, HashSet<Node>> genomeGenomeShared = new HashMap<>();
        Pantools.logger.info("syntenic genes between seqs.");
        HashSet<String> sequence_combinations = new HashSet<>();
        for (String sequence_combi_key : collinear_genes_map.keySet()) {
            String[] sequence_combi_array = sequence_combi_key.split("#");
            sequence_combinations.add(sequence_combi_array[0] + "#" + sequence_combi_array[1]);
        }

        for (String sequenceCombi : sequence_combinations) {
            String[] sequence_combi_array = sequenceCombi.split("#"); // example is 1_1#2_1
            String[] seq1_array = sequence_combi_array[0].split("_"); // example is 1_1
            String[] seq2_array = sequence_combi_array[1].split("_"); // example is 2_1
            HashSet<Node> mrna_set1 = collinear_genes_map.get(sequenceCombi + "#" + sequence_combi_array[0]);
            HashSet<Node> mrna_set2 = collinear_genes_map.get(sequenceCombi + "#" + sequence_combi_array[1]);
            collinear_genes_map.remove(sequenceCombi + "#" + sequence_combi_array[0]); // clear the map, content is no longer needed
            collinear_genes_map.remove(sequenceCombi + "#" + sequence_combi_array[1]);
            for (Node mrna_node : mrna_set1) {
                collinear_genes_map.computeIfAbsent(sequence_combi_array[0], k -> new HashSet<>()).add(mrna_node);
                genomeGenomeShared.computeIfAbsent(seq1_array[0] + "#" + seq2_array[0] + "#" + seq1_array[0], k -> new HashSet<>()).add(mrna_node);
            }
            for (Node mrna_node : mrna_set2) {
                collinear_genes_map.computeIfAbsent(sequence_combi_array[1], k -> new HashSet<>()).add(mrna_node);
                genomeGenomeShared.computeIfAbsent(seq1_array[0] + "#" + seq2_array[0] + "#" + seq2_array[0], k -> new HashSet<>()).add(mrna_node);
            }
            //System.out.println("synlogs " + sequence_combi_array[0] + ": " + mrna_set1.size() + " met " + sequence_combi_array[1] + ": " + mrna_set2.size());
            syntelog_gene_count_map.put(sequenceCombi + "#" + sequence_combi_array[0], mrna_set1.size()); // example key 1_1#2_1#1_1
            syntelog_gene_count_map.put(sequenceCombi + "#" + sequence_combi_array[1], mrna_set2.size()); // example key 1_1#2_1#2_1
            if (seq1_array[0].equals(seq2_array[0])) { // same genome
                //continue;
            }
            //syntelog_gene_count_map.merge(seq1_array[0] + "#" + seq2_array[0] + "#" + seq1_array[0], mrna_set1.size(), Integer::sum); // example key 1#2#1
            //syntelog_gene_count_map.merge(seq1_array[0] + "#" + seq2_array[0] + "#" + seq2_array[0], mrna_set2.size(), Integer::sum); // example key 1#2#2
        }

        for (String key : genomeGenomeShared.keySet()) {
            HashSet<Node> set = genomeGenomeShared.get(key);
            Pantools.logger.info("{} {}", key, set.size());
            syntelog_gene_count_map.put(key, set.size());
        }

        //System.out.println("sum of distinct syntenic genes per sequence:");
        int[] syntenic_per_genome = new int[total_genomes];
        for (String seq_id : collinear_genes_map.keySet()) {
            if (seq_id.contains("#")) { // skip example is 1_1#2_1, continue with 1_1 or 2_1 etc.
                continue;
            }
            int syntenic_genes = collinear_genes_map.get(seq_id).size();
            //System.out.println(seq_id + " " + syntenic_genes);
            String[] seq_array = seq_id.split("_");
            int genome_nr = Integer.parseInt(seq_array[0]);
            syntenic_per_genome[genome_nr-1] += syntenic_genes;
            syntelog_gene_count_map.put(seq_id, syntenic_genes);
            syntelog_gene_count_map.merge(seq_array[0], syntenic_genes, Integer::sum);
        }
        collinear_genes_map.clear(); // clear the map, content is no longer neede
        return syntelog_gene_count_map;
    }

    public static HashMap<String, String> readMsaSequenceInfo(String homologyGroupNr) {
        String path = WORKING_DIRECTORY + "alignments/msa_per_group/grouping_v" + grouping_version + "/" + homologyGroupNr+ "/input/sequences.info";
        HashMap<String, String> mrnaSequenceIdMap = new HashMap<>();
        boolean startReading = false;
        try (BufferedReader in = new BufferedReader(new FileReader(path))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                if (line.equals("")) {
                    continue;
                }
                if (line.startsWith("#genome,")){
                    startReading = true;
                    continue;
                }
                if (startReading) {
                    String[] lineArray = line.split(", ");
                    String[] addressArray = lineArray[4].split(" ");
                    mrnaSequenceIdMap.put(lineArray[2], addressArray[0] + "_" + addressArray[1]);
                }
            }
        } catch (IOException e) {
            Pantools.logger.error(e.getMessage());
            System.exit(1);
        }
        return mrnaSequenceIdMap;
    }

    public static HashMap<String, int[]> mrnaAddressSequence(Node[] mrnaNodes) {
        HashMap<String, int[]> mrnaAdddressMap = new HashMap<>(); // key is mrna identifier, value is [ start, stop position]
        for (Node mrnaNode : mrnaNodes){
            int[] address = (int[]) mrnaNode.getProperty("address");
            String id = (String) mrnaNode.getProperty("protein_ID");
            int[] startStop = new int[]{address[2], address[3]};
            //System.out.println(id);
            mrnaAdddressMap.put(id, startStop);
        }
        return mrnaAdddressMap;
    }

    /**
     * File must be always one line
     * @param file file location
     * @return string with skipped genomes
     */
    public static String readOneLineFile(String file, boolean stopWhenMissing) {
        String skippedGenomes = "";
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            while (in.ready()) {
                skippedGenomes = in.readLine().trim();
            }
        } catch (IOException e) {
            if (stopWhenMissing) {
                Pantools.logger.error("Unable to read: {}", file);
                System.exit(1);
            }
        }
        return skippedGenomes;
    }

    /**
     * File must be always one line
     * @param fileIn file location
     * @return last (and first) line of the input file
     */
    public static String readOneLineFile(Path fileIn) {
        int lineCounter = 0;
        String finalLine = "";
        try {
            BufferedReader br = Files.newBufferedReader(fileIn);
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                finalLine = line;
                lineCounter++;
            }
        } catch (IOException e) {
            Pantools.logger.error("Unable to read: {}", fileIn);
            System.exit(1);
        }
        if (lineCounter > 1) {
            Pantools.logger.info("{} contained not 1 but {} lines.", fileIn, fileIn);
        }
        return finalLine;
    }

    public HashMap<Node, String> read_core_accessory_unique_groups(boolean all_required) {
        String originalGroups = SELECTED_HMGROUPS;
        HashMap<Node, String> hmgroup_class_map = new HashMap<>();
        boolean exists1 = checkIfFileExists(WORKING_DIRECTORY + "gene_classification/group_identifiers/core_groups.csv");
        boolean exists2 = checkIfFileExists(WORKING_DIRECTORY + "gene_classification/group_identifiers/accessory_groups.csv");
        boolean exists3 = checkIfFileExists(WORKING_DIRECTORY + "gene_classification/group_identifiers/unique_groups.csv");
        if ((!exists1 || !exists2 || !exists3) && all_required) {
            Pantools.logger.error("This analysis requires the gene content to be classified with gene_classification.");
            System.exit(1);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            if (exists1) {
                SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/group_identifiers/core_groups.csv";
                ArrayList<Node> hmNodeList = findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1);
                for (Node hm_node : hmNodeList) {
                    hmgroup_class_map.put(hm_node, "core");
                }
            }
            if (exists2) {
                SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/group_identifiers/accessory_groups.csv";
                ArrayList<Node> hmNodeList = findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1);
                for (Node hm_node : hmNodeList) {
                    hmgroup_class_map.put(hm_node, "accessory");
                }
            }
            if (exists3) {
                SELECTED_HMGROUPS = WORKING_DIRECTORY + "gene_classification/group_identifiers/unique_groups.csv";
                ArrayList<Node> hmNodeList = findHmNodes(parseHmFile(Paths.get(SELECTED_HMGROUPS)), 1);
                for (Node hm_node : hmNodeList) {
                    hmgroup_class_map.put(hm_node, "unique");
                }
            }
            tx.success();
        } catch (IOException ioe) {
            Pantools.logger.error("Unable to read homology groups file: {}", SELECTED_HMGROUPS);
            throw new RuntimeException(ioe);
        }
        SELECTED_HMGROUPS = originalGroups;
        return hmgroup_class_map;
    }

//BLOCK HAS BEEN COMMENTED OUT BECAUSE 1) IT IS NOT IN USE, AND 2) IT USES IS_SIMILAR_TO RELATIONSHIPS IN A WAY THAT IS NOT ALLOWED
//    /**
//     * Check which genes are homologous. Genes must be in same homology group and have 'is_similar_to' relationship
//     *
//     *  Types of keys for homolog_counts_per_seq_and_combi
//     *  1. genome number
//     *  2. genome 1 number + "#" genome 2 number + "#" + number of genome 1 or 2
//     *  3. sequence identifier
//     *  4. sequence 1 identifier + "#" sequence 2 identifier + "#" + identifier of sequence 1 or 2
//     * @return hashmap
//     */
//    public static HashMap<String, Integer> gather_homologs_between_sequences() {
//        HashMap<String, HashSet<Node>> homologs_per_seq_and_combi = new HashMap<>();
//        HashSet<String> seq_combinations = new HashSet<>();
//        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
//            ResourceIterator<Node> homology_groups = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
//            int groupCounter = 0;
//            int totalGroups = (int) count_nodes(HOMOLOGY_GROUP_LABEL);
//            while (homology_groups.hasNext()) {
//                groupCounter++;
//                Node hm_node = homology_groups.next();
//                if (groupCounter % 1000 == 0 || groupCounter < 11) {
//                    System.out.print("\rGathering homology groups: " + groupCounter + "/" + totalGroups + "               ");
//                }
//                Iterable<Relationship> relationships = hm_node.getRelationships(RelTypes.has_homolog);
//                for (Relationship rel : relationships) {
//                    Node mrnaNode = rel.getEndNode();
//                    int[] address = (int[]) mrnaNode.getProperty("address");
//                    int genomeNr1 = address[0];
//                    int sequenceNr1 = address[1];
//                    String sequenceId1 = genomeNr1 + "_" + sequenceNr1;
//                    if (skip_array[genomeNr1 - 1] || skip_seq_array[genomeNr1 - 1][sequenceNr1 - 1]) {
//                        continue;
//                    }
//
//                    Iterable<Relationship> is_similar_relations = mrnaNode.getRelationships(RelTypes.is_similar_to); //TODO: SHOULD NOT USE IS_SIMILAR_TO
//                    boolean gene_has_homolog = false;
//                    for (Relationship rel2 : is_similar_relations) {
//                        Node mrna_node2 = rel2.getEndNode();
//                        if (mrna_node2.getId() == mrnaNode.getId()) {
//                            mrna_node2 = rel2.getStartNode();
//                        }
//                        int[] address2 = (int[]) mrna_node2.getProperty("address");
//                        int genomeNr2 = address2[0];
//                        int sequenceNr2 = address2[1];
//                        String sequenceId2 = genomeNr2 + "_" + sequenceNr2;
//
//                        if (genomeNr1 == genomeNr2 && sequenceNr1 == sequenceNr2) { // same sequence
//                            continue;
//                        }
//                        if (skip_seq_array[genomeNr2 - 1][sequenceNr2 - 1] || skip_array[genomeNr2 - 1] ) {
//                            continue;
//                        }
//
//                        Relationship hm_rel = mrna_node2.getSingleRelationship(RelTypes.has_homolog, Direction.INCOMING);
//                        Node hm_node2 = hm_rel.getStartNode();
//                        if (hm_node2.getId() != hm_node.getId()) { // proteins are not part of the same homology group
//                            continue;
//                        }
//                        gene_has_homolog = true;
//                        if (genomeNr2 < genomeNr1 || sequenceNr2 < sequenceNr1) {
//                            continue; // the mrna node was already added
//                        }
//                        homologs_per_seq_and_combi.computeIfAbsent(sequenceId1 + "#" + sequenceId2 + "#" + sequenceId1, k -> new HashSet<>()).add(mrnaNode);
//                        homologs_per_seq_and_combi.computeIfAbsent(sequenceId1 + "#" + sequenceId2 + "#" + sequenceId2, k -> new HashSet<>()).add(mrna_node2);
//                        seq_combinations.add(sequenceId1 + "#" + sequenceId2);
//                    }
//                    if (gene_has_homolog) {
//                        homologs_per_seq_and_combi.computeIfAbsent(genomeNr1 + "_" + sequenceNr1, k -> new HashSet<>()).add(mrnaNode);
//                    }
//                }
//            }
//            tx.success();
//        }
//
//        // Use the size of the hashset per sequence (and combination) and store as integer
//        HashMap<String, Integer> homolog_counts_per_seq_and_combi = new HashMap<>();
//       // System.out.println("HOMOLOGS per seq");
//        for (String seq_id : homologs_per_seq_and_combi.keySet()) {
//            if (seq_id.contains("3_1844#3_1845")){
//                System.out.println("zie je ");
//            }
//            if (seq_id.contains("#")) {
//                continue;
//            }
//            HashSet<Node> nodes_of_seq = homologs_per_seq_and_combi.get(seq_id);
//            //System.out.println(seq_id + " " + nodes_of_seq.size());
//            String[] seq_array = seq_id.split("_");
//
//            homolog_counts_per_seq_and_combi.merge(seq_array[0], nodes_of_seq.size(), Integer::sum);
//            homolog_counts_per_seq_and_combi.merge(seq_id, nodes_of_seq.size(), Integer::sum);
//        }
//
//        for (String seq_combination : seq_combinations) {
//            if (seq_combination.contains("3_1844#3_1845")){
//                Pantools.logger.debug("zie je 2");
//            }
//            String[] seq_combi_array = seq_combination.split("#"); // example is 1_1#2_1
//            String[] seq1_array = seq_combi_array[0].split("_"); // example is 1_1
//            String[] seq2_array = seq_combi_array[1].split("_"); // example is 2_1
//            String key1 = seq_combination + "#" + seq_combi_array[0];
//            String key2 = seq_combination + "#" + seq_combi_array[1];
//
//            String genome_key1 = seq1_array[0] + "#" + seq2_array[0] + "#" + seq1_array[0];
//            String genome_key2 = seq1_array[0] + "#" + seq2_array[0] + "#" + seq2_array[0];
//            //System.out.println(seq_combination + " " + homologs_per_seq_and_combi.get(key1).size() + " " + homologs_per_seq_and_combi.get(key2).size());
//            homolog_counts_per_seq_and_combi.put(seq_combination + "#" + seq_combi_array[0], homologs_per_seq_and_combi.get(key1).size());
//            homolog_counts_per_seq_and_combi.put(seq_combination + "#" + seq_combi_array[1], homologs_per_seq_and_combi.get(key2).size());
//            homolog_counts_per_seq_and_combi.merge(genome_key1, homologs_per_seq_and_combi.get(key1).size(), Integer::sum);
//            homolog_counts_per_seq_and_combi.merge(genome_key2, homologs_per_seq_and_combi.get(key2).size(), Integer::sum);
//        }
//
//        //System.out.println("HOMOLOG counts");
//        //for (String key : homolog_counts_per_seq_and_combi.keySet()) {
//        //    System.out.println(" " + key + " " + homolog_counts_per_seq_and_combi.get(key));
//        //}
//
//        return homolog_counts_per_seq_and_combi;
//    }

    /**
     * assumes the longest transcripts are already known
     * @return
     */
    public static HashMap<String, int[]> retrieve_gene_addresses(boolean renameforMCscanx) {
        int gene_counter = 0;
        int[] genes_per_genome = new int[total_genomes];
        int[][] genes_per_sequence = new int[total_genomes][0];
        HashMap<String, int[]> mrna_address_map = new HashMap<>();
        for (String identifier : annotation_identifiers) {
            if (identifier.endsWith("_0")) {
                continue;
            }
            String[] id_array = identifier.split("_");
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                int genomeNr = Integer.parseInt(id_array[0]);
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                genes_per_sequence[genomeNr-1] = new int[num_sequences];
                ResourceIterator<Node> mrna_nodes;
                mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "annotation_id", identifier);
                while (mrna_nodes.hasNext()) {
                    Node mrna_node = mrna_nodes.next();
                    if (!mrna_node.hasProperty("protein_ID")) {
                        continue;
                    }
                    if (longest_transcripts && !mrna_node.hasProperty("longest_transcript")) {
                        continue;
                    }
                    gene_counter++;
                    if (gene_counter % 10000 == 0 || gene_counter == 1) {
                        System.out.print("\rGathering genes: Genome " + id_array[0] + ". Total gene count : " + gene_counter);
                    }
                    String protein_id = (String) mrna_node.getProperty("protein_ID");
                    protein_id = protein_id.replace("-","_"); // - causes mcscanx or one of the visulisation tools to crash
                    if (renameforMCscanx){
                        protein_id = protein_id.replace("|","_").replace(":","").replace("-","_");
                    }
                    int[] address = (int[]) mrna_node.getProperty("address");
                    mrna_address_map.put(genomeNr + "_" + address[1] + "#" + protein_id, address);
                    genes_per_genome[genomeNr-1]++;
                    genes_per_sequence[genomeNr-1][address[1]-1]++;
                }
                tx.success();
            }
        }
        System.out.print("\r                                                        "); // spaces are intentional
        return mrna_address_map;
    }

     /**
     * assumes the longest transcripts are already known
     * @return
     */
    public static HashMap<String, Node> retrieve_mrna_nodes(boolean changeIdentifiers) {
        int gene_counter = 0;
        HashMap<String, Node> mrna_node_map = new HashMap<>();
        for (String identifier : annotation_identifiers) {
            if (identifier.endsWith("_0")) {
                continue;
            }
            String[] id_array = identifier.split("_");
            try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
                int genomeNr = Integer.parseInt(id_array[0]);
                if (skip_array[genomeNr-1]) {
                    continue;
                }
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", genomeNr);
                //int num_sequences = (int) genome_node.getProperty("num_sequences");
                //genes_per_sequence[genomeNr-1] = new int[num_sequences];
                ResourceIterator<Node> mrna_nodes;
                mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "annotation_id", identifier);
                while (mrna_nodes.hasNext()) {
                    Node mrna_node = mrna_nodes.next();
                    if (!mrna_node.hasProperty("protein_ID")) {
                        continue;
                    }
                    if (longest_transcripts && !mrna_node.hasProperty("longest_transcript")) {
                        continue;
                    }
                    gene_counter++;
                    if (gene_counter % 10000 == 0) {
                        System.out.print("\rGathering genes: Genome " + id_array[0] + ". Total gene count: " + gene_counter);
                    }
                    String proteinId = (String) mrna_node.getProperty("protein_ID");
                    if (changeIdentifiers) {
                        proteinId = proteinId.replace(":","").replace("-","_").replace("|","_");
                    }
                    int[] address = (int[]) mrna_node.getProperty("address");
                    mrna_node_map.put(genomeNr + "_" + address[1] + "#" + proteinId, mrna_node);
                }
                tx.success();
            }
        }
        System.out.print("\r                                                        "); // spaces are intentional
        //System.out.println("genes per genomes " + Arrays.toString(genes_per_genome));
        //System.out.println("Genes per sequence, genome 1 " + Arrays.toString(genes_per_sequence[0]));
        //System.out.println("Genes per sequence, genome 2 " + Arrays.toString(genes_per_sequence[1]));
        return mrna_node_map;
    }

    /**
     * Retrieve 'homology_group' nodes. Stop if no groups are present.
     * @return homologyNodes
     */
    public static ArrayList<Node> retrieveHomologyNodes() {
        ArrayList<Node> homologyNodes = new ArrayList<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> homologyGroupNodes = GRAPH_DB.findNodes(HOMOLOGY_GROUP_LABEL);
            int counter = 0;
            while (homologyGroupNodes.hasNext()) {
                counter++;
                Node homologyNode = homologyGroupNodes.next();
                homologyNodes.add(homologyNode);
                if (counter % 10000 == 0 || counter < 11) {
                    System.out.print("\rGathering homology groups: " + counter);
                }
            }
            if (homologyNodes.size() < 2) {
                throw new RuntimeException("Requires activate homology grouping");
            }
            System.out.print("\r                                    "); // spaces are intentional
            tx.success();
        }
        return homologyNodes;
    }

    /**
     * @return
     */
    public ArrayList<String> obtainPhasingSeqIdentifiers() {
        createDirectory("proteins/per_phase/", true);
        ArrayList<String> phasedIdentifiers = new ArrayList<>();
        if (phasingInfoMap == null) {
            return phasedIdentifiers;
        }

        // start the file writers for phasing counts
        for (String seqId : selectedSequences) {
            String[] seqIdArray = seqId.split("_"); // 1_1 becomes [1,1]
            if (!phasingInfoMap.containsKey(seqId)) {
                if (!phasedIdentifiers.contains(seqIdArray[0] + "_unphased")) {
                    phasedIdentifiers.add(seqIdArray[0] + "_unphased");
                }
                continue;
            }
            String[] phasingInfo = phasingInfoMap.get(seqId); // example [1, D, 1D, 1_D]
            String[] splitId = phasingInfo[3].split("_"); // 1_D becomes [1,D]
            if (!phasedIdentifiers.contains(seqIdArray[0] + "_" + splitId[1])) {
                phasedIdentifiers.add(seqIdArray[0] + "_" + splitId[1]);
            }
        }
        return phasedIdentifiers;
    }

    /**
     * Order the sequences per chromosome and haplotype phase (a/b/c/d) 1A -> 1B -> 2A
     * @param originalSequenceIds unordered list with sequence identifiers (1_1, 1_2). should belong to one genome
     */
    public ArrayList<String> sequencesInChromosomePhasingOrder(ArrayList<String> originalSequenceIds) {
        ArrayList<String> newSequenceIds = new ArrayList<>();
        HashMap<String, ArrayList<String>> sequencesPerChromosome = new HashMap<>();
        for (String sequenceId : originalSequenceIds) {
            String[] phasing_info = phasingInfoMap.get(sequenceId); // [1, B, 1B, 1_B]
            if (phasing_info == null) {
                sequencesPerChromosome.computeIfAbsent( "unphased", k -> new ArrayList<>()).add(sequenceId);
            } else {
                sequencesPerChromosome.computeIfAbsent(phasing_info[0], k -> new ArrayList<>()).add(sequenceId);
            }
        }

        for (int i = 1; i < 10000; i++) { // i is a chromosome number
            if (!sequencesPerChromosome.containsKey(i + "")) {
                continue;
            }
            ArrayList<String> sequenceIdentifiers = sequencesPerChromosome.get(i + "");
            String[] orderedSequenceIds = new String[8]; // first in array because sometimes not all sequences of a chromosome or present
            if (sequenceIdentifiers.size() > 8) {
                Pantools.logger.error("Ploidy up to 8 allowed.");
                System.exit(1);
            }
            for (String seq_id : sequenceIdentifiers) {
                String[] phasing_info = phasingInfoMap.get(seq_id);  // [1, B, 1B, 1_B]
                if (phasing_info[1].equals("A")) {
                    orderedSequenceIds[0] = seq_id;
                } else if (phasing_info[1].equals("B")) {
                    orderedSequenceIds[1] = seq_id;
                } else if (phasing_info[1].equals("C")) {
                    orderedSequenceIds[2] = seq_id;
                } else if (phasing_info[1].equals("D")) {
                    orderedSequenceIds[3] = seq_id;
                } else if (phasing_info[1].equals("E")) {
                    orderedSequenceIds[4] = seq_id;
                } else if (phasing_info[1].equals("F")) {
                    orderedSequenceIds[5] = seq_id;
                }  else if (phasing_info[1].equals("G")) {
                    orderedSequenceIds[6] = seq_id;
                }  else if (phasing_info[1].equals("H")) {
                    orderedSequenceIds[7] = seq_id;
                }  else if (phasing_info[1].equals("unphased")) { // unphased, only 1 sequence per chromosome
                    orderedSequenceIds[0] = seq_id;
                }
            }
            String[] finalOrderedSequenceIds = new String[sequenceIdentifiers.size()];
            int counter = 0;
            for (String sequenceId : orderedSequenceIds){
                if (sequenceId == null) {
                    continue;
                }
                finalOrderedSequenceIds[counter] = sequenceId;
                counter++;
            }
            newSequenceIds.addAll(Arrays.asList(finalOrderedSequenceIds));
        }

        if (sequencesPerChromosome.containsKey("unphased")) {
            //TO DO, order remaining sequence by genome and sequence numbers?
            ArrayList<String> sequenceIdentifiers = sequencesPerChromosome.get("unphased");
            newSequenceIds.addAll(sequenceIdentifiers);
        }


        if (newSequenceIds.size() != originalSequenceIds.size()) {
           //System.out.println(newSequenceIds.size() + " " +  originalSequenceIds.size() + " " + newSequenceIds + " " + originalSequenceIds);
           throw new RuntimeException("Something went wrong on the development side here. 832343897");
        }
        return newSequenceIds;
    }


    /**
     * Fills phasingInfoMap. key is sequence identifier, value is array with four annotations:
     * example [1, B, 1B, 1_B]
     * - chromosome 1
     * - haplotype B
     * - haplotype identifier (without underscore)
     * - haplotype identifier
     *
     * example [3, unphased, 3unphased, 3_unphased]
     * - chromosome 3 but an unphased sequence
     *
     * @param print Use print statements
     * @param selectedSequences sequence identifiers in the current analysis
     * @return number of genomes with phasing information
    */
    public int preparePhasedGenomeInformation(boolean print, LinkedHashSet<String> selectedSequences) {
        phasingInfoMap = new HashMap<>(); // genome number and sequence identifiers as key
        int counter = 0;
        HashSet<Integer> genomesWithPhasing = new HashSet<>();
        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            ResourceIterator<Node> sequenceNodes = GRAPH_DB.findNodes(SEQUENCE_LABEL);
            long sequenceNodeCount = count_nodes(SEQUENCE_LABEL);
            while (sequenceNodes.hasNext()) {
                counter ++;
                Node sequenceNode = sequenceNodes.next();
                if (counter % 1000 == 0 || counter == sequenceNodeCount || counter < 11) {  //TODO replace with progress bar
                    System.out.print("\r Retrieving phasing information: " + counter + "/" + sequenceNodeCount);
                }
                if (!sequenceNode.hasProperty("phasing_assigned")) {
                    continue;
                }
                String sequenceId = (String) sequenceNode.getProperty("identifier");
                int genomeNr = (int) sequenceNode.getProperty("genome");
                int sequenceNr = (int) sequenceNode.getProperty("number");
                if (selectedSequences != null && !selectedSequences.contains(genomeNr + "_" + sequenceNr)) { // sequence was skipped via --selection-file argument
                    continue;
                }

                if (!sequenceNode.hasProperty("phasing_ID")) {
                    continue;
                }
                int chromosomeNr = (int) sequenceNode.getProperty("phasing_chromosome");
                String phasingId = (String) sequenceNode.getProperty("phasing_ID"); // consists of chromosome number + "_" + phase
                String[] phasing_array = phasingId.split("_"); // 1_A becomes [1,A], 2_unphased [2,unphased]
                String[] phasing_info = new String[4]; //[chromosome number, phasing letter, combination of the two, combination of the two with '_' ]
                phasing_info[0] = chromosomeNr + "";
                phasing_info[3] = phasingId;
                if (chromosomeNr != 0) {
                    genomesWithPhasing.add(genomeNr);
                    String underscore = "_";
                    if (!phasing_array[1].equals("unphased")) {
                        underscore = "";
                    }
                    phasing_info[1] = phasing_array[1];
                    phasing_info[2] = chromosomeNr + underscore + phasing_array[1];
                }  else { // chromosome 0
                    phasing_info[1] = "unphased";
                    phasing_info[2] = "0_unphased";
                }
                phasingInfoMap.put(sequenceId, phasing_info); // [1, B, 1B, 1_B]
            }
            tx.success();
        }
        System.out.println(""); // to print nicely on next line. TODO remove with progress bar

        if (print) {
            if (genomesWithPhasing.size() == 1) {
                Pantools.logger.info("One genome with chromosome/phasing information.");
            } else if (genomesWithPhasing.size() > 1) {
                Pantools.logger.info(genomesWithPhasing.size() + " genomes with chromosome/phasing information.");
            } else {
                Pantools.logger.info("No phased genomes are present.");
            }
        }
        return genomesWithPhasing.size();
    }

    /**
     *
     * @param include_non_coding_mrnas
     * @param allow_same_start
     * @return
     */
    public HashMap<String, Node[]> getOrderedMrnaNodesPerSequence(boolean include_non_coding_mrnas, boolean allow_same_start,
                                                                   ArrayList<String> sequenceSelection) {

        HashMap<String, Node[]> mrnaNodesPerSequence = new HashMap<>();
        try (Transaction tx = GRAPH_DB.beginTx()) {
            for (int i = 1; i <= total_genomes; i++) { // i is a genome number
                if (skip_array[i-1]) {
                    continue;
                }
                int total_mrna_counter = 0;
                Node genome_node = GRAPH_DB.findNode(GENOME_LABEL, "number", i);
                int num_sequences = (int) genome_node.getProperty("num_sequences");
                for (int j = 1; j <= num_sequences; j++) {
                    if (sequenceSelection != null && !sequenceSelection.contains(i + "_" + j)) {
                        continue;
                    }
                    if (skip_seq_array != null && skip_seq_array[i-1][j-1]) {
                        continue;
                    }
                    TreeSet<Integer> gene_start_positions = new TreeSet<>(); // automatically ordered, contains starting locations of genes
                    HashMap<Integer, ArrayList<Node>> mrna_nodes_per_start_position = new HashMap<>();
                    ResourceIterator<Node> mrna_nodes = GRAPH_DB.findNodes(MRNA_LABEL, "genome", i, "sequence", j);
                    while (mrna_nodes.hasNext()) {
                        Node mrna_node = mrna_nodes.next();
                        if (longest_transcripts && !mrna_node.hasProperty("longest_transcript")) {
                            continue;
                        }
                        if (!include_non_coding_mrnas && !mrna_node.hasProperty("protein_ID")) {
                            continue;
                        }
                        total_mrna_counter ++;
                        if (total_mrna_counter % 10000 == 0 || total_mrna_counter == 1) {
                            System.out.print("\rFinding correct order of mRNAs: "
                                    + "Genome " + i + " sequence " + j + ". " + total_mrna_counter + " mRNAs      "); // spaces are intentional
                        }
                        int[] address = (int[]) mrna_node.getProperty("address");
                        gene_start_positions.add(address[2]);
                        mrna_nodes_per_start_position.computeIfAbsent(address[2], k -> new ArrayList<>()).add(mrna_node); // used to find genes with the same start position
                    }
                    if (gene_start_positions.isEmpty()) {
                        continue;
                    }
                    Node[] mrnas_ordered = put_ordered_genes_in_array(gene_start_positions, mrna_nodes_per_start_position, allow_same_start); // order the genes by start position
                    mrnaNodesPerSequence.put(i +"_" + j, mrnas_ordered);
                }
            }
            tx.success();
        }
        return mrnaNodesPerSequence;
    }

    /**
     *
     * @param gene_start_positions
     * @param mrna_nodes_per_start_position
     * @param allow_same_start
     * @return
     */
    private Node[] put_ordered_genes_in_array(TreeSet<Integer> gene_start_positions,
                                              HashMap<Integer, ArrayList<Node>> mrna_nodes_per_start_position, boolean allow_same_start) {

        ArrayList<Node> ordered_mrnas = new ArrayList<>();
        for (int gene_start : gene_start_positions) {
            ArrayList<Node> mrna_nodes_list = mrna_nodes_per_start_position.get(gene_start);
            if (mrna_nodes_list.size() > 1) {
               if (allow_same_start) { // add all mrna's but order them from shortest to largest
                    TreeSet<Integer> lengths = new TreeSet<>();
                    HashMap<Integer, ArrayList<Node>> nodes_per_length = new HashMap<>();
                    for (Node mrna_node : mrna_nodes_list) {
                        int length = (int) mrna_node.getProperty("length");
                        lengths.add(length);
                        nodes_per_length.computeIfAbsent(length, k -> new ArrayList<>()).add(mrna_node);
                    }
                    for (int length : lengths) {
                        ArrayList<Node> nodes = nodes_per_length.get(length);
                        ordered_mrnas.addAll(nodes);
                    }
               } else { // only add the largest
                   int highest = 0;
                   Node mrna_of_highest = GRAPH_DB.getNodeById(0);
                    for (Node mrna_node : mrna_nodes_list) {
                        int length = (int) mrna_node.getProperty("length");
                        if (highest < length) {
                            mrna_of_highest = mrna_node;
                        }
                    }
                    ordered_mrnas.add(mrna_of_highest);
               }
            } else {
                ordered_mrnas.add(mrna_nodes_list.get(0));
            }
        }
        return ordered_mrnas.toArray(new Node[ordered_mrnas.size()]); // list_to_array list to array ;
    }

    /**
     * Check the --genome, --sequence and --mode phasing arguments
     */
    public void checkGenomeSequenceArguments() {
        if (!compare_genomes && !compare_sequences) {
            Pantools.logger.error("Please include --genome or --sequence");
            System.exit(1);
        } else if (compare_genomes && compare_sequences) {
            Pantools.logger.error("Please include --genome or --sequence. Not both.");
            System.exit(1);
        } else if (compare_genomes) {
            System.out.println("\r--genome was selected");
            if (PHASED_ANALYSIS) {
                Pantools.logger.error("--phasing does not work on a table with values for genomes.");
                System.exit(1);
            }
        } else { // compare_sequences is true
            if (PROTEOME) {
                Pantools.logger.error("The --sequence argument cannot be used in combination with a panproteome.");
                System.exit(1);
            }
            //System.out.println("\r--sequence was selected");
            if (PHASED_ANALYSIS) {
                System.out.println("\r--phasing was included. Adding phasing information");
            }
        }
    }

    /**
     * should replace rename_matrix function
     */
    public void rename_matrix2() {
        System.out.println("\nRename a CSV formatted matrix file\n");
        if (INPUT_FILE == null) {
            Pantools.logger.error("No table was provided via --input-file/-if");
            System.exit(1);
        } else if (!INPUT_FILE.contains(".")) {
            Pantools.logger.error("The file provided through --input-file/-if must have a file extension, .csv for example.");
            System.exit(1);
        }

        try (Transaction tx = GRAPH_DB.beginTx()) { // start database transaction
            skip.create_skip_arrays(false, true); // create skip array if -skip/-ref is provided by user

            if (PHENOTYPE == null) {
                System.out.println("\rNo --phenotype provided! Placing only genome numbers in the headers (first row/column)");
            } else {
                System.out.println("\rSelected '" + PHENOTYPE + "' as phenotype. Using this to rename the headers (first row/column)");
                retrieve_phenotypes(); // Creates and fills geno_pheno_map and phenotype_map when a phenotype was provided by the user
            }
            tx.success(); // transaction successful, commit changes
        } catch (NotFoundException nee) {
            Pantools.logger.error("Unable to retrieve data from the pangenome.");
            System.exit(1);
        }
        skip.retrieveSelectedGenomes();
        skip.retrieveSelectedSequences(0, false);

        checkGenomeSequenceArguments();

        preparePhasedGenomeInformation(PHASED_ANALYSIS, selectedSequences);

        StringBuilder output_builder = new StringBuilder(); // new matrix file
        int matrix_width = 0, line_counter = 0;
        ArrayList<Integer> columns_to_skip = new ArrayList<>(); // contains column numbers
        try (BufferedReader in = new BufferedReader(new FileReader(INPUT_FILE))) {
            while (in.ready()) {
                String line = in.readLine().trim();
                line_counter ++;
                if (line_counter % 10000 == 0) {
                    System.out.print("\rRenaming matrix: line " + line_counter);
                }

                // check if every line has the same number of columns
                String[] line_array = line.trim().split(",");
                if (line_array.length != matrix_width && matrix_width > 0) {
                    Pantools.logger.error("Not all rows have the same number of columns.");
                    System.exit(1);
                }
                matrix_width = line_array.length;

                if (line_counter == 1) { // first row. header with genome numbers/sequence id + phenotype
                    StringBuilder header_builder = new StringBuilder(line_array[0] + ",");
                    for (int i = 1; i < line_array.length; i++) {
                        String[] cell_array = line_array[i].split(" "); // value of leftmost cell
                        String seqId_or_genomeNr = cell_array[0];
                        int genome_nr = matrix_validate_genome_nr_in_cell(seqId_or_genomeNr);
                        String new_value;
                        if (compare_sequences) {
                            if (!selectedSequences.contains(seqId_or_genomeNr)) { // skip row completely
                                columns_to_skip.add(i);
                                continue;
                            }
                        } else { // genomes
                            if (skip_array[genome_nr-1]) {
                                columns_to_skip.add(i);
                                continue; // skip row completely
                            }

                        }
                        new_value = obtain_value_for_matrix(seqId_or_genomeNr);
                        header_builder.append(new_value).append(",");
                    }
                    String header = header_builder.toString().replaceFirst(".$","");
                    output_builder.append(header).append("\n");
                } else {
                    StringBuilder row_builder = new StringBuilder();
                    String[] cell_array = line_array[0].split(" "); // value of leftmost cell
                    String seqId_or_genomeNr = cell_array[0];
                    int genome_nr = matrix_validate_genome_nr_in_cell(seqId_or_genomeNr);
                    String new_value;
                    if (compare_sequences) {
                        if (!selectedSequences.contains(seqId_or_genomeNr)) { // skip row completely
                            continue;
                        }
                    } else {
                        if (skip_array[genome_nr-1]) {
                            continue; // skip row completely
                        }
                    }
                    new_value = obtain_value_for_matrix(seqId_or_genomeNr);
                    row_builder.append(new_value).append(",");
                    for (int i = 1; i < line_array.length; i++) {
                        if (columns_to_skip.contains(i)) { // skip a column
                            continue;
                        }
                        row_builder.append(line_array[i]).append(",");
                    }
                    String row = row_builder.toString().replaceFirst(".$",""); // remove last character
                    output_builder.append(row).append("\n");
                }
            }
        } catch (IOException ioe) {
            Pantools.logger.error("Failed to read: {}", INPUT_FILE);
            System.exit(1);
        }
        String output_file = addStringBeforeFinalDot(INPUT_FILE, "_RENAMED");
        write_SB_to_file_full_path(output_builder, output_file);
        Pantools.logger.info("New matrix written to {}", output_file);
    }

    /**
     * Check if the value in the cell was generated by a PanTools function
     * @param seqId_or_genomeNr example is 1_1 or 1
     * @return a genome number
     */
    public static int matrix_validate_genome_nr_in_cell(String seqId_or_genomeNr) {
        String[] seq_array = seqId_or_genomeNr.split("_"); // example is 1_1 or 1
        if (compare_sequences && seq_array.length != 2) {
            Pantools.logger.error("{} is not a valid sequence", seqId_or_genomeNr);
            System.exit(1);
        } else if (compare_genomes && seq_array.length != 1) {
            Pantools.logger.error("{} is not a valid genome number", seqId_or_genomeNr);
            System.exit(1);
        }
        int genome_nr = 0;
        try {
            genome_nr = Integer.parseInt(seq_array[0]);
        } catch (NumberFormatException nfe) {
            Pantools.logger.error("Phenotype information can only be included when the header of a cell only " +
                    "consists of a value or the cell starts with the genome number.");
            System.exit(1);
        }
        return genome_nr;
    }

    /**
     * Add or remove sequence id/ genome number phenotype and phasing
     * @param  seqId_or_genomeNr example is 1_1 or 1
     * @return Returns a new value for the cell
     */
    public static String obtain_value_for_matrix(String seqId_or_genomeNr) {
        String[] seq_array = seqId_or_genomeNr.split("_");
        String phasing_id = "";
        if (PHASED_ANALYSIS && compare_sequences) {
            String[] phasingInfo = phasingInfoMap.get(seqId_or_genomeNr);  // [1, B, 1B, 1_B]
            if (phasingInfo == null) {
                phasing_id = "unphased";
            } else {
                phasing_id = phasingInfo[3];
            }
        }

        int genome_nr = Integer.parseInt(seq_array[0]);
        if (PHENOTYPE != null) { // a --phenotype was included
            if ((compare_sequences && selectedSequences.contains(seqId_or_genomeNr)) ||
                    (compare_genomes && !skip_array[genome_nr])) {
                String phenotype = geno_pheno_map.get(genome_nr);
                if (phenotype.equals("?")) {
                    phenotype = "Unknown";
                }

                if (Mode.contains("NO-NUMBERS")) { // remove the genome number/sequence id
                    if (!phasing_id.equals("") && !phenotype.equals("")) {
                        return phasing_id + " " + phenotype;
                    } else {
                        return phasing_id + phenotype;
                    }
                } else {
                    if (!phasing_id.equals("") && !phenotype.equals("")) {
                        return seqId_or_genomeNr + " " + phasing_id + " " + phenotype;
                    } else if (!phasing_id.equals("") || !phenotype.equals("")) {
                        return seqId_or_genomeNr + " " + phasing_id + phenotype;
                    } // both phasing and pheno are empty
                    return seqId_or_genomeNr + phasing_id + phenotype;
                }
            }
        } else { // no --phenotype given, so remove the last value/string of value_arra
            if (!phasing_id.equals("")) {
                return seqId_or_genomeNr + " " + phasing_id;
            } else {
                return seqId_or_genomeNr;
            }
        }
        return ""; // this never happens because of the if else statement above
    }
}

