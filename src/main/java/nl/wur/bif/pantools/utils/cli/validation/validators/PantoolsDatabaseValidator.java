package nl.wur.bif.pantools.utils.cli.validation.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import nl.wur.bif.pantools.utils.cli.validation.Constraints.GraphDatabase;
import nl.wur.bif.pantools.Pantools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static nl.wur.bif.pantools.utils.BeanUtils.setViolationMessage;

/**
 * Custom ConstraintValidator that validates that the database directory exists and has an existing neo4j graph database.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class PantoolsDatabaseValidator implements ConstraintValidator<GraphDatabase, Pantools> {

    @Override
    public void initialize(final GraphDatabase constraintAnnotation) {
    }

    /**
     * Validates the output directory's neo4j graph database. The database is invalid when the neo4j graph database
     * does not exist or is empty.
     *
     * @param pantools @ParentCommand annotated Pantools class
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return boolean for validity of the constraint
     */
    @Override
    public boolean isValid(Pantools pantools, ConstraintValidatorContext context) {
        final Path databaseDirectory = pantools.getDatabaseDirectory();

        if (!Files.isDirectory(databaseDirectory.resolve("databases").resolve("graph.db"))) {
            setViolationMessage(context, "Neo4j graph database does not exist");
            return false;
        }

        try (Stream<Path> entries = Files.list(databaseDirectory)) {
            setViolationMessage(context, "Neo4j graph database directory is empty");
            return entries.findFirst().isPresent();
        } catch (IOException e) {
            setViolationMessage(context, e.getMessage());
            return false;
        }
    }
}
