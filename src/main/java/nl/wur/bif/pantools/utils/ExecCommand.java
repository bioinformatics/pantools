package nl.wur.bif.pantools.utils;

import nl.wur.bif.pantools.Pantools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.io.BufferedWriter;
import java.io.FileWriter;

import static nl.wur.bif.pantools.utils.Globals.write_log;
import static nl.wur.bif.pantools.utils.Globals.WORKING_DIRECTORY;

/**
 * Original code from https://bjurr.com/runtime-exec-hangs-a-complete-solution/
 * @author edited by Eef Jonkheer, Bioinformatics group, Wageningen University, the Netherlands.
 */
public class ExecCommand {

    private static Semaphore outputSem;
    private static String output;
    private static Semaphore errorSem;
    private static String error;
    private static Process p;
    private static boolean processing_done = false;

    public static class InputWriter extends Thread {
        public String input;

        public InputWriter(String input) {
            this.input = input;
        }

        public void run() {
            PrintWriter pw = new PrintWriter(p.getOutputStream());
            pw.println(input);
            pw.flush();
         }
    }

    public static class OutputReader extends Thread {
        public OutputReader() {
            try {
                outputSem = new Semaphore(1);
                outputSem.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }   

        public void run() {
            try {
                StringBuilder readBuffer = new StringBuilder();
                BufferedReader isr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String buff = new String();
                while ((buff = isr.readLine()) != null) {
                    readBuffer.append(buff).append("\n");
                }
                output = readBuffer.toString();
                outputSem.release();
            } catch (IOException e) {
                e.printStackTrace();
            }
            processing_done = true;
        }
    }

    public static class ErrorReader extends Thread {
        public ErrorReader() {
            try {
                errorSem = new Semaphore(1);
                errorSem.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void run() {
            try {
                StringBuffer readBuffer = new StringBuffer();
                BufferedReader isr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String buff = new String();
                while ((buff = isr.readLine()) != null) {
                    readBuffer.append(buff);
                }
               
                error = readBuffer.toString();
                errorSem.release();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void ExecCommand(String command, String input) {
        try {
            p = Runtime.getRuntime().exec(makeArray(command));
            new InputWriter(input).start();
            new OutputReader().start();
            new ErrorReader().start();
            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();       
        }
    } 

    public static void ExecCommand(String command) {
        try {
            p = Runtime.getRuntime().exec(makeArray(command));
            new OutputReader().start();
            new ErrorReader().start();
            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public static String ExecCommand(String[] command) {
        Pantools.logger.debug("Executing: '{}' in directory: '{}'", String.join(" ", command), WORKING_DIRECTORY);

        processing_done = false; // is set to true when the OutputReader is completely finished 
        try {
            p = Runtime.getRuntime().exec(command);
            new OutputReader().start();
            new ErrorReader().start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        while (!processing_done) {
            try {
                Thread.sleep(500); // wait half a second
            } catch(InterruptedException ex){
                Thread.currentThread().interrupt();
            }     
        }
        if (write_log && output != null) {
            try (BufferedWriter out = new BufferedWriter(new FileWriter(WORKING_DIRECTORY + "tmp.log"))) {
                out.write(output);
            } catch (IOException ioe) {  
                Pantools.logger.error(ioe.getMessage());
                System.exit(1);
            }
        } 
        if (output == null) {
            return "";
        }
        return output;
    }
   
    public String getOutput() {
        try {
            outputSem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();        
        }
        String value = output;
        outputSem.release();
        return value;
    }

    public String getError() {
        try {
            errorSem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String value = error;
        errorSem.release();
        return value;
    }

    public static String[] makeArray(String command) {
        ArrayList<String> commandArray = new ArrayList<String>();
        String buff = "";   
        boolean lookForEnd = false;
        for (int i = 0; i < command.length(); i++) {
            if (lookForEnd) {
                if (command.charAt(i) == '\"') {
                    if (buff.length() > 0) {
                        commandArray.add(buff);
                    }
                    buff = "";
                    lookForEnd = false;
                } else {
                    buff += command.charAt(i);
                }
            } else {
                if (command.charAt(i) == '\"') {
                    lookForEnd = true;
                } else if (command.charAt(i) == ' ') {
                    if (buff.length() > 0)
                        commandArray.add(buff);
                    buff = "";  
                } else {
                    buff += command.charAt(i);
                }   
            }
        }   
        if (buff.length() > 0) {
            commandArray.add(buff);
        }
        String[] array = new String[commandArray.size()];
        for (int i = 0; i < commandArray.size(); i++) {
            array[i] = commandArray.get(i);
        }
        return array;
    }
}
