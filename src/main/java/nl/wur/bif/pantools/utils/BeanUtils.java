package nl.wur.bif.pantools.utils;

import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import nl.wur.bif.pantools.Pantools;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.ParameterException;

import java.util.Set;

/**
 * Contains functions for jakarta bean hibernate validation.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public final class BeanUtils {

    private BeanUtils() {}

    /**
     * Validate jakarta bean validation constraints for a number of bean objects and their parameters
     * @param spec CommandSpec from the pantools subcommand to throw a ParameterException
     * @param beans @Command annotated subcommand class, @ParentCommand annotated Pantools class, @Mixin or @ArgGroup
     *              annotated classes containing options that need to be validated.
     * @exception ParameterException thrown when one ore more beans are invalid
     */
    public static void argValidation(CommandSpec spec, Object ... beans) throws ParameterException {
        final Validator validator = createValidator();

        boolean isValid = true;
        for (Object bean : beans) {
            if (bean == null) continue;
            if (!isValidBean(bean, validator)) isValid = false;
        }

        if (!isValid) throw new ParameterException(spec.commandLine(), "One or more input parameters are not valid");
    }

    private static boolean isValidBean(Object bean, Validator validator) {
        final Set<ConstraintViolation<Object>> violations = validator.validate(bean);
        for (ConstraintViolation<?> violation : violations) {
            Pantools.logger.error(violation.getMessage());
        }
        return violations.size() == 0;
    }

    /**
     * Create a jakarta bean ValidatorFactory containing error messages from the ErrorMessages.properties bundle
     * @return jakarta bean Validator with custom error messages
     */
    public static Validator createValidator() {
        return Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(
                        new ResourceBundleMessageInterpolator(
                                new PlatformResourceBundleLocator("ErrorMessages")
                        )
                )
                .buildValidatorFactory()
                .getValidator();
    }

    /**
     * Disables the default constraint violation message sets a new custom message.
     * @param context ConstraintValidatorContext containing the constraint violation message
     * @param msg violation message to be added
     */
    public static void setViolationMessage(ConstraintValidatorContext context, String msg) {
        HibernateConstraintValidatorContext hibernateContext;
        hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);
        hibernateContext.disableDefaultConstraintViolation();
        hibernateContext.buildConstraintViolationWithTemplate(msg).enableExpressionLanguage().addConstraintViolation();
    }
}
