package nl.wur.bif.pantools.utils.cli.validation.validators;

import nl.wur.bif.pantools.utils.cli.validation.Constraints.MatchInteger;
import org.apache.commons.lang.ArrayUtils;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

/**
 * Custom ConstraintValidator that matches an input integer to a selection of allowed options.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class MatchIntegerValidator implements ConstraintValidator<MatchInteger, Integer> {

    private int[] value;

    @Override
    public void initialize(MatchInteger constraintAnnotation) {
        this.value = constraintAnnotation.value();
    }

    /**
     * Verifies whether the given integer is allowed
     * @param integer input integer
     * @param context ConstraintValidatorContext containing contextual data for a given constraint validator
     * @return true if the integer is within the array "value" provided in the constraint, false if not
     */
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext context) {
        return ArrayUtils.contains(value, integer);
    }
}
