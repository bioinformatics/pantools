package nl.wur.bif.pantools.utils;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * Class for console input functions using Scanner
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public final class ConsoleUtils {

    private ConsoleUtils() {}

    /**
     * Asks a yes/no question to the user via console.
     *
     * @param query the question, which can be answered by yes or no
     * @return boolean for the answer (true=yes, false=no)
     */
    public static boolean askYesOrNo(String query) {
        // give 3 attempts at giving a valid answer
        for (int i = 0; i < 4; i++) {
            System.out.printf("%s (Y|N)\n", query);
            final String yesOrNo = getConsoleInput();

            final Pattern yes = Pattern.compile("Y|YES", CASE_INSENSITIVE);
            if (yes.matcher(yesOrNo).matches())
                return true;

            final Pattern no = Pattern.compile("N|NO", CASE_INSENSITIVE);
            if (no.matcher(yesOrNo).matches())
                return false;

            if (i < 3)
                System.out.println("Invalid input, please enter 'Y' or 'N'.");
        }

        // throw exception after multiple unrecognized inputs
        throw new InputMismatchException("User input not recognized");
    }

    /**
     * Gets user input.
     *
     * @return user command line input string
     */
    private static String getConsoleInput() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
