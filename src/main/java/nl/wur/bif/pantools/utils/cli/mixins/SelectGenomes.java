package nl.wur.bif.pantools.utils.cli.mixins;

import jakarta.validation.constraints.Size;
import nl.wur.bif.pantools.utils.cli.validation.Constraints;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static nl.wur.bif.pantools.utils.StringUtils.stringToIntegerRange;
import static picocli.CommandLine.*;

/**
 * Argument group (mutually exclusive) for '--include' and '--exclude' options. Accepts lists (1,2,3), ranges (5-10)
 * or a combination (1,2,3,5-10) as user input for both options.
 *
 * @author Robin van Esch, Wageningen University, the Netherlands.
 */
public class SelectGenomes {

    @Option(names = {"-i", "--include"})
    void setInclude(String value) {
        include = makeGenomeSelectionList(value);
    }
    @Size(min = 1, message = "{size.empty.include}")
    List<Integer> include;

    @Option(names = {"-e", "--exclude"})
    void setExclude(String value) {
        exclude = makeGenomeSelectionList(value);
    }
    @Size(min = 1, message = "{size.empty.exclude}")
    List<Integer> exclude;

    @Option(names = "--selection-file")
    @Constraints.InputFile(message = "{file.selection-file}")
    Path selectionFile;

    /**
     * Initialization function for --include and --exclude genome lists.
     * @param value user input value string
     * @return integer list containing all genome numbers to include/exclude
     */
    private List<Integer> makeGenomeSelectionList(String value) {
        if (!value.matches("[0-9,-]+")) return Collections.emptyList();
        String[] listElements = value.split(",");
        return createGenomeList(listElements);
    }

    /**
     * Create a list of integers from a set of strings containing integers and ranges.
     * Returns an empty list when the user input is incorrect.
     * @param elements String for genome number ("1") or range ("1-3").
     * @return integer list of all genomes to include/exclude.
     */
    private List<Integer> createGenomeList(String[] elements) {
        List<Integer> genomeList = new ArrayList<>();
        for (String element : elements) {
            final List<Integer> genomes = parseIntOrRange(element);
            if (genomes.isEmpty()) return genomes;
            genomeList.addAll(genomes);
        }
        return genomeList;
    }

    /**
     * Turns a string into either an integer ("1" -> 1) or a range of integers ("1-3" -> 1,2,3).
     * Returns an empty list when the element does not match as a number or possible range (e.g. "3-1").
     * @param element string containing [1-9-]+
     * @return one or multiple genome numbers
     */
    private List<Integer> parseIntOrRange(String element) {
        if (element.matches("[0-9]+")) {
            return Collections.singletonList(Integer.parseInt(element));
        }
        if (element.matches("[0-9]+-[0-9]+")) {
            return stringToIntegerRange(element);
        }
        return Collections.emptyList();
    }

    public String toString() {
        return String.format("SelectGenomes{include=%s, exclude=%s, selectionFile=%s}",
                include, exclude, selectionFile);
    }

    public Path getSelectionFile() {
        return selectionFile;
    }

    public List<Integer> getInclude() {
        return include;
    }

    public List<Integer> getExclude() {
        return exclude;
    }
}
