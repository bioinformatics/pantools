package nl.wur.bif.pantools.utils;

import htsjdk.samtools.SAMReadGroupRecord;
import nl.wur.bif.pantools.construction.annotations.AnnotationLayer;
import nl.wur.bif.pantools.analysis.classification.Classification;
import nl.wur.bif.pantools.utils.cli.mixins.SelectGenomes;
import nl.wur.bif.pantools.analysis.function_analysis.FunctionalAnalysis;
import nl.wur.bif.pantools.construction.build_pangenome.GenomeLayer;
import nl.wur.bif.pantools.construction.index.IndexDatabase;
import nl.wur.bif.pantools.construction.index.IndexScanner;
import nl.wur.bif.pantools.Pantools;
import nl.wur.bif.pantools.analysis.phylogeny.Phylogeny;
import nl.wur.bif.pantools.construction.grouping.Grouping;
import nl.wur.bif.pantools.construction.sequence.SequenceDatabase;
import nl.wur.bif.pantools.construction.sequence.SequenceScanner;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.stream.Collectors;

import static nl.wur.bif.pantools.utils.Utils.create_full_path_working_directory;

/**
 * Class for global values.
 */
public final class Globals {

    private Globals() {}

    public static final Label PANGENOME_LABEL = Label.label("pangenome");
    public static final Label GENOME_LABEL = Label.label("genome");
    public static final Label SEQUENCE_LABEL = Label.label("sequence");
    public static final Label NUCLEOTIDE_LABEL = Label.label("nucleotide");
    public static final Label DEGENERATE_LABEL = Label.label("degenerate");
    public static final Label ANNOTATION_LABEL = Label.label("annotation");
    public static final Label HOMOLOGY_GROUP_LABEL = Label.label("homology_group");
    public static final Label VARIANT_LABEL = Label.label("variant");
    public static final Label FEATURE_LABEL = Label.label("feature");
    public static final Label GENE_LABEL = Label.label("gene");
    public static final Label CODING_GENE_LABEL = Label.label("coding_gene");
    public static final Label MRNA_LABEL = Label.label("mRNA");
    public static final Label TRNA_LABEL = Label.label("tRNA");
    public static final Label RRNA_LABEL = Label.label("rRNA");
    public static final Label CDS_LABEL = Label.label("CDS");
    public static final Label EXON_LABEL = Label.label("exon");
    public static final Label INTRON_LABEL = Label.label("intron");
    public static final Label COFEATURE_LABEL = Label.label("cofeature");
    public static final Label PHENOTYPE_LABEL = Label.label("phenotype");
    public static final Label ACCESSION_LABEL = Label.label("accession");
    public static final Label GO_LABEL = Label.label("GO");
    public static final Label INTERPRO_LABEL = Label.label("interpro");
    public static final Label BGC_LABEL = Label.label("bgc");
    public static final Label PFAM_LABEL = Label.label("pfam");
    public static final Label TIGRFAM_LABEL = Label.label("tigrfam");
    public static final Label INACTIVE_HOMOLOGY_GROUP_LABEL = Label.label("inactive_homology_group");
    public static final Label BUSCO_LABEL = Label.label("busco");
    public static final Label PHOBIUS_LABEL = Label.label("phobius");
    public static final Label COG_LABEL = Label.label("COG");
    public static final Label SIGNALP_LABEL = Label.label("signalp");
    public static final Label REPEAT_LABEL = Label.label("repeat");
    public static final Label SYNTENY_LABEL = Label.label("synteny");
    public static String GRAPH_DATABASE_PATH = "/databases/graph.db/";
    public static String INDEX_DATABASE_PATH = "/databases/index.db/";
    public static String GENOME_DATABASE_PATH = "/databases/genome.db/";
    public static String OUTPUT_PATH = "";

    public static GraphDatabaseService GRAPH_DB;
    public static IndexDatabase INDEX_DB;
    public static IndexScanner INDEX_SC;
    public static SequenceDatabase GENOME_DB;
    public static SequenceScanner GENOME_SC;

    public static String WORKING_DIRECTORY;
    public static String WD_full_path;
    public static String PATH_TO_THE_GENOMES_FILE;
    public static String PATH_TO_THE_PROTEOMES_FILE;
    public static String PATH_TO_THE_ANNOTATIONS_FILE;
    public static String PATH_TO_THE_REGIONS_FILE;
    public static String RAW_ABUNDANCE_FILE = "";
    public static String PATH_TO_THE_FIRST_SRA;
    public static String PATH_TO_THE_SECOND_SRA;

    public static double INTERSECTION_RATE = 0.08;
    public static double CONTRAST = 8;
    public static double MCL_INFLATION = 10.8;
    public static int MIN_NORMALIZED_SIMILARITY = 95;

    public static int K_SIZE;
    public static int MAX_ALIGNMENT_LENGTH = 2000;
    public static int GAP_OPEN = -20;
    public static int GAP_EXT = -3;
    public static int ANCHORS_DISTANCE = 10000; // The distance between two anchor nodes
    public static int MAX_TRANSACTION_SIZE = 1000; // The number of transactions to be committed in batch
    public static int cores = Runtime.getRuntime().availableProcessors();
    public static long heapSize = Runtime.getRuntime().maxMemory();
    public static boolean SHOW_KMERS;
    public static int THREADS;

    public static double MIN_IDENTITY = 0.5; // This matches the value of very senstive
    public static int NUM_KMER_SAMPLES = 15; // This matches the value of fast
    public static int MAX_NUM_LOCATIONS = 15; // This matches the value of fast
    public static int ALIGNMENT_BOUND = 5; // This value is between fast and very fast
    public static int CLIPPING_STRINGENCY = 1; // 0: no-clipping // This matches the value of fast
    // 1: low
    // 2: medium
    // 3: high
    public static int MIN_HIT_LENGTH = 13;
    public static int MAX_FRAGMENT_LENGTH = 4998;
    public static int SHOULDER = 100;
    public static int ALIGNMENT_MODE = 2; // 0: all-hits
    // -1: pangenomic unique_best
    // -2: pangenomic random_best
    // -3: pangenomic all_bests
    // 1: genomic unique_best
    // 2: genomic random_best
    // 3: genomic all_bests
    public static String OUTFORMAT = "SAM";
    public static boolean INTERLEAVED = false;
    public static boolean VERYSENSITIVE = false;

    public static SAMReadGroupRecord READ_GROUP;
    public static String PATH_TO_THE_PANGENOME_DATABASE;
    public static int total_genomes = 0; // number of genomes in pangenome
    public static int adj_total_genomes = 0; // number of genomes used in the current analysis, influenced by --skip/--reference
    public static int core_threshold = 0;
    public static int unique_threshold = 0;
    public static int phenotype_threshold = 100;

    public static String Mode = "0";
    public static String NODE_ID;
    public static Long NODE_ID_long;
    public static String current_path;
    public static String SELECTED_NAME;
    public static String target_genome;
    public static String skip_genomes;
    public static ArrayList<Integer> skip_list; // genome numbers that must be exluded from the analysis
    public static ArrayList<String> skip_seq_list; // sequence identifiers that must be exluded from the analysis
    public static boolean[] skip_array; // [genomes] is always the length total_genomes
    public static boolean[][] skip_seq_array; // [genomes][number of sequences of the genome]
    public static ArrayList<String> annotation_identifiers;
    public static boolean APPEND = false;
    public static boolean OVERWRITE = false;
    public static boolean write_log = false;
    public static String PHENOTYPE;
    public static boolean PROTEOME = false;
    public static boolean longest_transcripts = false;
    public static boolean compare_sequences = false;
    public static boolean compare_genomes = false;
    public static String SELECTED_LABEL;
    public static String NODE_PROPERTY;
    public static String NODE_VALUE;
    public static String SELECTED_HMGROUPS;
    public static String INPUT_FILE;
    public static int grouping_version = -1;
    public static String GROUPING_VERSION;
    public static boolean FAST;
    public static String CLUSTERING_METHOD = "ML";
    public static int BLOSUM = 62;

    public static HashMap<String, Integer> phenotype_threshold_map;
    public static HashMap<Integer, String> geno_pheno_map; // genome number is coupled to its phenotype
    public static HashMap<String, int[]> phenotype_map; // key is phenptype, array contains genome numbers
    public static HashMap<Node, String> hmgroup_class_map; // key is homology group node, value is 'core', 'accessory' or 'unique'
    public static boolean PHASED_ANALYSIS = false;
    public static Path SELECTION_FILE;
    public static LinkedHashSet<String> selectedSequences;
    public static LinkedHashSet<Integer> selectedGenomes;
    public static LinkedHashSet<String> selectedSubgenomes;

    public enum RelTypes implements RelationshipType {
        FF, FR, RF, RR, // nucleotide <-> nucleotide, sequence <-> nucleotide
        has, // genome -> sequence,
        starts, // gene -> nucleotide
        stops, // gene -> nucleotide
        has_homolog, // homology_group -> mRNA
        codes_for, // gene -> mRNA
        is_parent_of, // gene -> mRNA, gene -> CDS, gene -> exon
        contributes_to, // CDS -> mRNA
        is_similar_to, // mRNA <-> mRNA                                           //TODO: currently also in use for linking similar functional annotation, should be changed
        annotates, // annotation -> genome
        varies, // ??
        has_phenotype, // genome -> phenotype
        has_go, // mrna -> go
        has_tigrfam, // mrna -> tigrfam
        has_pfam, // mrna -> pfam
        has_interpro, // mrna -> interpro
        //  is_part_of, // go -> go (part of GO hierarchy)
        is_a, // go -> go (part of GO hierarchy)
        part_of, // go -> go (part of GO hierarchy), gene -> bgc, mrna -> synteny
        positively_regulates, // go -> go (part of GO hierarchy)
        negatively_regulates, // go -> go (part of GO hierarchy)
        regulates, // go -> go (part of GO hierarchy)

        has_inactive_homolog, // inactive_homology_group -> mRNA
        has_busco, // genome -> busco
        is_syntenic_to, // mRNA <-> mRNA
        has_variant, // mRNA -> variant
    }

    public static char[] sym = new char[]{'A', 'C', 'G', 'T', 'M', 'R', 'W', 'S', 'Y', 'K', 'V', 'H', 'D', 'B', 'N'};
    public static int[] complement = new int[]{3, 2, 1, 0, 9, 8, 6, 7, 5, 4, 13, 12, 11, 10, 14};
    public static int[] binary = new int[256];

    public static long phaseTime;
    public static long num_nodes;
    public static int num_degenerates;
    public static long num_edges;
    public static long num_bases;
    public static Map<String, Label> labels;

    public static GenomeLayer seqLayer;
    public static AnnotationLayer annLayer;
    public static Grouping proLayer;
    public static Classification classification;
    public static CreateSkipArrays skip;
    public static Phylogeny phylogeny;
    public static FunctionalAnalysis functionalAnnotations;

    public static void setGlobals(Pantools pantools) {
        Path currentRelativePath = Paths.get("");
        current_path = currentRelativePath.toAbsolutePath() + "/";

        WORKING_DIRECTORY = pantools.getDatabaseDirectory() + "/";
        PATH_TO_THE_PANGENOME_DATABASE = pantools.getDatabaseDirectory() + "/";
        OUTPUT_PATH = pantools.getDatabaseDirectory() + "/";
        create_full_path_working_directory();

        binary['A'] = 0;
        binary['C'] = 1;
        binary['G'] = 2;
        binary['T'] = 3;
        binary['M'] = 4;
        binary['R'] = 5;
        binary['W'] = 6;
        binary['S'] = 7;
        binary['Y'] = 8;
        binary['K'] = 9;
        binary['V'] = 10;
        binary['H'] = 11;
        binary['D'] = 12;
        binary['B'] = 13;
        binary['N'] = 14;
        classification = new Classification();
        skip = new CreateSkipArrays();
        phylogeny = new Phylogeny();
        functionalAnnotations = new FunctionalAnalysis();
        seqLayer = new GenomeLayer();
        annLayer = new AnnotationLayer();
        proLayer = new Grouping();
        labels = new HashMap<>();

        //    public static Node db_node;
        String[] label_strings = new String[]{"pangenome", "genome", "sequence", "nucleotide", "degenerate", "annotation", "variation", "gene", "coding_gene",
                "mRNA", "tRNA", "rRNA", "CDS", "exon", "intron", "feature", "broken_protein", "homology_group"};
        for (String label_string : label_strings) {
            labels.put(label_string, Label.label(label_string));
        }
    }

    public static void setGenomeSelectionOptions(SelectGenomes selectGenomes) {
        Pantools.logger.debug("Setting genome selection options for {}", selectGenomes);
        if (selectGenomes == null) return;
        if (selectGenomes.getExclude() != null) skip_genomes = selectGenomes.getExclude().stream().map(String::valueOf)
                .collect(Collectors.joining(","));
        if (selectGenomes.getInclude() != null) target_genome = selectGenomes.getInclude().stream().map(String::valueOf)
                .collect(Collectors.joining(","));
        if (selectGenomes.getSelectionFile() != null){
            SELECTION_FILE = selectGenomes.getSelectionFile();
        }
    }

    public static void setReadGroup(HashMap<String, String> readGroup) {
        if (readGroup == null) return;
        //instantiate the SAMReadGroupRecord with read group ID
        if(readGroup.containsKey("ID")) {
            READ_GROUP = new SAMReadGroupRecord(readGroup.get("ID"));
        }
        else {
            throw new IllegalArgumentException("--read-group string must contain an ID.");
        }
        // set the remaining tags in SAMReadGroupRecord
        for (String key : readGroup.keySet()) {
            READ_GROUP.setAttribute(key, readGroup.get(key));
        }
    }
}
